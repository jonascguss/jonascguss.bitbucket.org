(function($) {


/**
* Bindning för datum
* lägger till datepicker till inputelement
*
* @binding rcc-date
*/
ko.bindingHandlers[RCC.bindingsPrefix + 'date'] = {
    init: function(element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
       ko.bindingHandlers['rcc-value'].init(element, valueAccessor, allBindingsAccessor, viewModel, bindingContext);
       
       // add datepicker
       $(element).datepicker({ 
       		dateFormat: "yy-mm-dd",
       		/* fix buggy IE focus functionality */
            fixFocusIE: false,
			
            
            /* blur needed to correctly handle placeholder text */
            onSelect: function(dateText, inst) {
                  this.fixFocusIE = true;
                  $(this).blur().change().focus();
            },
            onClose: function(dateText, inst) {
                  this.fixFocusIE = true;
                  this.focus();
            },
            beforeShow: function(input, inst) {
                  var result = $.browser.msie ? !this.fixFocusIE : true;
                  this.fixFocusIE = false;
                  return result;
            }
       
       });  
    },
    update: function(element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
    
     	var value = valueAccessor();
		
		if(value() == "") {
			value(null);
		}
		
		if (value()/* && value.rcc.validation.errors().length == 0*/){
			
			// parse date to yyyy-mm-dd
			var parsedDate = RCC.Utils.parseYMD(value());
			if(parsedDate)
				value(RCC.Utils.ISO8601(parsedDate));
		}	
	}
};

/**
 * Bindning för chosen
 *
 * @binding rcc-chosen
 */
ko.bindingHandlers[RCC.bindingsPrefix + 'chosen'] = {
    init: function(element, valueAccessor) {
		$(element).focus(function() {
            if(!($(element).next().hasClass('chosen-container')))
                $(element).chosen({'search_contains': true});

            $(element).css({'display':'none'});
            $(element).next().css({'display':'inline-block'});
            $(element).trigger('chosen:open');
        });

        $(element).on('chosen:blur', function(evt, params) {
            $(element).next().css({'display':'none'});
            $(element).css({'display':'inline'});
        });
    }
};


/**
* Bindning för help
* lägger till help till ett element
*
* @binding rcc-help
*/
ko.bindingHandlers[RCC.bindingsPrefix + 'help'] = {
    init: function(element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
	    var title = $(element).text();
		text = valueAccessor;
		
		$(element).qtip({
			content: {
				text: text,
				title: {text: title}
			},
			show: 'click',
			hide: 'unfocus',
		 
			position: {
				my: 'left center',
				at: 'center right'
			}
		});
    }
}; 

 

/**
* Bindning för att ge en knapp jqury stil
*
* @binding rcc-button
*/
ko.bindingHandlers[RCC.bindingsPrefix + 'button'] = {
    init: function(element, valueAccessor, allBindingsAccessor) {
		var value = valueAccessor();
		if(value == 'add')
			$(element).button({icons: {primary: "ui-icon-circle-plus"}});  	
		else if(value == 'delete')
			$(element).button({icons: {primary: "ui-icon-circle-minus"}}); 
		else if(value == 'generic')
			$(element).button(); 	
    }
};

ko.bindingHandlers[RCC.bindingsPrefix + 'compare'] = {
    init: function(element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {	  
		var valueBinding;

	  	if(allBindingsAccessor()['rcc-value'])
    		valueBinding = allBindingsAccessor()['rcc-value'];
    	else if(allBindingsAccessor()['rcc-var'])
    		valueBinding = allBindingsAccessor()['rcc-var'];
    	else if(allBindingsAccessor()['rcc-list'])
    		valueBinding = allBindingsAccessor()['rcc-list'];
    	else if(allBindingsAccessor()['rcc-date'])
    		valueBinding = allBindingsAccessor()['rcc-date'];
    	else if(allBindingsAccessor()['rcc-checked'])
    		valueBinding = allBindingsAccessor()['rcc-checked'];
    	else if(allBindingsAccessor()['rcc-decimal'])
    		valueBinding = allBindingsAccessor()['rcc-decimal'];	
    	else if(allBindingsAccessor()['rcc-autocomplete'])
    		valueBinding = allBindingsAccessor()['rcc-autocomplete'];
		
		$(element).after( "<span class='rcc-diff rcc-diff-hidden'>* </span>" );
	  
	  	ko.computed(function(){
			if (valueBinding && valueBinding.rcc.showInclude()){
				
				if(vm.RegisterDataExist()){
					var text;
					if(!(valueAccessor() in vm.getSelectedNPCR(vm.$vd.prostataregister_vd())))
						text = 'Variabel saknas';
					else if(!vm.getSelectedNPCR(vm.$vd.prostataregister_vd())[valueAccessor()])
						text = 'Värde saknas';
					else if(allBindingsAccessor()['rcc-checked'])
						if(vm.getSelectedNPCR(vm.$vd.prostataregister_vd())[valueAccessor()])
							text = 'true';
						else
							text = 'false';
					else
						text = vm.getSelectedNPCR(vm.$vd.prostataregister_vd())[valueAccessor()];	
	  					
	  				$(element).removeData("qtip").qtip({
						content: {
							text: text,
							title: {text: 'Registervärde'}
						},
						position: {
							my: 'top center',
							at: 'bottom center'
						}
					});
					
					
					
					
					var currentValue;
					if(allBindingsAccessor()['rcc-list'] && valueBinding())
						currentValue = valueBinding().text;
					else if(allBindingsAccessor()['rcc-checked'] && !valueBinding())
						currentValue = null;
					else if(allBindingsAccessor()['rcc-decimal']){
						currentValue = parseFloat(valueBinding());
					}
					else
						currentValue = valueBinding();
					
					var registerValue = vm.getSelectedNPCR(vm.$vd.prostataregister_vd())[valueAccessor()];
						
					//console.log(valueAccessor() + " Register: " + registerValue + " Form: " + currentValue);	
						
					if(currentValue == registerValue)
						$(element).next().addClass("rcc-diff-hidden");
					else
						$(element).next().removeClass("rcc-diff-hidden");

				}
				
					
			}
			else{
				$(element).qtip('destroy');
				$(element).next().addClass("rcc-diff-hidden");
			}
		});
	}
};



/**
* Bindning för att ge ett fieldset möjligt att dynamiskt öppnas och stängas 
* true: collapsable, stängd default
* false: collapsable, öppet default
*
* @binding collapse
*/
ko.bindingHandlers.collapse = {
    init: function(element, valueAccessor, allBindingsAccessor) {
        var value = valueAccessor();
        $(element).addClass("collapsefieldset");
        $(element).collapsefieldset({collapsed:value});     		
    }
};


/**
* Bindning för autocompletet
*
* @binding rcc-autocomplete
*/
ko.bindingHandlers[RCC.bindingsPrefix + 'autocomplete'] = {
    init: function(element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
       ko.bindingHandlers['rcc-value'].init(element, valueAccessor, allBindingsAccessor, viewModel, bindingContext);
	   
	   var value = valueAccessor();
	   var objects = allBindingsAccessor()['autocomplete-objects'];
	   
	   $(element).autocomplete(
	{
		      minLength: 0,
		      source: objects,
			  change: function (event, ui) {
			        if(!ui.item){
			            $(event.target).val("");
						value(undefined);
			        }
			    }, 
				focus: function (event, ui) {
			        return false;
			    },
			  select: function( event, ui ) {
				$(element).val( ui.item.label + "  (" + ui.item.value + ")");
				value(ui.item.label + "  (" + ui.item.value + ")");
				return false;
			 }
		 });
    }
};

/**
* Bindning för decimaltal
*
* @binding rcc-decimal
*/


ko.bindingHandlers[RCC.bindingsPrefix + 'decimal'] = {
    init: function(element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
    	ko.bindingHandlers['rcc-value'].init(element, valueAccessor, allBindingsAccessor, viewModel, bindingContext);

		var newValue = valueAccessor();
     	var precision = ko.utils.unwrapObservable(allBindingsAccessor().precision) || ko.bindingHandlers[RCC.bindingsPrefix + 'decimal'].defaultPrecision;
     	var formattedValue;
		
		if(newValue()){
			if(typeof newValue() == 'string')
				formattedValue = parseFloat(newValue().replace(',','.')).toFixed(precision);
			else
				formattedValue = newValue().toFixed(precision);	
		}
		$(element).val(formattedValue);

        //handle the field changing
        ko.utils.registerEventHandler(element, "change", function () {
            var observable = valueAccessor();
            observable($(element).val());
        });
		
		
    },
    update: function(element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
     	var newValue = valueAccessor();
     	var precision = ko.utils.unwrapObservable(allBindingsAccessor().precision) || ko.bindingHandlers[RCC.bindingsPrefix + 'decimal'].defaultPrecision;
		var formattedValue;
		
		if(newValue()){
			if(typeof newValue() == 'string')
				formattedValue = parseFloat(newValue().replace(',','.')).toFixed(precision);
			else
				formattedValue = newValue().toFixed(precision);	
		}
		
		$(element).val(formattedValue);
	},
    defaultPrecision: 1
}




RCC.Validation.Tests.NotFutureDate =
function ( date, opt )
{
	if ( !opt )
		opt = {};

	return function ( )
	{
		var d = date();

		if ( opt.ignoreMissingValues && !RCC.Validation.Tests._hasValue( d, true ) )
			return { result: 'ok' };

		var parsedDate = RCC.Utils.parseYMD(d);
		var todayDate = RCC.Utils.parseYMD(getDagensDatum());
		
		if(parsedDate > todayDate)
			return { result: 'failed', message: 'Datum kan inte inträffa efter dagens datum'};
		else 
			return { result: 'ok' };
	};
};

RCC.Validation.Tests.NotAfterDeathDate =
function ( date, opt )
{
	if ( !opt )
		opt = {};

	return function ( )
	{
		var d = date();

		if ( opt.ignoreMissingValues && !RCC.Validation.Tests._hasValue( d, true ) )
			return { result: 'ok' };

		var parsedDate = RCC.Utils.parseYMD(d);
		var deathDate = RCC.Utils.parseYMD(opt.deathDate);
		
		if(parsedDate > deathDate)
			return { result: 'failed', message: 'Datum kan inte inträffa efter dödsdatum'};
		else 
			return { result: 'ok' };
	};
};

RCC.Validation.Tests.NotBeforeBirthDate =
function ( date, opt )
{
    if ( !opt )
        opt = {};

    return function ( )
    {
        var d = date();

        if ( opt.ignoreMissingValues && !RCC.Validation.Tests._hasValue( d, true ) )
            return { result: 'ok' };

        var parsedDate = RCC.Utils.parseYMD(d);
        var birthDate = RCC.Utils.parseYMD(opt.birthDate);

        if(parsedDate < birthDate)
            return { result: 'failed', message: 'Datum kan inte inträffa före födelsedatum'};
        else
            return { result: 'ok' };
    };
};

RCC.Validation.Tests.CompareDate =
function ( date, opt )
{
	

	if ( !opt )
		opt = {};

	return function ( )
	{
		
		var d = date();

		if ( opt.ignoreMissingValues && !RCC.Validation.Tests._hasValue( d, true ) )
			return { result: 'ok' };

		var parsedDate = RCC.Utils.parseYMD(d);
		
		if (RCC.Validation.Tests._hasValue(opt.compareDate)){
			var parsedCompareDate =  RCC.Utils.parseYMD(opt.compareDate);
			
			if(parsedCompareDate > parsedDate)
				return { result: 'failed', message: 'Datum kan inte inträffa innan ' + opt.compareTitle + ' (' + opt.compareDate + ')'};
			else 
				return { result: 'ok' };
		}
		else
			return { result: 'ok' };
	};
};

RCC.Validation.Tests.NotBeforeDate =
function ( date, opt )
{
	if ( !opt )
		opt = {};

	return function ( )
	{
		var d = date();

		if ( opt.ignoreMissingValues && !RCC.Validation.Tests._hasValue( d, true ) )
			return { result: 'ok' };

		var parsedDate = RCC.Utils.parseYMD(d);
		
		if (opt.dependables){	
			for (var i=0; i < opt.dependables.length; i++){
				if(RCC.Validation.Tests._hasValue(opt.dependables[i])){
					var parsedCompareDate =  RCC.Utils.parseYMD(opt.dependables[i]());
					if(parsedCompareDate > parsedDate)
						return { result: 'failed', message: 'Datum kan inte inträffa innan ' + opt.dependables[i].rcc.regvar.compareDescription + ' (' + opt.dependables[i]() + ')'};
				}
			}	
		}
		
		return { result: 'ok' };
	};
};

RCC.Validation.Tests.NotAfterDate =
function ( date, opt )
{
    if ( !opt )
        opt = {};

    return function ( )
    {
        var d = date();

        if ( opt.ignoreMissingValues && !RCC.Validation.Tests._hasValue( d, true ) )
            return { result: 'ok' };

        var parsedDate = RCC.Utils.parseYMD(d);
        
        if (opt.dependables){   
            for (var i=0; i < opt.dependables.length; i++){
                if(RCC.Validation.Tests._hasValue(opt.dependables[i])){
                    var parsedCompareDate =  RCC.Utils.parseYMD(opt.dependables[i]());
                    if(parsedCompareDate < parsedDate)
                        return { result: 'failed', message: 'Datum kan inte inträffa efter ' + opt.dependables[i].rcc.regvar.compareDescription + ' (' + opt.dependables[i]() + ')'};
                }
            }   
        }
        
        return { result: 'ok' };
    };
};

RCC.Validation.Tests.AtleastOneYes =
function ( obj, opt )
{
	if ( !opt )
		opt = {};

	return function ( )
	{
		if ( opt.ignoreMissingValues && !obj())
			return { result: 'ok' };
		
		if(opt.dependables.length > 0){
			for (var i=0; i < opt.dependables.length; i++){
				if(!opt.dependables[i]() || (opt.dependables[i]() && opt.dependables[i]().value != '0')){
			  		return { result: 'ok' };
			  	}
			}
			obj.rcc.accessed(true);
			for (var i=0; i < opt.dependables.length; i++){
				opt.dependables[i].rcc.accessed(true);
			}
			return { result: 'failed', message: "Minst ett svar skall vara 'Ja'"};
		}
		else
			return { result: 'ok' };
	};
};

RCC.Validation.Tests.AtleastOneNo =
function ( obj, opt )
{
	if ( !opt )
		opt = {};

	return function ( )
	{
		if ( opt.ignoreMissingValues && !obj())
			return { result: 'ok' };
		
		if(opt.dependables.length > 0){
			for (var i=0; i < opt.dependables.length; i++){
				if(!opt.dependables[i]() || (opt.dependables[i]() && opt.dependables[i]().value != '1')){
			  		return { result: 'ok' };
			  	}
			}
			obj.rcc.accessed(true);
			for (var i=0; i < opt.dependables.length; i++){
				opt.dependables[i].rcc.accessed(true);
			}
			return { result: 'failed', message: "Minst ett svar skall vara 'Nej'"};
		}
		else
			return { result: 'ok' };
	};
};

RCC.Validation.Tests.AtleastOneChecked =
function ( obj, opt )
{
	if ( !opt )
		opt = {};

	return function ( )
	{
		if((!opt.condition || (opt.condition() && opt.condition().value == 1)) && opt.dependables.length > 0){
			for (var i=0; i < opt.dependables.length; i++){
				if(opt.dependables[i]()){
			  		return { result: 'ok' };
			  	}
			}
			return { result: 'failed', message: "Minst en kryssruta skall vara vald"};
		}
		else
			return { result: 'ok' };
	};
};

RCC.Validation.Tests.atLeastOneValue =
function ( obj, opt )
{
	if ( !opt )
		opt = {};

	return function ( )
	{
		//if ( opt.ignoreMissingValues && !obj())
		//	return { result: 'ok' };
		
		if(opt.dependables.length > 0) {
			for (var i=0; i < opt.dependables.length; i++) {
				// Om en dependable är ifylld, returnera ok
				if(opt.dependables[i]()) {
					return { result: 'ok' }; // Hittar en som är ifylld med något värde
				}
			}
			// Om ingen variabel är ifylld, returnera failed
			return { result: 'failed', message: "Minst ett värde skall fyllas i" };
		}
		else //Finns inga beroende variabler 
			return { result: 'ok' };
	};
};

RCC.Validation.Tests.NotSameListval =
function ( obj, opt )
{
	if ( !opt )
		opt = {};
	
	return function ( )
	{
		
		if ( opt.ignoreMissingValues && !obj())
			return { result: 'ok' };
		
		var value = obj().value;
		
		if(opt.dependables.length > 0){
			for (var i=0; i < opt.dependables.length; i++){
				if(!opt.dependables[i]() || (opt.dependables[i]() && opt.dependables[i]().value != value)) {
			  		return { result: 'ok' };
			  	}
			}
			obj.rcc.accessed(true);
			for (var i=0; i < opt.dependables.length; i++){
				opt.dependables[i].rcc.accessed(true);
			}
			return { result: 'failed', message: "Dessa får inte ha samma listvärde!"};
		}
		else
			return { result: 'ok' };
	};
};

RCC.Validation.Tests.DynamicMinMaxInteger =
function ( obj, opt )
{
	if ( !opt )
		opt = {};

	return function ( )
	{
		if ( opt.ignoreMissingValues && !obj())
			return { result: 'ok' };
		
		if (opt.min){	
			if(parseInt(obj())<parseInt(opt.min()))
			return { result: 'failed', message: 'Minsta tillåtna värde: ' + opt.min()};
		}
		if (opt.max){	
			if(parseInt(obj())>parseInt(opt.max()))
			return { result: 'failed', message: 'Största tillåtna värde: ' + opt.max()};
		}
		
		
		else
			return { result: 'ok' };
	};
};

RCC.Validation.Tests.DynamicMinMaxDecimal =
function ( obj, opt )
{
	if ( !opt )
		opt = {};

	return function ( )
	{
		if ( opt.ignoreMissingValues && !obj())
			return { result: 'ok' };
		
		if (opt.min){	
			if(parseFloat(obj())<parseFloat(opt.min()))
			return { result: 'failed', message: 'Minsta tillåtna värde: ' + opt.min()};
		}
		if (opt.max){	
			if(parseFloat(obj())>parseFloat(opt.max()))
			return { result: 'failed', message: 'Största tillåtna värde: ' + opt.max()};
		}
		else
			return { result: 'ok' };
	};
};


/**
 * Testar om flera objekt har valda värden som är giltiga samtidigt.
 * 		obj = objekt som valideras (Biopsi), 
 *		val = objektets giltiga listvärden, 
 *		opt = 	dependables: lista av ingående objekt, 
 *				values: lista med ingående objekts listor över giltiga värden (de måste ligga i samma ordning som objekten)
 *				ignoreMissingValues: Kör inte valideringen när objektet saknar värde
 *	Ex
 *	Om Biopsi = Ja får inte Diagnos baseras på 'Bilddiagnostik enbart' eller 'Klinisk undersökning'
 *  vm.Biopsi.rcc.validation.add(new RCC.Validation.Tests.ValidListValues(vm.Biopsi, ["1"], {dependables: [vm.Vdigr], values: [["3","5"]], ignoreMissingValues: true}));
 * 	
 */
RCC.Validation.Tests.ValidListValues =
function ( obj, val, opt ) {
	if ( !opt )
		opt = {};

	return function ( )
	{
        objVals = val.toString() + ",";

        //Om objektet inte har några värden eller om 
        //om objektet inte har rätt värde
		if ((opt.ignoreMissingValues && !obj()) || (obj() && objVals.search(obj().value + ",") == -1))
			return { result: 'ok' };

        var foundVal=false;
        var okVal = new Array();

        if(opt.dependables.length > 0 && opt.values.length > 0){
			for (var i=0; i < opt.dependables.length; i++){
                foundVal=false;
                if(!opt.dependables[i]()) {
                    return { result: 'ok' };
                }
				if(opt.values[i] && opt.values[i].length > 0 && RCC.Validation.Tests._hasValue(opt.dependables[i])) {
                    var validVals = opt.values[i].toString() + ",";
                    foundVal=(validVals.search(opt.dependables[i]().value + ",") != -1);
                }
                okVal.push(foundVal);
                foundVal=false;
			}

            //alert(okVal.toString());
            //Om något av objekten med dess värden inte är godkända
            if(okVal.toString().search("false") != -1) {
                messStr = "När " + obj.rcc.regvar.compareDescription + " = " + obj().text + ", måste någon av följande vara: ";
                obj.rcc.accessed(true);

                for(var i=0; i<opt.dependables.length; i++) {
                    
                    var antal = 0;

                    for(var j=0; j<opt.values[i].length; j++) {    
                        for(var k=0; k<opt.dependables[i].rcc.term.listValues.length; k++) {
                            if(RCC.Validation.Tests._hasValue(opt.dependables[i]) && opt.dependables[i].rcc.term.listValues[k].value == opt.values[i][j]) {
                                if(antal>0)
                                    messStr += " eller ";
                                messStr += opt.dependables[i].rcc.regvar.compareDescription + " = " + opt.dependables[i].rcc.term.listValues[k].text;
                                opt.dependables[i].rcc.accessed(true);
                                antal++;
                            }
                        }           
                    }
                }

                //alert(messStr);

                return { result: 'failed', message: messStr};
            }
            else
                return { result: 'ok' };
		}
		else
			return { result: 'ok' };
	};
};


})(jQuery);