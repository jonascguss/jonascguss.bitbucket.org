(function($) {

positioner = [
    {
        "id": 102,
        "name": "Kir klin",
        "shortName": null,
        "code": "301",
        "description": "Gällivare sjukhus",
        "fullNameWithCode": "OC Norr (6) - Gällivare sjukhus (065012) - Kir klin (301)",
        "parentId": 89,
        "regionId": 1,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 107,
        "name": "Kir klin",
        "shortName": null,
        "code": "301",
        "description": "Härnösands sjukhus",
        "fullNameWithCode": "OC Norr (6) - Härnösands sjukhus (062012) - Kir klin (301)",
        "parentId": 84,
        "regionId": 1,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 101,
        "name": "Kir klin",
        "shortName": null,
        "code": "301",
        "description": "Kalix sjukhus",
        "fullNameWithCode": "OC Norr (6) - Kalix sjukhus (065014) - Kir klin (301)",
        "parentId": 91,
        "regionId": 1,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 104,
        "name": "Kir klin",
        "shortName": null,
        "code": "301",
        "description": "Lycksele lasarett",
        "fullNameWithCode": "OC Norr (6) - Lycksele lasarett (064011) - Kir klin (301)",
        "parentId": 88,
        "regionId": 1,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 24,
        "name": "Kir klin",
        "shortName": null,
        "code": "301",
        "description": "NUS Umeå",
        "fullNameWithCode": "OC Norr (6) - NUS Umeå (064001) - Kir klin (301)",
        "parentId": 7,
        "regionId": 1,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 711,
        "name": "Lung klin",
        "shortName": null,
        "code": "111",
        "description": "NUS Umeå",
        "fullNameWithCode": "OC Norr (6) - NUS Umeå (064001) - Lung klin (111)",
        "parentId": 7,
        "regionId": 1,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 807,
        "name": "Neurokir klin",
        "shortName": null,
        "code": "331",
        "description": "Neurokirurgisk klinik, NUS -  Umeå",
        "fullNameWithCode": "OC Norr (6) - NUS Umeå (064001) - Neurokir klin (331)",
        "parentId": 7,
        "regionId": 1,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 430,
        "name": "Onk klin",
        "shortName": null,
        "code": "741",
        "description": "NUS Umeå",
        "fullNameWithCode": "OC Norr (6) - NUS Umeå (064001) - Onk klin (741)",
        "parentId": 7,
        "regionId": 1,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 81,
        "name": "Urol klin",
        "shortName": null,
        "code": "361",
        "description": "NUS Umeå",
        "fullNameWithCode": "OC Norr (6) - NUS Umeå (064001) - Urol klin (361)",
        "parentId": 7,
        "regionId": 1,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 455,
        "name": "ÖNH klin",
        "shortName": null,
        "code": "521",
        "description": "NUS Umeå",
        "fullNameWithCode": "OC Norr (6) - NUS Umeå (064001) - ÖNH klin (521)",
        "parentId": 7,
        "regionId": 1,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 95,
        "name": "Kir klin",
        "shortName": null,
        "code": "301",
        "description": "Piteå älvdals sjukhus",
        "fullNameWithCode": "OC Norr (6) - Piteå älvdals sjukhus (065013) - Kir klin (301)",
        "parentId": 90,
        "regionId": 1,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 103,
        "name": "Kir klin",
        "shortName": null,
        "code": "301",
        "description": "Skellefteå lasarett",
        "fullNameWithCode": "OC Norr (6) - Skellefteå lasarett (064010) - Kir klin (301)",
        "parentId": 87,
        "regionId": 1,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 94,
        "name": "Kir klin",
        "shortName": null,
        "code": "301",
        "description": "Sollefteå sjukhus",
        "fullNameWithCode": "OC Norr (6) - Sollefteå sjukhus (062013) - Kir klin (301)",
        "parentId": 85,
        "regionId": 1,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 412,
        "name": "Med klin",
        "shortName": null,
        "code": "101",
        "description": "Sollefteå sjukhus",
        "fullNameWithCode": "OC Norr (6) - Sollefteå sjukhus (062013) - Med klin (101)",
        "parentId": 85,
        "regionId": 1,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 98,
        "name": "Kir klin",
        "shortName": null,
        "code": "301",
        "description": "Sunderby sjukhus",
        "fullNameWithCode": "OC Norr (6) - Sunderby sjukhus (065016) - Kir klin (301)",
        "parentId": 93,
        "regionId": 1,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 714,
        "name": "Lung klin",
        "shortName": null,
        "code": "111",
        "description": "Sunderby sjukhus",
        "fullNameWithCode": "OC Norr (6) - Sunderby sjukhus (065016) - Lung klin (111)",
        "parentId": 93,
        "regionId": 1,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 458,
        "name": "ÖNH klin",
        "shortName": null,
        "code": "521",
        "description": "Sunderby sjukhus",
        "fullNameWithCode": "OC Norr (6) - Sunderby sjukhus (065016) - ÖNH klin (521)",
        "parentId": 93,
        "regionId": 1,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 895,
        "name": "Hud klin",
        "shortName": null,
        "code": "211",
        "description": "Sundsvalls sjukhus",
        "fullNameWithCode": "OC Norr (6) - Sundsvalls sjukhus (062010) - Hud klin (211)",
        "parentId": 82,
        "regionId": 1,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 96,
        "name": "Kir klin",
        "shortName": null,
        "code": "301",
        "description": "Sundsvalls sjukhus",
        "fullNameWithCode": "OC Norr (6) - Sundsvalls sjukhus (062010) - Kir klin (301)",
        "parentId": 82,
        "regionId": 1,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 445,
        "name": "Onk klin",
        "shortName": null,
        "code": "741",
        "description": "Sundsvalls sjukhus",
        "fullNameWithCode": "OC Norr (6) - Sundsvalls sjukhus (062010) - Onk klin (741)",
        "parentId": 82,
        "regionId": 1,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 100,
        "name": "Kir klin",
        "shortName": null,
        "code": "301",
        "description": "Östersunds sjukhus",
        "fullNameWithCode": "OC Norr (6) - Östersunds sjukhus (063010) - Kir klin (301)",
        "parentId": 86,
        "regionId": 1,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 106,
        "name": "Kir klin",
        "shortName": null,
        "code": "301",
        "description": "Öviks sjukhus",
        "fullNameWithCode": "OC Norr (6) - Öviks sjukhus (062011) - Kir klin (301)",
        "parentId": 83,
        "regionId": 1,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 410,
        "name": "Med klin",
        "shortName": null,
        "code": "101",
        "description": "Öviks sjukhus",
        "fullNameWithCode": "OC Norr (6) - Öviks sjukhus (062011) - Med klin (101)",
        "parentId": 83,
        "regionId": 1,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 1381,
        "name": "Privatklinik",
        "shortName": null,
        "code": "010",
        "description": "Bröstmott. Christinaklin. Sophiahemmet",
        "fullNameWithCode": "OC Sthlm/Gotland (1) - Bröstmott. Christinaklin. Sophiahemmet (097563) - Privatklinik (010)",
        "parentId": 1380,
        "regionId": 2,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 1144,
        "name": "Privatläkare",
        "shortName": null,
        "code": "010",
        "description": "Christer Kihlfors (090410)",
        "fullNameWithCode": "OC Sthlm/Gotland (1) - Christer Kihlfors (090410) - Privatläkare (010)",
        "parentId": 1143,
        "regionId": 2,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 225,
        "name": "Kirurgkliniken",
        "shortName": null,
        "code": "301",
        "description": "Danderyds sjukhus",
        "fullNameWithCode": "OC Sthlm/Gotland (1) - Danderyds sjukhus (011010) - Kirurgkliniken (301)",
        "parentId": 224,
        "regionId": 2,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 529,
        "name": "Onkologkliniken",
        "shortName": null,
        "code": "741",
        "description": "Danderyds sjukhus",
        "fullNameWithCode": "OC Sthlm/Gotland (1) - Danderyds sjukhus (011010) - Onkologkliniken (741)",
        "parentId": 224,
        "regionId": 2,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 535,
        "name": "Urologkliniken",
        "shortName": null,
        "code": "361",
        "description": "Danderyds sjukhus",
        "fullNameWithCode": "OC Sthlm/Gotland (1) - Danderyds sjukhus (011010) - Urologkliniken (361)",
        "parentId": 224,
        "regionId": 2,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 1297,
        "name": "Privatläkare",
        "shortName": null,
        "code": "010",
        "description": "Dushanka Kristiansson (090952)",
        "fullNameWithCode": "OC Sthlm/Gotland (1) - Dushanka Kristiansson (090952) - Privatläkare (010)",
        "parentId": 1296,
        "regionId": 2,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 227,
        "name": "Kirurgkliniken",
        "shortName": null,
        "code": "301",
        "description": "Ersta sjukhus",
        "fullNameWithCode": "OC Sthlm/Gotland (1) - Ersta sjukhus (010481) - Kirurgkliniken (301)",
        "parentId": 226,
        "regionId": 2,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 1379,
        "name": "Privatklinik",
        "shortName": null,
        "code": "010",
        "description": "Försäkringsmott, Sophiahemmet",
        "fullNameWithCode": "OC Sthlm/Gotland (1) - Försäkringsmott, Sophiahemmet (010783) - Privatklinik (010)",
        "parentId": 1378,
        "regionId": 2,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 138,
        "name": "Kirurgkliniken",
        "shortName": null,
        "code": "301",
        "description": "KS Huddinge",
        "fullNameWithCode": "OC Sthlm/Gotland (1) - KS Huddinge (011002) - Kirurgkliniken (301)",
        "parentId": 137,
        "regionId": 2,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 612,
        "name": "Kvinnoklinik",
        "shortName": null,
        "code": "451",
        "description": "HS Huddinge",
        "fullNameWithCode": "OC Sthlm/Gotland (1) - KS Huddinge (011002) - Kvinnoklinik (451)",
        "parentId": 137,
        "regionId": 2,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 650,
        "name": "Lungkliniken",
        "shortName": null,
        "code": "111",
        "description": "KS Huddinge",
        "fullNameWithCode": "OC Sthlm/Gotland (1) - KS Huddinge (011002) - Lungkliniken (111)",
        "parentId": 137,
        "regionId": 2,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 187,
        "name": "Urologkliniken",
        "shortName": null,
        "code": "361",
        "description": "KS Huddinge",
        "fullNameWithCode": "OC Sthlm/Gotland (1) - KS Huddinge (011002) - Urologkliniken (361)",
        "parentId": 137,
        "regionId": 2,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 119,
        "name": "Bröstcentrum",
        "shortName": null,
        "code": "301M10",
        "description": "KS Solna",
        "fullNameWithCode": "OC Sthlm/Gotland (1) - KS Solna (011001) - Bröstcentrum (301M10)",
        "parentId": 8,
        "regionId": 2,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 1184,
        "name": "Endokrinologen",
        "shortName": null,
        "code": "161",
        "description": "KS Solna",
        "fullNameWithCode": "OC Sthlm/Gotland (1) - KS Solna (011001) - Endokrinologen (161)",
        "parentId": 8,
        "regionId": 2,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 342,
        "name": "Hematologkliniken",
        "shortName": null,
        "code": "107",
        "description": "KS Solna",
        "fullNameWithCode": "OC Sthlm/Gotland (1) - KS Solna (011001) - Hematologkliniken (107)",
        "parentId": 8,
        "regionId": 2,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 19,
        "name": "Kirurgkliniken",
        "shortName": null,
        "code": "301",
        "description": "KS Solna",
        "fullNameWithCode": "OC Sthlm/Gotland (1) - KS Solna (011001) - Kirurgkliniken (301)",
        "parentId": 8,
        "regionId": 2,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 609,
        "name": "Kvinnoklinik",
        "shortName": null,
        "code": "451",
        "description": "KS Solna",
        "fullNameWithCode": "OC Sthlm/Gotland (1) - KS Solna (011001) - Kvinnoklinik (451)",
        "parentId": 8,
        "regionId": 2,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 648,
        "name": "Lungkliniken",
        "shortName": null,
        "code": "111",
        "description": "KS Solna",
        "fullNameWithCode": "OC Sthlm/Gotland (1) - KS Solna (011001) - Lungkliniken (111)",
        "parentId": 8,
        "regionId": 2,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 20,
        "name": "Medicinkliniken",
        "shortName": null,
        "code": "101",
        "description": "KS Solna",
        "fullNameWithCode": "OC Sthlm/Gotland (1) - KS Solna (011001) - Medicinkliniken (101)",
        "parentId": 8,
        "regionId": 2,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 805,
        "name": "Neurologiska kliniken",
        "shortName": null,
        "code": "221",
        "description": "Karolinska Universitetssjukhuset, KS (011001)",
        "fullNameWithCode": "OC Sthlm/Gotland (1) - KS Solna (011001) - Neurologiska kliniken (221)",
        "parentId": 8,
        "regionId": 2,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 1002,
        "name": "Onkologi Gyn",
        "shortName": null,
        "code": "751",
        "description": "KS Solna",
        "fullNameWithCode": "OC Sthlm/Gotland (1) - KS Solna (011001) - Onkologi Gyn (751)",
        "parentId": 8,
        "regionId": 2,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 518,
        "name": "Onkologkliniken",
        "shortName": null,
        "code": "741",
        "description": "KS Solna",
        "fullNameWithCode": "OC Sthlm/Gotland (1) - KS Solna (011001) - Onkologkliniken (741)",
        "parentId": 8,
        "regionId": 2,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 1003,
        "name": "Ortopedkirurg",
        "shortName": null,
        "code": "311",
        "description": "KS Solna",
        "fullNameWithCode": "OC Sthlm/Gotland (1) - KS Solna (011001) - Ortopedkirurg (311)",
        "parentId": 8,
        "regionId": 2,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 956,
        "name": "Plastikkir. klin",
        "shortName": null,
        "code": "351",
        "description": "Karolinska Universitetssjukhuset, Solna (011001)",
        "fullNameWithCode": "OC Sthlm/Gotland (1) - KS Solna (011001) - Plastikkir. klin (351)",
        "parentId": 8,
        "regionId": 2,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 186,
        "name": "Urologkliniken",
        "shortName": null,
        "code": "361",
        "description": "KS Solna",
        "fullNameWithCode": "OC Sthlm/Gotland (1) - KS Solna (011001) - Urologkliniken (361)",
        "parentId": 8,
        "regionId": 2,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 566,
        "name": "ÖNH",
        "shortName": null,
        "code": "521",
        "description": "KS Solna",
        "fullNameWithCode": "OC Sthlm/Gotland (1) - KS Solna (011001) - ÖNH (521)",
        "parentId": 8,
        "regionId": 2,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 995,
        "name": "Privatläkare",
        "shortName": null,
        "code": "010",
        "description": "Magnus Törnblom (090982)",
        "fullNameWithCode": "OC Sthlm/Gotland (1) - Magnus Törnblom (090982) - Privatläkare (010)",
        "parentId": 994,
        "regionId": 2,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 321,
        "name": "kirurgkliniken",
        "shortName": null,
        "code": "301",
        "description": "Norrtälje sjukhus",
        "fullNameWithCode": "OC Sthlm/Gotland (1) - Norrtälje sjukhus (011012) - kirurgkliniken (301)",
        "parentId": 320,
        "regionId": 2,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 1129,
        "name": "Ultragyn",
        "shortName": null,
        "code": "431",
        "description": "Odenplans läkarhus",
        "fullNameWithCode": "OC Sthlm/Gotland (1) - Odenplans läkarhus (097371) - Ultragyn (431)",
        "parentId": 1128,
        "regionId": 2,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 436,
        "name": "Okänd klinik",
        "shortName": null,
        "code": "999",
        "description": "Privata sjukhus",
        "fullNameWithCode": "OC Sthlm/Gotland (1) - Privata sjukhus () - Okänd klinik (999)",
        "parentId": 385,
        "regionId": 2,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 434,
        "name": "Okänd klinik",
        "shortName": null,
        "code": "999",
        "description": "Privatläkare",
        "fullNameWithCode": "OC Sthlm/Gotland (1) - Privatläkare (011199) - Okänd klinik (999)",
        "parentId": 383,
        "regionId": 2,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 23,
        "name": "Kirurgkliniken",
        "shortName": null,
        "code": "301",
        "description": "S:t Görans sjukhus",
        "fullNameWithCode": "OC Sthlm/Gotland (1) - S:t Görans sjukhus (010011) - Kirurgkliniken (301)",
        "parentId": 22,
        "regionId": 2,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 1070,
        "name": "Urologen",
        "shortName": null,
        "code": "361",
        "description": "Sabbsberg närsjukhus",
        "fullNameWithCode": "OC Sthlm/Gotland (1) - Sabbsberg närsjukhus (010484) - Urologen (361)",
        "parentId": 463,
        "regionId": 2,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 474,
        "name": "Kirurgkliniken",
        "shortName": null,
        "code": "301",
        "description": "Sophiahemmet",
        "fullNameWithCode": "OC Sthlm/Gotland (1) - Sophiahemmet (010483) - Kirurgkliniken (301)",
        "parentId": 473,
        "regionId": 2,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 1130,
        "name": "Medicinkliniken",
        "shortName": null,
        "code": "101",
        "description": "Sophiahemmet",
        "fullNameWithCode": "OC Sthlm/Gotland (1) - Sophiahemmet (010483) - Medicinkliniken (101)",
        "parentId": 473,
        "regionId": 2,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 1068,
        "name": "Urologen",
        "shortName": null,
        "code": "361",
        "description": "Sophiahemmet",
        "fullNameWithCode": "OC Sthlm/Gotland (1) - Sophiahemmet (010483) - Urologen (361)",
        "parentId": 473,
        "regionId": 2,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 425,
        "name": "Hematologkliniken",
        "shortName": null,
        "code": "107",
        "description": "Södersjukhuset",
        "fullNameWithCode": "OC Sthlm/Gotland (1) - Södersjukhuset (010013) - Hematologkliniken (107)",
        "parentId": 117,
        "regionId": 2,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 228,
        "name": "Kirurgkliniken",
        "shortName": null,
        "code": "301",
        "description": "Södersjukhuset",
        "fullNameWithCode": "OC Sthlm/Gotland (1) - Södersjukhuset (010013) - Kirurgkliniken (301)",
        "parentId": 117,
        "regionId": 2,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 983,
        "name": "Medicinkliniken",
        "shortName": null,
        "code": "101",
        "description": "Södersjukhuset (010013)",
        "fullNameWithCode": "OC Sthlm/Gotland (1) - Södersjukhuset (010013) - Medicinkliniken (101)",
        "parentId": 117,
        "regionId": 2,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 118,
        "name": "Onkologkliniken",
        "shortName": null,
        "code": "741",
        "description": "Södersjukhuset",
        "fullNameWithCode": "OC Sthlm/Gotland (1) - Södersjukhuset (010013) - Onkologkliniken (741)",
        "parentId": 117,
        "regionId": 2,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 323,
        "name": "Kirurgkliniken",
        "shortName": null,
        "code": "301",
        "description": "Södertälje sjukhus",
        "fullNameWithCode": "OC Sthlm/Gotland (1) - Södertälje sjukhus (011011) - Kirurgkliniken (301)",
        "parentId": 322,
        "regionId": 2,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 1001,
        "name": "Medicinkliniken",
        "shortName": null,
        "code": "101",
        "description": "Södertälje sjukhus",
        "fullNameWithCode": "OC Sthlm/Gotland (1) - Södertälje sjukhus (011011) - Medicinkliniken (101)",
        "parentId": 322,
        "regionId": 2,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 1357,
        "name": "Privatläkare",
        "shortName": null,
        "code": "010",
        "description": "UroClinic Sophiahemmet (190518)",
        "fullNameWithCode": "OC Sthlm/Gotland (1) - UroClinic Sophiahemmet (190518) - Privatläkare (010)",
        "parentId": 1355,
        "regionId": 2,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 435,
        "name": "Okänd klinik",
        "shortName": null,
        "code": "999",
        "description": "VC/ Tjänsteläkare",
        "fullNameWithCode": "OC Sthlm/Gotland (1) - VC/ Tjänsteläkare (011198) - Okänd klinik (999)",
        "parentId": 384,
        "regionId": 2,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 651,
        "name": "Infektionskliniken",
        "shortName": null,
        "code": "121",
        "description": "Visby Lasarett",
        "fullNameWithCode": "OC Sthlm/Gotland (1) - Visby Lasarett (026010) - Infektionskliniken (121)",
        "parentId": 426,
        "regionId": 2,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 462,
        "name": "Kirurgkliniken",
        "shortName": null,
        "code": "301",
        "description": "Visby Lasarett",
        "fullNameWithCode": "OC Sthlm/Gotland (1) - Visby Lasarett (026010) - Kirurgkliniken (301)",
        "parentId": 426,
        "regionId": 2,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 1259,
        "name": "Onkologiska kliniken",
        "shortName": null,
        "code": "741",
        "description": "Visby lasarett (026010)",
        "fullNameWithCode": "OC Sthlm/Gotland (1) - Visby Lasarett (026010) - Onkologiska kliniken (741)",
        "parentId": 426,
        "regionId": 2,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 717,
        "name": "Urologkliniken",
        "shortName": null,
        "code": "361",
        "description": "Visby Lasarett",
        "fullNameWithCode": "OC Sthlm/Gotland (1) - Visby Lasarett (026010) - Urologkliniken (361)",
        "parentId": 426,
        "regionId": 2,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 915,
        "name": "ÖNH",
        "shortName": null,
        "code": "521",
        "description": "Visby Lasarett",
        "fullNameWithCode": "OC Sthlm/Gotland (1) - Visby Lasarett (026010) - ÖNH (521)",
        "parentId": 426,
        "regionId": 2,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 683,
        "name": "Hudkliniken",
        "shortName": null,
        "code": "211",
        "description": "Blekingesjukhuset i Karlshamn",
        "fullNameWithCode": "OC Syd (4) - Blekingesjukhuset i Karlshamn (27011) - Hudkliniken (211)",
        "parentId": 56,
        "regionId": 9,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 262,
        "name": "Kirurgiska kliniken",
        "shortName": null,
        "code": "301",
        "description": "Blekingesjukhuset i Karlshamn",
        "fullNameWithCode": "OC Syd (4) - Blekingesjukhuset i Karlshamn (27011) - Kirurgiska kliniken (301)",
        "parentId": 56,
        "regionId": 9,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 596,
        "name": "ÖNH",
        "shortName": null,
        "code": "521",
        "description": "Blekingesjukhuset i Karlshamn",
        "fullNameWithCode": "OC Syd (4) - Blekingesjukhuset i Karlshamn (27011) - ÖNH (521)",
        "parentId": 56,
        "regionId": 9,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 674,
        "name": "Hudkliniken",
        "shortName": null,
        "code": "211",
        "description": "Blekingesjukhuset i Karlskrona",
        "fullNameWithCode": "OC Syd (4) - Blekingesjukhuset i Karlskrona (27010) - Hudkliniken (211)",
        "parentId": 47,
        "regionId": 9,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 257,
        "name": "Kirurgiska kliniken",
        "shortName": null,
        "code": "301",
        "description": "Blekingesjukhuset i Karlskrona",
        "fullNameWithCode": "OC Syd (4) - Blekingesjukhuset i Karlskrona (27010) - Kirurgiska kliniken (301)",
        "parentId": 47,
        "regionId": 9,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 727,
        "name": "Kvinnoklinik",
        "shortName": null,
        "code": "451",
        "description": "Karlskrona",
        "fullNameWithCode": "OC Syd (4) - Blekingesjukhuset i Karlskrona (27010) - Kvinnoklinik (451)",
        "parentId": 47,
        "regionId": 9,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 686,
        "name": "Thorax Klin",
        "shortName": null,
        "code": "341",
        "description": "Blekingesjukhuset i Karlskrona",
        "fullNameWithCode": "OC Syd (4) - Blekingesjukhuset i Karlskrona (27010) - Thorax Klin (341)",
        "parentId": 47,
        "regionId": 9,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 587,
        "name": "ÖNH",
        "shortName": null,
        "code": "521",
        "description": "Blekingesjukhuset i Karlskrona",
        "fullNameWithCode": "OC Syd (4) - Blekingesjukhuset i Karlskrona (27010) - ÖNH (521)",
        "parentId": 47,
        "regionId": 9,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 672,
        "name": "Hudkliniken",
        "shortName": null,
        "code": "211",
        "description": "Centrallasarettet i Växjö",
        "fullNameWithCode": "OC Syd (4) - Centrallasarettet i Växjö (23010) - Hudkliniken (211)",
        "parentId": 45,
        "regionId": 9,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 254,
        "name": "Kirurgiska kliniken",
        "shortName": null,
        "code": "301",
        "description": "Centrallasarettet i Växjö",
        "fullNameWithCode": "OC Syd (4) - Centrallasarettet i Växjö (23010) - Kirurgiska kliniken (301)",
        "parentId": 45,
        "regionId": 9,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 725,
        "name": "Kvinnoklinik",
        "shortName": null,
        "code": "451",
        "description": "Växjö",
        "fullNameWithCode": "OC Syd (4) - Centrallasarettet i Växjö (23010) - Kvinnoklinik (451)",
        "parentId": 45,
        "regionId": 9,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 311,
        "name": "Onkologiska kliniken",
        "shortName": null,
        "code": "741",
        "description": "Centrallasarettet i Växjö",
        "fullNameWithCode": "OC Syd (4) - Centrallasarettet i Växjö (23010) - Onkologiska kliniken (741)",
        "parentId": 45,
        "regionId": 9,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 585,
        "name": "ÖNH",
        "shortName": null,
        "code": "521",
        "description": "Centrallasarett i Växjö",
        "fullNameWithCode": "OC Syd (4) - Centrallasarettet i Växjö (23010) - ÖNH (521)",
        "parentId": 45,
        "regionId": 9,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 675,
        "name": "Hudkliniken",
        "shortName": null,
        "code": "211",
        "description": "Centralsjukhuset i Kristianstad",
        "fullNameWithCode": "OC Syd (4) - Centralsjukhuset i Kristianstad (28010) - Hudkliniken (211)",
        "parentId": 48,
        "regionId": 9,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 251,
        "name": "Kirurgiska kliniken",
        "shortName": null,
        "code": "301",
        "description": "Centralsjukhuset i Kristianstad",
        "fullNameWithCode": "OC Syd (4) - Centralsjukhuset i Kristianstad (28010) - Kirurgiska kliniken (301)",
        "parentId": 48,
        "regionId": 9,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 728,
        "name": "Kvinnoklinik",
        "shortName": null,
        "code": "451",
        "description": "Kristianstad",
        "fullNameWithCode": "OC Syd (4) - Centralsjukhuset i Kristianstad (28010) - Kvinnoklinik (451)",
        "parentId": 48,
        "regionId": 9,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 1185,
        "name": "Onkologen",
        "shortName": null,
        "code": "741",
        "description": "Central sjukhuset i Kristianstad",
        "fullNameWithCode": "OC Syd (4) - Centralsjukhuset i Kristianstad (28010) - Onkologen (741)",
        "parentId": 48,
        "regionId": 9,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 797,
        "name": "Urologiska kliniken",
        "shortName": null,
        "code": "361",
        "description": "Centralsjukhuset i Kristianstad",
        "fullNameWithCode": "OC Syd (4) - Centralsjukhuset i Kristianstad (28010) - Urologiska kliniken (361)",
        "parentId": 48,
        "regionId": 9,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 588,
        "name": "ÖNH",
        "shortName": null,
        "code": "521",
        "description": "Centralsjukhuset i Kristianstad",
        "fullNameWithCode": "OC Syd (4) - Centralsjukhuset i Kristianstad (28010) - ÖNH (521)",
        "parentId": 48,
        "regionId": 9,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 440,
        "name": "Kliniken saknas",
        "shortName": null,
        "code": "999",
        "description": "Enhet utan INCA-inrapportör",
        "fullNameWithCode": "OC Syd (4) - Enhet utan INCA-inrapportör (49999) - Kliniken saknas (999)",
        "parentId": 439,
        "regionId": 9,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 681,
        "name": "Hudkliniken",
        "shortName": null,
        "code": "211",
        "description": "Helsingborgs lasarett",
        "fullNameWithCode": "OC Syd (4) - Helsingborgs lasarett (41012) - Hudkliniken (211)",
        "parentId": 54,
        "regionId": 9,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 260,
        "name": "Kirurgiska kliniken",
        "shortName": null,
        "code": "301",
        "description": "Helsingborgs lasarett",
        "fullNameWithCode": "OC Syd (4) - Helsingborgs lasarett (41012) - Kirurgiska kliniken (301)",
        "parentId": 54,
        "regionId": 9,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 731,
        "name": "Kvinnoklinik",
        "shortName": null,
        "code": "451",
        "description": "Helsingborg",
        "fullNameWithCode": "OC Syd (4) - Helsingborgs lasarett (41012) - Kvinnoklinik (451)",
        "parentId": 54,
        "regionId": 9,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 70,
        "name": "Medicinkliniken",
        "shortName": null,
        "code": "101",
        "description": "Helsingborgs lasarett",
        "fullNameWithCode": "OC Syd (4) - Helsingborgs lasarett (41012) - Medicinkliniken (101)",
        "parentId": 54,
        "regionId": 9,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 1186,
        "name": "Onkologen",
        "shortName": null,
        "code": "741",
        "description": "Helsinborgs lasarett",
        "fullNameWithCode": "OC Syd (4) - Helsingborgs lasarett (41012) - Onkologen (741)",
        "parentId": 54,
        "regionId": 9,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 794,
        "name": "Urologiska kliniken",
        "shortName": null,
        "code": "361",
        "description": "Helsingborgs lasarett",
        "fullNameWithCode": "OC Syd (4) - Helsingborgs lasarett (41012) - Urologiska kliniken (361)",
        "parentId": 54,
        "regionId": 9,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 313,
        "name": "Öron-Näs-Hals kliniken",
        "shortName": null,
        "code": "521",
        "description": "Helsingborgs lasarett",
        "fullNameWithCode": "OC Syd (4) - Helsingborgs lasarett (41012) - Öron-Näs-Hals kliniken (521)",
        "parentId": 54,
        "regionId": 9,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 679,
        "name": "Hudkliniken",
        "shortName": null,
        "code": "211",
        "description": "Landskrona lasarett",
        "fullNameWithCode": "OC Syd (4) - Landskrona lasarett (41010) - Hudkliniken (211)",
        "parentId": 52,
        "regionId": 9,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 259,
        "name": "Kirurgiska kliniken",
        "shortName": null,
        "code": "301",
        "description": "Landskrona lasarett",
        "fullNameWithCode": "OC Syd (4) - Landskrona lasarett (41010) - Kirurgiska kliniken (301)",
        "parentId": 52,
        "regionId": 9,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 593,
        "name": "ÖNH",
        "shortName": null,
        "code": "521",
        "description": "Landskrona lasarett",
        "fullNameWithCode": "OC Syd (4) - Landskrona lasarett (41010) - ÖNH (521)",
        "parentId": 52,
        "regionId": 9,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 673,
        "name": "Hudkliniken",
        "shortName": null,
        "code": "211",
        "description": "Ljungby lasarett",
        "fullNameWithCode": "OC Syd (4) - Ljungby lasarett (23011) - Hudkliniken (211)",
        "parentId": 46,
        "regionId": 9,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 256,
        "name": "Kirurgiska kliniken",
        "shortName": null,
        "code": "301",
        "description": "Ljungby lasarett",
        "fullNameWithCode": "OC Syd (4) - Ljungby lasarett (23011) - Kirurgiska kliniken (301)",
        "parentId": 46,
        "regionId": 9,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 586,
        "name": "ÖNH",
        "shortName": null,
        "code": "521",
        "description": "Ljungby lasarett",
        "fullNameWithCode": "OC Syd (4) - Ljungby lasarett (23011) - ÖNH (521)",
        "parentId": 46,
        "regionId": 9,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 671,
        "name": "Hudkliniken",
        "shortName": null,
        "code": "211",
        "description": "Länssjukhuset i Halmstad",
        "fullNameWithCode": "OC Syd (4) - Länssjukhuset i Halmstad (42010) - Hudkliniken (211)",
        "parentId": 44,
        "regionId": 9,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 250,
        "name": "Kirurgiska kliniken",
        "shortName": null,
        "code": "301",
        "description": "Länssjukhuset i Halmstad",
        "fullNameWithCode": "OC Syd (4) - Länssjukhuset i Halmstad (42010) - Kirurgiska kliniken (301)",
        "parentId": 44,
        "regionId": 9,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 695,
        "name": "Lungmedicin",
        "shortName": null,
        "code": "111",
        "description": "Länssjukhuset i Halmstad",
        "fullNameWithCode": "OC Syd (4) - Länssjukhuset i Halmstad (42010) - Lungmedicin (111)",
        "parentId": 44,
        "regionId": 9,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 68,
        "name": "Medicinkliniken",
        "shortName": null,
        "code": "101",
        "description": "Länssjukhuset i Halmstad",
        "fullNameWithCode": "OC Syd (4) - Länssjukhuset i Halmstad (42010) - Medicinkliniken (101)",
        "parentId": 44,
        "regionId": 9,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 583,
        "name": "ÖNH",
        "shortName": null,
        "code": "521",
        "description": "Länssjukhuset i Halmstad",
        "fullNameWithCode": "OC Syd (4) - Länssjukhuset i Halmstad (42010) - ÖNH (521)",
        "parentId": 44,
        "regionId": 9,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 922,
        "name": "Patologavdelningen, Lund",
        "shortName": null,
        "code": "411",
        "description": "Södra regionen.",
        "fullNameWithCode": "OC Syd (4) - Patologavdelningen, Lund (411)",
        "parentId": 3,
        "regionId": 9,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 499,
        "name": "Kliniken saknas",
        "shortName": null,
        "code": "999",
        "description": "Privatläkare Blekinge län",
        "fullNameWithCode": "OC Syd (4) - Privatläkare Blekinge län (27199) - Kliniken saknas (999)",
        "parentId": 491,
        "regionId": 9,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 500,
        "name": "Urologen i Carlshamns specialistklinik AB",
        "shortName": null,
        "code": "998",
        "description": "Privatläkare Blekinge län",
        "fullNameWithCode": "OC Syd (4) - Privatläkare Blekinge län (27199) - Urologen i Carlshamns specialistklinik AB (998)",
        "parentId": 491,
        "regionId": 9,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 497,
        "name": "Kliniken saknas",
        "shortName": null,
        "code": "999",
        "description": "Privatläkare gamla Kristianstads län",
        "fullNameWithCode": "OC Syd (4) - Privatläkare gamla Kristianstads län (28199) - Kliniken saknas (999)",
        "parentId": 492,
        "regionId": 9,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 498,
        "name": "Urologen i Kristianstadskliniken",
        "shortName": null,
        "code": "998",
        "description": "Privatläkare gamla Kristianstads län",
        "fullNameWithCode": "OC Syd (4) - Privatläkare gamla Kristianstads län (28199) - Urologen i Kristianstadskliniken (998)",
        "parentId": 492,
        "regionId": 9,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 460,
        "name": "Capio Cityklinken",
        "shortName": null,
        "code": "998",
        "description": "Privatläkare gamla Malmöhus län",
        "fullNameWithCode": "OC Syd (4) - Privatläkare gamla Malmöhus län (41199) - Capio Cityklinken (998)",
        "parentId": 307,
        "regionId": 9,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 1214,
        "name": "Capio, Eslöv",
        "shortName": null,
        "code": "989",
        "description": "Carema, Eslöv byte namn. Fiktiv inrapportör fortfarande.",
        "fullNameWithCode": "OC Syd (4) - Privatläkare gamla Malmöhus län (41199) - Capio, Eslöv (989)",
        "parentId": 307,
        "regionId": 9,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 483,
        "name": "Carema, Eslöv",
        "shortName": null,
        "code": "994",
        "description": "Privatläkare gamla Malmöhus län",
        "fullNameWithCode": "OC Syd (4) - Privatläkare gamla Malmöhus län (41199) - Carema, Eslöv (994)",
        "parentId": 307,
        "regionId": 9,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 521,
        "name": "Carema, Lund",
        "shortName": null,
        "code": "991",
        "description": "Privatläkare gamla Malmöhus län",
        "fullNameWithCode": "OC Syd (4) - Privatläkare gamla Malmöhus län (41199) - Carema, Lund (991)",
        "parentId": 307,
        "regionId": 9,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 1202,
        "name": "Gastro Center Skåne, Lund",
        "shortName": null,
        "code": "990",
        "description": "Privat klinik under gamla Malmöhuslän",
        "fullNameWithCode": "OC Syd (4) - Privatläkare gamla Malmöhus län (41199) - Gastro Center Skåne, Lund (990)",
        "parentId": 307,
        "regionId": 9,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 438,
        "name": "Kliniken saknas",
        "shortName": null,
        "code": "999",
        "description": "Privatläkare gamla Malmöhus län",
        "fullNameWithCode": "OC Syd (4) - Privatläkare gamla Malmöhus län (41199) - Kliniken saknas (999)",
        "parentId": 307,
        "regionId": 9,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 481,
        "name": "Läkargruppen i Vellinge",
        "shortName": null,
        "code": "996",
        "description": "Privatläkare gamla Malmöhus län",
        "fullNameWithCode": "OC Syd (4) - Privatläkare gamla Malmöhus län (41199) - Läkargruppen i Vellinge (996)",
        "parentId": 307,
        "regionId": 9,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 482,
        "name": "Specialistläkarmottagningen i Lund",
        "shortName": null,
        "code": "995",
        "description": "Privatläkare gamla Malmöhus län",
        "fullNameWithCode": "OC Syd (4) - Privatläkare gamla Malmöhus län (41199) - Specialistläkarmottagningen i Lund (995)",
        "parentId": 307,
        "regionId": 9,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 484,
        "name": "Urologen i Kristianstadskliniken",
        "shortName": null,
        "code": "993",
        "description": "Privatläkare gamla Malmöhus län",
        "fullNameWithCode": "OC Syd (4) - Privatläkare gamla Malmöhus län (41199) - Urologen i Kristianstadskliniken (993)",
        "parentId": 307,
        "regionId": 9,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 1125,
        "name": "Wullt läkarmottagning",
        "shortName": null,
        "code": "988",
        "description": "Privat läkarmottagning",
        "fullNameWithCode": "OC Syd (4) - Privatläkare gamla Malmöhus län (41199) - Wullt läkarmottagning (988)",
        "parentId": 307,
        "regionId": 9,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 502,
        "name": "Kliniken saknas",
        "shortName": null,
        "code": "999",
        "description": "Privatläkare Hallands län",
        "fullNameWithCode": "OC Syd (4) - Privatläkare Hallands län (42199) - Kliniken saknas (999)",
        "parentId": 494,
        "regionId": 9,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 924,
        "name": "Specialist mottagning i urologi och gyn",
        "shortName": null,
        "code": "998",
        "description": "Tudor kliniken, Skansgatan 1C 302 46 Halmstad",
        "fullNameWithCode": "OC Syd (4) - Privatläkare Hallands län (42199) - Specialist mottagning i urologi och gyn (998)",
        "parentId": 494,
        "regionId": 9,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 1179,
        "name": "Gränsbygdsklinken AB",
        "shortName": null,
        "code": "998",
        "description": "Privat läkare i Kronoberg, Markaryd för prostata och blåsa",
        "fullNameWithCode": "OC Syd (4) - Privatläkare Kronobergs län (23199) - Gränsbygdsklinken AB (998)",
        "parentId": 490,
        "regionId": 9,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 501,
        "name": "Kliniken saknas",
        "shortName": null,
        "code": "999",
        "description": "Privatläkare Kronobergs län",
        "fullNameWithCode": "OC Syd (4) - Privatläkare Kronobergs län (23199) - Kliniken saknas (999)",
        "parentId": 490,
        "regionId": 9,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 495,
        "name": "Kliniken saknas",
        "shortName": null,
        "code": "999",
        "description": "Privatläkare Malmö stad",
        "fullNameWithCode": "OC Syd (4) - Privatläkare Malmö stad (30199) - Kliniken saknas (999)",
        "parentId": 493,
        "regionId": 9,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 496,
        "name": "Läkarhuset Ellenbogen",
        "shortName": null,
        "code": "998",
        "description": "Privatläkare Malmö stad",
        "fullNameWithCode": "OC Syd (4) - Privatläkare Malmö stad (30199) - Läkarhuset Ellenbogen (998)",
        "parentId": 493,
        "regionId": 9,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 530,
        "name": "Slottsstadens läkarhus",
        "shortName": null,
        "code": "997",
        "description": "Privatläkare Malmö stad",
        "fullNameWithCode": "OC Syd (4) - Privatläkare Malmö stad (30199) - Slottsstadens läkarhus (997)",
        "parentId": 493,
        "regionId": 9,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 677,
        "name": "Hudkliniken",
        "shortName": null,
        "code": "211",
        "description": "Simrishamns sjukhus",
        "fullNameWithCode": "OC Syd (4) - Simrishamns sjukhus (28013) - Hudkliniken (211)",
        "parentId": 50,
        "regionId": 9,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 316,
        "name": "Kirurgiska kliniken",
        "shortName": null,
        "code": "301",
        "description": "Simrishamns sjukhus",
        "fullNameWithCode": "OC Syd (4) - Simrishamns sjukhus (28013) - Kirurgiska kliniken (301)",
        "parentId": 50,
        "regionId": 9,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 592,
        "name": "ÖNH",
        "shortName": null,
        "code": "521",
        "description": "Simrishamns sjukhus",
        "fullNameWithCode": "OC Syd (4) - Simrishamns sjukhus (28013) - ÖNH (521)",
        "parentId": 50,
        "regionId": 9,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 684,
        "name": "Hudkliniken",
        "shortName": null,
        "code": "211",
        "description": "Sjukhuset i Hässleholm",
        "fullNameWithCode": "OC Syd (4) - Sjukhuset i Hässleholm (28012) - Hudkliniken (211)",
        "parentId": 57,
        "regionId": 9,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 314,
        "name": "Kirurgiska kliniken",
        "shortName": null,
        "code": "301",
        "description": "Sjukhuset i Hässleholm",
        "fullNameWithCode": "OC Syd (4) - Sjukhuset i Hässleholm (28012) - Kirurgiska kliniken (301)",
        "parentId": 57,
        "regionId": 9,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 598,
        "name": "ÖNH",
        "shortName": null,
        "code": "521",
        "description": "Sjukhuset i Hässleholm",
        "fullNameWithCode": "OC Syd (4) - Sjukhuset i Hässleholm (28012) - ÖNH (521)",
        "parentId": 57,
        "regionId": 9,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 1025,
        "name": "Gastro Center Skåne",
        "shortName": null,
        "code": "104",
        "description": "Avdelning i den privata kliniken Specialist Center Skåne",
        "fullNameWithCode": "OC Syd (4) - Specialist Center Skåne (42299) - Gastro Center Skåne (104)",
        "parentId": 1024,
        "regionId": 9,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 680,
        "name": "Hudkliniken",
        "shortName": null,
        "code": "211",
        "description": "Trelleborgs lasarett",
        "fullNameWithCode": "OC Syd (4) - Trelleborgs lasarett (41011) - Hudkliniken (211)",
        "parentId": 53,
        "regionId": 9,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 317,
        "name": "Kirurgiska kliniken",
        "shortName": null,
        "code": "301",
        "description": "Trelleborgs lasarett",
        "fullNameWithCode": "OC Syd (4) - Trelleborgs lasarett (41011) - Kirurgiska kliniken (301)",
        "parentId": 53,
        "regionId": 9,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 793,
        "name": "Urologiska kliniken",
        "shortName": null,
        "code": "361",
        "description": "Trelleborgs lasarett",
        "fullNameWithCode": "OC Syd (4) - Trelleborgs lasarett (41011) - Urologiska kliniken (361)",
        "parentId": 53,
        "regionId": 9,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 594,
        "name": "ÖNH",
        "shortName": null,
        "code": "521",
        "description": "Trelleborgs lasarett",
        "fullNameWithCode": "OC Syd (4) - Trelleborgs lasarett (41011) - ÖNH (521)",
        "parentId": 53,
        "regionId": 9,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 1177,
        "name": "endokrinologisk klinik",
        "shortName": null,
        "code": "103",
        "description": "endokrinologisk klinik Lund",
        "fullNameWithCode": "OC Syd (4) - Universitetssjukhuset i Lund (41001) - endokrinologisk klinik (103)",
        "parentId": 43,
        "regionId": 9,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 456,
        "name": "Gastroenterologi",
        "shortName": null,
        "code": "104",
        "description": "Universitetssjukhuset i Lund",
        "fullNameWithCode": "OC Syd (4) - Universitetssjukhuset i Lund (41001) - Gastroenterologi (104)",
        "parentId": 43,
        "regionId": 9,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 59,
        "name": "Hematologiska kliniken",
        "shortName": null,
        "code": "105",
        "description": "Universitetssjukhuset i Lund",
        "fullNameWithCode": "OC Syd (4) - Universitetssjukhuset i Lund (41001) - Hematologiska kliniken (105)",
        "parentId": 43,
        "regionId": 9,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 670,
        "name": "Hudkliniken",
        "shortName": null,
        "code": "211",
        "description": "Universitetssjukhuset i Lund",
        "fullNameWithCode": "OC Syd (4) - Universitetssjukhuset i Lund (41001) - Hudkliniken (211)",
        "parentId": 43,
        "regionId": 9,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 132,
        "name": "Kirurgiska kliniken",
        "shortName": null,
        "code": "301",
        "description": "Universitetssjukhuset i Lund",
        "fullNameWithCode": "OC Syd (4) - Universitetssjukhuset i Lund (41001) - Kirurgiska kliniken (301)",
        "parentId": 43,
        "regionId": 9,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 702,
        "name": "Kvinnoklinik",
        "shortName": null,
        "code": "451",
        "description": null,
        "fullNameWithCode": "OC Syd (4) - Universitetssjukhuset i Lund (41001) - Kvinnoklinik (451)",
        "parentId": 43,
        "regionId": 9,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 685,
        "name": "Lung Klin",
        "shortName": null,
        "code": "107",
        "description": "Universitetssjukhuset i Lund",
        "fullNameWithCode": "OC Syd (4) - Universitetssjukhuset i Lund (41001) - Lung Klin (107)",
        "parentId": 43,
        "regionId": 9,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 669,
        "name": "Neurokirurgklin",
        "shortName": null,
        "code": "331",
        "description": "Univerisitetssjukhuset i Lund",
        "fullNameWithCode": "OC Syd (4) - Universitetssjukhuset i Lund (41001) - Neurokirurgklin (331)",
        "parentId": 43,
        "regionId": 9,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 65,
        "name": "Onkologiska kliniken",
        "shortName": null,
        "code": "741",
        "description": "Universitetssjukhuset i Lund",
        "fullNameWithCode": "OC Syd (4) - Universitetssjukhuset i Lund (41001) - Onkologiska kliniken (741)",
        "parentId": 43,
        "regionId": 9,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 221,
        "name": "Urologiska kliniken",
        "shortName": null,
        "code": "361",
        "description": "Universitetssjukhuset i Lund",
        "fullNameWithCode": "OC Syd (4) - Universitetssjukhuset i Lund (41001) - Urologiska kliniken (361)",
        "parentId": 43,
        "regionId": 9,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 581,
        "name": "ÖNH",
        "shortName": null,
        "code": "521",
        "description": "Universitetssjukhuset i Lund",
        "fullNameWithCode": "OC Syd (4) - Universitetssjukhuset i Lund (41001) - ÖNH (521)",
        "parentId": 43,
        "regionId": 9,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 1178,
        "name": "endokrinologisk klinik",
        "shortName": null,
        "code": "161",
        "description": "endokinologen MAS",
        "fullNameWithCode": "OC Syd (4) - Universitetssjukhuset MAS (30001) - endokrinologisk klinik (161)",
        "parentId": 51,
        "regionId": 9,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 66,
        "name": "Hematologen",
        "shortName": null,
        "code": "105",
        "description": "Universitetssjukhuset MAS",
        "fullNameWithCode": "OC Syd (4) - Universitetssjukhuset MAS (30001) - Hematologen (105)",
        "parentId": 51,
        "regionId": 9,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 678,
        "name": "Hudkliniken",
        "shortName": null,
        "code": "211",
        "description": "Universitetssjukhuset MAS",
        "fullNameWithCode": "OC Syd (4) - Universitetssjukhuset MAS (30001) - Hudkliniken (211)",
        "parentId": 51,
        "regionId": 9,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 258,
        "name": "Kirurgiska kliniken",
        "shortName": null,
        "code": "301",
        "description": "Universitetssjukhuset MAS",
        "fullNameWithCode": "OC Syd (4) - Universitetssjukhuset MAS (30001) - Kirurgiska kliniken (301)",
        "parentId": 51,
        "regionId": 9,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 730,
        "name": "Kvinnoklinik",
        "shortName": null,
        "code": "451",
        "description": "Malmö",
        "fullNameWithCode": "OC Syd (4) - Universitetssjukhuset MAS (30001) - Kvinnoklinik (451)",
        "parentId": 51,
        "regionId": 9,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 687,
        "name": "Lung Klin",
        "shortName": null,
        "code": "111",
        "description": "Universitetssjukhuset MAS",
        "fullNameWithCode": "OC Syd (4) - Universitetssjukhuset MAS (30001) - Lung Klin (111)",
        "parentId": 51,
        "regionId": 9,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 319,
        "name": "Onkologiska kliniken",
        "shortName": null,
        "code": "741",
        "description": "Universitetssjukhuset MAS",
        "fullNameWithCode": "OC Syd (4) - Universitetssjukhuset MAS (30001) - Onkologiska kliniken (741)",
        "parentId": 51,
        "regionId": 9,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 723,
        "name": "Plastikkirurgi",
        "shortName": null,
        "code": "351",
        "description": "Universitetssjukhuset MAS",
        "fullNameWithCode": "OC Syd (4) - Universitetssjukhuset MAS (30001) - Plastikkirurgi (351)",
        "parentId": 51,
        "regionId": 9,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 222,
        "name": "Urologiska kliniken",
        "shortName": null,
        "code": "361",
        "description": "Universitetssjukhuset MAS",
        "fullNameWithCode": "OC Syd (4) - Universitetssjukhuset MAS (30001) - Urologiska kliniken (361)",
        "parentId": 51,
        "regionId": 9,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 312,
        "name": "Öron-Näs-Hals kliniken",
        "shortName": null,
        "code": "521",
        "description": "Universitetssjukhuset MAS",
        "fullNameWithCode": "OC Syd (4) - Universitetssjukhuset MAS (30001) - Öron-Näs-Hals kliniken (521)",
        "parentId": 51,
        "regionId": 9,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 437,
        "name": "Kliniken saknas",
        "shortName": null,
        "code": "999",
        "description": "Vårdcentral i Södra regionen",
        "fullNameWithCode": "OC Syd (4) - Vårdcentral i Södra regionen (40000) - Kliniken saknas (999)",
        "parentId": 305,
        "regionId": 9,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 682,
        "name": "Hudkliniken",
        "shortName": null,
        "code": "211",
        "description": "Ystads lasarett",
        "fullNameWithCode": "OC Syd (4) - Ystads lasarett (41013) - Hudkliniken (211)",
        "parentId": 55,
        "regionId": 9,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 261,
        "name": "Kirurgiska kliniken",
        "shortName": null,
        "code": "301",
        "description": "Ystads lasarett",
        "fullNameWithCode": "OC Syd (4) - Ystads lasarett (41013) - Kirurgiska kliniken (301)",
        "parentId": 55,
        "regionId": 9,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 795,
        "name": "Urologiska kliniken",
        "shortName": null,
        "code": "361",
        "description": "Ystads lasarett",
        "fullNameWithCode": "OC Syd (4) - Ystads lasarett (41013) - Urologiska kliniken (361)",
        "parentId": 55,
        "regionId": 9,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 595,
        "name": "ÖNH",
        "shortName": null,
        "code": "521",
        "description": "Ystads lasarett",
        "fullNameWithCode": "OC Syd (4) - Ystads lasarett (41013) - ÖNH (521)",
        "parentId": 55,
        "regionId": 9,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 676,
        "name": "Hudkliniken",
        "shortName": null,
        "code": "211",
        "description": "Ängelholms sjukhus",
        "fullNameWithCode": "OC Syd (4) - Ängelholms sjukhus (28011) - Hudkliniken (211)",
        "parentId": 49,
        "regionId": 9,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 318,
        "name": "Kirurgiska kliniken",
        "shortName": null,
        "code": "301",
        "description": "Ängelholms sjukhus",
        "fullNameWithCode": "OC Syd (4) - Ängelholms sjukhus (28011) - Kirurgiska kliniken (301)",
        "parentId": 49,
        "regionId": 9,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 591,
        "name": "ÖNH",
        "shortName": null,
        "code": "521",
        "description": "Ängelholms sjukhus",
        "fullNameWithCode": "OC Syd (4) - Ängelholms sjukhus (28011) - ÖNH (521)",
        "parentId": 49,
        "regionId": 9,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 395,
        "name": "Kirurgkliniken",
        "shortName": null,
        "code": "301",
        "description": "Eksjö",
        "fullNameWithCode": "OC Sydost (3) - Eksjö (22011) - Kirurgkliniken (301)",
        "parentId": 231,
        "regionId": 5,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 241,
        "name": "Urologiska kliniken",
        "shortName": null,
        "code": "361",
        "description": "Eksjö",
        "fullNameWithCode": "OC Sydost (3) - Eksjö (22011) - Urologiska kliniken (361)",
        "parentId": 231,
        "regionId": 5,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 452,
        "name": "Klinik",
        "shortName": null,
        "code": "999",
        "description": "Enhet utan INCA-rapportör",
        "fullNameWithCode": "OC Sydost (3) - Enhet utan INCA-rapportör (39999) - Klinik (999)",
        "parentId": 451,
        "regionId": 5,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 393,
        "name": "Kirurgkliniken",
        "shortName": null,
        "code": "301",
        "description": "Jönköping",
        "fullNameWithCode": "OC Sydost (3) - Jönköping (22010) - Kirurgkliniken (301)",
        "parentId": 230,
        "regionId": 5,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 394,
        "name": "Medicinkliniken",
        "shortName": null,
        "code": "101",
        "description": "Jönköping",
        "fullNameWithCode": "OC Sydost (3) - Jönköping (22010) - Medicinkliniken (101)",
        "parentId": 230,
        "regionId": 5,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 392,
        "name": "Onkologen",
        "shortName": null,
        "code": "741",
        "description": "Jönköping",
        "fullNameWithCode": "OC Sydost (3) - Jönköping (22010) - Onkologen (741)",
        "parentId": 230,
        "regionId": 5,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 240,
        "name": "Urologiska kliniken",
        "shortName": null,
        "code": "361",
        "description": "Jönköping",
        "fullNameWithCode": "OC Sydost (3) - Jönköping (22010) - Urologiska kliniken (361)",
        "parentId": 230,
        "regionId": 5,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 397,
        "name": "Kirurgkliniken",
        "shortName": null,
        "code": "301",
        "description": "Kalmar",
        "fullNameWithCode": "OC Sydost (3) - Kalmar (25010) - Kirurgkliniken (301)",
        "parentId": 232,
        "regionId": 5,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 1213,
        "name": "Onkologen",
        "shortName": null,
        "code": "741",
        "description": "Kalmar",
        "fullNameWithCode": "OC Sydost (3) - Kalmar (25010) - Onkologen (741)",
        "parentId": 232,
        "regionId": 5,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 242,
        "name": "Urologiska kliniken",
        "shortName": null,
        "code": "361",
        "description": "Kalmar",
        "fullNameWithCode": "OC Sydost (3) - Kalmar (25010) - Urologiska kliniken (361)",
        "parentId": 232,
        "regionId": 5,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 303,
        "name": "Endokrinkirurgiska kliniken",
        "shortName": null,
        "code": "161",
        "description": "Linköping US",
        "fullNameWithCode": "OC Sydost (3) - Linköping US (21001) - Endokrinkirurgiska kliniken (161)",
        "parentId": 61,
        "regionId": 5,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 522,
        "name": "Hematologiska kliniken",
        "shortName": null,
        "code": "666",
        "description": "Linköping US",
        "fullNameWithCode": "OC Sydost (3) - Linköping US (21001) - Hematologiska kliniken (666)",
        "parentId": 61,
        "regionId": 5,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 761,
        "name": "Hudkliniken",
        "shortName": null,
        "code": "211",
        "description": "Universitetssjukhuset i Linköping (21001)",
        "fullNameWithCode": "OC Sydost (3) - Linköping US (21001) - Hudkliniken (211)",
        "parentId": 61,
        "regionId": 5,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 64,
        "name": "Kirurgkliniken",
        "shortName": null,
        "code": "301",
        "description": "Linköping US",
        "fullNameWithCode": "OC Sydost (3) - Linköping US (21001) - Kirurgkliniken (301)",
        "parentId": 61,
        "regionId": 5,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 391,
        "name": "Kolorektalkirurgi",
        "shortName": null,
        "code": "667",
        "description": "Linköping US",
        "fullNameWithCode": "OC Sydost (3) - Linköping US (21001) - Kolorektalkirurgi (667)",
        "parentId": 61,
        "regionId": 5,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 751,
        "name": "Kvinnokliniken",
        "shortName": null,
        "code": "451",
        "description": "Linköping US (21001)",
        "fullNameWithCode": "OC Sydost (3) - Linköping US (21001) - Kvinnokliniken (451)",
        "parentId": 61,
        "regionId": 5,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 622,
        "name": "Lungmedicinska",
        "shortName": null,
        "code": "111",
        "description": "Linköping US",
        "fullNameWithCode": "OC Sydost (3) - Linköping US (21001) - Lungmedicinska (111)",
        "parentId": 61,
        "regionId": 5,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 389,
        "name": "Onkologen",
        "shortName": null,
        "code": "741",
        "description": "Linköping US",
        "fullNameWithCode": "OC Sydost (3) - Linköping US (21001) - Onkologen (741)",
        "parentId": 61,
        "regionId": 5,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 1127,
        "name": "RegCNS",
        "shortName": null,
        "code": "000",
        "description": "Universitetssjukhuset i Linköping (21001)",
        "fullNameWithCode": "OC Sydost (3) - Linköping US (21001) - RegCNS (000)",
        "parentId": 61,
        "regionId": 5,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 302,
        "name": "Sektionen för övre abdominell kirurgi",
        "shortName": null,
        "code": "665",
        "description": "Linköping US",
        "fullNameWithCode": "OC Sydost (3) - Linköping US (21001) - Sektionen för övre abdominell kirurgi (665)",
        "parentId": 61,
        "regionId": 5,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 441,
        "name": "Urologkliniken",
        "shortName": null,
        "code": "361",
        "description": "Linköping US",
        "fullNameWithCode": "OC Sydost (3) - Linköping US (21001) - Urologkliniken (361)",
        "parentId": 61,
        "regionId": 5,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 568,
        "name": "ÖNH",
        "shortName": null,
        "code": "521",
        "description": "Linköping",
        "fullNameWithCode": "OC Sydost (3) - Linköping US (21001) - ÖNH (521)",
        "parentId": 61,
        "regionId": 5,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 960,
        "name": "Proxima kirurgi",
        "shortName": null,
        "code": "309",
        "description": "Motala (21014)",
        "fullNameWithCode": "OC Sydost (3) - Motala (21014) - Proxima kirurgi (309)",
        "parentId": 387,
        "regionId": 5,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 442,
        "name": "Urologkliniken",
        "shortName": null,
        "code": "361",
        "description": "Norrköping ViN",
        "fullNameWithCode": "OC Sydost (3) - Norrköping ViN (21013) - Urologkliniken (361)",
        "parentId": 386,
        "regionId": 5,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 399,
        "name": "Kirurgkliniken",
        "shortName": null,
        "code": "301",
        "description": "Oskarshamn",
        "fullNameWithCode": "OC Sydost (3) - Oskarshamn (25011) - Kirurgkliniken (301)",
        "parentId": 234,
        "regionId": 5,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 244,
        "name": "Urologiska kliniken",
        "shortName": null,
        "code": "361",
        "description": "Oskarshamn",
        "fullNameWithCode": "OC Sydost (3) - Oskarshamn (25011) - Urologiska kliniken (361)",
        "parentId": 234,
        "regionId": 5,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 450,
        "name": "Klinik",
        "shortName": null,
        "code": "999",
        "description": "Privatläkare",
        "fullNameWithCode": "OC Sydost (3) - Privatläkare (30199) - Klinik (999)",
        "parentId": 449,
        "regionId": 5,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 489,
        "name": "Kirurgkliniken",
        "shortName": null,
        "code": "301",
        "description": "Sergelkliniken",
        "fullNameWithCode": "OC Sydost (3) - Sergelkliniken (211992) - Kirurgkliniken (301)",
        "parentId": 488,
        "regionId": 5,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 448,
        "name": "Klinik",
        "shortName": null,
        "code": "999",
        "description": "Vårdcentral",
        "fullNameWithCode": "OC Sydost (3) - Vårdcentral (30000) - Klinik (999)",
        "parentId": 447,
        "regionId": 5,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 400,
        "name": "Kirurgkliniken",
        "shortName": null,
        "code": "301",
        "description": "Värnamo",
        "fullNameWithCode": "OC Sydost (3) - Värnamo (22012) - Kirurgkliniken (301)",
        "parentId": 236,
        "regionId": 5,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 246,
        "name": "Urologiska kliniken",
        "shortName": null,
        "code": "361",
        "description": "Värnamo",
        "fullNameWithCode": "OC Sydost (3) - Värnamo (22012) - Urologiska kliniken (361)",
        "parentId": 236,
        "regionId": 5,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 402,
        "name": "Kirurgkliniken",
        "shortName": null,
        "code": "301",
        "description": "Västervik",
        "fullNameWithCode": "OC Sydost (3) - Västervik (24010) - Kirurgkliniken (301)",
        "parentId": 237,
        "regionId": 5,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 247,
        "name": "Urologiska kliniken",
        "shortName": null,
        "code": "361",
        "description": "Västervik",
        "fullNameWithCode": "OC Sydost (3) - Västervik (24010) - Urologiska kliniken (361)",
        "parentId": 237,
        "regionId": 5,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 267,
        "name": "Kir klin",
        "shortName": null,
        "code": "301",
        "description": "Arvika sjh",
        "fullNameWithCode": "OC Uppsala/Örebro (2) - Arvika sjh (54012) - Kir klin (301)",
        "parentId": 266,
        "regionId": 3,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 281,
        "name": "Kir klin",
        "shortName": null,
        "code": "301",
        "description": "Enköping las",
        "fullNameWithCode": "OC Uppsala/Örebro (2) - Enköping las (12010) - Kir klin (301)",
        "parentId": 280,
        "regionId": 3,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 487,
        "name": "Med klin",
        "shortName": null,
        "code": "101",
        "description": "Enköping las",
        "fullNameWithCode": "OC Uppsala/Örebro (2) - Enköping las (12010) - Med klin (101)",
        "parentId": 280,
        "regionId": 3,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 215,
        "name": "Kir klin",
        "shortName": null,
        "code": "301",
        "description": "Eskilstuna M.sjh",
        "fullNameWithCode": "OC Uppsala/Örebro (2) - Eskilstuna M.sjh (13010) - Kir klin (301)",
        "parentId": 214,
        "regionId": 3,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 378,
        "name": "Onk klin",
        "shortName": null,
        "code": "741",
        "description": "Eskilstuna M.sjh",
        "fullNameWithCode": "OC Uppsala/Örebro (2) - Eskilstuna M.sjh (13010) - Onk klin (741)",
        "parentId": 214,
        "regionId": 3,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 178,
        "name": "Kir klin",
        "shortName": null,
        "code": "301",
        "description": "Falun F.las",
        "fullNameWithCode": "OC Uppsala/Örebro (2) - Falun F.las (57010) - Kir klin (301)",
        "parentId": 177,
        "regionId": 3,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 275,
        "name": "Kir klin",
        "shortName": null,
        "code": "301",
        "description": "Gävle sjh",
        "fullNameWithCode": "OC Uppsala/Örebro (2) - Gävle sjh (61010) - Kir klin (301)",
        "parentId": 274,
        "regionId": 3,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 379,
        "name": "Onk klin",
        "shortName": null,
        "code": "741",
        "description": "Gävle sjh",
        "fullNameWithCode": "OC Uppsala/Örebro (2) - Gävle sjh (61010) - Onk klin (741)",
        "parentId": 274,
        "regionId": 3,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 603,
        "name": "ÖNH",
        "shortName": null,
        "code": "521",
        "description": "Gävle sjh",
        "fullNameWithCode": "OC Uppsala/Örebro (2) - Gävle sjh (61010) - ÖNH (521)",
        "parentId": 274,
        "regionId": 3,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 212,
        "name": "Kir klin",
        "shortName": null,
        "code": "301",
        "description": "Hudiksvalls sjh",
        "fullNameWithCode": "OC Uppsala/Örebro (2) - Hudiksvalls sjh (61012) - Kir klin (301)",
        "parentId": 211,
        "regionId": 3,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 277,
        "name": "Kir klin",
        "shortName": null,
        "code": "301",
        "description": "Karlskoga las",
        "fullNameWithCode": "OC Uppsala/Örebro (2) - Karlskoga las (55011) - Kir klin (301)",
        "parentId": 270,
        "regionId": 3,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 127,
        "name": "Kir klin",
        "shortName": null,
        "code": "301",
        "description": "Karlstad C.sjh",
        "fullNameWithCode": "OC Uppsala/Örebro (2) - Karlstad C.sjh (54010) - Kir klin (301)",
        "parentId": 126,
        "regionId": 3,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 372,
        "name": "Med klin",
        "shortName": null,
        "code": "101",
        "description": "Karlstad C.sjh",
        "fullNameWithCode": "OC Uppsala/Örebro (2) - Karlstad C.sjh (54010) - Med klin (101)",
        "parentId": 126,
        "regionId": 3,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 128,
        "name": "Onk klin",
        "shortName": null,
        "code": "741",
        "description": "Karlstad C.sjh",
        "fullNameWithCode": "OC Uppsala/Örebro (2) - Karlstad C.sjh (54010) - Onk klin (741)",
        "parentId": 126,
        "regionId": 3,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 284,
        "name": "Kir klin",
        "shortName": null,
        "code": "301",
        "description": "Katrineholm Kul sjh",
        "fullNameWithCode": "OC Uppsala/Örebro (2) - Katrineholm Kul sjh (13012) - Kir klin (301)",
        "parentId": 283,
        "regionId": 3,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 279,
        "name": "Kir klin",
        "shortName": null,
        "code": "301",
        "description": "Lindesbergs las",
        "fullNameWithCode": "OC Uppsala/Örebro (2) - Lindesbergs las (55012) - Kir klin (301)",
        "parentId": 278,
        "regionId": 3,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 220,
        "name": "Kir klin",
        "shortName": null,
        "code": "301",
        "description": "Mora Las",
        "fullNameWithCode": "OC Uppsala/Örebro (2) - Mora Las (57011) - Kir klin (301)",
        "parentId": 219,
        "regionId": 3,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 276,
        "name": "Kir klin",
        "shortName": null,
        "code": "301",
        "description": "Nyköping las",
        "fullNameWithCode": "OC Uppsala/Örebro (2) - Nyköping las (13011) - Kir klin (301)",
        "parentId": 62,
        "regionId": 3,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 264,
        "name": "Kir klin",
        "shortName": null,
        "code": "301",
        "description": "Torsby sjh",
        "fullNameWithCode": "OC Uppsala/Örebro (2) - Torsby sjh (54014) - Kir klin (301)",
        "parentId": 263,
        "regionId": 3,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 366,
        "name": "Med klin",
        "shortName": null,
        "code": "101",
        "description": "Torsby sjh",
        "fullNameWithCode": "OC Uppsala/Örebro (2) - Torsby sjh (54014) - Med klin (101)",
        "parentId": 263,
        "regionId": 3,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 1187,
        "name": "Endokrin",
        "shortName": null,
        "code": "161",
        "description": "Akademiska sjukhuset",
        "fullNameWithCode": "OC Uppsala/Örebro (2) - UAS (12001) - Endokrin (161)",
        "parentId": 9,
        "regionId": 3,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 867,
        "name": "Hudkliniken",
        "shortName": null,
        "code": "211",
        "description": "UAS",
        "fullNameWithCode": "OC Uppsala/Örebro (2) - UAS (12001) - Hudkliniken (211)",
        "parentId": 9,
        "regionId": 3,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 218,
        "name": "Kir klin",
        "shortName": null,
        "code": "301",
        "description": "UAS",
        "fullNameWithCode": "OC Uppsala/Örebro (2) - UAS (12001) - Kir klin (301)",
        "parentId": 9,
        "regionId": 3,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 367,
        "name": "Med klin",
        "shortName": null,
        "code": "101",
        "description": "UAS",
        "fullNameWithCode": "OC Uppsala/Örebro (2) - UAS (12001) - Med klin (101)",
        "parentId": 9,
        "regionId": 3,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 421,
        "name": "Onk klin",
        "shortName": null,
        "code": "741",
        "description": "UAS",
        "fullNameWithCode": "OC Uppsala/Örebro (2) - UAS (12001) - Onk klin (741)",
        "parentId": 9,
        "regionId": 3,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 866,
        "name": "Plastikkir. klin",
        "shortName": null,
        "code": "351",
        "description": "UAS",
        "fullNameWithCode": "OC Uppsala/Örebro (2) - UAS (12001) - Plastikkir. klin (351)",
        "parentId": 9,
        "regionId": 3,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 903,
        "name": "Test",
        "shortName": null,
        "code": "555",
        "description": "Test enhet",
        "fullNameWithCode": "OC Uppsala/Örebro (2) - UAS (12001) - Test (555)",
        "parentId": 9,
        "regionId": 3,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 206,
        "name": "Uro klin",
        "shortName": null,
        "code": "361",
        "description": "UAS",
        "fullNameWithCode": "OC Uppsala/Örebro (2) - UAS (12001) - Uro klin (361)",
        "parentId": 9,
        "regionId": 3,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 550,
        "name": "ÖNH klin",
        "shortName": null,
        "code": "521",
        "description": "UAS (12001)",
        "fullNameWithCode": "OC Uppsala/Örebro (2) - UAS (12001) - ÖNH klin (521)",
        "parentId": 9,
        "regionId": 3,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 182,
        "name": "Kir klin",
        "shortName": null,
        "code": "301",
        "description": "Västerås C.las",
        "fullNameWithCode": "OC Uppsala/Örebro (2) - Västerås C.las (56010) - Kir klin (301)",
        "parentId": 181,
        "regionId": 3,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 628,
        "name": "Lung klin",
        "shortName": null,
        "code": "111",
        "description": null,
        "fullNameWithCode": "OC Uppsala/Örebro (2) - Västerås C.las (56010) - Lung klin (111)",
        "parentId": 181,
        "regionId": 3,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 382,
        "name": "Onk klin",
        "shortName": null,
        "code": "741",
        "description": "Västerås C.las",
        "fullNameWithCode": "OC Uppsala/Örebro (2) - Västerås C.las (56010) - Onk klin (741)",
        "parentId": 181,
        "regionId": 3,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 290,
        "name": "Uro klin",
        "shortName": null,
        "code": "361",
        "description": "Västerås C.las",
        "fullNameWithCode": "OC Uppsala/Örebro (2) - Västerås C.las (56010) - Uro klin (361)",
        "parentId": 181,
        "regionId": 3,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 209,
        "name": "Kir klin",
        "shortName": null,
        "code": "301",
        "description": "Örebro U.sjh",
        "fullNameWithCode": "OC Uppsala/Örebro (2) - Örebro U.sjh (55010) - Kir klin (301)",
        "parentId": 207,
        "regionId": 3,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 210,
        "name": "Onk klin",
        "shortName": null,
        "code": "741",
        "description": "Örebro U.sjh",
        "fullNameWithCode": "OC Uppsala/Örebro (2) - Örebro U.sjh (55010) - Onk klin (741)",
        "parentId": 207,
        "regionId": 3,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 851,
        "name": "Plastikkir. klin",
        "shortName": null,
        "code": "351",
        "description": "Örebro Universitetssjukhus (55010)",
        "fullNameWithCode": "OC Uppsala/Örebro (2) - Örebro U.sjh (55010) - Plastikkir. klin (351)",
        "parentId": 207,
        "regionId": 3,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 208,
        "name": "Uro klin",
        "shortName": null,
        "code": "361",
        "description": "Örebro U.sjh",
        "fullNameWithCode": "OC Uppsala/Örebro (2) - Örebro U.sjh (55010) - Uro klin (361)",
        "parentId": 207,
        "regionId": 3,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 161,
        "name": "Kirurgen",
        "shortName": null,
        "code": "301",
        "description": "Alingsås",
        "fullNameWithCode": "OC Väst (5) - Alingsås (52012) - Kirurgen (301)",
        "parentId": 144,
        "regionId": 4,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 638,
        "name": "Kvinnoklinik",
        "shortName": null,
        "code": "451",
        "description": "Alingsås",
        "fullNameWithCode": "OC Väst (5) - Alingsås (52012) - Kvinnoklinik (451)",
        "parentId": 144,
        "regionId": 4,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 332,
        "name": "Medicinkliniken",
        "shortName": null,
        "code": "101",
        "description": "Alingsås",
        "fullNameWithCode": "OC Väst (5) - Alingsås (52012) - Medicinkliniken (101)",
        "parentId": 144,
        "regionId": 4,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 504,
        "name": "Plastikkirurgi",
        "shortName": null,
        "code": "351",
        "description": "Artclinic",
        "fullNameWithCode": "OC Väst (5) - Artclinic (500002) - Plastikkirurgi (351)",
        "parentId": 503,
        "regionId": 4,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 644,
        "name": "Gyn onk klinik",
        "shortName": null,
        "code": "751",
        "description": "Borås",
        "fullNameWithCode": "OC Väst (5) - Borås (52011) - Gyn onk klinik (751)",
        "parentId": 143,
        "regionId": 4,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 742,
        "name": "Hudkliniken",
        "shortName": null,
        "code": "211",
        "description": "Hudkliniken Borås",
        "fullNameWithCode": "OC Väst (5) - Borås (52011) - Hudkliniken (211)",
        "parentId": 143,
        "regionId": 4,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 160,
        "name": "Kirurgen",
        "shortName": null,
        "code": "301",
        "description": "Borås",
        "fullNameWithCode": "OC Väst (5) - Borås (52011) - Kirurgen (301)",
        "parentId": 143,
        "regionId": 4,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 637,
        "name": "Kvinnoklinik",
        "shortName": null,
        "code": "451",
        "description": "Borås",
        "fullNameWithCode": "OC Väst (5) - Borås (52011) - Kvinnoklinik (451)",
        "parentId": 143,
        "regionId": 4,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 693,
        "name": "Lungmedicin",
        "shortName": null,
        "code": "111",
        "description": "SÄS/Borås (52011)",
        "fullNameWithCode": "OC Väst (5) - Borås (52011) - Lungmedicin (111)",
        "parentId": 143,
        "regionId": 4,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 330,
        "name": "Medicinkliniken",
        "shortName": null,
        "code": "101",
        "description": "Borås",
        "fullNameWithCode": "OC Väst (5) - Borås (52011) - Medicinkliniken (101)",
        "parentId": 143,
        "regionId": 4,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 335,
        "name": "Onkologiska kliniken",
        "shortName": null,
        "code": "741",
        "description": "Borås",
        "fullNameWithCode": "OC Väst (5) - Borås (52011) - Onkologiska kliniken (741)",
        "parentId": 143,
        "regionId": 4,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 334,
        "name": "ÖNH",
        "shortName": null,
        "code": "521",
        "description": "Borås",
        "fullNameWithCode": "OC Väst (5) - Borås (52011) - ÖNH (521)",
        "parentId": 143,
        "regionId": 4,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 175,
        "name": "Kirurgen",
        "shortName": null,
        "code": "301",
        "description": "Carlanderska",
        "fullNameWithCode": "OC Väst (5) - Carlanderska (50480) - Kirurgen (301)",
        "parentId": 158,
        "regionId": 4,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 1196,
        "name": "ÖNH",
        "shortName": null,
        "code": "521",
        "description": "Dalslands sjukhus",
        "fullNameWithCode": "OC Väst (5) - Dalslands sjukhus (52014) - ÖNH (521)",
        "parentId": 1195,
        "regionId": 4,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 331,
        "name": "Enhet utan INCA-inrapportör",
        "shortName": null,
        "code": "59999",
        "description": "OC Väst",
        "fullNameWithCode": "OC Väst (5) - Enhet utan INCA-inrapportör (59999)",
        "parentId": 2,
        "regionId": 4,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 1188,
        "name": "Okänd klinik",
        "shortName": null,
        "code": "999",
        "description": "Okänd klinikVästra regionen",
        "fullNameWithCode": "OC Väst (5) - Enhet utan INCA-inrapportör (59999) - Okänd klinik (999)",
        "parentId": 331,
        "regionId": 4,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 165,
        "name": "Kirurgen",
        "shortName": null,
        "code": "301",
        "description": "Falköping",
        "fullNameWithCode": "OC Väst (5) - Falköping (53010) - Kirurgen (301)",
        "parentId": 148,
        "regionId": 4,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 969,
        "name": "Hudkliniken",
        "shortName": null,
        "code": "211",
        "description": "Hagakliniken (500003)",
        "fullNameWithCode": "OC Väst (5) - Hagakliniken (500003) - Hudkliniken (211)",
        "parentId": 968,
        "regionId": 4,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 172,
        "name": "Kirurgen",
        "shortName": null,
        "code": "301",
        "description": "Kungsbacka",
        "fullNameWithCode": "OC Väst (5) - Kungsbacka (421021) - Kirurgen (301)",
        "parentId": 155,
        "regionId": 4,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 162,
        "name": "Kirurgen",
        "shortName": null,
        "code": "301",
        "description": "Kungälv",
        "fullNameWithCode": "OC Väst (5) - Kungälv (51012) - Kirurgen (301)",
        "parentId": 145,
        "regionId": 4,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 639,
        "name": "Kvinnoklinik",
        "shortName": null,
        "code": "451",
        "description": "Kungälv",
        "fullNameWithCode": "OC Väst (5) - Kungälv (51012) - Kvinnoklinik (451)",
        "parentId": 145,
        "regionId": 4,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 337,
        "name": "Medicinkliniken",
        "shortName": null,
        "code": "101",
        "description": "Kungälv",
        "fullNameWithCode": "OC Väst (5) - Kungälv (51012) - Medicinkliniken (101)",
        "parentId": 145,
        "regionId": 4,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 169,
        "name": "Kirurgen",
        "shortName": null,
        "code": "301",
        "description": "Lidköping",
        "fullNameWithCode": "OC Väst (5) - Lidköping (53011) - Kirurgen (301)",
        "parentId": 152,
        "regionId": 4,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 646,
        "name": "Kvinnoklinik",
        "shortName": null,
        "code": "451",
        "description": "Lidköping",
        "fullNameWithCode": "OC Väst (5) - Lidköping (53011) - Kvinnoklinik (451)",
        "parentId": 152,
        "regionId": 4,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 325,
        "name": "Medicinkliniken",
        "shortName": null,
        "code": "101",
        "description": "Lidköping",
        "fullNameWithCode": "OC Väst (5) - Lidköping (53011) - Medicinkliniken (101)",
        "parentId": 152,
        "regionId": 4,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 173,
        "name": "Kirurgen",
        "shortName": null,
        "code": "301",
        "description": "Lundby",
        "fullNameWithCode": "OC Väst (5) - Lundby (50070) - Kirurgen (301)",
        "parentId": 156,
        "regionId": 4,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 429,
        "name": "Urologen",
        "shortName": null,
        "code": "361",
        "description": "Lundby",
        "fullNameWithCode": "OC Väst (5) - Lundby (50070) - Urologen (361)",
        "parentId": 156,
        "regionId": 4,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 977,
        "name": "ÖNH",
        "shortName": null,
        "code": "521",
        "description": "Lundby",
        "fullNameWithCode": "OC Väst (5) - Lundby (50070) - ÖNH (521)",
        "parentId": 156,
        "regionId": 4,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 847,
        "name": "Hudkliniken",
        "shortName": null,
        "code": "211",
        "description": "Läkarhuste (500001)",
        "fullNameWithCode": "OC Väst (5) - Läkarhuset i Göteborg (500001) - Hudkliniken (211)",
        "parentId": 505,
        "regionId": 4,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 506,
        "name": "Kirurgi",
        "shortName": null,
        "code": "301",
        "description": "Läkarhuset i Göteborg",
        "fullNameWithCode": "OC Väst (5) - Läkarhuset i Göteborg (500001) - Kirurgi (301)",
        "parentId": 505,
        "regionId": 4,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 916,
        "name": "Hudkliniken",
        "shortName": null,
        "code": "211",
        "description": "Mariestad (53012)",
        "fullNameWithCode": "OC Väst (5) - Mariestad (53012) - Hudkliniken (211)",
        "parentId": 153,
        "regionId": 4,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 170,
        "name": "Kirurgen",
        "shortName": null,
        "code": "301",
        "description": "Mariestad",
        "fullNameWithCode": "OC Väst (5) - Mariestad (53012) - Kirurgen (301)",
        "parentId": 153,
        "regionId": 4,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 1146,
        "name": "Kvinnoklinik",
        "shortName": null,
        "code": "451",
        "description": "Mariestad (53012)",
        "fullNameWithCode": "OC Väst (5) - Mariestad (53012) - Kvinnoklinik (451)",
        "parentId": 153,
        "regionId": 4,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 164,
        "name": "Kirurgen",
        "shortName": null,
        "code": "301",
        "description": "NÄL",
        "fullNameWithCode": "OC Väst (5) - NÄL (52017) - Kirurgen (301)",
        "parentId": 147,
        "regionId": 4,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 641,
        "name": "Kvinnoklinik",
        "shortName": null,
        "code": "451",
        "description": "NÄL",
        "fullNameWithCode": "OC Väst (5) - NÄL (52017) - Kvinnoklinik (451)",
        "parentId": 147,
        "regionId": 4,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 690,
        "name": "Lungmedicin",
        "shortName": null,
        "code": "111",
        "description": "NÄL (52017)",
        "fullNameWithCode": "OC Väst (5) - NÄL (52017) - Lungmedicin (111)",
        "parentId": 147,
        "regionId": 4,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 1015,
        "name": "Neurologiska kliniken",
        "shortName": null,
        "code": "221",
        "description": "NÄL",
        "fullNameWithCode": "OC Väst (5) - NÄL (52017) - Neurologiska kliniken (221)",
        "parentId": 147,
        "regionId": 4,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 304,
        "name": "ÖNH",
        "shortName": null,
        "code": "521",
        "description": "NÄL",
        "fullNameWithCode": "OC Väst (5) - NÄL (52017) - ÖNH (521)",
        "parentId": 147,
        "regionId": 4,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 328,
        "name": "Privatläkare",
        "shortName": null,
        "code": "50199",
        "description": "OC Väst",
        "fullNameWithCode": "OC Väst (5) - Privatläkare (50199)",
        "parentId": 2,
        "regionId": 4,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 1018,
        "name": "Privatläkare Bohuslän",
        "shortName": null,
        "code": "51199",
        "description": "Privatläkare Bohuslän",
        "fullNameWithCode": "OC Väst (5) - Privatläkare Bohuslän (51199)",
        "parentId": 2,
        "regionId": 4,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 1139,
        "name": "Privatläkare norra Halland",
        "shortName": null,
        "code": "421990",
        "description": "Privatläkare i norra Halland för västra regionen",
        "fullNameWithCode": "OC Väst (5) - Privatläkare norra Halland (421990)",
        "parentId": 2,
        "regionId": 4,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 949,
        "name": "Privatläkare Skaraborg",
        "shortName": null,
        "code": "53199",
        "description": "Privarläkare i Skaraborg",
        "fullNameWithCode": "OC Väst (5) - Privatläkare Skaraborg (53199)",
        "parentId": 2,
        "regionId": 4,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 950,
        "name": "Privatläkare Älvsborg",
        "shortName": null,
        "code": "52199",
        "description": "Privatläkare i Älvsborg",
        "fullNameWithCode": "OC Väst (5) - Privatläkare Älvsborg (52199)",
        "parentId": 2,
        "regionId": 4,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 168,
        "name": "Kirurgen",
        "shortName": null,
        "code": "301",
        "description": "Skene",
        "fullNameWithCode": "OC Väst (5) - Skene (52013) - Kirurgen (301)",
        "parentId": 151,
        "regionId": 4,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 432,
        "name": "Hudkliniken",
        "shortName": null,
        "code": "211",
        "description": "Skövde",
        "fullNameWithCode": "OC Väst (5) - Skövde (53013) - Hudkliniken (211)",
        "parentId": 14,
        "regionId": 4,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 16,
        "name": "Kirurgen",
        "shortName": null,
        "code": "301",
        "description": "Skövde",
        "fullNameWithCode": "OC Väst (5) - Skövde (53013) - Kirurgen (301)",
        "parentId": 14,
        "regionId": 4,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 636,
        "name": "Kvinnoklinik",
        "shortName": null,
        "code": "451",
        "description": "Skövde",
        "fullNameWithCode": "OC Väst (5) - Skövde (53013) - Kvinnoklinik (451)",
        "parentId": 14,
        "regionId": 4,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 691,
        "name": "Lungmedicin",
        "shortName": null,
        "code": "111",
        "description": "KSS (53013)",
        "fullNameWithCode": "OC Väst (5) - Skövde (53013) - Lungmedicin (111)",
        "parentId": 14,
        "regionId": 4,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 15,
        "name": "Medicinkliniken",
        "shortName": null,
        "code": "101",
        "description": "Skövde",
        "fullNameWithCode": "OC Väst (5) - Skövde (53013) - Medicinkliniken (101)",
        "parentId": 14,
        "regionId": 4,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 377,
        "name": "Urologen",
        "shortName": null,
        "code": "361",
        "description": "Skövde",
        "fullNameWithCode": "OC Väst (5) - Skövde (53013) - Urologen (361)",
        "parentId": 14,
        "regionId": 4,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 17,
        "name": "ÖNH",
        "shortName": null,
        "code": "521",
        "description": "Skövde",
        "fullNameWithCode": "OC Väst (5) - Skövde (53013) - ÖNH (521)",
        "parentId": 14,
        "regionId": 4,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 846,
        "name": "Hudkliniken",
        "shortName": null,
        "code": "211",
        "description": "Falkenberg (421001)",
        "fullNameWithCode": "OC Väst (5) - Specialistsjukv i Falkenberg (421001) - Hudkliniken (211)",
        "parentId": 507,
        "regionId": 4,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 508,
        "name": "Kirurgi",
        "shortName": null,
        "code": "301",
        "description": "Specialistsjukv i Falkenberg",
        "fullNameWithCode": "OC Väst (5) - Specialistsjukv i Falkenberg (421001) - Kirurgi (301)",
        "parentId": 507,
        "regionId": 4,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 166,
        "name": "Kirurgen",
        "shortName": null,
        "code": "301",
        "description": "SU/Mölndal",
        "fullNameWithCode": "OC Väst (5) - SU/Mölndal (51011) - Kirurgen (301)",
        "parentId": 149,
        "regionId": 4,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 1189,
        "name": "Endokrinologen",
        "shortName": null,
        "code": "161",
        "description": "Endokrinologen SU/Sahlgrenska",
        "fullNameWithCode": "OC Väst (5) - SU/Sahlgrenska (50001) - Endokrinologen (161)",
        "parentId": 10,
        "regionId": 4,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 634,
        "name": "Gyn onk klinik",
        "shortName": null,
        "code": "751",
        "description": "SU/Sahlgrenska",
        "fullNameWithCode": "OC Väst (5) - SU/Sahlgrenska (50001) - Gyn onk klinik (751)",
        "parentId": 10,
        "regionId": 4,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 336,
        "name": "Hudkliniken",
        "shortName": null,
        "code": "211",
        "description": "SU/Sahlgrenska",
        "fullNameWithCode": "OC Väst (5) - SU/Sahlgrenska (50001) - Hudkliniken (211)",
        "parentId": 10,
        "regionId": 4,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 18,
        "name": "Kirurgen",
        "shortName": null,
        "code": "301",
        "description": "SU/Sahlgrenska",
        "fullNameWithCode": "OC Väst (5) - SU/Sahlgrenska (50001) - Kirurgen (301)",
        "parentId": 10,
        "regionId": 4,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 643,
        "name": "Kvinnoklinik",
        "shortName": null,
        "code": "451",
        "description": "SU/S",
        "fullNameWithCode": "OC Väst (5) - SU/Sahlgrenska (50001) - Kvinnoklinik (451)",
        "parentId": 10,
        "regionId": 4,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 625,
        "name": "Lungmedicin",
        "shortName": null,
        "code": "111",
        "description": "SU/Sahlgrenska (50001)",
        "fullNameWithCode": "OC Väst (5) - SU/Sahlgrenska (50001) - Lungmedicin (111)",
        "parentId": 10,
        "regionId": 4,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 11,
        "name": "Medicinkliniken",
        "shortName": null,
        "code": "101",
        "description": "SU/Sahlgrenska",
        "fullNameWithCode": "OC Väst (5) - SU/Sahlgrenska (50001) - Medicinkliniken (101)",
        "parentId": 10,
        "regionId": 4,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 896,
        "name": "Neurokirurgiska kliniken",
        "shortName": null,
        "code": "331",
        "description": "SU/Sahlgrenska",
        "fullNameWithCode": "OC Väst (5) - SU/Sahlgrenska (50001) - Neurokirurgiska kliniken (331)",
        "parentId": 10,
        "regionId": 4,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 1168,
        "name": "Neurologiska kliniken",
        "shortName": null,
        "code": "221",
        "description": "SU/Sahlgrenska",
        "fullNameWithCode": "OC Väst (5) - SU/Sahlgrenska (50001) - Neurologiska kliniken (221)",
        "parentId": 10,
        "regionId": 4,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 21,
        "name": "Onkologiska kliniken",
        "shortName": null,
        "code": "741",
        "description": "SU/Sahlgrenska",
        "fullNameWithCode": "OC Väst (5) - SU/Sahlgrenska (50001) - Onkologiska kliniken (741)",
        "parentId": 10,
        "regionId": 4,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 1395,
        "name": "Ortopeden",
        "shortName": "Ortopeden",
        "code": "311",
        "description": "Ortopeden SU/Sahlgrenska",
        "fullNameWithCode": "OC Väst (5) - SU/Sahlgrenska (50001) - Ortopeden (311)",
        "parentId": 10,
        "regionId": 4,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 840,
        "name": "Plastikkirurgen",
        "shortName": null,
        "code": "351",
        "description": "SU/Sahlgrenska (50001)",
        "fullNameWithCode": "OC Väst (5) - SU/Sahlgrenska (50001) - Plastikkirurgen (351)",
        "parentId": 10,
        "regionId": 4,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 141,
        "name": "Urologen",
        "shortName": null,
        "code": "361",
        "description": "SU/Sahlgrenska",
        "fullNameWithCode": "OC Väst (5) - SU/Sahlgrenska (50001) - Urologen (361)",
        "parentId": 10,
        "regionId": 4,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 13,
        "name": "ÖNH",
        "shortName": null,
        "code": "521",
        "description": "SU/Sahlgrenska",
        "fullNameWithCode": "OC Väst (5) - SU/Sahlgrenska (50001) - ÖNH (521)",
        "parentId": 10,
        "regionId": 4,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 163,
        "name": "Kirurgen",
        "shortName": null,
        "code": "301",
        "description": "SU/Östra",
        "fullNameWithCode": "OC Väst (5) - SU/Östra (50010) - Kirurgen (301)",
        "parentId": 146,
        "regionId": 4,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 640,
        "name": "Kvinnoklinik",
        "shortName": null,
        "code": "451",
        "description": "SU/Ö",
        "fullNameWithCode": "OC Väst (5) - SU/Östra (50010) - Kvinnoklinik (451)",
        "parentId": 146,
        "regionId": 4,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 329,
        "name": "Medicinkliniken",
        "shortName": null,
        "code": "101",
        "description": "SU/Östra",
        "fullNameWithCode": "OC Väst (5) - SU/Östra (50010) - Medicinkliniken (101)",
        "parentId": 146,
        "regionId": 4,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 849,
        "name": "Hudkliniken",
        "shortName": null,
        "code": "211",
        "description": "Uddevalla (51010)",
        "fullNameWithCode": "OC Väst (5) - Uddevalla (51010) - Hudkliniken (211)",
        "parentId": 150,
        "regionId": 4,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 167,
        "name": "Kirurgen",
        "shortName": null,
        "code": "301",
        "description": "Uddevalla",
        "fullNameWithCode": "OC Väst (5) - Uddevalla (51010) - Kirurgen (301)",
        "parentId": 150,
        "regionId": 4,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 645,
        "name": "Kvinnoklinik",
        "shortName": null,
        "code": "451",
        "description": "Uddevalla",
        "fullNameWithCode": "OC Väst (5) - Uddevalla (51010) - Kvinnoklinik (451)",
        "parentId": 150,
        "regionId": 4,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 692,
        "name": "Lungmedicin",
        "shortName": null,
        "code": "111",
        "description": "Uddevalla",
        "fullNameWithCode": "OC Väst (5) - Uddevalla (51010) - Lungmedicin (111)",
        "parentId": 150,
        "regionId": 4,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 324,
        "name": "Medicinkliniken",
        "shortName": null,
        "code": "101",
        "description": "Uddevalla",
        "fullNameWithCode": "OC Väst (5) - Uddevalla (51010) - Medicinkliniken (101)",
        "parentId": 150,
        "regionId": 4,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 745,
        "name": "Hudkliniken",
        "shortName": null,
        "code": "211",
        "description": "Hudkliniken Västra Frölunda",
        "fullNameWithCode": "OC Väst (5) - V:a Frölunda (50071) - Hudkliniken (211)",
        "parentId": 157,
        "regionId": 4,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 174,
        "name": "Kirurgen",
        "shortName": null,
        "code": "301",
        "description": "V:a Frölunda",
        "fullNameWithCode": "OC Väst (5) - V:a Frölunda (50071) - Kirurgen (301)",
        "parentId": 157,
        "regionId": 4,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 1215,
        "name": "ÖNH",
        "shortName": null,
        "code": "521",
        "description": "Västra Frölunda",
        "fullNameWithCode": "OC Väst (5) - V:a Frölunda (50071) - ÖNH (521)",
        "parentId": 157,
        "regionId": 4,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 171,
        "name": "Kirurgen",
        "shortName": null,
        "code": "301",
        "description": "Varberg",
        "fullNameWithCode": "OC Väst (5) - Varberg (42011) - Kirurgen (301)",
        "parentId": 154,
        "regionId": 4,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 647,
        "name": "Kvinnoklinik",
        "shortName": null,
        "code": "451",
        "description": "Varberg",
        "fullNameWithCode": "OC Väst (5) - Varberg (42011) - Kvinnoklinik (451)",
        "parentId": 154,
        "regionId": 4,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 340,
        "name": "Medicinkliniken",
        "shortName": null,
        "code": "101",
        "description": "Varberg",
        "fullNameWithCode": "OC Väst (5) - Varberg (42011) - Medicinkliniken (101)",
        "parentId": 154,
        "regionId": 4,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 1406,
        "name": "Urologen",
        "shortName": "Urologen",
        "code": "361",
        "description": "Urologen på Varbergs sjukhus",
        "fullNameWithCode": "OC Väst (5) - Varberg (42011) - Urologen (361)",
        "parentId": 154,
        "regionId": 4,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 732,
        "name": "ÖNH",
        "shortName": null,
        "code": "521",
        "description": "Varberg (42011)",
        "fullNameWithCode": "OC Väst (5) - Varberg (42011) - ÖNH (521)",
        "parentId": 154,
        "regionId": 4,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 327,
        "name": "Vårdcentral",
        "shortName": null,
        "code": "50000",
        "description": "OC Väst",
        "fullNameWithCode": "OC Väst (5) - Vårdcentral (50000)",
        "parentId": 2,
        "regionId": 4,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 1138,
        "name": "Vårdcentral i norra Halland",
        "shortName": null,
        "code": "420000",
        "description": "Vårdcentral i norra Halland för västra regionen",
        "fullNameWithCode": "OC Väst (5) - Vårdcentral i norra Halland (420000)",
        "parentId": 2,
        "regionId": 4,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 201,
        "name": "Kir klin",
        "shortName": null,
        "code": "301",
        "description": "Kalix",
        "fullNameWithCode": "OC Demo (0) - Kalix (0650140) - Kir klin (301)",
        "parentId": 200,
        "regionId": 7,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 1288,
        "name": "Demo Onkologen",
        "shortName": null,
        "code": "741",
        "description": "Demo Onkolgen (011001)",
        "fullNameWithCode": "OC Demo (0) - KS Solna (0111001) - Demo Onkologen (741)",
        "parentId": 531,
        "regionId": 7,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 1292,
        "name": "Demo Klinik",
        "shortName": null,
        "code": "111",
        "description": "Demo Klinik",
        "fullNameWithCode": "OC Demo (0) - KS-Solna Demo (011001) - Demo Klinik (111)",
        "parentId": 1291,
        "regionId": 7,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 136,
        "name": "Kirurgkliniken",
        "shortName": null,
        "code": "301",
        "description": "Linköping US",
        "fullNameWithCode": "OC Demo (0) - Linköping US (210010) - Kirurgkliniken (301)",
        "parentId": 134,
        "regionId": 7,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 540,
        "name": "ÖNH",
        "shortName": null,
        "code": "521",
        "description": "Borås",
        "fullNameWithCode": "OC Demo (0) - Linköping US (210010) - ÖNH (521)",
        "parentId": 134,
        "regionId": 7,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 459,
        "name": "Hematologiska klinken",
        "shortName": null,
        "code": "105",
        "description": "Lund",
        "fullNameWithCode": "OC Demo (0) - Lund (410010) - Hematologiska klinken (105)",
        "parentId": 120,
        "regionId": 7,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 121,
        "name": "Kirurgen",
        "shortName": null,
        "code": "301",
        "description": "Lund",
        "fullNameWithCode": "OC Demo (0) - Lund (410010) - Kirurgen (301)",
        "parentId": 120,
        "regionId": 7,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 1112,
        "name": "Medicinska kliniken",
        "shortName": null,
        "code": "101",
        "description": "demo klinik för södra regionen",
        "fullNameWithCode": "OC Demo (0) - Lund (410010) - Medicinska kliniken (101)",
        "parentId": 120,
        "regionId": 7,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 1105,
        "name": "Onkologiska kliniken",
        "shortName": null,
        "code": "741",
        "description": "Länssjukhuset i Halmstad",
        "fullNameWithCode": "OC Demo (0) - Lund (410010) - Onkologiska kliniken (741)",
        "parentId": 120,
        "regionId": 7,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 1107,
        "name": "Uro klin",
        "shortName": null,
        "code": "361",
        "description": "Bollnäs sjh",
        "fullNameWithCode": "OC Demo (0) - Lund (410010) - Uro klin (361)",
        "parentId": 120,
        "regionId": 7,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 1114,
        "name": "Demo kirugen",
        "shortName": null,
        "code": "301",
        "description": "Demo kirurgen för Södra regionen",
        "fullNameWithCode": "OC Demo (0) - Lunds Universitetssjukhuset (41001) - Demo kirugen (301)",
        "parentId": 1113,
        "regionId": 7,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 1115,
        "name": "Demo med klin",
        "shortName": null,
        "code": "101",
        "description": "demo klinik för södra regionen",
        "fullNameWithCode": "OC Demo (0) - Lunds Universitetssjukhuset (41001) - Demo med klin (101)",
        "parentId": 1113,
        "regionId": 7,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 1124,
        "name": "Demo onkologen",
        "shortName": null,
        "code": "741",
        "description": "Demo onkologen för södra regionen",
        "fullNameWithCode": "OC Demo (0) - Lunds Universitetssjukhuset (41001) - Demo onkologen (741)",
        "parentId": 1113,
        "regionId": 7,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 1117,
        "name": "Demo onkologiska kliniken",
        "shortName": null,
        "code": "741",
        "description": "demo klinik för södra regionen",
        "fullNameWithCode": "OC Demo (0) - Lunds Universitetssjukhuset (41001) - Demo onkologiska kliniken (741)",
        "parentId": 1113,
        "regionId": 7,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 191,
        "name": "Kir klin",
        "shortName": null,
        "code": "301",
        "description": "Lycksele",
        "fullNameWithCode": "OC Demo (0) - Lycksele (0640110) - Kir klin (301)",
        "parentId": 190,
        "regionId": 7,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 123,
        "name": "Kirurgen",
        "shortName": null,
        "code": "301",
        "description": "NUS",
        "fullNameWithCode": "OC Demo (0) - NUS (0640010) - Kirurgen (301)",
        "parentId": 122,
        "regionId": 7,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 129,
        "name": "Urol klin",
        "shortName": null,
        "code": "361",
        "description": "NUS",
        "fullNameWithCode": "OC Demo (0) - NUS (0640010) - Urol klin (361)",
        "parentId": 122,
        "regionId": 7,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 41,
        "name": "Kirurgen",
        "shortName": null,
        "code": "301",
        "description": "Skövde",
        "fullNameWithCode": "OC Demo (0) - Skövde (530130) - Kirurgen (301)",
        "parentId": 35,
        "regionId": 7,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 42,
        "name": "ÖNH",
        "shortName": null,
        "code": "521",
        "description": "Skövde",
        "fullNameWithCode": "OC Demo (0) - Skövde (530130) - ÖNH (521)",
        "parentId": 35,
        "regionId": 7,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 38,
        "name": "Kirurgen",
        "shortName": null,
        "code": "301",
        "description": "SU/Sahlgrenska",
        "fullNameWithCode": "OC Demo (0) - SU/Sahlgrenska (500010) - Kirurgen (301)",
        "parentId": 34,
        "regionId": 7,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 36,
        "name": "Medicinkliniken",
        "shortName": null,
        "code": "101",
        "description": "SU/Sahlgrenska",
        "fullNameWithCode": "OC Demo (0) - SU/Sahlgrenska (500010) - Medicinkliniken (101)",
        "parentId": 34,
        "regionId": 7,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 39,
        "name": "Onkologiska kliniken",
        "shortName": null,
        "code": "741",
        "description": "SU/Sahlgrenska",
        "fullNameWithCode": "OC Demo (0) - SU/Sahlgrenska (500010) - Onkologiska kliniken (741)",
        "parentId": 34,
        "regionId": 7,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 130,
        "name": "Urol klin",
        "shortName": null,
        "code": "361",
        "description": "SU/Sahlgrenska",
        "fullNameWithCode": "OC Demo (0) - SU/Sahlgrenska (500010) - Urol klin (361)",
        "parentId": 34,
        "regionId": 7,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 37,
        "name": "ÖNH",
        "shortName": null,
        "code": "521",
        "description": "SU/Sahlgrenska",
        "fullNameWithCode": "OC Demo (0) - SU/Sahlgrenska (500010) - ÖNH (521)",
        "parentId": 34,
        "regionId": 7,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 140,
        "name": "Kir klin",
        "shortName": null,
        "code": "301",
        "description": "UAS",
        "fullNameWithCode": "OC Demo (0) - UAS (120010) - Kir klin (301)",
        "parentId": 131,
        "regionId": 7,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    },
    {
        "id": 664,
        "name": "Lunga",
        "shortName": null,
        "code": "666",
        "description": null,
        "fullNameWithCode": "OC Demo (0) - UAS (120010) - Lunga (666)",
        "parentId": 131,
        "regionId": 7,
        "isEnabled": false,
        "isReporting": false,
        "director": {
            "name": null,
            "phone": null,
            "email": null
        },
        "contact": {
            "name": null,
            "phone": null,
            "email": null
        },
        "address": {
            "streetAddress": null,
            "zip": null,
            "city": null,
            "country": null
        }
    }
];


})(jQuery);