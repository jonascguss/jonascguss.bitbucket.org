﻿/**
 * jQuery Plugin for creating collapsible fieldset
 * @requires jQuery 1.2 or later
 *
 * Copyright (c) 2010 Lucky <bogeyman2007@gmail.com>
 * Licensed under the GPL license:
 *   http://www.gnu.org/licenses/gpl.html
 *
 * "animation" and "speed" options are added by Mitch Kuppinger <dpneumo@gmail.com>
 */

(function($) {
	function hideFieldsetContent(obj, options){
		obj.children('legend').children('.indicator').html(" &#x25ba; ");
		
		if(options.animation==true)
			obj.children('div').slideUp(options.speed);
		else
			obj.children('div').hide();
		
		obj.removeClass("expanded");
		obj.addClass("collapsed");
		
		
	}
	
	function showFieldsetContent(obj, options){
		obj.children('legend').children('.indicator').html(" &#x25bc; ");
		
		if(options.animation==true)
			obj.children('div').slideDown(options.speed);
		else
			obj.children('div').show();
		
		obj.removeClass("collapsed");
		obj.addClass("expanded");
		
		
	}
	
	$.fn.collapsefieldset = function(options){
		var setting={collapsed:false, animation:true, speed:'medium', update:false};
		$.extend(setting, options);
		
		this.each(function(){
			var fieldset=$(this);
			var legend=fieldset.children('legend');
			
			if(setting.update==false)
				legend.html("<span class='indicator' style='padding-bottom: 3px'></span>" + legend.html());
			
			if(setting.collapsed==true){

				legend.children('.indicator').html(" &#x25ba; ");
				
				legend.toggle(
					function(){
						showFieldsetContent(fieldset, setting);
					},
					function(){
						hideFieldsetContent(fieldset, setting);
					}
				)
				
				hideFieldsetContent(fieldset, {animation:false});
			}
			else{
			
				legend.children('.indicator').html(" &#x25bc; ");
				
				legend.toggle(
					function(){
						hideFieldsetContent(fieldset, setting);
					},
					function(){
						showFieldsetContent(fieldset, setting);
					}
				)
			}
		});
	}
})(jQuery);