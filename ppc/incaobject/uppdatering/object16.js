new IncaOffline( 
			{ 
				emulate:
{
    "inca": {
        "scriptSupport": {},
        "services": {
            "infoText": {},
            "report": {},
            "viewTemplate": {}
        },
        "user": {
            "id": 6855,
            "firstName": "Jonas",
            "lastName": "Celander Guss",
            "username": "oc2gujo",
            "email": "jonas.celander.guss@akademiska.se",
            "phoneNumber": null,
            "role": {
                "id": 43,
                "name": "Inrapportör (Patientöversikt)",
                "isReviewer": false
            },
            "position": {
                "id": 36,
                "name": "Medicinkliniken",
                "code": "101",
                "fullNameWithCode": "OC Demo (0) - SU/Sahlgrenska (500010) - Medicinkliniken (101)"
            },
            "region": {
                "id": 7,
                "name": "Region Demo"
            },
            "previousLogin": "2016-04-22T07:49:29.587Z"
        },
        "serverDate": "2016-04-22T07:56:22.583Z",
        "form": {
            "metadata": {
                "Uppföljning": {
                    "subTables": [
                        "Bilddiagnostik",
                        "Labbprov",
                        "Besök",
                        "SRE",
                        "Läkemedel",
                        "Strålbehandling",
                        "Bidragande",
						"PROM"
                    ],
                    "regvars": {
                        "U_datum": {
                            "term": 1002,
                            "description": "Lagrar senaste datum då registret uppdaterades",
                            "technicalName": "T1123082",
                            "sortOrder": 0,
                            "compareDescription": "",
                            "isComparable": false
                        },
                        "U_npcr_fk": {
                            "term": 5565,
                            "description": "Koppling till lagrad patientinfo i NPCR register",
                            "technicalName": "T1124304",
                            "sortOrder": 0,
                            "compareDescription": "",
                            "isComparable": false
                        },
                        "U_crpcdatum": {
                            "term": 1002,
                            "description": "Datum för kastrationsresistent sjukdom",
                            "technicalName": "T1166816",
                            "sortOrder": 0,
                            "compareDescription": "",
                            "isComparable": false
                        },
                        "U_metastasdatum": {
                            "term": 1002,
                            "description": "1:a metastas datum",
                            "technicalName": "T1166818",
                            "sortOrder": 0,
                            "compareDescription": "",
                            "isComparable": false
                        },
                        "U_hormonstartdatum": {
                            "term": 1002,
                            "description": "Datum för start av primär hormonbehandling",
                            "technicalName": "T1166820",
                            "sortOrder": 0,
                            "compareDescription": "",
                            "isComparable": false
                        },
                        "U_psadatum": {
                            "term": 1002,
                            "description": "Datum för PSA recidiv",
                            "technicalName": "T1166817",
                            "sortOrder": 0,
                            "compareDescription": "",
                            "isComparable": false
                        },
                        "U_pallHemSjkvrd": {
                            "term": 1002,
                            "description": "Datum för palliativ hemsjukvård",
                            "technicalName": "T1312662",
                            "sortOrder": 0,
                            "compareDescription": "Datum för palliativ hemsjukvård",
                            "isComparable": false
                        },
                        "U_patMedgivande": {
                            "term": 5583,
                            "description": "Har patienten lämnat sitt medgivande",
                            "technicalName": "T1312663",
                            "sortOrder": 0,
                            "compareDescription": "Har patienten lämnat sitt medgivande",
                            "isComparable": false
                        }
                    },
                    "vdlists": {},
                    "terms": {
                        "1002": {
                            "name": "Datum",
                            "shortName": "Datum",
                            "description": "",
                            "dataType": "datetime",
                            "includeTime": false
                        },
                        "5565": {
                            "name": "ProstataProcess_NPCR_FK",
                            "shortName": "PP_NPCR_FK",
                            "description": "",
                            "dataType": "vd"
                        },
                        "5583": {
                            "name": "ProstataProcess_NejJa",
                            "shortName": "PP_NejJa",
                            "description": "",
                            "dataType": "list",
                            "listValues": [
                                {
                                    "id": 17406,
                                    "text": "Nej",
                                    "value": "0",
                                    "validFrom": "0001-01-01T00:00:00",
                                    "validTo": "9999-12-31T00:00:00"
                                },
                                {
                                    "id": 17407,
                                    "text": "Ja",
                                    "value": "1",
                                    "validFrom": "0001-01-01T00:00:00",
                                    "validTo": "9999-12-31T00:00:00"
                                }
                            ]
                        }
                    },
                    "treeLevel": 1
                },
                "Bilddiagnostik": {
                    "subTables": [
                        "BD_Metastas"
                    ],
                    "regvars": {
                        "BD_datum": {
                            "term": 1002,
                            "description": "Datum för bilddiagnostik",
                            "technicalName": "T1123083",
                            "sortOrder": 0,
                            "compareDescription": "",
                            "isComparable": false
                        },
                        "BD_radbed": {
                            "term": 5652,
                            "description": "Radiologisk bedömning",
                            "technicalName": "T1166632",
                            "sortOrder": 0,
                            "compareDescription": "",
                            "isComparable": false
                        },
                        "BD_radundtypannan": {
                            "term": 4075,
                            "description": "Annan typ av radiologisk undersökning",
                            "technicalName": "T1502990",
                            "sortOrder": 0,
                            "compareDescription": "Annan typ av radiologisk undersökning",
                            "isComparable": false
                        },
                        "BD_radundertyp": {
                            "term": 6268,
                            "description": "Typ av radiologisk undersökning",
                            "technicalName": "T1502989",
                            "sortOrder": 0,
                            "compareDescription": "Typ av radiologisk undersökning",
                            "isComparable": false
                        },
                        "BD_bsi": {
                            "term": 1460,
                            "description": "Bone Scan Index för skelettskintigrafi",
                            "technicalName": "T1520379",
                            "sortOrder": 0,
                            "compareDescription": "Bone Scan Index för skelettskintigrafi",
                            "isComparable": false
                        }
                    },
                    "vdlists": {},
                    "terms": {
                        "1002": {
                            "name": "Datum",
                            "shortName": "Datum",
                            "description": "",
                            "dataType": "datetime",
                            "includeTime": false
                        },
                        "1460": {
                            "name": "Decimal1",
                            "shortName": "Decimal1",
                            "description": "Decimal tal med 1 decimal",
                            "dataType": "decimal",
                            "precision": 1,
                            "min": null,
                            "max": null
                        },
                        "4075": {
                            "name": "Text(8000)",
                            "shortName": "TEXT",
                            "description": "Textvariabel med möjlighet att lagra maximalt antal tecken (8000).",
                            "dataType": "string",
                            "maxLength": 8000,
                            "validCharacters": null
                        },
                        "5652": {
                            "name": "ProstataProcess_RadiologiskBedömning",
                            "shortName": "PP_RadBedömning",
                            "description": "",
                            "dataType": "list",
                            "listValues": [
                                {
                                    "id": 17668,
                                    "text": "Inga synliga tecken på metastaser",
                                    "value": "3",
                                    "validFrom": "0001-01-01T00:00:00",
                                    "validTo": "9999-12-31T00:00:00"
                                },
                                {
                                    "id": 17665,
                                    "text": "Regress",
                                    "value": "0",
                                    "validFrom": "0001-01-01T00:00:00",
                                    "validTo": "9999-12-31T00:00:00"
                                },
                                {
                                    "id": 17666,
                                    "text": "Stationärt",
                                    "value": "1",
                                    "validFrom": "0001-01-01T00:00:00",
                                    "validTo": "9999-12-31T00:00:00"
                                },
                                {
                                    "id": 18001,
                                    "text": "Blandad respons",
                                    "value": "4",
                                    "validFrom": "0001-01-01T00:00:00",
                                    "validTo": "9999-12-31T00:00:00"
                                },
                                {
                                    "id": 17667,
                                    "text": "Progress",
                                    "value": "2",
                                    "validFrom": "0001-01-01T00:00:00",
                                    "validTo": "9999-12-31T00:00:00"
                                }
                            ]
                        },
                        "6268": {
                            "name": "ProstataProcess_RadiologiskUndersökningTyp",
                            "shortName": "ProstataProcess_Radi",
                            "description": "",
                            "dataType": "list",
                            "listValues": [
                                {
                                    "id": 19896,
                                    "text": "Skelettskintigrafi",
                                    "value": "1",
                                    "validFrom": "0001-01-01T00:00:00",
                                    "validTo": "9999-12-31T00:00:00"
                                },
                                {
                                    "id": 19897,
                                    "text": "CT",
                                    "value": "2",
                                    "validFrom": "0001-01-01T00:00:00",
                                    "validTo": "9999-12-31T00:00:00"
                                },
                                {
                                    "id": 20150,
                                    "text": "CT buk",
                                    "value": "21",
                                    "validFrom": "0001-01-01T00:00:00",
                                    "validTo": "9999-12-31T00:00:00"
                                },
                                {
                                    "id": 20151,
                                    "text": "CT thorax",
                                    "value": "22",
                                    "validFrom": "0001-01-01T00:00:00",
                                    "validTo": "9999-12-31T00:00:00"
                                },
                                {
                                    "id": 20152,
                                    "text": "CT skalle",
                                    "value": "23",
                                    "validFrom": "0001-01-01T00:00:00",
                                    "validTo": "9999-12-31T00:00:00"
                                },
                                {
                                    "id": 19898,
                                    "text": "MR",
                                    "value": "3",
                                    "validFrom": "0001-01-01T00:00:00",
                                    "validTo": "9999-12-31T00:00:00"
                                },
                                {
                                    "id": 20153,
                                    "text": "MR buk/bäcken",
                                    "value": "31",
                                    "validFrom": "0001-01-01T00:00:00",
                                    "validTo": "9999-12-31T00:00:00"
                                },
                                {
                                    "id": 20154,
                                    "text": "MR rygg",
                                    "value": "32",
                                    "validFrom": "0001-01-01T00:00:00",
                                    "validTo": "9999-12-31T00:00:00"
                                },
                                {
                                    "id": 20155,
                                    "text": "MR skalle",
                                    "value": "33",
                                    "validFrom": "0001-01-01T00:00:00",
                                    "validTo": "9999-12-31T00:00:00"
                                },
                                {
                                    "id": 19899,
                                    "text": "PET-CT",
                                    "value": "4",
                                    "validFrom": "0001-01-01T00:00:00",
                                    "validTo": "9999-12-31T00:00:00"
                                },
                                {
                                    "id": 19900,
                                    "text": "Annan",
                                    "value": "9",
                                    "validFrom": "0001-01-01T00:00:00",
                                    "validTo": "9999-12-31T00:00:00"
                                }
                            ]
                        }
                    },
                    "treeLevel": 2
                },
                "BD_Metastas": {
                    "subTables": [],
                    "regvars": {
                        "BDM_metastaslokalann": {
                            "term": 4075,
                            "description": "Nytillkomna metastaser i bilddiagnostik, Vilken annan lokal?",
                            "technicalName": "T1143283",
                            "sortOrder": 0,
                            "compareDescription": "",
                            "isComparable": false
                        },
                        "BDM_metastaslokal": {
                            "term": 5799,
                            "description": "Nytillkomna metastaser i bilddiagnostik",
                            "technicalName": "T1218964",
                            "sortOrder": 0,
                            "compareDescription": "",
                            "isComparable": false
                        }
                    },
                    "vdlists": {},
                    "terms": {
                        "4075": {
                            "name": "Text(8000)",
                            "shortName": "TEXT",
                            "description": "Textvariabel med möjlighet att lagra maximalt antal tecken (8000).",
                            "dataType": "string",
                            "maxLength": 8000,
                            "validCharacters": null
                        },
                        "5799": {
                            "name": "ProstataProcess_BDLokal",
                            "shortName": "PP_bdlokal",
                            "description": "",
                            "dataType": "list",
                            "listValues": [
                                {
                                    "id": 18002,
                                    "text": "Skelett",
                                    "value": "0",
                                    "validFrom": "0001-01-01T00:00:00",
                                    "validTo": "9999-12-31T00:00:00"
                                },
                                {
                                    "id": 18003,
                                    "text": "Lymfkörtlar",
                                    "value": "1",
                                    "validFrom": "0001-01-01T00:00:00",
                                    "validTo": "9999-12-31T00:00:00"
                                },
                                {
                                    "id": 18004,
                                    "text": "Lunga",
                                    "value": "2",
                                    "validFrom": "0001-01-01T00:00:00",
                                    "validTo": "9999-12-31T00:00:00"
                                },
                                {
                                    "id": 18005,
                                    "text": "Lever",
                                    "value": "3",
                                    "validFrom": "0001-01-01T00:00:00",
                                    "validTo": "9999-12-31T00:00:00"
                                },
                                {
                                    "id": 18006,
                                    "text": "Hjärna",
                                    "value": "4",
                                    "validFrom": "0001-01-01T00:00:00",
                                    "validTo": "9999-12-31T00:00:00"
                                },
                                {
                                    "id": 18007,
                                    "text": "Annan",
                                    "value": "5",
                                    "validFrom": "0001-01-01T00:00:00",
                                    "validTo": "9999-12-31T00:00:00"
                                }
                            ]
                        }
                    },
                    "treeLevel": 3
                },
                "Labbprov": {
                    "subTables": [],
                    "regvars": {
                        "LP_datum": {
                            "term": 1002,
                            "description": "Datum för labbprov",
                            "technicalName": "T1123087",
                            "sortOrder": 0,
                            "compareDescription": "",
                            "isComparable": false
                        },
                        "LP_hb": {
                            "term": 1040,
                            "description": "Hb(g/L)",
                            "technicalName": "T1123088",
                            "sortOrder": 0,
                            "compareDescription": "",
                            "isComparable": false
                        },
                        "LP_psa": {
                            "term": 1242,
                            "description": "PSA(ng/mL)",
                            "technicalName": "T1127493",
                            "sortOrder": 0,
                            "compareDescription": "",
                            "isComparable": false
                        },
                        "LP_krea": {
                            "term": 1040,
                            "description": "Krea(µmol/L)",
                            "technicalName": "T1127494",
                            "sortOrder": 0,
                            "compareDescription": "",
                            "isComparable": false
                        },
                        "LP_alp": {
                            "term": 1460,
                            "description": "ALP(µkat/L)",
                            "technicalName": "T1130686",
                            "sortOrder": 0,
                            "compareDescription": "",
                            "isComparable": false
                        },
                        "LP_testosteron": {
                            "term": 1242,
                            "description": "Testosteron (nmol/L)",
                            "technicalName": "T1206136",
                            "sortOrder": 0,
                            "compareDescription": "",
                            "isComparable": false
                        }
                    },
                    "vdlists": {},
                    "terms": {
                        "1002": {
                            "name": "Datum",
                            "shortName": "Datum",
                            "description": "",
                            "dataType": "datetime",
                            "includeTime": false
                        },
                        "1040": {
                            "name": "Heltal",
                            "shortName": "Heltal",
                            "description": "",
                            "dataType": "integer",
                            "min": null,
                            "max": null
                        },
                        "1242": {
                            "name": "Decimal2",
                            "shortName": "DecimalTal2",
                            "description": "Decimal tal med 2 decimaler",
                            "dataType": "decimal",
                            "precision": 2,
                            "min": null,
                            "max": null
                        },
                        "1460": {
                            "name": "Decimal1",
                            "shortName": "Decimal1",
                            "description": "Decimal tal med 1 decimal",
                            "dataType": "decimal",
                            "precision": 1,
                            "min": null,
                            "max": null
                        }
                    },
                    "treeLevel": 2
                },
                "Besök": {
                    "subTables": [],
                    "regvars": {
                        "B_besokdatum": {
                            "term": 1002,
                            "description": "Datum för återbesök",
                            "technicalName": "T1127265",
                            "sortOrder": 0,
                            "compareDescription": "",
                            "isComparable": false
                        },
                        "B_biobank": {
                            "term": 5583,
                            "description": "Biobanksprov",
                            "technicalName": "T1127270",
                            "sortOrder": 0,
                            "compareDescription": "",
                            "isComparable": false
                        },
                        "B_ecogwho": {
                            "term": 5584,
                            "description": "ECOG-WHO",
                            "technicalName": "T1127267",
                            "sortOrder": 0,
                            "compareDescription": "",
                            "isComparable": false
                        },
                        "B_studie": {
                            "term": 5583,
                            "description": "Patienten ingår i studie",
                            "technicalName": "T1127271",
                            "sortOrder": 0,
                            "compareDescription": "",
                            "isComparable": false
                        },
                        "B_klinbed": {
                            "term": 5585,
                            "description": "Sammantagen klinisk bedömning",
                            "technicalName": "T1127268",
                            "sortOrder": 0,
                            "compareDescription": "",
                            "isComparable": false
                        },
                        "B_lakare": {
                            "term": 4075,
                            "description": "Undersökande läkare för återbesök",
                            "technicalName": "T1127266",
                            "sortOrder": 0,
                            "compareDescription": "",
                            "isComparable": false
                        },
                        "B_studietxt": {
                            "term": 4075,
                            "description": "Patienten ingår i studie, Vilken studie?",
                            "technicalName": "T1155627",
                            "sortOrder": 0,
                            "compareDescription": "",
                            "isComparable": false
                        },
                        "B_multikonf": {
                            "term": 5583,
                            "description": "Multidisciplinär konferens",
                            "technicalName": "T1166633",
                            "sortOrder": 0,
                            "compareDescription": "",
                            "isComparable": false
                        },
                        "B_crpc": {
                            "term": 5583,
                            "description": "Kastrationsresistent sjukdom",
                            "technicalName": "T1169219",
                            "sortOrder": 0,
                            "compareDescription": "",
                            "isComparable": false
                        },
                        "B_palliativvard": {
                            "term": 5583,
                            "description": "Patienten är inskriven i palliativ hemsjukvård",
                            "technicalName": "T1218976",
                            "sortOrder": 0,
                            "compareDescription": "",
                            "isComparable": false
                        },
                        "B_klinEjBed": {
                            "term": 1004,
                            "description": "Sjuksköterskebesök",
                            "technicalName": "T1312664",
                            "sortOrder": 0,
                            "compareDescription": "Sjuksköterskebesök",
                            "isComparable": false
                        },
                        "B_biobankprovtyp": {
                            "term": 6048,
                            "description": "Typ av prov lämnat till biobank",
                            "technicalName": "T1375444",
                            "sortOrder": 0,
                            "compareDescription": "Typ av prov lämnat till biobank",
                            "isComparable": false
                        },
                        "B_biobankDatum": {
                            "term": 1002,
                            "description": "Biobanksprov datum",
                            "technicalName": "T1597959",
                            "sortOrder": 0,
                            "compareDescription": "Biobanksprov datum",
                            "isComparable": false
                        }
                    },
                    "vdlists": {},
                    "terms": {
                        "1002": {
                            "name": "Datum",
                            "shortName": "Datum",
                            "description": "",
                            "dataType": "datetime",
                            "includeTime": false
                        },
                        "1004": {
                            "name": "Kryssruta",
                            "shortName": "Kryssruta",
                            "description": "",
                            "dataType": "boolean"
                        },
                        "4075": {
                            "name": "Text(8000)",
                            "shortName": "TEXT",
                            "description": "Textvariabel med möjlighet att lagra maximalt antal tecken (8000).",
                            "dataType": "string",
                            "maxLength": 8000,
                            "validCharacters": null
                        },
                        "5583": {
                            "name": "ProstataProcess_NejJa",
                            "shortName": "PP_NejJa",
                            "description": "",
                            "dataType": "list",
                            "listValues": [
                                {
                                    "id": 17406,
                                    "text": "Nej",
                                    "value": "0",
                                    "validFrom": "0001-01-01T00:00:00",
                                    "validTo": "9999-12-31T00:00:00"
                                },
                                {
                                    "id": 17407,
                                    "text": "Ja",
                                    "value": "1",
                                    "validFrom": "0001-01-01T00:00:00",
                                    "validTo": "9999-12-31T00:00:00"
                                }
                            ]
                        },
                        "5584": {
                            "name": "ProstataProcess_ECOG-WHO",
                            "shortName": "PP_ECOGWHO",
                            "description": "",
                            "dataType": "list",
                            "listValues": [
                                {
                                    "id": 17412,
                                    "text": "Klarar all normal aktivitet utan begränsning.",
                                    "value": "0",
                                    "validFrom": "0001-01-01T00:00:00",
                                    "validTo": "9999-12-31T00:00:00"
                                },
                                {
                                    "id": 17413,
                                    "text": "Klarar inte fysiskt krävande aktivitet men är uppegående och i stånd till lättare arbete.",
                                    "value": "1",
                                    "validFrom": "0001-01-01T00:00:00",
                                    "validTo": "9999-12-31T00:00:00"
                                },
                                {
                                    "id": 17414,
                                    "text": "Är uppegående och kan sköta sig själv men klarar inte att arbeta. Är uppe och i rörelse mer än 50% av dygnets vakna timmar.",
                                    "value": "2",
                                    "validFrom": "0001-01-01T00:00:00",
                                    "validTo": "9999-12-31T00:00:00"
                                },
                                {
                                    "id": 17415,
                                    "text": "Kan endast delvis sköta sig själv. Är bunden till säng eller stol mer än 50% av dygnets vakna timmar.",
                                    "value": "3",
                                    "validFrom": "0001-01-01T00:00:00",
                                    "validTo": "9999-12-31T00:00:00"
                                },
                                {
                                    "id": 17416,
                                    "text": "Klarar inte någonting. Kan inte sköta sig själv. Är bunden till säng eller stol.",
                                    "value": "4",
                                    "validFrom": "0001-01-01T00:00:00",
                                    "validTo": "9999-12-31T00:00:00"
                                }
                            ]
                        },
                        "5585": {
                            "name": "ProstataProcess_KliniskBedömning",
                            "shortName": "PP_KliniskBedömning",
                            "description": "",
                            "dataType": "list",
                            "listValues": [
                                {
                                    "id": 18264,
                                    "text": "Komplett respons",
                                    "value": "4",
                                    "validFrom": "0001-01-01T00:00:00",
                                    "validTo": "9999-12-31T00:00:00"
                                },
                                {
                                    "id": 17408,
                                    "text": "Partiell respons",
                                    "value": "0",
                                    "validFrom": "0001-01-01T00:00:00",
                                    "validTo": "9999-12-31T00:00:00"
                                },
                                {
                                    "id": 17409,
                                    "text": "Stationär sjukdom",
                                    "value": "1",
                                    "validFrom": "0001-01-01T00:00:00",
                                    "validTo": "9999-12-31T00:00:00"
                                },
                                {
                                    "id": 17410,
                                    "text": "Progress",
                                    "value": "2",
                                    "validFrom": "0001-01-01T00:00:00",
                                    "validTo": "9999-12-31T00:00:00"
                                },
                                {
                                    "id": 17411,
                                    "text": "Avvakta bedömning",
                                    "value": "3",
                                    "validFrom": "0001-01-01T00:00:00",
                                    "validTo": "9999-12-31T00:00:00"
                                }
                            ]
                        },
                        "6048": {
                            "name": "ProstataProcess_BiobankTyp",
                            "shortName": "PP_BiobankTyp",
                            "description": "",
                            "dataType": "list",
                            "listValues": [
                                {
                                    "id": 18688,
                                    "text": "Blod",
                                    "value": "1",
                                    "validFrom": "0001-01-01T00:00:00",
                                    "validTo": "9999-12-31T00:00:00"
                                },
                                {
                                    "id": 18689,
                                    "text": "Benmärg",
                                    "value": "2",
                                    "validFrom": "0001-01-01T00:00:00",
                                    "validTo": "9999-12-31T00:00:00"
                                },
                                {
                                    "id": 18690,
                                    "text": "Skelett",
                                    "value": "3",
                                    "validFrom": "0001-01-01T00:00:00",
                                    "validTo": "9999-12-31T00:00:00"
                                },
                                {
                                    "id": 18691,
                                    "text": "Övrigt",
                                    "value": "4",
                                    "validFrom": "0001-01-01T00:00:00",
                                    "validTo": "9999-12-31T00:00:00"
                                }
                            ]
                        }
                    },
                    "treeLevel": 2
                },
                "SRE": {
                    "subTables": [
                        "SRE_Händelse"
                    ],
                    "regvars": {
                        "SRE_datum": {
                            "term": 1002,
                            "description": "Datum för SRE",
                            "technicalName": "T1127281",
                            "sortOrder": 0,
                            "compareDescription": "",
                            "isComparable": false
                        }
                    },
                    "vdlists": {},
                    "terms": {
                        "1002": {
                            "name": "Datum",
                            "shortName": "Datum",
                            "description": "",
                            "dataType": "datetime",
                            "includeTime": false
                        }
                    },
                    "treeLevel": 2
                },
                "SRE_Händelse": {
                    "subTables": [],
                    "regvars": {
                        "SREH_handelse": {
                            "term": 5586,
                            "description": "Vilken SRE Händelse",
                            "technicalName": "T1143273",
                            "sortOrder": 0,
                            "compareDescription": "",
                            "isComparable": false
                        }
                    },
                    "vdlists": {},
                    "terms": {
                        "5586": {
                            "name": "ProstataProcess_SRE",
                            "shortName": "PP_SRE",
                            "description": "",
                            "dataType": "list",
                            "listValues": [
                                {
                                    "id": 17417,
                                    "text": "Medullakompression",
                                    "value": "0",
                                    "validFrom": "0001-01-01T00:00:00",
                                    "validTo": "9999-12-31T00:00:00"
                                },
                                {
                                    "id": 17418,
                                    "text": "Patologisk fraktur",
                                    "value": "1",
                                    "validFrom": "0001-01-01T00:00:00",
                                    "validTo": "9999-12-31T00:00:00"
                                },
                                {
                                    "id": 17419,
                                    "text": "Strålbehandling mot skelett",
                                    "value": "2",
                                    "validFrom": "0001-01-01T00:00:00",
                                    "validTo": "9999-12-31T00:00:00"
                                },
                                {
                                    "id": 17420,
                                    "text": "Kirurgisk åtgärd",
                                    "value": "3",
                                    "validFrom": "0001-01-01T00:00:00",
                                    "validTo": "9999-12-31T00:00:00"
                                }
                            ]
                        }
                    },
                    "treeLevel": 3
                },
                "Läkemedel": {
                    "subTables": [
                        "LM_Insättning"
                    ],
                    "regvars": {
                        "LM_studielkmtxt": {
                            "term": 4075,
                            "description": "Vilket Studieläkemedel",
                            "technicalName": "T1155661",
                            "sortOrder": 0,
                            "compareDescription": "",
                            "isComparable": false
                        },
                        "LM_typ": {
                            "term": 5629,
                            "description": "Läkemedelstyp",
                            "technicalName": "T1155662",
                            "sortOrder": 0,
                            "compareDescription": "",
                            "isComparable": false
                        },
                        "LM_substans": {
                            "term": 5190,
                            "description": "Endokrin terapisubstanser. Värdedomänskoppling till Substans/Kategori register",
                            "technicalName": "T1169225",
                            "sortOrder": 0,
                            "compareDescription": "",
                            "isComparable": false
                        },
                        "LM_studielkm": {
                            "term": 1004,
                            "description": "Läkemedel, studieläkemedel",
                            "technicalName": "T1204427",
                            "sortOrder": 0,
                            "compareDescription": "",
                            "isComparable": false
                        },
                        "LM_ablatiotestis": {
                            "term": 1004,
                            "description": "Läkemedel, ablatio testis",
                            "technicalName": "T1204426",
                            "sortOrder": 0,
                            "compareDescription": "",
                            "isComparable": false
                        },
                        "LM_ablatiotestisdatum": {
                            "term": 1002,
                            "description": "Läkemedel, ablatio testis datum",
                            "technicalName": "T1204425",
                            "sortOrder": 0,
                            "compareDescription": "",
                            "isComparable": false
                        }
                    },
                    "vdlists": {},
                    "terms": {
                        "1002": {
                            "name": "Datum",
                            "shortName": "Datum",
                            "description": "",
                            "dataType": "datetime",
                            "includeTime": false
                        },
                        "1004": {
                            "name": "Kryssruta",
                            "shortName": "Kryssruta",
                            "description": "",
                            "dataType": "boolean"
                        },
                        "4075": {
                            "name": "Text(8000)",
                            "shortName": "TEXT",
                            "description": "Textvariabel med möjlighet att lagra maximalt antal tecken (8000).",
                            "dataType": "string",
                            "maxLength": 8000,
                            "validCharacters": null
                        },
                        "5190": {
                            "name": "SubstansKategori_VD",
                            "shortName": "substanskategori_vd",
                            "description": "Innehåller värdedomän till tabellen Substans i registret Substans/Kategori",
                            "dataType": "vd"
                        },
                        "5629": {
                            "name": "ProstataProcess_Läkemedelstyp",
                            "shortName": "PP_lakemedelstyp",
                            "description": "",
                            "dataType": "list",
                            "listValues": [
                                {
                                    "id": 17531,
                                    "text": "Endokrin terapi",
                                    "value": "1",
                                    "validFrom": "0001-01-01T00:00:00",
                                    "validTo": "9999-12-31T00:00:00"
                                },
                                {
                                    "id": 17530,
                                    "text": "Cytostatika",
                                    "value": "0",
                                    "validFrom": "0001-01-01T00:00:00",
                                    "validTo": "9999-12-31T00:00:00"
                                },
                                {
                                    "id": 17735,
                                    "text": "Isotopterapi",
                                    "value": "3",
                                    "validFrom": "0001-01-01T00:00:00",
                                    "validTo": "9999-12-31T00:00:00"
                                },
                                {
                                    "id": 17734,
                                    "text": "Benstärkande terapi",
                                    "value": "2",
                                    "validFrom": "0001-01-01T00:00:00",
                                    "validTo": "9999-12-31T00:00:00"
                                },
                                {
                                    "id": 17736,
                                    "text": "Övriga cancerläkemedel",
                                    "value": "4",
                                    "validFrom": "0001-01-01T00:00:00",
                                    "validTo": "9999-12-31T00:00:00"
                                }
                            ]
                        }
                    },
                    "treeLevel": 2
                },
                "LM_Insättning": {
                    "subTables": [
                        "LMI_Händelse",
                        "LMI_Utsättning"
                    ],
                    "regvars": {
                        "LMI_indikation": {
                            "term": 5588,
                            "description": "Indikation för insättning",
                            "technicalName": "T1127292",
                            "sortOrder": 0,
                            "compareDescription": "",
                            "isComparable": false
                        }
                    },
                    "vdlists": {},
                    "terms": {
                        "5588": {
                            "name": "ProstataProcess_Indikation",
                            "shortName": "PP_Indikation",
                            "description": "",
                            "dataType": "list",
                            "listValues": [
                                {
                                    "id": 17426,
                                    "text": "Palliativ",
                                    "value": "0",
                                    "validFrom": "0001-01-01T00:00:00",
                                    "validTo": "9999-12-31T00:00:00"
                                }
                            ]
                        }
                    },
                    "treeLevel": 3
                },
                "LMI_Händelse": {
                    "subTables": [],
                    "regvars": {
                        "LMIH_handelse": {
                            "term": 5589,
                            "description": "Vilken händelse",
                            "technicalName": "T1127294",
                            "sortOrder": 0,
                            "compareDescription": "",
                            "isComparable": false
                        },
                        "LMIH_handelsedatum": {
                            "term": 1002,
                            "description": "Datum för händelse",
                            "technicalName": "T1127293",
                            "sortOrder": 0,
                            "compareDescription": "",
                            "isComparable": false
                        },
                        "LMIH_handelseorsakann": {
                            "term": 4075,
                            "description": "Orsak till händelse, Annan",
                            "technicalName": "T1143285",
                            "sortOrder": 0,
                            "compareDescription": "",
                            "isComparable": false
                        },
                        "LMIH_handelseorsak": {
                            "term": 5590,
                            "description": "Orsak till händelse",
                            "technicalName": "T1218970",
                            "sortOrder": 0,
                            "compareDescription": "",
                            "isComparable": false
                        }
                    },
                    "vdlists": {},
                    "terms": {
                        "1002": {
                            "name": "Datum",
                            "shortName": "Datum",
                            "description": "",
                            "dataType": "datetime",
                            "includeTime": false
                        },
                        "4075": {
                            "name": "Text(8000)",
                            "shortName": "TEXT",
                            "description": "Textvariabel med möjlighet att lagra maximalt antal tecken (8000).",
                            "dataType": "string",
                            "maxLength": 8000,
                            "validCharacters": null
                        },
                        "5589": {
                            "name": "ProstataProcess_Händelse",
                            "shortName": "PP_handelse",
                            "description": "",
                            "dataType": "list",
                            "listValues": [
                                {
                                    "id": 17427,
                                    "text": "Full dos",
                                    "value": "0",
                                    "validFrom": "0001-01-01T00:00:00",
                                    "validTo": "9999-12-31T00:00:00"
                                },
                                {
                                    "id": 17428,
                                    "text": "Reducerad dos",
                                    "value": "1",
                                    "validFrom": "0001-01-01T00:00:00",
                                    "validTo": "9999-12-31T00:00:00"
                                },
                                {
                                    "id": 17429,
                                    "text": "Doseskalering",
                                    "value": "2",
                                    "validFrom": "0001-01-01T00:00:00",
                                    "validTo": "9999-12-31T00:00:00"
                                },
                                {
                                    "id": 17430,
                                    "text": "Paus",
                                    "value": "3",
                                    "validFrom": "0001-01-01T00:00:00",
                                    "validTo": "9999-12-31T00:00:00"
                                }
                            ]
                        },
                        "5590": {
                            "name": "ProstataProcess_HändelseOrsak",
                            "shortName": "PP_handelseorsak",
                            "description": "",
                            "dataType": "list",
                            "listValues": [
                                {
                                    "id": 17465,
                                    "text": "Nyinsättning",
                                    "value": "5",
                                    "validFrom": "0001-01-01T00:00:00",
                                    "validTo": "9999-12-31T00:00:00"
                                },
                                {
                                    "id": 17431,
                                    "text": "Biverkning",
                                    "value": "0",
                                    "validFrom": "0001-01-01T00:00:00",
                                    "validTo": "9999-12-31T00:00:00"
                                },
                                {
                                    "id": 17432,
                                    "text": "Planerat stopp",
                                    "value": "1",
                                    "validFrom": "0001-01-01T00:00:00",
                                    "validTo": "9999-12-31T00:00:00"
                                },
                                {
                                    "id": 17434,
                                    "text": "Patientens önskemål",
                                    "value": "3",
                                    "validFrom": "0001-01-01T00:00:00",
                                    "validTo": "9999-12-31T00:00:00"
                                },
                                {
                                    "id": 18687,
                                    "text": "Strålbehandling",
                                    "value": "2",
                                    "validFrom": "0001-01-01T00:00:00",
                                    "validTo": "9999-12-31T00:00:00"
                                },
                                {
                                    "id": 17435,
                                    "text": "Annan orsak",
                                    "value": "4",
                                    "validFrom": "0001-01-01T00:00:00",
                                    "validTo": "9999-12-31T00:00:00"
                                }
                            ]
                        }
                    },
                    "treeLevel": 4
                },
                "LMI_Utsättning": {
                    "subTables": [],
                    "regvars": {
                        "LMIU_utsattningsdatum": {
                            "term": 1002,
                            "description": "Läkemedel, utsättningsdatum",
                            "technicalName": "T1127296",
                            "sortOrder": 0,
                            "compareDescription": "",
                            "isComparable": false
                        },
                        "LMIU_utsattningsorsak": {
                            "term": 5591,
                            "description": "Läkemedel, utsättningsorsak",
                            "technicalName": "T1127297",
                            "sortOrder": 0,
                            "compareDescription": "",
                            "isComparable": false
                        },
                        "LMIU_utsattningsorsakann": {
                            "term": 4075,
                            "description": "Läkemedel, utsättningsorsak annan",
                            "technicalName": "T1143289",
                            "sortOrder": 0,
                            "compareDescription": "",
                            "isComparable": false
                        }
                    },
                    "vdlists": {},
                    "terms": {
                        "1002": {
                            "name": "Datum",
                            "shortName": "Datum",
                            "description": "",
                            "dataType": "datetime",
                            "includeTime": false
                        },
                        "4075": {
                            "name": "Text(8000)",
                            "shortName": "TEXT",
                            "description": "Textvariabel med möjlighet att lagra maximalt antal tecken (8000).",
                            "dataType": "string",
                            "maxLength": 8000,
                            "validCharacters": null
                        },
                        "5591": {
                            "name": "ProstataProcess_UtsättningOrsak",
                            "shortName": "PP_utsattningorsak",
                            "description": "",
                            "dataType": "list",
                            "listValues": [
                                {
                                    "id": 17436,
                                    "text": "Tumörprogress",
                                    "value": "0",
                                    "validFrom": "0001-01-01T00:00:00",
                                    "validTo": "9999-12-31T00:00:00"
                                },
                                {
                                    "id": 17437,
                                    "text": "Biverkning",
                                    "value": "1",
                                    "validFrom": "0001-01-01T00:00:00",
                                    "validTo": "9999-12-31T00:00:00"
                                },
                                {
                                    "id": 17438,
                                    "text": "Planerat stopp/skifte",
                                    "value": "2",
                                    "validFrom": "0001-01-01T00:00:00",
                                    "validTo": "9999-12-31T00:00:00"
                                },
                                {
                                    "id": 17439,
                                    "text": "Patientens önskemål",
                                    "value": "3",
                                    "validFrom": "0001-01-01T00:00:00",
                                    "validTo": "9999-12-31T00:00:00"
                                },
                                {
                                    "id": 17664,
                                    "text": "Dödsfall",
                                    "value": "5",
                                    "validFrom": "0001-01-01T00:00:00",
                                    "validTo": "9999-12-31T00:00:00"
                                },
                                {
                                    "id": 17440,
                                    "text": "Annan orsak",
                                    "value": "4",
                                    "validFrom": "0001-01-01T00:00:00",
                                    "validTo": "9999-12-31T00:00:00"
                                }
                            ]
                        }
                    },
                    "treeLevel": 4
                },
                "Strålbehandling": {
                    "subTables": [
                        "SB_Grupp"
                    ],
                    "regvars": {
                        "SB_slutdatum": {
                            "term": 1002,
                            "description": "Slutdatum för strålbehandling",
                            "technicalName": "T1123084",
                            "sortOrder": 0,
                            "compareDescription": "",
                            "isComparable": false
                        },
                        "SB_frakdos2": {
                            "term": 1040,
                            "description": "Dos per fraktion (Gy) för strålbehandling",
                            "technicalName": "T1123086",
                            "sortOrder": 0,
                            "compareDescription": "",
                            "isComparable": false
                        },
                        "SB_slutdos2": {
                            "term": 1040,
                            "description": "Slutdos (Gy) för strålbehandling",
                            "technicalName": "T1123085",
                            "sortOrder": 0,
                            "compareDescription": "",
                            "isComparable": false
                        }
                    },
                    "vdlists": {},
                    "terms": {
                        "1002": {
                            "name": "Datum",
                            "shortName": "Datum",
                            "description": "",
                            "dataType": "datetime",
                            "includeTime": false
                        },
                        "1040": {
                            "name": "Heltal",
                            "shortName": "Heltal",
                            "description": "",
                            "dataType": "integer",
                            "min": null,
                            "max": null
                        }
                    },
                    "treeLevel": 2
                },
                "SB_Grupp": {
                    "subTables": [
                        "SBG_Lokal"
                    ],
                    "regvars": {
                        "SBG_slutdos": {
                            "term": 1040,
                            "description": "Slutdos (Gy) för strålbehandling",
                            "technicalName": "T1266632",
                            "sortOrder": 0,
                            "compareDescription": "",
                            "isComparable": false
                        },
                        "SBG_frakdos": {
                            "term": 1040,
                            "description": "Strålbehandling, dos per fraktion",
                            "technicalName": "T1266633",
                            "sortOrder": 0,
                            "compareDescription": "",
                            "isComparable": false
                        },
                        "SBL_lokal2": {
                            "term": 5621,
                            "description": "Lokal för strålbehandling",
                            "technicalName": "T1155630",
                            "sortOrder": 0,
                            "compareDescription": "",
                            "isComparable": false
                        },
                        "SBL_lokalann2": {
                            "term": 4075,
                            "description": "Lokal för strålbehandling, Vilken annan lokal?",
                            "technicalName": "T1143284",
                            "sortOrder": 0,
                            "compareDescription": "",
                            "isComparable": false
                        },
                        "SBL_lokalskelett2": {
                            "term": 4075,
                            "description": "Lokal för strålbehandling, Vilken skelett lokal?",
                            "technicalName": "T1155626",
                            "sortOrder": 0,
                            "compareDescription": "",
                            "isComparable": false
                        }
                    },
                    "vdlists": {},
                    "terms": {
                        "1040": {
                            "name": "Heltal",
                            "shortName": "Heltal",
                            "description": "",
                            "dataType": "integer",
                            "min": null,
                            "max": null
                        },
                        "4075": {
                            "name": "Text(8000)",
                            "shortName": "TEXT",
                            "description": "Textvariabel med möjlighet att lagra maximalt antal tecken (8000).",
                            "dataType": "string",
                            "maxLength": 8000,
                            "validCharacters": null
                        },
                        "5621": {
                            "name": "ProstataProcess_SBLokal",
                            "shortName": "PP_sblokal",
                            "description": "",
                            "dataType": "list",
                            "listValues": [
                                {
                                    "id": 17505,
                                    "text": "Skelett",
                                    "value": "2",
                                    "validFrom": "0001-01-01T00:00:00",
                                    "validTo": "9999-12-31T00:00:00"
                                },
                                {
                                    "id": 17504,
                                    "text": "Prostata",
                                    "value": "3",
                                    "validFrom": "0001-01-01T00:00:00",
                                    "validTo": "9999-12-31T00:00:00"
                                },
                                {
                                    "id": 17501,
                                    "text": "Blåsa",
                                    "value": "4",
                                    "validFrom": "0001-01-01T00:00:00",
                                    "validTo": "9999-12-31T00:00:00"
                                },
                                {
                                    "id": 17503,
                                    "text": "Lymfkörtlar",
                                    "value": "6",
                                    "validFrom": "0001-01-01T00:00:00",
                                    "validTo": "9999-12-31T00:00:00"
                                },
                                {
                                    "id": 17502,
                                    "text": "Hjärna",
                                    "value": "0",
                                    "validFrom": "0001-01-01T00:00:00",
                                    "validTo": "9999-12-31T00:00:00"
                                },
                                {
                                    "id": 17506,
                                    "text": "Annan",
                                    "value": "5",
                                    "validFrom": "0001-01-01T00:00:00",
                                    "validTo": "9999-12-31T00:00:00"
                                }
                            ]
                        }
                    },
                    "treeLevel": 3
                },
                "SBG_Lokal": {
                    "subTables": [],
                    "regvars": {
                        "SBGL_lokal": {
                            "term": 5621,
                            "description": "Lokal för strålbehandling",
                            "technicalName": "T1266637",
                            "sortOrder": 0,
                            "compareDescription": "",
                            "isComparable": false
                        },
                        "SBGL_lokalann": {
                            "term": 4075,
                            "description": "Lokal för strålbehandling, Vilken annan lokal?",
                            "technicalName": "T1266634",
                            "sortOrder": 0,
                            "compareDescription": "",
                            "isComparable": false
                        },
                        "SBGL_lokalskelett": {
                            "term": 4075,
                            "description": "Lokal för strålbehandling, Vilken skelett lokal?",
                            "technicalName": "T1266635",
                            "sortOrder": 0,
                            "compareDescription": "",
                            "isComparable": false
                        }
                    },
                    "vdlists": {},
                    "terms": {
                        "4075": {
                            "name": "Text(8000)",
                            "shortName": "TEXT",
                            "description": "Textvariabel med möjlighet att lagra maximalt antal tecken (8000).",
                            "dataType": "string",
                            "maxLength": 8000,
                            "validCharacters": null
                        },
                        "5621": {
                            "name": "ProstataProcess_SBLokal",
                            "shortName": "PP_sblokal",
                            "description": "",
                            "dataType": "list",
                            "listValues": [
                                {
                                    "id": 17505,
                                    "text": "Skelett",
                                    "value": "2",
                                    "validFrom": "0001-01-01T00:00:00",
                                    "validTo": "9999-12-31T00:00:00"
                                },
                                {
                                    "id": 17504,
                                    "text": "Prostata",
                                    "value": "3",
                                    "validFrom": "0001-01-01T00:00:00",
                                    "validTo": "9999-12-31T00:00:00"
                                },
                                {
                                    "id": 17501,
                                    "text": "Blåsa",
                                    "value": "4",
                                    "validFrom": "0001-01-01T00:00:00",
                                    "validTo": "9999-12-31T00:00:00"
                                },
                                {
                                    "id": 17503,
                                    "text": "Lymfkörtlar",
                                    "value": "6",
                                    "validFrom": "0001-01-01T00:00:00",
                                    "validTo": "9999-12-31T00:00:00"
                                },
                                {
                                    "id": 17502,
                                    "text": "Hjärna",
                                    "value": "0",
                                    "validFrom": "0001-01-01T00:00:00",
                                    "validTo": "9999-12-31T00:00:00"
                                },
                                {
                                    "id": 17506,
                                    "text": "Annan",
                                    "value": "5",
                                    "validFrom": "0001-01-01T00:00:00",
                                    "validTo": "9999-12-31T00:00:00"
                                }
                            ]
                        }
                    },
                    "treeLevel": 4
                },
                "Bidragande": {
                    "subTables": [],
                    "regvars": {
                        "BI_datum": {
                            "term": 1002,
                            "description": "Datum då ansökan för bidragande klinik gjorts",
                            "technicalName": "T1421910",
                            "sortOrder": 0,
                            "compareDescription": "Datum då ansökan för bidragande klinik gjorts",
                            "isComparable": false
                        },
                        "BI_godkannande": {
                            "term": 1004,
                            "description": "Godkännande av att lagar och regler kring patientsäkerhet har getts",
                            "technicalName": "T1421911",
                            "sortOrder": 0,
                            "compareDescription": "Godkännande av att lagar och regler kring patientsäkerhet har getts",
                            "isComparable": false
                        },
                        "BI_inrappenh": {
                            "term": 4075,
                            "description": "Inrapporterande enhet som gör ansökan om bidragande klinik",
                            "technicalName": "T1421912",
                            "sortOrder": 0,
                            "compareDescription": "Inrapporterande enhet som gör ansökan om bidragande klinik",
                            "isComparable": false
                        },
                        "BI_inrapp": {
                            "term": 4075,
                            "description": "Inrapportör som gör ansökan för kliniken för att bli bidragande klinik",
                            "technicalName": "T1421913",
                            "sortOrder": 0,
                            "compareDescription": "Inrapportör som gör ansökan för kliniken för att bli bidragande klinik",
                            "isComparable": false
                        },
                        "BI_kommentar": {
                            "term": 4075,
                            "description": "Övriga kommentarer/upplysningar",
                            "technicalName": "T1421914",
                            "sortOrder": 0,
                            "compareDescription": "Övriga kommentarer/upplysningar",
                            "isComparable": false
                        },
                        "BI_sjukklin": {
                            "term": 4075,
                            "description": "Klinikkod för bidragande klinik",
                            "technicalName": "T1598049",
                            "sortOrder": 0,
                            "compareDescription": "Klinikkod för bidragande klinik",
                            "isComparable": false
                        },
                        "BI_sjukkod": {
                            "term": 4075,
                            "description": "Sjukhuskod för bidragande klinik",
                            "technicalName": "T1598050",
                            "sortOrder": 0,
                            "compareDescription": "Sjukhuskod för bidragande klinik",
                            "isComparable": false
                        }
                    },
                    "vdlists": {},
                    "terms": {
                        "1002": {
                            "name": "Datum",
                            "shortName": "Datum",
                            "description": "",
                            "dataType": "datetime",
                            "includeTime": false
                        },
                        "1004": {
                            "name": "Kryssruta",
                            "shortName": "Kryssruta",
                            "description": "",
                            "dataType": "boolean"
                        },
                        "4075": {
                            "name": "Text(8000)",
                            "shortName": "TEXT",
                            "description": "Textvariabel med möjlighet att lagra maximalt antal tecken (8000).",
                            "dataType": "string",
                            "maxLength": 8000,
                            "validCharacters": null
                        }
                    },
                    "treeLevel": 2
                },
                "PROM": {
                    "subTables": [],
                    "regvars": {
                        "PROM_datum": {
                            "term": 1002,
                            "description": "Datum då enkät fyllts i",
                            "technicalName": "T1632910",
                            "sortOrder": 0,
                            "compareDescription": "Datum då enkät fyllts i",
                            "isComparable": false
                        },
                        "PROM_fr1": {
                            "term": 1040,
                            "description": "Ändrad smakupplevelse",
                            "technicalName": "T1632911",
                            "sortOrder": 0,
                            "compareDescription": "Fråga 1",
                            "isComparable": false
                        },
                        "PROM_fr2": {
                            "term": 1040,
                            "description": "Sexuell lust",
                            "technicalName": "T1632912",
                            "sortOrder": 0,
                            "compareDescription": "Fråga 3",
                            "isComparable": false
                        },
                        "PROM_fr3": {
                            "term": 1040,
                            "description": "Svettningar",
                            "technicalName": "T1632913",
                            "sortOrder": 0,
                            "compareDescription": "Fråga 3",
                            "isComparable": false
                        },
                        "PROM_fr4": {
                            "term": 1040,
                            "description": "Andfåddhet",
                            "technicalName": "T1632914",
                            "sortOrder": 0,
                            "compareDescription": "Fråga 4",
                            "isComparable": false
                        },
                        "PROM_fr5": {
                            "term": 1040,
                            "description": "Orkeslöshet",
                            "technicalName": "T1632915",
                            "sortOrder": 0,
                            "compareDescription": "Fråga 5",
                            "isComparable": false
                        },
                        "PROM_fr6": {
                            "term": 1040,
                            "description": "Irritation",
                            "technicalName": "T1632916",
                            "sortOrder": 0,
                            "compareDescription": "Fråga 6",
                            "isComparable": false
                        },
                        "PROM_fr7": {
                            "term": 1040,
                            "description": "Svårt att kissa",
                            "technicalName": "T1632917",
                            "sortOrder": 0,
                            "compareDescription": "Fråga 7",
                            "isComparable": false
                        },
                        "PROM_fr8": {
                            "term": 1040,
                            "description": "Sömnsvårigheter",
                            "technicalName": "T1632918",
                            "sortOrder": 0,
                            "compareDescription": "Fråga 8",
                            "isComparable": false
                        },
                        "PROM_fr9": {
                            "term": 1040,
                            "description": "Viktnedgång",
                            "technicalName": "T1632919",
                            "sortOrder": 0,
                            "compareDescription": "Fråga 9",
                            "isComparable": false
                        },
                        "PROM_fr10": {
                            "term": 1040,
                            "description": "Upplevd smärta",
                            "technicalName": "T1632920",
                            "sortOrder": 0,
                            "compareDescription": "Fråga 10",
                            "isComparable": false
                        },
                        "PROM_fr11": {
                            "term": 1040,
                            "description": "Diarré",
                            "technicalName": "T1632920",
                            "sortOrder": 0,
                            "compareDescription": "Fråga 10",
                            "isComparable": false
                        },
                        "PROM_fr12": {
                            "term": 1040,
                            "description": "Förstoppning",
                            "technicalName": "T1632920",
                            "sortOrder": 0,
                            "compareDescription": "Fråga 10",
                            "isComparable": false
                        },
                        "PROM_fr13": {
                            "term": 1040,
                            "description": "Illamående",
                            "technicalName": "T1632920",
                            "sortOrder": 0,
                            "compareDescription": "Fråga 10",
                            "isComparable": false
                        },
                        "PROM_fr14": {
                            "term": 1040,
                            "description": "Dåsighet",
                            "technicalName": "T1632920",
                            "sortOrder": 0,
                            "compareDescription": "Fråga 10",
                            "isComparable": false
                        },
                        "PROM_fr15": {
                            "term": 1040,
                            "description": "Nedstämdhet",
                            "technicalName": "T1632920",
                            "sortOrder": 0,
                            "compareDescription": "Fråga 10",
                            "isComparable": false
                        },
                        "PROM_fr16": {
                            "term": 1040,
                            "description": "Oro",
                            "technicalName": "T1632920",
                            "sortOrder": 0,
                            "compareDescription": "Fråga 10",
                            "isComparable": false
                        }
                    },
                    "vdlists": {},
                    "terms": {
                        "1002": {
                            "name": "Datum",
                            "shortName": "Datum",
                            "description": "",
                            "dataType": "datetime",
                            "includeTime": false
                        },
                        "1040": {
                            "name": "Heltal",
                            "shortName": "Heltal",
                            "description": "",
                            "dataType": "integer",
                            "min": 0,
                            "max": 4
                        }
                    },
                    "treeLevel": 2
                }
            },
            "data": {
                "id": 111,
                "subTables": {
                    "Bilddiagnostik": [
                        
                    ],
                    "Labbprov": [
                        {
                            "id": 616,
                            "subTables": {},
                            "regvars": {
                                "LP_datum": {
                                    "value": "2013-03-11",
                                    "include": true
                                },
                                "LP_hb": {
                                    "value": 130,
                                    "include": true
                                },
                                "LP_psa": {
                                    "value": 95,
                                    "include": true
                                },
                                "LP_krea": {
                                    "value": 60,
                                    "include": true
                                },
                                "LP_alp": {
                                    "value": 1.5,
                                    "include": true
                                },
                                "LP_testosteron": {
                                    "value": null,
                                    "include": true
                                }
                            },
                            "vdlists": {}
                        },
                        {
                            "id": 617,
                            "subTables": {},
                            "regvars": {
                                "LP_datum": {
                                    "value": "2013-09-13",
                                    "include": true
                                },
                                "LP_hb": {
                                    "value": 132,
                                    "include": true
                                },
                                "LP_psa": {
                                    "value": 261,
                                    "include": true
                                },
                                "LP_krea": {
                                    "value": 60,
                                    "include": true
                                },
                                "LP_alp": {
                                    "value": 0.9,
                                    "include": true
                                },
                                "LP_testosteron": {
                                    "value": null,
                                    "include": true
                                }
                            },
                            "vdlists": {}
                        },
                        {
                            "id": 618,
                            "subTables": {},
                            "regvars": {
                                "LP_datum": {
                                    "value": "2013-12-06",
                                    "include": true
                                },
                                "LP_hb": {
                                    "value": 118,
                                    "include": true
                                },
                                "LP_psa": {
                                    "value": 148,
                                    "include": true
                                },
                                "LP_krea": {
                                    "value": 64,
                                    "include": true
                                },
                                "LP_alp": {
                                    "value": 1.1,
                                    "include": true
                                },
                                "LP_testosteron": {
                                    "value": null,
                                    "include": true
                                }
                            },
                            "vdlists": {}
                        },
                        {
                            "id": 619,
                            "subTables": {},
                            "regvars": {
                                "LP_datum": {
                                    "value": "2014-04-16",
                                    "include": true
                                },
                                "LP_hb": {
                                    "value": 122,
                                    "include": true
                                },
                                "LP_psa": {
                                    "value": 64,
                                    "include": true
                                },
                                "LP_krea": {
                                    "value": 66,
                                    "include": true
                                },
                                "LP_alp": {
                                    "value": 1.5,
                                    "include": true
                                },
                                "LP_testosteron": {
                                    "value": null,
                                    "include": true
                                }
                            },
                            "vdlists": {}
                        },
                        {
                            "id": 622,
                            "subTables": {},
                            "regvars": {
                                "LP_datum": {
                                    "value": "2014-08-31",
                                    "include": true
                                },
                                "LP_hb": {
                                    "value": 135,
                                    "include": true
                                },
                                "LP_psa": {
                                    "value": 130,
                                    "include": true
                                },
                                "LP_krea": {
                                    "value": 51,
                                    "include": true
                                },
                                "LP_alp": {
                                    "value": 1.2,
                                    "include": true
                                },
                                "LP_testosteron": {
                                    "value": null,
                                    "include": true
                                }
                            },
                            "vdlists": {}
                        },
                        {
                            "id": 627,
                            "subTables": {},
                            "regvars": {
                                "LP_datum": {
                                    "value": "2014-11-11",
                                    "include": true
                                },
                                "LP_hb": {
                                    "value": null,
                                    "include": true
                                },
                                "LP_psa": {
                                    "value": 221,
                                    "include": true
                                },
                                "LP_krea": {
                                    "value": null,
                                    "include": true
                                },
                                "LP_alp": {
                                    "value": null,
                                    "include": true
                                },
                                "LP_testosteron": {
                                    "value": null,
                                    "include": true
                                }
                            },
                            "vdlists": {}
                        },
                        {
                            "id": 628,
                            "subTables": {},
                            "regvars": {
                                "LP_datum": {
                                    "value": "2014-12-11",
                                    "include": true
                                },
                                "LP_hb": {
                                    "value": null,
                                    "include": true
                                },
                                "LP_psa": {
                                    "value": 143,
                                    "include": true
                                },
                                "LP_krea": {
                                    "value": null,
                                    "include": true
                                },
                                "LP_alp": {
                                    "value": null,
                                    "include": true
                                },
                                "LP_testosteron": {
                                    "value": null,
                                    "include": true
                                }
                            },
                            "vdlists": {}
                        },
                        {
                            "id": 629,
                            "subTables": {},
                            "regvars": {
                                "LP_datum": {
                                    "value": "2015-02-25",
                                    "include": true
                                },
                                "LP_hb": {
                                    "value": null,
                                    "include": true
                                },
                                "LP_psa": {
                                    "value": 91,
                                    "include": true
                                },
                                "LP_krea": {
                                    "value": null,
                                    "include": true
                                },
                                "LP_alp": {
                                    "value": null,
                                    "include": true
                                },
                                "LP_testosteron": {
                                    "value": null,
                                    "include": true
                                }
                            },
                            "vdlists": {}
                        },
                        {
                            "id": 630,
                            "subTables": {},
                            "regvars": {
                                "LP_datum": {
                                    "value": "2015-04-22",
                                    "include": true
                                },
                                "LP_hb": {
                                    "value": null,
                                    "include": true
                                },
                                "LP_psa": {
                                    "value": 71,
                                    "include": true
                                },
                                "LP_krea": {
                                    "value": null,
                                    "include": true
                                },
                                "LP_alp": {
                                    "value": null,
                                    "include": true
                                },
                                "LP_testosteron": {
                                    "value": null,
                                    "include": true
                                }
                            },
                            "vdlists": {}
                        },
                        {
                            "id": 631,
                            "subTables": {},
                            "regvars": {
                                "LP_datum": {
                                    "value": "2015-07-24",
                                    "include": true
                                },
                                "LP_hb": {
                                    "value": null,
                                    "include": true
                                },
                                "LP_psa": {
                                    "value": 102,
                                    "include": true
                                },
                                "LP_krea": {
                                    "value": null,
                                    "include": true
                                },
                                "LP_alp": {
                                    "value": null,
                                    "include": true
                                },
                                "LP_testosteron": {
                                    "value": null,
                                    "include": true
                                }
                            },
                            "vdlists": {}
                        },
                        {
                            "id": 1061,
                            "subTables": {},
                            "regvars": {
                                "LP_datum": {
                                    "value": "2016-06-13",
                                    "include": true
                                },
                                "LP_hb": {
                                    "value": 107,
                                    "include": true
                                },
                                "LP_psa": {
                                    "value": 122,
                                    "include": true
                                },
                                "LP_krea": {
                                    "value": 55,
                                    "include": true
                                },
                                "LP_alp": {
                                    "value": 2,
                                    "include": true
                                },
                                "LP_testosteron": {
                                    "value": 2.08,
                                    "include": true
                                }
                            },
                            "vdlists": {}
                        },
                        {
                            "id": 2536,
                            "subTables": {},
                            "regvars": {
                                "LP_datum": {
                                    "value": "2016-10-01",
                                    "include": true
                                },
                                "LP_hb": {
                                    "value": null,
                                    "include": true
                                },
                                "LP_psa": {
                                    "value": null,
                                    "include": true
                                },
                                "LP_krea": {
                                    "value": 55,
                                    "include": true
                                },
                                "LP_alp": {
                                    "value": null,
                                    "include": true
                                },
                                "LP_testosteron": {
                                    "value": null,
                                    "include": true
                                }
                            },
                            "vdlists": {}
                        }
                    ],
                    "Besök": [
                        {
                            "id": 315,
                            "subTables": {},
                            "regvars": {
                                "B_besokdatum": {
                                    "value": "2013-03-15",
                                    "include": true
                                },
                                "B_biobank": {
                                    "value": 17406,
                                    "include": true
                                },
                                "B_ecogwho": {
                                    "value": 17412,
                                    "include": true
                                },
                                "B_studie": {
                                    "value": 17406,
                                    "include": true
                                },
                                "B_klinbed": {
                                    "value": 17410,
                                    "include": true
                                },
                                "B_lakare": {
                                    "value": "Testläkare",
                                    "include": true
                                },
                                "B_studietxt": {
                                    "value": null,
                                    "include": true
                                },
                                "B_multikonf": {
                                    "value": 17406,
                                    "include": true
                                },
                                "B_crpc": {
                                    "value": null,
                                    "include": true
                                },
                                "B_palliativvard": {
                                    "value": null,
                                    "include": true
                                },
                                "B_klinEjBed": {
                                    "value": null,
                                    "include": true
                                },
                                "B_biobankprovtyp": {
                                    "value": null,
                                    "include": true
                                },
                                "B_biobankDatum": {
                                    "value": null,
                                    "include": true
                                }
                            },
                            "vdlists": {}
                        },
                        {
                            "id": 316,
                            "subTables": {},
                            "regvars": {
                                "B_besokdatum": {
                                    "value": "2012-09-30",
                                    "include": true
                                },
                                "B_biobank": {
                                    "value": 17406,
                                    "include": true
                                },
                                "B_ecogwho": {
                                    "value": 17412,
                                    "include": true
                                },
                                "B_studie": {
                                    "value": 17406,
                                    "include": true
                                },
                                "B_klinbed": {
                                    "value": 18264,
                                    "include": true
                                },
                                "B_lakare": {
                                    "value": "testdoktor",
                                    "include": true
                                },
                                "B_studietxt": {
                                    "value": null,
                                    "include": true
                                },
                                "B_multikonf": {
                                    "value": 17406,
                                    "include": true
                                },
                                "B_crpc": {
                                    "value": null,
                                    "include": true
                                },
                                "B_palliativvard": {
                                    "value": 17407,
                                    "include": true
                                },
                                "B_klinEjBed": {
                                    "value": null,
                                    "include": true
                                },
                                "B_biobankprovtyp": {
                                    "value": null,
                                    "include": true
                                },
                                "B_biobankDatum": {
                                    "value": null,
                                    "include": true
                                }
                            },
                            "vdlists": {}
                        },
                        {
                            "id": 317,
                            "subTables": {},
                            "regvars": {
                                "B_besokdatum": {
                                    "value": "2013-09-13",
                                    "include": true
                                },
                                "B_biobank": {
                                    "value": 17406,
                                    "include": true
                                },
                                "B_ecogwho": {
                                    "value": 17412,
                                    "include": true
                                },
                                "B_studie": {
                                    "value": 17407,
                                    "include": true
                                },
                                "B_klinbed": {
                                    "value": 17410,
                                    "include": true
                                },
                                "B_lakare": {
                                    "value": "test",
                                    "include": true
                                },
                                "B_studietxt": {
                                    "value": "firstana",
                                    "include": true
                                },
                                "B_multikonf": {
                                    "value": 17406,
                                    "include": true
                                },
                                "B_crpc": {
                                    "value": null,
                                    "include": true
                                },
                                "B_palliativvard": {
                                    "value": null,
                                    "include": true
                                },
                                "B_klinEjBed": {
                                    "value": null,
                                    "include": true
                                },
                                "B_biobankprovtyp": {
                                    "value": null,
                                    "include": true
                                },
                                "B_biobankDatum": {
                                    "value": null,
                                    "include": true
                                }
                            },
                            "vdlists": {}
                        },
                        {
                            "id": 318,
                            "subTables": {},
                            "regvars": {
                                "B_besokdatum": {
                                    "value": "2013-12-06",
                                    "include": true
                                },
                                "B_biobank": {
                                    "value": 17406,
                                    "include": true
                                },
                                "B_ecogwho": {
                                    "value": 17412,
                                    "include": true
                                },
                                "B_studie": {
                                    "value": 17406,
                                    "include": true
                                },
                                "B_klinbed": {
                                    "value": 17408,
                                    "include": true
                                },
                                "B_lakare": {
                                    "value": "test",
                                    "include": true
                                },
                                "B_studietxt": {
                                    "value": null,
                                    "include": true
                                },
                                "B_multikonf": {
                                    "value": 17406,
                                    "include": true
                                },
                                "B_crpc": {
                                    "value": null,
                                    "include": true
                                },
                                "B_palliativvard": {
                                    "value": null,
                                    "include": true
                                },
                                "B_klinEjBed": {
                                    "value": null,
                                    "include": true
                                },
                                "B_biobankprovtyp": {
                                    "value": null,
                                    "include": true
                                },
                                "B_biobankDatum": {
                                    "value": null,
                                    "include": true
                                }
                            },
                            "vdlists": {}
                        },
                        {
                            "id": 319,
                            "subTables": {},
                            "regvars": {
                                "B_besokdatum": {
                                    "value": "2014-04-17",
                                    "include": true
                                },
                                "B_biobank": {
                                    "value": 17406,
                                    "include": true
                                },
                                "B_ecogwho": {
                                    "value": 17413,
                                    "include": true
                                },
                                "B_studie": {
                                    "value": 17406,
                                    "include": true
                                },
                                "B_klinbed": {
                                    "value": 17408,
                                    "include": true
                                },
                                "B_lakare": {
                                    "value": "test4",
                                    "include": true
                                },
                                "B_studietxt": {
                                    "value": null,
                                    "include": true
                                },
                                "B_multikonf": {
                                    "value": 17406,
                                    "include": true
                                },
                                "B_crpc": {
                                    "value": null,
                                    "include": true
                                },
                                "B_palliativvard": {
                                    "value": null,
                                    "include": true
                                },
                                "B_klinEjBed": {
                                    "value": null,
                                    "include": true
                                },
                                "B_biobankprovtyp": {
                                    "value": null,
                                    "include": true
                                },
                                "B_biobankDatum": {
                                    "value": null,
                                    "include": true
                                }
                            },
                            "vdlists": {}
                        },
                        {
                            "id": 323,
                            "subTables": {},
                            "regvars": {
                                "B_besokdatum": {
                                    "value": "2014-09-04",
                                    "include": true
                                },
                                "B_biobank": {
                                    "value": 17406,
                                    "include": true
                                },
                                "B_ecogwho": {
                                    "value": 17413,
                                    "include": true
                                },
                                "B_studie": {
                                    "value": 17407,
                                    "include": true
                                },
                                "B_klinbed": {
                                    "value": 17410,
                                    "include": true
                                },
                                "B_lakare": {
                                    "value": "test4",
                                    "include": true
                                },
                                "B_studietxt": {
                                    "value": "concab",
                                    "include": true
                                },
                                "B_multikonf": {
                                    "value": 17406,
                                    "include": true
                                },
                                "B_crpc": {
                                    "value": null,
                                    "include": true
                                },
                                "B_palliativvard": {
                                    "value": null,
                                    "include": true
                                },
                                "B_klinEjBed": {
                                    "value": null,
                                    "include": true
                                },
                                "B_biobankprovtyp": {
                                    "value": null,
                                    "include": true
                                },
                                "B_biobankDatum": {
                                    "value": null,
                                    "include": true
                                }
                            },
                            "vdlists": {}
                        },
                        {
                            "id": 329,
                            "subTables": {},
                            "regvars": {
                                "B_besokdatum": {
                                    "value": "2015-02-25",
                                    "include": true
                                },
                                "B_biobank": {
                                    "value": 17406,
                                    "include": true
                                },
                                "B_ecogwho": {
                                    "value": 17412,
                                    "include": true
                                },
                                "B_studie": {
                                    "value": 17406,
                                    "include": true
                                },
                                "B_klinbed": {
                                    "value": 17408,
                                    "include": true
                                },
                                "B_lakare": {
                                    "value": "testdoktor",
                                    "include": true
                                },
                                "B_studietxt": {
                                    "value": null,
                                    "include": true
                                },
                                "B_multikonf": {
                                    "value": 17406,
                                    "include": true
                                },
                                "B_crpc": {
                                    "value": null,
                                    "include": true
                                },
                                "B_palliativvard": {
                                    "value": null,
                                    "include": true
                                },
                                "B_klinEjBed": {
                                    "value": null,
                                    "include": true
                                },
                                "B_biobankprovtyp": {
                                    "value": null,
                                    "include": true
                                },
                                "B_biobankDatum": {
                                    "value": null,
                                    "include": true
                                }
                            },
                            "vdlists": {}
                        },
                        {
                            "id": 330,
                            "subTables": {},
                            "regvars": {
                                "B_besokdatum": {
                                    "value": "2015-05-14",
                                    "include": true
                                },
                                "B_biobank": {
                                    "value": 17406,
                                    "include": true
                                },
                                "B_ecogwho": {
                                    "value": 17413,
                                    "include": true
                                },
                                "B_studie": {
                                    "value": 17406,
                                    "include": true
                                },
                                "B_klinbed": {
                                    "value": 17408,
                                    "include": true
                                },
                                "B_lakare": {
                                    "value": "testdoktor",
                                    "include": true
                                },
                                "B_studietxt": {
                                    "value": null,
                                    "include": true
                                },
                                "B_multikonf": {
                                    "value": 17406,
                                    "include": true
                                },
                                "B_crpc": {
                                    "value": null,
                                    "include": true
                                },
                                "B_palliativvard": {
                                    "value": null,
                                    "include": true
                                },
                                "B_klinEjBed": {
                                    "value": null,
                                    "include": true
                                },
                                "B_biobankprovtyp": {
                                    "value": null,
                                    "include": true
                                },
                                "B_biobankDatum": {
                                    "value": null,
                                    "include": true
                                }
                            },
                            "vdlists": {}
                        },
                        {
                            "id": 331,
                            "subTables": {},
                            "regvars": {
                                "B_besokdatum": {
                                    "value": "2015-07-24",
                                    "include": true
                                },
                                "B_biobank": {
                                    "value": 17406,
                                    "include": true
                                },
                                "B_ecogwho": {
                                    "value": 17413,
                                    "include": true
                                },
                                "B_studie": {
                                    "value": 17406,
                                    "include": true
                                },
                                "B_klinbed": {
                                    "value": 17410,
                                    "include": true
                                },
                                "B_lakare": {
                                    "value": "test4",
                                    "include": true
                                },
                                "B_studietxt": {
                                    "value": null,
                                    "include": true
                                },
                                "B_multikonf": {
                                    "value": 17407,
                                    "include": true
                                },
                                "B_crpc": {
                                    "value": null,
                                    "include": true
                                },
                                "B_palliativvard": {
                                    "value": null,
                                    "include": true
                                },
                                "B_klinEjBed": {
                                    "value": null,
                                    "include": true
                                },
                                "B_biobankprovtyp": {
                                    "value": null,
                                    "include": true
                                },
                                "B_biobankDatum": {
                                    "value": null,
                                    "include": true
                                }
                            },
                            "vdlists": {}
                        },
                        {
                            "id": 569,
                            "subTables": {},
                            "regvars": {
                                "B_besokdatum": {
                                    "value": "2016-06-13",
                                    "include": true
                                },
                                "B_biobank": {
                                    "value": 17406,
                                    "include": true
                                },
                                "B_ecogwho": {
                                    "value": 17414,
                                    "include": true
                                },
                                "B_studie": {
                                    "value": null,
                                    "include": true
                                },
                                "B_klinbed": {
                                    "value": null,
                                    "include": true
                                },
                                "B_lakare": {
                                    "value": "Marie Hjälm Eriksson",
                                    "include": true
                                },
                                "B_studietxt": {
                                    "value": null,
                                    "include": true
                                },
                                "B_multikonf": {
                                    "value": 17406,
                                    "include": true
                                },
                                "B_crpc": {
                                    "value": null,
                                    "include": true
                                },
                                "B_palliativvard": {
                                    "value": 17406,
                                    "include": true
                                },
                                "B_klinEjBed": {
                                    "value": true,
                                    "include": true
                                },
                                "B_biobankprovtyp": {
                                    "value": null,
                                    "include": true
                                },
                                "B_biobankDatum": {
                                    "value": null,
                                    "include": true
                                }
                            },
                            "vdlists": {}
                        }
                    ],
                    "SRE": [
                        {
                            "id": 283,
                            "subTables": {
                                "SRE_Händelse": [
                                    {
                                        "id": 260,
                                        "subTables": {},
                                        "regvars": {
                                            "SREH_handelse": {
                                                "value": 17419,
                                                "include": true
                                            }
                                        },
                                        "vdlists": {}
                                    },
                                    {
                                        "id": 266,
                                        "subTables": {},
                                        "regvars": {
                                            "SREH_handelse": {
                                                "value": 17419,
                                                "include": true
                                            }
                                        },
                                        "vdlists": {}
                                    }
                                ]
                            },
                            "regvars": {
                                "SRE_datum": {
                                    "value": "2016-06-04",
                                    "include": true
                                }
                            },
                            "vdlists": {}
                        }
                    ],
                    "Läkemedel": [
                        {
                            "id": 245,
                            "subTables": {
                                "LM_Insättning": [
                                    {
                                        "id": 296,
                                        "subTables": {
                                            "LMI_Händelse": [
                                                {
                                                    "id": 421,
                                                    "subTables": {},
                                                    "regvars": {
                                                        "LMIH_handelse": {
                                                            "value": 17427,
                                                            "include": true
                                                        },
                                                        "LMIH_handelsedatum": {
                                                            "value": "2013-03-30",
                                                            "include": true
                                                        },
                                                        "LMIH_handelseorsakann": {
                                                            "value": null,
                                                            "include": true
                                                        },
                                                        "LMIH_handelseorsak": {
                                                            "value": 17465,
                                                            "include": true
                                                        }
                                                    },
                                                    "vdlists": {}
                                                }
                                            ],
                                            "LMI_Utsättning": []
                                        },
                                        "regvars": {
                                            "LMI_indikation": {
                                                "value": 17426,
                                                "include": true
                                            }
                                        },
                                        "vdlists": {}
                                    }
                                ]
                            },
                            "regvars": {
                                "LM_studielkmtxt": {
                                    "value": null,
                                    "include": true
                                },
                                "LM_typ": {
                                    "value": 17531,
                                    "include": true
                                },
                                "LM_substans": {
                                    "value": 2289,
                                    "include": true
                                },
                                "LM_studielkm": {
                                    "value": null,
                                    "include": true
                                },
                                "LM_ablatiotestis": {
                                    "value": null,
                                    "include": true
                                },
                                "LM_ablatiotestisdatum": {
                                    "value": null,
                                    "include": true
                                }
                            },
                            "vdlists": {}
                        },
                        {
                            "id": 246,
                            "subTables": {
                                "LM_Insättning": [
                                    {
                                        "id": 297,
                                        "subTables": {
                                            "LMI_Händelse": [
                                                {
                                                    "id": 422,
                                                    "subTables": {},
                                                    "regvars": {
                                                        "LMIH_handelse": {
                                                            "value": 17427,
                                                            "include": true
                                                        },
                                                        "LMIH_handelsedatum": {
                                                            "value": "2012-09-30",
                                                            "include": true
                                                        },
                                                        "LMIH_handelseorsakann": {
                                                            "value": null,
                                                            "include": true
                                                        },
                                                        "LMIH_handelseorsak": {
                                                            "value": 17465,
                                                            "include": true
                                                        }
                                                    },
                                                    "vdlists": {}
                                                }
                                            ],
                                            "LMI_Utsättning": [
                                                {
                                                    "id": 216,
                                                    "subTables": {},
                                                    "regvars": {
                                                        "LMIU_utsattningsdatum": {
                                                            "value": "2013-03-30",
                                                            "include": true
                                                        },
                                                        "LMIU_utsattningsorsak": {
                                                            "value": 17436,
                                                            "include": true
                                                        },
                                                        "LMIU_utsattningsorsakann": {
                                                            "value": null,
                                                            "include": true
                                                        }
                                                    },
                                                    "vdlists": {}
                                                }
                                            ]
                                        },
                                        "regvars": {
                                            "LMI_indikation": {
                                                "value": 17426,
                                                "include": true
                                            }
                                        },
                                        "vdlists": {}
                                    }
                                ]
                            },
                            "regvars": {
                                "LM_studielkmtxt": {
                                    "value": null,
                                    "include": true
                                },
                                "LM_typ": {
                                    "value": 17531,
                                    "include": true
                                },
                                "LM_substans": {
                                    "value": 2482,
                                    "include": true
                                },
                                "LM_studielkm": {
                                    "value": null,
                                    "include": true
                                },
                                "LM_ablatiotestis": {
                                    "value": null,
                                    "include": true
                                },
                                "LM_ablatiotestisdatum": {
                                    "value": null,
                                    "include": true
                                }
                            },
                            "vdlists": {}
                        },
                        {
                            "id": 247,
                            "subTables": {
                                "LM_Insättning": [
                                    {
                                        "id": 298,
                                        "subTables": {
                                            "LMI_Händelse": [
                                                {
                                                    "id": 423,
                                                    "subTables": {},
                                                    "regvars": {
                                                        "LMIH_handelse": {
                                                            "value": 17427,
                                                            "include": true
                                                        },
                                                        "LMIH_handelsedatum": {
                                                            "value": "2013-09-13",
                                                            "include": true
                                                        },
                                                        "LMIH_handelseorsakann": {
                                                            "value": null,
                                                            "include": true
                                                        },
                                                        "LMIH_handelseorsak": {
                                                            "value": 17465,
                                                            "include": true
                                                        }
                                                    },
                                                    "vdlists": {}
                                                }
                                            ],
                                            "LMI_Utsättning": [
                                                {
                                                    "id": 217,
                                                    "subTables": {},
                                                    "regvars": {
                                                        "LMIU_utsattningsdatum": {
                                                            "value": "2014-04-17",
                                                            "include": true
                                                        },
                                                        "LMIU_utsattningsorsak": {
                                                            "value": 17437,
                                                            "include": true
                                                        },
                                                        "LMIU_utsattningsorsakann": {
                                                            "value": null,
                                                            "include": true
                                                        }
                                                    },
                                                    "vdlists": {}
                                                }
                                            ]
                                        },
                                        "regvars": {
                                            "LMI_indikation": {
                                                "value": 17426,
                                                "include": true
                                            }
                                        },
                                        "vdlists": {}
                                    }
                                ]
                            },
                            "regvars": {
                                "LM_studielkmtxt": {
                                    "value": null,
                                    "include": true
                                },
                                "LM_typ": {
                                    "value": 17530,
                                    "include": true
                                },
                                "LM_substans": {
                                    "value": 3121,
                                    "include": true
                                },
                                "LM_studielkm": {
                                    "value": null,
                                    "include": true
                                },
                                "LM_ablatiotestis": {
                                    "value": null,
                                    "include": true
                                },
                                "LM_ablatiotestisdatum": {
                                    "value": null,
                                    "include": true
                                }
                            },
                            "vdlists": {}
                        },
                        {
                            "id": 251,
                            "subTables": {
                                "LM_Insättning": [
                                    {
                                        "id": 302,
                                        "subTables": {
                                            "LMI_Händelse": [
                                                {
                                                    "id": 427,
                                                    "subTables": {},
                                                    "regvars": {
                                                        "LMIH_handelse": {
                                                            "value": 17427,
                                                            "include": true
                                                        },
                                                        "LMIH_handelsedatum": {
                                                            "value": "2014-09-18",
                                                            "include": true
                                                        },
                                                        "LMIH_handelseorsakann": {
                                                            "value": null,
                                                            "include": true
                                                        },
                                                        "LMIH_handelseorsak": {
                                                            "value": 17465,
                                                            "include": true
                                                        }
                                                    },
                                                    "vdlists": {}
                                                }
                                            ],
                                            "LMI_Utsättning": [
                                                {
                                                    "id": 221,
                                                    "subTables": {},
                                                    "regvars": {
                                                        "LMIU_utsattningsdatum": {
                                                            "value": "2015-05-14",
                                                            "include": true
                                                        },
                                                        "LMIU_utsattningsorsak": {
                                                            "value": 17438,
                                                            "include": true
                                                        },
                                                        "LMIU_utsattningsorsakann": {
                                                            "value": null,
                                                            "include": true
                                                        }
                                                    },
                                                    "vdlists": {}
                                                }
                                            ]
                                        },
                                        "regvars": {
                                            "LMI_indikation": {
                                                "value": 17426,
                                                "include": true
                                            }
                                        },
                                        "vdlists": {}
                                    }
                                ]
                            },
                            "regvars": {
                                "LM_studielkmtxt": {
                                    "value": null,
                                    "include": true
                                },
                                "LM_typ": {
                                    "value": 17530,
                                    "include": true
                                },
                                "LM_substans": {
                                    "value": 107,
                                    "include": true
                                },
                                "LM_studielkm": {
                                    "value": null,
                                    "include": true
                                },
                                "LM_ablatiotestis": {
                                    "value": null,
                                    "include": true
                                },
                                "LM_ablatiotestisdatum": {
                                    "value": null,
                                    "include": true
                                }
                            },
                            "vdlists": {}
                        },
                        {
                            "id": 255,
                            "subTables": {
                                "LM_Insättning": [
                                    {
                                        "id": 306,
                                        "subTables": {
                                            "LMI_Händelse": [
                                                {
                                                    "id": 431,
                                                    "subTables": {},
                                                    "regvars": {
                                                        "LMIH_handelse": {
                                                            "value": 17427,
                                                            "include": true
                                                        },
                                                        "LMIH_handelsedatum": {
                                                            "value": "2015-07-24",
                                                            "include": true
                                                        },
                                                        "LMIH_handelseorsakann": {
                                                            "value": null,
                                                            "include": true
                                                        },
                                                        "LMIH_handelseorsak": {
                                                            "value": 17465,
                                                            "include": true
                                                        }
                                                    },
                                                    "vdlists": {}
                                                }
                                            ],
                                            "LMI_Utsättning": []
                                        },
                                        "regvars": {
                                            "LMI_indikation": {
                                                "value": 17426,
                                                "include": true
                                            }
                                        },
                                        "vdlists": {}
                                    }
                                ]
                            },
                            "regvars": {
                                "LM_studielkmtxt": {
                                    "value": null,
                                    "include": true
                                },
                                "LM_typ": {
                                    "value": 17736,
                                    "include": true
                                },
                                "LM_substans": {
                                    "value": 1503,
                                    "include": true
                                },
                                "LM_studielkm": {
                                    "include": true
                                },
                                "LM_ablatiotestis": {
                                    "include": true
                                },
                                "LM_ablatiotestisdatum": {
                                    "value": null,
                                    "include": true
                                }
                            },
                            "vdlists": {}
                        },
                        {
                            "id": 909,
                            "subTables": {
                                "LM_Insättning": []
                            },
                            "regvars": {
                                "LM_studielkmtxt": {
                                    "value": "shd",
                                    "include": true
                                },
                                "LM_typ": {
                                    "value": 17530,
                                    "include": true
                                },
                                "LM_substans": {
                                    "value": null,
                                    "include": true
                                },
                                "LM_studielkm": {
                                    "value": true,
                                    "include": true
                                },
                                "LM_ablatiotestis": {
                                    "value": null,
                                    "include": true
                                },
                                "LM_ablatiotestisdatum": {
                                    "value": null,
                                    "include": true
                                }
                            },
                            "vdlists": {}
                        }
                    ],
                    "Strålbehandling": [
                        {
                            "id": 180,
                            "subTables": {
                                "SB_Grupp": [
                                    {
                                        "id": 304,
                                        "subTables": {
                                            "SBG_Lokal": [
                                                {
                                                    "id": 14,
                                                    "subTables": {},
                                                    "regvars": {
                                                        "SBGL_lokal": {
                                                            "value": 17505,
                                                            "include": true
                                                        },
                                                        "SBGL_lokalann": {
                                                            "value": null,
                                                            "include": true
                                                        },
                                                        "SBGL_lokalskelett": {
                                                            "value": "Hö bäckenhalva",
                                                            "include": true
                                                        }
                                                    },
                                                    "vdlists": {}
                                                }
                                            ]
                                        },
                                        "regvars": {
                                            "SBG_slutdos": {
                                                "value": 8,
                                                "include": true
                                            },
                                            "SBG_frakdos": {
                                                "value": 8,
                                                "include": true
                                            },
                                            "SBL_lokal2": {
                                                "value": null,
                                                "include": true
                                            },
                                            "SBL_lokalann2": {
                                                "value": null,
                                                "include": true
                                            },
                                            "SBL_lokalskelett2": {
                                                "value": null,
                                                "include": true
                                            }
                                        },
                                        "vdlists": {}
                                    },
                                    {
                                        "id": 305,
                                        "subTables": {
                                            "SBG_Lokal": [
                                                {
                                                    "id": 15,
                                                    "subTables": {},
                                                    "regvars": {
                                                        "SBGL_lokal": {
                                                            "value": 17505,
                                                            "include": true
                                                        },
                                                        "SBGL_lokalann": {
                                                            "value": null,
                                                            "include": true
                                                        },
                                                        "SBGL_lokalskelett": {
                                                            "value": "Th3-4",
                                                            "include": true
                                                        }
                                                    },
                                                    "vdlists": {}
                                                }
                                            ]
                                        },
                                        "regvars": {
                                            "SBG_slutdos": {
                                                "value": 8,
                                                "include": true
                                            },
                                            "SBG_frakdos": {
                                                "value": 8,
                                                "include": true
                                            },
                                            "SBL_lokal2": {
                                                "value": null,
                                                "include": true
                                            },
                                            "SBL_lokalann2": {
                                                "value": null,
                                                "include": true
                                            },
                                            "SBL_lokalskelett2": {
                                                "value": null,
                                                "include": true
                                            }
                                        },
                                        "vdlists": {}
                                    }
                                ]
                            },
                            "regvars": {
                                "SB_slutdatum": {
                                    "value": "2016-06-04",
                                    "include": true
                                },
                                "SB_frakdos2": {
                                    "value": null,
                                    "include": true
                                },
                                "SB_slutdos2": {
                                    "value": null,
                                    "include": true
                                }
                            },
                            "vdlists": {}
                        }
                    ],
                    "Bidragande": [
                        {
                            "id": 8,
                            "subTables": {},
                            "regvars": {
                                "BI_datum": {
                                    "value": "2016-11-13",
                                    "include": true
                                },
                                "BI_godkannande": {
                                    "value": true,
                                    "include": true
                                },
                                "BI_inrappenh": {
                                    "value": "-- Välj enhet i organisationen --",
                                    "include": true
                                },
                                "BI_inrapp": {
                                    "value": "Jonas Celander Guss",
                                    "include": true
                                },
                                "BI_kommentar": {
                                    "value": null,
                                    "include": true
                                },
                                "BI_sjukklin": {
                                    "value": null,
                                    "include": true
                                },
                                "BI_sjukkod": {
                                    "value": null,
                                    "include": true
                                }
                            },
                            "vdlists": {}
                        },
                        {
                            "id": 1427,
                            "subTables": {},
                            "regvars": {
                                "BI_datum": {
                                    "value": "2016-03-26",
                                    "include": true
                                },
                                "BI_godkannande": {
                                    "value": true,
                                    "include": true
                                },
                                "BI_inrappenh": {
                                    "value": "OC Demo (0) - SU/Sahlgrenska (500010) - Medicinkliniken (101)",
                                    "include": true
                                },
                                "BI_inrapp": {
                                    "value": "Jonas Celander Guss",
                                    "include": true
                                },
                                "BI_kommentar": {
                                    "value": "adfg",
                                    "include": true
                                },
                                "BI_sjukklin": {
                                    "value": null,
                                    "include": true
                                },
                                "BI_sjukkod": {
                                    "value": null,
                                    "include": true
                                }
                            },
                            "vdlists": {}
                        }
                    ],
                    "PROM": [
                        {
                            "id": 1535,
                            "subTables": {},
                            "regvars": {
                                "PROM_datum": {
                                    "value": "2015-05-26",
                                    "include": true
                                },
                                "PROM_fr1": {
                                    "value": 1,
                                    "include": true
                                },
                                "PROM_fr2": {
                                    "value": 2,
                                    "include": true
                                },
                                "PROM_fr3": {
                                    "value": 0,
                                    "include": true
                                },
                                "PROM_fr4": {
                                    "value": 2,
                                    "include": true
                                },
                                "PROM_fr5": {
                                    "value": 4,
                                    "include": true
                                },
                                "PROM_fr6": {
                                    "value": 3,
                                    "include": true
                                },
                                "PROM_fr7": {
                                    "value": 1,
                                    "include": true
                                },
                                "PROM_fr8": {
                                    "value": 1,
                                    "include": true
                                },
                                "PROM_fr9": {
                                    "value": 0,
                                    "include": true
                                },
                                "PROM_fr10": {
                                    "value": 2,
                                    "include": true
                                },
                                "PROM_fr11": {
                                    "value": 3,
                                    "include": true
                                },
                                "PROM_fr12": {
                                    "value": 0,
                                    "include": true
                                },
                                "PROM_fr13": {
                                    "value": 4,
                                    "include": true
                                },
                                "PROM_fr14": {
                                    "value": 4,
                                    "include": true
                                },
                                "PROM_fr15": {
                                    "value": 1,
                                    "include": true
                                },
                                "PROM_fr16": {
                                    "value": 2,
                                    "include": true
                                }
                            },
                            "vdlists": {}
						},
						{
                            "id": 1536,
                            "subTables": {},
                            "regvars": {
                                "PROM_datum": {
                                    "value": "2016-12-06",
                                    "include": true
                                },
                                "PROM_fr1": {
                                    "value": 2,
                                    "include": true
                                },
                                "PROM_fr2": {
                                    "value": 2,
                                    "include": true
                                },
                                "PROM_fr3": {
                                    "value": 1,
                                    "include": true
                                },
                                "PROM_fr4": {
                                    "value": 3,
                                    "include": true
                                },
                                "PROM_fr5": {
                                    "value": 4,
                                    "include": true
                                },
                                "PROM_fr6": {
                                    "value": 3,
                                    "include": true
                                },
                                "PROM_fr7": {
                                    "value": 2,
                                    "include": true
                                },
                                "PROM_fr8": {
                                    "value": 3,
                                    "include": true
                                },
                                "PROM_fr9": {
                                    "value": 1,
                                    "include": true
                                },
                                "PROM_fr10": {
                                    "value": 4,
                                    "include": true
                                },
                                "PROM_fr11": {
                                    "value": 2,
                                    "include": true
                                },
                                "PROM_fr12": {
                                    "value": 3,
                                    "include": true
                                },
                                "PROM_fr13": {
                                    "value": 1,
                                    "include": true
                                },
                                "PROM_fr14": {
                                    "value": 2,
                                    "include": true
                                },
                                "PROM_fr15": {
                                    "value": 1,
                                    "include": true
                                },
                                "PROM_fr16": {
                                    "value": 3,
                                    "include": true
                                }
                            }
						},
						{
                            "id": 1537,
                            "subTables": {},
                            "regvars": {
                                "PROM_datum": {
                                    "value": "2016-08-16",
                                    "include": true
                                },
                                "PROM_fr1": {
                                    "value": 2,
                                    "include": true
                                },
                                "PROM_fr2": {
                                    "value": 1,
                                    "include": true
                                },
                                "PROM_fr3": {
                                    "value": 3,
                                    "include": true
                                },
                                "PROM_fr4": {
                                    "value": 2,
                                    "include": true
                                },
                                "PROM_fr5": {
                                    "value": 3,
                                    "include": true
                                },
                                "PROM_fr6": {
                                    "value": 1,
                                    "include": true
                                },
                                "PROM_fr7": {
                                    "value": 1,
                                    "include": true
                                },
                                "PROM_fr8": {
                                    "value": 2,
                                    "include": true
                                },
                                "PROM_fr9": {
                                    "value": 0,
                                    "include": true
                                },
                                "PROM_fr10": {
                                    "value": 1,
                                    "include": true
                                },
                                "PROM_fr11": {
                                    "value": 0,
                                    "include": true
                                },
                                "PROM_fr12": {
                                    "value": 0,
                                    "include": true
                                },
                                "PROM_fr13": {
                                    "value": 3,
                                    "include": true
                                },
                                "PROM_fr14": {
                                    "value": 0,
                                    "include": true
                                },
                                "PROM_fr15": {
                                    "value": 1,
                                    "include": true
                                },
                                "PROM_fr16": {
                                    "value": 0,
                                    "include": true
                                }
                            }
						},
						{
                            "id": 1538,
                            "subTables": {},
                            "regvars": {
                                "PROM_datum": {
                                    "value": "2016-04-20",
                                    "include": true
                                },
                                "PROM_fr1": {
                                    "value": 1,
                                    "include": true
                                },
                                "PROM_fr2": {
                                    "value": 0,
                                    "include": true
                                },
                                "PROM_fr3": {
                                    "value": 2,
                                    "include": true
                                },
                                "PROM_fr4": {
                                    "value": 1,
                                    "include": true
                                },
                                "PROM_fr5": {
                                    "value": 2,
                                    "include": true
                                },
                                "PROM_fr6": {
                                    "value": 0,
                                    "include": true
                                },
                                "PROM_fr7": {
                                    "value": 2,
                                    "include": true
                                },
                                "PROM_fr8": {
                                    "value": 0,
                                    "include": true
                                },
                                "PROM_fr9": {
                                    "value": 1,
                                    "include": true
                                },
                                "PROM_fr10": {
                                    "value": 1,
                                    "include": true
                                },
                                "PROM_fr11": {
                                    "value": 2,
                                    "include": true
                                },
                                "PROM_fr12": {
                                    "value": 2,
                                    "include": true
                                },
                                "PROM_fr13": {
                                    "value": 1,
                                    "include": true
                                },
                                "PROM_fr14": {
                                    "value": 0,
                                    "include": true
                                },
                                "PROM_fr15": {
                                    "value": 2,
                                    "include": true
                                },
                                "PROM_fr16": {
                                    "value": 1,
                                    "include": true
                                }
							}
						},
						{
                            "id": 1539,
                            "subTables": {},
                            "regvars": {
                                "PROM_datum": {
                                    "value": "2016-01-05",
                                    "include": true
                                },
                                "PROM_fr1": {
                                    "value": 4,
                                    "include": true
                                },
                                "PROM_fr2": {
                                    "value": 3,
                                    "include": true
                                },
                                "PROM_fr3": {
                                    "value": 4,
                                    "include": true
                                },
                                "PROM_fr4": {
                                    "value": 1,
                                    "include": true
                                },
                                "PROM_fr5": {
                                    "value": 3,
                                    "include": true
                                },
                                "PROM_fr6": {
                                    "value": 2,
                                    "include": true
                                },
                                "PROM_fr7": {
                                    "value": 4,
                                    "include": true
                                },
                                "PROM_fr8": {
                                    "value": 2,
                                    "include": true
                                },
                                "PROM_fr9": {
                                    "value": 3,
                                    "include": true
                                },
                                "PROM_fr10": {
                                    "value": 4,
                                    "include": true
                                },
                                "PROM_fr11": {
                                    "value": 1,
                                    "include": true
                                },
                                "PROM_fr12": {
                                    "value": 4,
                                    "include": true
                                },
                                "PROM_fr13": {
                                    "value": 2,
                                    "include": true
                                },
                                "PROM_fr14": {
                                    "value": 3,
                                    "include": true
                                },
                                "PROM_fr15": {
                                    "value": 2,
                                    "include": true
                                },
                                "PROM_fr16": {
                                    "value": 3,
                                    "include": true
                                }
							}
						},
						{
                            "id": 1540,
                            "subTables": {},
                            "regvars": {
                                "PROM_datum": {
                                    "value": "2015-09-09",
                                    "include": true
                                },
                                "PROM_fr1": {
                                    "value": 3,
                                    "include": true
                                },
                                "PROM_fr2": {
                                    "value": 2,
                                    "include": true
                                },
                                "PROM_fr3": {
                                    "value": 3,
                                    "include": true
                                },
                                "PROM_fr4": {
                                    "value": 4,
                                    "include": true
                                },
                                "PROM_fr5": {
                                    "value": 3,
                                    "include": true
                                },
                                "PROM_fr6": {
                                    "value": 4,
                                    "include": true
                                },
                                "PROM_fr7": {
                                    "value": 1,
                                    "include": true
                                },
                                "PROM_fr8": {
                                    "value": 4,
                                    "include": true
                                },
                                "PROM_fr9": {
                                    "value": 2,
                                    "include": true
                                },
                                "PROM_fr10": {
                                    "value": 1,
                                    "include": true
                                },
                                "PROM_fr11": {
                                    "value": 3,
                                    "include": true
                                },
                                "PROM_fr12": {
                                    "value": 4,
                                    "include": true
                                },
                                "PROM_fr13": {
                                    "value": 3,
                                    "include": true
                                },
                                "PROM_fr14": {
                                    "value": 3,
                                    "include": true
                                },
                                "PROM_fr15": {
                                    "value": 4,
                                    "include": true
                                },
                                "PROM_fr16": {
                                    "value": 4,
                                    "include": true
                                }
							}
						}
					]
                },
                "regvars": {
                    "U_datum": {
                        "value": "2016-07-02",
                        "include": true
                    },
                    "U_npcr_fk": {
                        "value": 425705,
                        "include": true
                    },
                    "U_crpcdatum": {
                        "value": "2013-03-22",
                        "include": true
                    },
                    "U_metastasdatum": {
                        "value": "",
                        "include": true
                    },
                    "U_hormonstartdatum": {
                        "value": "2012-03-25",
                        "include": true
                    },
                    "U_psadatum": {
                        "value": "2012-03-12",
                        "include": true
                    },
                    "U_pallHemSjkvrd": {
                        "value": "2012-09-30",
                        "include": true
                    },
                    "U_patMedgivande": {
                        "value": 17406,
                        "include": true
                    }
                },
                "vdlists": {}
            },
            "listValuesValidFrom": "0001-01-01T00:00:00Z",
            "listValuesValidTo": "9999-12-30T23:00:00Z",
            "env": {
                "_INREPNAME": "",
                "_INUNITNAME": "",
                "_REPORTERNAME": "",
                "_ADDRESS": "SJUNETVÄGEN 9 D VÅN 3",
                "_AVREGDATUM": "",
                "_AVREGORSAK": "",
                "_BEFOLKNINGSREGISTER": "Demo Befolkningsregister",
                "_CITY": "GÖTEBORG",
                "_DATEOFDEATH": "",
                "_DODSORSAK": "",
                "_DODSORSAKSBESKR": "",
                "_FIRSTNAME": "LEIF EGON",
                "_FODELSEDATUM": "1214-12-04",
                "_FORSAMLING": "17",
                "_KOMMUN": "80",
                "_LAN": "14",
                "_LAND": "Sweden",
                "_LKF": "148017",
                "_NAVETDATUM": "2016-04-17",
                "_PATIENTID": "117",
                "_PERSNR": "12141204-8041",
                "_POSTALCODE": "41727",
                "_SECRET": "True",
                "_SEX": "M",
                "_SURNAME": "BIZTALK"
            },
            "isReadOnly": false,
            "useHtmlLayout": true
        },
        "overview": {
            "supportsTabs": false
        }
    },
    "recording": {
        "getValueDomainValues": {
            "[[\"error\",null],[\"parameters\",[[\"patientid\",\"117\"]]],[\"success\",null],[\"vdlist\",\"U_npcr_fk\"]]": [
                [
                    {
                        "text": "Rapporteringsdatum: 2013-12-10 (id 425705)",
                        "id": 425705,
                        "data": {
                            "D_pk": 425705,
                            "s_startdatseeds": null,
                            "s_startdatrt": "2009-01-15",
                            "s_startdatboost": "2009-02-03",
                            "s_slutdosseeds": null,
                            "s_slutdosboost": 20,
                            "s_slutdosrt": 50,
                            "s_seeds_value": "0",
                            "s_seeds": "Nej",
                            "s_primextrt_value": "1",
                            "s_primextrt": "Ja",
                            "s_postop_rt_value": "0",
                            "s_postop_rt": "Nej",
                            "s_fraktdosboost": 10,
                            "s_fraktdosrt": 2,
                            "s_boost_value": "1",
                            "s_boost": "Ja",
                            "D_pk17": 425705,
                            "PAT_ID": 117,
                            "MAKULERAD": false,
                            "D_Tstad_value": "3",
                            "D_Tstad": "T3 - Palpabel, växer genom kapseln och ev in i sädesblåsorna eller i blåshalsen",
                            "D_SpsaSakn": null,
                            "D_SpsaDat": null,
                            "D_SPSA": 34,
                            "D_Rappdat": "2015-12-10",
                            "D_Nstad_value": "1",
                            "D_Nstad": "N1 - Tecken på lymfkörtelmetastaser",
                            "D_Mstad_value": "1",
                            "D_Mstad": "M0 - Inga tecken på fjärrmetastaser",
                            "D_GleasTva": 3,
                            "D_GleasSakn": null,
                            "D_GleasSa": 7,
                            "D_GleasEtt": 4,
                            "D_DiaDat": "2007-03-02",
                            "D_DiaAge": 792,
                            "D_DatumPADSak": null,
                            "D_DatumPAD": "2007-05-30",
                            "B_RT_value": "3",
                            "B_RT": "Kombination av extern radioterapi och högdos brakyterapi",
                            "B_ProstektDat": null,
                            "B_Prostekt_value": "",
                            "B_Prostekt": "",
                            "B_OpTyp_value": null,
                            "B_OpTyp": null,
                            "B_KonsTer_value": "",
                            "B_KonsTer": "",
                            "B_BehTyp_value": "2",
                            "B_BehTyp": "Kurativ terapi",
                            "B_BehBesDat": "2007-06-18"
                        }
                    }
                ]
            ],
            "[[\"error\",null],[\"parameters\",[[\"kategori\",\"ProstataProcess_ET\"]]],[\"success\",null],[\"vdlist\",\"LM_substans\"]]": [
                [
                    {
                        "text": "abirateron",
                        "id": 3469,
                        "data": {
                            "sub_pk": 3469,
                            "sub_subnamnrek": "abirateron",
                            "sub_atc": "QL02BX03",
                            "sub_cas": "154229-19-3",
                            "sub_narkotikaklass": "0",
                            "sub_nslid": "IDL68G8E1D4IM7BSRV",
                            "sub_snomed": null,
                            "sub_srsunii": "G819A456D0",
                            "sub_versiondtm": "2015-01-15",
                            "kat_substansfarg": "#ee9572"
                        }
                    },
                    {
                        "text": "bikalutamid",
                        "id": 835,
                        "data": {
                            "sub_pk": 835,
                            "sub_subnamnrek": "bikalutamid",
                            "sub_atc": "QL02BB03",
                            "sub_cas": "90357-06-5",
                            "sub_narkotikaklass": "0",
                            "sub_nslid": "ID3C4L4D1YC7USBMSR",
                            "sub_snomed": "386908000",
                            "sub_srsunii": "A0Z3NAU9DP",
                            "sub_versiondtm": "2015-06-18",
                            "kat_substansfarg": "#ee9a49"
                        }
                    },
                    {
                        "text": "buserelin",
                        "id": 2482,
                        "data": {
                            "sub_pk": 2482,
                            "sub_subnamnrek": "buserelin",
                            "sub_atc": "L02AE01",
                            "sub_cas": "57982-77-1",
                            "sub_narkotikaklass": "0",
                            "sub_nslid": "IDE4POF6UARMDVERT1",
                            "sub_snomed": "395744006",
                            "sub_srsunii": "PXW8U3YXDV",
                            "sub_versiondtm": "2014-09-25",
                            "kat_substansfarg": "#d98719"
                        }
                    },
                    {
                        "text": "cyproteronacetat",
                        "id": 1509,
                        "data": {
                            "sub_pk": 1509,
                            "sub_subnamnrek": "cyproteronacetat",
                            "sub_atc": "G03HA01",
                            "sub_cas": "427-51-0",
                            "sub_narkotikaklass": "0",
                            "sub_nslid": "IDE4POCAU9DXYVERT1",
                            "sub_snomed": "126120000",
                            "sub_srsunii": "4KM2BN5JHF",
                            "sub_versiondtm": "2015-01-15",
                            "kat_substansfarg": "#ffa07a"
                        }
                    },
                    {
                        "text": "degarelix",
                        "id": 667,
                        "data": {
                            "sub_pk": 667,
                            "sub_subnamnrek": "degarelix",
                            "sub_atc": "QL02BX02",
                            "sub_cas": "214766-78-6",
                            "sub_narkotikaklass": "0",
                            "sub_nslid": "ID1QKOM5K1CBHWLCSR",
                            "sub_snomed": "441864003",
                            "sub_srsunii": "SX0XJI3A11",
                            "sub_versiondtm": "2014-09-25",
                            "kat_substansfarg": "#cdaa7d"
                        }
                    },
                    {
                        "text": "enzalutamid",
                        "id": 5105,
                        "data": {
                            "sub_pk": 5105,
                            "sub_subnamnrek": "enzalutamid",
                            "sub_atc": null,
                            "sub_cas": "915087-33-1",
                            "sub_narkotikaklass": "0",
                            "sub_nslid": "ID6347656300001275",
                            "sub_snomed": null,
                            "sub_srsunii": "93T0T9GKNU",
                            "sub_versiondtm": "2014-09-25",
                            "kat_substansfarg": "#ee9a49"
                        }
                    },
                    {
                        "text": "flutamid",
                        "id": 2093,
                        "data": {
                            "sub_pk": 2093,
                            "sub_subnamnrek": "flutamid",
                            "sub_atc": "QL02BB01",
                            "sub_cas": "13311-84-7",
                            "sub_narkotikaklass": "0",
                            "sub_nslid": "IDE4POEGUA7IJVERT1",
                            "sub_snomed": "387587007",
                            "sub_srsunii": "76W6J0943E",
                            "sub_versiondtm": "2015-01-15",
                            "kat_substansfarg": "#db9370"
                        }
                    },
                    {
                        "text": "goserelin",
                        "id": 2546,
                        "data": {
                            "sub_pk": 2546,
                            "sub_subnamnrek": "goserelin",
                            "sub_atc": "QL02AE03",
                            "sub_cas": "65807-02-5",
                            "sub_narkotikaklass": "0",
                            "sub_nslid": "IDE4POFAUAUR0VERT1",
                            "sub_snomed": "108771008",
                            "sub_srsunii": "0F65R8P09N",
                            "sub_versiondtm": "2014-09-25",
                            "kat_substansfarg": "#cdad00"
                        }
                    },
                    {
                        "text": "histrelin",
                        "id": 2635,
                        "data": {
                            "sub_pk": 2635,
                            "sub_subnamnrek": "histrelin",
                            "sub_atc": "L02AE05",
                            "sub_cas": "76712-82-8",
                            "sub_narkotikaklass": "0",
                            "sub_nslid": "IDE4POFFUAYCXVERT1",
                            "sub_snomed": "109049002",
                            "sub_srsunii": "H50H3S3W74",
                            "sub_versiondtm": "2015-02-11",
                            "kat_substansfarg": "#cd8162"
                        }
                    },
                    {
                        "text": "ketokonazol",
                        "id": 2545,
                        "data": {
                            "sub_pk": 2545,
                            "sub_subnamnrek": "ketokonazol",
                            "sub_atc": "D01AC08",
                            "sub_cas": "65277-42-1",
                            "sub_narkotikaklass": "0",
                            "sub_nslid": "IDE4POFAUAUHUVERT1",
                            "sub_snomed": "387216007",
                            "sub_srsunii": "R9400W927I",
                            "sub_versiondtm": "2015-01-15",
                            "kat_substansfarg": "#8c7853"
                        }
                    },
                    {
                        "text": "leuprorelin",
                        "id": 2430,
                        "data": {
                            "sub_pk": 2430,
                            "sub_subnamnrek": "leuprorelin",
                            "sub_atc": "QL02AE02",
                            "sub_cas": "53714-56-0",
                            "sub_narkotikaklass": "0",
                            "sub_nslid": "IDE4POF2UAONTVERT1",
                            "sub_snomed": "397198002",
                            "sub_srsunii": "EFY6W0M8TG",
                            "sub_versiondtm": "2014-09-25",
                            "kat_substansfarg": "#ee9A00"
                        }
                    },
                    {
                        "text": "polyestradiolfosfat",
                        "id": 2289,
                        "data": {
                            "sub_pk": 2289,
                            "sub_subnamnrek": "polyestradiolfosfat",
                            "sub_atc": "QL02AA02",
                            "sub_cas": "28014-46-2",
                            "sub_narkotikaklass": "0",
                            "sub_nslid": "IDE4POETUAHBTVERT1",
                            "sub_snomed": "126084009",
                            "sub_srsunii": "P14877CDX2",
                            "sub_versiondtm": "2015-01-15",
                            "kat_substansfarg": "#b87333"
                        }
                    },
                    {
                        "text": "triptorelin",
                        "id": 2478,
                        "data": {
                            "sub_pk": 2478,
                            "sub_subnamnrek": "triptorelin",
                            "sub_atc": "QL02AE04",
                            "sub_cas": "57773-63-4",
                            "sub_narkotikaklass": "0",
                            "sub_nslid": "IDE4POF6UARH2VERT1",
                            "sub_snomed": "395915003",
                            "sub_srsunii": "9081Y98W2V",
                            "sub_versiondtm": "2015-01-15",
                            "kat_substansfarg": "#eeb422"
                        }
                    }
                ],
                [
                    {
                        "text": "abirateron",
                        "id": 3469,
                        "data": {
                            "sub_pk": 3469,
                            "sub_subnamnrek": "abirateron",
                            "sub_atc": "QL02BX03",
                            "sub_cas": "154229-19-3",
                            "sub_narkotikaklass": "0",
                            "sub_nslid": "IDL68G8E1D4IM7BSRV",
                            "sub_snomed": null,
                            "sub_srsunii": "G819A456D0",
                            "sub_versiondtm": "2015-01-15",
                            "kat_substansfarg": "#ee9572"
                        }
                    },
                    {
                        "text": "bikalutamid",
                        "id": 835,
                        "data": {
                            "sub_pk": 835,
                            "sub_subnamnrek": "bikalutamid",
                            "sub_atc": "QL02BB03",
                            "sub_cas": "90357-06-5",
                            "sub_narkotikaklass": "0",
                            "sub_nslid": "ID3C4L4D1YC7USBMSR",
                            "sub_snomed": "386908000",
                            "sub_srsunii": "A0Z3NAU9DP",
                            "sub_versiondtm": "2015-06-18",
                            "kat_substansfarg": "#ee9a49"
                        }
                    },
                    {
                        "text": "buserelin",
                        "id": 2482,
                        "data": {
                            "sub_pk": 2482,
                            "sub_subnamnrek": "buserelin",
                            "sub_atc": "L02AE01",
                            "sub_cas": "57982-77-1",
                            "sub_narkotikaklass": "0",
                            "sub_nslid": "IDE4POF6UARMDVERT1",
                            "sub_snomed": "395744006",
                            "sub_srsunii": "PXW8U3YXDV",
                            "sub_versiondtm": "2014-09-25",
                            "kat_substansfarg": "#d98719"
                        }
                    },
                    {
                        "text": "cyproteronacetat",
                        "id": 1509,
                        "data": {
                            "sub_pk": 1509,
                            "sub_subnamnrek": "cyproteronacetat",
                            "sub_atc": "G03HA01",
                            "sub_cas": "427-51-0",
                            "sub_narkotikaklass": "0",
                            "sub_nslid": "IDE4POCAU9DXYVERT1",
                            "sub_snomed": "126120000",
                            "sub_srsunii": "4KM2BN5JHF",
                            "sub_versiondtm": "2015-01-15",
                            "kat_substansfarg": "#ffa07a"
                        }
                    },
                    {
                        "text": "degarelix",
                        "id": 667,
                        "data": {
                            "sub_pk": 667,
                            "sub_subnamnrek": "degarelix",
                            "sub_atc": "QL02BX02",
                            "sub_cas": "214766-78-6",
                            "sub_narkotikaklass": "0",
                            "sub_nslid": "ID1QKOM5K1CBHWLCSR",
                            "sub_snomed": "441864003",
                            "sub_srsunii": "SX0XJI3A11",
                            "sub_versiondtm": "2014-09-25",
                            "kat_substansfarg": "#cdaa7d"
                        }
                    },
                    {
                        "text": "enzalutamid",
                        "id": 5105,
                        "data": {
                            "sub_pk": 5105,
                            "sub_subnamnrek": "enzalutamid",
                            "sub_atc": null,
                            "sub_cas": "915087-33-1",
                            "sub_narkotikaklass": "0",
                            "sub_nslid": "ID6347656300001275",
                            "sub_snomed": null,
                            "sub_srsunii": "93T0T9GKNU",
                            "sub_versiondtm": "2014-09-25",
                            "kat_substansfarg": "#ee9a49"
                        }
                    },
                    {
                        "text": "flutamid",
                        "id": 2093,
                        "data": {
                            "sub_pk": 2093,
                            "sub_subnamnrek": "flutamid",
                            "sub_atc": "QL02BB01",
                            "sub_cas": "13311-84-7",
                            "sub_narkotikaklass": "0",
                            "sub_nslid": "IDE4POEGUA7IJVERT1",
                            "sub_snomed": "387587007",
                            "sub_srsunii": "76W6J0943E",
                            "sub_versiondtm": "2015-01-15",
                            "kat_substansfarg": "#db9370"
                        }
                    },
                    {
                        "text": "goserelin",
                        "id": 2546,
                        "data": {
                            "sub_pk": 2546,
                            "sub_subnamnrek": "goserelin",
                            "sub_atc": "QL02AE03",
                            "sub_cas": "65807-02-5",
                            "sub_narkotikaklass": "0",
                            "sub_nslid": "IDE4POFAUAUR0VERT1",
                            "sub_snomed": "108771008",
                            "sub_srsunii": "0F65R8P09N",
                            "sub_versiondtm": "2014-09-25",
                            "kat_substansfarg": "#cdad00"
                        }
                    },
                    {
                        "text": "histrelin",
                        "id": 2635,
                        "data": {
                            "sub_pk": 2635,
                            "sub_subnamnrek": "histrelin",
                            "sub_atc": "L02AE05",
                            "sub_cas": "76712-82-8",
                            "sub_narkotikaklass": "0",
                            "sub_nslid": "IDE4POFFUAYCXVERT1",
                            "sub_snomed": "109049002",
                            "sub_srsunii": "H50H3S3W74",
                            "sub_versiondtm": "2015-02-11",
                            "kat_substansfarg": "#cd8162"
                        }
                    },
                    {
                        "text": "ketokonazol",
                        "id": 2545,
                        "data": {
                            "sub_pk": 2545,
                            "sub_subnamnrek": "ketokonazol",
                            "sub_atc": "D01AC08",
                            "sub_cas": "65277-42-1",
                            "sub_narkotikaklass": "0",
                            "sub_nslid": "IDE4POFAUAUHUVERT1",
                            "sub_snomed": "387216007",
                            "sub_srsunii": "R9400W927I",
                            "sub_versiondtm": "2015-01-15",
                            "kat_substansfarg": "#8c7853"
                        }
                    },
                    {
                        "text": "leuprorelin",
                        "id": 2430,
                        "data": {
                            "sub_pk": 2430,
                            "sub_subnamnrek": "leuprorelin",
                            "sub_atc": "QL02AE02",
                            "sub_cas": "53714-56-0",
                            "sub_narkotikaklass": "0",
                            "sub_nslid": "IDE4POF2UAONTVERT1",
                            "sub_snomed": "397198002",
                            "sub_srsunii": "EFY6W0M8TG",
                            "sub_versiondtm": "2014-09-25",
                            "kat_substansfarg": "#ee9A00"
                        }
                    },
                    {
                        "text": "polyestradiolfosfat",
                        "id": 2289,
                        "data": {
                            "sub_pk": 2289,
                            "sub_subnamnrek": "polyestradiolfosfat",
                            "sub_atc": "QL02AA02",
                            "sub_cas": "28014-46-2",
                            "sub_narkotikaklass": "0",
                            "sub_nslid": "IDE4POETUAHBTVERT1",
                            "sub_snomed": "126084009",
                            "sub_srsunii": "P14877CDX2",
                            "sub_versiondtm": "2015-01-15",
                            "kat_substansfarg": "#b87333"
                        }
                    },
                    {
                        "text": "triptorelin",
                        "id": 2478,
                        "data": {
                            "sub_pk": 2478,
                            "sub_subnamnrek": "triptorelin",
                            "sub_atc": "QL02AE04",
                            "sub_cas": "57773-63-4",
                            "sub_narkotikaklass": "0",
                            "sub_nslid": "IDE4POF6UARH2VERT1",
                            "sub_snomed": "395915003",
                            "sub_srsunii": "9081Y98W2V",
                            "sub_versiondtm": "2015-01-15",
                            "kat_substansfarg": "#eeb422"
                        }
                    }
                ],
                [
                    {
                        "text": "abirateron",
                        "id": 3469,
                        "data": {
                            "sub_pk": 3469,
                            "sub_subnamnrek": "abirateron",
                            "sub_atc": "QL02BX03",
                            "sub_cas": "154229-19-3",
                            "sub_narkotikaklass": "0",
                            "sub_nslid": "IDL68G8E1D4IM7BSRV",
                            "sub_snomed": null,
                            "sub_srsunii": "G819A456D0",
                            "sub_versiondtm": "2015-01-15",
                            "kat_substansfarg": "#ee9572"
                        }
                    },
                    {
                        "text": "bikalutamid",
                        "id": 835,
                        "data": {
                            "sub_pk": 835,
                            "sub_subnamnrek": "bikalutamid",
                            "sub_atc": "QL02BB03",
                            "sub_cas": "90357-06-5",
                            "sub_narkotikaklass": "0",
                            "sub_nslid": "ID3C4L4D1YC7USBMSR",
                            "sub_snomed": "386908000",
                            "sub_srsunii": "A0Z3NAU9DP",
                            "sub_versiondtm": "2015-06-18",
                            "kat_substansfarg": "#ee9a49"
                        }
                    },
                    {
                        "text": "buserelin",
                        "id": 2482,
                        "data": {
                            "sub_pk": 2482,
                            "sub_subnamnrek": "buserelin",
                            "sub_atc": "L02AE01",
                            "sub_cas": "57982-77-1",
                            "sub_narkotikaklass": "0",
                            "sub_nslid": "IDE4POF6UARMDVERT1",
                            "sub_snomed": "395744006",
                            "sub_srsunii": "PXW8U3YXDV",
                            "sub_versiondtm": "2014-09-25",
                            "kat_substansfarg": "#d98719"
                        }
                    },
                    {
                        "text": "cyproteronacetat",
                        "id": 1509,
                        "data": {
                            "sub_pk": 1509,
                            "sub_subnamnrek": "cyproteronacetat",
                            "sub_atc": "G03HA01",
                            "sub_cas": "427-51-0",
                            "sub_narkotikaklass": "0",
                            "sub_nslid": "IDE4POCAU9DXYVERT1",
                            "sub_snomed": "126120000",
                            "sub_srsunii": "4KM2BN5JHF",
                            "sub_versiondtm": "2015-01-15",
                            "kat_substansfarg": "#ffa07a"
                        }
                    },
                    {
                        "text": "degarelix",
                        "id": 667,
                        "data": {
                            "sub_pk": 667,
                            "sub_subnamnrek": "degarelix",
                            "sub_atc": "QL02BX02",
                            "sub_cas": "214766-78-6",
                            "sub_narkotikaklass": "0",
                            "sub_nslid": "ID1QKOM5K1CBHWLCSR",
                            "sub_snomed": "441864003",
                            "sub_srsunii": "SX0XJI3A11",
                            "sub_versiondtm": "2014-09-25",
                            "kat_substansfarg": "#cdaa7d"
                        }
                    },
                    {
                        "text": "enzalutamid",
                        "id": 5105,
                        "data": {
                            "sub_pk": 5105,
                            "sub_subnamnrek": "enzalutamid",
                            "sub_atc": null,
                            "sub_cas": "915087-33-1",
                            "sub_narkotikaklass": "0",
                            "sub_nslid": "ID6347656300001275",
                            "sub_snomed": null,
                            "sub_srsunii": "93T0T9GKNU",
                            "sub_versiondtm": "2014-09-25",
                            "kat_substansfarg": "#ee9a49"
                        }
                    },
                    {
                        "text": "flutamid",
                        "id": 2093,
                        "data": {
                            "sub_pk": 2093,
                            "sub_subnamnrek": "flutamid",
                            "sub_atc": "QL02BB01",
                            "sub_cas": "13311-84-7",
                            "sub_narkotikaklass": "0",
                            "sub_nslid": "IDE4POEGUA7IJVERT1",
                            "sub_snomed": "387587007",
                            "sub_srsunii": "76W6J0943E",
                            "sub_versiondtm": "2015-01-15",
                            "kat_substansfarg": "#db9370"
                        }
                    },
                    {
                        "text": "goserelin",
                        "id": 2546,
                        "data": {
                            "sub_pk": 2546,
                            "sub_subnamnrek": "goserelin",
                            "sub_atc": "QL02AE03",
                            "sub_cas": "65807-02-5",
                            "sub_narkotikaklass": "0",
                            "sub_nslid": "IDE4POFAUAUR0VERT1",
                            "sub_snomed": "108771008",
                            "sub_srsunii": "0F65R8P09N",
                            "sub_versiondtm": "2014-09-25",
                            "kat_substansfarg": "#cdad00"
                        }
                    },
                    {
                        "text": "histrelin",
                        "id": 2635,
                        "data": {
                            "sub_pk": 2635,
                            "sub_subnamnrek": "histrelin",
                            "sub_atc": "L02AE05",
                            "sub_cas": "76712-82-8",
                            "sub_narkotikaklass": "0",
                            "sub_nslid": "IDE4POFFUAYCXVERT1",
                            "sub_snomed": "109049002",
                            "sub_srsunii": "H50H3S3W74",
                            "sub_versiondtm": "2015-02-11",
                            "kat_substansfarg": "#cd8162"
                        }
                    },
                    {
                        "text": "ketokonazol",
                        "id": 2545,
                        "data": {
                            "sub_pk": 2545,
                            "sub_subnamnrek": "ketokonazol",
                            "sub_atc": "D01AC08",
                            "sub_cas": "65277-42-1",
                            "sub_narkotikaklass": "0",
                            "sub_nslid": "IDE4POFAUAUHUVERT1",
                            "sub_snomed": "387216007",
                            "sub_srsunii": "R9400W927I",
                            "sub_versiondtm": "2015-01-15",
                            "kat_substansfarg": "#8c7853"
                        }
                    },
                    {
                        "text": "leuprorelin",
                        "id": 2430,
                        "data": {
                            "sub_pk": 2430,
                            "sub_subnamnrek": "leuprorelin",
                            "sub_atc": "QL02AE02",
                            "sub_cas": "53714-56-0",
                            "sub_narkotikaklass": "0",
                            "sub_nslid": "IDE4POF2UAONTVERT1",
                            "sub_snomed": "397198002",
                            "sub_srsunii": "EFY6W0M8TG",
                            "sub_versiondtm": "2014-09-25",
                            "kat_substansfarg": "#ee9A00"
                        }
                    },
                    {
                        "text": "polyestradiolfosfat",
                        "id": 2289,
                        "data": {
                            "sub_pk": 2289,
                            "sub_subnamnrek": "polyestradiolfosfat",
                            "sub_atc": "QL02AA02",
                            "sub_cas": "28014-46-2",
                            "sub_narkotikaklass": "0",
                            "sub_nslid": "IDE4POETUAHBTVERT1",
                            "sub_snomed": "126084009",
                            "sub_srsunii": "P14877CDX2",
                            "sub_versiondtm": "2015-01-15",
                            "kat_substansfarg": "#b87333"
                        }
                    },
                    {
                        "text": "triptorelin",
                        "id": 2478,
                        "data": {
                            "sub_pk": 2478,
                            "sub_subnamnrek": "triptorelin",
                            "sub_atc": "QL02AE04",
                            "sub_cas": "57773-63-4",
                            "sub_narkotikaklass": "0",
                            "sub_nslid": "IDE4POF6UARH2VERT1",
                            "sub_snomed": "395915003",
                            "sub_srsunii": "9081Y98W2V",
                            "sub_versiondtm": "2015-01-15",
                            "kat_substansfarg": "#eeb422"
                        }
                    }
                ]
            ],
            "[[\"error\",null],[\"parameters\",[[\"kategori\",\"ProstataProcess_CY\"]]],[\"success\",null],[\"vdlist\",\"LM_substans\"]]": [
                [
                    {
                        "text": "cisplatin",
                        "id": 2139,
                        "data": {
                            "sub_pk": 2139,
                            "sub_subnamnrek": "cisplatin",
                            "sub_atc": "QL01XA01",
                            "sub_cas": "15663-27-1",
                            "sub_narkotikaklass": "0",
                            "sub_nslid": "IDE4POEKUAA72VERT1",
                            "sub_snomed": "387318005",
                            "sub_srsunii": "Q20Q21Q62J",
                            "sub_versiondtm": "2014-09-25",
                            "kat_substansfarg": "#7b68ee"
                        }
                    },
                    {
                        "text": "cyklofosfamid (vattenfri)",
                        "id": 1002,
                        "data": {
                            "sub_pk": 1002,
                            "sub_subnamnrek": "cyklofosfamid (vattenfri)",
                            "sub_atc": "QL01AA01",
                            "sub_cas": "50-18-0",
                            "sub_narkotikaklass": "0",
                            "sub_nslid": "IDE4POBSU8ZUAVERT1",
                            "sub_snomed": null,
                            "sub_srsunii": "6UXW23996M",
                            "sub_versiondtm": "2014-09-25",
                            "kat_substansfarg": "#00bfff"
                        }
                    },
                    {
                        "text": "docetaxel, vattenfri",
                        "id": 3121,
                        "data": {
                            "sub_pk": 3121,
                            "sub_subnamnrek": "docetaxel, vattenfri",
                            "sub_atc": "QL01CD02",
                            "sub_cas": "114977-28-5",
                            "sub_narkotikaklass": "0",
                            "sub_nslid": "IDE4POIAUCB20VERT1",
                            "sub_snomed": null,
                            "sub_srsunii": "699121PHCA",
                            "sub_versiondtm": "2015-01-15",
                            "kat_substansfarg": "#3299cc"
                        }
                    },
                    {
                        "text": "doxorubicin",
                        "id": 2230,
                        "data": {
                            "sub_pk": 2230,
                            "sub_subnamnrek": "doxorubicin",
                            "sub_atc": "QL01DB01",
                            "sub_cas": "23214-92-8",
                            "sub_narkotikaklass": "0",
                            "sub_nslid": "IDE4POEPUAELMVERT1",
                            "sub_snomed": "372817009",
                            "sub_srsunii": "80168379AG",
                            "sub_versiondtm": "2015-01-15",
                            "kat_substansfarg": "#87cefa"
                        }
                    },
                    {
                        "text": "estramustin",
                        "id": 1786,
                        "data": {
                            "sub_pk": 1786,
                            "sub_subnamnrek": "estramustin",
                            "sub_atc": "QL01XX11",
                            "sub_cas": "2998-57-4",
                            "sub_narkotikaklass": "0",
                            "sub_nslid": "IDE4POCSU9RHVVERT1",
                            "sub_snomed": "386909008",
                            "sub_srsunii": "35LT29625A",
                            "sub_versiondtm": "2014-09-25",
                            "kat_substansfarg": "#a2b5cd"
                        }
                    },
                    {
                        "text": "etoposid",
                        "id": 2325,
                        "data": {
                            "sub_pk": 2325,
                            "sub_subnamnrek": "etoposid",
                            "sub_atc": "QL01CB01",
                            "sub_cas": "33419-42-0",
                            "sub_narkotikaklass": "0",
                            "sub_nslid": "IDE4POEVUAJ75VERT1",
                            "sub_snomed": "387316009",
                            "sub_srsunii": "6PLQ3CP4P3",
                            "sub_versiondtm": "2014-09-25",
                            "kat_substansfarg": "#7ac5cd"
                        }
                    },
                    {
                        "text": "fluorouracil",
                        "id": 1029,
                        "data": {
                            "sub_pk": 1029,
                            "sub_subnamnrek": "fluorouracil",
                            "sub_atc": "L01BC02",
                            "sub_cas": "51-21-8",
                            "sub_narkotikaklass": "0",
                            "sub_nslid": "IDE4POBSU9087VERT1",
                            "sub_snomed": "387172005",
                            "sub_srsunii": "U3P01618RT",
                            "sub_versiondtm": "2015-01-15",
                            "kat_substansfarg": "#a4d3ee"
                        }
                    },
                    {
                        "text": "gemcitabin",
                        "id": 2748,
                        "data": {
                            "sub_pk": 2748,
                            "sub_subnamnrek": "gemcitabin",
                            "sub_atc": "QL01BC05",
                            "sub_cas": "95058-81-4",
                            "sub_narkotikaklass": "0",
                            "sub_nslid": "IDE4POFLUB3A4VERT1",
                            "sub_snomed": "386920008",
                            "sub_srsunii": "B76N6SBZ8R",
                            "sub_versiondtm": "2015-01-15",
                            "kat_substansfarg": "#6959cd"
                        }
                    },
                    {
                        "text": "kabazitaxel",
                        "id": 107,
                        "data": {
                            "sub_pk": 107,
                            "sub_subnamnrek": "kabazitaxel",
                            "sub_atc": "QL01CD04",
                            "sub_cas": "183133-96-2",
                            "sub_narkotikaklass": "0",
                            "sub_nslid": "ID16FSULLC436YIMSR",
                            "sub_snomed": "446706007",
                            "sub_srsunii": "51F690397J",
                            "sub_versiondtm": "2015-01-15",
                            "kat_substansfarg": "#00c5cd"
                        }
                    },
                    {
                        "text": "karboplatin",
                        "id": 2377,
                        "data": {
                            "sub_pk": 2377,
                            "sub_subnamnrek": "karboplatin",
                            "sub_atc": "QL01XA02",
                            "sub_cas": "41575-94-4",
                            "sub_narkotikaklass": "0",
                            "sub_nslid": "IDE4POEZUAM6JVERT1",
                            "sub_snomed": "386905002",
                            "sub_srsunii": "BG3F62OND5",
                            "sub_versiondtm": "2015-01-15",
                            "kat_substansfarg": "#7a67ee"
                        }
                    },
                    {
                        "text": "mitoxantron",
                        "id": 2544,
                        "data": {
                            "sub_pk": 2544,
                            "sub_subnamnrek": "mitoxantron",
                            "sub_atc": "QL01DB07",
                            "sub_cas": "65271-80-9",
                            "sub_narkotikaklass": "0",
                            "sub_nslid": "IDE4POFAUAUHKVERT1",
                            "sub_snomed": "386913001",
                            "sub_srsunii": "BZ114NVM5P",
                            "sub_versiondtm": "2015-01-15",
                            "kat_substansfarg": "#1c86ee"
                        }
                    },
                    {
                        "text": "paklitaxel",
                        "id": 3056,
                        "data": {
                            "sub_pk": 3056,
                            "sub_subnamnrek": "paklitaxel",
                            "sub_atc": "QL01CD01",
                            "sub_cas": "33069-62-4",
                            "sub_narkotikaklass": "0",
                            "sub_nslid": "IDE4POI6UC7RSVERT1",
                            "sub_snomed": "387374002",
                            "sub_srsunii": "P88XT4IS4D",
                            "sub_versiondtm": "2015-01-15",
                            "kat_substansfarg": "#5f9ea0"
                        }
                    }
                ],
                [
                    {
                        "text": "cisplatin",
                        "id": 2139,
                        "data": {
                            "sub_pk": 2139,
                            "sub_subnamnrek": "cisplatin",
                            "sub_atc": "QL01XA01",
                            "sub_cas": "15663-27-1",
                            "sub_narkotikaklass": "0",
                            "sub_nslid": "IDE4POEKUAA72VERT1",
                            "sub_snomed": "387318005",
                            "sub_srsunii": "Q20Q21Q62J",
                            "sub_versiondtm": "2014-09-25",
                            "kat_substansfarg": "#7b68ee"
                        }
                    },
                    {
                        "text": "cyklofosfamid (vattenfri)",
                        "id": 1002,
                        "data": {
                            "sub_pk": 1002,
                            "sub_subnamnrek": "cyklofosfamid (vattenfri)",
                            "sub_atc": "QL01AA01",
                            "sub_cas": "50-18-0",
                            "sub_narkotikaklass": "0",
                            "sub_nslid": "IDE4POBSU8ZUAVERT1",
                            "sub_snomed": null,
                            "sub_srsunii": "6UXW23996M",
                            "sub_versiondtm": "2014-09-25",
                            "kat_substansfarg": "#00bfff"
                        }
                    },
                    {
                        "text": "docetaxel, vattenfri",
                        "id": 3121,
                        "data": {
                            "sub_pk": 3121,
                            "sub_subnamnrek": "docetaxel, vattenfri",
                            "sub_atc": "QL01CD02",
                            "sub_cas": "114977-28-5",
                            "sub_narkotikaklass": "0",
                            "sub_nslid": "IDE4POIAUCB20VERT1",
                            "sub_snomed": null,
                            "sub_srsunii": "699121PHCA",
                            "sub_versiondtm": "2015-01-15",
                            "kat_substansfarg": "#3299cc"
                        }
                    },
                    {
                        "text": "doxorubicin",
                        "id": 2230,
                        "data": {
                            "sub_pk": 2230,
                            "sub_subnamnrek": "doxorubicin",
                            "sub_atc": "QL01DB01",
                            "sub_cas": "23214-92-8",
                            "sub_narkotikaklass": "0",
                            "sub_nslid": "IDE4POEPUAELMVERT1",
                            "sub_snomed": "372817009",
                            "sub_srsunii": "80168379AG",
                            "sub_versiondtm": "2015-01-15",
                            "kat_substansfarg": "#87cefa"
                        }
                    },
                    {
                        "text": "estramustin",
                        "id": 1786,
                        "data": {
                            "sub_pk": 1786,
                            "sub_subnamnrek": "estramustin",
                            "sub_atc": "QL01XX11",
                            "sub_cas": "2998-57-4",
                            "sub_narkotikaklass": "0",
                            "sub_nslid": "IDE4POCSU9RHVVERT1",
                            "sub_snomed": "386909008",
                            "sub_srsunii": "35LT29625A",
                            "sub_versiondtm": "2014-09-25",
                            "kat_substansfarg": "#a2b5cd"
                        }
                    },
                    {
                        "text": "etoposid",
                        "id": 2325,
                        "data": {
                            "sub_pk": 2325,
                            "sub_subnamnrek": "etoposid",
                            "sub_atc": "QL01CB01",
                            "sub_cas": "33419-42-0",
                            "sub_narkotikaklass": "0",
                            "sub_nslid": "IDE4POEVUAJ75VERT1",
                            "sub_snomed": "387316009",
                            "sub_srsunii": "6PLQ3CP4P3",
                            "sub_versiondtm": "2014-09-25",
                            "kat_substansfarg": "#7ac5cd"
                        }
                    },
                    {
                        "text": "fluorouracil",
                        "id": 1029,
                        "data": {
                            "sub_pk": 1029,
                            "sub_subnamnrek": "fluorouracil",
                            "sub_atc": "L01BC02",
                            "sub_cas": "51-21-8",
                            "sub_narkotikaklass": "0",
                            "sub_nslid": "IDE4POBSU9087VERT1",
                            "sub_snomed": "387172005",
                            "sub_srsunii": "U3P01618RT",
                            "sub_versiondtm": "2015-01-15",
                            "kat_substansfarg": "#a4d3ee"
                        }
                    },
                    {
                        "text": "gemcitabin",
                        "id": 2748,
                        "data": {
                            "sub_pk": 2748,
                            "sub_subnamnrek": "gemcitabin",
                            "sub_atc": "QL01BC05",
                            "sub_cas": "95058-81-4",
                            "sub_narkotikaklass": "0",
                            "sub_nslid": "IDE4POFLUB3A4VERT1",
                            "sub_snomed": "386920008",
                            "sub_srsunii": "B76N6SBZ8R",
                            "sub_versiondtm": "2015-01-15",
                            "kat_substansfarg": "#6959cd"
                        }
                    },
                    {
                        "text": "kabazitaxel",
                        "id": 107,
                        "data": {
                            "sub_pk": 107,
                            "sub_subnamnrek": "kabazitaxel",
                            "sub_atc": "QL01CD04",
                            "sub_cas": "183133-96-2",
                            "sub_narkotikaklass": "0",
                            "sub_nslid": "ID16FSULLC436YIMSR",
                            "sub_snomed": "446706007",
                            "sub_srsunii": "51F690397J",
                            "sub_versiondtm": "2015-01-15",
                            "kat_substansfarg": "#00c5cd"
                        }
                    },
                    {
                        "text": "karboplatin",
                        "id": 2377,
                        "data": {
                            "sub_pk": 2377,
                            "sub_subnamnrek": "karboplatin",
                            "sub_atc": "QL01XA02",
                            "sub_cas": "41575-94-4",
                            "sub_narkotikaklass": "0",
                            "sub_nslid": "IDE4POEZUAM6JVERT1",
                            "sub_snomed": "386905002",
                            "sub_srsunii": "BG3F62OND5",
                            "sub_versiondtm": "2015-01-15",
                            "kat_substansfarg": "#7a67ee"
                        }
                    },
                    {
                        "text": "mitoxantron",
                        "id": 2544,
                        "data": {
                            "sub_pk": 2544,
                            "sub_subnamnrek": "mitoxantron",
                            "sub_atc": "QL01DB07",
                            "sub_cas": "65271-80-9",
                            "sub_narkotikaklass": "0",
                            "sub_nslid": "IDE4POFAUAUHKVERT1",
                            "sub_snomed": "386913001",
                            "sub_srsunii": "BZ114NVM5P",
                            "sub_versiondtm": "2015-01-15",
                            "kat_substansfarg": "#1c86ee"
                        }
                    },
                    {
                        "text": "paklitaxel",
                        "id": 3056,
                        "data": {
                            "sub_pk": 3056,
                            "sub_subnamnrek": "paklitaxel",
                            "sub_atc": "QL01CD01",
                            "sub_cas": "33069-62-4",
                            "sub_narkotikaklass": "0",
                            "sub_nslid": "IDE4POI6UC7RSVERT1",
                            "sub_snomed": "387374002",
                            "sub_srsunii": "P88XT4IS4D",
                            "sub_versiondtm": "2015-01-15",
                            "kat_substansfarg": "#5f9ea0"
                        }
                    }
                ],
                [
                    {
                        "text": "cisplatin",
                        "id": 2139,
                        "data": {
                            "sub_pk": 2139,
                            "sub_subnamnrek": "cisplatin",
                            "sub_atc": "QL01XA01",
                            "sub_cas": "15663-27-1",
                            "sub_narkotikaklass": "0",
                            "sub_nslid": "IDE4POEKUAA72VERT1",
                            "sub_snomed": "387318005",
                            "sub_srsunii": "Q20Q21Q62J",
                            "sub_versiondtm": "2014-09-25",
                            "kat_substansfarg": "#7b68ee"
                        }
                    },
                    {
                        "text": "cyklofosfamid (vattenfri)",
                        "id": 1002,
                        "data": {
                            "sub_pk": 1002,
                            "sub_subnamnrek": "cyklofosfamid (vattenfri)",
                            "sub_atc": "QL01AA01",
                            "sub_cas": "50-18-0",
                            "sub_narkotikaklass": "0",
                            "sub_nslid": "IDE4POBSU8ZUAVERT1",
                            "sub_snomed": null,
                            "sub_srsunii": "6UXW23996M",
                            "sub_versiondtm": "2014-09-25",
                            "kat_substansfarg": "#00bfff"
                        }
                    },
                    {
                        "text": "docetaxel, vattenfri",
                        "id": 3121,
                        "data": {
                            "sub_pk": 3121,
                            "sub_subnamnrek": "docetaxel, vattenfri",
                            "sub_atc": "QL01CD02",
                            "sub_cas": "114977-28-5",
                            "sub_narkotikaklass": "0",
                            "sub_nslid": "IDE4POIAUCB20VERT1",
                            "sub_snomed": null,
                            "sub_srsunii": "699121PHCA",
                            "sub_versiondtm": "2015-01-15",
                            "kat_substansfarg": "#3299cc"
                        }
                    },
                    {
                        "text": "doxorubicin",
                        "id": 2230,
                        "data": {
                            "sub_pk": 2230,
                            "sub_subnamnrek": "doxorubicin",
                            "sub_atc": "QL01DB01",
                            "sub_cas": "23214-92-8",
                            "sub_narkotikaklass": "0",
                            "sub_nslid": "IDE4POEPUAELMVERT1",
                            "sub_snomed": "372817009",
                            "sub_srsunii": "80168379AG",
                            "sub_versiondtm": "2015-01-15",
                            "kat_substansfarg": "#87cefa"
                        }
                    },
                    {
                        "text": "estramustin",
                        "id": 1786,
                        "data": {
                            "sub_pk": 1786,
                            "sub_subnamnrek": "estramustin",
                            "sub_atc": "QL01XX11",
                            "sub_cas": "2998-57-4",
                            "sub_narkotikaklass": "0",
                            "sub_nslid": "IDE4POCSU9RHVVERT1",
                            "sub_snomed": "386909008",
                            "sub_srsunii": "35LT29625A",
                            "sub_versiondtm": "2014-09-25",
                            "kat_substansfarg": "#a2b5cd"
                        }
                    },
                    {
                        "text": "etoposid",
                        "id": 2325,
                        "data": {
                            "sub_pk": 2325,
                            "sub_subnamnrek": "etoposid",
                            "sub_atc": "QL01CB01",
                            "sub_cas": "33419-42-0",
                            "sub_narkotikaklass": "0",
                            "sub_nslid": "IDE4POEVUAJ75VERT1",
                            "sub_snomed": "387316009",
                            "sub_srsunii": "6PLQ3CP4P3",
                            "sub_versiondtm": "2014-09-25",
                            "kat_substansfarg": "#7ac5cd"
                        }
                    },
                    {
                        "text": "fluorouracil",
                        "id": 1029,
                        "data": {
                            "sub_pk": 1029,
                            "sub_subnamnrek": "fluorouracil",
                            "sub_atc": "L01BC02",
                            "sub_cas": "51-21-8",
                            "sub_narkotikaklass": "0",
                            "sub_nslid": "IDE4POBSU9087VERT1",
                            "sub_snomed": "387172005",
                            "sub_srsunii": "U3P01618RT",
                            "sub_versiondtm": "2015-01-15",
                            "kat_substansfarg": "#a4d3ee"
                        }
                    },
                    {
                        "text": "gemcitabin",
                        "id": 2748,
                        "data": {
                            "sub_pk": 2748,
                            "sub_subnamnrek": "gemcitabin",
                            "sub_atc": "QL01BC05",
                            "sub_cas": "95058-81-4",
                            "sub_narkotikaklass": "0",
                            "sub_nslid": "IDE4POFLUB3A4VERT1",
                            "sub_snomed": "386920008",
                            "sub_srsunii": "B76N6SBZ8R",
                            "sub_versiondtm": "2015-01-15",
                            "kat_substansfarg": "#6959cd"
                        }
                    },
                    {
                        "text": "kabazitaxel",
                        "id": 107,
                        "data": {
                            "sub_pk": 107,
                            "sub_subnamnrek": "kabazitaxel",
                            "sub_atc": "QL01CD04",
                            "sub_cas": "183133-96-2",
                            "sub_narkotikaklass": "0",
                            "sub_nslid": "ID16FSULLC436YIMSR",
                            "sub_snomed": "446706007",
                            "sub_srsunii": "51F690397J",
                            "sub_versiondtm": "2015-01-15",
                            "kat_substansfarg": "#00c5cd"
                        }
                    },
                    {
                        "text": "karboplatin",
                        "id": 2377,
                        "data": {
                            "sub_pk": 2377,
                            "sub_subnamnrek": "karboplatin",
                            "sub_atc": "QL01XA02",
                            "sub_cas": "41575-94-4",
                            "sub_narkotikaklass": "0",
                            "sub_nslid": "IDE4POEZUAM6JVERT1",
                            "sub_snomed": "386905002",
                            "sub_srsunii": "BG3F62OND5",
                            "sub_versiondtm": "2015-01-15",
                            "kat_substansfarg": "#7a67ee"
                        }
                    },
                    {
                        "text": "mitoxantron",
                        "id": 2544,
                        "data": {
                            "sub_pk": 2544,
                            "sub_subnamnrek": "mitoxantron",
                            "sub_atc": "QL01DB07",
                            "sub_cas": "65271-80-9",
                            "sub_narkotikaklass": "0",
                            "sub_nslid": "IDE4POFAUAUHKVERT1",
                            "sub_snomed": "386913001",
                            "sub_srsunii": "BZ114NVM5P",
                            "sub_versiondtm": "2015-01-15",
                            "kat_substansfarg": "#1c86ee"
                        }
                    },
                    {
                        "text": "paklitaxel",
                        "id": 3056,
                        "data": {
                            "sub_pk": 3056,
                            "sub_subnamnrek": "paklitaxel",
                            "sub_atc": "QL01CD01",
                            "sub_cas": "33069-62-4",
                            "sub_narkotikaklass": "0",
                            "sub_nslid": "IDE4POI6UC7RSVERT1",
                            "sub_snomed": "387374002",
                            "sub_srsunii": "P88XT4IS4D",
                            "sub_versiondtm": "2015-01-15",
                            "kat_substansfarg": "#5f9ea0"
                        }
                    }
                ]
            ],
            "[[\"error\",null],[\"parameters\",[[\"kategori\",\"ProstataProcess_IS\"]]],[\"success\",null],[\"vdlist\",\"LM_substans\"]]": [
                [
                    {
                        "text": "radium(Ra-223)diklorid",
                        "id": 5107,
                        "data": {
                            "sub_pk": 5107,
                            "sub_subnamnrek": "radium(Ra-223)diklorid",
                            "sub_atc": "QV10XX03",
                            "sub_cas": "444811-40-9",
                            "sub_narkotikaklass": "0",
                            "sub_nslid": "ID6349643612217999",
                            "sub_snomed": null,
                            "sub_srsunii": "RJ00KV3VTG",
                            "sub_versiondtm": "2016-02-12",
                            "kat_substansfarg": "#ffd700"
                        }
                    },
                    {
                        "text": "samarium (Sm-153) lexidronampentanatrium",
                        "id": 963,
                        "data": {
                            "sub_pk": 963,
                            "sub_subnamnrek": "samarium (Sm-153) lexidronampentanatrium",
                            "sub_atc": "QV10BX02",
                            "sub_cas": "160369-78-8",
                            "sub_narkotikaklass": "0",
                            "sub_nslid": "IDA0MX49OEWLPCMSRV",
                            "sub_snomed": null,
                            "sub_srsunii": "7389YR3OOV",
                            "sub_versiondtm": "2015-06-24",
                            "kat_substansfarg": "#d9d919"
                        }
                    },
                    {
                        "text": "strontium (Sr-89) klorid",
                        "id": 3021,
                        "data": {
                            "sub_pk": 3021,
                            "sub_subnamnrek": "strontium (Sr-89) klorid",
                            "sub_atc": "QV10BX01",
                            "sub_cas": "38270-90-5",
                            "sub_narkotikaklass": "0",
                            "sub_nslid": "IDE4POI4UC66RVERT1",
                            "sub_snomed": "125701003",
                            "sub_srsunii": "5R78837D4A",
                            "sub_versiondtm": "2015-01-15",
                            "kat_substansfarg": "#ffd700"
                        }
                    }
                ]
            ],
            "[[\"error\",null],[\"parameters\",[[\"kategori\",\"ProstataProcess_BB\"]]],[\"success\",null],[\"vdlist\",\"LM_substans\"]]": [
                [
                    {
                        "text": "alendronsyra",
                        "id": 2558,
                        "data": {
                            "sub_pk": 2558,
                            "sub_subnamnrek": "alendronsyra",
                            "sub_atc": "M05BA04",
                            "sub_cas": "66376-36-1",
                            "sub_narkotikaklass": "0",
                            "sub_nslid": "IDE4POFBUAUZXVERT1",
                            "sub_snomed": "391730008",
                            "sub_srsunii": "X1J18R4W8P",
                            "sub_versiondtm": "2015-02-11",
                            "kat_substansfarg": "#458b00"
                        }
                    },
                    {
                        "text": "denosumab",
                        "id": 87,
                        "data": {
                            "sub_pk": 87,
                            "sub_subnamnrek": "denosumab",
                            "sub_atc": "QM05BX04",
                            "sub_cas": "615258-40-7",
                            "sub_narkotikaklass": "0",
                            "sub_nslid": "ID166VJMO1164ISJMS",
                            "sub_snomed": "446321003",
                            "sub_srsunii": "4EQZ6YO2HI",
                            "sub_versiondtm": "2015-10-09",
                            "kat_substansfarg": "#2e8b57"
                        }
                    },
                    {
                        "text": "pamidronatdinatrium, vattenfri",
                        "id": 2801,
                        "data": {
                            "sub_pk": 2801,
                            "sub_subnamnrek": "pamidronatdinatrium, vattenfri",
                            "sub_atc": "QM05BA03",
                            "sub_cas": "57248-88-1",
                            "sub_narkotikaklass": "0",
                            "sub_nslid": "IDE4POGSUB5JXVERT1",
                            "sub_snomed": null,
                            "sub_srsunii": "C7S8VWP5DH",
                            "sub_versiondtm": "2014-11-09",
                            "kat_substansfarg": "#32cd99"
                        }
                    },
                    {
                        "text": "zoledronsyra (vattenfri)",
                        "id": 2819,
                        "data": {
                            "sub_pk": 2819,
                            "sub_subnamnrek": "zoledronsyra (vattenfri)",
                            "sub_atc": "QM05BA08",
                            "sub_cas": "118072-93-8",
                            "sub_narkotikaklass": "0",
                            "sub_nslid": "IDE4POGUUB6FMVERT1",
                            "sub_snomed": null,
                            "sub_srsunii": "70HZ18PH24",
                            "sub_versiondtm": "2015-04-22",
                            "kat_substansfarg": "#7ccd7c"
                        }
                    }
                ]
            ],
            "[[\"error\",null],[\"parameters\",[[\"kategori\",\"ProstataProcess_OC\"]]],[\"success\",null],[\"vdlist\",\"LM_substans\"]]": [
                [
                    {
                        "text": "betametason",
                        "id": 1503,
                        "data": {
                            "sub_pk": 1503,
                            "sub_subnamnrek": "betametason",
                            "sub_atc": "A07EA04",
                            "sub_cas": "378-44-9",
                            "sub_narkotikaklass": "0",
                            "sub_nslid": "IDE4POCAU9DR0VERT1",
                            "sub_snomed": "116571008",
                            "sub_srsunii": "9842X06Q6M",
                            "sub_versiondtm": "2015-01-15",
                            "kat_substansfarg": "#da70d6"
                        }
                    },
                    {
                        "text": "dexametason",
                        "id": 995,
                        "data": {
                            "sub_pk": 995,
                            "sub_subnamnrek": "dexametason",
                            "sub_atc": "A01AC02",
                            "sub_cas": "50-02-2",
                            "sub_narkotikaklass": "0",
                            "sub_nslid": "IDE4POBSU8ZR8VERT1",
                            "sub_snomed": "372584003",
                            "sub_srsunii": "7S5I7G3JQL",
                            "sub_versiondtm": "2015-01-15",
                            "kat_substansfarg": "#d15fee"
                        }
                    },
                    {
                        "text": "prednisolon",
                        "id": 1005,
                        "data": {
                            "sub_pk": 1005,
                            "sub_subnamnrek": "prednisolon",
                            "sub_atc": "A07EA01",
                            "sub_cas": "50-24-8",
                            "sub_narkotikaklass": "0",
                            "sub_nslid": "IDE4POBSU8ZVEVERT1",
                            "sub_snomed": "116601002",
                            "sub_srsunii": "9PHQ9Y1OLM",
                            "sub_versiondtm": "2015-01-15",
                            "kat_substansfarg": "#cd69c9"
                        }
                    }
                ]
            ]
        },
        "getRegisterRecordData": {}
    }
}
} 
		)