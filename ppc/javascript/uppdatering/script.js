(function($) {
	/**
	 * Extender för touch
	 *
	 * @extender touch
	 */
	ko.extenders.touch = function(target, table) {
		target.subscribe(function(newValue) {
		   vm.touchForm();
		   vm.touchTable(table);
		});
		return target;
	};

	ko.extenders.crpc = function(target, data) {
		if(target.rcc.regvarName == 'B_besokdatum') {
			target.subscribe(function(newValue) {
				if(data.B_crpc() && data.B_crpc().value == '1')
					vm.U_crpcdatum(data.B_besokdatum()); 
			});
		}
		if(target.rcc.regvarName == 'B_crpc') {
			target.subscribe(function(newValue) {
				if((newValue && newValue.value == '1') && data.B_besokdatum())
					vm.U_crpcdatum(data.B_besokdatum()); 
				if((!newValue || newValue.value != '1') && vm.U_crpcdatum())
					vm.U_crpcdatum(undefined);
			});
		}
		return target;
	};

	ko.extenders.pallHem = function(target, data) {
		if(target.rcc.regvarName == 'B_besokdatum') {
			target.subscribe(function(newValue) {
				if(data.B_palliativvard() && data.B_palliativvard().value == '1') {
					vm.U_pallHemSjkvrd(data.B_besokdatum());
				}
			});
		}
		if(target.rcc.regvarName == 'B_palliativvard') {
			target.subscribe(function(newValue) {
				if((newValue && newValue.value == '1') && data.B_besokdatum()) {
					vm.U_pallHemSjkvrd(data.B_besokdatum());
				}
				if((!newValue || newValue.value != '1') && vm.U_pallHemSjkvrd()) {
					vm.U_pallHemSjkvrd(undefined);
				}
			});
		}
		return target;
	};
	
	ko.extenders.firstMetastasis = function(target, parent) {
		if(target.rcc.regvarName == 'BDM_metastaslokal') {
			target.subscribe(function(newValue) {
				if(newValue) {
					if(parent.BD_datum()) {
						if(vm.U_metastasdatum()) {
							if(vm.U_npcr_fk() && vm.getSelectedNPCR(vm.U_npcr_fk()) && vm.getSelectedNPCR(vm.U_npcr_fk()).D_Mstad_value != 1) {
								var currentDate = new Date(vm.U_metastasdatum());
								var bdDate = new Date(parent.BD_datum());
								if(currentDate.getTime() > bdDate.getTime()) {
									vm.U_metastasdatum(parent.BD_datum());
								}
							}
						} else {
							vm.U_metastasdatum(parent.BD_datum());
						}
					}
				}
			});
		}
		return target;
	};
	
	ko.extenders.hormonStart = function(target, parent) {
		if(target.rcc.regvarName == 'LMIH_handelsedatum') {
			target.subscribe(function(newValue) {
				if(newValue) {
					if(parent.LM_typ().value == '1') {
						if(vm.U_hormonstartdatum()) {
							var currentDate = new Date(vm.U_hormonstartdatum());
							var lmDate = new Date(newValue);
							if(currentDate.getTime() > lmDate.getTime()) {
								vm.U_hormonstartdatum(newValue);
							}
						} else {
							vm.U_hormonstartdatum(newValue);
						}
					}
				}
			});
		}
		return target;
	};

	ko.extenders.sre = function(target, parent) {
		if(target.rcc.regvarName == 'SBGL_lokal') {
			target.subscribe(function(newValue) {
				if(newValue && newValue.value == '2') {
					vm.getAddDialog('#rcc-add-SRE').done( function() {
						if(parent.SB_slutdatum() != null) {
							vm.addSREStral(parent.SB_slutdatum());
						} else {
							vm.getInfoDialogJS('#rcc-sre-info');
						}
					}).fail(function() {
						
					});
				}
			});
		}
		return target;
	};

	ko.extenders.ejBed = function(target, data) {
		if(target.rcc.regvarName == 'B_klinEjBed') {
			target.subscribe(function(newValue) {
				if(newValue) {
					clear(data.B_klinbed);
				}
			});
		}
		return target;
	};

})(jQuery);


$(document).ready(
function ( )
{
/**
 *  Initialize viewmodel
 */
var validering = new RCC.Validation();
var vm = new RCC.ViewModel( { validation: validering } );
window.vm = vm;

// deaktivera formulärvalidering
vm.$validation.enabled(false);

	setTimeout(function() {
		$( "#rcc-progressbar" ).dialog({
		  height: 60,
		  width: 245,
		  modal: true
		});
	}, 1);
	
	var formWidth = $('#rcc-form').width();

/**
 *  LOKALA FORMULÄR VARIABLER
 */
vm.U_npcr_fk.listValues = ko.observableArray(undefined);

vm.PSA_funktioner_help = "För att beräkna PSA-dubbleringstid, håll in Ctrl knappen på tangentbordet och klicka på den punkt som du vill ha som startpunkt.<br>För att beräkna PSA-halveringstiden, håll in Alt knappen på tangentbordet och klicka på den punkt som du vill ha som startpunkt.<br><br>För att ta bort den uträknade punkten för dubblering eller halveringstid, håll in Ctrl och klicka på punkten.";
vm.zoom_help = "Klicka på och dra längs med tidsaxeln för att zooma i grafen.<br><br>När du zoomat in kan du återgå till ursprungligt läge med \"Återställ\" knappen.";
vm.linjar_skala_help = "Den linjära skalan är bra att använda när man har en liten variation bland PSA-mätvärdena.<br><br>Den är som namnet säger linjär och är således lättare att tolka för ögat än den logaritmiska skalan, men kan vara svår att läsa om det är både väldigt höga och väldigt låga mätvärden.";
vm.log_skala_help = "Den logaritmiska skalan är bra att använda när man har stor variation bland PSA-mätvärdena.<br><br>Den får kurvan att se jämnare ut, vilket gör den mer svårtolkad för ögat, men visar på skillnader både i liten och stor skala för små och stora mätvärden.";

vm.psa_help = "Efter radikal prostatektomi:  Två konsekutivt ökande PSA – värden över > 0.2 ug/l.<br><br>Efter kurativt syftande radioterapi: PSA-värdet > 2.0 ug/l från nadir efter strålbehandling";
vm.crpc_help = "Datum för kastrationsresistent sjukdom enligt Nationella vårdprogrammet<br><br>Plasmatestosteronvärde &lt; 1.7 nmol/l (50 ng/dl) under pågående behandling med GnRH analog eller orkidektomi samt att det finns ett eller flera av nedanstående tecken på progress minst 6 veckor efter att en eventuell bikalutamidbehandling har avslutats.<br><br><ol><li>Lokal progress värderad genom palpation eller bilddiagnostik</li><li>Nytillkomna eller tillväxande metastaser</li><li>2 på varandra stigande PSA värden över 2 ug/l.</li></ol>";
vm.klinbed_help = "Komplett respons – omätbart PSA &lt; 0.1 och normalt ALP, ej dekterbara metastaser, asymtomatisk<br><br>Partiell respons – Pat som ej uppfyller kriterier för komplett respons, stationär sjukdom eller progress.<br><br>Stationär sjukdom – stabilt PSA (&lt; 25% minskning) samt ALP, inga nytillkomna metastaser eller nytillkomna cancerrelaterade symtom.<br><br>Progress – stigande PSA och/eller ALP , nytillkomna metastaser, ökade cancerrelaterade symtom.<br><br>Sjuksköterskebesök";
vm.multidisc_help = "Multidisciplinär konferens där urolog, onkolog och kontaktsjuksköterska deltar";
vm.samtycke_help = "Godkänner patienten överföring av data till kvalitetsregister";

vm.overviewHighChartLabels = null;
vm.overviewHighChart = null;

vm.isCRPCSet = ko.observable(undefined);

vm.formHasChanged = ko.observable(false);
vm.overviewHighChartHasChanged = ko.observable(false);
vm.rootHasChanged = ko.observable(true);
vm.tableBHasChanged = ko.observable(true);
vm.tableBDHasChanged = ko.observable(true);
vm.tableSBHasChanged = ko.observable(true);
vm.tableSEHasChanged = ko.observable(true);
vm.tableLPHasChanged = ko.observable(true);
vm.tableLMHasChanged = ko.observable(true);
vm.tablePROMHasChanged = ko.observable(true);

var requestCount = ko.observable(0);

/**
 *  INCA FUNKTIONER
 */


/**
* Initializera NPCR lista från NPCR värdedomän
*/
requestCount( requestCount() + 1 );
inca.form.getValueDomainValues(
{
    vdlist: "U_npcr_fk",
    parameters: { patientid: inca.form.env._PATIENTID },
    success:
        function (data)
        {
			if(data.length == 1) {
				vm.U_npcr_fk.listValues(data);
				vm.U_npcr_fk(data[0].id);
			} else if(data.lenght > 1) {
				vm.U_npcr_fk.listValues(data);
			}
    		requestCount( requestCount() - 1 );
        },
	error:
		function ( err )
		{
			requestCount( requestCount() - 1 );
			$('<div title="Ett fel har uppstått"></div>')
				.text( 'Lyckades inte ladda rader från värdedomänen ”NPCR”: ' + err + ', ' + JSON.stringify(err) + '. Försök att ladda om formuläret.')
				.dialog( { buttons: { OK: function ( ) { $(this).close(); } } } );
			$(inca.form.getContainer()).hide();
		}
});


/**
 *  LOKALA FUNKTIONER
 */


/**
* Funktion för förberedande åtgärder innan formulärets initializeras
*
* @method runFunctionsAfterLoad
*/
vm.runFunctionsOnLoad = function() {

		var helpImgSrc = '../../Public/Files/uppsala/common-libs/icons/info.png';
		var zoomImgSrc = '../../Public/Files/uppsala/common-libs/icons/mag.jpg';

        $('img.rcc-help-icon').each(function() {
            $(this).attr('src', helpImgSrc);
        });
		$('img.rcc-zoom-icon').each(function() {
            $(this).attr('src', zoomImgSrc);
        });
    
	// initializera substanslista för samtliga läkemedel
	ko.utils.arrayForEach(vm.$$.Läkemedel(), function (lm) {
		lm.LM_substans.listValues = ko.observableArray(undefined);
		
		if(lm.LM_typ()){
			if(lm.LM_typ().value == '0')
				vm.loadSubstances(lm, 'ProstataProcess_CY');
			else if(lm.LM_typ().value == '1')
				vm.loadSubstances(lm, 'ProstataProcess_ET');
			else if(lm.LM_typ().value == '2')
				vm.loadSubstances(lm, 'ProstataProcess_BB');
			else if(lm.LM_typ().value == '3')
				vm.loadSubstances(lm, 'ProstataProcess_IS');
			else if(lm.LM_typ().value == '4')
				vm.loadSubstances(lm, 'ProstataProcess_OC');
		}
		
	});
	
	if(vm.U_crpcdatum())
		vm.isCRPCSet(true);
	
	// initializera formulärtabbar
	$("#prostataprocess-tabs").tabs({
    	select: function(event, ui) {
        	if(ui.index == 0) {
				//var start = new Date().getTime();  // log start timestamp
				
				vm.overviewHighChartHasChanged(false);
				//vm.chartInitialized(false);
				
				if(vm.rootHasChanged()) {
					redrawOverviewRootChart(vm);
					vm.rootHasChanged(false);
					vm.overviewHighChartHasChanged(true);
				}
				if(vm.tableBHasChanged()) {
					redrawOverviewBChart(vm);
					vm.tableBHasChanged(false);
					vm.overviewHighChartHasChanged(true);
				}
				if(vm.tableBDHasChanged()) {
					redrawOverviewBDChart(vm);
					vm.tableBDHasChanged(false);
					vm.overviewHighChartHasChanged(true);
				}
				if(vm.tableSBHasChanged()) {
					redrawOverviewSBChart(vm);
					vm.tableSBHasChanged(false);
					vm.overviewHighChartHasChanged(true);
				}
				if(vm.tableSEHasChanged()) {
					redrawOverviewSEChart(vm);
					vm.tableSEHasChanged(false);
					vm.overviewHighChartHasChanged(true);
				}
				if(vm.tableLPHasChanged()) {
					redrawOverviewPSAChart(vm);
					redrawOverviewALPChart(vm);
					redrawOverviewKreaChart(vm);
					redrawOverviewHbChart(vm);
					redrawOverviewTestosteronChart(vm);
					vm.tableLPHasChanged(false);
					vm.overviewHighChartHasChanged(true);
				}
				if(vm.tableLMHasChanged()) {
					redrawOverviewLMCharts(vm);
					vm.tableLMHasChanged(false);
					vm.overviewHighChartHasChanged(true);
				}
				if(vm.tablePROMHasChanged()) {
					redrawOverviewPROMChart(vm);
					vm.tablePROMHasChanged(false);
					vm.overviewHighChartHasChanged(true);
				}
				if(vm.overviewHighChartHasChanged()) {
				
					vm.overviewHighChart.redraw();
					setTimeout(function() {
						vm.redrawLegend();
					}, 1);
					
				}
			}
   		}
		
	});

};


/**
* Funktion för åtgärder efter att formuläret har initializeras
*
* @method runFunctionsAfterLoad
*/
vm.runFunctionsAfterLoad = function() {

	// initiera charts
	initializeOverviewHighChart(vm);
	
	// finns ett senast sparat datum
	if(vm.U_datum()) {
		var datumsplit = vm.U_datum().split("-");
		// lägg till sensast sparad till overview chart
		vm.overviewHighChart.xAxis[0].addPlotLine({id: 'saved-plotline', value: Date.UTC(datumsplit[0], (datumsplit[1] - 1), (datumsplit[2])), width: 1, color: '#bdbdbd', dashStyle: 'shortdash', zIndex: 2, label: { align: 'right', textAlign: 'right', x: -5, rotation: 0, style:{fontSize:'10px',color:'#aaa'}, text: 'Senast sparad<br>' + vm.U_datum()}});
		// add invisible point to senast sparad series
		vm.overviewHighChart.series[0].setData([{x: Date.UTC(datumsplit[0], (datumsplit[1] - 1), datumsplit[2]), y: 0}]);
	}
	
	// sätt datum för första metastas för M1 (om ej tidigare satt)
	if(vm.U_npcr_fk() && (!vm.U_metastasdatum() && vm.getSelectedNPCR(vm.U_npcr_fk()).D_Mstad_value == "1"))
		vm.U_metastasdatum(vm.getSelectedNPCR(vm.U_npcr_fk()).D_DiaDat)
	
	// initializera subscribers
	ko.utils.arrayForEach(vm.$$.Besök(), function (ab) {
		vm.setAterbesokSubscribers(ab);
	});
	ko.utils.arrayForEach(vm.$$.Bilddiagnostik(), function (bd) {
		ko.utils.arrayForEach(bd.$$.BD_Metastas(), function (metastas) {
			vm.setBDMetastasSubscribers(metastas);
		});
	});
	ko.utils.arrayForEach(vm.$$.Strålbehandling(), function (sb) {
		ko.utils.arrayForEach(sb.$$.SB_Grupp(), function (grupp) {
			ko.utils.arrayForEach(grupp.$$.SBG_Lokal(), function (lokal) {
				vm.setSBLokalSubscribers(lokal);
			});
		});
	});
	ko.utils.arrayForEach(vm.$$.Läkemedel(), function (lm) {
		vm.setLakemedelSubscribers(lm);
	});
	
	$('#prostataprocess-tabs').tabs('select', 1);
	$('#prostataprocess-tabs').tabs('select', 0);
	
	// initializera läkemedelstabbar
	$('.accordion').accordion({collapsible:true, active: 0, autoHeight: false});
	$('.LM-accordion').accordion({collapsible:true, active: 0, autoHeight: false});
	
	$('#prostataprocess-lmtabs').tabs();
	
	setTimeout(function() {
		$( "#rcc-progressbar" ).dialog( "destroy" );
		$('#rcc-form').removeClass('hidden');
	}, 1);
};


/**
* Funktion för att ladda substanslista från Substans/Kategori värdedomän.
*
* @method loadSubstances
* @param {String} lm
* @param {String} category
* @return {Object} Dataobject 
*/
vm.loadSubstances = function(lm, category) {
	requestCount( requestCount() + 1 );
	inca.form.getValueDomainValues(
	{
	    vdlist: "LM_substans",
	    parameters: { kategori: category },
	    success:
	        function (data)
	        {
				lm.LM_substans.listValues(data);
				requestCount( requestCount() - 1 );
	        },
		error:
			function ( err )
			{
				requestCount( requestCount() - 1 );
				$('<div title="Ett fel har uppstått"></div>')
					.text( 'Lyckades inte ladda rader från värdedomänen ”Substans/Kategori”: ' + err + ', ' + JSON.stringify(err) + '. Försök att ladda om formuläret.')
					.dialog( { buttons: { OK: function ( ) { $(this).dialog('close'); } } } ).position( { my: 'left', at: 'right', of: target } );
				$(inca.form.getContainer()).hide();
			}
	});
};


/**
* Funktion för att returnera en registerpost från NPCR värdedomän som ett dataobjekt.
*
* @method getSelectedNPCR
* @param {String} pk Primärnyckelvärde för dataobjekt 
* @return {Object} Dataobject 
*/
vm.getSelectedNPCR = function(pk) {
	if (pk != null) {
		var item = ko.utils.arrayFirst(vm.U_npcr_fk.listValues() || [], function (item) {
			return item.id == pk;
		});
		
		if(!item) 
			return undefined;
			
		return item.data;
	}
	else {
		return undefined;
	}
};


/**
* Funktion för att returnera en registerpost från substans/kategori värdedomän som ett dataobjekt.
*
* @method getSelectedSubstance
* @param {String} substance Substans variabel
* @return {Object} Dataobject 
*/
vm.getSelectedSubstance = function(substance) {
	if (substance != null) {
		var item = ko.utils.arrayFirst(substance.listValues() || [], function (item) {
			return item.id == substance();
		});
		
		if(!item) 
			return undefined;
			
		return item.data;
	}
	else {
		return undefined;
	}
};


/* ÅTERBEÖK */


/**
* Funktion för att lägga till ett Besök
* subscribers sätts sedan på berörda variabler
*
* @method addAterbesok
* @param {Object} data Dataobject
* @param {Object} event Eventobjekt
*/
vm.addAterbesok = function(data, event) {
    // lägg till en ny rad till Besökstabellen
    var item = vm.$$.Besök.rcc.add();
	
	//item.B_lakare(inca.user.firstName + " " + inca.user.lastName);
	
	// initializera subscribers
	vm.setAterbesokSubscribers(item);
	
	// uppdatera accordion för tabellen
	vm.accordTable('#B-accordion', true);
	
};


/**
* Funktion för att sätta 'Vårdgivare' till inloggad användare
*
* @method setName
* @param {Object} data Dataobject
* @param {Object} event Eventobjekt
*/
vm.setName = function(data, event) {
    data.B_lakare(inca.user.firstName + " " + inca.user.lastName);
};


/**
* Funktion för att sätta 'Vårdgivare' till inloggad användare
*
* @method setName
* @param {Object} data Dataobject
* @param {Object} event Eventobjekt
*/
vm.setDate = function(data, event) {
    data.B_besokdatum(getDagensDatum());
};


/**
* Funktion för att ta bort ett  Besök.
*
* @method removeAterbesok
* @param {Object} data Dataobject
* @param {Object} event Eventobjekt
*/
vm.removeAterbesok = function(data, event) {
    // ta bort en rad till läkemedelstabellen
    vm.$$.Besök.rcc.remove(data);
	
	// uppdatera accordion för tabellen
	vm.accordTable('#B-accordion', true);
};


/**
* Funktion för att lägga till subscribers
*
* @method setAterbesokSubscribers
* @param {Object} data Dataobject
*/
vm.setAterbesokSubscribers = function(data) {
	/*data.B_studie.subscribe(function (newValue) {	
		if (!newValue || newValue.value != '1')
			notRequiredAndClear(data.B_studietxt);
		else
			required(data.B_studietxt);
	}.bind(data));*/
};


/* BILDDIAGNOSTIK */


/**
* Funktion för att lägga till en bilddiagnostik
* subscribers sätts sedan på berörda variabler
*
* @method addBilddiagnostik
* @param {Object} data Dataobject
* @param {Object} event Eventobjekt
*/
vm.addBilddiagnostik = function(data, event) {
    // lägg till en ny rad till Besökstabellen
    vm.$$.Bilddiagnostik.rcc.add();
	
	// uppdatera accordion för tabellen
	vm.accordTable('#BD-accordion', true);
};


/**
* Funktion för att ta bort en Bilddiagnostik.
*
* @method removeBilddiagnostik
* @param {Object} data Dataobject
* @param {Object} event Eventobjekt
*/
vm.removeBilddiagnostik = function(data, event) {
    // ta bort en rad till läkemedelstabellen
    vm.$$.Bilddiagnostik.rcc.remove(data);
	
	// uppdatera accordion för tabellen
	vm.accordTable('#BD-accordion', true);
};


/**
* Funktion för att lägga till en bilddiagnostik metastas
* subscribers sätts sedan på berörda variabler
*
* @method addBDMetastas
* @param {Object} data Dataobject
* @param {Object} event Eventobjekt
*/
vm.addBDMetastas = function(data, event) {
    // lägg till en ny rad till Lokaltabellen
    var item = data.$$.BD_Metastas.rcc.add();
	
	// initializera subscribers
	vm.setBDMetastasSubscribers(item);
};


/**
* Funktion för att lägga till subscribers
*
* @method setBDMetastasSubscribers
* @param {Object} data Dataobject
*/
vm.setBDMetastasSubscribers = function(data) {
	/*data.BDM_metastaslokal.subscribe(function (newValue) {	
		if(!newValue || newValue.value != 5)
			notRequiredAndClear(data.BDM_metastaslokalann);
		else
			required(data.BDM_metastaslokalann);
	}.bind(data));*/
};

/* STRÅLBEHANDLING */


/**
* Funktion för att lägga till en strålbehandling
* subscribers sätts sedan på berörda variabler
*
* @method addStralbehandling
* @param {Object} data Dataobject
* @param {Object} event Eventobjekt
*/
vm.addStralbehandling = function(data, event) {
    // lägg till en ny rad till Strålbehandlingstabellen
    vm.$$.Strålbehandling.rcc.add();
    
    vm.$$.Strålbehandling()[vm.$$.Strålbehandling().length - 1].$$.SB_Grupp.rcc.add();
	
	// uppdatera accordion för tabellen
	vm.accordTable('#SB-accordion', true);
};


/**
* Funktion för att ta bort en strålbehandling.
*
* @method removeStralbehandling
* @param {Object} data Dataobject
* @param {Object} event Eventobjekt
*/
vm.removeStralbehandling = function(data, event) {
    // ta bort en rad till läkemedelstabellen
    vm.$$.Strålbehandling.rcc.remove(data);
	
	// uppdatera accordion för tabellen
	vm.accordTable('#SB-accordion', true);
};


/**
* Funktion för att lägga till en strålbehandling grupp
*
* @method addSBGrupp
* @param {Object} data Dataobject
* @param {Object} event Eventobjekt
*/
vm.addSBGrupp = function(data, event) {
    // lägg till en ny rad till Lokaltabellen
    data.$$.SB_Grupp.rcc.add();
  
};


/**
* Funktion för att lägga till en strålbehandling lokal
* subscribers sätts sedan på berörda variabler
*
* @method addSBLokal
* @param {Object} data Dataobject
* @param {Object} event Eventobjekt
*/
vm.addSBLokal = function(data, event) {
    // lägg till en ny rad till Lokaltabellen
    var item = data.$$.SBG_Lokal.rcc.add();
    
	// initializera subscribers
	vm.setSBLokalSubscribers(item);
	
};


/**
* Funktion för att lägga till subscribers
*
* @method setSBLokalSubscribers
* @param {Object} data Dataobject
*/
vm.setSBLokalSubscribers = function(data) {
	/*data.SBGL_lokal.subscribe(function (newValue) {	
		if(!newValue || (newValue.value != 2 && newValue.value != 5)){
			notRequiredAndClear(data.SBGL_lokalskelett);
			notRequiredAndClear(data.SBGL_lokalann);
		}
		else if(newValue.value == 2){
			required(data.SBGL_lokalskelett);
			notRequiredAndClear(data.SBGL_lokalann);
		}
		else if(newValue.value == 5){
			notRequiredAndClear(data.SBGL_lokalskelett);
			required(data.SBGL_lokalann);
		}
	}.bind(data));*/
};


/* SRE */


/**
* Funktion för att lägga till en tom SRE
*
* @method addSRE
* @param {Object} data Dataobject
* @param {Object} event Eventobjekt
*/
vm.addSRE = function(data, event) {
    // lägg till en ny rad till SRE tabellen
    vm.$$.SRE.rcc.add();
	
	// uppdatera accordion för tabellen
	vm.accordTable('#SE-accordion', true);
};


/**
* Funktion för att lägga till en SRE med förutbestämda värden
*
* @method addSREStral
* @param {Object} object Dataobject med förutbestämda värden
*/
vm.addSREStral = function(object) {
	// lägg till en ny rad till SRE tabellen
    var item = vm.$$.SRE.rcc.add();
	
	// uppdatera accordion för tabellen
	vm.accordTable('#SE-accordion', true);
	
	// sätt SRE datum
	item.SRE_datum(object);
	
	vm.addSEHandelseStral(item);
	
};


/**
* Funktion för att ta bort ett SRE.
*
* @method removeSRE
* @param {Object} data Dataobject
* @param {Object} event Eventobjekt
*/
vm.removeSRE = function(data, event) {
    // ta bort en rad till läkemedelstabellen
    vm.$$.SRE.rcc.remove(data);
	
	// uppdatera accordion för tabellen
	vm.accordTable('#SE-accordion', true);
	
};


/**
* Funktion för att lägga till en SRE händelse
* focus sätts därefter på den första läkemedelslinjens flik(om det finns kvarvarande linjer)
*
* @method addSEHandelse
* @param {Object} data SRE
* @param {Object} event Eventobject
*/
vm.addSEHandelse = function(data, event) {
    // lägg till en ny rad till Händelsetabellen
    data.$$.SRE_Händelse.rcc.add();
};


/**
* Funktion för att lägga till en SRE händelse
* focus sätts därefter på den första läkemedelslinjens flik(om det finns kvarvarande linjer)
*
* @method addSEHandelse
* @param {Object} data SRE
* @param {Object} event Eventobject
*/
vm.addSEHandelseStral = function(data) {
    // lägg till en ny rad till Händelsetabellen
    var item = data.$$.SRE_Händelse.rcc.add();
	//console.log(item);
	item.SREH_handelse(item.SREH_handelse.rcc.term.listValues[2]);
	
};


/* LABBPROV */


/**
* Funktion för att lägga till ett labbprov
* subscribers sätts sedan på berörda variabler
*
* @method addLabbprov
* @param {Object} data Dataobject
* @param {Object} event Eventobjekt
*/
vm.addLabbprov = function(data, event) {
    // lägg till en ny rad till Besökstabellen
    vm.$$.Labbprov.rcc.add();
};


/* LÄKEMEDEL */


/**
* Funktion för att lägga till en läkemedelslinje
* subscribers sätts sedan på berörda variabler
* focus sätts därefter på den tillagda läkemedelslinjens flik
*
* @method addLakemedel
* @param {Object} data Dataobject
* @param {Object} event Eventobjekt
*/
vm.addLakemedel = function(data, event) {
    // lägg till en ny rad till läkemedelstabellen
    var item = vm.$$.Läkemedel.rcc.add();

	// initializera substanslista
	item.LM_substans.listValues = ko.observableArray(undefined);	
	
	vm.setLakemedelSubscribers(item);
	
    // uppdatera läkemedelstabbar
    // HACK: tabs('refresh') stöds ej?
    $('#prostataprocess-lmtabs').tabs();
    $('#prostataprocess-lmtabs').tabs('destroy');
    $('#prostataprocess-lmtabs').tabs();
    
    // fokusera på den senast tillagda fliken
    $('#prostataprocess-lmtabs').tabs('select', $('#prostataprocess-lmtabs >ul >li').size() - 1);
};


/**
* Funktion för att ta bort en läkemedelslinje.
* focus sätts därefter på den första läkemedelslinjens flik(om det finns kvarvarande linjer)
*
* @method removeLakemedel
* @param {Object} data Dataobject
* @param {Object} event Eventobjekt
*/
vm.removeLakemedel = function(data, event) {

    // ta bort en rad till läkemedelstabellen
    vm.$$.Läkemedel.rcc.remove(data);
	
	// uppdatera läkemedelstabbar
    // HACK: tabs('refresh') stöds ej?
    $('#prostataprocess-lmtabs').tabs('destroy');
    $('#prostataprocess-lmtabs').tabs();
    
    // om det fortfarande finns flikar, fokusera på den första
    if($('#prostataprocess-lmtabs >ul >li').size() > 0)
		$('#prostataprocess-lmtabs').tabs('select', 0);
};


/**
* funktion för att lägga till en Läkemedelsinsättning
* subscribers sätts sedan på berörda variabler
*
* @method addLmInsattning
* @param {Object} index Läkemedelsindex
* @param {Object} data Dataobject
* @param {Object} event Eventobjekt
*/
vm.addLmInsattning = function(index, data, event) {
	var id = '#LM-' + index + '-accordion';
	//var id = '.LM-accordion';
	
	// lägg till en ny rad till Insättningstabellen
    var insattning = data.$$.LM_Insättning.rcc.add();

	// sätt värde till på Indikationslistan till  "Palliativ" 
	insattning.LMI_indikation(insattning.LMI_indikation.rcc.term.listValues[0]);
	
	var handelse = insattning.$$.LMI_Händelse.rcc.add();
	//handelse.LMIH_handelseorsak(handelse.LMIH_handelseorsak.rcc.term.listValues[5])

	// uppdatera accordion för tabellen
	if($(id).hasClass('ui-accordion'))
		vm.accordTable(id, true);
	else
		vm.accordTable(id, false);
};


/**
* Funktion för att ta bort en läkemedelsinsättning.
*
* @method removeLMInsattning
* @param {Object} index Läkemedel
* @param {Object} index Läkemedelsindex
* @param {Object} data Dataobject
* @param {Object} event Eventobjekt
*/
vm.removeLMInsattning = function(parent, index, data, event) {
    var id = '.LM-accordion';
	
	// ta bort en rad till läkemedelstabellen
    parent.$$.LM_Insättning.rcc.remove(data);
	
	// uppdatera accordion för tabellen
	vm.accordTable(id, true);
};


/**
 * funktion för att lägga till en Läkemedelshändelse
* subscribers sätts sedan på berörda variabler
*
* @method addLmHandelse
* @param {Object} data Dataobject
* @param {Object} event Eventobjekt
 */
vm.addLmHandelse = function(data, event) {
    // lägg till en ny rad till Händelsetabellen
    data.$$.LMI_Händelse.rcc.add();
};


/**
* funktion för att lägga till en läkemedelsutsättning
* subscribers sätts sedan på berörda variabler
*
* @method addLmUtsattning
* @param {Object} data Dataobject
* @param {Object} event Eventobjekt
 */
vm.addLmUtsattning = function(data, event) {
    // lägg till en ny rad till Utsättningstabellen
    data.$$.LMI_Utsättning.rcc.add();
};


/**
* Funktion för att lägga till subscribers
*
* @method setLakemedelSubscribers
* @param {Object} data Dataobject
*/
vm.setLakemedelSubscribers = function(data) {
	/*data.LM_studielkm.subscribe(function (newValue) {	
		if(newValue){
			notRequiredAndClear(data.LM_substans);
			required(data.LM_studielkmtxt);
			notRequiredAndClear(data.LM_ablatiotestisdatum);
		}
		else if(!newValue){
			notRequiredAndClear(data.LM_studielkmtxt);
			required(data.LM_substans);
			notRequiredAndClear(data.LM_ablatiotestisdatum);
		}
	}.bind(data));*/
	
	data.LM_ablatiotestis.subscribe(function (newValue) {
		if(newValue) {
			if(data.$$.LM_Insättning().length > 0)
				data.$$.LM_Insättning.removeAll();
		}
		/*else{
			required(data.LM_substans);
			notRequiredAndClear(data.LM_studielkmtxt);
			notRequiredAndClear(data.LM_ablatiotestisdatum);
		}*/
	}.bind(data));
	
	data.LM_typ.subscribe(function(newValue) {
		data.LM_substans(undefined);
		data.LM_studielkm(undefined);
		data.LM_ablatiotestis(undefined);
		if(newValue) {
			if(newValue.value == '0')
				vm.loadSubstances(data, 'ProstataProcess_CY');
			else if(newValue.value == '1')
				vm.loadSubstances(data, 'ProstataProcess_ET');
			else if(newValue.value == '2')
				vm.loadSubstances(data, 'ProstataProcess_BB');
			else if(newValue.value == '3')
				vm.loadSubstances(data, 'ProstataProcess_IS');
			else if(newValue.value == '4')
				vm.loadSubstances(data, 'ProstataProcess_OC');		
		}
	}.bind(data));
};


/**
* Funktion för att flagga formuländringar
*
* @method touchForm
*/
vm.touchForm = function() {
    if(!vm.formHasChanged()) {
		vm.formHasChanged(true);

		// hämta dagens datum
		//var d = new Date();
		//var datum = d.getFullYear() + "-" + ((''+d.getMonth() + 1).length < 2 ? '0' : '') + (d.getMonth() + 1) + "-" + ((''+d.getDate()).length < 2 ? '0' : '') + d.getDate();
		//vm.U_datum(datum);
	}
};


/**
* Funktion för att flagga tabelländringar
*
* @method touchTable
* @param {String} table Tabell för touch
*/
vm.touchTable = function(table) {
    if(table == 'root') {
		vm.rootHasChanged(true);
	}
	else if(table == 'B') {
		vm.tableBHasChanged(true);
	}
	else if(table == 'BD') {
		vm.tableBDHasChanged(true);
	}
	else if(table == 'SB') {
		vm.tableSBHasChanged(true);
	}
	else if(table == 'SE') {
		vm.tableSEHasChanged(true);
	}
	else if(table == 'LP') {
		vm.tableLPHasChanged(true);
	}
	else if(table == 'PROM') {
		vm.tablePROMHasChanged(true);
	}
	else if(table == 'LM') {
		vm.tableLMHasChanged(true);
		if($("#prostataprocess-lmtabs").data("tabs")) {
			$("#prostataprocess-lmtabs").tabs("refresh");
		}
	}
};


/**
* Funktion för att uppdatera en accordion
*
* @method accordTable
* @param {String} table Tabell att uppdatera accordion för
* @param {Boolean} destroy Skall accordion förstöras innan uppdatering
*/
vm.accordTable = function(table, destroy) {
	if(destroy)
		$(table).accordion('destroy');
	$(table).accordion({collapsible:true, active: 0, autoHeight: false});
};


/**
* Funktion för att returnera sorterade tabellrader
*
* @method sortTable
* @param {Array} table Tabell att sortera
* @param {String} key Variabelnamn att sortera på
* @return {Array} Sorterad tabell
*/
vm.sortTable = function(table, key, order) {
	return table.slice(0).sort(function(a, b) {
		var va = (a[key]() == null) ? "9999-99-99" : a[key]();
        var vb = (b[key]() == null) ? "9999-99-99" : b[key]();
		if(order == 'ascending')
			return va == vb ? 0 : (va < vb ? -1 : 1);
		else if(order == 'descending')
			return va == vb ? 0 : (va > vb ? -1 : 1);
	});
};


/**
* Funktion för att returnera sorterade tabellrader i läkemedelstabell
*
* @method sortLMTable
* @param {Array} table Tabell att sortera
* @return {Array} Sorterad tabell
*/
vm.sortLMTable = function(table) {

	return table.slice(0).sort(function(a, b) {
		 var va = (vm.firstLMEventDate(a) == null) ? "9999-99-99" : vm.firstLMEventDate(a);
         var vb = (vm.firstLMEventDate(b) == null) ? "9999-99-99" : vm.firstLMEventDate(b);
	
		return va == vb ? 0 : (va < vb ? -1 : 1);
	});
};


/**
* Funktion för att returnera sorterade tabellrader i insättningstabell
*
* @method sortInsTable
* @param {Array} table Tabell att sortera
* @return {Array} Sorterad tabell
*/
vm.sortInsTable = function(table) {
	return table.slice(0).sort(function(a, b) {
		var va = (vm.firstInsEventDate(a) == null) ? "9999-99-99" : vm.firstInsEventDate(a);
        var vb = (vm.firstInsEventDate(b) == null) ? "9999-99-99" : vm.firstInsEventDate(b);
		return va == vb ? 0 : (va > vb ? -1 : 1);
	});
};


/**
* Funktion för att returnera datum för första läkemedelshändelse 
*
* @method firstLMEventDate
* @param {Object} obj Läkemdel
* @return {String} Datum för första läkemedelshändelse
*/
vm.firstLMEventDate = function(obj) {
	var first = null;
	
	if(obj.LM_ablatiotestisdatum())
		first = obj.LM_ablatiotestisdatum();
	else 
		ko.utils.arrayForEach(obj.$$.LM_Insättning(), function (insattning) {
			if(!first || vm.firstInsEventDate(insattning) < first)
				first = vm.firstInsEventDate(insattning);
		});

	return first;
};


/**
* Funktion för att returnera datum för första insättningshändelse
*
* @method firstInsEventDate
* @param {Object} obj Insättning
* @return {String} Datum för första insättningshändelse
*/
vm.firstInsEventDate = function(obj) {
	var first = null;

	ko.utils.arrayForEach(obj.$$.LMI_Händelse(), function (handelse) {
		if(!first || handelse.LMIH_handelsedatum() < first)
			first = handelse.LMIH_handelsedatum();
	});

	return first;
};


vm.firstUtsEventDate = function(obj) {
	var first = null;

	ko.utils.arrayForEach(obj.$$.LMI_Utsättning(), function (utsattning) {
		if(!first || utsattning.LMIU_utsattningsdatum() < first)
			first = utsattning.LMIU_utsattningsdatum();
	});

	return first;
};


/**
* Beräknande funktion för beräkna och returnera samtliga ordinationsdoser i regimen
*
* @method ordinationDoser
* @return {Array} Array med samtliga ordinationsdoser i regimen
*/
vm.studyList = ko.computed(function() {
	var studys = [];
		
	// loopa igenom alla Besök
	ko.utils.arrayForEach(vm.$$.Besök(), function(item) {
		if(item.B_studietxt())
			studys.push({ date: item.B_besokdatum(), study: item.B_studietxt() });
	});
		
	// returnera studier
	return studys.reverse();
});


	/**
	 *	Specifika funktioner för översiktsregistret, som ännu ej är i generella hjälpfunktioner.
	 */
	
/**
 * Funktion för att visa "Info"-dialog.
 *
 * @method getInfoDialog
 * @param {*} [dialog] Identifierare för dialogen.
 */
vm.getInfoDialogHTML = function(dialog) {
	return function(data, event) {
		$(dialog).dialog(
		{
			dialogClass: 'rcc-dialog',
			position: [ event.clientX + 15, event.clientY - 30 ],
			width: 500,
			modal: true,
			buttons: [ { text: 'OK', priority: 'primary', click: function ( ) {$(this).dialog('close');} } ]
		});
	};
};


/**
 * Funktion för att visa "Info"-dialog från javascript.
 *
 * @method getInfoDialog
 * @param {*} [dialog] Identifierare för dialogen.
 */
vm.getInfoDialogJS = function(dialog) {
	$(dialog).dialog(
	{
		dialogClass: 'rcc-dialog',
		position: [ (formWidth/2)-150, 300 ],
		width: 500,
		modal: true,
		buttons: [ { text: 'OK', priority: 'primary', click: function ( ) {$(this).dialog('close');} } ]
	});
};


/**
 * Funktion för att visa "Add"-dialog.
 *
 * @method getAddDialog
 * @param {*} [dialog] Identifierare för dialogen.
 */
vm.getAddDialog = function(dialog) {
	var def = $.Deferred();
	$(dialog).dialog(
	{
		dialogClass: 'rcc-dialog',
		position: [ (formWidth/2)-150, 300 ],
		width: 500,
		modal: true,
		buttons: 
		[
			{ text: 'Avbryt', click: function ( ) { $(this).dialog('close'); def.reject(); }, priority: 'primary' },
			{ text: 'Lägg till', click: function ( ) { $(this).dialog('close'); def.resolve(); }, priority: 'secondary' }
		]
	});
	return def.promise();
};


vm.splitTNM = function(object) {
	var split = object.split(" - ");
	var result;
	if(split[0])
		result = split[0];
	else
		result = object;
	
	return result;
};


	/****************************************
	 *										*
	 *				Validering				*
	 *										*
	 ****************************************/

	// Variabler i rot-tabellen
	notRequired(vm.U_psadatum);
	notRequired(vm.U_hormonstartdatum);
	notRequired(vm.U_crpcdatum);
	notRequired(vm.U_metastasdatum);
	notRequired(vm.U_pallHemSjkvrd);
	notRequired(vm.U_patMedgivande);
	notRequired(vm.U_npcr_fk);

	// Besöksvalidering, tända släcka funktion anpassning
	ko.computed(function() {
		if(vm.$$.Besök().length > 0) {
			for(var i = 0; i < vm.$$.Besök().length; i++) {
				
				if(vm.U_crpcdatum() && (!vm.$$.Besök()[i].B_crpc() || vm.$$.Besök()[i].B_crpc().value != 1)) {
					notRequired(vm.$$.Besök()[i].B_crpc);
				} else if(vm.$$.Besök()[i].B_klinEjBed()) {
					notRequiredAndClear(vm.$$.Besök()[i].B_crpc);
				} else {
					required(vm.$$.Besök()[i].B_crpc);
				}
				
				if(vm.U_pallHemSjkvrd() && (!vm.$$.Besök()[i].B_palliativvard() || vm.$$.Besök()[i].B_palliativvard().value == 1)) {
					notRequired(vm.$$.Besök()[i].B_palliativvard);
				} else {
					required(vm.$$.Besök()[i].B_palliativvard);
				}
				
				if(!vm.$$.Besök()[i].B_studie() || vm.$$.Besök()[i].B_studie().value != 1) {
					notRequiredAndClear(vm.$$.Besök()[i].B_studietxt);
				} else {
					required(vm.$$.Besök()[i].B_studietxt);
				}
				
				if(vm.$$.Besök()[i].B_klinEjBed()) {
					notRequiredAndClear(vm.$$.Besök()[i].B_klinbed);
					notRequiredAndClear(vm.$$.Besök()[i].B_studie);
				} else {
					required(vm.$$.Besök()[i].B_klinbed);
					required(vm.$$.Besök()[i].B_studie);
				}
				
				if(!vm.$$.Besök()[i].B_biobank() || vm.$$.Besök()[i].B_biobank().value != 1) {
					notRequiredAndClear(vm.$$.Besök()[i].B_biobankprovtyp);
					notRequiredAndClear(vm.$$.Besök()[i].B_biobankDatum);
				} else {
					required(vm.$$.Besök()[i].B_biobankprovtyp);
					required(vm.$$.Besök()[i].B_biobankDatum);
				}
				
			}
			
		}
		
	});
	
	// Bilddiagnostiksvalidering, tända släcka funktion anpassning
	ko.computed(function() {
		if(vm.$$.Bilddiagnostik().length > 0) {
			for(var i = 0; i < vm.$$.Bilddiagnostik().length; i++) {
			
				notRequired(vm.$$.Bilddiagnostik()[i].BD_radbed);
				
				resetValidation(vm.$$.Bilddiagnostik()[i].BD_bsi);
				vm.$$.Bilddiagnostik()[i].BD_bsi.rcc.validation.add( new RCC.Validation.Tests.Decimal(vm.$$.Bilddiagnostik()[i].BD_bsi, {decimalPlaces: 1, min: 0, max: 100, ignoreMissingValues: true} ) );
				
				if(!vm.$$.Bilddiagnostik()[i].BD_radundertyp() || vm.$$.Bilddiagnostik()[i].BD_radundertyp().value != 9) {
					notRequiredAndClear(vm.$$.Bilddiagnostik()[i].BD_radundtypannan);
				} else {
					required(vm.$$.Bilddiagnostik()[i].BD_radundtypannan);
				}
				
				var undertabell = vm.$$.Bilddiagnostik()[i].$$.BD_Metastas();
				for(var n = 0; n < undertabell.length; n++) {
					if(!undertabell[n].BDM_metastaslokal() || undertabell[n].BDM_metastaslokal().value != 5) {
						notRequiredAndClear(undertabell[n].BDM_metastaslokalann);
					} else {
						required(undertabell[n].BDM_metastaslokalann);
					}
				}
			}
		}
	});

	// Strålbehandlingsvalidering, tända släcka funktion anpassning
	ko.computed(function() {
		if(vm.$$.Strålbehandling().length > 0) {
			for(var i = 0; i < vm.$$.Strålbehandling().length; i++) {
				// Variabler som inte används?
				notRequired(vm.$$.Strålbehandling()[i].SB_frakdos2);
				notRequired(vm.$$.Strålbehandling()[i].SB_slutdos2);
				
				var tabell = vm.$$.Strålbehandling()[i].$$.SB_Grupp();
				for(var n = 0; n < tabell.length; n++) {
					// Variabler som inte används?
					notRequired(tabell[n].SBL_lokal2);
					notRequired(tabell[n].SBL_lokalann2);
					notRequired(tabell[n].SBL_lokalskelett2);
					
					var undertabell = tabell[n].$$.SBG_Lokal();
					for(var m = 0; m < undertabell.length; m++) {
						if(!undertabell[m].SBGL_lokal() || undertabell[m].SBGL_lokal().value != 2) {
							notRequiredAndClear(undertabell[m].SBGL_lokalskelett);
						} else {
							required(undertabell[m].SBGL_lokalskelett);
						}
						
						if(!undertabell[m].SBGL_lokal() || undertabell[m].SBGL_lokal().value != 5) {
							notRequiredAndClear(undertabell[m].SBGL_lokalann);
						} else {
							required(undertabell[m].SBGL_lokalann);
						}
					}
				}
			}
		}
	});
	
	// SRE validering - behövs inte i dagsläget då ingen tänd släck logik finns
	
	// Labbprov validering, minst ett av värdena måste fyllas i logik som styr valideringen här
	ko.computed(function() {
		if(vm.$$.Labbprov().length > 0) {
			for(var i = 0; i < vm.$$.Labbprov().length; i++) {
				// Set them to notRequired so that empty values are OK
				notRequired(vm.$$.Labbprov()[i].LP_psa);
				notRequired(vm.$$.Labbprov()[i].LP_alp);
				notRequired(vm.$$.Labbprov()[i].LP_krea);
				notRequired(vm.$$.Labbprov()[i].LP_hb);
				notRequired(vm.$$.Labbprov()[i].LP_testosteron);
				
				// Add dynamic min-max values for decimal
				vm.$$.Labbprov()[i].LP_psa.rcc.validation.add(new RCC.Validation.Tests.Decimal(vm.$$.Labbprov()[i].LP_psa, { decimalPlaces: 2, min: 0.01, ignoreMissingValues: true } ));
				
				// Add the new validation so that at least one of the fields has to have a value
				vm.$$.Labbprov()[i].LP_psa.rcc.validation.add(new RCC.Validation.Tests.atLeastOneValue(vm.$$.Labbprov()[i].LP_psa, { dependables: [vm.$$.Labbprov()[i].LP_psa, vm.$$.Labbprov()[i].LP_alp, vm.$$.Labbprov()[i].LP_krea, vm.$$.Labbprov()[i].LP_hb, vm.$$.Labbprov()[i].LP_testosteron], ignoreMissingValues: true }));
				vm.$$.Labbprov()[i].LP_alp.rcc.validation.add(new RCC.Validation.Tests.atLeastOneValue(vm.$$.Labbprov()[i].LP_alp, { dependables: [vm.$$.Labbprov()[i].LP_psa, vm.$$.Labbprov()[i].LP_alp, vm.$$.Labbprov()[i].LP_krea, vm.$$.Labbprov()[i].LP_hb, vm.$$.Labbprov()[i].LP_testosteron], ignoreMissingValues: true }));
				vm.$$.Labbprov()[i].LP_krea.rcc.validation.add(new RCC.Validation.Tests.atLeastOneValue(vm.$$.Labbprov()[i].LP_krea, { dependables: [vm.$$.Labbprov()[i].LP_psa, vm.$$.Labbprov()[i].LP_alp, vm.$$.Labbprov()[i].LP_krea, vm.$$.Labbprov()[i].LP_hb, vm.$$.Labbprov()[i].LP_testosteron], ignoreMissingValues: true }));
				vm.$$.Labbprov()[i].LP_hb.rcc.validation.add(new RCC.Validation.Tests.atLeastOneValue(vm.$$.Labbprov()[i].LP_hb, { dependables: [vm.$$.Labbprov()[i].LP_psa, vm.$$.Labbprov()[i].LP_alp, vm.$$.Labbprov()[i].LP_krea, vm.$$.Labbprov()[i].LP_hb, vm.$$.Labbprov()[i].LP_testosteron], ignoreMissingValues: true }));
				vm.$$.Labbprov()[i].LP_testosteron.rcc.validation.add(new RCC.Validation.Tests.atLeastOneValue(vm.$$.Labbprov()[i].LP_testosteron, { dependables: [vm.$$.Labbprov()[i].LP_psa, vm.$$.Labbprov()[i].LP_alp, vm.$$.Labbprov()[i].LP_krea, vm.$$.Labbprov()[i].LP_hb, vm.$$.Labbprov()[i].LP_testosteron], ignoreMissingValues: true }));
			}
		}
	});
	
	// Läkemedel validering, tända släcka funktion anpassning
	ko.computed(function() {
		if(vm.$$.Läkemedel().length > 0) {
			for(var i = 0; i < vm.$$.Läkemedel().length; i++) {
				if(!vm.$$.Läkemedel()[i].LM_ablatiotestis() && !vm.$$.Läkemedel()[i].LM_studielkm()) {
					required(vm.$$.Läkemedel()[i].LM_substans);
				} else {
					notRequiredAndClear(vm.$$.Läkemedel()[i].LM_substans);
				}
			
				if(!vm.$$.Läkemedel()[i].LM_ablatiotestis()) {
					notRequiredAndClear(vm.$$.Läkemedel()[i].LM_ablatiotestisdatum);
				} else {
					required(vm.$$.Läkemedel()[i].LM_ablatiotestisdatum);
				}
				
				if(!vm.$$.Läkemedel()[i].LM_studielkm()) {
					notRequiredAndClear(vm.$$.Läkemedel()[i].LM_studielkmtxt);
				} else {
					required(vm.$$.Läkemedel()[i].LM_studielkmtxt);
				}
				
				var tabell = vm.$$.Läkemedel()[i].$$.LM_Insättning();
				for(var j = 0; j < tabell.length; j++) {
					
					var undertabellHand = tabell[j].$$.LMI_Händelse();
					for(var n = 0; n < undertabellHand.length; n++) {
						
						if(!undertabellHand[n].LMIH_handelseorsak() || undertabellHand[n].LMIH_handelseorsak().value != 4) {
							notRequiredAndClear(undertabellHand[n].LMIH_handelseorsakann);
						} else {
							required(undertabellHand[n].LMIH_handelseorsakann);
						}
						
					}
					
					var undertabellUt = tabell[j].$$.LMI_Utsättning();
					for(var n = 0; n < undertabellUt.length; n++) {
						
						if(!undertabellUt[n].LMIU_utsattningsorsak() || undertabellUt[n].LMIU_utsattningsorsak().value != 4) {
							notRequiredAndClear(undertabellUt[n].LMIU_utsattningsorsakann);
						} else {
							required(undertabellUt[n].LMIU_utsattningsorsakann);
						}
						
					}
					
				}
				
			}
		}
	});

	/*
	
		Nuvarande koncept för validering:
		
		Testa används för att kolla varje "rad" och returnerar ett värde, sant eller falskt, om
		det finns fel eller inte på den rad som undersöks.
		Om raden har en undertabell måste även denna testas, varje underrad till varje rad testas
		rekursivt innuti funktionen.
		Har ett fel upptäckts visas detta med röd text i HTML koden för "raden" i fråga.
		
		För att hålla reda på om det finns något fel i någon av alla rader i en tab, och i så fall
		göra texten	röd på tabben, finns en beräknad variabel för varje tabell tillhörande en tab
		och ännu en variabel som kollar alla beräknade variabler som hör till en tab.
		Om fel hittas sätts variabeln (tillhörande tabellen med raden med fel i) till true, annars
		är den false. Om det finns fel visas detta med röd färg på texten för tabben i fråga.
	
	*/
	
	vm.validate = ko.observable(false);
	
	vm.validateData = function() {
		vm.validate(true);
		
		if(vm.besokFelFinns() || vm.tab5FelFinns() || vm.tab6FelFinns() || vm.tab7FelFinns() || vm.labbprovFelFinns() || vm.tab9FelFinns()) {
			getInfoDialog("Fel funna i formuläret", "Det saknas information eller finns fel i de inmatade uppgifterna i formuläret. De flikar och rader som innehåller fel har rödmarkerats.");
			return false;
		} else {
			if(vm.ejGjordBedömning()) {
				if(!warningDialog("Ett eller flera besök väntar på att få en sammantagen klinisk bedömning. Är du säker på att du vill spara utan att fylla i dessa?")) {
					return false;
				}
			}
			if(vm.ejIfylldRadBed()) {
				if(!warningDialog("Det saknas radiologisk bedömning på en eller flera bilddiagnostiker. Är du säker på att du vill spara utan att fylla i dessa?")) {
					return false;
				}
			}
		}
		
		vm.actionsToDoBeforePost();
		
	};
	
	inca.on('validation', vm.validateData);
	
	vm.actionsToDoBeforePost = function() {
		if(vm.formHasChanged()) {
			var d = new Date();
			var datum = d.getFullYear() + "-" + ((''+d.getMonth() + 1).length < 2 ? '0' : '') + (d.getMonth() + 1) + "-" + ((''+d.getDate()).length < 2 ? '0' : '') + d.getDate();
			vm.U_datum(datum);
		}
	};

	/**
	 *
	 *	Funktionen testar en rad och returnerar true eller false beroende på om det finns något fel i just den raden i tabellen.
	 *	Funktionen tar hänsyn till tabeller med undertabeller och dess rader och loopar igenom alla ingående poster rekursivt.
	 *	Denna funktion ger enbart svar på om en specifik rad har något fel, inte om någon av alla rader i en flik har ett fel.
	 *
	 */
	vm.testaRad = function(row) {
	
		if(vm.validate()) { 
			var errors = false;
			
			// Loopa genom alla radens "prop" (funktioner/variabler)
			for(prop in row) {
				// Om det innehåller en .rcc är det en variabel
				if(row[prop].rcc) {
					// Om variabeln har något fel i sig
					if(row[prop].rcc.validation.errors().length > 0) {
						//console.log(prop + ' har ett fel');
						row[prop].rcc.accessed(true);
						errors = true;
					} else {
						//console.log(prop + ' är ok');
					}
				// Om prop är "$$" kan tabellen ha undertabeller med variabler som också måste testas
				} else if(prop == '$$') {
					var property = prop;
					// Loopa igenom undertabellerna
					for(tabell in row[property]) {
						var undertabell = tabell;
						// Om det är en undertabell som har något innehåll
						if(row[property][undertabell]().length > 0) {
							for(var i = 0; i < row[property][undertabell]().length; i++) {
								// Testa dessa också och se om de innehåller fel
								if(vm.testaRad(row[property][undertabell]()[i])) {
									errors = true;
								}
							}
						}
					}
				// OM det inte är en inca-variabel utan någon funktion eller annat ska inget göras
				} else {
					//console.log(prop + ' är inte en incavariabel');
				}
			}
			
			if(errors) {
				return true;
			}
			return false;
		}
		else
			return false;
	};
	
	vm.avvaktarBedömning = function(row) {
		var avvaktar = false;
		
		if(vm.validate()) {
			
			if(row.B_klinbed() && row.B_klinbed().value == '3') {
				avvaktar = true;
			}
		}
	
		return avvaktar;
	};
	
	vm.saknarRadBed = function(row) {
		var saknar = false;

		if(vm.validate()) {
			
			if(!row.BD_radbed()) {
				saknar = true;
			}
		}
	
		return saknar;
	};
	
	/**
	 *
	 *	Validering för flikarna
	 *
	 */
	
	// Tab-4 : Besök
	vm.besokFelFinns = ko.computed(function() {
		if(vm.validate()) {
			// Loopa igenom tabellens rader och eventuella undertabeller och deras rader
			for(var i = 0; i < vm.$$.Besök().length; i++) {
				var row = vm.$$.Besök()[i];
				// Om en rad har en prop som är .rcc är det en variabel
				for(prop in row) {
					// Om variabeln har fel i sig, errors().length > 0, avbryt loop och returnera true
					if(row[prop].rcc && row[prop].rcc.validation.errors().length > 0) {
						return true;
					}
				}
			}
			// Om ingen variabel har fel i sig returneras false
			return false;
		}
		else
			return false;
	});
	
	
	// Tab-5 : Bilddiagnos
	vm.bildDiaFelFinns = ko.computed(function() {
		if(vm.validate()) {
			// Loopa igenom tabellens rader och eventuella undertabeller och deras rader
			for(var i = 0; i < vm.$$.Bilddiagnostik().length; i++) {
				var row = vm.$$.Bilddiagnostik()[i];
				// Om en rad har en prop som är .rcc är det en variabel
				for(prop in row) {
					// Om variabeln har fel i sig, errors().length > 0, avbryt loop och returnera true
					if(row[prop].rcc && row[prop].rcc.validation.errors().length > 0) {
						return true;
					}
				}
			}
			// Om ingen variabel har fel i sig returneras false
			return false;
		}
		else
			return false;
	});
	
	vm.metastasFelFinns = ko.computed(function() {
		if(vm.validate()) {
			// Loopa igenom tabellens rader och eventuella undertabeller och deras rader
			for(var i = 0; i < vm.$$.Bilddiagnostik().length; i++) {
				var undertabell = vm.$$.Bilddiagnostik()[i].$$.BD_Metastas();
				// Loopa igenom undertabellens rader
				for(var n = 0; n < undertabell.length; n++) {
					var row = undertabell[n];
					// Om en rad har en prop som är .rcc är det en variabel
					for(property in row) {
						// Om variabeln har fel i sig, errors().length > 0, avbryt loop och returnera true
						if(row[property].rcc && row[property].rcc.validation.errors().length > 0) {
							return true;
						}
					}
				}
			}
			// Om ingen variabel har fel i sig returneras false
			return false;
		}
		else
			return false;
	});
	
	// Beräknad variabel för flera tabeller i samma flik
	vm.tab5FelFinns = ko.computed(function() {
		if(vm.validate()) {
			// Om det finns ett fel, antingen bland variablerna i bilddiagnostik eller bland metastaser
			if(vm.metastasFelFinns() || vm.bildDiaFelFinns())
				return true;
				
			return false;
		}
		else
			return false;
	});
	
	
	// Tab-6 : Strålbehandling
	vm.stralBehFelFinns = ko.computed(function() {
		if(vm.validate()) {
			for(var i = 0; i < vm.$$.Strålbehandling().length; i++) {
				var row = vm.$$.Strålbehandling()[i];
				for(prop in row) {
					if(row[prop].rcc && row[prop].rcc.validation.errors().length > 0) {
						return true;
					}
				}
			}
			return false;
		}
		else
			return false;
	});
	
	vm.gruppFelFinns = ko.computed(function() {
		if(vm.validate()) {
			for(var i = 0; i < vm.$$.Strålbehandling().length; i++) {
				var undertabell = vm.$$.Strålbehandling()[i].$$.SB_Grupp();
				for(var n = 0; n < undertabell.length; n++) {
					var row = undertabell[n];
					for(property in row) {
						if(row[property].rcc && row[property].rcc.validation.errors().length > 0) {
							return true;
						}
					}
				}
			}
			return false;
		}
		else
			return false;
	});

	vm.lokalFelFinns = ko.computed(function() {
		if(vm.validate()) {
			for(var i = 0; i < vm.$$.Strålbehandling().length; i++) {
				for(var n = 0; n < vm.$$.Strålbehandling()[i].$$.SB_Grupp().length; n++) {
					var undertabell = vm.$$.Strålbehandling()[i].$$.SB_Grupp()[n].$$.SBG_Lokal();
					for(var m = 0; m < undertabell.length; m++) {
						var row = undertabell[m];
						for(property in row) {
							if(row[property].rcc && row[property].rcc.validation.errors().length > 0) {
								return true;
							}
						}
					}
				}
			}
			return false;
		}
		else
			return false;
	});

	vm.tab6FelFinns = ko.computed(function() {
		if(vm.validate()) {
			if(vm.stralBehFelFinns() || vm.gruppFelFinns() || vm.lokalFelFinns())
				return true;
				
			return false;
		}
		else
			return false;
	});
	
	
	// Tab-7 : SRE
	vm.SREFelFinns = ko.computed(function() {
		if(vm.validate()) {
			for(var i = 0; i < vm.$$.SRE().length; i++) {
				var row = vm.$$.SRE()[i];
				for(prop in row) {
					if(row[prop].rcc && row[prop].rcc.validation.errors().length > 0) {
						return true;
					}
				}
			}
			return false;
		}
		else
			return false;
	});
	
	vm.SREHändelseFelFinns = ko.computed(function() {
		if(vm.validate()) {
			for(var i = 0; i < vm.$$.SRE().length; i++) {
				var undertabell = vm.$$.SRE()[i].$$.SRE_Händelse();
				for(var n = 0; n < undertabell.length; n++) {
					var row = undertabell[n];
					for(property in row) {
						if(row[property].rcc && row[property].rcc.validation.errors().length > 0) {
							return true;
						}
					}
				}
			}
			return false;
		}
		else
			return false;
	});

	vm.tab7FelFinns = ko.computed(function() {
		if(vm.validate()) {
			if(vm.SREFelFinns() || vm.SREHändelseFelFinns())
				return true;
				
			return false;
		}
		else
			return false;
	});
	
	
	// Tab-8 : Labbprov
	vm.labbprovFelFinns = ko.computed(function() {
		if(vm.validate()) {
			for(var i = 0; i < vm.$$.Labbprov().length; i++) {
				var row = vm.$$.Labbprov()[i];
				for(prop in row) {
					if(row[prop].rcc && row[prop].rcc.validation.errors().length > 0) {
						return true;
					}
				}
			}
			return false;
		}
		else
			return false;
	});
	
	
	// Tab-9 : Läkemedel
	vm.lakemFelFinns = ko.computed(function() {
		if(vm.validate()) {
			for(var i = 0; i < vm.$$.Läkemedel().length; i++) {
				var row = vm.$$.Läkemedel()[i];
				for(prop in row) {
					if(row[prop].rcc && row[prop].rcc.validation.errors().length > 0) {
						return true;
					}
				}
			}
			return false;
		}
		else
			return false;
	});
	
	vm.lakemInsattFelFinns = ko.computed(function() {
		if(vm.validate()) {
			for(var i = 0; i < vm.$$.Läkemedel().length; i++) {
				var undertabell = vm.$$.Läkemedel()[i].$$.LM_Insättning();
				for(var n = 0; n < undertabell.length; n++) {
					var row = undertabell[n];
					for(property in row) {
						if(row[property].rcc && row[property].rcc.validation.errors().length > 0) {
							return true;
						}
					}
				}
			}
			return false;
		}
		else
			return false;
	});
	
	vm.lakemInsattHandFelFinns = ko.computed(function() {
		if(vm.validate()) {
			for(var i = 0; i < vm.$$.Läkemedel().length; i++) {
				for(var n = 0; n < vm.$$.Läkemedel()[i].$$.LM_Insättning().length; n++) {
					var undertabell = vm.$$.Läkemedel()[i].$$.LM_Insättning()[n].$$.LMI_Händelse();
					for(var m = 0; m < undertabell.length; m++) {
						var row = undertabell[m];
						for(property in row) {
							if(row[property].rcc && row[property].rcc.validation.errors().length > 0) {
								return true;
							}
						}
					}
				}
			}
			return false;
		}
		else
			return false;
	});
	
	vm.lakemInsattUtFelFinns = ko.computed(function() {
		if(vm.validate()) {
			for(var i = 0; i < vm.$$.Läkemedel().length; i++) {
				for(var n = 0; n < vm.$$.Läkemedel()[i].$$.LM_Insättning().length; n++) {
					var undertabell = vm.$$.Läkemedel()[i].$$.LM_Insättning()[n].$$.LMI_Utsättning();
					for(var m = 0; m < undertabell.length; m++) {
						var row = undertabell[m];
						for(property in row) {
							if(row[property].rcc && row[property].rcc.validation.errors().length > 0) {
								return true;
							}
						}
					}
				}
			}
			return false;
		}
		else
			return false;
	});

	vm.tab9FelFinns = ko.computed(function() {
		if(vm.validate()) {
			if(vm.lakemFelFinns() || vm.lakemInsattFelFinns() || vm.lakemInsattHandFelFinns() || vm.lakemInsattUtFelFinns())
				return true;
				
			return false;
		}
		else
			return false;
	});
	
	
	vm.hormonbehandlingStartad = ko.computed(function() {
		for(var i = 0; i < vm.$$.Läkemedel().length; i++) {
			if(vm.$$.Läkemedel()[i] && vm.$$.Läkemedel()[i].LM_typ() && vm.$$.Läkemedel()[i].LM_typ().value == '1') {
				return true;
			}
		}
		return false;
	});
	
	
	vm.ejGjordBedömning = ko.computed(function() {
		if(vm.validate()) {
			for(var i = 0; i < vm.$$.Besök().length; i++) {
				if(vm.$$.Besök()[i].B_klinbed() && vm.$$.Besök()[i].B_klinbed().value == 3) {
					return true;
				}
			}
			return false;
		}
		else
			return false;
	});
	
	
	vm.ejIfylldRadBed = ko.computed(function() {
		if(vm.validate()) {
			for(var i = 0; i < vm.$$.Bilddiagnostik().length; i++) {
				if(!vm.$$.Bilddiagnostik()[i].BD_radbed()) {
					return true;
				}
			}
			return false;
		}
		else
			return false;
	});
	
	vm.patientAge = ko.computed(function() {
		var now;
		if(vm.$env._DATEOFDEATH != "") {
			now = new Date(vm.$env._DATEOFDEATH).getTime();
		} else {
			now = Date.now();
		}
		var birthday = new Date(vm.$env._PERSNR.slice(0,4) + '-' + vm.$env._PERSNR.slice(4,6) + '-' + vm.$env._PERSNR.slice(6,8));
		var ageDifMs = now - birthday.getTime();
		var ageDate = new Date(ageDifMs); // miliseconds from epoch
		return Math.abs(ageDate.getUTCFullYear() - 1970);
	});
	
	
	$('body').parents().on('keydown', function(e) {
		if (e.which === 8 && !$(e.target).is("input, textarea")) {
			e.preventDefault();
		}
	});
	
	
	$(window).bind('beforeunload', function(){
		if(vm.formHasChanged()) {
  			return 'Det finns osparade förändringar i formuläret. Är du säker på att du vill lämna sidan?';
  		}
	});
	
	$('input[name=scaletype]:last').prop("checked", true);
	
	$('input[name=scaletype]').change(function() {
		var self = this;
		if(self.value == 'linear') {
			$('#overview-highchart').highcharts().yAxis[5].update({ type: self.value, tickInterval: null, min: 0 });
		}
		if(self.value == 'logarithmic') {
			$('#overview-highchart').highcharts().yAxis[5].update({ type: self.value, tickInterval: 1, min: 0.01 });
		}
	});
	
	vm.zoomOut = function() {
		if(vm.overviewHighChart) {
			vm.overviewHighChart.zoomOut();
			$('#zoomOutBtn').addClass('hiding');
		}
	};

	// anropa förberedande åtrgärder innan formulärets binds
	vm.runFunctionsOnLoad();

	// applicera knockout bindningar för vymodell
	ko.applyBindings( vm );
	

	/* BERÄKNANDE FUNKTIONER */

	/**
	 *  Kör "afterload" funktioner när formuläret är initializerat.
	 */	
	vm.init = ko.computed( function ( ) { 
		if(requestCount() == 0){
			vm.runFunctionsAfterLoad();
			vm.init.dispose();
		}
	});

});