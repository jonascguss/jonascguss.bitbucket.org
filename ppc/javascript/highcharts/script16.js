(function($) {

/* CHART FUNKTIONER */

initializeOverviewHighChart = function(vm) {
	// create the chart

	vm.overviewHighChartLabels = ['Sammantagen bedömning', 'ECOG-WHO', 'Bilddiagnostik', 'Strålbehandling', 'SRE', 'Senast sparad', 'CRPC datum', 'PSA', 'ALP', 'Krea', 'Hb', 'Testo', 'PSA dubblering', 'PSA halvering', 'PROM', 'PROM pie'];
	vm.overviewWidth = 950; // 850
	vm.overviewLabHeight = 160; // 120
	vm.overviewLabPadding = 50;
	vm.overviewAxisPadding = 15;
	vm.overviewSBHeight = 30;
	vm.overviewSBPadding = 35;
	vm.overviewPlotHeight = 40;
	vm.overviewPlotPadding = 25;
	vm.overviewPROMHeight = 20;
	vm.overviewPROMPadding = 20;
	vm.LMHeight = 20;
	vm.overviewLMHeight = vm.$$.Läkemedel().length * vm.LMHeight;
	vm.overviewLMPadding = 25;
	
	vm.modifiedPSA = ko.observable(false);
	vm.modifiedALP = ko.observable(false);
	vm.modifiedKrea = ko.observable(false);
	vm.modifiedHb = ko.observable(false);
	vm.modifiedTesto = ko.observable(false);
	
	vm.redrawLegend = function() {
		vm.overviewHighChart.legend.destroyItem(vm.overviewHighChart.series[2]);
		vm.overviewHighChart.legend.renderItem(vm.overviewHighChart.series[2]);
		vm.overviewHighChart.legend.destroyItem(vm.overviewHighChart.series[3]);
		vm.overviewHighChart.legend.renderItem(vm.overviewHighChart.series[3]);
		vm.overviewHighChart.legend.destroyItem(vm.overviewHighChart.series[4]);
		vm.overviewHighChart.legend.renderItem(vm.overviewHighChart.series[4]);
		vm.overviewHighChart.legend.destroyItem(vm.overviewHighChart.series[5]);
		vm.overviewHighChart.legend.renderItem(vm.overviewHighChart.series[5]);
		vm.overviewHighChart.legend.destroyItem(vm.overviewHighChart.series[6]);
		vm.overviewHighChart.legend.renderItem(vm.overviewHighChart.series[6]);
		vm.overviewHighChart.legend.render();
	};
	
	vm.senastePSA = ko.computed(function() {
		if(vm.modifiedPSA() && vm.overviewHighChart.series[2].yData.length > 0) {
			var PSAn = vm.overviewHighChart.series[2].yData;
			var datum = vm.overviewHighChart.series[2].xData;
			
			vm.modifiedPSA(false);
			
			return PSAn[PSAn.length-1] + ' µg/L<br>' + new Date(datum[datum.length-1]).toJSON().slice(0,10);
		} else {
			return undefined;
		}
	});
	
	vm.senasteALP = ko.computed(function() {
		if(vm.modifiedALP() && vm.overviewHighChart.series[3].yData.length > 0) {
			var ALPn = vm.overviewHighChart.series[3].yData;
			var datum = vm.overviewHighChart.series[3].xData;
			
			vm.modifiedALP(false);
			
			return ALPn[ALPn.length-1] + ' µkat/L<br>' + new Date(datum[datum.length-1]).toJSON().slice(0,10);
		} else {
			return undefined;
		}
	});
	
	vm.senasteKrea = ko.computed(function() {
		if(vm.modifiedKrea() && vm.overviewHighChart.series[4].yData.length > 0) {
			var Krean = vm.overviewHighChart.series[4].yData;
			var datum = vm.overviewHighChart.series[4].xData;
			
			vm.modifiedKrea(false);
			
			return Krean[Krean.length-1] + ' µmol/L<br>' + new Date(datum[datum.length-1]).toJSON().slice(0,10);
		} else {
			return undefined;
		}
	});
	
	vm.senasteHb = ko.computed(function() {
		if(vm.modifiedHb() && vm.overviewHighChart.series[5].yData.length > 0) {
			var Hbn = vm.overviewHighChart.series[5].yData;
			var datum = vm.overviewHighChart.series[5].xData;
			
			vm.modifiedHb(false);
			
			return Hbn[Hbn.length-1] + ' g/L<br>' + new Date(datum[datum.length-1]).toJSON().slice(0,10);
		} else {
			return undefined;
		}
	});
	
	vm.senasteTesto = ko.computed(function() {
		if(vm.modifiedTesto() && vm.overviewHighChart.series[6].yData.length > 0) {
			var Teston = vm.overviewHighChart.series[6].yData;
			var datum = vm.overviewHighChart.series[6].xData;
			
			vm.modifiedTesto(false);
			
			return Teston[Teston.length-1] + ' nmol/L<br>' + new Date(datum[datum.length-1]).toJSON().slice(0,10);
		} else {
			return undefined;
		}
	});
	
	vm.xOffset = -(vm.overviewPlotHeight + vm.overviewPlotPadding + vm.overviewLMHeight + vm.overviewLMPadding + vm.overviewSBHeight + vm.overviewSBPadding + vm.overviewPROMHeight + vm.overviewPROMPadding);

	vm.overviewHighChart = new Highcharts.Chart({
	
	    chart: {
	        renderTo: 'overview-highchart',
			zoomType: 'x',
			alignTicks: false,
			marginLeft: 25,
			marginRight: 225,
			//type: 'scatter',
			width: vm.overviewWidth,
			height: vm.overviewLabHeight + vm.overviewLabPadding + vm.overviewSBHeight + vm.overviewSBPadding + vm.overviewPlotHeight + vm.overviewPlotPadding + vm.overviewPROMHeight + vm.overviewPROMPadding + vm.overviewLMHeight + vm.overviewLMPadding,
			resetZoomButton: {
				theme: {
					display: 'none'
				}
			},
			events: {
				selection: function(event) {
					$('#zoomOutBtn').removeClass('hiding');
				}
			}
	    },
		
		credits: {
		    enabled: false
		  },
	    
	    title: {
	        text: ''
	    },
	
	    xAxis: {
	        type: 'datetime',
	        labels: {
				//overflow: 'justify',
                formatter: function () {
                    return Highcharts.dateFormat('%e %b %Y', this.value);
                }
            },
			lineColor: '#000000',
			tickColor: '#101010',
			offset: vm.xOffset,
			startOnTick: false,
	        endOnTick: false,
			minPadding: 0.05,
	        maxPadding: 0.05,
			showEmpty: false,
			tickPixelInterval: 125
	    },
	
	    yAxis: [{
            title: {
				text: ''
			},
			labels: {
				enabled: true,
				style: {
		            color: '#000000'
		         },
				formatter: function() {
					return this.value;
				}
			},
			gridLineColor: '#d3d3d3',
			gridLineWidth: 1,
			offset: 0,
			min: 0,
			opposite: true,
			height: vm.overviewLabHeight
        },{
            title: { 
				text: ''
			},
			labels: {
				enabled: false,
				style: {
		            color: '#000000'
		         },
				formatter: function() {
					return this.value;
				}
			},
			tickInterval: 1,
			gridLineColor: '#d3d3d3',
			gridLineWidth: 0,
			offset: 0,
			min: 0,
			max: 4,
			opposite: false,
			height: vm.overviewLabHeight
        },{
			tickInterval: 1,
			gridLineColor: '#d3d3d3',
	        labels: {
				enabled: true,
				x: 6,
				y: 3,
				formatter: function() {
					if(vm.overviewHighChartLabels[this.value])
						return vm.overviewHighChartLabels[this.value];
				},
				style: {
					width: '150px'
				}
	        },
			offset: 0,
			min: 0,
			max: 1,
	        startOnTick: false,
	        endOnTick: false,
	        opposite: true,
	        title: {
	            text: ''
	        },
			plotBands: {
				color: '#eeeeee',
				zIndex: 0,
                value: 0.5,
                width: vm.overviewSBHeight*1.8
			},
			reversed: true,
			showEmpty: true,
			top: vm.overviewLabHeight + vm.overviewLabPadding + vm.overviewAxisPadding,
			height: vm.overviewSBHeight
	    },{
			tickInterval: 1,
			gridLineColor: '#d3d3d3',
	        labels: {
				enabled: true,
				x: 6,
				y: 3,
				formatter: function() {
					if(vm.overviewHighChartLabels[this.value])
						return vm.overviewHighChartLabels[this.value];
				},
				style: {
					width: '150px'
				}
	        },
			offset: 0,
			min: 2,
			max: 4,
	        startOnTick: false,
	        endOnTick: false,
	        opposite: true,
	        title: {
	            text: ''
	        },
			reversed: true,
			showEmpty: true,
			top: vm.overviewLabHeight + vm.overviewLabPadding + vm.overviewSBHeight + vm.overviewSBPadding + vm.overviewAxisPadding + vm.overviewPROMHeight + vm.overviewPROMPadding,
			height: vm.overviewPlotHeight
	    },{
			tickInterval: 1,
			gridLineColor: '#d3d3d3',
	        labels: {
				enabled: true,
				x: 6,
				y: 3,
				formatter: function() {
					if(vm.overviewHighChartLabels[this.value]){
						return vm.overviewHighChartLabels[this.value];
					}
				},
				style: {
					width: '150px'
				}
	        },
			offset: 0,
			maxPadding: 0.1,
			minPadding: 0.1,
	        startOnTick: false,
	        endOnTick: false,
	        opposite: true,
	        title: {
	            text: ''
	        },
			showEmpty: false,
			reversed: false,
			top: vm.overviewLabHeight + vm.overviewLabPadding + vm.overviewSBHeight + vm.overviewSBPadding + vm.overviewPlotHeight + vm.overviewPlotPadding + vm.overviewAxisPadding + vm.overviewPROMHeight + vm.overviewPROMPadding,
			height: vm.overviewLMHeight
	    },{
            title: { 
				text: ''
			},
			labels: {
				enabled: true,
				x: 6,
				y: 3,
				style: {
		            color: '#000000'
		         },
				formatter: function() {
					return this.value;
				}
			},
			gridLineColor: '#d3d3d3',
			gridLineWidth: 1,
			offset: 0,
			min: 0.01,
			opposite: true,
			height: vm.overviewLabHeight,
			tickInterval: 1,
			type: 'logarithmic'
        },{
			tickInterval: 1,
			gridLineColor: '#d3d3d3',
	        labels: {
				enabled: true,
				x: 6,
				y: 3,
				formatter: function() {
					if(vm.overviewHighChartLabels[this.value]){
						return vm.overviewHighChartLabels[this.value];
					}
				},
				style: {
					width: '150px'
				}
	        },
	        title: {
	            text: ''
	        },
			opposite: true,
			offset: 0,
			top: vm.overviewLabHeight + vm.overviewLabPadding + vm.overviewPlotHeight + vm.overviewPlotPadding + vm.overviewAxisPadding,
			height: vm.overviewPROMHeight
		}],
	
	    legend: {
	        enabled: true,
			layout: 'vertical',
			align: 'right',
			verticalAlign: 'top',
			labelFormatter: function() {
				var legend = '<span>' + this.name + '</span>';
				if(this.name == 'PSA') {
					if(vm.senastePSA() != undefined) {
						legend += ': ' + vm.senastePSA();
					}
				} else if(this.name == 'ALP'){
					if(vm.senasteALP() != undefined) {
						legend += ': ' + vm.senasteALP();
					}
				} else if(this.name == 'Krea'){
					if(vm.senasteKrea() != undefined) {
						legend += ': ' + vm.senasteKrea();
					}
				} else if(this.name == 'Hb'){
					if(vm.senasteHb() != undefined) {
						legend += ': ' + vm.senasteHb();
					}
				} else if(this.name == 'Testo'){
					if(vm.senasteTesto() != undefined) {
						legend += ': ' + vm.senasteTesto();
					}
				}
                return legend;
			}
	    },
	
	    tooltip: {
			snap: 2,
            hideDelay: 0,
	    	borderColor : null,
			backgroundColor: '#ffffff',
	        formatter: function() {
				if(this.point.tooltip) {
					var text = '<b>' + this.series.name + '</b>';  
					
					if(this.series.name == 'Bilddiagnostik') {
						if(this.point.datum)
							text = text  + '<br/>' + 'Datum: ' + this.point.datum;
						if(this.point.typ)
							text = text  + '<br/>' + this.point.typ;
						if(this.point.radbed)
							text = text  + '<br/>' + this.point.radbed;
						if(this.point.metastaser.length > 0){
							text = text + '<br/>' +'Nytillkomna metastaser:' + '<br/>';
							
							$.each(this.point.metastaser, function(i, metastas) {
								text = text + metastas + '<br/>'
							});
						}
						else 
							text = text + '<br/>' + '<i>Inga nytillkomna metastaser registrerade.</i>';
							
					}
					else if(this.series.name == 'Strålbehandling') {
						if(this.point.slutdatum)
							text = text  + '<br/>' + 'Slutdatum: ' + this.point.slutdatum;
							
						if(this.point.grupper.length > 0){
							$.each(this.point.grupper, function(i, grupp) {
								if(grupp.frakdos)
									text = text  + '<br/>' + 'Dos per fraktion (Gy): ' + grupp.frakdos;
								if(grupp.slutdos)
									text = text  + '<br/>' + 'Slutdos (Gy): ' + grupp.slutdos;
								if(grupp.lokaler.length > 0){
									text = text + '<br/>' +'Lokaler:';
									
									$.each(grupp.lokaler, function(i, lokal) {
										text = text + '<br/>' + lokal;
									});
								}
								else 
									text = text + '<br/>' + '<i>Inga lokaler registrerade.</i>';
							});
						}
					}
					else if(this.series.name == 'SRE') {
						if(this.point.datum)
							text = text  + '<br/>' + 'Datum: ' + this.point.datum;
						if(this.point.handelser.length > 0){
							text = text + '<br/>' +'Händelser:';
							
							$.each(this.point.handelser, function(i, handelse) {
								text = text + '<br/>' + handelse;
							});
						}
						else 
							text = text + '<br/>' + '<i>Inga händelser registrerade.</i>';
											}
					else if(this.series.name == 'ECOG-WHO') {
						if(this.point.datum)
							text = text  + '<br/>' + 'Datum: ' + this.point.datum;
						if(this.point.ecogwholabel)
							text = text  + '<br/>' + this.point.ecogwholabel;
					}
					else if(this.series.name == 'Sammantagen bedömning') {
						if(this.point.datum)
							text = text  + '<br/>' + 'Datum: ' + this.point.datum;
						if(this.point.klinbed)
							text = text  + '<br/>' + this.point.klinbed;
					}
					else if(this.series.name == 'PSA') {
						if(this.point.datum)
							text = text  + '<br/>' + 'Datum: ' + this.point.datum;
						if(this.point.y)
							text = text  + '<br/>' + 'Värde: ' + this.point.y + ' [µg/L]';
					}
					else if(this.series.name == 'ALP') {
						if(this.point.datum)
							text = text  + '<br/>' + 'Datum: ' + this.point.datum;
						if(this.point.y)
							text = text  + '<br/>' + 'Värde: ' + this.point.y + ' [µkat/L]';
					}
					else if(this.series.name == 'Krea') {
						if(this.point.datum)
							text = text  + '<br/>' + 'Datum: ' + this.point.datum;
						if(this.point.y)
							text = text  + '<br/>' + 'Värde: ' + this.point.y + ' [µmol/L]';
					}
					else if(this.series.name == 'Hb') {
						if(this.point.datum)
							text = text  + '<br/>' + 'Datum: ' + this.point.datum;
						if(this.point.y)
							text = text  + '<br/>' + 'Värde: ' + this.point.y + ' [g/L]';
					}
					else if(this.series.name == 'Testosteron') {
						if(this.point.datum)
							text = text  + '<br/>' + 'Datum: ' + this.point.datum;
						if(this.point.y)
							text = text  + '<br/>' + 'Värde: ' + this.point.y + ' [nmol/L]';
					}
					else if(this.series.name == 'PSA dubblering') {
						if(this.point.dubbleringstid) {
							text = text + '<br/><i>' + 'PSA dubblering resultat:' + '</i>';
							text = text + '<br/>' + 'Dubbleringstid: ' + (this.point.dubbleringstid/365.25).toFixed(2) + ' år eller ' + Math.round(this.point.dubbleringstid) + ' dagar';
						}
						if(this.point.datum)
							text = text + '<br/>' + 'Dubbleringsdatum: ' + this.point.datum;
						if(this.point.y)
							text = text + '<br/>' + 'Dubbleringsvärde: ' + this.point.y + ' [µg/L]';
					}
					else if(this.series.name == 'PSA halvering') {
						if(this.point.halveringstid) {
							text = text + '<br/><i>' + 'PSA halvering resultat:' + '</i>';
							text = text + '<br/>' + 'Halveringstid: ' + (this.point.halveringstid/365.25).toFixed(2) + ' år eller ' + Math.round(this.point.halveringstid) + ' dagar';
						}
						if(this.point.datum)
							text = text + '<br/>' + 'Halveringsdatum: ' + this.point.datum;
						if(this.point.y)
							text = text + '<br/>' + 'Halveringsvärde: ' + this.point.y + ' [µg/L]';
					} else if(this.series.name == 'PROM enkäter') {
						if(this.point.datum)
							text = text + '<br/>' + 'Datum för inmatning: ' + this.point.datum;
						if(this.point.label !==	"")
							text = text + '<br/>' + 'Snittvärde: ' + this.point.label;
					} else {
						if(this.point.datum)
							text = text  + '<br/>' + 'Datum: ' + this.point.datum;
						if(this.point.handelse)
							text = text  + '<br/>' + 'Händelse: ' + this.point.handelse;
						if(this.point.orsak)
							text = text  + '<br/>' + 'Orsak: ' + this.point.orsak;
					}
					
					return text;
				} else if(this.series.name == 'PROM pie') {
					var text = '<b>' + this.point.name + '</b>'; 
					
					text = text + '<br/>' + 'Snittvärde: ' + this.point.value;
					
					text = text + '<br/><br/>' + 'Detaljinfo: ';
					text = text + '<br><i>' + 'Frekvens: </i>' + this.point.value;
					text = text + '<br><i>' + 'Kännbarhet: </i>' + this.point.value;
					text = text + '<br><i>' + 'Besvär, oro: </i>' + this.point.value;
					
					return text;
				}
				else
					return false;
            },
	        crosshairs: true,
		    shared: false
	    },
		
	    plotOptions: {
			pie: {
                dataLabels: {
                    enabled: false/*,
                    distance: 25,
                    style: {
                        fontWeight: 'bold',
                        color: 'white',
                        textShadow: '0px 1px 2px black'
                    }*/
                },
                slicedOffset: 0,
                startAngle: 0,
                endAngle: 360,
				stickyTracking: false,
				size: "6%"
			},
			series: {
				stickyTracking: false,
				cursor: 'pointer',
                point: {
                    events: {
                        click: function (event) {
							var self = this;
							
							if(event.ctrlKey && self.y == 14) {
								redrawOverviewPROMPie(self.values, self.plotX, vm.overviewLabHeight + vm.overviewLabPadding + vm.overviewPlotHeight + vm.overviewPlotPadding + vm.overviewAxisPadding);
							}
							if(event.altKey && self.y == 14) {
								redrawOverviewPROMPie([]);
							}
							
							if(event.ctrlKey && self.options.psa) {
								var index = $.inArray(self.x, self.series.xData); //self.series.xData.indexOf(self.x);
								if(index < self.series.data.length-2) {
									var psa1 = self.y;
									var psa1datum = self.x;

									var psa2 = "";
									var psa2datum;

									var psa3 = "";
									var psa3datum;
									
									var oneDay = 24*60*60*1000; // en dagsranson av millisekunder
									
									try {
										while(psa2 == "") {
											index++;
											if(index == self.series.data.length) {
												throw "Index out of bounds";
											}
											if(self.series.yData[index] >= psa1) {
												psa2 = self.series.yData[index];
												psa2datum = self.series.xData[index];
											}
										}
										
										while(psa3 == "") {
											index++;
											if(index == self.series.data.length) {
												throw "Index out of bounds";
											}
											if(self.series.yData[index] >= psa2) {
												psa3 = self.series.yData[index];
												psa3datum = self.series.xData[index];
												
												var datediff = Math.round(Math.abs((psa1datum - psa3datum)/(oneDay)));
												if(datediff <= 90 ) {
													psa3 = "";
												}
											}
										}
									} catch(err) {
										//alert(err);
										return;
									}
									
									$.each(self.series.data, function(i, point) {
										if(point.x == psa1datum) {
											point.graphic.attr({ fill: '#FF0000' });
										}
										if(point.x == psa2datum) {
											point.graphic.attr({ fill: '#FF0000' });
										}
										if(point.x == psa3datum) {
											point.graphic.attr({ fill: '#FF0000' });
										}
									});
									self.series.redraw();
	
									// Ia delen i uträkningen
									var psa1log = Math.log(psa1);
									var psa2log = Math.log(psa2);
									var psa3log = Math.log(psa3);
									
									var tid1 = 0;
									var tid2 = psa2datum - psa1datum;
									var tid3 = psa3datum - psa1datum;
									
									var tid1sqr = tid1*tid1;
									var tid2sqr = tid2*tid2;
									var tid3sqr = tid3*tid3;
									
									var Iasumma = (tid1sqr*psa1)+(tid2sqr*psa2)+(tid3sqr*psa3);

									// IIa delen i uträkningen
									var IIasumma = (psa1*psa1log)+(psa2*psa2log)+(psa3*psa3log);

									// IIIa delen i uträkningen
									var IIIasumma = (tid1*psa1)+(tid2*psa2)+(tid3*psa3);

									// IVa delen i uträkningen
									var IVasumma = (tid1*psa1*psa1log)+(tid2*psa2*psa2log)+(tid3*psa3*psa3log);

									// Va delen i uträkningen
									var Vasumma = psa1+psa2+psa3;

									// Uträkningen
									var a = ((Iasumma*IIasumma)-(IIIasumma*IVasumma))/((Vasumma*Iasumma)-(IIIasumma*IIIasumma));

									var b = ((Vasumma*IVasumma)-(IIIasumma*IIasumma))/((Vasumma*Iasumma)-(IIIasumma*IIIasumma));
									
									//Formel för uträkning av y : ln(y) = a+(b*x) => x = (ln(y)-a)/b
									var y = psa1*2;
									var x = (Math.log(y)-a)/b;

									var psadubbleringsdatum = psa1datum + x;
									//vm.U_psadubblering(psa1datum + x);
									
									redrawOverviewPSADubblering(psa1, psa1datum, psa2datum, psa3datum, psadubbleringsdatum, x);
									
								} else {
									alert('färre än tre psa tagna från det valda startvärdet');
									// getinfodialog("kan inte räkna ut psa-dubblering då det inte finns tre värden efter valt värde")
								}
							} else if(event.altKey && self.options.psa) {
								var index = $.inArray(self.x, self.series.xData); //self.series.xData.indexOf(self.x);
								if(index < self.series.data.length-1) {
									var psa1 = self.y;
									var psa1datum = self.x;

									var psa2 = "";
									var psa2datum;
									
									index++;
									if(self.series.yData[index] < psa1) {
										psa2 = self.series.yData[index];
										psa2datum = self.series.xData[index];
									} 
									
									index++;
									if(psa2 == "" && index < self.series.data.length) {
										psa2 = self.series.yData[index];
										psa2datum = self.series.xData[index];
									} 
									
									if(psa2 == "") {
										alert('kan inte räkna ut psa-halvering då det inte finns ett fallande värde direkt efter valt värde');
									}
									
									$.each(self.series.data, function(i, point) {
										if(point.x == psa1datum) {
											point.graphic.attr({ fill: '#00FF00' });
										}
										if(point.x == psa2datum) {
											point.graphic.attr({ fill: '#00FF00' });
										}
									});
									self.series.redraw();
									
									var slope = (psa1 - psa2)/(psa1datum - psa2datum);
									
									var x = ((psa1/2)-psa1)/slope;
									
									var halveringsdatum = psa1datum + x;
									
									redrawOverviewPSAhalvering(psa1, psa1datum, psa2datum, halveringsdatum, x);
									
								} else {
									alert('kan inte räkna ut psa-halvering då det inte finns ett fallande värde efter valt värde');
								}
							}
							else if((event.ctrlKey || event.altKey) && (self.options.psadbl || self.options.psahlv)) {
								self.remove();
							} else {
								$('#prostataprocess-tabs').tabs('option', 'active', self.options.tab-1);
								var active = $('#prostataprocess-tabs').tabs('option', 'active')+1;
								var subactive = "";
								var i = "";
								switch (active) {
									case 2:
										
										$('#B-accordion h3 a').each(function(index, elem) {
											var datum = $(elem).children('span').first().text();
											
											if(datum == self.options.datum) {
												i = index;
											}
											
										});
										
										if(i != "") {
											$('#B-accordion').accordion( "option", "active", i );
										}
										break;
									
									case 3:
										
										$('#BD-accordion h3 a').each(function(index, elem) {
											var datum = $(elem).children('span').first().text();
											
											if(datum == self.options.datum) {
												i = index;
											}
											
										});
										
										if(i != "") {
											$('#BD-accordion').accordion( "option", "active", i );
										}
										break;
									
									case 4:
										
										$('#SB-accordion h3 a').each(function(index, elem) {
											var datum = $(elem).children('span').first().text();
											
											if(datum == self.options.datum) {
												i = index;
											}
											
										});
										
										if(i != "") {
											$('#SB-accordion').accordion( "option", "active", i );
										}
										break;
									
									case 5:
										
										$('#SE-accordion h3 a').each(function(index, elem) {
											var datum = $(elem).children('span').first().text();
											
											if(datum == self.options.datum) {
												i = index;
											}
											
										});
										
										if(i != "") {
											$('#SE-accordion').accordion( "option", "active", i );
										}
										break;
									
									case 7:
										
										$('#prostataprocess-lmtabs ul li a span.rcc-small span').each(function(index, elem) {
											var datum = $(elem).text();
											
											if(datum == self.options.startdatum) {
												i = index;
											}
											
										});
										
										if(i != "") {
											$('#prostataprocess-lmtabs').tabs( "option", "active", i );
										}
										break;
									
									default:
										break;
								}
							}
                        }
                    }
                }
			},
	        line: {
	            lineWidth: 12,
				cursor: 'pointer',
				shadow: false,
				stickyTracking: false,
	            marker: {
	                symbol: 'circle',
	                lineWidth: 2, 
	                lineColor: '#FFFFFF',
					  radius: 6              
	            },
				events: {
                   hide: function(event) {
                		/*if(!vm.overviewHighChart.series[7].visible && !vm.overviewHighChart.series[8].visible && !vm.overviewHighChart.series[9].visible && !vm.overviewHighChart.series[10].visible && !vm.overviewHighChart.series[11].visible){
							vm.overviewHighChart.setSize(vm.overviewWidth, vm.overviewPlotHeight + vm.overviewPlotPadding + vm.overviewLMHeight + vm.overviewLabPadding);
						}*/
                   },
					show: function(event) {
						/*if(vm.overviewHighChart.chartHeight == (vm.overviewPlotHeight + vm.overviewPlotPadding + vm.overviewLMHeight + vm.overviewLabPadding)){
							vm.overviewHighChart.setSize(vm.overviewWidth, vm.overviewPlotHeight + vm.overviewPlotPadding + vm.overviewLMHeight + vm.overviewLMPadding + vm.overviewLabHeight + vm.overviewLabPadding);
						}*/
						
						if(event.currentTarget.index == 2){
							if(!vm.overviewHighChart.series[12].visible)
								vm.overviewHighChart.series[12].show();
							
							$('#PSA-log-lin').css("visibility", "visible");
							$('#PSA-help').css("visibility", "visible");
							
							if(vm.overviewHighChart.series[3].visible)
								vm.overviewHighChart.series[3].hide();
							else if(vm.overviewHighChart.series[4].visible)
								vm.overviewHighChart.series[4].hide();
							else if(vm.overviewHighChart.series[5].visible)
								vm.overviewHighChart.series[5].hide();
							else if(vm.overviewHighChart.series[6].visible)
								vm.overviewHighChart.series[6].hide();
						}
						else if(event.currentTarget.index == 3){
							$('#PSA-log-lin').css("visibility", "hidden");
							$('#PSA-help').css("visibility", "hidden");
							if(vm.overviewHighChart.series[2].visible)
								vm.overviewHighChart.series[2].hide();
								if(vm.overviewHighChart.series[12].visible)
									vm.overviewHighChart.series[12].hide();
							else if(vm.overviewHighChart.series[4].visible)
								vm.overviewHighChart.series[4].hide();
							else if(vm.overviewHighChart.series[5].visible)
								vm.overviewHighChart.series[5].hide();
							else if(vm.overviewHighChart.series[6].visible)
								vm.overviewHighChart.series[6].hide();
						}
						else if(event.currentTarget.index == 4){
							$('#PSA-log-lin').css("visibility", "hidden");
							$('#PSA-help').css("visibility", "hidden");
							if(vm.overviewHighChart.series[2].visible)
								vm.overviewHighChart.series[2].hide();
								if(vm.overviewHighChart.series[12].visible)
									vm.overviewHighChart.series[12].hide();
							else if(vm.overviewHighChart.series[3].visible)
								vm.overviewHighChart.series[3].hide();
							else if(vm.overviewHighChart.series[5].visible)
								vm.overviewHighChart.series[5].hide();
							else if(vm.overviewHighChart.series[6].visible)
								vm.overviewHighChart.series[6].hide();
						}
						else if(event.currentTarget.index == 5){
							$('#PSA-log-lin').css("visibility", "hidden");
							$('#PSA-help').css("visibility", "hidden");
							if(vm.overviewHighChart.series[2].visible)
								vm.overviewHighChart.series[2].hide();
								if(vm.overviewHighChart.series[12].visible)
									vm.overviewHighChart.series[12].hide();
							else if(vm.overviewHighChart.series[3].visible)
								vm.overviewHighChart.series[3].hide();
							else if(vm.overviewHighChart.series[4].visible)
								vm.overviewHighChart.series[4].hide();
							else if(vm.overviewHighChart.series[6].visible)
								vm.overviewHighChart.series[6].hide();
						}
						else if(event.currentTarget.index == 6){
							$('#PSA-log-lin').css("visibility", "hidden");
							$('#PSA-help').css("visibility", "hidden");
							if(vm.overviewHighChart.series[2].visible)
								vm.overviewHighChart.series[2].hide();
								if(vm.overviewHighChart.series[12].visible)
									vm.overviewHighChart.series[12].hide();
							else if(vm.overviewHighChart.series[3].visible)
								vm.overviewHighChart.series[3].hide();
							else if(vm.overviewHighChart.series[4].visible)
								vm.overviewHighChart.series[4].hide();
							else if(vm.overviewHighChart.series[5].visible)
								vm.overviewHighChart.series[5].hide();
						}
                    }
                },
	            dataLabels: {
	                enabled: true,
	                align: 'center',
	                formatter: function() {
	                    return this.point.options && this.point.options.label;
	                }
	            }
	        },
			scatter: {
	            marker: {
	                symbol: 'circle',
	                lineWidth: 2, 
	                lineColor: '#FFFFFF',
					  radius: 6            
	            },
				stickyTracking: false
	        }
	    },
	
	    series: [{
        	 name: 'Senast sparad',
			 type: 'scatter', 
			 yAxis: 2,
			 marker: {
		        enabled: false
		     },
			 showInLegend: false,
			 data: []			 
        },{
        	 name: 'CRPC datum',
			 type: 'scatter', 
			 yAxis: 2,
			 marker: {
		        enabled: false
		     },
			 showInLegend: false,
			 data: []			 
        },{
        	 name: 'PSA',
			 color: '#000080',
			 visible: true,
			 marker : {
				radius : 5
			 },
			 legendIndex: 0,
			 shadow : false,
			 yAxis: 5,
			 zIndex: 1,
	         lineWidth: 0.75
        },{
        	 name: 'ALP',
			 color: '#008080',
			 visible: false,
			 marker : {
				radius : 5
			 },
			 legendIndex: 1,
			 shadow : false,
			 yAxis: 0,
	         lineWidth: 0.75
        },{
        	 name: 'Krea',
			 color: '#006400',
			 visible: false,
			 marker : {
				radius : 5
			 },
			 legendIndex: 2,
			 shadow : false,
			 yAxis: 0,
	         lineWidth: 0.75
        },{
        	 name: 'Hb',
			 color: '#800000',
			 visible: false,
			 marker : {
				radius : 5
			 },
			 legendIndex: 3,
			 shadow : false,
			 yAxis: 0,
	         lineWidth: 0.75
        },{
        	 name: 'Testo',
			 color: '#696969',
			 visible: false,
			 marker : {
				radius : 5
			 },
			 legendIndex: 4,
			 shadow : false,
			 yAxis: 0,
	         lineWidth: 0.75
        },{
        	 name: 'ECOG-WHO',
			 color: '#d3d3d3',
			 type: 'scatter', 
			 yAxis: 2,
			 marker: {
		        symbol: 'square',
				radius: 8,
				lineWidth: 0
		     },
			 dataLabels: {
                enabled: true,
				align: 'center',
				x: -3,
				y: -5,
				formatter: function() {
					var text = '';
					if(this.point.ecogwholabel)
						text = text + this.point.ecogwholabel;
					return text;
				}
             },
			 showInLegend: false,
	         lineWidth: 0
        },{
        	 name: 'Sammantagen bedömning',
			 type: 'scatter', 
			 yAxis: 2,
			 marker: {
		        symbol: 'triangle',
				radius: 6
		     },
			 dataLabels: {
                enabled: true,
				align: 'right',
				x: -5,
				y: 9,
				formatter: function() {
					var text = '';
					if(this.point.klinbedlabel) {
						text = this.point.klinbedlabel;
						if(text.length == 1) {
							text = "\u00A0" + text;
						}
					}
					return text;
				}
             },
			 showInLegend: false,
			 data: []
        },{
        	 name: 'Bilddiagnostik',
			 color: '#484848',
			 type: 'scatter', 
			 yAxis: 3,
			 marker: {
		        symbol: 'triangle-down',
				radius: 6
		     },
			 showInLegend: false,
			 data: []			 
        },{
        	 name: 'Strålbehandling',
			 color: '#484848',
			 type: 'scatter', 
			 yAxis: 3,
			 marker: {
		        symbol: 'square',
				radius: 5
		     },
			 showInLegend: false,
			 data: []
        },{
        	 name: 'SRE',
			 color: '#484848',
			 type: 'scatter',			 
			 yAxis: 3,
			 marker: {
		        symbol: 'diamond',
				radius: 6
		     },
			 showInLegend: false,
			 data: []		 
        },{
        	 name: 'PSA dubblering',
			 color: '#ff0000',
			 visible: true,
			 marker: {
		        symbol: 'diamond',
				radius: 8
		     },
			 showInLegend: false,
			 shadow : false,
			 yAxis: 5,
	         lineWidth: 0.75,
			 zIndex: 1
        },{
        	 name: 'PSA halvering',
			 color: '#00ff00',
			 visible: true,
			 marker: {
		        symbol: 'diamond',
				radius: 8
		     },
			 showInLegend: false,
			 shadow : false,
			 yAxis: 5,
	         lineWidth: 0.75,
			 zIndex: 2
        },{
        	 name: 'PROM enkäter',
			 type: 'scatter', 
			 yAxis: 6,
			 marker: {
		        symbol: 'circle',
				radius: 8
		     },
			 dataLabels: {
                enabled: false,
				align: 'right',
				style: {
		            color: '#222'
		         },
				x: -6,
				y: -4,
				formatter: function() {
					var text = '';
					if(this.point.label !==	"") {
						text = this.point.label + "";
						if(text.length == 1) {
							text = "\u00A0" + text;
						}
					}
					return text;
				}
             },
			 showInLegend: false,
			 data: []
        },{
			 type: 'pie',
        	 name: 'PROM pie',
             innerSize: '11%',
			 yAxis: 6,
			 zIndex: 10,
			 showInLegend: false,
			 data: []
        }/*,{
        	 name: 'ECOG-WHO',
			 color: '#f8f8f8',
			 type: 'scatter', 
			 yAxis: 2,
			 marker: {
		        symbol: 'square',
				radius: 8
		     },
			 dataLabels: {
                enabled: true,
				y:+10,
				formatter: function() { 
					var text = '';
					if(this.point.ecogwholabel)
						text = text + this.point.ecogwholabel;
					return text;
				}
             },
			 showInLegend: false,
			 data: []			 
        }*/
		]
	
	});
	
};


/* REDRAW FUNKTIONER */

/* OVERVIEW */


/**
* Funktion för uppdatering av Root data för Highcharts
*
* @method redrawOverviewRootChart
*/
redrawOverviewRootChart = function(vm) {
	if(vm.U_crpcdatum()) {
		var crpcDatumsplit = vm.U_crpcdatum().split("-");
		// remove old crpc plotline
		vm.overviewHighChart.xAxis[0].removePlotLine('crpc-plotline');
		// add new crpc plotline
		vm.overviewHighChart.xAxis[0].addPlotLine({id: 'crpc-plotline', value: Date.UTC(crpcDatumsplit[0], (crpcDatumsplit[1] - 1), (crpcDatumsplit[2])), width: 1, color: '#92a2bb', dashStyle: 'shortdash',  zIndex: 2, label: { align: 'left', textAlign: 'left', x: 5, rotation: 0, style:{fontSize:'10px',color:'#92a2bb'}, text: 'CRPC Resistent<br>' + vm.U_crpcdatum()}});
		// add invisible point to crpc series
		vm.overviewHighChart.series[1].setData([{x: Date.UTC(crpcDatumsplit[0], (crpcDatumsplit[1] - 1), crpcDatumsplit[2]), y: 0}], false);
	} else {
		vm.overviewHighChart.xAxis[0].removePlotLine('crpc-plotline');
	}
};

/* LAB */

/**
* Funktion för uppdatering av  Labbprov, PSA data för Highcharts
*
* @method redrawLabPSAChart
*/
redrawOverviewPSAChart = function(vm) {
	var data = [];
	
	ko.utils.arrayForEach(vm.sortTable(vm.$$.Labbprov, 'LP_datum', 'ascending'), function (item) {
		var datum = item.LP_datum();
		var value = item.LP_psa();
		
		if(value) {
			if(typeof value == 'string')
				value = parseFloat(value.replace(',','.'));
				
			if(datum) {
				var datumsplit = datum.split("-");
				if(!isNaN(value))
					data.push({x: Date.UTC(datumsplit[0], (datumsplit[1] - 1), datumsplit[2]), y: value, tab: '6', psa: true, datum: datum, tooltip: true});
				else
					data.push({x: Date.UTC(datumsplit[0], (datumsplit[1] - 1), datumsplit[2]), y: 0, tab: '6', psa: true, datum: datum, tooltip: true});
			}
		}
		
		
	});
	
	if(vm.overviewHighChart) {
		vm.overviewHighChart.series[2].setData(data, false);
		
		vm.modifiedPSA(true);
		
	}
};

/**
* Funktion för uppdatering av PSA-dubbleringstid
*
* @method redrawOverviewPSADubblering
*/
redrawOverviewPSADubblering = function(psa1, psa1datum, psa2datum, psa3datum, psadubbleringsdatum, psadubbleringstid) {
	
	var data = [];
	
	if(psadubbleringstid > 0) {
		data.push({x: psadubbleringsdatum, y: psa1*2, datum: new Date(psadubbleringsdatum).getFullYear() + '-' + ((''+(new Date(psadubbleringsdatum).getMonth()+1)).length < 2 ? '0' : '') + (new Date(psadubbleringsdatum).getMonth() + 1) + '-' + ((''+new Date(psadubbleringsdatum).getDate()).length < 2 ? '0' : '') + new Date(psadubbleringsdatum).getDate(), startpsa: psa1, datum1: new Date(psa1datum).toJSON().slice(0,10), datum2: new Date(psa2datum).toJSON().slice(0,10), datum3: new Date(psa3datum).toJSON().slice(0,10), dubbleringstid: psadubbleringstid/(24*60*60*1000), psadbl: true, tooltip: true});
		
	}
	
	if(vm.overviewHighChart) {
		vm.overviewHighChart.series[12].setData(data, true);
	}
};

/**
* Funktion för uppdatering av PSA-dubbleringstid
*
* @method redrawOverviewPSAhalvering
*/
redrawOverviewPSAhalvering = function(psa1, psa1datum, psa2datum, psahalveringsdatum, psahalveringstid) {
	
	var data = [];
	
	if(psahalveringstid > 0) {
		data.push({x: psahalveringsdatum, y: psa1/2, datum: new Date(psahalveringsdatum).getFullYear() + '-' + ((''+(new Date(psahalveringsdatum).getMonth()+1)).length < 2 ? '0' : '') + (new Date(psahalveringsdatum).getMonth() + 1) + '-' + ((''+new Date(psahalveringsdatum).getDate()).length < 2 ? '0' : '') + new Date(psahalveringsdatum).getDate(), startpsa: psa1, datum1: new Date(psa1datum).toJSON().slice(0,10), datum2: new Date(psa2datum).toJSON().slice(0,10), halveringstid: psahalveringstid/(24*60*60*1000), psahlv: true, tooltip: true});
		
	}
	
	if(vm.overviewHighChart) {
		vm.overviewHighChart.series[13].setData(data, true);
	}
};

/**
* Funktion för uppdatering av  Labbprov, PSA data för Highcharts
*
* @method redrawLabALPChart
*/
redrawOverviewALPChart = function(vm) {
	var data = [];
	
	ko.utils.arrayForEach(vm.sortTable(vm.$$.Labbprov, 'LP_datum', 'ascending'), function (item) {
		var datum = item.LP_datum();
		var value = item.LP_alp();
		
		if(value){
			if(typeof value == 'string')
				value = parseFloat(value.replace(',','.'));
				
			if(datum){
				var datumsplit = datum.split("-");
				if(!isNaN(value))
					data.push({x: Date.UTC(datumsplit[0], (datumsplit[1] - 1), datumsplit[2]), y: value, tab: '6', datum: datum, tooltip: true});
				else
					data.push({x: Date.UTC(datumsplit[0], (datumsplit[1] - 1), datumsplit[2]), y: 0, tab: '6', datum: datum, tooltip: true});
			}
		}
	});
	
	if(vm.overviewHighChart) {
		vm.overviewHighChart.series[3].setData(data, false);
		
		vm.modifiedALP(true);
		
	}
};

/**
* Funktion för uppdatering av  Labbprov, Krea data för Highcharts
*
* @method redrawLabKreaChart
*/
redrawOverviewKreaChart = function(vm) {
	var data = [];
	
	ko.utils.arrayForEach(vm.sortTable(vm.$$.Labbprov, 'LP_datum', 'ascending'), function (item) {
		var datum = item.LP_datum();
		var value = item.LP_krea();
		
		if(value){
			if(typeof value == 'string')
				value = parseFloat(value.replace(',','.'));
				
			if(datum){
				var datumsplit = datum.split("-");
				if(!isNaN(value))
					data.push({x: Date.UTC(datumsplit[0], (datumsplit[1] - 1), datumsplit[2]), y: value, tab: '6', datum: datum, tooltip: true});
				else
					data.push({x: Date.UTC(datumsplit[0], (datumsplit[1] - 1), datumsplit[2]), y: 0, tab: '6', datum: datum, tooltip: true});
			}
		}
		
		
	});
	
	if(vm.overviewHighChart) {
		vm.overviewHighChart.series[4].setData(data, false);
		
		vm.modifiedKrea(true);
		
	}
};

/**
* Funktion för uppdatering av  Labbprov, Hb data för Highcharts
*
* @method redrawLabHbChart
*/
redrawOverviewHbChart = function(vm) {
	var data = [];
	
	ko.utils.arrayForEach(vm.sortTable(vm.$$.Labbprov, 'LP_datum', 'ascending'), function (item) {
		var datum = item.LP_datum();
		var value = item.LP_hb();
		
		if(value){
			if(typeof value == 'string')
				value = parseFloat(value.replace(',','.'));
				
			if(datum){
				var datumsplit = datum.split("-");
				if(!isNaN(value))
					data.push({x: Date.UTC(datumsplit[0], (datumsplit[1] - 1), datumsplit[2]), y: value, tab: '6', datum: datum, tooltip: true});
				else
					data.push({x: Date.UTC(datumsplit[0], (datumsplit[1] - 1), datumsplit[2]), y: 0, tab: '6', datum: datum, tooltip: true});
			}
		}
		
	});
	
	if(vm.overviewHighChart) {
		vm.overviewHighChart.series[5].setData(data, false);
		
		vm.modifiedHb(true);
		
	}
};

/**
* Funktion för uppdatering av  Labbprov, Hb data för Highcharts
*
* @method redrawLabHbChart
*/
redrawOverviewTestosteronChart = function(vm) {
	var data = [];
	
	ko.utils.arrayForEach(vm.sortTable(vm.$$.Labbprov, 'LP_datum', 'ascending'), function (item) {
		var datum = item.LP_datum();
		var value = item.LP_testosteron();
		
		if(value){
			if(typeof value == 'string')
				value = parseFloat(value.replace(',','.'));
				
			if(datum){
				var datumsplit = datum.split("-");
				if(!isNaN(value))
					data.push({x: Date.UTC(datumsplit[0], (datumsplit[1] - 1), datumsplit[2]), y: value, tab: '6', datum: datum, tooltip: true});
				else
					data.push({x: Date.UTC(datumsplit[0], (datumsplit[1] - 1), datumsplit[2]), y: 0, tab: '6', datum: datum, tooltip: true});
			}
		}
		
	});
	
	if(vm.overviewHighChart) {
		vm.overviewHighChart.series[6].setData(data, false);
		
		vm.modifiedTesto(true);
		
	}
};


/**
* Funktion för uppdatering av Besök  data för Highcharts
*
* @method redrawOverviewBChart
*/
redrawOverviewBChart = function(vm) {
	var crpc = false;
	var dataEW = [];
	var dataSB = [];
	
	ko.utils.arrayForEach(vm.sortTable(vm.$$.Besök, 'B_besokdatum', 'ascending'), function (b) {
		var datum = b.B_besokdatum();
		if(datum){
			var datumsplit = datum.split("-");
			if(b.B_ecogwho()) {
				var color;
				var value = b.B_ecogwho().value;
				if(typeof value == 'string')
					value = parseFloat(value.replace(',','.'));
				if(value == 2) {
					color = '#ffe500';
				} else if(value == 3) {
					color = '#ffb500';
				} else if(value == 4) {
					color = '#ff7500';
				} else {
					color = '#d3d3d3';
				}
				dataEW.push({x: Date.UTC(datumsplit[0], (datumsplit[1] - 1), datumsplit[2]), y: 1, tab: '2', datum: datum, ecogwholabel: b.B_ecogwho().value, tooltip: true, marker: { fillColor: color, states: { hover: { fillColor: color }}}});
			}
			if(b.B_klinbed()) {
				if(b.B_klinbed().value == 0)
					dataSB.push({x: Date.UTC(datumsplit[0], (datumsplit[1] - 1), datumsplit[2]), y: 0, tab: '2', datum: datum, klinbed: b.B_klinbed().text, klinbedlabel: 'R', color: '#86cd86', marker: { fillColor: '#86cd86', lineColor: '#64ab64', lineWidth: 2, states: { hover: { fillColor: '#86cd86', lineColor: '#64ab64', lineWidth: 2}}}, tooltip: true});
				else if(b.B_klinbed().value == 1)
					dataSB.push({x: Date.UTC(datumsplit[0], (datumsplit[1] - 1), datumsplit[2]), y: 0, tab: '2', datum: datum, klinbed: b.B_klinbed().text, klinbedlabel: 'S', color: '#aaaaaa', marker: { fillColor: '#aaaaaa', lineColor: '#777777', lineWidth: 2, states: { hover: { fillColor: '#aaaaaa', lineColor: '#777777', lineWidth: 2}}}, tooltip: true});
				else if(b.B_klinbed().value == 2)
					dataSB.push({x: Date.UTC(datumsplit[0], (datumsplit[1] - 1), datumsplit[2]), y: 0, tab: '2', datum: datum, klinbed: b.B_klinbed().text, klinbedlabel: 'P', color: '#ff0000', marker: { fillColor: '#ff0000', lineColor: '#cc0000', lineWidth: 2, states: { hover: { fillColor: '#ff0000', lineColor: '#cc0000', lineWidth: 2}}}, tooltip: true});
				else if(b.B_klinbed().value == 3)
					dataSB.push({x: Date.UTC(datumsplit[0], (datumsplit[1] - 1), datumsplit[2]), y: 0, tab: '2', datum: datum, klinbed: b.B_klinbed().text, klinbedlabel: 'AB', color: '#dddddd', marker: { fillColor: '#dddddd', lineColor: '#aaaaaa', lineWidth: 2, states: { hover: { fillColor: '#dddddd', lineColor: '#aaaaaa', lineWidth: 2}}}, tooltip: true});
				else if(b.B_klinbed().value == 4)
					dataSB.push({x: Date.UTC(datumsplit[0], (datumsplit[1] - 1), datumsplit[2]), y: 0, tab: '2', datum: datum, klinbed: b.B_klinbed().text, klinbedlabel: 'KR', color: '#008600', marker: { fillColor: '#008600', lineColor: '#006400', lineWidth: 2, states: { hover: { fillColor: '#008600', lineColor: '#006400', lineWidth: 2}}}, tooltip: true});
			}
		}	
		
	});
	
	if(vm.overviewHighChart){
		vm.overviewHighChart.series[7].setData(dataEW, false);
		vm.overviewHighChart.series[8].setData(dataSB, false);
	}
};


/**
* Funktion för uppdatering av PROM data för Highcharts
*
* @method redrawOverviewPROMChart
*/
redrawOverviewPROMChart = function(vm) {
	var median = 0;
	var data = [];
	
	ko.utils.arrayForEach(vm.sortTable(vm.$$.PROM, 'PROM_datum', 'ascending'), function (p) {
		var datum = p.PROM_datum();
		
		if(datum) {
			var datumsplit = datum.split("-");
			
			var values = [{y: 1, value: p.PROM_fr1(), name: p.PROM_fr1.rcc.regvar.description}, {y: 1, value: p.PROM_fr2(), name: p.PROM_fr2.rcc.regvar.description}, {y: 1, value: p.PROM_fr3(), name: p.PROM_fr3.rcc.regvar.description}, {y: 1, value: p.PROM_fr4(), name: p.PROM_fr4.rcc.regvar.description}, {y: 1, value: p.PROM_fr5(), name: p.PROM_fr5.rcc.regvar.description}, {y: 1, value: p.PROM_fr6(), name: p.PROM_fr6.rcc.regvar.description}, {y: 1, value: p.PROM_fr7(), name: p.PROM_fr7.rcc.regvar.description}, {y: 1, value: p.PROM_fr8(), name: p.PROM_fr8.rcc.regvar.description}, {y: 1, value: p.PROM_fr9(), name: p.PROM_fr9.rcc.regvar.description}, {y: 1, value: p.PROM_fr10(), name: p.PROM_fr10.rcc.regvar.description}, {y: 1, value: p.PROM_fr11(), name: p.PROM_fr11.rcc.regvar.description}, {y: 1, value: p.PROM_fr12(), name: p.PROM_fr12.rcc.regvar.description}, {y: 1, value: p.PROM_fr13(), name: p.PROM_fr13.rcc.regvar.description}, {y: 1, value: p.PROM_fr14(), name: p.PROM_fr14.rcc.regvar.description}, {y: 1, value: p.PROM_fr15(), name: p.PROM_fr15.rcc.regvar.description}, {y: 1, value: p.PROM_fr16(), name: p.PROM_fr16.rcc.regvar.description}];
			//var values = [p.PROM_fr1(), p.PROM_fr2(), p.PROM_fr3(), p.PROM_fr4(), p.PROM_fr5(), p.PROM_fr6(), p.PROM_fr7(), p.PROM_fr8(), p.PROM_fr9(), p.PROM_fr10()];
			
			var avg = 0.0;
			
			avg = 10*(p.PROM_fr1() + p.PROM_fr2() + p.PROM_fr3() + p.PROM_fr4() + p.PROM_fr5() + p.PROM_fr6() + p.PROM_fr7() + p.PROM_fr8() + p.PROM_fr9() + p.PROM_fr10())/100;
			
			var h = Math.floor((5 - (avg+1)) * 120 / 5);
			var s = 1;
			var v = 1;
			
			/*var r = Math.round(((1+avg)/5)*255-((1/(avg+1))*16));
			if(r < 0)
				r = 0;
			var g = Math.round((1/(avg+1))*255-(((avg+1)/5)*64));
			if(g < 0)
				g = 0;
			var b = 16;*/
			
			//var color = "#" + ((1 << 24) + (r << 16) + (g << 8) + b).toString(16).slice(1);
			var color = hsv2rgb(h, s, 0.9);
			
			var linecolor = hsv2rgb(h, s, 0.67);
			
			data.push({x: Date.UTC(datumsplit[0], (datumsplit[1] - 1), datumsplit[2]), y: 14, datum: datum, label: avg, values: values, marker: { fillColor: color, lineColor: linecolor, lineWidth: 2, states: { hover: { fillColor: color, lineColor: linecolor, lineWidth: 2}}}, tooltip: true});
			
		}
		
	});
	
	if(vm.overviewHighChart) {
		vm.overviewHighChart.series[14].setData(data, false);
	}
};


/**
* Funktion för uppdatering av PSA-dubbleringstid
*
* @method redrawOverviewPSAhalvering
*/
redrawOverviewPROMPie = function(values, x, y) {
	
	var data = [];
	
	if(!values == []) {
		
		for(var i = 0; i < values.length; i++) {
			
			var h = Math.floor((5 - (values[i].value+1)) * 120 / 5);
			var s = 1;
			var v = 1;
			
			var color = hsv2rgb(h, s, 0.9);
			
			values[i].color = color;
			values[i].dataLabels = { format: values[i].name + " " + values[i].value };
			
		}
		
		data = values;
		
	}
	//data.push({x: psahalveringsdatum, y: psa1/2, datum: new Date(psahalveringsdatum).getFullYear() + '-' + ((''+(new Date(psahalveringsdatum).getMonth()+1)).length < 2 ? '0' : '') + (new Date(psahalveringsdatum).getMonth() + 1) + '-' + ((''+new Date(psahalveringsdatum).getDate()).length < 2 ? '0' : '') + new Date(psahalveringsdatum).getDate(), startpsa: psa1, datum1: new Date(psa1datum).toJSON().slice(0,10), datum2: new Date(psa2datum).toJSON().slice(0,10), halveringstid: psahalveringstid/(24*60*60*1000), psahlv: true, tooltip: true});
	
	if(vm.overviewHighChart) {
		vm.overviewHighChart.series[15].setData(data, false);
		vm.overviewHighChart.series[15].update({center: [x, y]}, true);
	}
};

/**
* Funktion för uppdatering av  Bilddiagnostik  data för Highcharts
*
* @method redrawOverviewBDChart
*/
redrawOverviewBDChart = function(vm) {
	var data = [];
	
	ko.utils.arrayForEach(vm.sortTable(vm.$$.Bilddiagnostik, 'BD_datum', 'ascending'), function (bd) {
		var datum = bd.BD_datum();
		var radbed;
		var typ;
		var metastaser = [];
		
		if(bd.BD_radbed())
			radbed = bd.BD_radbed().text;
		
		if(bd.BD_radundertyp())
			typ = bd.BD_radundertyp().text;
		
		if(bd.$$.BD_Metastas) {
			ko.utils.arrayForEach(bd.$$.BD_Metastas(), function (metastas) {
				if(metastas.BDM_metastaslokal())
					if(metastas.BDM_metastaslokal().value == '5')
						metastaser.push(metastas.BDM_metastaslokalann());
					else
						metastaser.push(metastas.BDM_metastaslokal().text);	
			});
		}
		
		if(datum) {
			var datumsplit = datum.split("-");
			data.push({x: Date.UTC(datumsplit[0], (datumsplit[1] - 1), datumsplit[2]), y: 2, tab: '3', datum: datum, radbed: radbed, metastaser: metastaser, typ: typ, marker: { fillColor: '#606060', lineColor: '#303030', lineWidth: 2, states: { hover: { fillColor: '#606060', lineColor: '#303030', lineWidth: 2}}}, tooltip: true});
		}
	});

	if(vm.overviewHighChart)
		vm.overviewHighChart.series[9].setData(data, false);
};


/**
* Funktion för uppdatering av Strålbehandling  data för Highcharts
*
* @method redrawOverviewSBChart
*/
redrawOverviewSBChart = function(vm) {
	var data = [];
	
	ko.utils.arrayForEach(vm.sortTable(vm.$$.Strålbehandling, 'SB_slutdatum', 'ascending'), function (sb) {
		var slutdatum = sb.SB_slutdatum();
		var grupper = [];
		
		if(sb.$$.SB_Grupp){
			ko.utils.arrayForEach(sb.$$.SB_Grupp(), function (grupp) {
				var grupp;
				grupp.frakdos = grupp.SBG_frakdos();
				grupp.slutdos = grupp.SBG_slutdos();
				grupp.lokaler = [];
				
				if(grupp.$$.SBG_Lokal){
					ko.utils.arrayForEach(grupp.$$.SBG_Lokal(), function (lokal) {
						if(lokal.SBGL_lokal())
							if(lokal.SBGL_lokal().value == '2' && lokal.SBGL_lokalskelett())
								grupp.lokaler.push(lokal.SBGL_lokal().text + " (" + lokal.SBGL_lokalskelett() + ")");	
							else if(lokal.SBGL_lokal().value == '5')
								grupp.lokaler.push(lokal.SBGL_lokalann());
							else
								grupp.lokaler.push(lokal.SBGL_lokal().text);
					});
				}
				grupper.push(grupp);
			});
		}
		
		if(slutdatum){
			var slutdatumsplit = slutdatum.split("-");
			data.push({x: Date.UTC(slutdatumsplit[0], (slutdatumsplit[1] - 1), slutdatumsplit[2]), y: 3, tab: '4', slutdatum: slutdatum, grupper: grupper, marker: { fillColor: '#606060', lineColor: '#303030', lineWidth: 2, states: { hover: { fillColor: '#606060', lineColor: '#303030', lineWidth: 2}}}, tooltip: true});
		}
	});
	
	if(vm.overviewHighChart)
		vm.overviewHighChart.series[10].setData(data, false);
};


/**
* Funktion för uppdatering av SRE  data för Highcharts
*
* @method redrawOverviewSEChart
*/
redrawOverviewSEChart = function(vm) {
	var data = [];
	
	ko.utils.arrayForEach(vm.sortTable(vm.$$.SRE, 'SRE_datum', 'ascending'), function (sre) {
		var datum = sre.SRE_datum();
		var handelser = [];
			
		if(sre.$$.SRE_Händelse){
			ko.utils.arrayForEach(sre.$$.SRE_Händelse(), function (handelse) {
				if(handelse.SREH_handelse())
					handelser.push(handelse.SREH_handelse().text);
			});
		}	
		
		if(datum){
			var datumsplit = datum.split("-");
			data.push({x: Date.UTC(datumsplit[0], (datumsplit[1] - 1), datumsplit[2]), y: 4, tab: '5', datum: datum, handelser: handelser, marker: { fillColor: '#606060', lineColor: '#303030', lineWidth: 2, states: { hover: { fillColor: '#606060', lineColor: '#303030', lineWidth: 2}}}, tooltip: true});
		}
	});
	
	if(vm.overviewHighChart)
		vm.overviewHighChart.series[11].setData(data, false);
};

/**
* Funktion för uppdatering av Läkemedel  data för Highcharts
*
* @method redrawOverviewLMCharts
*/
redrawOverviewLMCharts = function(vm) {
	var index = 16;
	var length = vm.overviewHighChart.series.length;
	
	// ta bort lm serier
	for (var i = length - 1; i > (index - 1); i--) {
		vm.overviewHighChart.series[i].remove(false);
	}
	
	// ta bort lm labels
	if(vm.overviewHighChartLabels)
		vm.overviewHighChartLabels = vm.overviewHighChartLabels.slice(0,index);
	
	
	ko.utils.arrayForEach(vm.sortLMTable(vm.$$.Läkemedel()), function (lm) {
		if(lm.LM_typ() && (vm.firstLMEventDate(lm) && ((lm.LM_studielkm() && lm.LM_studielkmtxt()) || (lm.LM_typ() && vm.getSelectedSubstance(lm.LM_substans))) || (lm.LM_ablatiotestis() && lm.LM_ablatiotestisdatum()))){
			var lmName = null;
			var lmColor = '#000000';
			
			if(lm.LM_studielkm() && lm.LM_studielkmtxt()) {
				lmName = lm.LM_studielkmtxt() + " (studie)";
				lmColor = "#000000";
			}
			else if(lm.LM_ablatiotestis() && lm.LM_ablatiotestisdatum()) {
				lmName = "ablatio testis (behandling)";
				lmColor = "#000000";
			}
			else if(lm.LM_typ() && lm.LM_substans()) {
				lmName = vm.getSelectedSubstance(lm.LM_substans).sub_subnamnrek;
				//lmColor = vm.getSelectedSubstance(lm.LM_substans).kat_substansfarg;
			}
			
			var lmNumber = index;
			
			// lägg till lm label
			vm.overviewHighChartLabels.push(lmName);
			
			//  ablatio testis
			if(lm.LM_ablatiotestis()) {
				var behandlingsDatum = lm.LM_ablatiotestisdatum();

				if(behandlingsDatum) {
					var behandlingsDatumSplit = behandlingsDatum.split("-");
					var handelse = 'Behandling';
					
					// skapa punkt av behandlingen
					var lmBehandlingPoint = {
						name: lmName,
						yAxis: 4,
						showInLegend: false,
						zIndex: 2,
						color: lmColor,
						type: 'scatter',
						marker: {
							symbol: 'diamond',
							radius: 6
						}
					}
					
					// skapa dataobjekt och lägg till behandlingen till punkten
					lmBehandlingPoint.data = [];
					lmBehandlingPoint.data.push({x: Date.UTC(behandlingsDatumSplit[0], (behandlingsDatumSplit[1] - 1), behandlingsDatumSplit[2]), y: lmNumber, tab: '7', datum: behandlingsDatum, startdatum: behandlingsDatum, handelse: handelse, tooltip: true});
					
												
					// om overviewchart finns, lägg till behandlingspunkten
					if(vm.overviewHighChart){
						vm.overviewHighChart.addSeries(lmBehandlingPoint, false);
					}
				
					// hämta dagens datum
					var d = new Date();
					var dagensDatum = d.getFullYear() + "-" + ((''+(d.getMonth()+1)).length < 2 ? '0' : '') + (d.getMonth() + 1) + "-" + ((''+(d.getDate())).length < 2 ? '0' : '') + d.getDate();
					
					if(dagensDatum){
						var dagensDatumSplit = dagensDatum.split("-");
						
						// skapa behandlings serie
						var lmBehandlingSerie = {
							name: lmName,
							color: lmColor,
							type: 'line',
							zIndex: 1,
							lineWidth: 7,
							yAxis: 4,
							showInLegend: false,
							marker: { enabled: false },
							states: { hover: { enabled: false } },
							enableMouseTracking: true
						}
						
						// skapa dataobjekt och lägg till behandlingsserien.
						lmBehandlingSerie.data = [];
						lmBehandlingSerie.data.push({x: Date.UTC(behandlingsDatumSplit[0], (behandlingsDatumSplit[1] - 1), (behandlingsDatumSplit[2])), y: lmNumber, tab: '7', startdatum: behandlingsDatum, tooltip: false}, {x: Date.UTC(dagensDatumSplit[0], (dagensDatumSplit[1] - 1), (dagensDatumSplit[2])), y: lmNumber, tab: '7', startdatum: behandlingsDatum, tooltip: false});
						
						// om overviewchart finns, lägg till behandlingsserie objektet.
						if(vm.overviewHighChart){
							vm.overviewHighChart.addSeries(lmBehandlingSerie, false);
						}
						
						// skapa slut punkt
						var lmPoint = {
							name: lmName,
							yAxis: 4,
							showInLegend: false,
							zIndex: 0,
							color: lmColor,
							type: 'scatter',
							marker: {
								symbol: 'diamond',
								radius: 6
							}	
						}

						// skapa dataobjekt och lägg till slutpunkten
						lmPoint.data = [];
						lmPoint.data.push({x: Date.UTC(dagensDatumSplit[0], (dagensDatumSplit[1] - 1), dagensDatumSplit[2]), y: lmNumber, tab: '7', datum: behandlingsDatum, startdatum: behandlingsDatum, tooltip: false});
						
						// om overviewchart finns, lägg till slutpunkten
						if(vm.overviewHighChart){
							vm.overviewHighChart.addSeries(lmPoint, false);
						}
					}
				}
			}
			// om insättningar finns
			else if(lm.$$.LM_Insättning()) {
				// loopa igenom insättningar
				ko.utils.arrayForEach(lm.$$.LM_Insättning(), function (lmIns) {
					var pDatum;
					var pHandelse;
					var startdatum;
					
					// finns händelser för insättningen
					if(lmIns.$$.LMI_Händelse()) {
						// loopa igenom händelser
						ko.utils.arrayForEach(lmIns.$$.LMI_Händelse(), function (lmHandelse) {
							var datum = lmHandelse.LMIH_handelsedatum();
							// har händelsen ett datum
							if(datum) {
								lmColor = '#000000';
								startdatum = datum;
								var datumsplit = datum.split("-");
								var handelse;
								var orsak;
								
								// har händelseobjektet en händelseaternativ angivet
								if(lmHandelse.LMIH_handelse())
									handelse = lmHandelse.LMIH_handelse().text;
								// har händelseobjektet en händelseorsak angiven
								if(lmHandelse.LMIH_handelseorsak())
									if(lmHandelse.LMIH_handelseorsak().value == '4')
										orsak = lmHandelse.LMIH_handelseorsakann();
									else
										orsak = lmHandelse.LMIH_handelseorsak().text;

								// markerar händelsen ett slut, skapa och lägg till en händelseserie av händelsen utifrån den tidiagre händelsen 
								if(pDatum) {
									var pDatumsplit = pDatum.split("-");
									
									// skapa serie av händelsen
									var lmHandelseSerie = {
							        	name: lmName,
										color: lmColor,
										type: 'line',
										zIndex: 1,
										yAxis: 4,
										lineWidth: 11, // nytt
										showInLegend: false,
										marker: { enabled: false },
										states: { hover: { enabled: false } },
										enableMouseTracking: true
							        }
									
									// markerar händelsen slutet av en paus, byt färg på händelsen.
									if(pHandelse == 'Paus')
										lmHandelseSerie.color = increaseLMBrightness(lmColor, 75);
									
									// skapa dataobjekt och lägg till händelsen till serien.
									lmHandelseSerie.data = [];
									lmHandelseSerie.data.push({x: Date.UTC(pDatumsplit[0], (pDatumsplit[1] - 1), pDatumsplit[2]), y: lmNumber, tab: '7', startdatum: startdatum, tooltip: false}, {x: Date.UTC(datumsplit[0], (datumsplit[1] - 1), datumsplit[2]), y: lmNumber, tab: '7', startdatum: startdatum, tooltip: false});
									
									
									// om overviewchart finns, lägg till händelseserien.
									if(vm.overviewHighChart) {
										vm.overviewHighChart.addSeries(lmHandelseSerie, false);
									}
									
								}
								
								if(lmIns.$$.LMI_Utsättning() && lmIns.$$.LMI_Utsättning().length > 0) {
									lmColor = '#666666';
								}
								
								// skapa punkt av händelsen
								var lmInsattningPoint = {
									name: lmName,
									yAxis: 4,
									showInLegend: false,
									zIndex: 2,
									color: lmColor,
									type: 'scatter',
									marker: {radius: 6}
								}
								
								// markerar händelsen början av en paus, byt färg och justera storleken på punkten.
								if(handelse == 'Paus') {
									lmInsattningPoint.color = increaseLMBrightness(lmColor, 75);
									lmColor = increaseLMBrightness(lmColor, 75);
									lmInsattningPoint.marker.radius = 4;
								} else {
									lmInsattningPoint.color = lmColor
									lmInsattningPoint.marker.radius = 6;
								}

								// skapa dataobjekt och lägg till händelsen till punkten
								lmInsattningPoint.data = [];
								lmInsattningPoint.data.push({x: Date.UTC(datumsplit[0], (datumsplit[1] - 1), datumsplit[2]), y: lmNumber, tab: '7', datum: datum, startdatum: startdatum, handelse: handelse, orsak: orsak, tooltip: true});
								
															
								// om overviewchart finns, lägg till händelsepunkten
								if(vm.overviewHighChart) {
									vm.overviewHighChart.addSeries(lmInsattningPoint, false);
								}
								
								// spara händelseuppgifter till nästkommande händelse
								pHandelse = handelse;
								pDatum = datum;
							}
						});
					}
					
					// finns utsättning för insättningen
					if(lmIns.$$.LMI_Utsättning() && lmIns.$$.LMI_Utsättning().length > 0) {
						// loopa igenom utsättningar
						ko.utils.arrayForEach(lmIns.$$.LMI_Utsättning(), function (lmUtsattning) {
							var datum = lmUtsattning.LMIU_utsattningsdatum();
							// har utsättningen ett datum
							if(datum) {
								var datumsplit = datum.split("-");
								var handelse = 'Utsättning';
								var orsak;
								
								if(lmUtsattning.LMIU_utsattningsorsak())
									if(lmUtsattning.LMIU_utsattningsorsak().value == '4')
										orsak = lmUtsattning.LMIU_utsattningsorsakann();
									else
										orsak = lmUtsattning.LMIU_utsattningsorsak().text;
									
								
								// markerar utsättningen ett slut, skapa och lägg till en utsättningsserie av utsättningen utifrån den tidiagre händelsen 
								if(pDatum) {
									lmColor = '#666666';
									var pDatumsplit = pDatum.split("-");
		
									var lmUtsattningSerie = {
							        	name: lmName,
										color: lmColor,
										type: 'line',
										zIndex: 1,
										yAxis: 4,
										lineWidth: 11, // nytt
										showInLegend: false,
										marker: { enabled: false },
										states: { hover: { enabled: false } },
										enableMouseTracking: true
							        }
									
									// markerar händelsen slutet av en paus, byt färg på händelsen.
									if(pHandelse == 'Paus') {
										lmUtsattningSerie.color = increaseLMBrightness(lmColor, 75);
										lmColor = increaseLMBrightness(lmColor, 75);
									}
									
									// skapa dataobjekt och lägg till utsättningen till serien.
									lmUtsattningSerie.data = [];
									lmUtsattningSerie.data.push({x: Date.UTC(pDatumsplit[0], (pDatumsplit[1] - 1), (pDatumsplit[2])), y: lmNumber, tab: '7', startdatum: startdatum, tooltip: false}, {x: Date.UTC(datumsplit[0], (datumsplit[1] - 1), (datumsplit[2])), y: lmNumber, tab: '7', startdatum: startdatum, tooltip: false});
									
									// om overviewchart finns, lägg till utsättningsserien.
									if(vm.overviewHighChart){
										vm.overviewHighChart.addSeries(lmUtsattningSerie, false);
									}
								}
								
								// skapa punkt av utsättningen
								var lmUtsattningPoint = {
									name: lmName,
									yAxis: 4,
									showInLegend: false,
									zIndex: 2,
									color: lmColor,
									type: 'scatter'
								}
								
								// skapa dataobjekt och lägg till utsättningen till punkten
								lmUtsattningPoint.data = [];
								lmUtsattningPoint.data.push({x: Date.UTC(datumsplit[0], (datumsplit[1] - 1), datumsplit[2]), y: lmNumber, tab: '7', datum: datum, startdatum: startdatum, handelse: handelse, orsak: orsak, tooltip: true});	
								
								// om overviewchart finns, lägg till utsättningspunkten
								if(vm.overviewHighChart){
									vm.overviewHighChart.addSeries(lmUtsattningPoint, false);
								}
								
								// spara utsättningsuppgifter till nästkommande händelse
								pDatum = datum;
							}
						});
					}
					// finns ingen utsättning för insättningen, låt den löpa till dagens datum
					else {
						// hämta dagens datum
						var d = new Date();
						var datum = d.getFullYear() + "-" + ((''+(d.getMonth()+1)).length < 2 ? '0' : '') + (d.getMonth() + 1) + "-" + ((''+(d.getDate())).length < 2 ? '0' : '') + d.getDate();
						var datumsplit = datum.split("-");
						
						// markerar utsättningen ett slut, skapa och lägg till en utsättningsserie av utsättningen utifrån den tidiagre händelsen 
						if(pDatum) {
							lmColor = '#000000';
							var pDatumsplit = pDatum.split("-");
							var handelse = 'Ej utsatt';
							
							var lmUtsattningSerie = {
								name: lmName,
								color: lmColor,
								type: 'line',
								zIndex: 1,
								yAxis: 4,
								lineWidth: 11, // nytt
								showInLegend: false,
								marker: { enabled: false },
								states: { hover: { enabled: false } },
								enableMouseTracking: true
							}
							
							// markerar händelsen slutet av en paus, byt färg på händelsen.
							if(pHandelse == 'Paus')
								lmUtsattningSerie.color = increaseLMBrightness(lmColor, 50);

							// skapa dataobjekt och lägg till utsättningen till serien.
							lmUtsattningSerie.data = [];
							lmUtsattningSerie.data.push({x: Date.UTC(pDatumsplit[0], (pDatumsplit[1] - 1), (pDatumsplit[2])), y: lmNumber, tab: '7', startdatum: startdatum, tooltip: false}, {x: Date.UTC(datumsplit[0], (datumsplit[1] - 1), (datumsplit[2])), y: lmNumber, tab: '7', startdatum: startdatum, tooltip: false});
							
							// om overviewchart finns, lägg till utsättningsserien.
							if(vm.overviewHighChart){
								vm.overviewHighChart.addSeries(lmUtsattningSerie, false);
							}
						}
						
						// skapa punkt för pågående
						var lmUtsattningPoint = {
							name: lmName,
							yAxis: 4,
							showInLegend: false,
							zIndex: 2,
							color: lmColor,
							type: 'scatter',
							marker: {
								symbol: 'diamond',
								radius: 6,
								lineWidth: 0
							}
						}

						// skapa dataobjekt och lägg till utsättningen till punkten
						lmUtsattningPoint.data = [];
						lmUtsattningPoint.data.push({x: Date.UTC(datumsplit[0], (datumsplit[1] - 1), datumsplit[2]), y: lmNumber, tab: '7', datum: datum, startdatum: startdatum, handelse: handelse, tooltip: true});
						
						// om overviewchart finns, lägg till utsättningspunkten
						if(vm.overviewHighChart){
							vm.overviewHighChart.addSeries(lmUtsattningPoint, false);
						}		
						
					}
				});

			}
			index++;
		}
	});
	
	
	// temp
	if(vm.overviewLMHeight != (vm.$$.Läkemedel().length * vm.LMHeight)){
		vm.overviewLMHeight = vm.$$.Läkemedel().length * vm.LMHeight;
		vm.overviewHighChart.yAxis[4].update({height: vm.overviewLMHeight}, false);
		vm.overviewHighChart.xAxis[0].update({offset: -(vm.overviewPlotHeight + vm.overviewPlotPadding + vm.overviewLMHeight + vm.overviewLMPadding + vm.overviewSBHeight + vm.overviewSBPadding)}, false);
		vm.overviewHighChart.setSize(vm.overviewWidth, vm.overviewLabHeight + vm.overviewLabPadding + vm.overviewSBHeight + vm.overviewSBPadding + vm.overviewPlotHeight + vm.overviewPlotPadding + vm.overviewLMHeight + vm.overviewLMPadding, false);
	}
};




/* ÖVRIGA FUNKTIONER */

/**
* Funktion för justera färgsättning på en läkemedelslinje
*
* @method increaseLMBrightness
*/
increaseLMBrightness = function(hex, percent){
    // strip the leading # if it's there
    hex = hex.replace(/^\s*#|\s*$/g, '');

    // convert 3 char codes --> 6, e.g. `E0F` --> `EE00FF`
    if(hex.length == 3){
        hex = hex.replace(/(.)/g, '$1$1');
    }

    var r = parseInt(hex.substr(0, 2), 16),
        g = parseInt(hex.substr(2, 2), 16),
        b = parseInt(hex.substr(4, 2), 16);

    return '#' +
       ((0|(1<<8) + r + (256 - r) * percent / 100).toString(16)).substr(1) +
       ((0|(1<<8) + g + (256 - g) * percent / 100).toString(16)).substr(1) +
       ((0|(1<<8) + b + (256 - b) * percent / 100).toString(16)).substr(1);
}

var hsv2rgb = function(h, s, v) {
  // adapted from http://schinckel.net/2012/01/10/hsv-to-rgb-in-javascript/
  var rgb, i, data = [];
  if (s === 0) {
    rgb = [v,v,v];
  } else {
    h = h / 60;
    i = Math.floor(h);
    data = [v*(1-s), v*(1-s*(h-i)), v*(1-s*(1-(h-i)))];
    switch(i) {
      case 0:
        rgb = [v, data[2], data[0]];
        break;
      case 1:
        rgb = [data[1], v, data[0]];
        break;
      case 2:
        rgb = [data[0], v, data[2]];
        break;
      case 3:
        rgb = [data[0], data[1], v];
        break;
      case 4:
        rgb = [data[2], data[0], v];
        break;
      default:
        rgb = [v, data[0], data[1]];
        break;
    }
  }
  return '#' + rgb.map(function(x){
    return ("0" + Math.round(x*239).toString(16)).slice(-2);
  }).join('');
};

})(jQuery);