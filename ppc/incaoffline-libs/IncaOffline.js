(function ( $, window )
{

/**
 * Stöd för att emulera INCA offline.
 *
 * @param [opt={}] Alternativ.
 * @param [opt.record = false] Om sant sparas hämtade värdedomäner/registerposter.
 * @param [opt.emulate] Objektet med data som ska ska emuleras i offline-läge.
 * @param [opt.container='body'] HTML-element som omsluter formuläret, skickas till jQuery.
 *
 * @constructor
 */
IncaOffline =
function ( opt )
{
	var self = this;

	self._recording = { getValueDomainValues: {}, getRegisterRecordData: {} };

	if ( !opt )
		opt = {};

	if ( opt.emulate )
		self.setupEmulation(opt.emulate);	

	if ( opt.record )
		self.setupRecording();
	
	self.container = opt.container || 'body';
};

/**
 * Patcha API-metoderna i globala INCA-objektet för att spela in svaren.
 * @private
 */
IncaOffline.prototype.setupRecording =
function ( )
{
	var self = this;

	$.each( ['getValueDomainValues', 'getRegisterRecordData'],
		function ( unused, methodName )
		{
			var originalMethod = window.inca.form[methodName];

			window.inca.form[methodName] =
				function ( opt )
				{
					var query = IncaOffline.serializeQuery(opt);

					if ( !self._recording[methodName][query] )
						self._recording[methodName][query] = [];

					originalMethod.call( window.inca.form,
						$.extend( {}, opt,
							{
								success:
									function ( data )
									{
										self._recording[methodName][query].push( data );

										if ( opt.success )
											opt.success.apply( this, arguments );
									}
							} ) );
				};
		} );
};

/**
 * Serialisera argumenten till ett API-anrop.
 *
 * @param opt Alternativen som ska serialiseras.
 * 
 * @private
 */
IncaOffline.serializeQuery =
function ( opt )
{
	// { x: 123, a: 453 } --> [ ['a', 453], ['x', 123] ] etc
	var toOrderedRepresentation =
		function ( obj )
		{
			var keys = $.map( obj, function ( val, key ) { return key; } ).sort();
			return $.map( keys,
				function ( key )
				{
					// Recursively order sub-objects.
					if ( typeof obj[key] == 'object' && obj[key] )
						return [[ key, toOrderedRepresentation( obj[key] ) ]];
					// Treat all false-ish values as empty strings.
					else
						return [[ key, obj[key] || '' ]];
				} );
		};

	return JSON.stringify( toOrderedRepresentation(opt) );
};


/**
 * Skriv över det globala INCA-objektet och konfigurera API-funktioner för
 * att retunera de inspelade värdena.
 * 
 * @param data Datan som ska emuleras, från en deserialisering av {@link IncaOffline.prototype.serialize}.
 * 
 * @private
 */
IncaOffline.prototype.setupEmulation =
function ( data )
{
	var self = this;

	if ( window.inca )
		throw new Error( 'window.inca already exists.' );

	var inca = window.inca = data.inca;
	
	$.each( ['getValueDomainValues', 'getRegisterRecordData'],
		function ( unused, methodName )
		{
			inca.form[methodName] =
				function ( opt )
				{
					var query = IncaOffline.serializeQuery(opt);
					var replies = data.recording[methodName][query];

					if ( replies )
					{
						if ( !replies.hasOwnProperty('_count') )
							replies._count = 0;

						var reply = replies[replies._count++ % replies.length];

						if ( opt.success )
							setTimeout( function ( ) { opt.success(reply); }, (1 + Math.random())*250 );
					}
					else if ( opt.error )
						opt.error( 'Unexpected query: ' + query );
					else
						throw new Error( 'Unexpected query: ' + query );
				};
		} );

	$.each( ['on', 'off'],
		function ( unused, methodName )
		{
			inca[methodName] = function ( ) { alert( 'inca.' + methodName + ' not implemented.' ); };
		} );

	inca.form.getContainer = function ( ) { return $(self.container)[0]; };

	// From inca.overview.js
	window.inca.form.createDataRow = function (name) {
		var self = window.inca.form;

		var metadata = self.metadata[name], i, prop, model = { regvars: {}, vdlists: {}, subTables: {} };

		for (prop in metadata.regvars) {
			if (metadata.regvars.hasOwnProperty(prop)) {
				model.regvars[prop] = {
					value: null,
					include: true
				};
			}
		}

		for (prop in metadata.vdlists) {
			if (metadata.vdlists.hasOwnProperty(prop)) {
				model.vdlists[prop] = {
					value: null,
					include: true
				};
			}
		}

		for (i = 0; i < metadata.subTables.length; i += 1) {
			model.subTables[metadata.subTables[i]] = [];
		}

		return model;
	};
};


/**
 * Serialisera INCA-objektet och inspelade API-anrop.
 *
 * @param {Boolean} [prettyPrint=true] Om sant indenteras datan.
 * @returns {String} Serialiserad data.
 */
IncaOffline.prototype.serialize =
function ( prettyPrint )
{
	var self = this;
	if ( typeof prettyPrint == 'undefined' )
		prettyPrint = true;

	return JSON.stringify( { inca: window.inca, recording: self._recording }, null, prettyPrint ? 4 : 0 );
};

/**
 * Bekvämlighetsfunktion som serialiserar INCA-objektet och
 * visar resultatet i en jQuery UI dialog.
 * 
 * @param {Boolean} [prettyPrint] Se {@link IncaOffline.prototype.serialize}.
 */
IncaOffline.prototype.serializeToDialog =
function ( prettyPrint )
{
	var self = this;

	$('<div></div>').append( $('<textarea></textarea>').text( self.serialize(prettyPrint) ) ).dialog();
};

window['IncaOffline'] = IncaOffline;

})( window.jQuery, window );

