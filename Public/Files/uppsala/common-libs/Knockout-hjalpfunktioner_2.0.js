(function ($) {

  ko.bindingHandlers[RCC.bindingsPrefix + 'multiselect'] = {
    init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
      var cssClasses = {
        includedByMonitor: RCC.bindingsPrefix + 'included',
        includedIcon: RCC.bindingsPrefix + 'included-icon',
        validationFailed: RCC.bindingsPrefix + 'invalid',
        glyphiconCheck: 'glyphicon-check',
        glyphiconUnchecked: 'glyphicon-unchecked'
      };
      var availableValues = [];
      var selectedValues = [];

      ko.utils.arrayForEach(valueAccessor().variables, function (checkbox) {
        availableValues.push(checkbox.rcc.regvarName);
        if (checkbox()) {
          selectedValues.push(checkbox.rcc.regvarName);
        }
      });

      var bindings = {
        options: availableValues,
        optionsText: function (item) {
          return vm[item].rcc.regvar.description;
        },
        selectedOptions: selectedValues,
        multiselect: {
          includeSelectAllOption: valueAccessor().selectall,
          selectAllText: 'Välj alla',
          nonSelectedText: '– Flervalslista –',
          numberDisplayed: 6,
          allSelectedText: 'Alla valda',
          onDropdownHidden: function () {
            ko.utils.arrayForEach(valueAccessor().variables, function (variable) {
              variable.rcc.accessed(true);
            });
          },
          onChange: function (option, checked) {
            vm[option[0].value](checked);
          },
          onSelectAll: function () {
            ko.utils.arrayForEach(availableValues, function (variable) {
              vm[variable](true);
            });
          },
          onDeselectAll: function () {
            ko.utils.arrayForEach(availableValues, function (variable) {
              vm[variable](false);
            });
          }
        }
      };

      var $el = $(element);
      var $icon = $('<span class="rcc-include-icon rcc-icon paddl rcc-large glyphicon glyphicon-check"></span>');
      var $includeEl = $('<div class="include-icon-block hidden"></div>').append($icon);

      var $anchorElement = ($el.closest('.relative').siblings('.input-group-addon').length == 0 ? ($el.closest('.relative').attr('multiple')) != undefined ? $el : $el.closest('.relative') : $el.parent());
      $anchorElement.after($includeEl);

      $includeEl.click(function () {
        ko.utils.arrayForEach(valueAccessor().variables, function (variable) {
          vm[variable.rcc.regvarName][RCC.metadataProperty].include(!vm[variable.rcc.regvarName][RCC.metadataProperty].include());
        });
      });

      var v = valueAccessor().variables[0];
      ko.computed(function () {
        if (v[RCC.metadataProperty].showInclude()) {
          $includeEl.removeClass('hidden');

          // Set element class based on inclusion.
          ko.utils.toggleDomNodeCssClass($icon[0], cssClasses.includedIcon, v[RCC.metadataProperty].include());
          ko.utils.toggleDomNodeCssClass($icon[0], cssClasses.glyphiconCheck, v[RCC.metadataProperty].include());
          ko.utils.toggleDomNodeCssClass($icon[0], cssClasses.glyphiconUnchecked, !v[RCC.metadataProperty].include());
        } else {
          $includeEl.addClass('hidden');
        }
      });

      var validation = {};
      var validator = valueAccessor().validation;
      if (validator && validator.hasOwnProperty('max')) {
        validation.max = validator.max;
      }
      if (validator && validator.hasOwnProperty('min')) {
        validation.min = validator.min;
      }
      v.rcc.validation.add(new RCC.Validation.Tests.Multiselect(v, {
        dependables: valueAccessor().variables,
        validation: validation
      }));

      ko.computed(function() {
        var accessed = ko.utils.arrayFirst(valueAccessor().variables, function (variable) {
          if (variable[RCC.metadataProperty].accessed()) {
            return variable;
          }
        });

        if (!accessed) {
          ko.utils.toggleDomNodeCssClass(element, cssClasses.validationFailed, false);
          return;
        }

        var el = $(element).siblings('.btn-group').children('button')[0];
        if (el && v[RCC.metadataProperty].validation) {
          var errors = v[RCC.metadataProperty].validation.errors();
          if (errors.length > 0) {
            var escapeHtml = function (str) {
              return $('<div>').text(str).html();
            };

            var validationMessage = $.map(errors, function (err) {
              return escapeHtml(err.message);
            });

            if (!$('#error-row-' + $el.attr('id'))[0]) {
              var $errorEl = $('<div id="error-row-' + $el.attr('id') + '"class="row error-row"><div class="col-xs-3">&nbsp;</div><div class="input-group errors"><span class="rcc-error rcc-larger paddl glyphicon glyphicon-exclamation-sign"></span>&nbsp;<span id="errors-' + $el.attr('id') + '">' + validationMessage.join('') + '</span></div></div>');
              $el.closest('.form-group').after($errorEl);
            }

          } else {
            var $errorEl = $('#error-row-' + $el.attr('id'));
            $errorEl.remove();
          }
          ko.utils.toggleDomNodeCssClass(el, cssClasses.validationFailed, errors.length > 0);
        }
      });

      var currentBindings = allBindingsAccessor();
      for (var binding in bindings)
        if (bindings.hasOwnProperty(binding) && currentBindings.hasOwnProperty(binding))
          bindings[binding] = currentBindings[binding];

      ko.applyBindingsToNode(element, bindings, bindingContext);
    }
  };

  ko.bindingHandlers.datetimepicker = {
    init: function (element, valueAccessor, allBindings) {
      var options = {
        format: 'YYYY-MM-DD',
        extraFormats: ['YY-MM-DD'],
        locale: 'sv',
        useCurrent: false,
        keepInvalid: true,
        defaultDate: ko.utils.unwrapObservable(valueAccessor())
      };
      var val = valueAccessor().variable ? valueAccessor().variable : valueAccessor();
      var validation = valueAccessor().validation ? valueAccessor().validation : null;

      // Lägg till valideringar för
      if (validation && validation.NotFutureDate && validation.NotFutureDate == true) {
        // Not future date
        val.rcc.validation.add(new RCC.Validation.Tests.NotFutureDate(val, {
          dependables: [val],
          ignoreMissingValues: true
        }));
      }
      if (validation && validation.NotBeforeBirthDate && validation.NotBeforeBirthDate == true) {
        // Not before birth date
        val.rcc.validation.add(new RCC.Validation.Tests.NotBeforeBirthDate(val, {
          dependables: [val],
          birthDate: vm.$env._PERSNR.slice(0, 8),
          ignoreMissingValues: true
        }));
      }
      if (validation && validation.NotAfterDeathDate && validation.NotAfterDeathDate == true) {
        // Not after death date
        val.rcc.validation.add(new RCC.Validation.Tests.NotAfterDeathDate(val, {
          dependables: [val],
          birthDate: vm.$env._DATEOFDEATH,
          ignoreMissingValues: true
        }));
      }

      $(element).datetimepicker(options);

      ko.utils.registerEventHandler(element, "dp.change", function (event) {
        var value = valueAccessor().variable ? valueAccessor().variable : valueAccessor();
        if (event.timeStamp !== undefined) {
          if (event.date !== null && event.date !== false && event.date !== undefined) {
            event.date._isValid ? value(event.date.format(options.format)) : value(event.date._i);
          } else {
            value(undefined);
          }
        }
      });

      ko.utils.domNodeDisposal.addDisposeCallback(element, function () {
        var picker = $(element).data("DateTimePicker");
        if (picker) {
          picker.destroy();
        }
      });
    },
    update: function (element, valueAccessor) {
      var picker = $(element).data("DateTimePicker");
      if (picker) {
        var date = ko.utils.unwrapObservable(valueAccessor().variable ? valueAccessor().variable : valueAccessor());
        if (!date) {
          date = null;
        }
        picker.date(date);
      }
    }
  };

  /**
   * Bindning för datum
   * lägger till datepicker till inputelement
   *
   * @binding rcc-date
   */
  ko.bindingHandlers[RCC.bindingsPrefix + 'date'] = {
    init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
      ko.bindingHandlers['rcc-value'].init(element, valueAccessor, allBindingsAccessor, viewModel, bindingContext);
    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
      var value = valueAccessor();
      if (value() == "") {
        value(undefined);
      }
      if (value() /* && value.rcc.validation.errors().length == 0*/) {
        // parse date to yyyy-mm-dd
        var parsedDate = RCC.Utils.parseYMD(value());
        if (parsedDate) {
          value(RCC.Utils.ISO8601(parsedDate));
        }
      }
    }
  };

  /**
   *  Bindning för år
   *
   */
  ko.bindingHandlers[RCC.bindingsPrefix + 'year'] = {
    init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
      ko.bindingHandlers['rcc-value'].init(element, valueAccessor, allBindingsAccessor, viewModel, bindingContext);
    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
      var value = valueAccessor();
      if (value() == "") {
        value(undefined);
      } else if (value()) {
        // parse date to yyyy-mm-dd
        var parsedYear = RCC.Utils.parseYear(value());
        if (parsedYear) {
          value(parsedYear);
        }
      }
    }
  };


  /**
   * Bindning för chosen
   *
   * @binding rcc-chosen
   */
  ko.bindingHandlers[RCC.bindingsPrefix + 'chosen'] = {
    init: function (element, valueAccessor) {
      $(element).focus(function (evt, params) {
        var $el = $(this);

        if (!($el.siblings().hasClass('chosen-container'))) {
          $el.chosen({
            'search_contains': true
          });
        }

        var $chosen = $el.siblings('.chosen-container');
        if ($chosen.hasClass('no-show')) {
          $el.css({
            'display': 'none'
          });
          $chosen.removeClass('no-show');
        }

        //$el.addClass('no-show');
        //$chosen.css({'display':'inline-block'});
        $el.trigger('chosen:open');
      });

      $(element).on('chosen:hiding_dropdown', function (evt, params) {
        var $el = $(this);

        $el.chosen("destroy");

        //$el.siblings('.chosen-container').addClass('no-show');
        //$el.removeClass('no-show');
        $el.css({
          'display': 'inline-table'
        });
      });
    }
  };


  /**
   * Bindning för help
   * lägger till help till ett element
   *
   * @binding rcc-help
   */
  ko.bindingHandlers[RCC.bindingsPrefix + 'helpMe'] = {
    init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
      var valAcc = ko.unwrap(valueAccessor());

      if (!valAcc)
        return;

      var self = $(element);
      var text = valueAccessor().helpText;
      if (vm[text]) {
        text = vm[text];
      }
      var pos = valueAccessor().pos;
      var helper = $('[' + 'for' + '=' + self.attr('id') + ']');
      var title = helper.text();
      if (!title || title == '') {
        title = helper.siblings().text();
      }

      self.addClass('has-popover');
      self.attr('data-html', true);
      self.attr('title', title);
      self.attr('data-placement', pos);
      self.attr('data-content', text);
      self.attr('data-trigger', 'manual');
      self.attr('data-toggle', 'popover');
    }
  };


  ko.bindingHandlers[RCC.bindingsPrefix + 'helper'] = {
    init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
      var self = $(element);

      var helpId = self.attr('for');
      var $helpIcon = $('<span> &nbsp; <span helpId="' + helpId + '" class="rcc-help glyphicon glyphicon-info-sign"></span></span>');

      self.after($helpIcon);

      function toggleHelp(event) {
        var helpIcon = self.siblings().children();
        var id = helpIcon.attr('helpId');
        if (id.match("^#")) {
          id = id.substring(1, id.length);
        }
        var helpWanted = $('#' + id);
        helpWanted.popover('toggle');
      };

      $helpIcon.click({
        attr: valueAccessor()
      }, toggleHelp);

    }
  };

  ko.bindingHandlers[RCC.bindingsPrefix + 'warning'] = {
    init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
      var self = $(element);
      var valAcc = ko.unwrap(valueAccessor());

      if (!valAcc)
        return;

      var variable = valueAccessor().variable ? valueAccessor().variable : undefined;
      if (!variable)
        return;

      var warningtext = "";
      if (valAcc.warningtext) {
        warningtext = valAcc.warningtext;
      } else {
        warningtext = "Ojdå, något saknas i scriptet. Det var nog tänkt att det skulle finnas en varningstext här...";
      }
      
      variable.rcc.validation.add(new RCC.Validation.Tests.Warning(variable, { condition: valueAccessor().condition, warningText: warningtext }));

      var warningEl = $('<div id="warning-row-' + self.attr('id') + '"class="row hidden warning-row"><div class="col-xs-3">&nbsp;</div><div class="input-group warnings"><span class="rcc-warning paddl rcc-larger rcc-valmid glyphicon glyphicon-alert"></span>&nbsp;<span id="warnings-' + self.attr('id') + '">' + warningtext + '</span></div></div>');
      self.closest('.form-group').after(warningEl);
    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
      var self = $(element);
      var valAcc = ko.unwrap(valueAccessor());
      if (!valAcc)
        return;

      var variable = valueAccessor().variable ? valueAccessor().variable : undefined;
      if (!variable)
        return;

      if (valueAccessor().condition) {
        var showWarning = valueAccessor().condition();
        var $warningRow = self.closest('.form-group').siblings('#warning-row-' + self.attr('id'));
        if (showWarning != undefined && showWarning == true) {
          $warningRow.removeClass('hidden');
          self.addClass('rcc-warningB')
        } else {
          $warningRow.addClass('hidden');
          self.removeClass('rcc-warningB')
        }
      }
    }
  };

  /**
   *  Ta bort? jQuery UI beroende...
   */

  /**
   * Bindning för att ge en knapp jqury stil
   *
   * @binding rcc-button
   */
  ko.bindingHandlers[RCC.bindingsPrefix + 'button'] = {
    init: function (element, valueAccessor, allBindingsAccessor) {
      var value = valueAccessor();
      if (value == 'add')
        $(element).button({
          icons: {
            primary: "ui-icon-circle-plus"
          }
        });
      else if (value == 'delete')
        $(element).button({
          icons: {
            primary: "ui-icon-circle-minus"
          }
        });
      else if (value == 'generic')
        $(element).button();
    }
  };

  ko.bindingHandlers[RCC.bindingsPrefix + 'compare'] = {
    init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
      var valueBinding;

      if (allBindingsAccessor()['rcc-value'])
        valueBinding = allBindingsAccessor()['rcc-value'];
      else if (allBindingsAccessor()['rcc-var'])
        valueBinding = allBindingsAccessor()['rcc-var'];
      else if (allBindingsAccessor()['rcc-list'])
        valueBinding = allBindingsAccessor()['rcc-list'];
      else if (allBindingsAccessor()['rcc-date'])
        valueBinding = allBindingsAccessor()['rcc-date'];
      else if (allBindingsAccessor()['rcc-checked'])
        valueBinding = allBindingsAccessor()['rcc-checked'];
      else if (allBindingsAccessor()['rcc-decimal'])
        valueBinding = allBindingsAccessor()['rcc-decimal'];
      else if (allBindingsAccessor()['rcc-autocomplete'])
        valueBinding = allBindingsAccessor()['rcc-autocomplete'];

      $(element).after("<span class='rcc-diff rcc-diff-hidden'>* </span>");

      ko.computed(function () {
        if (valueBinding && valueBinding.rcc.showInclude()) {

          if (vm.RegisterDataExist()) {
            var text;
            if (!(valueAccessor() in vm.getSelectedNPCR(vm.$vd.prostataregister_vd())))
              text = 'Variabel saknas';
            else if (!vm.getSelectedNPCR(vm.$vd.prostataregister_vd())[valueAccessor()])
              text = 'Värde saknas';
            else if (allBindingsAccessor()['rcc-checked'])
              if (vm.getSelectedNPCR(vm.$vd.prostataregister_vd())[valueAccessor()])
                text = 'true';
              else
                text = 'false';
            else
              text = vm.getSelectedNPCR(vm.$vd.prostataregister_vd())[valueAccessor()];

            $(element).removeData("qtip").qtip({
              content: {
                text: text,
                title: {
                  text: 'Registervärde'
                }
              },
              position: {
                my: 'top center',
                at: 'bottom center'
              }
            });

            var currentValue;
            if (allBindingsAccessor()['rcc-list'] && valueBinding()) {
              currentValue = valueBinding().text;
            } else if (allBindingsAccessor()['rcc-checked'] && !valueBinding()) {
              currentValue = null;
            } else if (allBindingsAccessor()['rcc-decimal']) {
              currentValue = parseFloat(valueBinding());
            } else {
              currentValue = valueBinding();
            }
            var registerValue = vm.getSelectedNPCR(vm.$vd.prostataregister_vd())[valueAccessor()];

            if (currentValue == registerValue)
              $(element).next().addClass("rcc-diff-hidden");
            else
              $(element).next().removeClass("rcc-diff-hidden");
          }
        } else {
          $(element).qtip('destroy');
          $(element).next().addClass("rcc-diff-hidden");
        }
      });
    }
  };


  /**
   *  Ta bort? Används inte längre...
   */

  /**
   * Bindning för att ge ett fieldset möjligt att dynamiskt öppnas och stängas
   * true: collapsable, stängd default
   * false: collapsable, öppet default
   *
   * @binding collapse
   */
  ko.bindingHandlers.collapse = {
    init: function (element, valueAccessor, allBindingsAccessor) {
      var value = valueAccessor();
      $(element).addClass("collapsefieldset");
      $(element).collapsefieldset({
        collapsed: value
      });
    }
  };


  /**
   *  Ta bort?
   */

  /**
   * Bindning för autocompletet
   *
   * @binding rcc-autocomplete
   */
  ko.bindingHandlers[RCC.bindingsPrefix + 'autocomplete'] = {
    init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
      ko.bindingHandlers['rcc-value'].init(element, valueAccessor, allBindingsAccessor, viewModel, bindingContext);

      var value = valueAccessor();
      var objects = allBindingsAccessor()['autocomplete-objects'];

      $(element).autocomplete({
        minLength: 0,
        source: objects,
        change: function (event, ui) {
          if (!ui.item) {
            $(event.target).val("");
            value(undefined);
          }
        },
        focus: function (event, ui) {
          return false;
        },
        select: function (event, ui) {
          $(element).val(ui.item.label + "  (" + ui.item.value + ")");
          value(ui.item.label + "  (" + ui.item.value + ")");
          return false;
        }
      });
    }
  };


  /**
   * Bindning för decimaltal
   *
   * @binding rcc-decimal
   */
  ko.bindingHandlers[RCC.bindingsPrefix + 'decimal'] = {
    init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
      ko.bindingHandlers['rcc-value'].init(element, valueAccessor, allBindingsAccessor, viewModel, bindingContext);

      var newValue = valueAccessor();
      var precision = ko.utils.unwrapObservable(allBindingsAccessor().precision) || ko.bindingHandlers[RCC.bindingsPrefix + 'decimal'].defaultPrecision;
      var formattedValue;

      if (newValue()) {
        if (typeof newValue() == 'string')
          formattedValue = parseFloat(newValue().replace(',', '.')).toFixed(precision);
        else
          formattedValue = newValue().toFixed(precision);
      }
      $(element).val(formattedValue);

      //handle the field changing
      ko.utils.registerEventHandler(element, "change", function () {
        var observable = valueAccessor();
        observable($(element).val());
      });


    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
      var newValue = valueAccessor();
      var precision = ko.utils.unwrapObservable(allBindingsAccessor().precision) || ko.bindingHandlers[RCC.bindingsPrefix + 'decimal'].defaultPrecision;
      var formattedValue;

      if (newValue()) {
        if (typeof newValue() == 'string')
          formattedValue = parseFloat(newValue().replace(',', '.')).toFixed(precision);
        else
          formattedValue = newValue().toFixed(precision);
      }

      $(element).val(formattedValue);
    },
    defaultPrecision: 1
  };

  RCC.Validation.Tests.Warning = function(variable, opt) {
    if(!opt) 
      opt = {};

    return function() {
      if(opt.condition()) {
        return {
          result: 'warning',
          message: opt.warningText
        };
      } else {
        return {
          result: 'ok'
        };
      }
    }
  };

  RCC.Validation.Tests.NotFutureDate =
    function (date, opt) {
      if (!opt)
        opt = {};

      return function () {
        var d = date();

        if (opt.ignoreMissingValues && !RCC.Validation.Tests._hasValue(d, true))
          return {
            result: 'ok'
          };

        var parsedDate = RCC.Utils.parseYMD(d);
        var todayDate = RCC.Utils.parseYMD(getDagensDatum());

        if (parsedDate > todayDate)
          return {
            result: 'failed',
            message: 'Datum kan inte inträffa efter dagens datum'
          };
        else
          return {
            result: 'ok'
          };
      };
    };

  RCC.Validation.Tests.NotAfterDeathDate =
    function (date, opt) {
      if (!opt)
        opt = {};

      return function () {
        var d = date();

        if (opt.ignoreMissingValues && !RCC.Validation.Tests._hasValue(d, true))
          return {
            result: 'ok'
          };

        var parsedDate = RCC.Utils.parseYMD(d);
        var deathDate = RCC.Utils.parseYMD(opt.deathDate);

        if (parsedDate > deathDate)
          return {
            result: 'failed',
            message: 'Datum kan inte inträffa efter dödsdatum'
          };
        else
          return {
            result: 'ok'
          };
      };
    };

  RCC.Validation.Tests.NotBeforeBirthDate =
    function (date, opt) {
      if (!opt)
        opt = {};

      return function () {
        var d = date();

        if (opt.ignoreMissingValues && !RCC.Validation.Tests._hasValue(d, true)) {
          return {
            result: 'ok'
          };
        }

        var parsedDate = RCC.Utils.parseYMD(d);
        var birthDate = RCC.Utils.parseYMD(opt.birthDate);

        if (parsedDate < birthDate) {
          return {
            result: 'failed',
            message: 'Datum kan inte inträffa före födelsedatum'
          };
        } else {
          return {
            result: 'ok'
          };
        }
      };
    };

  RCC.Validation.Tests.NotValidYear =
    function (year, opt) {
      if (!opt)
        opt = {};

      return function () {
        var d = year();
        if (opt.ignoreMissingValues && !RCC.Validation.Tests._hasValue(d, true))
          return {
            result: 'ok'
          };

        if (!isInteger(d) || d.length > 4)
          return {
            result: 'failed',
            message: d + ' är inte ett godkänt årtal'
          };
        else
          return {
            result: 'ok'
          };
      };
    };

  RCC.Validation.Tests.NotFutureYear =
    function (year, opt) {
      if (!opt)
        opt = {};

      return function () {
        var d = year();
        var today = new Date().getFullYear();

        if (opt.ignoreMissingValues && !RCC.Validation.Tests._hasValue(d, true))
          return {
            result: 'ok'
          };

        if (d > today)
          return {
            result: 'failed',
            message: 'År kan inte vara ett framtida årtal.'
          };
        else
          return {
            result: 'ok'
          };
      };
    };


  RCC.Validation.Tests.CompareDate =
    function (date, opt) {


      if (!opt)
        opt = {};

      return function () {

        var d = date();

        if (opt.ignoreMissingValues && !RCC.Validation.Tests._hasValue(d, true))
          return {
            result: 'ok'
          };

        var parsedDate = RCC.Utils.parseYMD(d);

        if (RCC.Validation.Tests._hasValue(opt.compareDate)) {
          var parsedCompareDate = RCC.Utils.parseYMD(opt.compareDate);

          if (parsedCompareDate > parsedDate)
            return {
              result: 'failed',
              message: 'Datum kan inte inträffa innan ' + opt.compareTitle + ' (' + opt.compareDate + ')'
            };
          else
            return {
              result: 'ok'
            };
        } else
          return {
            result: 'ok'
          };
      };
    };

  RCC.Validation.Tests.NotBeforeDate =
    function (date, opt) {
      if (!opt)
        opt = {};

      return function () {
        var d = date();

        if (opt.ignoreMissingValues && !RCC.Validation.Tests._hasValue(d, true))
          return {
            result: 'ok'
          };

        var parsedDate = RCC.Utils.parseYMD(d);

        if (opt.dependables) {
          for (var i = 0; i < opt.dependables.length; i++) {
            if (RCC.Validation.Tests._hasValue(opt.dependables[i])) {
              var parsedCompareDate = RCC.Utils.parseYMD(opt.dependables[i]());
              if (parsedCompareDate > parsedDate)
                return {
                  result: 'failed', // quick-fix från .regvar.compareDescription till .term.description
                  message: 'Datum kan inte inträffa innan ' + opt.dependables[i].rcc.regvar.compareDescription + ' (' + opt.dependables[i]() + ')'
                };
            }
          }
        }

        return {
          result: 'ok'
        };
      };
    };

  RCC.Validation.Tests.NotAfterDate =
    function (date, opt) {
      if (!opt)
        opt = {};

      return function () {
        var d = date();

        if (opt.ignoreMissingValues && !RCC.Validation.Tests._hasValue(d, true))
          return {
            result: 'ok'
          };

        var parsedDate = RCC.Utils.parseYMD(d);

        if (opt.dependables) {
          for (var i = 0; i < opt.dependables.length; i++) {
            if (RCC.Validation.Tests._hasValue(opt.dependables[i])) {
              var parsedCompareDate = RCC.Utils.parseYMD(opt.dependables[i]());
              if (parsedCompareDate < parsedDate)
                return {
                  result: 'failed',
                  message: 'Datum kan inte inträffa efter ' + opt.dependables[i].rcc.regvar.compareDescription + ' (' + opt.dependables[i]() + ')'
                };
            }
          }
        }

        return {
          result: 'ok'
        };
      };
    };

  RCC.Validation.Tests.AtleastOneYes =
    function (obj, opt) {
      if (!opt)
        opt = {};

      return function () {
        if (opt.ignoreMissingValues && !obj())
          return {
            result: 'ok'
          };

        if (opt.dependables.length > 0) {
          for (var i = 0; i < opt.dependables.length; i++) {
            if (!opt.dependables[i]() || (opt.dependables[i]() && opt.dependables[i]().value != '0')) {
              return {
                result: 'ok'
              };
            }
          }
          obj.rcc.accessed(true);
          for (var i = 0; i < opt.dependables.length; i++) {
            opt.dependables[i].rcc.accessed(true);
          }
          return {
            result: 'failed',
            message: "Minst ett svar skall vara 'Ja'"
          };
        } else
          return {
            result: 'ok'
          };
      };
    };

  RCC.Validation.Tests.AtleastOneNo =
    function (obj, opt) {
      if (!opt)
        opt = {};

      return function () {
        if (opt.ignoreMissingValues && !obj())
          return {
            result: 'ok'
          };

        if (opt.dependables.length > 0) {
          for (var i = 0; i < opt.dependables.length; i++) {
            if (!opt.dependables[i]() || (opt.dependables[i]() && opt.dependables[i]().value != '1')) {
              return {
                result: 'ok'
              };
            }
          }
          obj.rcc.accessed(true);
          for (var i = 0; i < opt.dependables.length; i++) {
            opt.dependables[i].rcc.accessed(true);
          }
          return {
            result: 'failed',
            message: "Minst ett svar skall vara 'Nej'"
          };
        } else
          return {
            result: 'ok'
          };
      };
    };

  RCC.Validation.Tests.AtleastOneChecked =
    function (obj, opt) {
      if (!opt)
        opt = {};

      return function () {
        if ((!opt.condition || (opt.condition() && opt.condition().value == 1)) && opt.dependables.length > 0) {
          for (var i = 0; i < opt.dependables.length; i++) {
            if (opt.dependables[i]()) {
              return {
                result: 'ok'
              };
            }
          }
          return {
            result: 'failed',
            message: "Minst en kryssruta skall vara vald"
          };
        } else
          return {
            result: 'ok'
          };
      };
    };

  RCC.Validation.Tests.Multiselect = function (obj, opt) {
    if (!opt)
      opt = {};

    return function () {
      var num2text = ["noll", "en", "två", "tre", "fyra", "fem", "sex", "sju", "åtta", "nio", "tio"];
      if (opt.dependables && opt.dependables.length > 0) {
        // ?? Finns dependables men ingen eller tom validering - vad göra? Returnera OK?
        if (!opt.validation || opt.validation == undefined || opt.validation == null || $.isEmptyObject(opt.validation)) {
          return {
            result: 'ok'
          };
        } else {
          var selectedCount = 0;
          for (var i = 0; i < opt.dependables.length; i++) {
            if (opt.dependables[i]()) {
              selectedCount++;
            }
          }
          if (opt.validation.min && selectedCount < opt.validation.min) {
            return {
              result: 'failed',
              message: "Minst " + num2text[opt.validation.min] + " kryssruta skall vara vald."
            };
          } else if (opt.validation.max && selectedCount > opt.validation.max) {
            return {
              result: 'failed',
              message: "Max " + num2text[opt.validation.max] + " kryssrutor får vara valda."
            };
          } else {
            return {
              result: 'ok'
            };
          }
        }
      }
    };
  };

  RCC.Validation.Tests.atLeastOneValue =
    function (obj, opt) {
      if (!opt)
        opt = {};

      return function () {
        //if ( opt.ignoreMissingValues && !obj())
        //	return { result: 'ok' };

        if (opt.dependables.length > 0) {
          for (var i = 0; i < opt.dependables.length; i++) {
            // Om en dependable är ifylld, returnera ok
            if (opt.dependables[i]()) {
              return {
                result: 'ok'
              }; // Hittar en som är ifylld med något värde
            }
          }
          // Om ingen variabel är ifylld, returnera failed
          return {
            result: 'failed',
            message: "Minst ett värde skall fyllas i"
          };
        } else //Finns inga beroende variabler
          return {
            result: 'ok'
          };
      };
    };

  RCC.Validation.Tests.NotSameListval =
    function (obj, opt) {
      if (!opt)
        opt = {};

      return function () {

        if (opt.ignoreMissingValues && !obj())
          return {
            result: 'ok'
          };

        var value = obj().value;

        if (opt.dependables.length > 0) {
          for (var i = 0; i < opt.dependables.length; i++) {
            if (!opt.dependables[i]() || (opt.dependables[i]() && opt.dependables[i]().value != value)) {
              return {
                result: 'ok'
              };
            }
          }
          obj.rcc.accessed(true);
          for (var i = 0; i < opt.dependables.length; i++) {
            opt.dependables[i].rcc.accessed(true);
          }
          return {
            result: 'failed',
            message: "Dessa får inte ha samma listvärde!"
          };
        } else
          return {
            result: 'ok'
          };
      };
    };

  RCC.Validation.Tests.DynamicMinMaxInteger =
    function (obj, opt) {
      if (!opt)
        opt = {};

      return function () {
        if (opt.ignoreMissingValues && !obj())
          return {
            result: 'ok'
          };

        if (opt.min) {
          if (parseInt(obj()) < parseInt(opt.min()))
            return {
              result: 'failed',
              message: 'Minsta tillåtna värde: ' + opt.min()
            };
        }
        if (opt.max) {
          if (parseInt(obj()) > parseInt(opt.max()))
            return {
              result: 'failed',
              message: 'Största tillåtna värde: ' + opt.max()
            };
        } else
          return {
            result: 'ok'
          };
      };
    };

  RCC.Validation.Tests.DynamicMinMaxDecimal =
    function (obj, opt) {
      if (!opt)
        opt = {};

      return function () {
        if (opt.ignoreMissingValues && !obj())
          return {
            result: 'ok'
          };

        if (opt.min) {
          if (parseFloat(obj()) < parseFloat(opt.min()))
            return {
              result: 'failed',
              message: 'Minsta tillåtna värde: ' + opt.min()
            };
        }
        if (opt.max) {
          if (parseFloat(obj()) > parseFloat(opt.max()))
            return {
              result: 'failed',
              message: 'Största tillåtna värde: ' + opt.max()
            };
        } else
          return {
            result: 'ok'
          };
      };
    };


  /**
   * Testar om flera objekt har valda värden som är giltiga samtidigt.
   * 		obj = objekt som valideras (Biopsi),
   *		val = objektets giltiga listvärden,
   *		opt = 	dependables: lista av ingående objekt,
   *				values: lista med ingående objekts listor över giltiga värden (de måste ligga i samma ordning som objekten)
   *				ignoreMissingValues: Kör inte valideringen när objektet saknar värde
   *	Ex
   *	Om Biopsi = Ja får inte Diagnos baseras på 'Bilddiagnostik enbart' eller 'Klinisk undersökning'
   *  vm.Biopsi.rcc.validation.add(new RCC.Validation.Tests.ValidListValues(vm.Biopsi, ["1"], {dependables: [vm.Vdigr], values: [["3","5"]], ignoreMissingValues: true}));
   *
   */
  RCC.Validation.Tests.ValidListValues =
    function (obj, val, opt) {
      if (!opt)
        opt = {};

      return function () {
        objVals = val.toString() + ",";

        //Om objektet inte har några värden eller om
        //om objektet inte har rätt värde
        if ((opt.ignoreMissingValues && !obj()) || (obj() && objVals.search(obj().value + ",") == -1))
          return {
            result: 'ok'
          };

        var foundVal = false;
        var okVal = new Array();

        if (opt.dependables.length > 0 && opt.values.length > 0) {
          for (var i = 0; i < opt.dependables.length; i++) {
            foundVal = false;
            if (!opt.dependables[i]()) {
              return {
                result: 'ok'
              };
            }
            if (opt.values[i] && opt.values[i].length > 0 && RCC.Validation.Tests._hasValue(opt.dependables[i])) {
              var validVals = opt.values[i].toString() + ",";
              foundVal = (validVals.search(opt.dependables[i]().value + ",") != -1);
            }
            okVal.push(foundVal);
            foundVal = false;
          }

          //alert(okVal.toString());
          //Om något av objekten med dess värden inte är godkända
          if (okVal.toString().search("false") != -1) {
            messStr = "När " + obj.rcc.regvar.compareDescription + " = " + obj().text + ", måste någon av följande vara: ";
            obj.rcc.accessed(true);

            for (var i = 0; i < opt.dependables.length; i++) {

              var antal = 0;

              for (var j = 0; j < opt.values[i].length; j++) {
                for (var k = 0; k < opt.dependables[i].rcc.term.listValues.length; k++) {
                  if (RCC.Validation.Tests._hasValue(opt.dependables[i]) && opt.dependables[i].rcc.term.listValues[k].value == opt.values[i][j]) {
                    if (antal > 0)
                      messStr += " eller ";
                    messStr += opt.dependables[i].rcc.regvar.compareDescription + " = " + opt.dependables[i].rcc.term.listValues[k].text;
                    opt.dependables[i].rcc.accessed(true);
                    antal++;
                  }
                }
              }
            }

            //alert(messStr);

            return {
              result: 'failed',
              message: messStr
            };
          } else
            return {
              result: 'ok'
            };
        } else
          return {
            result: 'ok'
          };
      };
    };


})(jQuery);
