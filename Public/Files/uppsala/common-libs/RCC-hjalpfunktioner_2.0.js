﻿/* RCC.js */

(function (window) {

  /**
   * Namespace för RCC-klasserna.
   *
   * @property metadataProperty Egenskapen under vilket metadata sparas i variabler.
   * @property bindingsPrefix Prefix för datakopplingarna.
   *
   * @name RCC
   * @namespace
   * @type Object
   */
  var RCC = {
    metadataProperty: 'rcc',
    bindingsPrefix: 'rcc-'
  };
  window['RCC'] = RCC;

})(window);

/* RCC/BindingHandlers.js */

(function ($, ko, RCC) {

  var cssClasses = {
    includedByMonitor: RCC.bindingsPrefix + 'included',
    includedIcon: RCC.bindingsPrefix + 'included-icon',
    validationFailed: RCC.bindingsPrefix + 'invalid',
    glyphiconCheck: 'glyphicon-check',
    glyphiconUnchecked: 'glyphicon-unchecked'
  };

  /**
   * Hjälpfunktioner för Knockout.
   * @namespace
   */
  RCC.BindingHandlers = {};

  /**
   * Hitta närmaste variabeln med angivet namn i givet
   * binding context.
   *
   * @param {String} name Namn på sökt variabel.
   * @param {Object} bindingContext Binding context.
   * @returns {Object|undefined} The variable in question, or undefined if not found.
   */
  RCC.BindingHandlers.nearestVariable =
    function (name, bindingContext) {
      var contexts = [].concat(bindingContext.$data, bindingContext.$parents, bindingContext.$root);
      for (var i = 0; i < contexts.length; i++) {
        if (contexts[i].hasOwnProperty(name))
          return contexts[i][name];
      }
    };

  /**
   * Knockout, se <a href="http://knockoutjs.com/documentation/introduction.html">dokumentationen</a>.
   * @name ko
   * @namespace ko
   * @ignore
   */

  /**
   * Datakopplingar för Knockout.
   * Samlar de <a href="http://knockoutjs.com/documentation/introduction.html">inbyggda</a>
   * datakopplingarna och <a href="http://knockoutjs.com/documentation/custom-bindings.html">utvidgningar</a>.
   *
   * <p>Nedan förutsätts att RCC.bindingsPrefix (se {@link RCC}) är "rcc-". I annat fall
   * blir datakopplingarna istället <code>RCC.bindingsPrefix+'var'</code> istället
   * för rcc-var, och så vidare.</p>
   *
   * <p><em>Implementationskommentar:</em> Koden utnyttjar att <code>ko.applyBindingsToNode</code> kan ta
   * en vymodell <em>eller</em> ett binding-context som argument. Ett alternativ vore att istället anropa
   * <code>init</code> och <code>update</code> för den andra datakopplingen.</p>
   *
   * @namespace ko.bindingHandlers
   * @name ko.bindingHandlers
   */
  var rccBinding = RCC.bindingsPrefix + 'var';

  /*
   * Krävs(?) postprocessing för att ersätta _RCCPREFIX_ med rcc-, i och med
   * att jsdoctoolkit 2 inte stödjer namn innehållande "-".
   */

  /**
   * Binding för diverse INCA-funktionalitet.
   *
   * <ol>
   *	<li>Visar valideringsfel.</li>
   *	<li>Sätter readOnly/disabled om $form.isReadOnly är sann.</li>
   *	<li>Skapar monitor-kryssrutor om $user.role.isReviewer är sann och formuläret är skrivbart.</li>
   *  <li>Visar tooltips.</li>
   *  <li>Visar variabelns namn som en tooltip, om $opt.displayNames är sann.</li>
   * </ol>
   *
   * @name ko.bindingHandlers._RCCPREFIX_var
   * @public
   */
  ko.bindingHandlers[rccBinding] = {
    init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
      var v = valueAccessor();

      if (!v[RCC.metadataProperty])
        throw new Error('The binding "' + RCC.bindingsPrefix + 'var" requires an observable from RCC.ViewModel as argument.');

      if (element.type == 'checkbox' || element.type == 'radio')
        ko.utils.registerEventHandler(element, 'change', function () {
          v[RCC.metadataProperty].accessed(true);
        });
      else {
        ko.utils.registerEventHandler(element, 'blur', function () {
          v[RCC.metadataProperty].accessed(true);
        });
        ko.utils.registerEventHandler(element, 'focusout', function () {
          v[RCC.metadataProperty].accessed(true);
        });
      }

      var $el = $(element);

      // Find closest $form, $user and $opt objects.
      var obj = {};
      ko.utils.arrayForEach(['$form', '$user', '$opt'], function (x) {
        obj[x] = RCC.BindingHandlers.nearestVariable(x, bindingContext);
      });

      if (!obj.$form || !obj.$user)
        throw new Error('Unable to find $form or $user in binding context.');

      if (!obj.$opt)
        obj.$opt = {};

      // Set disabled/readonly if the form is read-only.
      if (obj.$form.isReadOnly) {
        $el.prop($el.is('select, option, button, input[type="checkbox"], input[type="radio"]') ? 'disabled' : 'readOnly', true);
      } else if (obj.$user.role.isReviewer) {
        var $icon = $('<span class="rcc-include-icon rcc-icon paddl rcc-large glyphicon glyphicon-check"></span>');
        var $includeEl = $('<div class="include-icon-block hidden"></div>').append($icon);
        var $anchorElement = ($el.closest('.relative').siblings('.input-group-addon').length == 0 ? $el.closest('.relative') : $el.parent());

        $anchorElement.after($includeEl);

        $includeEl.click(function () {
          if (v[RCC.metadataProperty].include())
            v[RCC.metadataProperty].include(false);
          else
            v[RCC.metadataProperty].include(true);
        });

        ko.computed(function () {
          if (v[RCC.metadataProperty].showInclude()) {
            $includeEl.removeClass('hidden');
            // Set element class based on inclusion.
            ko.utils.toggleDomNodeCssClass($icon[0], cssClasses.includedIcon, v[RCC.metadataProperty].include());
            ko.utils.toggleDomNodeCssClass($icon[0], cssClasses.glyphiconCheck, v[RCC.metadataProperty].include());
            ko.utils.toggleDomNodeCssClass($icon[0], cssClasses.glyphiconUnchecked, !v[RCC.metadataProperty].include());
          } else {
            $includeEl.addClass('hidden');
          }
        });
      }

      ko.computed(function () {
        if (!v[RCC.metadataProperty].accessed()) {
          ko.utils.toggleDomNodeCssClass(element, cssClasses.validationFailed, false);
          return;
        }
        if (v[RCC.metadataProperty].validation) {
          var errors = v[RCC.metadataProperty].validation.errors();
          if (errors.length > 0) {
            var escapeHtml = function (str) {
              return $('<div>').text(str).html();
            };

            var validationMessage = $.map(errors, function (err) {
              return escapeHtml(err.message);
            });

            if (!$('#error-row-' + $el.attr('id'))[0]) {
              var $errorEl = $('<div id="error-row-' + $el.attr('id') + '"class="row error-row"><div class="col-xs-3">&nbsp;</div><div class="input-group errors"><span class="rcc-error rcc-larger paddl glyphicon glyphicon-exclamation-sign"></span>&nbsp;<span id="errors-' + $el.attr('id') + '">' + validationMessage.join('') + '</span></div></div>');
              $el.closest('.form-group').after($errorEl);
            }
          } else {
            var $errorEl = $('#error-row-' + $el.attr('id'));
            $errorEl.remove();
          }

          ko.utils.toggleDomNodeCssClass(element, cssClasses.validationFailed, errors.length > 0);
        }
      });
    },
    update: function (element, valueAccessor, allBindings, viewModel, bindingContext) {
      var value = valueAccessor();
      if (value() == "") {
        value(undefined);
      }
    }
  };

  /**
   * Bekvämlighetsdatakoppling. Ekvivalent med att applicera Knockouts "text"
   * <em>och</em> {@link ko.bindingHandlers._RCCPREFIX_var}, med samma argument.
   *
   * @name ko.bindingHandlers._RCCPREFIX_text
   * @public
   */
  /**
   * Bekvämlighetsdatakoppling. Ekvivalent med att applicera Knockouts "value"
   * <em>och</em> {@link ko.bindingHandlers._RCCPREFIX_var}, med samma argument.
   *
   * @name ko.bindingHandlers._RCCPREFIX_value
   * @public
   */
  /**
   * Bekvämlighetsdatakoppling. Ekvivalent med att applicera Knockouts "checked"
   * <em>och</em> {@link ko.bindingHandlers._RCCPREFIX_var}, med samma argument.
   *
   * @name ko.bindingHandlers._RCCPREFIX_checked
   * @public
   */
  ko.utils.arrayForEach(['text', 'value', 'checked'],
    function (baseBinding) {
      ko.bindingHandlers[RCC.bindingsPrefix + baseBinding] = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
          var val = valueAccessor();

          // Can only handle INCA's observables (otherwise rcc-var makes no sense).
          if (!ko.isObservable(val) || !val[RCC.metadataProperty])
            throw new Error(RCC.bindingsPrefix + baseBinding + ' requires an observable from RCC.ViewModel as argument.');

          var bindings = {};
          ko.utils.arrayForEach([baseBinding, rccBinding], function (binding) {
            // Can't have both helper and base on the same element.
            if (binding in allBindingsAccessor())
              throw new Error('Cannot bind ' + RCC.bindingsPrefix + baseBinding + ' and ' + binding + ' to the same element.');

            bindings[binding] = valueAccessor();
          });

          /* Patch 2014-09-03 */
          /* Allow valueUpdate  parameter for rcc-value */
          if (baseBinding == "value" && "valueUpdate" in allBindingsAccessor()) {
            var valUpd = ko.unwrap(allBindingsAccessor().valueUpdate);
            if (valUpd) {
              bindings["valueUpdate"] = allBindingsAccessor().valueUpdate;
            }
          }
          /* Patch 2014-09-03 End */

          ko.applyBindingsToNode(element, bindings, bindingContext);
        }
      };
    });

  /**
   * Bekvämlighetsdatakoppling. Kopplingen med argument <code>x</code> är funktionsmässigt
   * ekvivalent med att applicera
   * <ul>
   *    <li>{@link ko.bindingHandlers._RCCPREFIX_var} med argument <code>x</code></li>
   *    <li>Knockouts koppling <code>options</code> med argument <code>x.term.listValues</code></li>
   *    <li>Knockouts koppling <code>optionsText</code> med argument <code>'text'</code></li>
   *    <li>Knockouts koppling <code>optionsCaption</code> med argument <code>'– Välj –'</code></li>
   * </ul>
   * Det går också att manuellt sätta enskilda kopplingar, vilka då får företräde framför värdena ovan.
   *
   * @example
   * Lista som visar listvärdena inom parentes:
   * &lt;select data-bind="rcc-list: x,
   *    optionsText: function ( val ) { return val.text + '(' + val.value + ')'; }"&gt;
   *
   * @example
   * Lista som inte har någon ”– Välj –”-platshållare.
   * &lt;select data-bind="rcc-list: x,
   *    optionsCaption: undefined"&gt;
   *
   * @name ko.bindingHandlers._RCCPREFIX_list
   * @public
   */
  ko.bindingHandlers[RCC.bindingsPrefix + 'list'] = {
    init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
      var val = valueAccessor();

      var metadata = val[RCC.metadataProperty];
      if (!metadata || !metadata.term || !metadata.term.listValues)
        throw new Error('The binding "' + RCC.bindingsPrefix + 'list" requires a list observable from RCC.ViewModel as argument.');

      var currentBindings = allBindingsAccessor();
      var bindings = {
        value: val,
        options: metadata.term.listValues,
        optionsText: 'text',
        optionsCaption: '– Välj –'
      };
      bindings[rccBinding] = val;

      /*
       * Allow user to override bindings. The option, optionsText and
       * optionCaption bindings need to be applied together.
       */
      for (var binding in bindings)
        if (bindings.hasOwnProperty(binding) && currentBindings.hasOwnProperty(binding))
          bindings[binding] = currentBindings[binding];

      ko.applyBindingsToNode(element, bindings, bindingContext);
    },
    /*  Patch 2014-09-16 */
    /* fix for IE8 select-lists width being set to 0 if not rendered at init of form */
    update: function (element, valueAccessor, allBindings, viewModel, bindingContext) {
      if (valueAccessor) {
        //$(element).css("width", "auto");
      }
    }
    /*  Patch 2014-09-16 End */
  };

  /** Jonas testing för single-select knappar istället för att använda listor... **/
  ko.bindingHandlers[RCC.bindingsPrefix + 'singleselect'] = {
    /*init: function(element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
      var val = valueAccessor();
      var metadata = val[RCC.metadataProperty];
      if (!metadata || !metadata.term || !metadata.term.listValues)
          throw new Error('The binding "' + RCC.bindingsPrefix + 'list" requires a list observable from RCC.ViewModel as argument.');

      var options = metadata.term.listValues;

      var $btngrp = $(element);

      for(var i = 0; i < options.length; i++) {
        var opt = options[i];
        var $btn = $('<label class="btn btn-default"><input type="radio" data-bind="checkedValue: ' + opt + ', checked: ' + val + '" />' + opt.text + '</label>');
        $btngrp.append($btn);
      }

      var bindings = {};
      bindings[rccBinding] = val;
      var currentBindings = allBindingsAccessor();

      for (var binding in bindings)
         if (bindings.hasOwnProperty(binding) && currentBindings.hasOwnProperty(binding))
             bindings[binding] = currentBindings[binding];

      ko.applyBindingsToNode(element, bindings, bindingContext);
    }*/

    init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
      var val = valueAccessor();
      var newValueAccessor = function () {
        return {
          change: function () {
            val(element.value);
          }
        };
      };
      var bindings = {};
      bindings[rccBinding] = val;
      var currentBindings = allBindingsAccessor();

      for (var binding in bindings)
        if (bindings.hasOwnProperty(binding) && currentBindings.hasOwnProperty(binding))
          bindings[binding] = currentBindings[binding];

      ko.applyBindingsToNode(element, bindings, bindingContext);

      ko.bindingHandlers.event.init(element, newValueAccessor, allBindingsAccessor, viewModel, bindingContext);
    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
      if ($(element).val() == ko.unwrap(valueAccessor())) {
        setTimeout(function () {
          $(element).closest('.btn').button('toggle');
        }, 1);
      }
    }
  };

})(window['jQuery'], window['ko'], window['RCC']);

/* RCC/ViewModel.js */

(function (inca, ko, RCC, window) {

  var sogeti = {};

  /**
   * Skapa en vymodell.
   *
   * @class
   *
   * @property {ko.observable} kortnamn ko.observable för registervariabeln med givet kortnamn
   * @property {Object} $$ Objekt med undertabeller, där varje undertabell <code>$$.<em>undertabellensNamn</em></code> är
   *			en <code>ko.observableArray</code> med alla underrader i tabellen <code>undertabellensNamn</code> som element,
   *			och egenskaper
   *			<ul>
   *				<li><code>$$.<em>undertabellensNamn</em>.rcc.add()</code>: skapar en ny rad i undertabellen</li>
   *				<li><code>$$.<em>undertabellensNamn</em>.rcc.remove( <em>rad</em> )</code>: radera givet radobjekt</li>
   *			</ul>
   *			Underraderna har egenskaper:
   *			<ul>
   *				<li><code><em>kortnamn</em></code>: <code>ko.observable</code> för registervariabeln i undertabellen med givet kortnamn</li>
   *				<li><code>$$</code>: objekt med undertabeller till undertabellen, rekursivt på samma form</li>
   *			</ul>
   * @property {Object} $env Objekt med konstanttermer
   * @property {Object} $form INCAs formulärobjekt (normalt sett <code>inca.form</code>)
   * @property {Object} $user INCAs användarobjekt (normalt sett <code>inca.user</code>)
   * @property {Object} $events Händelselyssnare; kan ändras dynamiskt
   * @property {Object} $vd Värdedomäner som inte också är registervariabler
   * @property {Object} $opt Allmänt inställningsobjekt för vymodellen.
   *
   * @constructor
   * @param {Object} [opt={}] Alternativ.
   * @param {String} [opt.rootTable] Rot-tabellens namn, om den saknas bestäms rottabellen från tabellstrukturen.
   * @param {String} [opt.incaForm=inca.form] INCAs formulärobjekt, sparas till <code>$form</code> i vymodellen.
   * @param {String} [opt.incaUser=inca.user] INCAs användarobjekt, sparas till <code>$user</code> i vymodellen.
   * @param {Object} opt.incaForm.data Data för vymodellen.
   * @param {Object} opt.incaForm.env Konstanttermer i vymodellen.
   * @param {Object} opt.incaForm.metadata Metadata för modellen.
   * @param {Object} opt.incaForm.createDataRow Funktion som skapar ny tabellrad.
   * @param {RCC.Validation} [opt.validation] Valideringsobjekt.
   * @param {Object} [opt.events={}] Händelselyssnare, kan ändras dynamiskt.
   * @param {Object} [opt.events.rowCreated={}] Objekt för återanropsfunktioner vid skapandet av en rad, där egenskapen anger
   *		tabellens namn, och värdena är en funktion eller en array av funktioner. Funktionerna anropas med den nya
   *		raden som enda argument.
   * @param {Object} [opt.events.rowAdded={}] Objekt för återanropsfunktioner vid tillägg av en rad, där egenskapen anger
   *		tabellens namn, och värdena är en funktion eller en array av funktioner. Funktionerna anropas med den nya
   *		raden som enda argument. Till skillnad mot rowCreated anropas dessa funktioner även när en befintlig rad
   *		initialt hämtas från databasen.
   * @param {String|false} [opt.tooltips=false] Om 'text' visas tooltips (tagna från registervariabelns "description") som text,
   *		om 'html' visas formatterade HTML-tooltips. Om falskt visas inga tooltips.
   * @param {Object} [opt.opt={}] Kopieras <code>$opt</code> i vymodellen.
   * @param {Boolean} [opt.opt.displayNames=false] Om sant visas registervariableras namn som en tooltip, för <code>rcc-var</code>-bundna variabler.
   *
   * @description
   * Klassen är wrapper kring en smärre modifikation av Sogetis sogeti.createViewModel och sogeti.fillViewModel.
   *
   * @example
   * var validering = new RCC.Validation();
   * var vm = new RCC.ViewModel( { validation: validering } );
   *
   * @constructor
   */
  RCC.ViewModel =
    function (opt) {
      if (!opt)
        opt = {};

      if (!opt.incaForm)
        opt.incaForm = inca.form;

      // Find unique table that is not a subtable.
      if (!('rootTable' in opt)) {
        var tables = [],
          isSubTable = {};
        for (var table in opt.incaForm.metadata) {
          if (opt.incaForm.metadata.hasOwnProperty(table)) {
            tables.push(table);

            var subTables = opt.incaForm.metadata[table].subTables;
            for (var i = 0; i < subTables.length; i++)
              isSubTable[subTables[i]] = true;
          }
        }

        var rootTables = ko.utils.arrayFilter(tables, function (table) {
          return !isSubTable[table];
        });

        if (rootTables.length == 1)
          opt.rootTable = rootTables[0];
        else {
          throw new Error('Must supply "rootTable" option for RCC.ViewModel (' +
            (rootTables.length == 0 ? 'no candidates found' : ('multiple candidates found: ' + rootTables.join(', '))) + ').');
        }
      }

      if (!opt.incaUser)
        opt.incaUser = inca.user;

      if (!opt.events)
        opt.events = {};

      var vm = sogeti.createViewModel(opt.rootTable, opt.incaForm.data, opt.incaForm.metadata, opt.incaForm.createDataRow.bind(opt.incaForm), opt);

      vm.$events = opt.events;
      vm.$env = opt.incaForm.env;
      vm.$form = opt.incaForm;
      vm.$user = opt.incaUser;
      vm.$opt = opt.opt || {};

      if (opt.validation) {
        vm.$validation = opt.validation;
        opt.validation.track(vm);
      }

      return vm;
    };

  /**
   * Fyll en vymodell eller en undertabell med data från ett objekt. Kopierar endast värden mellan
   * egenskaperna, och tar inte hänsyn till undertabeller med mera.
   *
   * @param {Object} opt Inställningar
   * @param {RCC.ViewModel|Object} opt.target Vymodellen/undertabellen som ska fyllas.
   * @param {Object} opt.data Data som ska användas.
   */
  RCC.ViewModel.fill =
    function (opt) {
      return sogeti.fillViewModel(opt.target, opt.data);
    };

  /**
   * Fyll en vymodell med data från ett objekt.
   *
   * @param {Object} data Data som ska användas.
   * @deprecated Kommer tas bort för att inte riskera kollision med registervariabel. Använd istället {@link RCC.ViewModel.fill}.
   */
  RCC.ViewModel.prototype.fill =
    function (data) {
      var self = this;
      return sogeti.fillViewModel(self, data);
    };

  /*
   * Nedanstående kodblock är taget från sogeti:s exempelformulär, med följande förändringar:
   * 1. Callback för skapande av rad är nu per vymodell, och anges vid skapandet (men kan också ändras).
   *		även lagt till callback för "tillägg av rad" (anropas när raderna skapas initialt, alltså
   *		utan att add-funktionerna anropas).
   * 2. Tidigare valideringskod är borttagen.
   * 3. Tillagda argument till createViewModel:
   *		- metadata: metadata-objektet för formuläret (typiskt sett från inca.form.metadata)
   *		- createDataRow: funktion för att skapa datarad (typiskt sett from inca.form.createDataRow)
   *		- opt: diverse alternativ
   *		- opt.validation: ett valideringsobjekt, om detta ges sätts för varje variabel metadatan "validation"
   *			till resultatet av validation.init, anropat med variabeln som argument.
   *		- opt.events: callbacks.
   *		- opt.tooltips: om 'html' eller 'text' sätts metadataegenskapen "tooltip" till ett objekt med
   *			egenskap 'html' respektive 'text' och värde taget från termens "description". För 'html' är
   *			värdet en ko.observable. Om false lämnas "tooltip" odefinierad.
   * 4. Metadata "accessed" (ko.observable med startvärde false) har lagts till; används av rcc-bindningarna.
   * 5. Undertabeller sparas under objektet $$, istället för med prefix.
   * 6. Värdedomäner som inte är registervariabler sparas under $vd, istället för med prefix.
   * 7. Metadata (term, include, ...) lagras nu under ett objekt "rcc".
   */
  /*
  Detta scriptblock skapar ett gäng hjälpfunktioner som alla hamnar under variabeln 'sogeti'.
  Följande hjälpfunktioner finns:
    - sogeti.createViewModel
    - sogeti.fillViewModel

  ----------------------------------------------------------------------------------------------------------
  sogeti.createViewModel
  Funktionen används för att underlätta konvertingen av inca.form.data till en vymodell som kan användas
  av Knockout. Den genererade vymodellen ser hela tiden till att datat i vymodellen ligger i sync med datat
  i inca.form.data.

  Exempel 1
  var viewModel = inca.createViewModel("Uppföljning", inca.form.data);

  ----------------------------------------------------------------------------------------------------------
  sogeti.fillViewModel
  Denna funktion används för att autofylla en vymodell utifrån datat en värdedomän pekar på.

  Funktionen tar två parameterar. Den första är vymodellen som ska fyllas med data och den andra är ett
  objekt varifrån datat tas. Kopiering sker om objekten har egenskaper med samma namn.

  Exempel 1
  // Låtsas att vi har en värdedomän som heter Kontaktlista, samt en variabel newValue som pekar på ett
  // id i den listan.
  inca.form.getRegisterRecordData({
  	vdlist: "VD_Kontaktlista",
  	value: newValue,
  	success: function (data) {
  		// data har en egenskap för varje registervariabel. Egenskapens namn är baserat på register-
  		// variabelns kortnamn.
  		sogeti.fillViewModel(viewModel, data);
  	}
  });
  */

  /*globals ko,inca*/
  (function (sogeti) {
    "use strict";

    var c = { // Constants related to the creation and filling of the viewmodel
      destroy: "_destroy", // Name of the property that marks a row for deletion
      subTables: "$$", // Subtables are stored under this attribute.
      vdlists: "$vd", // Non regvars vdlists are stored under this attribute.
      metadata: RCC.metadataProperty, // All metadata (include, accessed, ...) is stored under this attribute.
      addMethod: "add", // Name of the metadata property to add a new subtable row
      removeMethod: "remove", // Name of the metadata property to remove a subtable row

      /*  Patch 2013-03-24 */
      /* support for show/hide include pop-up in monitor mode */
      showInclude: "showInclude",
      /*  Patch 2013-03-24 End */

      include: "include", // Name of the metadata property to access the include observable (reviewer checkbox)
      accessed: "accessed", // Name of the metadata property to access the accessed observable (a bound form element has been blurred)
      regvar: "regvar", // Name of the metadata property to access the regvar metadata
      regvarName: "regvarName", // Name of the metadata property to access the name of the regvar
      term: "term", // Name of the metadata property to access the term metadata
      data: "$__data", // Name of the metadata reference to the underlying datarow
      validation: "validation", // Name of the metadata property to access the validation object.
      tooltip: "tooltip" // Name of the metadata property to access the tooltip object.
    };

    var createViewModel = (function () {
      // JSLint doesn't like when creating functions within loops. Lets conform to the rules and a let a method create those functions instead.
      var functionFactory = {
        createAdd: function (viewModel, name, metadata, createDataRow, opt) {
          return function (newItem) {
            newItem = newItem || createViewModel(name, undefined, metadata, createDataRow, opt);

            // Push the new data item onto the data object.
            viewModel[c.data].subTables[name].push(newItem[c.data]);
            viewModel[c.subTables][name].push(newItem);

            return newItem;
          };
        },
        createRemove: function (viewModel, name) {
          return function (item) {
            if (item[c.data].id) {
              item[c.data][c.destroy] = true;
            } else {
              ko.utils.arrayRemoveItem(viewModel[c.data].subTables[name], item[c.data]);
            }

            viewModel[c.subTables][name].remove(item);
          };
        }
      };

      function createObservable(viewModel, source, termMetadata, sourceName, propertyName) {
        /// <summary>Creates observables for value and include on the viewmodel and ensures the values are synced to the source.</summary>
        /// <param name="viewModel">Viewmodel on which the observables will be added.</param>
        /// <param name="source">Data source, e.g. inca.form.data.regvars.</param>
        /// <param name="termMetadata">Term metadata.</param>
        /// <param name="sourceName">Name of the source property.</param>
        /// <param name="propertyName">Property name on the target view model.</param>
        var initialValue = source[sourceName].value;

        propertyName = propertyName || sourceName;

        if (termMetadata.dataType === "list") {
          initialValue = ko.utils.arrayFirst(termMetadata.listValues, function (item) {
            return item.id === initialValue;
          }) || undefined;
        }
        /*  Patch 2013-10-12 */
        /* Initial value with '0' decimal(s) if needed */
        else if (termMetadata.dataType === "decimal" && typeof initialValue === "number") {
          initialValue = initialValue.toFixed(termMetadata.precision).replace(/\./, ",");
        }
        /*  Patch 2013-10-12 End */

        viewModel[propertyName] = ko.observable(initialValue);
        viewModel[propertyName].subscribe(function (newValue) {
          if (newValue && termMetadata.dataType === "list") {
            source[sourceName].value = newValue.id;
          } else {
            source[sourceName].value = newValue;
          }
        });

        if (c.metadata in viewModel[propertyName]) {
          throw new Error('The created ko.observable already has a property with name "' + c.metadata + '"');
        }



        viewModel[propertyName][c.metadata] = {};
        viewModel[propertyName][c.metadata][c.include] = ko.observable(source[sourceName].include);
        viewModel[propertyName][c.metadata][c.include].subscribe(function (newValue) {
          source[sourceName].include = newValue;
        });

        /*  Patch 2013-03-24 */
        /* support for show/hide include pop-up in monitor mode */
        viewModel[propertyName][c.metadata][c.showInclude] = ko.observable(false);
        /*  Patch 2013-03-24 End */

        // Accessed observable; not synced with the model.
        viewModel[propertyName][c.metadata][c.accessed] = ko.observable(false);
      }

      function setupRegisterVariables(viewModel, data, metadata, opt) {
        /// <summary>Ensures all register variables on the data object are mapped onto the viewmodel.</summary>
        /// <param name="viewModel">Viewmodel on which the register variables will be mapped to.</param>
        /// <param name="data">The data object, e.g. inca.form.data.</param>
        /// <param name="metadata">Metadata for the data object.</param>
        /// <param name="opt">Options object.</param>
        var prop, termMetadata;

        for (prop in metadata.regvars) {
          if (metadata.regvars.hasOwnProperty(prop)) {
            termMetadata = metadata.terms[metadata.regvars[prop].term];
            createObservable(viewModel, data.regvars, termMetadata, prop, undefined);

            // Add references to the regvar and term metadata.
            viewModel[prop][c.metadata][c.regvar] = metadata.regvars[prop];
            viewModel[prop][c.metadata][c.term] = termMetadata;

            // Store regvar name in metadata.
            viewModel[prop][c.metadata][c.regvarName] = prop;

            // Tooltips
            if (metadata.regvars[prop].description) {
              if (opt.tooltips == "html") {
                viewModel[prop][c.metadata][c.tooltip] = {
                  html: ko.observable(metadata.regvars[prop].description)
                };
              } else if (opt.tooltips == "text") {
                viewModel[prop][c.metadata][c.tooltip] = {
                  text: metadata.regvars[prop].description
                };
              }
            }
            // Validation handling.
            if (opt && opt.validation) {
              opt.validation.init(viewModel[prop], {
                source: {
                  name: prop,
                  type: 'regvar'
                }
              });
            }
          }
        }
      }

      function setupValueDomains(viewModel, data, metadata, opt) {
        /// <summary>Ensures all valuedomain lists on the data object are mapped onto the viewmodel.</summary>
        /// <param name="viewModel">Viewmodel on which the valuedomain lists will be mapped to.</param>
        /// <param name="data">The data object, e.g. inca.form.data.</param>
        /// <param name="metadata">Metadata for the data object.</param>
        var prop, termMetadata;

        viewModel[c.vdlists] = {};
        for (prop in metadata.vdlists) {
          if (metadata.vdlists.hasOwnProperty(prop)) {
            termMetadata = metadata.terms[metadata.vdlists[prop].term];
            createObservable(viewModel[c.vdlists], data.vdlists, termMetadata, prop, prop);

            // Add reference to the term metadata
            viewModel[c.vdlists][prop][c.metadata][c.term] = metadata.terms[metadata.vdlists[prop].term];

            // Validation handling.
            if (opt && opt.validation) {
              opt.validation.init(viewModel[c.vdlists][prop], {
                source: {
                  name: prop,
                  type: 'vdlist'
                }
              });
            }
          }
        }
      }

      function setupSubTables(viewModel, data, tableMetadata, metadata, createDataRow, opt) {
        /// <summary>Ensures the viewmodel gets observableArrays for each sub table.</summary>
        /// <param name="viewModel">Viewmodel on which the valuedomain lists will be mapped to.</param>
        /// <param name="data">The data object, e.g. inca.form.data.</param>
        /// <param name="metadata">Metadata for the data object.</param>
        /// <param name="opt">Options object.</param>
        var name, i, j, item;
        for (i = 0; i < tableMetadata.subTables.length; i += 1) {
          name = tableMetadata.subTables[i];

          viewModel[c.subTables][name] = ko.observableArray();
          viewModel[c.subTables][name][c.metadata] = {};

          // Create add and remove methods on the viewmodel to make it easier to add and remove child rows.
          viewModel[c.subTables][name][c.metadata][c.addMethod] = functionFactory.createAdd(viewModel, name, metadata, createDataRow, opt);
          viewModel[c.subTables][name][c.metadata][c.removeMethod] = functionFactory.createRemove(viewModel, name);

          for (j = 0; j < data.subTables[name].length; j += 1) {
            item = createViewModel(name, data.subTables[name][j], metadata, createDataRow, opt);
            viewModel[c.subTables][name].push(item);
          }
        }
      }

      return function (name, data, metadata, createDataRow, opt) {
        var tableMetadata = metadata[name],
          viewModel = {},
          raiseRowCreated = false;

        if (tableMetadata === undefined) {
          throw new Error('Metadata for table "' + name + '" not found');
        }

        // When creating a new viewmodel, ensure a new datarow is created for that viewmodel.
        if (data === undefined) {
          data = createDataRow(name);
          raiseRowCreated = true;
        }

        viewModel[c.data] = data;
        viewModel[c.subTables] = {};
        setupRegisterVariables(viewModel, data, tableMetadata, opt);
        setupValueDomains(viewModel, data, tableMetadata, opt);
        setupSubTables(viewModel, data, tableMetadata, metadata, createDataRow, opt);

        // Raise rowCreated event.
        if (raiseRowCreated && opt && opt.events && "rowCreated" in opt.events) {
          ko.utils.arrayForEach([].concat(opt.events["rowCreated"][name] || []), function (cb) {
            cb.call(viewModel, viewModel)
          });
        }

        // Raise rowAdded event.
        if (opt && opt.events && "rowAdded" in opt.events) {
          ko.utils.arrayForEach([].concat(opt.events["rowAdded"][name] || []), function (cb) {
            cb.call(viewModel, viewModel)
          });
        }

        return viewModel;
      };
    }()),
      fillViewModel = function (viewModel, data) {
        var prop, vmProp;

        for (prop in data) {
          if (data.hasOwnProperty(prop)) {
            if (ko.isWriteableObservable(viewModel[prop])) {
              vmProp = viewModel[prop];

              if (vmProp[c.metadata].term.dataType === "list") {
                viewModel[prop](ko.utils.arrayFirst(vmProp[c.metadata].term.listValues, function (item) {
                  return item.id == data[prop];
                }) || undefined);
              }
              /* Patch 2013-10-12 */
              /* Initial value with '0' decimal(s) if needed */
              else if (vmProp[c.metadata].term.dataType === "decimal" && typeof data[prop] === "number") {
                viewModel[prop](data[prop].toFixed(vmProp[c.metadata].term.precision).replace(/\./, ","));
              }
              /* Patch 2013-10-12 End*/
              else {
                viewModel[prop](data[prop]);
              }
            }
          }
        }
      };

    sogeti.createViewModel = createViewModel;
    sogeti.fillViewModel = fillViewModel;
  }(sogeti));

})(window['inca'], window['ko'], window['RCC'], window);

/* RCC/Utils.js */

(function ($, RCC, window) {
  /**
   * Various utility functions.
   *
   * @namespace
   */
  RCC.Utils = {};

  /**
   * Returns a dummy inca.form object.
   *
   * @param {Object} db An object with properties:
   *	<ul>
   *		<li><code>rootTable</code>: name of the root table</li>
   *		<li><code>regvars</code>: an object of regvars, where the property is the name of the
   *				variable and the value is the type, which should be one of the following:
   *			<ul>
   *				<li><code>string</code>: a text string.</li>
   *				<li><code>integer</code>: an integer.</li>
   *				<li><code>datetime</code>: a date.</li>
   *				<li><code>boolean</code>: a boolean value.</li>
   *				<li><code>String[]</code>: a list, where the values of the array are the allowed values.</li>
   *				<li><code>{id, value, text}[]</code>: a list, where the values of the array are the allowed objects.</li>
  				<li><code>{ term: { ... } }</code>: an object with a property "term", which is used as the term.</li>
   *			</ul>
  		</li>
   *		<li><code>subTables {Object}</code>: subtables, with properties <code>regvars</code> and <code>subTables</code>,
   *			recursively of the same form.</li>
   *	</ul>
   *
   * @example
   * // En enkel tabell "Anmälan", med ett namnfält och en flervalslista "status",
   * // samt en barntabell med en flervalslista "utfall":
   * inca.form = RCC.Utils.createDummyForm(
   * {
   *        rootTable: 'Anmälan',
   *        regvars: { namn: 'string', status: [ 'alt1', 'alt2' ] },
   *        subTables:
   *        {
   *            Behandling:
   *            {
   *                regvars: { utfall: [ 'ex1', 'ex2' ] },
   *                subTables: {}
   *            }
   *        }
   * } );
   *
   * @returns {Object} A data object that can be used in place of inca.form.
   */
  RCC.Utils.createDummyForm =
    function (db) {
      var form = {};
      form.env = {
        _PERSNR: '19111111-1111',
        _FIRSTNAME: 'Test',
        _SURNAME: 'Testsson',
        _INREPNAME: 'Rapportör Rapportörsson',
        _INUNITNAME: 'OC Demo (0)',
        _SEX: 'M',
        _ADDRESS: 'Testgatan 1',
        _POSTALCODE: '12345',
        _CITY: 'Teststad',
        _LKF: '123456',
        _SECRET: false,
        _DATEOFDEATH: '',
        _REPORTERNAME: '',
        _LAN: '12',
        _KOMMUN: '34',
        _FORSAMLING: '56',
        _PATIENTID: 1
      };

      var metadata = {},
        data = {};

      function addTable(table, values) {
        data[table] = {
          regvars: {},
          subTables: {}
        };
        metadata[table] = {
          regvars: {},
          subTables: [],
          terms: {}
        };
        $.each(values.regvars || {},
          function (name, val) {
            data[table].regvars[name] = {
              include: true,
              value: ""
            },
              metadata[table].regvars[name] = {
                term: name
              };

            if ($.isArray(val))
              metadata[table].terms[name] = {
                description: name,
                dataType: 'list',
                listValues: $.map(val, function (x) {
                  return typeof x == 'object' ? x : {
                    id: x,
                    value: x,
                    text: x
                  };
                })
              };
            else if (val.term)
              metadata[table].terms[name] = val.term;
            else
              metadata[table].terms[name] = {
                description: name,
                dataType: val
              };
          });

        $.each(values.subTables || {},
          function (childTable, childValues) {
            metadata[table].subTables.push(childTable);
            data[table].subTables[childTable] = [];
            addTable(childTable, childValues);
          });
      }

      addTable(db.rootTable, {
        regvars: db.regvars,
        subTables: db.subTables
      });

      form.metadata = metadata;
      form.createDataRow = function (table) {
        return $.extend(true, {}, data[table]);
      };
      form.data = form.createDataRow(db.rootTable);
      form.getContainer = function () {
        return document.getElementsByTagName('body')[0];
      };

      return form;
    };

  /**
   * Parses a date of the format YYYY-MM-DD or YYYYMMDD to a Date-object
   * at the given date, in the local time zone.
   *
   * @param {String} str The string to parse.
   * @returns {Date|undefined} The date, or undefined if the format is invalid.
   *
  RCC.Utils.parseYMD =
  function ( str )
  {
  	var m = (str || '').match( /^(\d{4})(-?)(\d\d)\2(\d\d)$/ );

  	return m ? new Date( m[1], parseInt(m[3].replace( /^0/, '' ), 10) - 1, m[4].replace( /^0/, '' ) ) : undefined; //RCC.Utils.parseYYMD(str);
  };*/

  /**
   * Jonas testing 14-07-09
   *
   * Parses a date of the format YYYY-MM-DD or YYYYMMDD or YY-MM-DD or YYMMDD to a Date-object
   * at the given date, in the local time zone.
   *
   * @param {String} str The string to parse.
   * @returns {Date|undefined} The date, or undefined if the format is invalid.
   */
  RCC.Utils.parseYMD = function (str) {
    var m = (str || '').match(/^(\d{4})(-?)(\d\d)\2(\d\d)$/);

    if (m == null) {
      m = (str || '').match(/^(\d{2})(-?)(\d\d)\2(\d\d)$/);

      if (m != null) {
        var n = new Date().getFullYear().toString().substr(2, 2);

        if (m[1] - n <= 0) {
          m[1] = "20" + m[1];
        }
      }
    }

    return m ? new Date(m[1], parseInt(m[3].replace(/^0/, ''), 10) - 1, m[4].replace(/^0/, '')) : undefined;
  };

  /**
   * Jonas testing 17-05-02
   *
   * Parses a year of the format YYYY or YY to a string
   * at the given date, in the local time zone.
   *
   * @param {String} str The string to parse.
   * @returns {String|undefined} The date, or undefined if the format is invalid.
   */
  RCC.Utils.parseYear = function (str) {
    var m = (str || '').match(/^(\d{4})$/);

    if (m == null) {
      m = (str || '').match(/^(\d{2})$/);

      if (m != null) {
        var n = new Date().getFullYear().toString().substr(2, 2);

        if (m[1] - n <= 0) {
          m[1] = "20" + m[1];
        } else {
          m[1] = "19" + m[1];
        }
      }
    }
    return m ? m[1] : undefined;
  };

  /**
   * Returns the local date in the format YYYY-MM-DD.
   *
   * @param {Date} date The date.
   * @returns {String} The date, in the format YYYY-MM-DD.
   */
  RCC.Utils.ISO8601 = function (date) {
    function zeropad(n) {
      return ('0' + n).slice(-2);
    }
    return [date.getFullYear(), zeropad(date.getMonth() + 1), zeropad(date.getDate())].join('-');
  };



})(window['jQuery'], window['RCC'], window);

/* RCC/Validation.js */

(function (ko, RCC, window) {

  /**
   * @license
   * Portions of this software may utilize the following copyrighted material,
   * the use of which is hereby acknowledged.
   *
   * Knockout Validate v0.1-pre
   * (c) Martin Landälv - http://mlandalv.github.com/knockout-validate/)
   * MIT license
   */

  /**
   * Container class for validation.
   *
   * <p>View models that should be tracked are added using the {@link RCC.Validation#track} method,
   * and the view models are then scanned (including rows of subtables) for errors, taking into
   * account any added or deleted rows in subtables.
   * Encountered errors are stored in the <code>errors</code> property of the RCC.Validation instance.
   * Variables are initialized using the {@link RCC.Validation#init} method.</p>
   *
   * <p>Typically, the validation object is passed to the constructor of {@link RCC.ViewModel},
   * which configures the validation automatically.</p>
   *
   * @property {ko.observableArray} errors Currently active errors, where each error is an object
   *	with a property "message" explaining the error. Errors are included only if the metadata
   *	property <code>included</code> of the variable is true.
   *
   * @param {Object} opt Options.
   * @param [opt.addDefaultValidations=true] If true, then default validations (date check, integer check, ...)
   * 		are added to initialized variables (in RCC.Validation.prototype.init).
   * @param {Boolean|ko.subscribable} [opt.requiredDefault=true] Default value for the "required" metadata value.
   * @param {Boolean|ko.subscribable} [opt.includedDefault=true] Default value for the "included" metadata value.
   *
   * @description
   * <p>Inspired by and loosely based on Knockout Validate v0.1-pre ((c) Martin Landälv -
   * http://mlandalv.github.com/knockout-validate/), created by Martin Landälv and licensed
   * under the MIT license.</p>
   *
   * @constructor
   */
  RCC.Validation =
    function (opt) {
      var self = this;

      if (!opt)
        opt = {};

      /* Patch 2013-03-12*/
      /* Added support for setting validation enabled true/false */
      self.enabled = ko.observable(true);
      /* Patch 2013-03-12 End*/

      self.viewModels = ko.observableArray([]);
      self.errors = ko.computed(RCC.Validation.prototype._getErrors.bind(self));
      self.warnings = ko.computed(RCC.Validation.prototype._getWarnings.bind(self));
      self.requiredDefault = 'requiredDefault' in opt ? opt.requiredDefault : true;
      self.includedDefault = 'includedDefault' in opt ? opt.includedDefault : true;

      if (!('addDefaultValidations' in opt))
        opt.addDefaultValidations = true;

      self.addDefaultValidations = opt.addDefaultValidations;

      // Manage non-element bound errors by creating a virtual model with a dummy element.
      self._dummyModel = {
        dummy: {}
      };
      self._dummyModel.dummy[RCC.metadataProperty] = {
        term: {
          dataType: 'rcc-dummy'
        }
      };
      self._dummyModel.dummy[RCC.metadataProperty].validation = self.init(self._dummyModel.dummy, {
        source: {
          type: 'unbound-errors'
        }
      });

      self.track(self._dummyModel);
    };

  /**
   * Add a new test. Usually, tests are added to the variable's in the view model,
   * but certain tests (such as "at least one row in subtable") do not belong to a
   * particular variable.
   *
   * @param {Function} test A test to add, will be passed to constructor of RCC.Validation.Test.
   */
  RCC.Validation.prototype.add =
    function () {
      var self = this;

      self._dummyModel.dummy[RCC.metadataProperty].validation.add.apply(self, Array.prototype.slice.call(arguments, 0));
    };

  /**
   * Track a view model, that is, include any errors in the model
   * in the property "errors".
   */
  RCC.Validation.prototype.track =
    function (vm) {
      var self = this;

      self.viewModels.push(vm);
    };

  /**
   * Creates initial validations for the given variable, including the "required" subscribable.
   * If the variable has a metadata object (RCC.metadataProperty), the result is also stored in the
   * "validation" property.
   *
   * @param x The variable.
   * @param info Information about the variable, stored in "info".
   * @returns {Object} Validation object with properties "errors" (current errors), "add" (adds a new test),
   *		"tests" (current tests), "required" (required variable or not; a ko.subscribable), and "info" (information about variable),
   *		"included" (if true, then errors for this variable are included; a ko.observable).
   */
  RCC.Validation.prototype.init =
    function (x, info) {
      var self = this;

      var obj = {};

      obj.warnings = ko.observableArray([]);
      obj.errors = ko.observableArray([]);
      obj.tests = ko.observableArray([]);
      obj.info = info;

      obj.add =
        function (test) {
          var testObj = new RCC.Validation.Test({
            warningsTo: obj.warnings,
            errorsTo: obj.errors,
            test: test,
            info: info
          });
          obj.tests.push(testObj);
          return testObj;
        };

      var metadata = x[RCC.metadataProperty];
      if (metadata) {
        metadata.validation = obj;

        if (self.addDefaultValidations) {
          var term = metadata.term;
          var hasValue = function (x) {
            return x !== undefined && x !== null;
          };

          if (term.dataType == 'datetime' && !term.includeTime)
            obj.add(new RCC.Validation.Tests.Date(x, {
              ignoreMissingValues: true
            }));
          else if (term.dataType == 'integer')
            obj.add(new RCC.Validation.Tests.Integer(x, {
              min: term.min,
              max: term.max,
              ignoreMissingValues: true
            }));
          else if (term.dataType == 'decimal')
            obj.add(new RCC.Validation.Tests.Decimal(x, {
              decimalPlaces: term.precision,
              min: term.min,
              max: term.max,
              ignoreMissingValues: true
            }));

          if (hasValue(term.maxLength))
            obj.add(new RCC.Validation.Tests.Length(x, {
              max: term.maxLength,
              treatMissingLengthAsZero: true,
              ignoreMissingValues: true
            }));

          if (hasValue(term.validCharacters))
            obj.add(new RCC.Validation.Tests.ValidCharacters(x, {
              characters: term.validCharacters,
              ignoreMissingValues: true
            }));

          /*
           * Create an observable for the "required" and "included" values. If the default value has not
           * been overwritten, we use self.requiredDefault or self.includedDefault, otherwise we use the supplied value.
           */
          ko.utils.arrayForEach(['required', 'included'],
            function (prop) {
              var val = ko.observable(undefined),
                overridden = ko.observable(false);

              obj[prop] = ko.computed({
                read: function () {
                  return overridden() ? val() : ko.utils.unwrapObservable(self[prop + 'Default']);
                },
                write: function (newValue) {
                  val(newValue);
                  overridden(true);
                }
              });
            });

          // Unable to validate booleans for missing value ("false" and NULL are stored as "null").
          if (ko.utils.arrayIndexOf(['boolean', 'rcc-dummy'], metadata.term.dataType) < 0)
            obj.add(new RCC.Validation.Tests.NotMissing(x));
        }
      }

      return obj;
    };

  /**
   * Walk through the given view models, including any subtables, and call the
   * callback for each variable (regvar and vdlist).
   *
   * @param {Array.&lt;Object&gt;} viewModels The view models to process.
   * @param {Function} callback The callback function, will be called as <code>callback(param)</code>.
   */
  RCC.Validation.prototype._forEachVariable =
    function (viewModels, callback) {
      ko.utils.arrayForEach(viewModels,
        // EDIT VALIDATION TEST
        //function processTable ( table )
        function processTable(table, tableName, tableIndex) {
          for (var prop in table) {

            if (prop.substring(0, 1) != '$' && table.hasOwnProperty(prop))
              // EDIT VALIDATION TEST
              //callback( table[prop] );
              callback(table[prop], table, tableName, tableIndex);
            // END
          }

          if (table.$vd) {
            for (var vdlist in table.$vd) {
              if (table.$vd.hasOwnProperty(vdlist))
                // EDIT VALIDATION TEST
                //callback( table.$vd[vdlist]);
                callback(table.$vd[vdlist], table, tableName, tableIndex);
              // END
            }
          }

          if (table.$$) {
            for (var subTable in table.$$) {
              if (table.$$.hasOwnProperty(subTable)) {
                // EDIT VALIDATION TEST
                //ko.utils.arrayForEach( table.$$[subTable](), processTable );

                for (var i = 0; i < table.$$[subTable]().length; i++) {
                  processTable(table.$$[subTable]()[i], subTable, i);
                }
                // END
              }
            }
          }
        });
    };


  /**
   * Get all errors in the currently tracked models.
   */
  RCC.Validation.prototype._getErrors =
    function () {
      var self = this;

      var errors = [];

      /* Patch 2013-03-12*/
      /* Check if validation is enabled */
      if (self.enabled()) {
        /* Patch 2013-03-12 End */
        self._forEachVariable(self.viewModels(),

          // EDIT VALIDATION TEST
          //function ( x )
          function (x, viewModel, viewModelName, viewModelIndex) {
            var metadata = x[RCC.metadataProperty];
            if (metadata && metadata.validation && metadata.validation.errors && ko.utils.unwrapObservable(metadata.validation.included)) {
              //errors.push(metadata.validation.errors());
              ko.utils.arrayForEach(metadata.validation.errors(), function (error) {
                error.info.source.metadata = metadata;
                if (viewModelName) {
                  error.info.source.viewModel = viewModel;
                  error.info.source.viewModelName = viewModelName;
                  error.info.source.viewModelIndex = viewModelIndex;
                }

                errors.push(error);
              });
            }
          });
        // END
        /* Patch 2013-03-12*/
        /* Check if validation is enabled */
      }
      /* Patch 2013-03-12 End */

      // Flatten the array and remove duplicates.
      return ko.utils.arrayGetDistinctValues(Array.prototype.concat.apply([], errors));
    };

  /**
   * Get all errors in the currently tracked models.
   */
  RCC.Validation.prototype._getWarnings =
    function () {
      var self = this;

      var warnings = [];

      /* Patch 2013-03-12*/
      /* Check if validation is enabled */
      if (self.enabled()) {
      /* Patch 2013-03-12 End */
      self._forEachVariable(self.viewModels(),
        // EDIT VALIDATION TEST
        //function ( x )
        function (x, viewModel, viewModelName, viewModelIndex) {
          var metadata = x[RCC.metadataProperty];
          if (metadata && metadata.validation && metadata.validation.warnings && ko.utils.unwrapObservable(metadata.validation.included)) {
            //errors.push(metadata.validation.errors());

            // Behöver skapa liknande/använda samma struktur för warnings som för errors med rcc.validation.test
            ko.utils.arrayForEach(metadata.validation.warnings(), function (warning) {
              warning.info.source.metadata = metadata;
              if (viewModelName) {
                warning.info.source.viewModel = viewModel;
                warning.info.source.viewModelName = viewModelName;
                warning.info.source.viewModelIndex = viewModelIndex;
              }

              warnings.push(warning);
            });
          }
        });
      // END
      /* Patch 2013-03-12*/
      /* Check if validation is enabled */
      }
      /* Patch 2013-03-12 End */

      // Flatten the array and remove duplicates.
      return ko.utils.arrayGetDistinctValues(Array.prototype.concat.apply([], warnings));
    };

  /**
   * Set "accessed" to true for each variable in the tracked view models.
   */
  RCC.Validation.prototype.markAllAsAccessed =
    function () {
      var self = this;

      self._forEachVariable(self.viewModels(),
        function (x) {
          var metadata = x[RCC.metadataProperty];
          if (metadata && metadata.accessed)
            metadata.accessed(true);
        });
    };

})(window['ko'], window['RCC'], window);

/* RCC/Validation/Test.js */

(function (ko, RCC, window) {

  /**
   * @license
   * Portions of this software may utilize the following copyrighted material,
   * the use of which is hereby acknowledged.
   *
   * Knockout Validate v0.1-pre
   * (c) Martin Landälv - http://mlandalv.github.com/knockout-validate/)
   * MIT license
   */

  /**
   * A validation test.
   *
   * @param {Object}opt Options.
   * @param {Array.&lt;ko.observableArray | Object&gt; | ko.observableArray | Object} opt.errorsTo
   *	Targets to update with any errors. If a given object <code>obj</code> has a property
   *	RCC.metadataProperty, with a subproperty "validation", then errors are stored in
   *	<code>obj[RCC.metadataProperty].validation.errors</code> and the test itself is added to
   *	<code>obj[RCC.metadataProperty].validation.tests</code>.
   * @param {Object} opt.info Object to be stored in the "info" property of the error.
   * @param {ko.subscribable|Function} opt.test State of the current test. If the test fails, the result (value/return value) should be of the form:
   *		<ul>
   *			<li><code>{ result: 'failed', message: 'message explaining why the test failed' }</code>, or</li>
   *			<li><code>'message explaining why the test failed'</code> if the test failed. (Convenience method.)</li>
   *		</ul>
   *		In the first case, the property <code>data</code> is also copied to the error object.
   *		If the test succeeds, the result should be
   *		<ul>
   *			<li><code>{ result: 'ok' }</code>, or</li>
   *			<li><code>undefined</code> (Convenience method.)</li>
   *		</ul>
   *		A function which is not a ko.subscribable is converted to a computed observable.
   *
   *
   * @description
   * <p>Inspired by and loosely based on Knockout Validate v0.1-pre ((c) Martin Landälv -
   * http://mlandalv.github.com/knockout-validate/), created by Martin Landälv and licensed
   * under the MIT license.</p>
   *
   * @constructor
   */
  RCC.Validation.Test =
    function (opt) {
      var self = this;

      self._disposed = ko.observable(false);

      self._error = {
        info: opt.info
      };
      self._warning = {
        info: opt.info
      }
      self._testReferences = [];
      self._targets =
        ko.utils.arrayMap([].concat(opt.errorsTo),
          function (obj) {
            var metadata = obj[RCC.metadataProperty];

            if (metadata && metadata.validation) {
              self._testReferences.push(metadata.validation.tests);
              metadata.validation.tests.push(self);
              return metadata.validation.errors;
            } else
              return obj;
          });

      self._warningTargets =
        ko.utils.arrayMap([].concat(opt.warningsTo),
          function (obj) {
            var metadata = obj[RCC.metadataProperty];

            if (metadata && metadata.validation) {
              return metadata.validation.warnings;
            } else {
              return obj;
            }
          });

      self._test = ko.isSubscribable(opt.test) ? opt.test : ko.computed({
        read: opt.test,
        disposeWhen: self._disposed
      });

      ko.computed({
        read: self._update.bind(self),
        disposeWhen: self._disposed
      });
    };

  /**
   * Disposes the test.
   */
  RCC.Validation.Test.prototype.dispose =
    function () {
      var self = this;

      self._disposed(true);
      ko.utils.arrayForEach(self._testReferences, function (target) {
        target.remove(self);
      });
      self._removeErrorFromTargets();
      self._removeWarningsFromTargets();
    };

  /**
   * Removes the error object from all targets.
   * @private
   */
  RCC.Validation.Test.prototype._removeErrorFromTargets =
    function () {
      var self = this;

      ko.utils.arrayForEach(self._targets, function (target) {
        target.remove(self._error);
      });
      delete self._error.message;
    };

  /**
   * Removes the error object from all targets.
   * @private
   */
  RCC.Validation.Test.prototype._removeWarningsFromTargets =
    function () {
      var self = this;

      ko.utils.arrayForEach(self._warningTargets, function (warning) {
        warning.remove(self._warning);
      });
      delete self._warning.message;
    };

  /**
   * Update the targets with a new test result.
   */
  RCC.Validation.Test.prototype._update =
    function () {
      var self = this;

      if (self._disposed()) {
        self._removeErrorFromTargets();
        self._removeWarningsFromTargets();
        return;
      }

      var val = self._test();

      // Normalize convenience values.
      if (typeof val == 'string')
        val = {
          result: 'failed',
          message: val
        };
      else if (typeof val == 'undefined')
        val = {
          result: 'ok'
        };
      else if (typeof val != 'object')
        throw new Error('Validation test returned bad value: ' + val);

      if (val.result == 'failed') {
        self._error.message = val.message;
        self._error.data = val.data;

        ko.utils.arrayForEach(self._targets,
          function (target) {
            target.remove(self._error);
            target.push(self._error);
          });
      } else if(val.result == 'warning') {
        self._warning.message = val.message;
        self._warning.data = val.data;

        ko.utils.arrayForEach(self._warningTargets,
          function (warning) {
            warning.remove(self._warning);
            warning.push(self._warning);
          });
      } else if (val.result == 'ok') {
        self._removeErrorFromTargets();
        self._removeWarningsFromTargets();
      } else {
        throw new Error('Validation test returned bad result: ' + val.result);
      }
    };

})(window['ko'], window['RCC'], window);

/* RCC/Validation/Tests.js */

(function (ko, RCC, window) {

  /**
   * @license
   * Portions of this software may utilize the following copyrighted material,
   * the use of which is hereby acknowledged.
   *
   * Knockout Validate v0.1-pre
   * (c) Martin Landälv - http://mlandalv.github.com/knockout-validate/)
   * MIT license
   */

  /**
   * A collection of general purpose tests, for use with RCC.Validation.Test.
   *
   * @description
   * <p>Inspired by and loosely based on Knockout Validate v0.1-pre ((c) Martin Landälv -
   * http://mlandalv.github.com/knockout-validate/), created by Martin Landälv and licensed
   * under the MIT license.</p>
   *
   * @namespace
   * @name RCC.Validation.Tests
   */
  RCC.Validation.Tests = {};

  /**
   * Check if a value is defined and non-null, and for strings optionally
   * that it also has non-zero length.
   *
   * @param x The value.
   * @param {Boolean} [disallowEmptyStrings=false] If true, then false is returned if the string is empty.
   * @returns {Boolean} True if x defined and non-null, otherwise false.
   */
  RCC.Validation.Tests._hasValue =
    function (x, disallowEmptyStrings) {
      return x !== undefined && x !== null && (!disallowEmptyStrings || typeof x != 'string' || x.length > 0);
    };

  /**
   * Returns a function which validates whether the given string,
   * of the format YYYY-MM-DD or YYYYMMDD, is a valid date.
   *
   * @param {ko.subscribable} date The date to validate.
   * @param {Object} [opt] Options.
   * @param {Boolean} [opt.ignoreMissingValues=false] If true, validation is not run unless the variable has a value.
   * @returns {Function} The validation function.
   */
  RCC.Validation.Tests.Date =
    function (date, opt) {
      if (!opt)
        opt = {};

      return function () {
        var d = date();

        if (opt.ignoreMissingValues && !RCC.Validation.Tests._hasValue(d, true))
          return {
            result: 'ok'
          };

        var parsedDate = RCC.Utils.parseYMD(d);
        if (!parsedDate)
          return {
            result: 'failed',
            message: 'Felaktigt datum (inte på formen ÅÅÅÅ-MM-DD eller ÅÅÅÅMMDD).'
          };

        var interpretedDate = RCC.Utils.ISO8601(parsedDate);

        if (interpretedDate.replace(/-/g, '') != d.replace(/-/g, ''))
          return {
            result: 'failed',
            message: 'Felaktigt datum.'
          };
        else
          return {
            result: 'ok'
          };
      };
    };

  /**
   * Validates that a given subscribable is an integer, which may
   * be prefixed by a plus or minus sign.
   *
   * @param {ko.subscribable} integer The number to validate.
   * @param {Object} [opt] Options.
   * @param {Number} [opt.min] The minimal number (inclusive).
   * @param {Number} [opt.max] The maximal number (inclusive).
   * @param {Boolean} [opt.ignoreMissingValues=false] If true, validation is not run unless the variable has a value.
   * @returns {Function} The validation function.
   */
  RCC.Validation.Tests.Integer =
    function (integer, opt) {
      if (!opt)
        opt = {};

      return function () {
        var n = integer();

        if (opt.ignoreMissingValues && !RCC.Validation.Tests._hasValue(n, true))
          return {
            result: 'ok'
          };

        if (!RCC.Validation.Tests._hasValue(n) || !n.toString().match(/^[-+]?(?:\d|[1-9]\d*)$/))
          return {
            result: 'failed',
            message: 'Ogiltigt heltal.'
          };

        var val = parseInt(n, 10);
        if (RCC.Validation.Tests._hasValue(opt.min) && val < opt.min)
          return {
            result: 'failed',
            message: 'Minsta tillåtna värde: ' + opt.min
          };

        if (RCC.Validation.Tests._hasValue(opt.max) && val > opt.max)
          return {
            result: 'failed',
            message: 'Största tillåtna värde: ' + opt.max
          };

        return {
          result: 'ok'
        };
      };
    };

  /**
   * Validates that a given subscribable is a decimal number, with a comma as
   * decimal separator, having a given number of decimal places.
   *
   * @param {ko.subscribable} decimal The number to validate.
   * @param {Object} opt Options.
   * @param {Boolean} [opt.ignoreMissingValues=false] If true, validation is not run unless the variable has a value.
   * @param {Integer} opt.decimalPlaces The number of decimal places.
   * @param {Number} [opt.min] The minimal number (inclusive).
   * @param {Number} [opt.max] The maximal number (inclusive).
   * @returns {Function} The validation function.
   */
  RCC.Validation.Tests.Decimal =
    function (decimal, opt) {
      return function () {
        var f = decimal();

        if (opt.ignoreMissingValues && !RCC.Validation.Tests._hasValue(f, true))
          return {
            result: 'ok'
          };

        /* Patch 2013-03-12*/
        /* Allow '.' and ', ' in a Decimal */
        //var re = new RegExp( '^[-+]?(?:\\d|[1-9]\\d*),\\d{' + opt.decimalPlaces + '}$' );
        var re = new RegExp('^[-+]?(?:\\d|[1-9]\\d*)([,\.]\\d{' + opt.decimalPlaces + '})?$');
        /* Patch 2013-03-12 End*/

        if (!RCC.Validation.Tests._hasValue(f) || !f.toString().match(re))
          return {
            result: 'failed',
            message: 'Ej decimaltal med ' + opt.decimalPlaces + ' decimal' + (opt.decimalPlaces == 1 ? '' : 'er') + '.'
          };
        var val = parseFloat(f.toString().replace(/,/, '.'));
        if (RCC.Validation.Tests._hasValue(opt.min) && val < opt.min)
          return {
            result: 'failed',
            message: 'Minsta tillåtna värde: ' + opt.min
          };
        if (RCC.Validation.Tests._hasValue(opt.max) && val > opt.max)
          return {
            result: 'failed',
            message: 'Största tillåtna värde: ' + opt.max
          };
        return {
          result: 'ok'
        };
      };
    };

  /**
   * Validates that a given subscribable has a property "length",
   * possibly inherited, within the given bounds.
   *
   * @param {ko.subscribable} obj The number to validate.
   * @param {Object} [opt] Options.
   * @param {Number} [opt.min] The minimal length.
   * @param {Number} [opt.max] The maximal length.
   * @param {Boolean} [opt.treatMissingLengthAsZero=false] If a missing "length" property should be treated as 0.
   * @param {Boolean} [opt.ignoreMissingValues=false] If true, validation is not run unless the variable has a value.
   * @returns {Function} The validation function.
   */
  RCC.Validation.Tests.Length =
    function (obj, opt) {
      if (!opt)
        opt = {};

      return function () {
        var o = obj();

        if (opt.ignoreMissingValues && !RCC.Validation.Tests._hasValue(o, true))
          return {
            result: 'ok'
          };

        var len;
        if (!RCC.Validation.Tests._hasValue(o) || typeof o.length == 'undefined') {
          if (opt.treatMissingLengthAsZero)
            len = 0;
          else
            return {
              result: 'failed',
              message: 'Längd saknas.'
            };
        } else
          len = o.length;

        if (RCC.Validation.Tests._hasValue(opt.min) && len < opt.min)
          return {
            result: 'failed',
            message: 'Minsta tillåtna längd: ' + opt.min
          };

        if (RCC.Validation.Tests._hasValue(opt.max) && len > opt.max)
          return {
            result: 'failed',
            message: 'Största tillåtna längd: ' + opt.max
          };

        return {
          result: 'ok'
        };
      };
    };

  /**
   * Validates that a given subscribable is a string containing
   * only the given characters.
   *
   * @param {ko.subscribable} obj The number to validate.
   * @param {Object} opt Options.
   * @param {Number} opt.characters The only characters that may occur.
   * @param {Boolean} [opt.ignoreMissingValues=false] If true, validation is not run unless the variable has a value.
   * @returns {Function} The validation function.
   */
  RCC.Validation.Tests.ValidCharacters =
    function (obj, opt) {
      return function () {
        var s = obj();

        if (opt.ignoreMissingValues && !RCC.Validation.Tests._hasValue(s, true))
          return {
            result: 'ok'
          };

        if (typeof s != 'string')
          return {
            result: 'failed',
            message: 'Inte en textsträng.'
          };

        for (var i = 0; i < s.length; i++)
          if (opt.characters.indexOf(s[i]) < 0)
            return {
              result: 'failed',
              message: 'Otillåtet tecken: ' + s[i]
            };

        return {
          result: 'ok'
        };
      };
    };

  /**
   * Verifies that the given subscribable either is not required,
   * or has a value.
   *
   * @param {ko.subscribable} obj The object to validate.
   */
  RCC.Validation.Tests.NotMissing =
    function (obj) {
      return function () {
        if (!obj[RCC.metadataProperty].validation.required())
          return {
            result: 'ok'
          };

        var val = obj();
        if (!RCC.Validation.Tests._hasValue(val) || (typeof val == 'string' && val.length == 0))
          return {
            result: 'failed',
            message: 'Värde saknas'
          };
        else
          return {
            result: 'ok'
          };
      };
    };


})(window['ko'], window['RCC'], window);
