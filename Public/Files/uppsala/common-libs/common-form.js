$(document).ready(function () {
    /**
	 *  VARIABLER
	 */

    // skapa vymodel med validering
    var validering = new RCC.Validation();
    var vm = new RCC.ViewModel({ validation: validering });
    window.vm = vm;

    // deaktivera runtime validering
    vm.$validation.enabled(false);

    // tracker för element med aktuellt fokus
    var lastFocused;
    $('#rcc-form').on({
        'focus': function () {
            lastFocused = $(this);
        }
    }, 'input, select');

    $("[data-toggle=popover]").popover();

    $('body').on('click', function (e) {
        $('[data-toggle="popover"]').each(function (index, item, i) {

            var binding = $(item).data().bind;

            if (binding && binding.toLowerCase().indexOf("rcc-helpme:") >= 0) {
                //the 'is' for buttons that trigger popups
                //the 'has' for icons within a button that triggers a popup
                // the last custom addition of select "[for=this.id] > *" is to capture clicks on the label itself (the text) or the child element (the icon)
                if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0 && !$("[helpId=" + this.id + "]").is(e.target) && !$("[helpId=" + this.id + "] > *").is(e.target)) {
                    $(this).popover('hide');
                }
            }
        });
    });

    $('#rcc-form').on('click', 'span span.rcc-help', function (e) {
        e.stopPropagation();
    });

    // variabler för att hålla koll på om formuläret har laddat färdigt
    vm.initialized = ko.observable(false);

    // variabel för att bocka för/bocka av alla include kryssrutor
    vm.include = ko.observable(true);

    // variabel för att visa/dölja alla gömda delar av formuläret
    vm.forceShow = ko.observable(false);

    // variabler och logik för formulärmiljö
    vm.normalErrand = ko.observable(false);
    vm.registerPost = ko.observable(false);
    vm.originalHandling = ko.observable(false);
    vm.externalInca = ko.observable(false);

    var loading = $('#progressModal');

    if (!inca.errand)
        vm.registerPost(true);
    else if (inca.form.isReadOnly)
        vm.originalHandling(true);
    else
        vm.normalErrand(true);

    if (typeof (inca.getBaseUrl) == "undefined")
        vm.externalInca(true);

    if (vm.registerPost()) {
        vm.forceShow(true);
    }

    // variabel för att aktivera tumörpanel
    vm.activateTumorPanel = ko.observable(false);

    // objekt för standardvariabler
    var commonVariables = { 'kompl': '', 'sjukhus': '', 'sjukkod': '', 'klinkod': '', 'initieratav': '', 'lkf': '', 'rappdat': '', 'rapportor': '', 'lakare': '' };
    var promises = [];

    //setCommonVariables(this,commonVariables);

    /**
	 *  VALIDERING, KOMPLETTERING OCH POSTFUNKTIONER
	 */

    /**
	 * Callback funktion för validering
	 *
	 * @method validateData
	 */
    vm.validateData = function () {
        if (inca.errand.action) {
            var actionsWithoutValidation = ['1', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12',
                '13', '14', '15', '16', '17', '18', '19', '20', '21', '22',
                '23', '24', '25', '26', '27', '28', '29', '39', '41', '46', '78'];

            var actionsWithConnect = ['2', '4', '23', '28', '29'];
            var action;
            var canIncaDialog, okppladDialog, warningDialog;

            if (vm.externalInca())
                if (vm.externalAction)
                    action = vm.externalAction;
                else
                    action = '34';
            else
                action = inca.errand.action.val();

            // om tomt val.
            if (action == '') {
                return false;
            }

            if (action == 6) {
                if (!confirm(C_WARNING_AVBRYT))
                    return false;
            }

            if (action == 46) {
                if (!confirm(C_WARNING_MAKULERA))
                    return false;
            }

            // om åtgärd med koppla
            if ($.inArray(action, actionsWithConnect) >= 0) {
                canIncaDialog = (vm.activateTumorPanel() && !vm.tumor()) ? vm.createConfirmDialog({ title: 'Okopplad post mot cancerregistret', posY: '0', content: 'Är du säker på att du vill spara posten okopplad mot cancerregistret på INCA?' }) : null;
                okppladDialog = (inca && inca.errand && inca.errand.errandInformation && inca.errand.errandInformation()[0] == "Ej kopplad") ? vm.createConfirmDialog({ title: 'Okopplad post mot befintlig registerpost', posY: '0', content: C_WARNING_KOPPLA }) : null;
            }

            // formulärspecifika åtgärder
            if (typeof actionsToDoBeforeValidate == 'function') {
                if (!actionsToDoBeforeValidate(vm, action)) {
                    return false;
                }
            }

            var canIncaConfirm = canIncaDialog != null ? vm.showModalDialog(canIncaDialog) : $.Deferred().resolve();
            var okoppladConfirm = okppladDialog != null ? vm.showModalDialog(okppladDialog) : $.Deferred().resolve();

            // om åtgärd utan validering
            if ($.inArray(action, actionsWithoutValidation) >= 0) {
                $.when(canIncaConfirm, okoppladConfirm).then(function () {
                    return true;
                }, function () {
                    return false;
                });
            // annars, formuläret ska valideras
            } else {
                // aktivera runtime validering
                vm.$validation.enabled(true);
                // hämta valideringsfel
                var errors = validering.errors();
                var warnings = validering.warnings();

                // hämta formulärspecifika valideringsfel
                if (!vm[commonVariables.kompl]()) {
                    var customErrors = vm.generateCustomViewModelErrors(vm);
                    if (customErrors) {
                        errors = errors.concat(customErrors);
                    }
                }

                // fel finns, visa och avbryt åtgärd
                if (errors.length > 0) {
                    // Visa alla fel och valideringsfel
                    validering.markAllAsAccessed();

                    // visa felmeddelande
                    vm.showValidationDialog(vm.generateErrorMessage(errors));

                    // deaktivera runtime validering
                    vm.$validation.enabled(false);

                    return false;
                } else {
                    // visa eventuella varningar innan inskick av formulär
                    warningDialog = (warnings.length > 0) ? vm.createConfirmDialog({ title: 'Formuläret innehåller ' + warnings.length + (warnings.length > 1 ? ' varningar' : ' varning'), posY: '0', content: generateWarningMessage(warnings) }) : null;

                    var warningConfirm = warningDialog != null ? vm.showModalDialog(warningDialog) : $.Deferred().resolve();

                    $.when(canIncaConfirm, okoppladConfirm, warningConfirm).then(function () {
                        // inga fel, skicka
                        vm.$validation.enabled(false);
                        return vm.actionsToDoBeforePost();
                    }, function () {
                        vm.$validation.enabled(false);
                        return false;
                    });
                }

            }
        }
    };

    // tilldela callbackfunktioner för validering och komplettering
    if (vm.normalErrand() && !vm.externalInca()) {
        // Temp fix for import data bug in INCA
        inca.off('validation');
        inca.off('comparison');

        inca.on('validation', vm.validateData);
        inca.on('comparison', vm.handleCompareResult);
    }

    /**
	 * Funktion för att generera ett valideringsfelmedelande
	 *
	 * @method generateErrorMessage
	 * @param {Object} errors Array med samtliga fel
	 * @return {String} felmeddelande
	 */
    vm.generateErrorMessage = function (errors) {
        // Initiera felmeddelande
        var message = "<p><b>Formuläret innehåller " + errors.length + " fel.</b></p><ul style='margin:0; padding:5px; list-style:none;'>";

        // Skapa array av samtliga nivåtitlar och sortera ut unika
        var viewModelNames = ko.utils.arrayMap(errors, function (item) { return item.info.source.viewModelName });
        var distinctViewModelNames = ko.utils.arrayGetDistinctValues(viewModelNames);

        // Gå igenom samtliga unika nivåtitlar
        ko.utils.arrayForEach(distinctViewModelNames, function (viewModelName) {
            // Skapa array av fel för aktuell nivåtitel
            var filteredErrorsByViewModelName = ko.utils.arrayFilter(errors, function (error) {
                return (error.info.source.viewModelName === viewModelName);
            });

            // Skapa array av samtliga index för aktuell nivåtitel och sortera ut unika
            var viewModelsIndexes = ko.utils.arrayMap(filteredErrorsByViewModelName, function (item) { return item.info.source.viewModelIndex })
            var distinctViewModelsIndexes = ko.utils.arrayGetDistinctValues(viewModelsIndexes);

            // Gå igenom samtliga unika index för aktuell nivåtitel
            ko.utils.arrayForEach(distinctViewModelsIndexes, function (viewModelIndex) {
                // Skapa array av fel för aktuellt index för aktuell nivåtitel
                var filteredErrorsByViewModelIndex = ko.utils.arrayFilter(filteredErrorsByViewModelName, function (error) {
                    return (error.info.source.viewModelIndex === viewModelIndex);
                });

                // Sortera fel baserat på sortOrder
                filteredErrorsByViewModelIndex.sort(function (a, b) {
                    var va = typeof a.info.source.metadata.regvar != 'undefined' ? a.info.source.metadata.regvar.sortOrder : 0;
                    var vb = typeof b.info.source.metadata.regvar != 'undefined' ? b.info.source.metadata.regvar.sortOrder : 0;

                    return va == vb ? 0 : (va < vb ? -1 : 1);
                });


                var first = true;
                // Gå igenom samtliga fel för aktuellt index för aktuell nivåtitel
                ko.utils.arrayForEach(filteredErrorsByViewModelIndex, function (error) {
                    // om första fel skriv ut nivåtitel
                    if (first) {
                        // om nivå != root
                        if (viewModelName && viewModelIndex) {
                            var viewModel = error.info.source.viewModel;
                            // hämta egendefinerad nivåtitel
                            var customViewModelName = vm.getCustomViewModelName(viewModel, viewModelName, viewModelIndex);
                            // om egendefinerad nivåtitel != null skriv ut annars använd generell
                            if (customViewModelName)
                                message += "<li style='margin-left:10px; padding-top: 10px; padding-bottom: 10px;'><b>" + customViewModelName + "</b></li>";
                            else
                                message += "<li style='margin-left:10px; padding-top: 10px; padding-bottom: 10px;'><b>" + viewModelName + " " + (viewModelIndex + 1) + "</b></li>";
                        }
                        first = false;
                    }

                    // skriv ut felmeddelande
                    message += "<li style='margin-left:10px;'>" + ((typeof error.info.source.metadata.regvar != 'undefined') ? error.info.source.metadata.regvar.compareDescription : error.info.source.metadata.term.description) +
                        " (" + error.info.source.metadata.regvarName + "):</li><li style='margin-left:20px; padding-bottom: 5px;'>" + error.message + "</li>";
                });
            });

        });

        message += "</ul>";
        return message;
    };

    /**
	 * Funktion för att samla formulärspecifika fel
	 *
	 * @method generateCustomViewModelErrors
	 * @param {Object} viewModel referens till nivå
	 * @return {Array} Array med formulärspecifika fel
	 */
    vm.generateCustomViewModelErrors = function (viewModel) {
        // hämta formulärspecifika fel för akutell nivå
        var errorArray = vm.getCustomViewModelErrors(viewModel, null, null);

        // gå igenom undernivåer
        for (var table in viewModel.$$) {
            // gå igenom samtliga index för aktuell undernivå
            for (var i = 0; i < viewModel.$$[table]().length; i++) {
                // hämta formulärspecifika fel för akutellt index och lägg till array med formulärspecifika fel
                errorArray = errorArray.concat(vm.getCustomViewModelErrors(viewModel.$$[table]()[i], table, i));
                vm.generateCustomViewModelErrors(viewModel.$$[table]()[i]);
            }
        }

        return errorArray;
    };

    /**
	 * Skapa ett formulärspecifikt ("custom") felobject
	 *
	 * @method createCustomViewModelError
	 * @param {Object} viewModel referens till nivå
	 * @param {String} viewModelName nivåtitel
	 * @param {Integer} viewModelIndex nivåindex
	 * @param {String} titel feltitel
	 * @param {String} message felmeddelande
	 * @param {String} sortOrder sorteringsvärde
	 * @return {Object} felObject
	 */
    vm.createCustomViewModelError = function (viewModel, viewModelName, viewModelIndex, title, message, sortOrder) {
        var errorObj = new Object();

        errorObj.info = new Object();
        errorObj.info.source = new Object();
        errorObj.info.source.viewModel = viewModel;
        errorObj.info.source.viewModelName = viewModelName;
        errorObj.info.source.viewModelIndex = viewModelIndex;

        errorObj.info.source.metadata = new Object();
        errorObj.info.source.metadata.regvar = new Object();
        errorObj.info.source.metadata.regvar.compareDescription = title;
        errorObj.info.source.metadata.regvar.sortOrder = sortOrder;

        errorObj.message = message;

        return errorObj;
    };


    /**
	 * Funktion för att definera egna nivåtitlar
	 *
	 * @method getCustomViewModelName
	 * @param {Object} viewModel referens till nivå
	 * @param {String} viewModelName nivåtitel
	 * @param {Integer} viewModelIndex nivåindex
	 * @return {String} nivåtitel
	 */
    vm.getCustomViewModelName = function (viewModel, viewModelName, viewModelIndex) {
        if (typeof getCustomViewModelName == 'function')
            return getCustomViewModelName(vm, viewModel, viewModelName, viewModelIndex);
    };


    /**
	 * Funktion för att definera formulärspecikfika fel för ett index för aktuell nivå
	 *
	 * @method getCustomViewModelErrors
	 * @param {Object} viewModel referens till nivå
	 * @param {String} viewModelName nivåtitel
	 * @param {Integer} viewModelIndex nivåindex
	 * @return {Array} Array med formulärspecifika fel
	 */
    vm.getCustomViewModelErrors = function (viewModel, viewModelName, viewModelIndex) {
        if (typeof getCustomViewModelErrors == 'function')
            return getCustomViewModelErrors(vm, viewModel, viewModelName, viewModelIndex);
    };


    /**
	 * Funktion för åtgärder innan formuläråtgärd utförs.
	 *
	 * @method actionsToDoBeforePost
	 */
    vm.actionsToDoBeforePost = function () {
        var action;
        var returnValue = true;

        if (vm.externalInca())
            if (vm.externalAction)
                action = vm.externalAction;
            else
                action = '34';
        else
            action = inca.errand.action.val();

        // om åtgärd är "Klar", "Klar sänd till OC" eller "Spara i register"
        if (action == 2 || action == 34 || action == 38) {
            // om inget inrapporteringsdatum är satt
            if (!vm[commonVariables.rappdat]())
                vm[commonVariables.rappdat](getDagensDatum());
            // om inrapportör och ingen inrapportör är satt
            if (!inca.user.role.isReviewer && !vm[commonVariables.rapportor]()) {
                var reporterName = inca.user.firstName + " " + inca.user.lastName;
                vm[commonVariables.rapportor](reporterName);
            }
        }

        // om åtgärd är "Klar sänd till OC" eller "Spara i register"
        if (action == 34 || action == 38) {
            // sätt inrapporterande enhet
            // enhetsnamn
            vm[commonVariables.sjukhus](inca.user.position.fullNameWithCode);
            // sjukhuskod
            vm[commonVariables.sjukkod](getHospitalCode(inca.user.position.fullNameWithCode));
            // klinikkod
            vm[commonVariables.klinkod](getClinicCode(inca.user.position.fullNameWithCode));
        }

        // formulärspecikfika åtgärder
        if (typeof actionsToDoBeforePost == 'function')
            returnValue = actionsToDoBeforePost(vm);

        return returnValue;
    };


    /**
	 * Callback funktion för komplettering
	 *
	 * @method handleCompareResult
	 * @param {Object} data referens till vymodell
	 */
    vm.handleCompareResult = function (data) {
        var shortName;

        for (shortName in data) {
            // Fyll inca.form.data med resultatet av jämförelsen
            //inca.form.data.regvars[shortName].value = data[shortName].value;
            //vm[shortName](data[shortName].value);

            // include motsvarar kryssrutorna för granskande roller
            if (inca.user.role.isReviewer) {
                //inca.form.data.regvars[shortName].include = data[shortName].include;
                vm[shortName].rcc.include(data[shortName].include);
            }
        }
    };

    /**
	 *  INITIERINGSFUNKTIONER
	 */

    /**
	 * Funktion för åtgärder vid initiering av formulär.
	 *
	 * @method runFunctionsOnLoad
	 */
    vm.runFunctionsOnLoad = function () {
        loading.modal({ backdrop: 'static', keyboard: false }).on('hidden.bs.modal', function () {
            $(this).data('bs.modal', null);
        });

        /* TO BE DELETED */
        var helpImgSrc = '../../Public/Files/uppsala/common-libs/icons/info.png';
        var progressbarImgSrc = '../../Public/Files/uppsala/common-libs/icons/progressbar.gif'

        $('img.rcc-help-icon').each(function () {
            $(this).attr('src', helpImgSrc);
        });

        $('img.rcc-progressbar-icon').each(function () {
            $(this).attr('src', progressbarImgSrc);
        });
        /* TO BE DELETED END */


        // sätt standardvariabler
        vm.setCommonVariables();

        // sätt lokala variabler
        vm.setLocalVariables();

        promises.push(vm.loadTemplates());

        // kontrollera äkta inca
        if (vm.normalErrand()) {
            if (!vm.externalInca()) {
                // kopiera reciver till inrapporterande sjukhus vid ändring
                if (inca.errand.status.val() == C_STATUS_MONITOR) {
                    inca.errand.receiver.change(function () {
                        if (inca.errand.receiver.val()) {
                            var org = inca.errand.receiver.selectedText();
                            vm[commonVariables.sjukhus](org);
                            vm[commonVariables.sjukkod](getHospitalCode(org));
                            vm[commonVariables.klinkod](getClinicCode(org));
                        }
                    });
                }
            }
            // kopiera systemvariabler
            vm.copySystemVariables();

            // ange speciell validering och ej obligatoriska värden
            vm.setCustomValidation();

            // ladda beräknad validering
            vm.setComputedValidation();
        }

        // sätt lokala funktioner
        vm.setFunctions();

        // sätt lokala subscribers
        vm.setSubscribers();

        // ladda värdedomäner
        vm.loadVD();

        // modifiera listor
        vm.modifyLists();

        // formulärspecikfika åtgärder
        if (typeof runFunctionsOnLoad == 'function')
            runFunctionsOnLoad(this);
    };


    /**
	 * Funktion för att sätta standardvariabler.
	 *
	 * @method setCommonVariables
	 */
    vm.setCommonVariables = function () {
        // formulärspecikfika åtgärder
        if (typeof setCommonVariables == 'function')
            setCommonVariables(this, commonVariables);
    };


    /**
	 * Funktion för att sätta lokala variabler.
	 *
	 * @method setLocalVariables
	 */
    vm.setLocalVariables = function () {
        // formulärspecikfika åtgärder
        if (typeof setLocalVariables == 'function') {
            setLocalVariables(this);
        }
    };

    /**
	 *
	 *
	 * @method loadTemplates
	 * @param {Object} value Värde
	 */
    vm.loadTemplates = function () {
        return new Promise(function (resolve, reject) {
            var promise = $.get('../../Public/Files/uppsala/common-libs/templates.html', function (templates) {
                $('body').append('<div style="display: none">' + templates + '</div>');
            });

            promise.fail(function (err) {
                reject(err);
            });

            promise.done(function (data) {
                resolve(data);
            });
        });
    };


    /**
	 * Funktion för åtgärder efter initiering av formulär.
	 *
	 * @method runFunctionsAfterLoad
	 */
    vm.runFunctionsAfterLoad = function () {
        // om reviwer, ladda tumörpanel
        if (inca.user.role.isReviewer && vm.activateTumorPanel())
            vm.loadTumorPanel();

        // formulärspecikfika åtgärder
        if (typeof runFunctionsAfterLoad == 'function')
            runFunctionsAfterLoad(this);
    };


    /**
	 * Funktion för slutgiltiga åtgärder efter initiering av formulär.
	 *
	 * @method runFunctionsAfterLoadComplete
	 */
    vm.runFunctionsAfterLoadComplete = function () {
        // flagga formuläret som färdig initilaizerat
        vm.initialized(true);

        // visa formuläret
        $('#rcc-content').removeClass('rcc-hidden');

        // fokusera på första element
        $('.rcc-focusfirst').focus();

        if (inca.user.role.isReviewer && vm.normalErrand()) {
            // varning om komplettering
            if (vm[commonVariables.kompl]())
                vm.showModalDialog({ title: "Information", content: C_WARNING_KOMPLETTERING });
        }

        // formulärspecikfika åtgärder
        if (typeof runFunctionsAfterLoadComplete == 'function')
            runFunctionsAfterLoadComplete(this);

        loading.modal('hide');
    };


    /**
	 * Funktion för att ladda tumörpanel
	 *
	 * @method runFunctionsAfterLoad
	 */
    vm.loadTumorPanel = function () {
        // formulärspecikfika åtgärder
        if (typeof loadTumorPanel == 'function')
            loadTumorPanel(this);
    };


    /**
	 * Funktion för att ladda värdedomäner
	 *
	 * @method loadVD
	 */
    vm.loadVD = function () {
        // formulärspecikfika åtgärder
        if (typeof loadVD == 'function')
            loadVD(this, promises);
    };


    /**
	 * Funktion för att justera valideringar
	 *
	 * @method setCustomValidation
	 */
    vm.setCustomValidation = function () {
        // tumörvärdedomän
        if (typeof vm.tumor == 'function')
            notRequired(vm.tumor);

        // formulärspecikfika åtgärder
        if (typeof setCustomValidation == 'function')
            setCustomValidation(this);

    };


    /**
	 * Funktion för att kopiera systemvariabler
	 *
	 * @method copySystemVariables
	 */
    vm.copySystemVariables = function () {
        // initierad av
        if (typeof vm[commonVariables.initieratav] == 'function') {
            if (!vm[commonVariables.initieratav]())
                vm[commonVariables.initieratav](inca.user.firstName + " " + inca.user.lastName);
        }

        // LKF vid diagnos (endast vid anmälan)
        if (typeof vm[commonVariables.lkf] == 'function') {
            if (!vm[commonVariables.lkf]())
                vm[commonVariables.lkf](vm.$env._LKF);
        }

        // formulärspecikfika åtgärder
        if (typeof copySystemVariables == 'function')
            copySystemVariables(this);
    };


    /**
	 * Funktion för att modifiera listor
	 *
	 * @method modifyLists
	 */
    vm.modifyLists = function () {
        // formulärspecikfika åtgärder
        if (typeof modifyLists == 'function')
            modifyLists(this);
    };


    /**
	 * Funktion för att sätta beräknad validering
	 *
	 * @method setComputedValidation
	 */
    vm.setComputedValidation = function () {
        //Slå av/på monitorkryssrutor
        /* KOLLA */
        ko.computed(function () {
            if (inca.user.role.isReviewer) {
                if (!vm[commonVariables.kompl]()) {
                    compareNone(vm);
                } else {
                    compareAll(vm);
                }
            }
        });
        /* KOLLA END */

        // formulärspecikfika åtgärder
        if (typeof setComputedValidation == 'function')
            setComputedValidation(this);
    };


    /**
	 * Funktion för att sätta lokala funktioner
	 *
	 * @method setFunctions
	 */
    vm.setFunctions = function () {
        // formulärspecikfika åtgärder
        if (typeof setFunctions == 'function')
            setFunctions(this);
    };


    /**
	 * Funktion för att sätta lokala subscribers
	 *
	 * @method setSubscribers
	 */
    vm.setSubscribers = function () {
        //Slå av/på inluderingskryssrutor
        vm.include.subscribe(function (newValue) {
            if (newValue == 0) {
                includeNone(vm);
            }
            else {
                includeAll(vm);
            }
        });

        // formulärspecikfika åtgärder
        if (typeof setSubscribers == 'function')
            setSubscribers(this);
    };

    vm.runFunctionsOnLoad();

    /**
	 * Efter alla promises är lösta, gå vidare.
	 */
    Promise.all(promises).then(function () {
        ko.applyBindings(vm);
        vm.runFunctionsAfterLoad();
        vm.runFunctionsAfterLoadComplete();
    });

    /**
	 *  Funktion för att visa valideringsrutan
	 *
	 *  @method showValidationDialog
	 *  @param {String} validationErrors en sträng med alla valideringsfel
	 *  @return null
	 */
    vm.showValidationDialog = function (validationErrors) {
        var $validationModal = vm.showModalDialog({ title: 'Validering', content: validationErrors, posY: '0', elementWidth: '', actionButtons: [{ text: 'Ok', cssClass: 'btn-primary', action: function () { $('.rcc-invalid:first').focus(); } }] });
    };

    /**
	 *  Funktion för att visa en confirmation dialog
	 *
	 *  @method showConfirmDialog
	 *  @param {Object} object JSON-objekt innehållande information som ska visas mm
	 *  @return {Object} Deferred object
	 */
    vm.showConfirmDialog = function (object) {
        var def = $.Deferred();
        var title = 'Bekräfta',
            content = '',
            posY = (lastFocused ? lastFocused.offset().top : '0');

        if (object) {
            if (object.title != undefined)
                title = object.title;
            if (object.content != undefined)
                content = object.content;
            if(object.posY != undefined)
                posY = object.posY;
        }
        else
            object = {};

        return vm.showModalDialog({
            returnValue: def,
            title: title, content: content, elementWidth: '', posY: posY,
            actionButtons: [{ text: 'Ja', cssClass: 'btn-success', action: function () { def.resolve(); return def.promise(); } },
            { text: 'Nej', cssClass: 'btn-danger', action: function () { def.reject(); return def.promise(); } }]
        });

    };

    /**
	 *  Funktion för att visa en confirmation dialog
	 *
	 *  @method createConfirmDialog
	 *  @param {Object} object JSON-objekt innehållande information som ska visas mm
	 *  @return {Object} Deferred object
	 */
    vm.createConfirmDialog = function (dialog) {
        var def = $.Deferred();
        var title = 'Bekräfta',
            content = '',
            posY = (lastFocused ? lastFocused.offset().top : '0');

        if (dialog) {
            if (dialog.title != undefined)
                title = dialog.title;
            if (dialog.content != undefined)
                content = dialog.content;
            if(dialog.posY != undefined)
                posY = dialog.posY;
        }
        else
            dialog = {};

        return {
            returnValue: def,
            title: title, content: content, elementWidth: '', posY: posY,
            actionButtons: [{ text: 'Ja', cssClass: 'btn-success', action: function () { def.resolve(); return def.promise(); } },
            { text: 'Nej', cssClass: 'btn-danger', action: function () { def.reject(); return def.promise(); } }]
        };

    };


    /**
    * Funktion för att skapa en modal dialogruta
    * Dialogrutan kan också användas med deferred objects
    * @method showModalDialog
    * @param {Object} object JSON-objekt innehållande information som ska visas mm
    * @return {Object} dialogruta som JQueryobjekt, alt returnValue
    */
    vm.showModalDialog = function (object) {
        // Lokala variabler, en del med defaultvärden
        var title = '',
            content = '',
            defaultAction = function () { $(d).hide(); },
            posY = (lastFocused ? lastFocused.offset().top : '0'),
            elementWidth = 'modal-sm',
            defaultButtonText = 'OK',
            defaultBtnClassName = 'btn-primary';
        // Läser in data från inskickat objekt 
        if (object) {
            if (object.title != undefined)
                title = object.title;
            if (object.content != undefined)
                content = object.content;
            if (object.posY != undefined)
                posY = object.posY;
            if (object.elementWidth != undefined)
                elementWidth = object.elementWidth;
        }
        else
            object = {};

        // Skapa element
        d = document.createElement('div');
        modalDialog = document.createElement('div');
        modalContent = document.createElement('div');
        modalHeaderButton = document.createElement('button');
        modalHeader = document.createElement('div');
        modalBody = document.createElement('div');
        modalFooter = document.createElement('div');
        h4 = document.createElement('h4');

        // Manipulera element och skapa DOM-struktur
        // *****************************************
        $(h4).addClass('modal-title')
            .html(title);
        $(modalHeaderButton).addClass('close')
            .attr('data-dismiss', 'modal')
            .html('&times')
            .on('click', function () { $(d).hide(); });
        $(modalHeader).addClass('modal-header')
            .append($(modalHeaderButton))
            .append($(h4));
        $(modalBody).addClass('modal-body')
            .html(content);

        // Om vi inte har skickat med egna knappar ska vi skapa en standardknapp
        if (!object.actionButtons || object.actionButtons.length == 0) {
            object.actionButtons = [];
            object.actionButtons.push({ text: defaultButtonText, action: defaultAction, cssClass: defaultBtnClassName });
        }

        // Vi loopar på alla actionbuttons som finns och lägger till dem i dialogen
        for (var i = 0; i < object.actionButtons.length; i++) {
            modalFooterButton = document.createElement('button');
            // Efter primary dvs 0 ska vi visa normala knappar, OM inget särskilt angivits i cssClass för resp knapp
            if (i >= 1) defaultBtnClassName = 'btn-default';
            $(modalFooterButton).addClass('btn ' + (object.actionButtons[i].cssClass ? object.actionButtons[i].cssClass : defaultBtnClassName))
                .attr('data-dismiss', 'modal')
                .html((object.actionButtons[i].text ? object.actionButtons[i].text : ''))
                .appendTo(modalFooter)
                .on('click', (object.actionButtons[i].action ? object.actionButtons[i].action : ''));
        }


        $(modalFooter).addClass('modal-footer');
        $(modalContent).addClass('modal-content')
            .append($(modalHeader))
            .append($(modalBody))
            .append($(modalFooter));
        $(modalDialog).addClass('modal-dialog ' + elementWidth)
            .append($(modalContent))

        $(d).addClass('modal fade')
            .css('top', posY)
            .append($(modalDialog))
            .attr('role', 'dialog')
            .appendTo($('body'))
            .modal({ show: 'true', backdrop: 'static', keyboard: false })
            .on('hide.bs.modal', function () { $(this).remove(); });
        // *****************************************


        // Returnera endera ett returobjekt, eller dialogrutan i sig
        if (object.returnValue) {
            // Vi kontrollerar om vi har ett deferred object
            if (object.returnValue.promise != 'function')
                return object.returnValue.promise();
            else
                // Annars returnera returobjektet i sig
                return object.returnValue
        }
        else
            //Annars returnera dialogrutan
            return $(d);
    }

});