new IncaOffline(
    {
        emulate:
        {
    "inca": {
        "scriptSupport": {},
        "services": {
            "infoText": {},
            "report": {},
            "viewTemplate": {},
            "logging": {},
            "documentation": {
                "registerDocumentation": {},
                "formDocumentation": {},
                "registerVariableDocumentation": {}
            }
        },
        "user": {
            "id": 6855,
            "firstName": "Jonas",
            "lastName": "Celander Guss",
            "username": "oc2gujo",
            "email": "jonas.celander.guss@akademiska.se",
            "phoneNumber": null,
            "role": {
                "id": 9,
                "name": "Inrapportör (Kvalitetsregister)",
                "isReviewer": false
            },
            "position": {
                "id": 36,
                "name": "Medicinkliniken",
                "code": "101",
                "fullNameWithCode": "OC Demo (0) - SU/Sahlgrenska (500010) - Medicinkliniken (101)"
            },
            "region": {
                "id": 7,
                "name": "Region Demo"
            },
            "previousLogin": "2017-05-22T08:45:32.577Z"
        },
        "serverDate": "2017-05-22T11:49:32.952Z",
        "errand": {
            "status": {
                "0": {},
                "length": 1,
                "prevObject": {
                    "0": {},
                    "length": 1,
                    "prevObject": {
                        "0": {
                            "jQuery17203742151368481017": 75
                        },
                        "context": {
                            "jQuery17203742151368481017": 75
                        },
                        "length": 1
                    },
                    "context": {
                        "jQuery17203742151368481017": 75
                    },
                    "selector": "[data-errand-panel]"
                },
                "context": {
                    "jQuery17203742151368481017": 75
                },
                "selector": "[data-errand-panel] [data-old-id=OverviewErrandHandler_txtState]"
            },
            "comment": {
                "0": {
                    "jQuery17203742151368481017": 90
                },
                "length": 1,
                "prevObject": {
                    "0": {},
                    "length": 1,
                    "prevObject": {
                        "0": {
                            "jQuery17203742151368481017": 75
                        },
                        "context": {
                            "jQuery17203742151368481017": 75
                        },
                        "length": 1
                    },
                    "context": {
                        "jQuery17203742151368481017": 75
                    },
                    "selector": "[data-errand-panel]"
                },
                "context": {
                    "jQuery17203742151368481017": 75
                },
                "selector": "[data-errand-panel] [data-old-id=OverviewErrandHandler_txtComments]"
            },
            "action": {
                "0": {
                    "0": {},
                    "1": {},
                    "2": {},
                    "3": {},
                    "4": {},
                    "5": {},
                    "jQuery17203742151368481017": 100
                },
                "length": 1,
                "prevObject": {
                    "0": {},
                    "length": 1,
                    "prevObject": {
                        "0": {
                            "jQuery17203742151368481017": 75
                        },
                        "context": {
                            "jQuery17203742151368481017": 75
                        },
                        "length": 1
                    },
                    "context": {
                        "jQuery17203742151368481017": 75
                    },
                    "selector": "[data-errand-panel]"
                },
                "context": {
                    "jQuery17203742151368481017": 75
                },
                "selector": "[data-errand-panel] [data-old-id=OverviewErrandHandler_ddEvent]"
            },
            "receiver": {
                "0": {
                    "0": {},
                    "jQuery17203742151368481017": 102
                },
                "length": 1,
                "prevObject": {
                    "0": {},
                    "length": 1,
                    "prevObject": {
                        "0": {
                            "jQuery17203742151368481017": 75
                        },
                        "context": {
                            "jQuery17203742151368481017": 75
                        },
                        "length": 1
                    },
                    "context": {
                        "jQuery17203742151368481017": 75
                    },
                    "selector": "[data-errand-panel]"
                },
                "context": {
                    "jQuery17203742151368481017": 75
                },
                "selector": "[data-errand-panel] [data-old-id=OverviewErrandHandler_ddReceiver]"
            },
            "submitValidationCallback": null
        },
        "form": {
            "metadata": {
                "Molekylärpatologi": {
                    "subTables": [
                        "Egfr",
                        "Raf",
                        "Ras",
                        "Alk",
                        "AndraGener",
                        "Fusionsanalys",
                        "CNV"
                    ],
                    "regvars": {
                        "MP_Initierat": {
                            "term": 4075,
                            "description": "Initierad av",
                            "technicalName": "T1573291",
                            "sortOrder": 0,
                            "label": "Initierad av",
                            "compareDescription": "Initierad av",
                            "isComparable": false,
                            "serversideOnly": false
                        },
                        "MP_Inrapp": {
                            "term": 4075,
                            "description": "Inrapporterad av",
                            "technicalName": "T1573292",
                            "sortOrder": 0,
                            "label": "Inrapporterad av",
                            "compareDescription": "Inrapporterad av",
                            "isComparable": false,
                            "serversideOnly": false
                        },
                        "MP_InrappDtm": {
                            "term": 1002,
                            "description": "Inrapporteringsdatum",
                            "technicalName": "T1573299",
                            "sortOrder": 0,
                            "label": "Inrapporteringsdatum",
                            "compareDescription": "Inrapporteringsdatum",
                            "isComparable": false,
                            "serversideOnly": false
                        },
                        "MP_InrappEnh": {
                            "term": 4075,
                            "description": "Inrapporterande enhet",
                            "technicalName": "T1573293",
                            "sortOrder": 0,
                            "label": "Inrapporterande enhet",
                            "compareDescription": "Inrapporterande enhet",
                            "isComparable": false,
                            "serversideOnly": false
                        },
                        "MP_Klinik": {
                            "term": 4075,
                            "description": "Inrapporterande klinikkod",
                            "technicalName": "T1573294",
                            "sortOrder": 0,
                            "label": "Inrapporterande klinikkod",
                            "compareDescription": "Inrapporterande klinikkod",
                            "isComparable": false,
                            "serversideOnly": false
                        },
                        "MP_MonKom": {
                            "term": 4075,
                            "description": "Monitors kommentar",
                            "technicalName": "T1573296",
                            "sortOrder": 0,
                            "label": "Monitors kommentar",
                            "compareDescription": "Monitors kommentar",
                            "isComparable": false,
                            "serversideOnly": false
                        },
                        "MP00_AnalysDtm": {
                            "term": 1002,
                            "description": "Datum då analysen utfördes",
                            "technicalName": "T1573300",
                            "sortOrder": 0,
                            "label": "Datum då analysen utfördes",
                            "compareDescription": "Datum då analysen utfördes",
                            "isComparable": false,
                            "serversideOnly": false
                        },
                        "MP00_InternId": {
                            "term": 4075,
                            "description": "Intern identifiering",
                            "technicalName": "T1573298",
                            "sortOrder": 0,
                            "label": "Intern identifiering",
                            "compareDescription": "Intern identifiering",
                            "isComparable": false,
                            "serversideOnly": false
                        },
                        "MP00_MorfKlass": {
                            "term": 6279,
                            "description": "Tumörtypsklassning",
                            "technicalName": "T1573306",
                            "sortOrder": 0,
                            "label": "Tumörtypsklassning",
                            "compareDescription": "Tumörtypsklassning",
                            "isComparable": false,
                            "serversideOnly": false
                        },
                        "MP00_PAD": {
                            "term": 4075,
                            "description": "PAD/identifiering av material",
                            "technicalName": "T1573302",
                            "sortOrder": 0,
                            "label": "PAD/identifiering av material",
                            "compareDescription": "PAD/identifiering av material",
                            "isComparable": false,
                            "serversideOnly": false
                        },
                        "MP00_ProvMtrl": {
                            "term": 6280,
                            "description": "Typ av provmaterial",
                            "technicalName": "T1573307",
                            "sortOrder": 0,
                            "label": "Typ av provmaterial",
                            "compareDescription": "Typ av provmaterial",
                            "isComparable": false,
                            "serversideOnly": false
                        },
                        "MP00_ProvMtrlVad": {
                            "term": 4075,
                            "description": "Typ av provmaterial annat, vad",
                            "technicalName": "T1573304",
                            "sortOrder": 0,
                            "label": "Typ av provmaterial annat, vad",
                            "compareDescription": "Typ av provmaterial annat, vad",
                            "isComparable": false,
                            "serversideOnly": false
                        },
                        "MP00_TumLok": {
                            "term": 6281,
                            "description": "Tumörlokal",
                            "technicalName": "T1573308",
                            "sortOrder": 0,
                            "label": "Tumörlokal",
                            "compareDescription": "Tumörlokal",
                            "isComparable": false,
                            "serversideOnly": false
                        },
                        "MP00_UppTumHalt": {
                            "term": 6282,
                            "description": "Uppskattad tumörcellshalt (efter ev makrodisektion)",
                            "technicalName": "T1573309",
                            "sortOrder": 0,
                            "label": "Uppskattad tumörcellshalt (efter ev makrodisektion)",
                            "compareDescription": "Uppskattad tumörcellshalt (efter ev makrodisektion)",
                            "isComparable": false,
                            "serversideOnly": false
                        },
                        "MP00_Fragestallning": {
                            "term": 7962,
                            "description": "Typ av frågeställning",
                            "technicalName": "T1573309",
                            "sortOrder": 0,
                            "label": "Typ av frågeställning",
                            "compareDescription": "Typ av frågeställning",
                            "isComparable": false,
                            "serversideOnly": false
                        },
                        "MP01_MutAnalys": {
                            "term": 6284,
                            "description": "Är mutationsanalys genomförd",
                            "technicalName": "T1573310",
                            "sortOrder": 0,
                            "label": "Är mutationsanalys genomförd",
                            "compareDescription": "Är mutationsanalys genomförd",
                            "isComparable": false,
                            "serversideOnly": false
                        },
                        "MP01_Mutation": {
                            "term": 6285,
                            "description": "Uppvisar mutation",
                            "technicalName": "T1573311",
                            "sortOrder": 0,
                            "label": "Uppvisar mutation",
                            "compareDescription": "Uppvisar mutation",
                            "isComparable": false,
                            "serversideOnly": false
                        },
                        "MP01_Panel": {
                            "term": 6286,
                            "description": "Allelspecifik, vad",
                            "technicalName": "T1573312",
                            "sortOrder": 0,
                            "label": "Allelspecifik, vad",
                            "compareDescription": "Allelspecifik, vad",
                            "isComparable": false,
                            "serversideOnly": false
                        },
                        "MP01_TekMutDet": {
                            "term": 6289,
                            "description": "Teknik för mutationsdetektion",
                            "technicalName": "T1573313",
                            "sortOrder": 0,
                            "label": "Teknik för mutationsdetektion",
                            "compareDescription": "Teknik för mutationsdetektion",
                            "isComparable": false,
                            "serversideOnly": false
                        },
                        "MP01_TekVad": {
                            "term": 4075,
                            "description": "Annan teknik, vad",
                            "technicalName": "T1573314",
                            "sortOrder": 0,
                            "label": "Annan teknik, vad",
                            "compareDescription": "Annan teknik, vad",
                            "isComparable": false,
                            "serversideOnly": false
                        },
                        "MP02_AnnFusion": {
                            "term": 6291,
                            "description": "Annan fusion",
                            "technicalName": "T1573316",
                            "sortOrder": 0,
                            "label": "Annan fusion",
                            "compareDescription": "Annan fusion",
                            "isComparable": false,
                            "serversideOnly": false
                        },
                        "MP02_AnnFusionVad": {
                            "term": 4075,
                            "description": "Vad för annan fusion",
                            "technicalName": "T1573317",
                            "sortOrder": 0,
                            "label": "Vad för annan fusion",
                            "compareDescription": "Vad för annan fusion",
                            "isComparable": false,
                            "serversideOnly": false
                        },
                        "MP02_FusGenAnalys": {
                            "term": 6292,
                            "description": "Har fusiongensanalys genomförts",
                            "technicalName": "T1573318",
                            "sortOrder": 0,
                            "label": "Har fusiongensanalys genomförts",
                            "compareDescription": "Har fusiongensanalys genomförts",
                            "isComparable": false,
                            "serversideOnly": false
                        },
                        "MP02_PadKloss": {
                            "term": 6294,
                            "description": "Har annan PAD-kloss än ovan använts",
                            "technicalName": "T1573321",
                            "sortOrder": 0,
                            "label": "Har annan PAD-kloss än ovan använts",
                            "compareDescription": "Har annan PAD-kloss än ovan använts",
                            "isComparable": false,
                            "serversideOnly": false
                        },
                        "MP02_PadKlossVilken": {
                            "term": 4075,
                            "description": "Annan PAD-kloss, vilken",
                            "technicalName": "T1573322",
                            "sortOrder": 0,
                            "label": "Annan PAD-kloss, vilken",
                            "compareDescription": "Annan PAD-kloss, vilken",
                            "isComparable": false,
                            "serversideOnly": false
                        },
                        "MP04_CNVUtford": {
                            "term": 6921,
                            "description": "Har CNV analys utförts",
                            "technicalName": "T1573318",
                            "sortOrder": 0,
                            "label": "Har CNV analys utförts",
                            "compareDescription": "Har CNV analys utförts",
                            "isComparable": false,
                            "serversideOnly": false
                        },
                        "MP03_Biobank": {
                            "term": 6296,
                            "description": "Finns ytterligare material på fallet",
                            "technicalName": "T1573325",
                            "sortOrder": 0,
                            "label": "Finns ytterligare material på fallet",
                            "compareDescription": "Finns ytterligare material på fallet",
                            "isComparable": false,
                            "serversideOnly": false
                        },
                        "MP03_BiobankMtrl": {
                            "term": 6297,
                            "description": "Material i biobank",
                            "technicalName": "T1573326",
                            "sortOrder": 0,
                            "label": "Material i biobank",
                            "compareDescription": "Material i biobank",
                            "isComparable": false,
                            "serversideOnly": false
                        },
                        "MP03_BiobankMtrlVad": {
                            "term": 4075,
                            "description": "Annat biobanksmaterial, vad",
                            "technicalName": "T1573327",
                            "sortOrder": 0,
                            "label": "Annat biobanksmaterial, vad",
                            "compareDescription": "Annat biobanksmaterial, vad",
                            "isComparable": false,
                            "serversideOnly": false
                        },
                        "MP01_AllelVad": {
                            "term": 4075,
                            "description": "Vad för annan allelspecifik teknik",
                            "technicalName": "T1599368",
                            "sortOrder": 0,
                            "label": "Vad för annan allelspecifik teknik",
                            "compareDescription": "Vad för annan allelspecifik teknik",
                            "isComparable": false,
                            "serversideOnly": false
                        },
                        "MP01_NGSVad": {
                            "term": 4075,
                            "description": "Vad för annan NGS panel",
                            "technicalName": "T1599369",
                            "sortOrder": 0,
                            "label": "Vad för annan NGS panel",
                            "compareDescription": "Vad för annan NGS panel",
                            "isComparable": false,
                            "serversideOnly": false
                        },
                        "MP01_NGSPanel": {
                            "term": 7405,
                            "description": "Vilken NGS panel",
                            "technicalName": "T1599369",
                            "sortOrder": 0,
                            "label": "Vilken NGS panel",
                            "compareDescription": "Vilken NGS panel",
                            "isComparable": false,
                            "serversideOnly": false
                        },
                        "MP00_MorfAnnan": {
                            "term": 4075,
                            "description": "Vad för annan cancer",
                            "technicalName": "T1599369",
                            "sortOrder": 0,
                            "label": "Vad för annan cancer",
                            "compareDescription": "Vad för annan cancer",
                            "isComparable": false,
                            "serversideOnly": false
                        },
                        "MP00_MorfAnnanLung": {
                            "term": 4075,
                            "description": "Vad för annan lungcancer",
                            "technicalName": "T1599369",
                            "sortOrder": 0,
                            "label": "Vad för annan lungcancer",
                            "compareDescription": "Vad för annan lungcancer",
                            "isComparable": false,
                            "serversideOnly": false
                        },
                        "MP04_CNVTeknik": {
                            "term": 6288,
                            "description": "CNV teknik",
                            "technicalName": "T1573332",
                            "sortOrder": 0,
                            "label": "CNV teknik",
                            "compareDescription": "CNV teknik",
                            "isComparable": false,
                            "serversideOnly": false
                        },
                        "MP04_CNVTeknikAnnan": {
                            "term": 4075,
                            "description": "Annan CNV teknik",
                            "technicalName": "T1573333",
                            "sortOrder": 0,
                            "label": "Annan CNV teknik",
                            "compareDescription": "Annan CNV teknik",
                            "isComparable": false,
                            "serversideOnly": false
                        },
                        "MP04_CNVPavisas": {
                            "term": 6882,
                            "description": "Påvisas CNV-förändring",
                            "technicalName": "T1573333",
                            "sortOrder": 0,
                            "label": "Påvisas CNV-förändring",
                            "compareDescription": "Påvisas CNV-förändring",
                            "isComparable": false,
                            "serversideOnly": false
                        },
                        "MP04_CNVVilken": {
                            "term": 4075,
                            "description": "Vilken förändring",
                            "technicalName": "T1573333",
                            "sortOrder": 0,
                            "label": "Vilken förändring",
                            "compareDescription": "Vilken förändring",
                            "isComparable": false,
                            "serversideOnly": false
                        }
                    },
                    "vdlists": {
                        "Lunga_Anmälan_VD": {
                            "term": 6298
                        },
                        "MolPat_VD": {
                            "term": 7058
                        }
                    },
                    "terms": {
                        "1002": {
                            "name": "Datum",
                            "shortName": "Datum",
                            "description": "",
                            "dataType": "datetime",
                            "includeTime": false
                        },
                        "4075": {
                            "name": "Text(8000)",
                            "shortName": "TEXT",
                            "description": "Textvariabel med möjlighet att lagra maximalt antal tecken (8000).",
                            "dataType": "string",
                            "maxLength": 8000,
                            "validCharacters": null
                        },
                        "7405": {
                            "name": "MP01_NGSPanel",
                            "shortName": "MP01_NGSPanel",
                            "description": "Vilken NGS panel",
                            "dataType": "list",
                            "listValues": [
                                {
                                    "id": 20061,
                                    "text": "Actionable Insights Tumor Panel",
                                    "value": "1",
                                    "validFrom": "0001-01-01T00:00:00",
                                    "validTo": "9999-12-31T00:00:00"
                                },
                                {
                                    "id": 20062,
                                    "text": "Ion AmpliSeq CLv2",
                                    "value": "2",
                                    "validFrom": "0001-01-01T00:00:00",
                                    "validTo": "9999-12-31T00:00:00"
                                },
                                {
                                    "id": 20063,
                                    "text": "Oncomine Focus",
                                    "value": "3",
                                    "validFrom": "0001-01-01T00:00:00",
                                    "validTo": "9999-12-31T00:00:00"
                                },
                                {
                                    "id": 20061,
                                    "text": "Oncomine Solid Tumour DNA",
                                    "value": "4",
                                    "validFrom": "0001-01-01T00:00:00",
                                    "validTo": "9999-12-31T00:00:00"
                                },
                                {
                                    "id": 20062,
                                    "text": "HaloPlex custom",
                                    "value": "5",
                                    "validFrom": "0001-01-01T00:00:00",
                                    "validTo": "9999-12-31T00:00:00"
                                },
                                {
                                    "id": 20063,
                                    "text": "Archer Solid Tumor",
                                    "value": "6",
                                    "validFrom": "0001-01-01T00:00:00",
                                    "validTo": "9999-12-31T00:00:00"
                                },
                                {
                                    "id": 20062,
                                    "text": "TruSight Tumor 15",
                                    "value": "7",
                                    "validFrom": "0001-01-01T00:00:00",
                                    "validTo": "9999-12-31T00:00:00"
                                },
                                {
                                    "id": 20063,
                                    "text": "Annan",
                                    "value": "8",
                                    "validFrom": "0001-01-01T00:00:00",
                                    "validTo": "9999-12-31T00:00:00"
                                }
                            ]
                        },
                        "7962": {
                            "name": "MP00_Fragestallning",
                            "shortName": "MP00_Fragestallning",
                            "description": "Typ av frågeställning",
                            "dataType": "list",
                            "listValues": [
                                {
                                    "id": 20061,
                                    "text": "Primär",
                                    "value": "1",
                                    "validFrom": "0001-01-01T00:00:00",
                                    "validTo": "9999-12-31T00:00:00"
                                },
                                {
                                    "id": 20062,
                                    "text": "Resistensfrågeställning",
                                    "value": "2",
                                    "validFrom": "0001-01-01T00:00:00",
                                    "validTo": "9999-12-31T00:00:00"
                                },
                                {
                                    "id": 20063,
                                    "text": "Annan",
                                    "value": "3",
                                    "validFrom": "0001-01-01T00:00:00",
                                    "validTo": "9999-12-31T00:00:00"
                                }
                            ]
                        },
                        "6279": {
                            "name": "MP00_MorfKlass",
                            "shortName": "MP00_MorfKlass",
                            "description": "Tumörtypsklassning",
                            "dataType": "list",
                            "listValues": [
                                {
                                    "id": 20061,
                                    "text": "Adenocarcinom",
                                    "value": "1",
                                    "validFrom": "0001-01-01T00:00:00",
                                    "validTo": "9999-12-31T00:00:00"
                                },
                                {
                                    "id": 20062,
                                    "text": "Skivepitel",
                                    "value": "2",
                                    "validFrom": "0001-01-01T00:00:00",
                                    "validTo": "9999-12-31T00:00:00"
                                },
                                {
                                    "id": 20063,
                                    "text": "NSCLC NOS",
                                    "value": "3",
                                    "validFrom": "0001-01-01T00:00:00",
                                    "validTo": "9999-12-31T00:00:00"
                                },
                                {
                                    "id": 20064,
                                    "text": "Okänd",
                                    "value": "4",
                                    "validFrom": "0001-01-01T00:00:00",
                                    "validTo": "9999-12-31T00:00:00"
                                },
                                {
                                    "id": 20064,
                                    "text": "Annan lungcancer",
                                    "value": "5",
                                    "validFrom": "0001-01-01T00:00:00",
                                    "validTo": "9999-12-31T00:00:00"
                                },
                                {
                                    "id": 20064,
                                    "text": "Annan cancer",
                                    "value": "6",
                                    "validFrom": "0001-01-01T00:00:00",
                                    "validTo": "9999-12-31T00:00:00"
                                }
                            ]
                        },
                        "6280": {
                            "name": "MP00_ProvMtrl",
                            "shortName": "MP00_ProvMtrl",
                            "description": "Typ av provmaterial",
                            "dataType": "list",
                            "listValues": [
                                {
                                    "id": 20077,
                                    "text": "Vävnad",
                                    "value": "7",
                                    "validFrom": "0001-01-01T00:00:00",
                                    "validTo": "9999-12-31T00:00:00"
                                },
                                {
                                    "id": 20077,
                                    "text": "Cytologi",
                                    "value": "3",
                                    "validFrom": "0001-01-01T00:00:00",
                                    "validTo": "9999-12-31T00:00:00"
                                },
                                {
                                    "id": 20077,
                                    "text": "ctDNA",
                                    "value": "8",
                                    "validFrom": "0001-01-01T00:00:00",
                                    "validTo": "9999-12-31T00:00:00"
                                },
                                {
                                    "id": 20080,
                                    "text": "Annat",
                                    "value": "6",
                                    "validFrom": "0001-01-01T00:00:00",
                                    "validTo": "9999-12-31T00:00:00"
                                }
                            ]
                        },
                        "6281": {
                            "name": "MP00_TumLok",
                            "shortName": "MP00_TumLok",
                            "description": "Tumörlokal",
                            "dataType": "list",
                            "listValues": [
                                {
                                    "id": 20142,
                                    "text": "Primärtumör",
                                    "value": "1",
                                    "validFrom": "0001-01-01T00:00:00",
                                    "validTo": "9999-12-31T00:00:00"
                                },
                                {
                                    "id": 20143,
                                    "text": "Körtelmetastas",
                                    "value": "2",
                                    "validFrom": "0001-01-01T00:00:00",
                                    "validTo": "9999-12-31T00:00:00"
                                },
                                {
                                    "id": 21842,
                                    "text": "Fjärrmetastas",
                                    "value": "4",
                                    "validFrom": "0001-01-01T00:00:00",
                                    "validTo": "9999-12-31T00:00:00"
                                },
                                {
                                    "id": 20144,
                                    "text": "Okänd",
                                    "value": "3",
                                    "validFrom": "0001-01-01T00:00:00",
                                    "validTo": "9999-12-31T00:00:00"
                                }
                            ]
                        },
                        "6282": {
                            "name": "MP00_UppTumHalt",
                            "shortName": "MP00_UppTumHalt",
                            "description": "Uppskattad tumörcellshalt (efter ev makrodisektion)",
                            "dataType": "list",
                            "listValues": [
                                {
                                    "id": 20145,
                                    "text": "<10%",
                                    "value": "1",
                                    "validFrom": "0001-01-01T00:00:00",
                                    "validTo": "9999-12-31T00:00:00"
                                },
                                {
                                    "id": 20146,
                                    "text": "10-20%",
                                    "value": "2",
                                    "validFrom": "0001-01-01T00:00:00",
                                    "validTo": "9999-12-31T00:00:00"
                                },
                                {
                                    "id": 20147,
                                    "text": "21-50%",
                                    "value": "3",
                                    "validFrom": "0001-01-01T00:00:00",
                                    "validTo": "9999-12-31T00:00:00"
                                },
                                {
                                    "id": 20148,
                                    "text": ">50%",
                                    "value": "4",
                                    "validFrom": "0001-01-01T00:00:00",
                                    "validTo": "9999-12-31T00:00:00"
                                },
                                {
                                    "id": 20149,
                                    "text": "Okänt",
                                    "value": "5",
                                    "validFrom": "0001-01-01T00:00:00",
                                    "validTo": "9999-12-31T00:00:00"
                                }
                            ]
                        },
                        "6284": {
                            "name": "MP01_MutAnalys",
                            "shortName": "MP01_MutAnalys",
                            "description": "Är mutationsanalys genomförd",
                            "dataType": "list",
                            "listValues": [
                                {
                                    "id": 20065,
                                    "text": "Nej",
                                    "value": "0",
                                    "validFrom": "0001-01-01T00:00:00",
                                    "validTo": "9999-12-31T00:00:00"
                                },
                                {
                                    "id": 20066,
                                    "text": "Ja",
                                    "value": "1",
                                    "validFrom": "0001-01-01T00:00:00",
                                    "validTo": "9999-12-31T00:00:00"
                                }
                            ]
                        },
                        "6285": {
                            "name": "MP01_Mutation",
                            "shortName": "MP01_Mutation",
                            "description": "Uppvisar mutation",
                            "dataType": "list",
                            "listValues": [
                                {
                                    "id": 20067,
                                    "text": "Nej",
                                    "value": "0",
                                    "validFrom": "0001-01-01T00:00:00",
                                    "validTo": "9999-12-31T00:00:00"
                                },
                                {
                                    "id": 20068,
                                    "text": "Ja",
                                    "value": "1",
                                    "validFrom": "0001-01-01T00:00:00",
                                    "validTo": "9999-12-31T00:00:00"
                                }
                            ]
                        },
                        "6286": {
                            "name": "MP01_Panel",
                            "shortName": "MP01_Panel",
                            "description": "Allelspecifik, vad",
                            "dataType": "list",
                            "listValues": [
                                {
                                    "id": 20071,
                                    "text": "Pyroseq",
                                    "value": "1",
                                    "validFrom": "0001-01-01T00:00:00",
                                    "validTo": "9999-12-31T00:00:00"
                                },
                                {
                                    "id": 20072,
                                    "text": "Entrogen",
                                    "value": "2",
                                    "validFrom": "0001-01-01T00:00:00",
                                    "validTo": "9999-12-31T00:00:00"
                                },
                                {
                                    "id": 20073,
                                    "text": "Therascreen",
                                    "value": "3",
                                    "validFrom": "0001-01-01T00:00:00",
                                    "validTo": "9999-12-31T00:00:00"
                                },
                                {
                                    "id": 20074,
                                    "text": "Annan",
                                    "value": "4",
                                    "validFrom": "0001-01-01T00:00:00",
                                    "validTo": "9999-12-31T00:00:00"
                                }
                            ]
                        },
                        "6289": {
                            "name": "MP01_TekMutDet",
                            "shortName": "MP01_TekMutDet",
                            "description": "Teknik för mutationsdetektion",
                            "dataType": "list",
                            "listValues": [
                                {
                                    "id": 20132,
                                    "text": "NGS",
                                    "value": "1",
                                    "validFrom": "0001-01-01T00:00:00",
                                    "validTo": "9999-12-31T00:00:00"
                                },
                                {
                                    "id": 20133,
                                    "text": "Allelspecifik",
                                    "value": "2",
                                    "validFrom": "0001-01-01T00:00:00",
                                    "validTo": "9999-12-31T00:00:00"
                                },
                                {
                                    "id": 20134,
                                    "text": "Sanger sekvensering",
                                    "value": "3",
                                    "validFrom": "0001-01-01T00:00:00",
                                    "validTo": "9999-12-31T00:00:00"
                                },
                                {
                                    "id": 20135,
                                    "text": "Annan",
                                    "value": "4",
                                    "validFrom": "0001-01-01T00:00:00",
                                    "validTo": "9999-12-31T00:00:00"
                                }
                            ]
                        },
                        "6290": {
                            "name": "MP02_Alk",
                            "shortName": "MP02_Alk",
                            "description": "Uppvisar ALK-rearrangemang",
                            "dataType": "list",
                            "listValues": [
                                {
                                    "id": 20034,
                                    "text": "Nej",
                                    "value": "0",
                                    "validFrom": "0001-01-01T00:00:00",
                                    "validTo": "9999-12-31T00:00:00"
                                },
                                {
                                    "id": 20035,
                                    "text": "Ja",
                                    "value": "1",
                                    "validFrom": "0001-01-01T00:00:00",
                                    "validTo": "9999-12-31T00:00:00"
                                },
                                {
                                    "id": 21841,
                                    "text": "Ej analyserbar",
                                    "value": "2",
                                    "validFrom": "0001-01-01T00:00:00",
                                    "validTo": "9999-12-31T00:00:00"
                                }
                            ]
                        },
                        "6291": {
                            "name": "MP02_AnnFusion",
                            "shortName": "MP02_AnnFusion",
                            "description": "Annan fusion",
                            "dataType": "list",
                            "listValues": [
                                {
                                    "id": 20036,
                                    "text": "Nej",
                                    "value": "0",
                                    "validFrom": "0001-01-01T00:00:00",
                                    "validTo": "9999-12-31T00:00:00"
                                },
                                {
                                    "id": 20037,
                                    "text": "Ja",
                                    "value": "1",
                                    "validFrom": "0001-01-01T00:00:00",
                                    "validTo": "9999-12-31T00:00:00"
                                },
                                {
                                    "id": 20038,
                                    "text": "Okänt",
                                    "value": "2",
                                    "validFrom": "0001-01-01T00:00:00",
                                    "validTo": "9999-12-31T00:00:00"
                                }
                            ]
                        },
                        "6292": {
                            "name": "MP02_FusGenAnalys",
                            "shortName": "MP02_FusGenAnalys",
                            "description": "Har fusiongensanalys genomförts",
                            "dataType": "list",
                            "listValues": [
                                {
                                    "id": 20057,
                                    "text": "Nej",
                                    "value": "0",
                                    "validFrom": "0001-01-01T00:00:00",
                                    "validTo": "9999-12-31T00:00:00"
                                },
                                {
                                    "id": 20058,
                                    "text": "Ja",
                                    "value": "1",
                                    "validFrom": "0001-01-01T00:00:00",
                                    "validTo": "9999-12-31T00:00:00"
                                }
                            ]
                        },
                        "6921": {
                            "name": "MP04_CNVUtford",
                            "shortName": "MP04_CNVUtford",
                            "description": "Har fusiongensanalys genomförts",
                            "dataType": "list",
                            "listValues": [
                                {
                                    "id": 27057,
                                    "text": "Nej",
                                    "value": "0",
                                    "validFrom": "0001-01-01T00:00:00",
                                    "validTo": "9999-12-31T00:00:00"
                                },
                                {
                                    "id": 27058,
                                    "text": "Ja",
                                    "value": "1",
                                    "validFrom": "0001-01-01T00:00:00",
                                    "validTo": "9999-12-31T00:00:00"
                                }
                            ]
                        },
                        "6293": {
                            "name": "MP02_FusionsPartner",
                            "shortName": "MP02_FusionsPartner",
                            "description": "Känd fusionspartner",
                            "dataType": "list",
                            "listValues": [
                                {
                                    "id": 20059,
                                    "text": "Nej",
                                    "value": "0",
                                    "validFrom": "0001-01-01T00:00:00",
                                    "validTo": "9999-12-31T00:00:00"
                                },
                                {
                                    "id": 20060,
                                    "text": "Ja",
                                    "value": "1",
                                    "validFrom": "0001-01-01T00:00:00",
                                    "validTo": "9999-12-31T00:00:00"
                                }
                            ]
                        },
                        "6294": {
                            "name": "MP02_PadKloss",
                            "shortName": "MP02_PadKloss",
                            "description": "Har annan PAD-kloss än ovan använts",
                            "dataType": "list",
                            "listValues": [
                                {
                                    "id": 20069,
                                    "text": "Nej",
                                    "value": "0",
                                    "validFrom": "0001-01-01T00:00:00",
                                    "validTo": "9999-12-31T00:00:00"
                                },
                                {
                                    "id": 20070,
                                    "text": "Ja",
                                    "value": "1",
                                    "validFrom": "0001-01-01T00:00:00",
                                    "validTo": "9999-12-31T00:00:00"
                                }
                            ]
                        },
                        "6296": {
                            "name": "MP03_Biobank",
                            "shortName": "MP03_Biobank",
                            "description": "Finns ytterligare material på fallet",
                            "dataType": "list",
                            "listValues": [
                                {
                                    "id": 20039,
                                    "text": "Nej",
                                    "value": "0",
                                    "validFrom": "0001-01-01T00:00:00",
                                    "validTo": "9999-12-31T00:00:00"
                                },
                                {
                                    "id": 20040,
                                    "text": "Ja",
                                    "value": "1",
                                    "validFrom": "0001-01-01T00:00:00",
                                    "validTo": "9999-12-31T00:00:00"
                                },
                                {
                                    "id": 20041,
                                    "text": "Okänt",
                                    "value": "2",
                                    "validFrom": "0001-01-01T00:00:00",
                                    "validTo": "9999-12-31T00:00:00"
                                }
                            ]
                        },
                        "6297": {
                            "name": "MP03_BiobankMtrl",
                            "shortName": "MP03_BiobankMtrl",
                            "description": "Material",
                            "dataType": "list",
                            "listValues": [
                                {
                                    "id": 20042,
                                    "text": "Paraffinmaterial",
                                    "value": "1",
                                    "validFrom": "0001-01-01T00:00:00",
                                    "validTo": "9999-12-31T00:00:00"
                                },
                                {
                                    "id": 20043,
                                    "text": "DNA/RNA",
                                    "value": "2",
                                    "validFrom": "0001-01-01T00:00:00",
                                    "validTo": "9999-12-31T00:00:00"
                                },
                                {
                                    "id": 20044,
                                    "text": "Cytologi",
                                    "value": "3",
                                    "validFrom": "0001-01-01T00:00:00",
                                    "validTo": "9999-12-31T00:00:00"
                                },
                                {
                                    "id": 20045,
                                    "text": "Annat",
                                    "value": "4",
                                    "validFrom": "0001-01-01T00:00:00",
                                    "validTo": "9999-12-31T00:00:00"
                                }
                            ]
                        },
                        "6288": {
                            "name": "MP04_CNVTeknik",
                            "shortName": "MP04_CNVTeknik",
                            "description": "CNV teknik",
                            "dataType": "list",
                            "listValues": [
                                {
                                    "id": 20089,
                                    "text": "NGS",
                                    "value": "1",
                                    "validFrom": "0001-01-01T00:00:00",
                                    "validTo": "9999-12-31T00:00:00"
                                },
                                {
                                    "id": 20098,
                                    "text": "FISH",
                                    "value": "2",
                                    "validFrom": "0001-01-01T00:00:00",
                                    "validTo": "9999-12-31T00:00:00"
                                },
                                {
                                    "id": 20131,
                                    "text": "Annan",
                                    "value": "3",
                                    "validFrom": "0001-01-01T00:00:00",
                                    "validTo": "9999-12-31T00:00:00"
                                }
                            ]
                        },
                        "6882": {
                            "name": "MP04_CNVPavisas",
                            "shortName": "MP04_CNVPavisas",
                            "description": "Påvisas CNV-förändring",
                            "dataType": "list",
                            "listValues": [
                                {
                                    "id": 20089,
                                    "text": "Nej",
                                    "value": "0",
                                    "validFrom": "0001-01-01T00:00:00",
                                    "validTo": "9999-12-31T00:00:00"
                                },
                                {
                                    "id": 20131,
                                    "text": "Ja",
                                    "value": "1",
                                    "validFrom": "0001-01-01T00:00:00",
                                    "validTo": "9999-12-31T00:00:00"
                                }
                            ]
                        },
                        "6298": {
                            "name": "Lunga_Anmälan_VD",
                            "shortName": "Lunga_Anmälan_VD",
                            "description": null,
                            "dataType": "vd"
                        },
                        "7058": {
                            "name": "MolPat_VD",
                            "shortName": "MolPat_VD",
                            "description": null,
                            "dataType": "vd"
                        }
                    },
                    "treeLevel": 2
                },
                "Egfr": {
                    "subTables": [],
                    "regvars": {
                        "MP01_Egfr": {
                            "term": 6283,
                            "description": "EGFR-mutation",
                            "technicalName": "T1573328",
                            "sortOrder": 0,
                            "label": "EGFR-mutation",
                            "compareDescription": "EGFR-mutation",
                            "isComparable": false,
                            "serversideOnly": false
                        },
                        "MP01_EgfrVad": {
                            "term": 4075,
                            "description": "Annan EGFR-mutation, vad",
                            "technicalName": "T1573329",
                            "sortOrder": 0,
                            "label": "Annan EGFR-mutation, vad",
                            "compareDescription": "Annan EGFR-mutation, vad",
                            "isComparable": false,
                            "serversideOnly": false
                        }
                    },
                    "vdlists": {},
                    "terms": {
                        "4075": {
                            "name": "Text(8000)",
                            "shortName": "TEXT",
                            "description": "Textvariabel med möjlighet att lagra maximalt antal tecken (8000).",
                            "dataType": "string",
                            "maxLength": 8000,
                            "validCharacters": null
                        },
                        "6283": {
                            "name": "MP01_Egfr",
                            "shortName": "MP01_Egfr",
                            "description": "EGFR-mutation",
                            "dataType": "list",
                            "listValues": [
                                {
                                    "id": 20046,
                                    "text": "Normal",
                                    "value": "1",
                                    "validFrom": "0001-01-01T00:00:00",
                                    "validTo": "9999-12-31T00:00:00"
                                },
                                {
                                    "id": 20047,
                                    "text": "Ej analyserad",
                                    "value": "2",
                                    "validFrom": "0001-01-01T00:00:00",
                                    "validTo": "9999-12-31T00:00:00"
                                },
                                {
                                    "id": 20048,
                                    "text": "Ej bedömbar",
                                    "value": "3",
                                    "validFrom": "0001-01-01T00:00:00",
                                    "validTo": "9999-12-31T00:00:00"
                                },
                                {
                                    "id": 20049,
                                    "text": "EGFR exon 19 del",
                                    "value": "4",
                                    "validFrom": "0001-01-01T00:00:00",
                                    "validTo": "9999-12-31T00:00:00"
                                },
                                {
                                    "id": 20050,
                                    "text": "EGFR p.L858R",
                                    "value": "5",
                                    "validFrom": "0001-01-01T00:00:00",
                                    "validTo": "9999-12-31T00:00:00"
                                },
                                {
                                    "id": 20051,
                                    "text": "EGFR p.L861Q",
                                    "value": "6",
                                    "validFrom": "0001-01-01T00:00:00",
                                    "validTo": "9999-12-31T00:00:00"
                                },
                                {
                                    "id": 20052,
                                    "text": "EGFR p.G719X",
                                    "value": "7",
                                    "validFrom": "0001-01-01T00:00:00",
                                    "validTo": "9999-12-31T00:00:00"
                                },
                                {
                                    "id": 20053,
                                    "text": "EGFR p.T790M",
                                    "value": "8",
                                    "validFrom": "0001-01-01T00:00:00",
                                    "validTo": "9999-12-31T00:00:00"
                                },
                                {
                                    "id": 20054,
                                    "text": "EGFR p.S768I",
                                    "value": "9",
                                    "validFrom": "0001-01-01T00:00:00",
                                    "validTo": "9999-12-31T00:00:00"
                                },
                                {
                                    "id": 20055,
                                    "text": "EGFR exon 20 ins",
                                    "value": "10",
                                    "validFrom": "0001-01-01T00:00:00",
                                    "validTo": "9999-12-31T00:00:00"
                                },
                                {
                                    "id": 20056,
                                    "text": "Annan",
                                    "value": "11",
                                    "validFrom": "0001-01-01T00:00:00",
                                    "validTo": "9999-12-31T00:00:00"
                                }
                            ]
                        }
                    },
                    "treeLevel": 3
                },
                "Raf": {
                    "subTables": [],
                    "regvars": {
                        "MP01_Raf": {
                            "term": 6287,
                            "description": "RAF-mutation",
                            "technicalName": "T1573330",
                            "sortOrder": 0,
                            "label": "RAF-mutation",
                            "compareDescription": "RAF-mutation",
                            "isComparable": false,
                            "serversideOnly": false
                        },
                        "MP01_RafVad": {
                            "term": 4075,
                            "description": "Annan RAF-mutation, vad",
                            "technicalName": "T1573331",
                            "sortOrder": 0,
                            "label": "Annan RAF-mutation, vad",
                            "compareDescription": "Annan RAF-mutation, vad",
                            "isComparable": false,
                            "serversideOnly": false
                        }
                    },
                    "vdlists": {},
                    "terms": {
                        "4075": {
                            "name": "Text(8000)",
                            "shortName": "TEXT",
                            "description": "Textvariabel med möjlighet att lagra maximalt antal tecken (8000).",
                            "dataType": "string",
                            "maxLength": 8000,
                            "validCharacters": null
                        },
                        "6287": {
                            "name": "MP01_Raf",
                            "shortName": "MP01_Raf",
                            "description": "RAF-mutation",
                            "dataType": "list",
                            "listValues": [
                                {
                                    "id": 20081,
                                    "text": "Normal",
                                    "value": "1",
                                    "validFrom": "0001-01-01T00:00:00",
                                    "validTo": "9999-12-31T00:00:00"
                                },
                                {
                                    "id": 20082,
                                    "text": "Ej analyserad",
                                    "value": "2",
                                    "validFrom": "0001-01-01T00:00:00",
                                    "validTo": "9999-12-31T00:00:00"
                                },
                                {
                                    "id": 20083,
                                    "text": "Ej bedömbar",
                                    "value": "3",
                                    "validFrom": "0001-01-01T00:00:00",
                                    "validTo": "9999-12-31T00:00:00"
                                },
                                {
                                    "id": 20084,
                                    "text": "BRAF p.T599X",
                                    "value": "4",
                                    "validFrom": "0001-01-01T00:00:00",
                                    "validTo": "9999-12-31T00:00:00"
                                },
                                {
                                    "id": 20085,
                                    "text": "BRAF p.V600D",
                                    "value": "5",
                                    "validFrom": "0001-01-01T00:00:00",
                                    "validTo": "9999-12-31T00:00:00"
                                },
                                {
                                    "id": 20086,
                                    "text": "BRAF p.V600E",
                                    "value": "6",
                                    "validFrom": "0001-01-01T00:00:00",
                                    "validTo": "9999-12-31T00:00:00"
                                },
                                {
                                    "id": 20087,
                                    "text": "BRAF p.V600K",
                                    "value": "7",
                                    "validFrom": "0001-01-01T00:00:00",
                                    "validTo": "9999-12-31T00:00:00"
                                },
                                {
                                    "id": 20088,
                                    "text": "Annan",
                                    "value": "8",
                                    "validFrom": "0001-01-01T00:00:00",
                                    "validTo": "9999-12-31T00:00:00"
                                }
                            ]
                        }
                    },
                    "treeLevel": 3
                },
                "Ras": {
                    "subTables": [],
                    "regvars": {
                        "MP01_Ras": {
                            "term": 6288,
                            "description": "RAS-mutation",
                            "technicalName": "T1573332",
                            "sortOrder": 0,
                            "label": "RAS-mutation",
                            "compareDescription": "RAS-mutation",
                            "isComparable": false,
                            "serversideOnly": false
                        },
                        "MP01_RasVad": {
                            "term": 4075,
                            "description": "Annan RAS-mutation, vad",
                            "technicalName": "T1573333",
                            "sortOrder": 0,
                            "label": "Annan RAS-mutation, vad",
                            "compareDescription": "Annan RAS-mutation, vad",
                            "isComparable": false,
                            "serversideOnly": false
                        }
                    },
                    "vdlists": {},
                    "terms": {
                        "4075": {
                            "name": "Text(8000)",
                            "shortName": "TEXT",
                            "description": "Textvariabel med möjlighet att lagra maximalt antal tecken (8000).",
                            "dataType": "string",
                            "maxLength": 8000,
                            "validCharacters": null
                        },
                        "6288": {
                            "name": "MP01_Ras",
                            "shortName": "MP01_Ras",
                            "description": "RAS-mutation",
                            "dataType": "list",
                            "listValues": [
                                {
                                    "id": 20089,
                                    "text": "Normal",
                                    "value": "1",
                                    "validFrom": "0001-01-01T00:00:00",
                                    "validTo": "9999-12-31T00:00:00"
                                },
                                {
                                    "id": 20090,
                                    "text": "Ej analyserad",
                                    "value": "2",
                                    "validFrom": "0001-01-01T00:00:00",
                                    "validTo": "9999-12-31T00:00:00"
                                },
                                {
                                    "id": 20091,
                                    "text": "Ej bedömbar",
                                    "value": "3",
                                    "validFrom": "0001-01-01T00:00:00",
                                    "validTo": "9999-12-31T00:00:00"
                                },
                                {
                                    "id": 20092,
                                    "text": "KRAS p.G12A",
                                    "value": "4",
                                    "validFrom": "0001-01-01T00:00:00",
                                    "validTo": "9999-12-31T00:00:00"
                                },
                                {
                                    "id": 20093,
                                    "text": "KRAS p.G12C",
                                    "value": "5",
                                    "validFrom": "0001-01-01T00:00:00",
                                    "validTo": "9999-12-31T00:00:00"
                                },
                                {
                                    "id": 20094,
                                    "text": "KRAS p.G12D",
                                    "value": "6",
                                    "validFrom": "0001-01-01T00:00:00",
                                    "validTo": "9999-12-31T00:00:00"
                                },
                                {
                                    "id": 20095,
                                    "text": "KRAS p.G12R",
                                    "value": "7",
                                    "validFrom": "0001-01-01T00:00:00",
                                    "validTo": "9999-12-31T00:00:00"
                                },
                                {
                                    "id": 20096,
                                    "text": "KRAS p.G12S",
                                    "value": "8",
                                    "validFrom": "0001-01-01T00:00:00",
                                    "validTo": "9999-12-31T00:00:00"
                                },
                                {
                                    "id": 20097,
                                    "text": "KRAS p.G12V",
                                    "value": "9",
                                    "validFrom": "0001-01-01T00:00:00",
                                    "validTo": "9999-12-31T00:00:00"
                                },
                                {
                                    "id": 20098,
                                    "text": "KRAS p.G13A",
                                    "value": "10",
                                    "validFrom": "0001-01-01T00:00:00",
                                    "validTo": "9999-12-31T00:00:00"
                                },
                                {
                                    "id": 20099,
                                    "text": "KRAS p.G13C",
                                    "value": "11",
                                    "validFrom": "0001-01-01T00:00:00",
                                    "validTo": "9999-12-31T00:00:00"
                                },
                                {
                                    "id": 20100,
                                    "text": "KRAS p.G13D",
                                    "value": "12",
                                    "validFrom": "0001-01-01T00:00:00",
                                    "validTo": "9999-12-31T00:00:00"
                                },
                                {
                                    "id": 20101,
                                    "text": "KRAS p.G13R",
                                    "value": "13",
                                    "validFrom": "0001-01-01T00:00:00",
                                    "validTo": "9999-12-31T00:00:00"
                                },
                                {
                                    "id": 20102,
                                    "text": "KRAS p.G13S",
                                    "value": "14",
                                    "validFrom": "0001-01-01T00:00:00",
                                    "validTo": "9999-12-31T00:00:00"
                                },
                                {
                                    "id": 20103,
                                    "text": "KRAS p.G13V",
                                    "value": "15",
                                    "validFrom": "0001-01-01T00:00:00",
                                    "validTo": "9999-12-31T00:00:00"
                                },
                                {
                                    "id": 20104,
                                    "text": "KRAS p.A59E",
                                    "value": "16",
                                    "validFrom": "0001-01-01T00:00:00",
                                    "validTo": "9999-12-31T00:00:00"
                                },
                                {
                                    "id": 20105,
                                    "text": "KRAS p.A59G",
                                    "value": "17",
                                    "validFrom": "0001-01-01T00:00:00",
                                    "validTo": "9999-12-31T00:00:00"
                                },
                                {
                                    "id": 20106,
                                    "text": "KRAS p.A59T",
                                    "value": "18",
                                    "validFrom": "0001-01-01T00:00:00",
                                    "validTo": "9999-12-31T00:00:00"
                                },
                                {
                                    "id": 20107,
                                    "text": "KRAS p.Q61H",
                                    "value": "19",
                                    "validFrom": "0001-01-01T00:00:00",
                                    "validTo": "9999-12-31T00:00:00"
                                },
                                {
                                    "id": 20108,
                                    "text": "KRAS p.Q61L",
                                    "value": "20",
                                    "validFrom": "0001-01-01T00:00:00",
                                    "validTo": "9999-12-31T00:00:00"
                                },
                                {
                                    "id": 20109,
                                    "text": "KRAS p.Q61R",
                                    "value": "21",
                                    "validFrom": "0001-01-01T00:00:00",
                                    "validTo": "9999-12-31T00:00:00"
                                },
                                {
                                    "id": 20110,
                                    "text": "KRAS p.K117X",
                                    "value": "22",
                                    "validFrom": "0001-01-01T00:00:00",
                                    "validTo": "9999-12-31T00:00:00"
                                },
                                {
                                    "id": 20111,
                                    "text": "KRAS p.146X",
                                    "value": "23",
                                    "validFrom": "0001-01-01T00:00:00",
                                    "validTo": "9999-12-31T00:00:00"
                                },
                                {
                                    "id": 20112,
                                    "text": "NRAS p.G12A",
                                    "value": "24",
                                    "validFrom": "0001-01-01T00:00:00",
                                    "validTo": "9999-12-31T00:00:00"
                                },
                                {
                                    "id": 20113,
                                    "text": "NRAS p.G12C",
                                    "value": "25",
                                    "validFrom": "0001-01-01T00:00:00",
                                    "validTo": "9999-12-31T00:00:00"
                                },
                                {
                                    "id": 20114,
                                    "text": "NRAS p.G12D",
                                    "value": "26",
                                    "validFrom": "0001-01-01T00:00:00",
                                    "validTo": "9999-12-31T00:00:00"
                                },
                                {
                                    "id": 20115,
                                    "text": "NRAS p.G12R",
                                    "value": "27",
                                    "validFrom": "0001-01-01T00:00:00",
                                    "validTo": "9999-12-31T00:00:00"
                                },
                                {
                                    "id": 20116,
                                    "text": "NRAS p.G12S",
                                    "value": "28",
                                    "validFrom": "0001-01-01T00:00:00",
                                    "validTo": "9999-12-31T00:00:00"
                                },
                                {
                                    "id": 20117,
                                    "text": "NRAS p.G12V",
                                    "value": "29",
                                    "validFrom": "0001-01-01T00:00:00",
                                    "validTo": "9999-12-31T00:00:00"
                                },
                                {
                                    "id": 20118,
                                    "text": "NRAS p.G13A",
                                    "value": "30",
                                    "validFrom": "0001-01-01T00:00:00",
                                    "validTo": "9999-12-31T00:00:00"
                                },
                                {
                                    "id": 20119,
                                    "text": "NRAS p.G13C",
                                    "value": "31",
                                    "validFrom": "0001-01-01T00:00:00",
                                    "validTo": "9999-12-31T00:00:00"
                                },
                                {
                                    "id": 20120,
                                    "text": "NRAS p.G13D",
                                    "value": "32",
                                    "validFrom": "0001-01-01T00:00:00",
                                    "validTo": "9999-12-31T00:00:00"
                                },
                                {
                                    "id": 20121,
                                    "text": "NRAS p.G13R",
                                    "value": "33",
                                    "validFrom": "0001-01-01T00:00:00",
                                    "validTo": "9999-12-31T00:00:00"
                                },
                                {
                                    "id": 20122,
                                    "text": "NRAS p.G13S",
                                    "value": "34",
                                    "validFrom": "0001-01-01T00:00:00",
                                    "validTo": "9999-12-31T00:00:00"
                                },
                                {
                                    "id": 20123,
                                    "text": "NRAS p.G13V",
                                    "value": "35",
                                    "validFrom": "0001-01-01T00:00:00",
                                    "validTo": "9999-12-31T00:00:00"
                                },
                                {
                                    "id": 20124,
                                    "text": "NRAS p.A59E",
                                    "value": "36",
                                    "validFrom": "0001-01-01T00:00:00",
                                    "validTo": "9999-12-31T00:00:00"
                                },
                                {
                                    "id": 20125,
                                    "text": "NRAS p.A59G",
                                    "value": "37",
                                    "validFrom": "0001-01-01T00:00:00",
                                    "validTo": "9999-12-31T00:00:00"
                                },
                                {
                                    "id": 20126,
                                    "text": "NRAS p.A59T",
                                    "value": "38",
                                    "validFrom": "0001-01-01T00:00:00",
                                    "validTo": "9999-12-31T00:00:00"
                                },
                                {
                                    "id": 20127,
                                    "text": "NRAS p.Q61H",
                                    "value": "39",
                                    "validFrom": "0001-01-01T00:00:00",
                                    "validTo": "9999-12-31T00:00:00"
                                },
                                {
                                    "id": 20128,
                                    "text": "NRAS p.Q61L",
                                    "value": "40",
                                    "validFrom": "0001-01-01T00:00:00",
                                    "validTo": "9999-12-31T00:00:00"
                                },
                                {
                                    "id": 20129,
                                    "text": "NRAS p.Q61R",
                                    "value": "41",
                                    "validFrom": "0001-01-01T00:00:00",
                                    "validTo": "9999-12-31T00:00:00"
                                },
                                {
                                    "id": 20130,
                                    "text": "NRAS p.146X",
                                    "value": "42",
                                    "validFrom": "0001-01-01T00:00:00",
                                    "validTo": "9999-12-31T00:00:00"
                                },
                                {
                                    "id": 20131,
                                    "text": "Annan",
                                    "value": "43",
                                    "validFrom": "0001-01-01T00:00:00",
                                    "validTo": "9999-12-31T00:00:00"
                                }
                            ]
                        }
                    },
                    "treeLevel": 3
                },
                "Alk": {
                    "subTables": [],
                    "regvars": {
                        "MP01_Alk": {
                            "term": 8628,
                            "description": "ALK-mutation",
                            "technicalName": "T1573332",
                            "sortOrder": 0,
                            "label": "ALK-mutation",
                            "compareDescription": "ALK-mutation",
                            "isComparable": false,
                            "serversideOnly": false
                        },
                        "MP01_AlkVad": {
                            "term": 4075,
                            "description": "Annan ALK-mutation, vad",
                            "technicalName": "T1573333",
                            "sortOrder": 0,
                            "label": "Annan ALK-mutation, vad",
                            "compareDescription": "Annan ALK-mutation, vad",
                            "isComparable": false,
                            "serversideOnly": false
                        }
                    },
                    "vdlists": {},
                    "terms": {
                        "4075": {
                            "name": "Text(8000)",
                            "shortName": "TEXT",
                            "description": "Textvariabel med möjlighet att lagra maximalt antal tecken (8000).",
                            "dataType": "string",
                            "maxLength": 8000,
                            "validCharacters": null
                        },
                        "8628": {
                            "name": "MP01_Alk",
                            "shortName": "MP01_Alk",
                            "description": "ALK-mutation",
                            "dataType": "list",
                            "listValues": [
                                {
                                    "id": 20089,
                                    "text": "Normal",
                                    "value": "1",
                                    "validFrom": "0001-01-01T00:00:00",
                                    "validTo": "9999-12-31T00:00:00"
                                },
                                {
                                    "id": 20090,
                                    "text": "Ej analyserad",
                                    "value": "2",
                                    "validFrom": "0001-01-01T00:00:00",
                                    "validTo": "9999-12-31T00:00:00"
                                },
                                {
                                    "id": 20091,
                                    "text": "Ej bedömbar",
                                    "value": "3",
                                    "validFrom": "0001-01-01T00:00:00",
                                    "validTo": "9999-12-31T00:00:00"
                                },
                                {
                                    "id": 20092,
                                    "text": "ALK p.1151Tins",
                                    "value": "4",
                                    "validFrom": "0001-01-01T00:00:00",
                                    "validTo": "9999-12-31T00:00:00"
                                },
                                {
                                    "id": 20093,
                                    "text": "ALK p.C1156Y",
                                    "value": "5",
                                    "validFrom": "0001-01-01T00:00:00",
                                    "validTo": "9999-12-31T00:00:00"
                                },
                                {
                                    "id": 20092,
                                    "text": "ALK p.I1171T",
                                    "value": "6",
                                    "validFrom": "0001-01-01T00:00:00",
                                    "validTo": "9999-12-31T00:00:00"
                                },
                                {
                                    "id": 20093,
                                    "text": "ALK p.F1174L",
                                    "value": "7",
                                    "validFrom": "0001-01-01T00:00:00",
                                    "validTo": "9999-12-31T00:00:00"
                                },
                                {
                                    "id": 20092,
                                    "text": "ALK p.V1180L",
                                    "value": "8",
                                    "validFrom": "0001-01-01T00:00:00",
                                    "validTo": "9999-12-31T00:00:00"
                                },
                                {
                                    "id": 20093,
                                    "text": "ALK p.L1196M",
                                    "value": "9",
                                    "validFrom": "0001-01-01T00:00:00",
                                    "validTo": "9999-12-31T00:00:00"
                                },
                                {
                                    "id": 20092,
                                    "text": "ALK p.G1202R",
                                    "value": "10",
                                    "validFrom": "0001-01-01T00:00:00",
                                    "validTo": "9999-12-31T00:00:00"
                                },
                                {
                                    "id": 20093,
                                    "text": "ALK p.S1206Y",
                                    "value": "11",
                                    "validFrom": "0001-01-01T00:00:00",
                                    "validTo": "9999-12-31T00:00:00"
                                },
                                {
                                    "id": 20092,
                                    "text": "ALK p.G1269A",
                                    "value": "12",
                                    "validFrom": "0001-01-01T00:00:00",
                                    "validTo": "9999-12-31T00:00:00"
                                },
                                {
                                    "id": 20131,
                                    "text": "Annan",
                                    "value": "13",
                                    "validFrom": "0001-01-01T00:00:00",
                                    "validTo": "9999-12-31T00:00:00"
                                }
                            ]
                        }
                    },
                    "treeLevel": 3
                },
                "AndraGener": {
                    "subTables": [],
                    "regvars": {
                        "MP01_AnnanVad": {
                            "term": 4075,
                            "description": "Annan RAS-mutation, vad",
                            "technicalName": "T1573333",
                            "sortOrder": 0,
                            "label": "Annan RAS-mutation, vad",
                            "compareDescription": "Annan RAS-mutation, vad",
                            "isComparable": false,
                            "serversideOnly": false
                        }
                    },
                    "vdlists": {},
                    "terms": {
                        "4075": {
                            "name": "Text(8000)",
                            "shortName": "TEXT",
                            "description": "Textvariabel med möjlighet att lagra maximalt antal tecken (8000).",
                            "dataType": "string",
                            "maxLength": 8000,
                            "validCharacters": null
                        }
                    },
                    "treeLevel": 3
                },
                "Fusionsanalys": {
                    "subTables": [
                        "Rearrangemang"
                    ],
                    "regvars": {
                        "MP02_Fusionanalysteknik": {
                            "term": 6288,
                            "description": "Fusionsanalysteknik",
                            "technicalName": "T1573332",
                            "sortOrder": 0,
                            "label": "Fusionsanalysteknik",
                            "compareDescription": "Fusionsanalysteknik",
                            "isComparable": false,
                            "serversideOnly": false
                        },
                        "MP02_AnnanFusionsteknik": {
                            "term": 4075,
                            "description": "Vad för annan teknik",
                            "technicalName": "T1573333",
                            "sortOrder": 0,
                            "label": "Vad för annan teknik",
                            "compareDescription": "Vad för annan teknik",
                            "isComparable": false,
                            "serversideOnly": false
                        }
                    },
                    "vdlists": {},
                    "terms": {
                        "4075": {
                            "name": "Text(8000)",
                            "shortName": "TEXT",
                            "description": "Textvariabel med möjlighet att lagra maximalt antal tecken (8000).",
                            "dataType": "string",
                            "maxLength": 8000,
                            "validCharacters": null
                        },
                        "6288": {
                            "name": "MP02_Fusionanalysteknik",
                            "shortName": "MP02_Fusionanalysteknik",
                            "description": "Fusionsanalysteknik",
                            "dataType": "list",
                            "listValues": [
                                {
                                    "id": 20136,
                                    "text": "IHC",
                                    "value": "1",
                                    "validFrom": "0001-01-01T00:00:00",
                                    "validTo": "9999-12-31T00:00:00"
                                },
                                {
                                    "id": 20137,
                                    "text": "In situ hybridisering (ISH)",
                                    "value": "2",
                                    "validFrom": "0001-01-01T00:00:00",
                                    "validTo": "9999-12-31T00:00:00"
                                },
                                {
                                    "id": 20139,
                                    "text": "NGS",
                                    "value": "4",
                                    "validFrom": "0001-01-01T00:00:00",
                                    "validTo": "9999-12-31T00:00:00"
                                },
                                {
                                    "id": 20140,
                                    "text": "RT PCR",
                                    "value": "5",
                                    "validFrom": "0001-01-01T00:00:00",
                                    "validTo": "9999-12-31T00:00:00"
                                },
                                {
                                    "id": 20138,
                                    "text": "Nanostring",
                                    "value": "7",
                                    "validFrom": "0001-01-01T00:00:00",
                                    "validTo": "9999-12-31T00:00:00"
                                },
                                {
                                    "id": 20141,
                                    "text": "Annan teknik",
                                    "value": "6",
                                    "validFrom": "0001-01-01T00:00:00",
                                    "validTo": "9999-12-31T00:00:00"
                                }
                            ]
                        }
                    },
                    "treeLevel": 3
                },
                "Rearrangemang": {
                    "subTables": [],
                    "regvars": {
                        "MP02_RearrAnalys": {
                            "term": 9876,
                            "description": "Vilka rearrangemang har analyserats",
                            "technicalName": "T1573315",
                            "sortOrder": 0,
                            "label": "Vilka rearrangemang har analyserats",
                            "compareDescription": "Vilka rearrangemang har analyserats",
                            "isComparable": false,
                            "serversideOnly": false
                        },
                        "MP02_RearrAnalysAnnat": {
                            "term": 4075,
                            "description": "Vilket annat rearrangemang har analyserats",
                            "technicalName": "T1573315",
                            "sortOrder": 0,
                            "label": "Vilket annat rearrangemang har analyserats",
                            "compareDescription": "Vilket annat rearrangemang har analyserats",
                            "isComparable": false,
                            "serversideOnly": false
                        },
                        "MP02_Rearr": {
                            "term": 6290,
                            "description": "Uppvisar rearrangemang",
                            "technicalName": "T1573315",
                            "sortOrder": 0,
                            "label": "Uppvisar rearrangemang",
                            "compareDescription": "Uppvisar rearrangemang",
                            "isComparable": false,
                            "serversideOnly": false
                        },
                        "MP02_Fusionspartner": {
                            "term": 6293,
                            "description": "Känd fusionspartner",
                            "technicalName": "T1573319",
                            "sortOrder": 0,
                            "label": "Känd fusionspartner",
                            "compareDescription": "Känd fusionspartner",
                            "isComparable": false,
                            "serversideOnly": false
                        },
                        "MP02_FusionspartnerVilken": {
                            "term": 4075,
                            "description": "Annan känd fusionspartner, vilken",
                            "technicalName": "T1573320",
                            "sortOrder": 0,
                            "label": "Annan känd fusionspartner, vilken",
                            "compareDescription": "Annan känd fusionspartner, vilken",
                            "isComparable": false,
                            "serversideOnly": false
                        }
                    },
                    "vdlists": {},
                    "terms": {
                        "4075": {
                            "name": "Text(8000)",
                            "shortName": "TEXT",
                            "description": "Textvariabel med möjlighet att lagra maximalt antal tecken (8000).",
                            "dataType": "string",
                            "maxLength": 8000,
                            "validCharacters": null
                        },
                        "9876": {
                            "name": "MP02_RearrAnalys",
                            "shortName": "MP02_RearrAnalys",
                            "description": "Vilka rearrangemang har analyserats",
                            "dataType": "list",
                            "listValues": [
                                {
                                    "id": 20136,
                                    "text": "ALK",
                                    "value": "1",
                                    "validFrom": "0001-01-01T00:00:00",
                                    "validTo": "9999-12-31T00:00:00"
                                },
                                {
                                    "id": 20139,
                                    "text": "ROS1",
                                    "value": "2",
                                    "validFrom": "0001-01-01T00:00:00",
                                    "validTo": "9999-12-31T00:00:00"
                                },
                                {
                                    "id": 20140,
                                    "text": "RET",
                                    "value": "3",
                                    "validFrom": "0001-01-01T00:00:00",
                                    "validTo": "9999-12-31T00:00:00"
                                },
                                {
                                    "id": 20138,
                                    "text": "NTRK1",
                                    "value": "4",
                                    "validFrom": "0001-01-01T00:00:00",
                                    "validTo": "9999-12-31T00:00:00"
                                },
                                {
                                    "id": 20141,
                                    "text": "Annat",
                                    "value": "5",
                                    "validFrom": "0001-01-01T00:00:00",
                                    "validTo": "9999-12-31T00:00:00"
                                }
                            ]
                        },
                        "6290": {
                            "name": "MP02_Rearr",
                            "shortName": "MP02_Rearr",
                            "description": "Uppvisar rearrangemang",
                            "dataType": "list",
                            "listValues": [
                                {
                                    "id": 20034,
                                    "text": "Nej",
                                    "value": "0",
                                    "validFrom": "0001-01-01T00:00:00",
                                    "validTo": "9999-12-31T00:00:00"
                                },
                                {
                                    "id": 20035,
                                    "text": "Ja",
                                    "value": "1",
                                    "validFrom": "0001-01-01T00:00:00",
                                    "validTo": "9999-12-31T00:00:00"
                                }
                            ]
                        },
                        "6293": {
                            "name": "MP02_Fusionspartner",
                            "shortName": "MP02_Fusionspartner",
                            "description": "Känd fusionspartner",
                            "dataType": "list",
                            "listValues": [
                                {
                                    "id": 20059,
                                    "text": "Nej",
                                    "value": "0",
                                    "validFrom": "0001-01-01T00:00:00",
                                    "validTo": "9999-12-31T00:00:00"
                                },
                                {
                                    "id": 20060,
                                    "text": "Ja",
                                    "value": "1",
                                    "validFrom": "0001-01-01T00:00:00",
                                    "validTo": "9999-12-31T00:00:00"
                                }
                            ]
                        }
                    },
                    "treeLevel": 4
                }
            },
            "designedFormId": 6791,
            "data": {
                "id": 9576,
                "subTables": {
                    "Egfr": [],
                    "Raf": [],
                    "Ras": [],
                    "Alk": [],
                    "AndraGener": [],
                    "Fusionsanalys": [],
                    "CNV": []
                },
                "regvars": {
                    "MP_Initierat": {
                        "value": "Jonas Celander Guss",
                        "include": true
                    },
                    "MP_Inrapp": {
                        "value": null,
                        "include": true
                    },
                    "MP_InrappDtm": {
                        "value": null,
                        "include": true
                    },
                    "MP_InrappEnh": {
                        "value": "OC Demo (0) - SU/Sahlgrenska (500010) - Medicinkliniken (101)",
                        "include": true
                    },
                    "MP_Klinik": {
                        "value": "101",
                        "include": true
                    },
                    "MP_MonKom": {
                        "value": null,
                        "include": true
                    },
                    "MP00_AnalysDtm": {
                        "value": null,
                        "include": true
                    },
                    "MP00_InternId": {
                        "value": null,
                        "include": true
                    },
                    "MP00_MorfKlass": {
                        "value": null,
                        "include": true
                    },
                    "MP00_PAD": {
                        "value": null,
                        "include": true
                    },
                    "MP00_ProvMtrl": {
                        "value": null,
                        "include": true
                    },
                    "MP00_ProvMtrlVad": {
                        "value": null,
                        "include": true
                    },
                    "MP00_TumLok": {
                        "value": null,
                        "include": true
                    },
                    "MP00_UppTumHalt": {
                        "value": null,
                        "include": true
                    },
                    "MP00_Fragestallning": {
                        "value": null,
                        "include": true
                    },
                    "MP01_MutAnalys": {
                        "value": null,
                        "include": true
                    },
                    "MP01_Mutation": {
                        "value": null,
                        "include": true
                    },
                    "MP01_Panel": {
                        "value": null,
                        "include": true
                    },
                    "MP01_TekMutDet": {
                        "value": null,
                        "include": true
                    },
                    "MP01_TekVad": {
                        "value": null,
                        "include": true
                    },
                    "MP02_AnnFusion": {
                        "value": null,
                        "include": true
                    },
                    "MP02_AnnFusionVad": {
                        "value": null,
                        "include": true
                    },
                    "MP02_FusGenAnalys": {
                        "value": null,
                        "include": true
                    },
                    "MP02_PadKloss": {
                        "value": null,
                        "include": true
                    },
                    "MP02_PadKlossVilken": {
                        "value": null,
                        "include": true
                    },
                    "MP04_CNVUtford": {
                        "value": null,
                        "include": true
                    },
                    "MP04_CNVTeknik": {
                        "value": null,
                        "include": true
                    },
                    "MP04_CNVTeknikAnnan": {
                        "value": null,
                        "include": true
                    },
                    "MP04_CNVPavisas": {
                        "value": null,
                        "include": true
                    },
                    "MP04_CNVVilken": {
                        "value": null,
                        "include": true
                    },
                    "MP03_Biobank": {
                        "value": null,
                        "include": true
                    },
                    "MP03_BiobankMtrl": {
                        "value": null,
                        "include": true
                    },
                    "MP03_BiobankMtrlVad": {
                        "value": null,
                        "include": true
                    },
                    "MP01_AllelVad": {
                        "value": null,
                        "include": true
                    },
                    "MP01_NGSVad": {
                        "value": null,
                        "include": true
                    },
                    "MP01_NGSPanel": {
                        "value": null,
                        "include": true
                    },
                    "MP00_MorfAnnan": {
                        "value": null,
                        "include": true
                    },
                    "MP00_MorfAnnanLung": {
                        "value": null,
                        "include": true
                    }
                },
                "vdlists": {
                    "Lunga_Anmälan_VD": {
                        "include": true
                    },
                    "MolPat_VD": {
                        "include": true
                    }
                },
                "errandId": 6681995
            },
            "listValuesValidFrom": "0001-01-01",
            "listValuesValidTo": "9999-12-31",
            "env": {
                "_INREPNAME": "Jonas Celander Guss",
                "_INUNITNAME": "OC Demo (0) - SU/Sahlgrenska (500010) - Medicinkliniken (101)",
                "_REPORTERNAME": "Jonas Celander Guss",
                "_ADDRESS": "AVENYN",
                "_AVREGDATUM": "",
                "_AVREGORSAK": "",
                "_BEFOLKNINGSREGISTER": "Demo Befolkningsregister",
                "_CITY": "GÖTEBORG",
                "_DATEOFDEATH": "",
                "_DISTRICT": "",
                "_DODSORSAK": "",
                "_DODSORSAKSBESKR": "",
                "_FIRSTNAME": "FIKTIV",
                "_FODELSEDATUM": "1912-12-12",
                "_FORSAMLING": "01",
                "_KOMMUN": "80",
                "_LAN": "14",
                "_LAND": "Sweden",
                "_LKF": "148001",
                "_NAVETDATUM": "2017-04-18",
                "_PATIENTID": "2",
                "_PERSNR": "19121212-1212",
                "_POSTALCODE": "30000",
                "_SECRET": "False",
                "_SEX": "M",
                "_SURNAME": "PERSON"
            },
            "isReadOnly": false,
            "useHtmlLayout": true,
            "width": 900,
            "name": "Molekylärpatologi"
        },
        "overview": {
            "supportsTabs": true,
            "tabs": {}
        }
    },
    "recording": {
        "getValueDomainValues": {
            "[[\"error\",null],[\"parameters\",[[\"patientid\",\"2\"]]],[\"success\",null],[\"vdlist\",\"VD_Lunga_Anmälan_VD\"]]": [
                [
                    {
                        "text": "Diagnos: Cytologisk/histologisk diagnos föreligger ej, Beslutsdatum: 2016-11-21",
                        "id": 112429,
                        "data": {
                            "R418T3804_ID": 112429,
                            "A_behbesdtm": "2016-11-21",
                            "A_diagnos_Beskrivning": "Cytologisk/histologisk diagnos föreligger ej",
                            "A_diagnos_Id": 7477,
                            "A_diagnos_Värde": "91"
                        }
                    }
                ]
            ],
            "[[\"error\",null],[\"parameters\",[[\"patientid\",\"2\"]]],[\"success\",null],[\"vdlist\",\"VD_MolPat_VD\"]]": [
                [
                    {
                        "text": "PAD: 1234 ID: 12341 Datum: 2012-12-12",
                        "id": 6811,
                        "data": {
                            "MP00_pk": 6811,
                            "MP00_PAD": "1234",
                            "MP00_InternId": "12341",
                            "MP00_AnalysDtm": "2012-12-12",
                            "REGISTERRECORD_ID": 5242100
                        }
                    },
                    {
                        "text": "PAD: 43211 ID: 4321 Datum: 2012-12-14",
                        "id": 6815,
                        "data": {
                            "MP00_pk": 6815,
                            "MP00_PAD": "43211",
                            "MP00_InternId": "4321",
                            "MP00_AnalysDtm": "2012-12-14",
                            "REGISTERRECORD_ID": 5242106
                        }
                    }
                ]
            ]
        },
        "getRegisterRecordData": {}
    }
}
    }
)