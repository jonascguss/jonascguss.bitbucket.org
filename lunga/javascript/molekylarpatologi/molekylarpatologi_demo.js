$(document).ready(function () {

    /**
     *  Initialize viewmodel
     */
    var validering = new RCC.Validation();
    var vm = new RCC.ViewModel({
        validation: validering
    });
    window.vm = vm;

    // stäng av runtime validering
    vm.$validation.enabled(false);

    var lastFocused;
    $('#rcc-form').on({
        'focus': function () {
            lastFocused = $(this);
        }
    }, 'input, select');

    setTimeout(function () {
        $("#rcc-progressbar").dialog({
            height: 60,
            width: 245,
            position: ['middle', 20],
            modal: true
        });
    }, 1);

    var requestCount = ko.observable(0);

    // variabel för att bocka för/bocka av alla include kryssrutor
    vm.include = ko.observable(true);

    // variabel för att visa/dölja alla gömda delar av formuläret
    vm.forceShow = ko.observable(false);

    // variabel för att hålla koll på om formuläret har laddat färdigt
    vm.initialized = ko.observable(false);

    // variabel för att hålla koll på om värdedomän används i formuläret
    vm.loadedVD = ko.observable(false);

    vm.normalErrand = ko.observable(false);
    vm.registerPost = ko.observable(false);
    vm.originalHandling = ko.observable(false);
    vm.externalInca = ko.observable(false);

    if (!inca.errand)
        vm.registerPost(true);
    else if (inca.form.isReadOnly)
        vm.originalHandling(true);
    else
        vm.normalErrand(true);

    if (inca.errand && typeof (inca.errand.status.val) == "undefined")
        vm.externalInca(true);

    if (vm.normalErrand()) {
        // Initiera värdedomänsvariabler
        vm.$vd.Lunga_Anmälan_VD.listValues = ko.observableArray(undefined);

        vm.anmalanDropDown = ko.observable(false);

        vm.RegisterDataExist = ko.computed(function () {
            return (vm.$vd.Lunga_Anmälan_VD());
        });
    }

    vm.$vd.MolPat_VD.listValues = ko.observableArray(undefined);

    vm.komplId = ko.observable(undefined);

    /**
     *  VALIDERING
     */
    vm.validateData = function () {

        if (inca.errand.action) {
            var actionsWithoutValidation = ['1', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12',
                '13', '14', '15', '16', '17', '18', '19', '20', '21', '22',
                '23', '24', '25', '26', '27', '28', '29', '39', '41', '46'
            ];

            var actionsWithConnect = ['2', '4', '23', '28', '29'];

            var action = inca.errand.action.val();

            // om tomt val.
            if (action == '') {
                return false;
            }

            // om komplettera.
            if (action == 3) {
                var activeid = inca.overview.tabs.getActiveTabId()

                if (inca.overview.supportsTabs) {
                    inca.overview.tabs.add(function (newTabId) {

                        // Visa registerpost att komplettera i den nya fliken
                        inca.overview.tabs.setContent(newTabId, 'registerrecord', vm.komplId(), 'Komplettera');

                        // Aktivera den nya fliken
                        inca.overview.tabs.activate(newTabId);

                        // stäng den gamla fliken
                        inca.overview.tabs.remove(activeid)

                        return true;
                    });
                } else
                    return false;

            }

            // om avbryt och radera.
            if (action == 6) {
                if (!warningDialog(C_WARNING_AVBRYT))
                    return false;
                else
                    return true;
            }

            // om makulera
            if (action == 46) {
                if (!warningDialog(C_WARNING_MAKULERA))
                    return false;
            }

            // om åtgärd utan validering
            if ($.inArray(action, actionsWithoutValidation) >= 0) {
                return true;
            }

            if (vm.$vd.Lunga_Anmälan_VD() && action == 34) {
                getInfoDialog("Det finns en registerpost för anmälan", 'Det finns en anmälan i lungcancerregistret att knyta till och ärendet ska således inte skickas till RCC utan sparas i registret.');
                return false;
            }

            if (!vm.$vd.Lunga_Anmälan_VD() && vm.$vd.Lunga_Anmälan_VD.listValues().length > 1 && action == 54) {
                getInfoDialog("Ingen registerpost vald för lungcanceranmälan", 'Det finns flera anmälan i lungcancerregistret att knyta till och en måste väljas i listan över anmälningar i formuläret. Den finns som en lista under rubriken "Information från registret".');
                return false;
            } else if (!vm.$vd.Lunga_Anmälan_VD() && action == 54) {
                getInfoDialog("Finns ingen registerpost för anmälan", 'Det finns ingen anmälan i lungcancerregistret att knyta till och ärendet måste således skickas till RCC.');
                return false;
            }

            if (!vm.$vd.Lunga_Anmälan_VD() && vm.$vd.Lunga_Anmälan_VD.listValues().length > 1 && action == 2) {
                getInfoDialog("Ingen registerpost vald för lungcanceranmälan", 'Det finns flera anmälan i lungcancerregistret att knyta till och en måste väljas i listan över anmälningar i formuläret. Den finns som en lista under rubriken "Information från registret".');
                return false;
            } else if (!vm.$vd.Lunga_Anmälan_VD() && action == 2) {
                getInfoDialog("Finns ingen registerpost för anmälan", 'Det finns ingen anmälan i lungcancerregistret att knyta till och ärendet kan således inte göras klart.');
                return false;
            }

            // formuläret ska valideras
            else {
                vm.$validation.enabled(true);
                var errors = validering.errors();
                var antalFel = errors.length;
                var ejValtDataError = "";

                if (!vm.mutationOK()) {
                    antalFel++;
                    if (vm.egfrCheck().error) {
                        ejValtDataError += "<li style='margin-left:10px;'>" + "EGFR-mutation" + ":</li><li style='margin-left:20px; padding-bottom: 5px;'>" + vm.egfrCheck().reason + "</li>";
                    }

                    if (vm.rafCheck().error) {
                        ejValtDataError += "<li style='margin-left:10px;'>" + "RAF-mutation" + ":</li><li style='margin-left:20px; padding-bottom: 5px;'>" + vm.rafCheck().reason + "</li>";
                    }

                    if (vm.rasCheck().error) {
                        ejValtDataError += "<li style='margin-left:10px;'>" + "RAS-mutation" + ":</li><li style='margin-left:20px; padding-bottom: 5px;'>" + vm.rasCheck().reason + "</li>";
                    }

                    if (vm.alkCheck().error) {
                        ejValtDataError += "<li style='margin-left:10px;'>" + "RAS-mutation" + ":</li><li style='margin-left:20px; padding-bottom: 5px;'>" + vm.rasCheck().reason + "</li>";
                    }

                    ejValtDataError += "<li style='margin-left:10px;'>" + "Mutation i andra gener" + ":</li><li style='margin-left:20px; padding-bottom: 5px;'>" + "Har ej angivits eller stämmer inte överrens med valet om uppvisar mutation i formuläret." + "</li>";
                }
                // fel finns, visa och avbryt åtgärd
                if (antalFel > 0) {
                    // Visa alla fel och valideringsfel
                    validering.markAllAsAccessed();

                    // start bygget av felmeddelande och lägg till antal fel
                    var sErrorMessage = "<p><b>Formuläret innehåller " + antalFel + " fel.</b></p><ul style='margin:0; padding:5px; list-style:none;'>";

                    // lägg till samtliga felande registervariablers beskrivningar och felmeddelanden
                    ko.utils.arrayForEach(errors, function (error) {
                        sErrorMessage = sErrorMessage + "<li style='margin-left:10px;'>" + vm[error.info.source.name].rcc.regvar.compareDescription +
                            ":</li><li style='margin-left:20px; padding-bottom: 5px;'>" + error.message + "</li>";
                    });

                    sErrorMessage = sErrorMessage + ejValtDataError + "</ul>";

                    // visa felmeddelande
                    getValidationDialog("Validering", sErrorMessage);

                    // deaktivera formulärvalidering
                    vm.$validation.enabled(false);

                    return false;
                }
                // inga fel, skicka
                else {
                    vm.actionsToDoBeforePost();
                    return true;
                }
            }
        }
    };

    /**
     *  Actions to do before post
     */
    vm.actionsToDoBeforePost = function () {
        var action = inca.errand.action.val();

        // om åtgärd är "Klar", "Klar sänd till OC" eller "Spara i register"
        if (action == 2 || action == 34 || action == 54) {
            // om inget inrapporteringsdatum är satt
            if (!vm.MP_InrappDtm())
                vm.MP_InrappDtm(getDagensDatum());
            // om inrapportör och ingen inrapportör är satt
            if (!vm.MP_Inrapp()) {
                var reporterName = inca.user.firstName + " " + inca.user.lastName;
                vm.MP_Inrapp(reporterName);
            }
        }
    };

    /**
     *  Hantera återkoppling från jämförelse
     */
    vm.handleCompareResult = function (data) {
        var shortName;

        for (shortName in data) {
            // Fyll inca.form.data med resultatet av jämförelsen
            // include motsvarar kryssrutorna för granskande roller
            if (inca.user.role.isReviewer) {
                vm[shortName].rcc.include(data[shortName].include);
            }
        }
    };

    // kontrollera äkta inca
    if (vm.normalErrand() && !vm.externalInca()) {
        // Temp fix for import data bug in INCA
        inca.off('validation');
        inca.off('comparison');

        inca.on('validation', vm.validateData);
        inca.on('comparison', vm.handleCompareResult);
    }

    /**
     *  INITIERING
     */

    /**
     *  OnLoad function
     */
    vm.runFunctionsOnLoad = function () {
        var helpImgSrc = '../../Public/Files/uppsala/common-libs/icons/info.png';
        var progressbarImgSrc = "../../Public/Files/uppsala/common-libs/icons/progressbar.gif";

        $('img.rcc-help-icon').each(function () {
            $(this).attr('src', helpImgSrc);
        });

        $('img.rcc-progressbar-icon').each(function () {
            $(this).attr('src', progressbarImgSrc);
        });

        //Kontrollera äkta inca och inte registerpost
        if (vm.normalErrand()) {
            if (!vm.externalInca()) {
                // om inrapportör
                if (!inca.user.role.isReviewer)
                    parent.$('#OverviewErrandHandler_ConnectionInfoLabel').css("visibility", "hidden");

                // om monitor
                if (inca.user.role.isReviewer) {

                    // visa/dölj organisation
                    if (inca.errand.status.val() == C_STATUS_MONITOR)
                        inca.errand.showReceivers();
                    else
                        inca.errand.hideReceivers();

                    if (inca.errand.action) {
                        inca.errand.action.change(function () {
                            if ((inca.errand.status.val() == C_STATUS_MONITOR) || (inca.errand.action.val() == C_EVENTS_SKICKA_TILL_ANNAT_OC) || (inca.errand.action.val() == C_EVENTS_SKICKA_PA_REMISS) || (inca.errand.action.val() == C_EVENTS_REMISS_DELSPARA))
                                inca.errand.showReceivers();
                            else
                                inca.errand.hideReceivers();
                        });
                    }
                }

                // kopiera reciver till inrapporterande sjukhus vid ändring
                if (inca.errand.status.val() == C_STATUS_MONITOR) {
                    inca.errand.receiver.change(function () {
                        if (inca.errand.receiver.val()) {
                            var org = inca.errand.receiver.selectedText();
                            vm.MP_InrappEnh(org);
                            vm.MP_Klinik(getPathologyCode(org));
                        }
                    });
                }
            }

            // kopiera systemvariabler
            vm.copySystemVariables();

            // ange speciell validering och ej obligatoriska värden
            vm.setCustomValidation();

            //Ladda värdedomän
            vm.loadVD();
        }

        if (!inca.user.role.isReviewer) {
            if (vm.registerPost()) {
                //Ladda MolPat värdedomän för registerpost
                vm.loadRegisterPostMolPatVD();
            } else if (vm.normalErrand()) {
                //Ladda MolPat värdedomän för registerpost
                vm.loadNormalErrandMolPatVD();
            }
        }

        // modifiera listor
        vm.modifyLists();
    };

    /**
     * Funktion för åtgärder efter att formuläret har initializeras
     *
     * @method runFunctionsAfterLoad
     */
    vm.runFunctionsAfterLoad = function () {

        // flagga formuläret som färdig initilaizerat
        vm.initialized(true);

        // visa formuläret
        $('#rcc-content').removeClass('rcc-hidden');

        // fokusera på första element
        $('.rcc-focusfirst').focus();

        // ta bort progressbar
        setTimeout(function () {
            $("#rcc-progressbar").dialog("destroy");
        }, 1);
    };


    /**
     * Funktion för att ladda värdedomänslogik
     *
     * @method loadVD
     */
    vm.loadVD = function () {
        requestCount(requestCount() + 1);
        inca.form.getValueDomainValues({
            vdlist: "VD_Lunga_Anmälan_VD",
            parameters: {
                patientid: inca.form.env._PATIENTID
            },
            success: function (list) {
                // Fyll värdedomänvariabelns lista med registrerade anmälningar
                vm.$vd.Lunga_Anmälan_VD.listValues(list);
                // Om det endast finns en anmälning registrerad sätt värdedomänvariabeln till denna
                if (!vm.$vd.Lunga_Anmälan_VD() && vm.$vd.Lunga_Anmälan_VD.listValues().length == 1) {
                    vm.$vd.Lunga_Anmälan_VD(vm.$vd.Lunga_Anmälan_VD.listValues()[0].id);
                }

                requestCount(requestCount() - 1);

                if (vm.$vd.Lunga_Anmälan_VD.listValues().length < 1) {
                    //Visa inte felmeddelande för registerpost ...
                    if (!vm.$form.isReadOnly) {
                        if (inca.user.role.isReviewer) {
                            getInfoDialog("Det fanns ingen registerpost för anmälan", "Det fanns ingen anmälan registrerad på patienten och detta ärende skickades för koppling till RCC.");
                        } else {
                            getInfoDialog("Kan inte ladda registerpost för anmälan", "Det fanns ingen anmälan registrerad på patienten och detta ärende måste således sändas till RCC när det är klart.");
                        }
                    }
                } else if (vm.$vd.Lunga_Anmälan_VD.listValues().length > 1) {
                    //Visa bara meddelandet om fler än 1 anmälan och ingen anmälan är vald i dropdownlistan
                    vm.anmalanDropDown(true);
                    if (!vm.$vd.Lunga_Anmälan_VD()) {
                        getInfoDialog("Fler än 1 anmälan.", "Det finns fler än 1 registrerad anmälan, välj i listan vilken anmälan blanketten ska lämnas in för.", lastFocused);
                    }
                }
            },
            error: function (err) {
                requestCount(requestCount() - 1);
                $('<div title="Ett fel har uppstått"></div>')
                    .text('Lyckades inte ladda registerposten: ' + err + ', ' + JSON.stringify(err) + '. Försök att ladda om formuläret.')
                    .dialog({
                        buttons: {
                            OK: function () {
                                $(this).dialog('close');
                            }
                        }
                    });
            }
        });
    };

    vm.loadNormalErrandMolPatVD = function () {
        requestCount(requestCount() + 1);
        inca.form.getValueDomainValues({
            vdlist: "VD_MolPat_VD",
            parameters: {
                patientid: inca.form.env._PATIENTID
            },
            success: function (data) {
                vm.$vd.MolPat_VD.listValues(data);

                // initiera subscriber för att ladda rätt uppföljning beroende på uppföljningstyp
                vm.$vd.MolPat_VD.subscribe(function (newValue) {
                    if (newValue) {
                        vm.clearVM();
                        vm.komplId(vm.getSelectedMolPat(newValue).REGISTERRECORD_ID);

                        $('select[name=selectedStateTransition] option', window.parent.document).each(function () {
                            var $thisOption = $(this);
                            if ($thisOption.val()) {
                                if ($thisOption.val() != 3)
                                    $thisOption.attr("disabled", "disabled");
                                else if ($thisOption.val() == 3)
                                    $thisOption.removeAttr("disabled");
                            }
                        });

                    } else {
                        vm.komplId(undefined);
                        $('select[name=selectedStateTransition] option', window.parent.document).each(function () {
                            var $thisOption = $(this);

                            if ($thisOption.val()) {
                                if ($thisOption.val() == 3)
                                    $thisOption.attr("disabled", "disabled");
                                else if ($thisOption.val() != 3)
                                    $thisOption.removeAttr("disabled");
                            }
                        });
                    }
                });

                vm.$vd.MolPat_VD.valueHasMutated();
                requestCount(requestCount() - 1);
            },
            error: function (err) {
                requestCount(requestCount() - 1);
                $('<div title="Ett fel har uppstått"></div>')
                    .text('Lyckades inte ladda rader från värdedomänen: ' + err + ', ' + JSON.stringify(err) + '. Försök att ladda om formuläret.')
                    .dialog({
                        buttons: {
                            OK: function () {
                                $(this).close();
                            }
                        }
                    });
                $(inca.form.getContainer()).hide();
            }
        });
    };

    vm.loadRegisterPostMolPatVD = function () {
        requestCount(requestCount() + 1);

        $('.btn-group', window.parent.document).hide();

        inca.form.getValueDomainValues({
            vdlist: "VD_MolPat_VD",
            parameters: {
                patientid: inca.form.env._PATIENTID,
                registerpostid: inca.form.data.registerRecordId
            },
            success: function (data) {
                vm.$vd.MolPat_VD.subscribe(function (newValue) {
                    if (newValue) {
                        $('.btn-group', window.parent.document).show();
                    } else {
                        $('.btn-group', window.parent.document).hide();
                    }
                });

                vm.$vd.MolPat_VD.listValues(data);
                if (!vm.$vd.MolPat_VD() && vm.$vd.MolPat_VD.listValues().length == 1) {
                    vm.$vd.MolPat_VD(vm.$vd.MolPat_VD.listValues()[0].id);
                }

                vm.$vd.MolPat_VD.valueHasMutated();
                if (!vm.$vd.MolPat_VD()) {
                    requestCount(requestCount() - 1);
                }
            },
            error: function (err) {
                //console.log("fel");
                requestCount(requestCount() - 1);
                $('<div title="Ett fel har uppstått"></div>')
                    .text('Lyckades inte ladda rader från värdedomänen: ' + err + ', ' + JSON.stringify(err) + '. Försök att ladda om formuläret.')
                    .dialog({
                        buttons: {
                            OK: function () {
                                $(this).close();
                            }
                        }
                    });
                $(inca.form.getContainer()).hide();
            }
        });
    };

    /**
     * Funktion för att returnera en registerpost från Lunga Anmälan värdedomän som ett dataobjekt.
     *
     * @method getSelectedAnmalan
     * @param {String} pk Primärnyckelvärde för dataobjekt
     * @return {Object} Dataobject
     */
    vm.getSelectedAnmalan = function (pk) {
        if (pk != null) {
            var item = ko.utils.arrayFirst(vm.$vd.Lunga_Anmälan_VD.listValues() || [], function (item) {
                return item.id == pk;
            });

            if (!item)
                return undefined;

            return item.data;
        } else {
            return undefined;
        }
    };

    /**
     * Funktion för att returnera en registerpost från MolPat värdedomän som ett dataobjekt.
     *
     * @method getSelectedMolPat
     * @param {String} pk Primärnyckelvärde för dataobjekt
     * @return {Object} Dataobject
     */
    vm.getSelectedMolPat = function (pk) {
        if (pk != null) {
            var item = ko.utils.arrayFirst(vm.$vd.MolPat_VD.listValues() || [], function (item) {
                return item.id == pk;
            });

            if (!item)
                return undefined;

            return item.data;
        } else {
            return undefined;
        }
    };

    /**
     * Funktion för att justera valideringar
     */
    vm.setCustomValidation = function () {
        if (vm.normalErrand()) {
            //Registerintern värdedomän
            notRequired(vm.$vd.Lunga_Anmälan_VD);
            notRequired(vm.$vd.MolPat_VD);
        }

        //Kommentarvariabler
        notRequired(vm.MP_MonKom);

        //Inrapporteringsdatum
        notRequired(vm.MP_InrappDtm);

        // Monitorspecifika variabler
        if (!inca.user.role.isReviewer) {
            notRequired(vm.MP_Inrapp);
            notRequired(vm.MP_InrappEnh);
            notRequired(vm.MP_Klinik);
        }
    };

    /**
     * Funktion för att kopiera utvalda systemvariabler till registrets variabler(om inte redan sparade)
     *
     * @method copySystemVariables
     */
    vm.copySystemVariables = function () {
        // Enhetsnamn
        if (!inca.user.role.isReviewer && !vm.MP_InrappEnh()) {
            vm.MP_InrappEnh(inca.user.position.fullNameWithCode);

            // sjukhuskod
            // klinikkod
            vm.MP_Klinik(getPathologyCode(inca.user.position.fullNameWithCode));
        }

        // initierad av
        if (!vm.MP_Initierat())
            vm.MP_Initierat(inca.user.firstName + " " + inca.user.lastName);
    };

    /**
     *  Funktion för att manipulera listor
     */
    vm.modifyLists = function () { };


    if (vm.normalErrand()) {
        /**
         *  BERÄKNANDE VALIDERING
         */

        /** Beräknande validering för:
         - Inrapportör {vm.MP_Inrapp}
         - Inrapporterande sjukhus {vm.MP_InrappEnh}
         - Inrapporterande sjukhus, sjukhuskod {vm.MP_Sjukhus}
         - Inrapporterande sjukhus, klinikkod {vm.MP_Klinik}
         - Anmälande läkare {vm.MP_Lakare}
         */
        ko.computed(function () {
            if (vm.initialized()) {
                if (inca.user.role.isReviewer) {
                    required(vm.MP_Inrapp);
                    required(vm.MP_InrappEnh);
                    required(vm.MP_Klinik);
                }
            }
        });

        /**
         * Morfologisk klass styr över vad för annan cancer och annan lungcancer
         */
        ko.computed(function () {
            if (vm.initialized())
                if (!vm.MP00_MorfKlass() || vm.MP00_MorfKlass().value != '5') {
                    clear(vm.MP00_MorfAnnanLung);
                } else if (!vm.MP00_MorfKlass() || vm.MP00_MorfKlass().value != '6') {
                    clear(vm.MP00_MorfAnnan);
                }
        });

        /**
         * Typ av provmaterial styr över annat provmaterial
         */
        ko.computed(function () {
            if (vm.initialized())
                if ((!vm.MP00_ProvMtrl() || vm.MP00_ProvMtrl().value != '6'))
                    notRequiredAndClear(vm.MP00_ProvMtrlVad);
                else
                    required(vm.MP00_ProvMtrlVad);
        });

        /**
         * Är mutationsanalys genomförd?
         *
         * - MP01_MutAnalys
         *      - MP01_TekMutDet
         *          - MP01_Panel
         *      - MP01_Mutation
         *          - Egfr
         *          - Ras
         *          - Raf
         */
        ko.computed(function () {
            if (vm.initialized()) {
                if ((!vm.MP01_MutAnalys() || vm.MP01_MutAnalys().value != '1')) {
                    notRequiredAndClear(vm.MP01_TekMutDet);
                    notRequiredAndClear(vm.MP01_Mutation);
                } else {
                    required(vm.MP01_TekMutDet);
                    required(vm.MP01_Mutation);
                }
            }
        });

        ko.computed(function () {
            if (vm.initialized()) {
                if (!vm.MP01_TekMutDet() || vm.MP01_TekMutDet().value != '1') {
                    notRequiredAndClear(vm.MP01_NGSPanel);
                } else {
                    required(vm.MP01_NGSPanel);
                }
            }
        });

        ko.computed(function () {
            if (vm.initialized()) {
                if (!vm.MP01_NGSPanel() || vm.MP01_NGSPanel().value != '8') {
                    notRequiredAndClear(vm.MP01_NGSVad);
                } else {
                    required(vm.MP01_NGSVad);
                }
            }
        });

        ko.computed(function () {
            if (vm.initialized())
                if ((!vm.MP01_TekMutDet() || vm.MP01_TekMutDet().value != '2'))
                    notRequiredAndClear(vm.MP01_Panel);
                else
                    required(vm.MP01_Panel);
        });

        ko.computed(function () {
            if (vm.initialized()) {
                if (!vm.MP01_Panel() || vm.MP01_Panel().value != '4') {
                    notRequiredAndClear(vm.MP01_AllelVad);
                } else {
                    required(vm.MP01_AllelVad);
                }
            }
        });

        ko.computed(function () {
            if (vm.initialized()) {
                if (!vm.MP01_TekMutDet() || vm.MP01_TekMutDet().value != '4') {
                    notRequiredAndClear(vm.MP01_TekVad);
                } else {
                    required(vm.MP01_TekVad);
                }
            }
        });


        ko.computed(function () {
            if (vm.initialized()) {
                //Gå igenom varje Egfr, Ras, Raf, Alk och ta bort required för om annan
                ko.utils.arrayForEach(vm.$$.Egfr(), function (item) {
                    notRequired(item.MP01_EgfrVad);
                });

                ko.utils.arrayForEach(vm.$$.Raf(), function (item) {
                    notRequired(item.MP01_RafVad);
                });

                ko.utils.arrayForEach(vm.$$.Ras(), function (item) {
                    notRequired(item.MP01_RasVad);
                });

                ko.utils.arrayForEach(vm.$$.Alk(), function (item) {
                    notRequired(item.MP01_AlkVad);
                });
            }
        });

        /**
         *
         * Har fusionsanalys genomförts?
         *
         * - MP02_PadKloss
         *      - MP02_PadKlossVilken
         * - MP02_AnnFusion
         *      - MP02_AnnFusionVad
         */
        ko.computed(function () {
            if (vm.initialized()) {
                if (!vm.MP02_FusGenAnalys() || vm.MP02_FusGenAnalys().value != '1') {
                    notRequiredAndClear(vm.MP02_PadKloss);
                    notRequiredAndClear(vm.MP02_AnnFusion);
                } else {
                    required(vm.MP02_PadKloss);
                    required(vm.MP02_AnnFusion);
                }
            }
        });

        ko.computed(function () {
            if (vm.initialized()) {
                if (!vm.MP02_PadKloss() || vm.MP02_PadKloss().value != '1') {
                    notRequiredAndClear(vm.MP02_PadKlossVilken);
                } else {
                    required(vm.MP02_PadKlossVilken);
                }
            }
        });

        ko.computed(function () {
            if (vm.initialized()) {
                if (!vm.MP02_AnnFusion() || vm.MP02_AnnFusion().value != '1') {
                    notRequiredAndClear(vm.MP02_AnnFusionVad);
                } else {
                    required(vm.MP02_AnnFusionVad);
                }
            }
        });

        /**
         *
         * Är CNV analys utförd
         *
         * - MP04_CNVUtford
         *      - MP04_CNVTeknik
         *          - MP04_CNVTeknikAnnan
         *      - MP04_CNVPavisas
         *          - MP04_CNVVilken
         *
         */
        ko.computed(function () {
            if (vm.initialized()) {
                if (!vm.MP04_CNVUtford() || vm.MP04_CNVUtford().value != '1') {
                    notRequiredAndClear(vm.MP04_CNVTeknik);
                    notRequiredAndClear(vm.MP04_CNVPavisas);
                } else {
                    required(vm.MP04_CNVTeknik);
                    required(vm.MP04_CNVPavisas);
                }
            }
        });

        ko.computed(function () {
            if (vm.initialized()) {
                if (!vm.MP04_CNVTeknik() || vm.MP04_CNVTeknik().value != '3') {
                    notRequiredAndClear(vm.MP04_CNVTeknikAnnan);
                } else {
                    required(vm.MP04_CNVTeknikAnnan);
                }
            }
        });

        ko.computed(function () {
            if (vm.initialized()) {
                if (!vm.MP04_CNVPavisas() || vm.MP04_CNVPavisas().value != '1') {
                    notRequiredAndClear(vm.MP04_CNVVilken);
                } else {
                    required(vm.MP04_CNVVilken);
                }
            }
        });

        /**
         *
         * Finns ytterligare material på fallet?
         *
         * - MP03_Biobank
         *      - MP03_BiobankMatrl
         *          - MP03_BiobankMtrlVad
         *
         */
        ko.computed(function () {
            if (vm.initialized()) {
                if (!vm.MP03_Biobank() || vm.MP03_Biobank().value != '1') {
                    notRequiredAndClear(vm.MP03_BiobankMtrl);
                } else {
                    required(vm.MP03_BiobankMtrl);
                }
            }
        });

        ko.computed(function () {
            if (vm.initialized()) {
                if (!vm.MP03_BiobankMtrl() || vm.MP03_BiobankMtrl().value != '4') {
                    notRequiredAndClear(vm.MP03_BiobankMtrlVad);
                } else {
                    required(vm.MP03_BiobankMtrlVad);
                }
            }
        });

    }

    // Custom validation för undertabellerna
    vm.egfrCheck = ko.computed(function () {
        if (vm.initialized()) {

            if (!vm.MP01_MutAnalys() || vm.MP01_MutAnalys().value != '1' || !vm.MP01_Mutation()) {
                return {
                    'error': false,
                    'reason': undefined
                };
            }

            if (vm.MP01_Mutation() && vm.MP01_Mutation().value == '1') {
                for (var i = 0; i < vm.$$.Egfr().length; i++) {
                    if (!vm.$$.Egfr()[i].MP01_Egfr()) {
                        return {
                            'error': true,
                            'reason': 'EGFR-mutationsangivelse saknas'
                        };
                    } else if (vm.$$.Egfr()[i].MP01_Egfr() && (vm.$$.Egfr()[i].MP01_Egfr().value == '1' || vm.$$.Egfr()[i].MP01_Egfr().value == '2' || vm.$$.Egfr()[i].MP01_Egfr().value == '3')) {
                        return {
                            'error': true,
                            'reason': '"Uppvisar mutation" har besvarats med "Ja", men en motsägande EGFR-mutation har angetts.'
                        };
                    } else if (vm.$$.Egfr()[i].MP01_Egfr() && vm.$$.Egfr()[i].MP01_Egfr().value == '43' && !vm.$$.Egfr()[i].MP01_EgfrVad()) {
                        return {
                            'error': true,
                            'reason': 'Annan EGFR mutation saknas'
                        };
                    }
                }
            }

            if (vm.MP01_Mutation() && vm.MP01_Mutation().value == '0') {
                for (var i = 0; i < vm.$$.Egfr().length; i++) {
                    if (!vm.$$.Egfr()[i].MP01_Egfr()) {
                        return {
                            'error': true,
                            'reason': 'EGFR-mutationsangivelse saknas'
                        };
                    } else if (!vm.$$.Egfr()[i].MP01_Egfr() || (vm.$$.Egfr()[i].MP01_Egfr().value != '1' && vm.$$.Egfr()[i].MP01_Egfr().value != '2' && vm.$$.Egfr()[i].MP01_Egfr().value != '3')) {
                        return {
                            'error': true,
                            'reason': '"Uppvisar mutation" har besvarats med "Nej", men sen har en EGFR-mutation angetts.'
                        };
                    }
                }
            }
            return {
                'error': false,
                'reason': undefined
            };
        }
        return {
            'error': true,
            'reason': 'Formuläret har inte initialiserats korrekt, prova att välja "Ej klar, kvar i inkorg" och öppna det igen. Om felet består, kontakta regional INCA-support'
        };
    });

    vm.rafCheck = ko.computed(function () {
        if (vm.initialized()) {

            if (!vm.MP01_MutAnalys() || vm.MP01_MutAnalys().value != '1' || !vm.MP01_Mutation()) {
                return {
                    'error': false,
                    'reason': undefined
                };
            }

            if (vm.MP01_Mutation() && vm.MP01_Mutation().value == '1') {
                for (var i = 0; i < vm.$$.Raf().length; i++) {
                    if (!vm.$$.Raf()[i].MP01_Raf()) {
                        return {
                            'error': true,
                            'reason': 'RAF-mutationsangivelse saknas'
                        };
                    } else if (vm.$$.Raf()[i].MP01_Raf() && (vm.$$.Raf()[i].MP01_Raf().value == '1' || vm.$$.Raf()[i].MP01_Raf().value == '2' || vm.$$.Raf()[i].MP01_Raf().value == '3')) {
                        return {
                            'error': true,
                            'reason': '"Uppvisar mutation" har besvarats med "Ja", men en motsägande RAF-mutation har angetts.'
                        };
                    } else if (vm.$$.Raf()[i].MP01_Raf() && vm.$$.Raf()[i].MP01_Raf().value == '43' && !vm.$$.Raf()[i].MP01_RafVad()) {
                        return {
                            'error': true,
                            'reason': 'Annan RAF mutation saknas'
                        };
                    }
                }
            }

            if (vm.MP01_Mutation() && vm.MP01_Mutation().value == '0') {
                for (var i = 0; i < vm.$$.Raf().length; i++) {
                    if (!vm.$$.Raf()[i].MP01_Raf()) {
                        return {
                            'error': true,
                            'reason': 'RAF-mutationsangivelse saknas'
                        };
                    } else if (!vm.$$.Raf()[i].MP01_Raf() || (vm.$$.Raf()[i].MP01_Raf().value != '1' && vm.$$.Raf()[i].MP01_Raf().value != '2' && vm.$$.Raf()[i].MP01_Raf().value != '3')) {
                        return {
                            'error': true,
                            'reason': '"Uppvisar mutation" har besvarats med "Nej", men sen har en RAF-mutation angetts.'
                        };
                    }
                }
            }
            return {
                'error': false,
                'reason': undefined
            };
        }
        return {
            'error': true,
            'reason': 'Formuläret har inte initialiserats korrekt, prova att välja "Ej klar, kvar i inkorg" och öppna det igen. Om felet består, kontakta regional INCA-support'
        };
    });

    vm.rasCheck = ko.computed(function () {
        if (vm.initialized()) {

            if (!vm.MP01_MutAnalys() || vm.MP01_MutAnalys().value != '1' || !vm.MP01_Mutation()) {
                return {
                    'error': false,
                    'reason': undefined
                };
            }

            if (vm.MP01_Mutation() && vm.MP01_Mutation().value == '1') {
                for (var i = 0; i < vm.$$.Ras().length; i++) {
                    if (!vm.$$.Ras()[i].MP01_Ras()) {
                        return {
                            'error': true,
                            'reason': 'RAS-mutationsangivelse saknas'
                        };
                    } else if (vm.$$.Ras()[i].MP01_Ras() && (vm.$$.Ras()[i].MP01_Ras().value == '1' || vm.$$.Ras()[i].MP01_Ras().value == '2' || vm.$$.Ras()[i].MP01_Ras().value == '3')) {
                        return {
                            'error': true,
                            'reason': '"Uppvisar mutation" har besvarats med "Ja", men en motsägande RAS-mutation har angetts.'
                        };
                    } else if (vm.$$.Ras()[i].MP01_Ras() && vm.$$.Ras()[i].MP01_Ras().value == '43' && !vm.$$.Ras()[i].MP01_RasVad()) {
                        return {
                            'error': true,
                            'reason': 'Annan RAS mutation saknas'
                        };
                    }
                }
            }

            if (vm.MP01_Mutation() && vm.MP01_Mutation().value == '0') {
                for (var i = 0; i < vm.$$.Ras().length; i++) {
                    if (!vm.$$.Ras()[i].MP01_Ras()) {
                        return {
                            'error': true,
                            'reason': 'RAS-mutationsangivelse saknas'
                        };
                    } else if (!vm.$$.Ras()[i].MP01_Ras() || (vm.$$.Ras()[i].MP01_Ras().value != '1' && vm.$$.Ras()[i].MP01_Ras().value != '2' && vm.$$.Ras()[i].MP01_Ras().value != '3')) {
                        return {
                            'error': true,
                            'reason': '"Uppvisar mutation" har besvarats med "Nej", men sen har en RAS-mutation angetts.'
                        };
                    }
                }
            }
            return {
                'error': false,
                'reason': undefined
            };
        }
        return {
            'error': true,
            'reason': 'Formuläret har inte initialiserats korrekt, prova att välja "Ej klar, kvar i inkorg" och öppna det igen. Om felet består, kontakta regional INCA-support'
        };
    });

    vm.alkCheck = ko.computed(function () {
        if (vm.initialized()) {

            if (!vm.MP01_MutAnalys() || vm.MP01_MutAnalys().value != '1' || !vm.MP01_Mutation()) {
                return {
                    'error': false,
                    'reason': undefined
                };
            }

            if (vm.MP01_Mutation() && vm.MP01_Mutation().value == '1') {
                for (var i = 0; i < vm.$$.Alk().length; i++) {
                    if (!vm.$$.Alk()[i].MP01_Alk()) {
                        return {
                            'error': true,
                            'reason': 'ALK-mutationsangivelse saknas'
                        };
                    } else if (vm.$$.Alk()[i].MP01_Alk() && (vm.$$.Alk()[i].MP01_Alk().value == '1' || vm.$$.Alk()[i].MP01_Alk().value == '2' || vm.$$.Alk()[i].MP01_Alk().value == '3')) {
                        return {
                            'error': true,
                            'reason': '"Uppvisar mutation" har besvarats med "Ja", men en motsägande ALK-mutation har angetts.'
                        };
                    } else if (vm.$$.Alk()[i].MP01_Alk() && vm.$$.Alk()[i].MP01_Alk().value == '13' && !vm.$$.Alk()[i].MP01_AlkVad()) {
                        return {
                            'error': true,
                            'reason': 'Annan ALK mutation saknas'
                        };
                    }
                }
            }

            if (vm.MP01_Mutation() && vm.MP01_Mutation().value == '0') {
                for (var i = 0; i < vm.$$.Alk().length; i++) {
                    if (!vm.$$.Alk()[i].MP01_Alk()) {
                        return {
                            'error': true,
                            'reason': 'ALK-mutationsangivelse saknas'
                        };
                    } else if (!vm.$$.Alk()[i].MP01_Alk() || (vm.$$.Alk()[i].MP01_Alk().value != '1' && vm.$$.Alk()[i].MP01_Alk().value != '2' && vm.$$.Alk()[i].MP01_Alk().value != '3')) {
                        return {
                            'error': true,
                            'reason': '"Uppvisar mutation" har besvarats med "Nej", men sen har en ALK-mutation angetts.'
                        };
                    }
                }
            }
            return {
                'error': false,
                'reason': undefined
            };
        }
        return {
            'error': true,
            'reason': 'Formuläret har inte initialiserats korrekt, prova att välja "Ej klar, kvar i inkorg" och öppna det igen. Om felet består, kontakta regional INCA-support'
        };
    });

    vm.mutationOK = ko.computed(function () {
        if (vm.initialized()) {
            if (!vm.MP01_MutAnalys() || vm.MP01_MutAnalys().value != '1') {
                return true;
            }
            if (vm.MP01_MutAnalys() && vm.MP01_MutAnalys().value == '1' && !vm.MP01_Mutation()) {
                return false;
            }
            if (vm.MP01_Mutation() && vm.MP01_Mutation().value == '0') {
                if (!vm.egfrCheck().error && !vm.rafCheck().error && !vm.rasCheck().error && !vm.alkCheck().error) {
                    return true;
                }
            }
            if (vm.MP01_Mutation() && vm.MP01_Mutation().value == '1') {
                if (!vm.egfrCheck().error || !vm.rafCheck().error || !vm.rasCheck().error || !vm.alkCheck().error) {
                    return true;
                }
            }
            return false;
        }
        return false;
    });


    /**
     * TABELLFUNKTIONER
     *
     */

    /**
     * funktion för att lägga till en rad i Egfr-tabellen
     * initierar subscribers
     *
     * @method addEgfr
     */
    vm.addEgfr = function () {
        // lägg till en ny rad till Egfr-tabellen
        vm.$$.Egfr.rcc.add();
    };

    vm.addRas = function () {
        // lägg till en ny rad till Ras-tabellen
        vm.$$.Ras.rcc.add();
    };

    vm.addRaf = function () {
        // lägg till en ny rad till Raf-tabellen
        vm.$$.Raf.rcc.add();
    };

    vm.addAlk = function () {
        // lägg till en ny rad till Alk-tabellen
        vm.$$.Alk.rcc.add();
    };

    vm.addAndraGener = function () {
        // lägg till en ny rad till AndraGener-tabellen
        vm.$$.AndraGener.rcc.add();
    };

    vm.addFusionsanalys = function () {
        // lägg till en ny rad till Fusionsanalys-tabellen
        row = vm.$$.Fusionsanalys.rcc.add();

        row.MP02_Fusionanalysteknik.subscribe(function (newValue) {
            if (newValue) {
                if(row.$$.Rearrangemang().length == 0) {
                    vm.addRearrangemang(row);
                }
            } else {
                if (row.$$.Rearrangemang().length > 0) {
                    for (var i = row.$$.Rearrangemang().length - 1; i >= 0; i--) {
                        row.$$.Rearrangemang.rcc.remove(row.$$.Rearrangemang()[i]);
                    }
                }
            }
        });

        ko.computed(function () {
            if (!row.MP02_Fusionanalysteknik() || row.MP02_Fusionanalysteknik().value != '6') {
                notRequiredAndClear(row.MP02_AnnanFusionsteknik);
            } else {
                required(row.MP02_AnnanFusionsteknik);
            }
        });
    };

    vm.addRearrangemang = function (row) {
        rearr = row.$$.Rearrangemang.rcc.add();

        ko.computed(function () {
            if (!rearr.MP02_RearrAnalys() || rearr.MP02_RearrAnalys().value != '5') {
                notRequiredAndClear(rearr.MP02_RearrAnalysAnnat);
            } else {
                required(rearr.MP02_RearrAnalysAnnat);
            }
        });

        ko.computed(function () {
            if (!rearr.MP02_RearrAnalys()) {
                notRequiredAndClear(rearr.MP02_Rearr);
            } else {
                required(rearr.MP02_Rearr);
            }
        });

        ko.computed(function () {
            if (!rearr.MP02_Rearr() || rearr.MP02_Rearr().value != '1') {
                notRequiredAndClear(rearr.MP02_Fusionspartner);
            } else {
                required(rearr.MP02_Fusionspartner);
            }
        });

        ko.computed(function () {
            if (!rearr.MP02_Fusionspartner() || rearr.MP02_Fusionspartner().value != '1') {
                notRequiredAndClear(rearr.MP02_FusionspartnerVilken);
            } else {
                required(rearr.MP02_FusionspartnerVilken);
            }
        });
    };


    vm.clearVM = function () {
        for (var obj in vm) {
            if (ko.isWriteableObservable(vm[obj]) && vm.hasOwnProperty(obj) && vm[obj].rcc) {
                vm[obj](undefined);
                vm[obj].rcc.accessed(false);
            }
        }
    };


    /**
     *  SUBSCRIBERS
     */
    vm.MP01_MutAnalys.subscribe(function (newValue) {
        if (newValue && newValue.value == '1') {
            if (vm.$$.Egfr().length == 0) {
                vm.addEgfr();
            }
            if (vm.$$.Ras().length == 0) {
                vm.addRas();
            }
            if (vm.$$.Raf().length == 0) {
                vm.addRaf();
            }
            if (vm.$$.Alk().length == 0) {
                vm.addAlk();
            }
        } else {
            if (vm.$$.Egfr().length > 0) {
                for (var i = vm.$$.Egfr().length - 1; i >= 0; i--) {
                    vm.$$.Egfr.rcc.remove(vm.$$.Egfr()[i]);
                }
            }
            if (vm.$$.Ras().length > 0) {
                for (var i = vm.$$.Ras().length - 1; i >= 0; i--) {
                    vm.$$.Ras.rcc.remove(vm.$$.Ras()[i]);
                }
            }
            if (vm.$$.Raf().length > 0) {
                for (var i = vm.$$.Raf().length - 1; i >= 0; i--) {
                    vm.$$.Raf.rcc.remove(vm.$$.Raf()[i]);
                }
            }
            if (vm.$$.Alk().length > 0) {
                for (var i = vm.$$.Alk().length - 1; i >= 0; i--) {
                    vm.$$.Alk.rcc.remove(vm.$$.Alk()[i]);
                }
            }
            if (vm.$$.AndraGener().length > 0) {
                for (var i = vm.$$.AndraGener().length - 1; i >= 0; i--) {
                    vm.$$.AndraGener.rcc.remove(vm.$$.AndraGener()[i]);
                }
            }
        }
    });

    vm.MP02_FusGenAnalys.subscribe(function (newValue) {
        if (newValue && newValue.value == '1') {
            if (vm.$$.Fusionsanalys().length == 0) {
                vm.addFusionsanalys();
            }
        } else {
            if (vm.$$.Fusionsanalys().length > 0) {
                for (var i = vm.$$.Fusionsanalys().length - 1; i >= 0; i--) {
                    vm.$$.Fusionsanalys.rcc.remove(vm.$$.Fusionsanalys()[i]);
                }
            }
        }
    });


    vm.runFunctionsOnLoad();

    ko.applyBindings(vm);

    if (vm.loadedVD()) {
        vm.init = ko.computed(function () {
            if (requestCount() == 0) {
                vm.runFunctionsAfterLoad();
                vm.init.dispose();
            }
        });
    } else
        vm.runFunctionsAfterLoad();

});
