new IncaOffline(
    {
        emulate:
        {
            "inca": {
                "scriptSupport": {},
                "services": {
                    "infoText": {},
                    "report": {},
                    "viewTemplate": {},
                    "logging": {},
                    "documentation": {
                        "registerDocumentation": {},
                        "formDocumentation": {},
                        "registerVariableDocumentation": {}
                    }
                },
                "user": {
                    "id": 6969,
                    "firstName": "Inrapportör",
                    "lastName": "Larsson",
                    "username": "oc2inrapp",
                    "email": "oc2@inrapp.se",
                    "phoneNumber": "123456",
                    "role": {
                        "id": 9,
                        "name": "Inrapportör (Kvalitetsregister)",
                        "isReviewer": true
                    },
                    "position": {
                        "id": 1292,
                        "name": "Demo Klinik",
                        "code": "111",
                        "fullNameWithCode": "OC Syd (4) - Centrallasarettet i Växjö (23010) - ögon klinik (511)"
                    },
                    "region": {
                        "id": 7,
                        "name": "Region Norr"
                    },
                    "previousLogin": "2016-11-02T06:11:23.663Z"
                },
                "serverDate": "2016-11-02T07:25:41.074Z",
                "errand": {
                    "status": {
                        "0": {},
                        "length": 1,
                        "prevObject": {
                            "0": {},
                            "length": 1,
                            "prevObject": {
                                "0": {
                                    "jQuery17204603418002401014": 62
                                },
                                "context": {
                                    "jQuery17204603418002401014": 62
                                },
                                "length": 1
                            },
                            "context": {
                                "jQuery17204603418002401014": 62
                            },
                            "selector": "[data-errand-panel]"
                        },
                        "context": {
                            "jQuery17204603418002401014": 62
                        },
                        "selector": "[data-errand-panel] [data-old-id=OverviewErrandHandler_txtState]"
                    },
                    "comment": {
                        "0": {
                            "jQuery17204603418002401014": 75
                        },
                        "length": 1,
                        "prevObject": {
                            "0": {},
                            "length": 1,
                            "prevObject": {
                                "0": {
                                    "jQuery17204603418002401014": 62
                                },
                                "context": {
                                    "jQuery17204603418002401014": 62
                                },
                                "length": 1
                            },
                            "context": {
                                "jQuery17204603418002401014": 62
                            },
                            "selector": "[data-errand-panel]"
                        },
                        "context": {
                            "jQuery17204603418002401014": 62
                        },
                        "selector": "[data-errand-panel] [data-old-id=OverviewErrandHandler_txtComments]"
                    },
                    "action": {
                        "0": {
                            "0": {},
                            "1": {},
                            "2": {},
                            "3": {},
                            "4": {},
                            "jQuery17204603418002401014": 85
                        },
                        "length": 1,
                        "prevObject": {
                            "0": {},
                            "length": 1,
                            "prevObject": {
                                "0": {
                                    "jQuery17204603418002401014": 62
                                },
                                "context": {
                                    "jQuery17204603418002401014": 62
                                },
                                "length": 1
                            },
                            "context": {
                                "jQuery17204603418002401014": 62
                            },
                            "selector": "[data-errand-panel]"
                        },
                        "context": {
                            "jQuery17204603418002401014": 62
                        },
                        "selector": "[data-errand-panel] [data-old-id=OverviewErrandHandler_ddEvent]"
                    },
                    "receiver": {
                        "0": {
                            "0": {},
                            "jQuery17204603418002401014": 87
                        },
                        "length": 1,
                        "prevObject": {
                            "0": {},
                            "length": 1,
                            "prevObject": {
                                "0": {
                                    "jQuery17204603418002401014": 62
                                },
                                "context": {
                                    "jQuery17204603418002401014": 62
                                },
                                "length": 1
                            },
                            "context": {
                                "jQuery17204603418002401014": 62
                            },
                            "selector": "[data-errand-panel]"
                        },
                        "context": {
                            "jQuery17204603418002401014": 62
                        },
                        "selector": "[data-errand-panel] [data-old-id=OverviewErrandHandler_ddReceiver]"
                    },
                    "submitValidationCallback": null
                },
                "form": {
                    "metadata": {
                        "Anmalan": {
                            "subTables": [],
                            "regvars": {
                                "a_diag_besdat": {
                                    "term": 1002,
                                    "description": "Datum 1:a besök i specialiserad vård",
                                    "technicalName": "T1782049",
                                    "sortOrder": 520,
                                    "label": "Datum 1:a besök i specialiserad vård",
                                    "compareDescription": "Datum 1:a besök i specialiserad vård",
                                    "isComparable": true
                                },
                                "a_diag_dat": {
                                    "term": 1002,
                                    "description": "Provtaningsdatum/undersökningsdatum",
                                    "technicalName": "T1782050",
                                    "sortOrder": 550,
                                    "label": "Provtaningsdatum/undersökningsdatum",
                                    "compareDescription": "Provtaningsdatum/undersökningsdatum",
                                    "isComparable": true
                                },
                                "a_diag_misscadat": {
                                    "term": 1002,
                                    "description": "Beslutsdatum för välgrundad misstanke om cancer",
                                    "technicalName": "T1782047",
                                    "sortOrder": 500,
                                    "label": "Beslutsdatum för välgrundad misstanke om cancer",
                                    "compareDescription": "Beslutsdatum för välgrundad misstanke om cancer",
                                    "isComparable": true
                                },
                                "a_inr_allkom": {
                                    "term": 4075,
                                    "description": "Allmän kommentar",
                                    "technicalName": "T1782030",
                                    "sortOrder": 180,
                                    "label": "Allmän kommentar",
                                    "compareDescription": "Allmän kommentar",
                                    "isComparable": true
                                },
                                "a_inr_anmlak": {
                                    "term": 4075,
                                    "description": "Anmälande läkare",
                                    "technicalName": "T1782028",
                                    "sortOrder": 150,
                                    "label": "Anmälande läkare",
                                    "compareDescription": "Anmälande läkare",
                                    "isComparable": true
                                },
                                "a_inr_dat": {
                                    "term": 1002,
                                    "description": "Rapporteringsdatum",
                                    "technicalName": "T1782026",
                                    "sortOrder": 160,
                                    "label": "Rapporteringsdatum",
                                    "compareDescription": "Rapporteringsdatum",
                                    "isComparable": true
                                },
                                "a_inr_enh": {
                                    "term": 4075,
                                    "description": "Inrapporterande enhet",
                                    "technicalName": "T1782038",
                                    "sortOrder": 110,
                                    "label": "Inrapporterande enhet",
                                    "compareDescription": "Inrapporterande enhet",
                                    "isComparable": true
                                },
                                "a_inr_initav": {
                                    "term": 4075,
                                    "description": "Initierat av",
                                    "technicalName": "T1782037",
                                    "sortOrder": 100,
                                    "label": "Initierat av",
                                    "compareDescription": "Initierat av",
                                    "isComparable": true
                                },
                                "a_inr_inrapportor": {
                                    "term": 4075,
                                    "description": "Anmälande inrapportör",
                                    "technicalName": "T1782027",
                                    "sortOrder": 140,
                                    "label": "Anmälande inrapportör",
                                    "compareDescription": "Anmälande inrapportör",
                                    "isComparable": true
                                },
                                "a_inr_klkkod": {
                                    "term": 4075,
                                    "description": "Rapporterande klinikkod",
                                    "technicalName": "T1782040",
                                    "sortOrder": 130,
                                    "label": "Rapporterande klinikkod",
                                    "compareDescription": "Rapporterande klinikkod",
                                    "isComparable": true
                                },
                                "a_inr_komplett": {
                                    "term": 1004,
                                    "description": "Markera med kryss om inrapporteringen avser komplettering eller rättning av enstaka uppgifter",
                                    "technicalName": "T1782036",
                                    "sortOrder": 20,
                                    "label": "Markera med kryss om inrapporteringen avser komplettering eller rättning av enstaka uppgifter",
                                    "compareDescription": "Markera med kryss om inrapporteringen avser komplettering eller rättning av enstaka uppgifter",
                                    "isComparable": true
                                },
                                "a_inr_monkom": {
                                    "term": 4075,
                                    "description": "Monitors kommentar",
                                    "technicalName": "T1782029",
                                    "sortOrder": 170,
                                    "label": "Monitors kommentar",
                                    "compareDescription": "Monitors kommentar",
                                    "isComparable": true
                                },
                                "a_inr_sjhkod": {
                                    "term": 4075,
                                    "description": "Rapporterande sjukhuskod",
                                    "technicalName": "T1782039",
                                    "sortOrder": 120,
                                    "label": "Rapporterande sjukhuskod",
                                    "compareDescription": "Rapporterande sjukhuskod",
                                    "isComparable": true
                                },
                                "a_kir_klkkod": {
                                    "term": 4075,
                                    "description": "Opererande klinik - kod",
                                    "technicalName": "T1782032",
                                    "sortOrder": 320,
                                    "label": "Opererande klinik - kod",
                                    "compareDescription": "Opererande klinik - kod",
                                    "isComparable": true
                                },
                                "a_kir_sjhklk": {
                                    "term": 7178,
                                    "description": "Opererande sjukhus",
                                    "technicalName": "T1782067",
                                    "sortOrder": 300,
                                    "label": "Opererande sjukhus",
                                    "compareDescription": "Opererande sjukhus",
                                    "isComparable": true
                                },
                                "a_kir_sjhkod": {
                                    "term": 4075,
                                    "description": "Opererande sjukhus - kod",
                                    "technicalName": "T1782031",
                                    "sortOrder": 310,
                                    "label": "Opererande sjukhus - kod",
                                    "compareDescription": "Opererande sjukhus - kod",
                                    "isComparable": true
                                },
                                "a_kir_sjhklkshowall": {
                                    "term": 1004,
                                    "description": "Visa alla sjukhus och kliniker",
                                    "technicalName": "T1782036",
                                    "sortOrder": 20,
                                    "label": "Visa alla sjukhus och kliniker",
                                    "compareDescription": "Visa alla sjukhus och kliniker",
                                    "isComparable": true
                                },
                                "a_uppf_sjhklkkirsamma": {
                                    "term": 1004,
                                    "description": "Opererande sjukhus/klinik är ansvarigt för inrapportering av Uppföljning",
                                    "technicalName": "T1782045",
                                    "sortOrder": 330,
                                    "label": "Opererande sjukhus/klinik är ansvarigt för inrapportering av Uppföljning",
                                    "compareDescription": "Opererande sjukhus/klinik är ansvarigt för inrapportering av Uppföljning",
                                    "isComparable": true
                                },
                                "a_beh_sjhklkonksamma": {
                                    "term": 1004,
                                    "description": "Onkologiskt sjukhus/klinik är ansvarigt för inrapportering av Adjuvant behandling",
                                    "technicalName": "T1782056",
                                    "sortOrder": 390,
                                    "label": "Onkologiskt sjukhus/klinik är ansvarigt för inrapportering av Adjuvant behandling",
                                    "compareDescription": "Onkologiskt sjukhus/klinik är ansvarigt för inrapportering av Adjuvant behandling",
                                    "isComparable": true
                                },
                                "a_onk_klkkod": {
                                    "term": 4075,
                                    "description": "Ansvarig klinik/mottagning för onkologisk behandling - kod",
                                    "technicalName": "T1782034",
                                    "sortOrder": 370,
                                    "label": "Ansvarig klinik/mottagning för onkologisk behandling - kod",
                                    "compareDescription": "Ansvarig klinik/mottagning för onkologisk behandling - kod",
                                    "isComparable": true
                                },
                                "a_onk_sjhklk": {
                                    "term": 7178,
                                    "description": "Ansvarigt sjukhus för onkologisk behandling",
                                    "technicalName": "T1782068",
                                    "sortOrder": 350,
                                    "label": "Ansvarigt sjukhus för onkologisk behandling",
                                    "compareDescription": "Ansvarigt sjukhus för onkologisk behandling",
                                    "isComparable": true
                                },
                                "a_onk_sjhkod": {
                                    "term": 4075,
                                    "description": "Ansvarigt sjukhus för onkologisk behandling - kod",
                                    "technicalName": "T1782033",
                                    "sortOrder": 360,
                                    "label": "Ansvarigt sjukhus för onkologisk behandling - kod",
                                    "compareDescription": "Ansvarigt sjukhus för onkologisk behandling - kod",
                                    "isComparable": true
                                },
                                "a_onk_sjhklkshowall": {
                                    "term": 1004,
                                    "description": "Visa alla sjukhus och kliniker",
                                    "technicalName": "T1782036",
                                    "sortOrder": 20,
                                    "label": "Visa alla sjukhus och kliniker",
                                    "compareDescription": "Visa alla sjukhus och kliniker",
                                    "isComparable": true
                                },
                                "a_uppf_sjhklkonksamma": {
                                    "term": 1004,
                                    "description": "Onkologiskt behandlande sjukhus/klinik är ansvarig för inrapportering av Uppföljning",
                                    "technicalName": "T1782055",
                                    "sortOrder": 380,
                                    "label": "Onkologiskt behandlande sjukhus/klinik är ansvarig för inrapportering av Uppföljning",
                                    "compareDescription": "Onkologiskt behandlande sjukhus/klinik är ansvarig för inrapportering av Uppföljning",
                                    "isComparable": true
                                },
                                "a_pad_pattxt": {
                                    "term": 4075,
                                    "description": "Diagnostiserande patologi/cytologiavdelning",
                                    "technicalName": "T1782051",
                                    "sortOrder": 600,
                                    "label": "Diagnostiserande patologi/cytologiavdelning",
                                    "compareDescription": "Diagnostiserande patologi/cytologiavdelning",
                                    "isComparable": true
                                },
                                "a_pad_patkod": {
                                    "term": 4075,
                                    "description": "Diagnostiserande patologi/cytologiavdelning - kod",
                                    "technicalName": "T1782052",
                                    "sortOrder": 610,
                                    "label": "Diagnostiserande patologi/cytologiavdelning - kod",
                                    "compareDescription": "Diagnostiserande patologi/cytologiavdelning - kod",
                                    "isComparable": true
                                },
                                "a_pad_prepar": {
                                    "term": 4075,
                                    "description": "Preparatår",
                                    "technicalName": "T1782054",
                                    "sortOrder": 630,
                                    "label": "Preparatår",
                                    "compareDescription": "Preparatår",
                                    "isComparable": true
                                },
                                "a_pad_prepnr": {
                                    "term": 4075,
                                    "description": "Preparatnummer",
                                    "technicalName": "T1782053",
                                    "sortOrder": 620,
                                    "label": "Preparatnummer",
                                    "compareDescription": "Preparatnummer",
                                    "isComparable": true
                                },
                                "a_pat_alder": {
                                    "term": 4075,
                                    "description": "Ålder vid diagnos",
                                    "technicalName": "T1782048",
                                    "sortOrder": 510,
                                    "label": "Ålder vid diagnos",
                                    "compareDescription": "Ålder vid diagnos",
                                    "isComparable": true
                                },
                                "a_pat_lkfdia": {
                                    "term": 4075,
                                    "description": "Hemort vid diagnos",
                                    "technicalName": "T1782035",
                                    "sortOrder": 10,
                                    "label": "Hemort vid diagnos",
                                    "compareDescription": "Hemort vid diagnos",
                                    "isComparable": true
                                },
                                "a_beh_sjhklkkirsamma": {
                                    "term": 1004,
                                    "description": "Opererande sjukhus/klinik är ansvarigt för inrapportering av Adjuvant behandling",
                                    "technicalName": "T1782046",
                                    "sortOrder": 340,
                                    "label": "Opererande sjukhus/klinik är ansvarigt för inrapportering av Adjuvant behandling",
                                    "compareDescription": "Opererande sjukhus/klinik är ansvarigt för inrapportering av Adjuvant behandling",
                                    "isComparable": true
                                },
                                "a_adj_klkkod": {
                                    "term": 4075,
                                    "description": "Sjukhus/klinik är ansvarigt för inrapportering av Adjuvant behandling - klinikkod",
                                    "technicalName": "T1782073",
                                    "sortOrder": 1420,
                                    "label": "Sjukhus/klinik är ansvarigt för inrapportering av Adjuvant behandling - klinikkod",
                                    "compareDescription": "Sjukhus/klinik är ansvarigt för inrapportering av Adjuvant behandling - klinikkod",
                                    "isComparable": true
                                },
                                "a_adj_sjhkod": {
                                    "term": 4075,
                                    "description": "Sjukhus/klinik är ansvarigt för inrapportering av Adjuvant behandling - sjukhuskod",
                                    "technicalName": "T1782072",
                                    "sortOrder": 1410,
                                    "label": "Sjukhus/klinik är ansvarigt för inrapportering av Adjuvant behandling - sjukhuskod",
                                    "compareDescription": "Sjukhus/klinik är ansvarigt för inrapportering av Adjuvant behandling - sjukhuskod",
                                    "isComparable": true
                                },
                                "a_uppf_klkkod": {
                                    "term": 4075,
                                    "description": "Sjukhus/klinik är ansvarigt för inrapportering av Uppföljning - klinikkod",
                                    "technicalName": "T1782071",
                                    "sortOrder": 1450,
                                    "label": "Sjukhus/klinik är ansvarigt för inrapportering av Uppföljning - klinikkod",
                                    "compareDescription": "Sjukhus/klinik är ansvarigt för inrapportering av Uppföljning - klinikkod",
                                    "isComparable": true
                                },
                                "a_uppf_sjhkod": {
                                    "term": 4075,
                                    "description": "Sjukhus/klinik är ansvarigt för inrapportering av Uppföljning - sjukhuskod",
                                    "technicalName": "T1782070",
                                    "sortOrder": 1440,
                                    "label": "Sjukhus/klinik är ansvarigt för inrapportering av Uppföljning - sjukhuskod",
                                    "compareDescription": "Sjukhus/klinik är ansvarigt för inrapportering av Uppföljning - sjukhuskod",
                                    "isComparable": true
                                },
                                "a_pat_sida": {
                                    "term": 7202,
                                    "description": "Sida",
                                    "technicalName": "T1782226",
                                    "sortOrder": 200,
                                    "label": "Sida",
                                    "compareDescription": "Sida",
                                    "isComparable": true
                                },
                                "a_diag_screening": {
                                    "term": 7172,
                                    "description": "Screeningupptäckt",
                                    "technicalName": "T1782227",
                                    "sortOrder": 530,
                                    "label": "Screeningupptäckt",
                                    "compareDescription": "Screeningupptäckt",
                                    "isComparable": true
                                },
                                "a_diag_preopmorf": {
                                    "term": 7171,
                                    "description": "Preoperativt fastställd malignitet",
                                    "technicalName": "T1782228",
                                    "sortOrder": 540,
                                    "label": "Preoperativt fastställd malignitet",
                                    "compareDescription": "Preoperativt fastställd malignitet",
                                    "isComparable": true
                                },
                                "a_diag_grund": {
                                    "term": 7170,
                                    "description": "Diagnosgrund",
                                    "technicalName": "T1782229",
                                    "sortOrder": 560,
                                    "label": "Diagnosgrund",
                                    "compareDescription": "Diagnosgrund",
                                    "isComparable": true
                                },
                                "a_pat_mensstat": {
                                    "term": 7173,
                                    "description": "Menstruationsstatus",
                                    "technicalName": "T1782230",
                                    "sortOrder": 640,
                                    "label": "Menstruationsstatus",
                                    "compareDescription": "Menstruationsstatus",
                                    "isComparable": true
                                },
                                "a_tnm_tklass": {
                                    "term": 7205,
                                    "description": "T-Klassifikation - Primärtumörer",
                                    "technicalName": "T1782231",
                                    "sortOrder": 700,
                                    "label": "T-Klassifikation - Primärtumörer",
                                    "compareDescription": "T-Klassifikation - Primärtumörer",
                                    "isComparable": true
                                },
                                "a_tnm_nklass": {
                                    "term": 7203,
                                    "description": "N-Klassifikation - Tumör i regionala lymfkörtlar",
                                    "technicalName": "T1782232",
                                    "sortOrder": 710,
                                    "label": "N-Klassifikation - Tumör i regionala lymfkörtlar",
                                    "compareDescription": "N-Klassifikation - Tumör i regionala lymfkörtlar",
                                    "isComparable": true
                                },
                                "a_tnm_mklass": {
                                    "term": 7204,
                                    "description": "M-Klassifikation",
                                    "technicalName": "T1782233",
                                    "sortOrder": 720,
                                    "label": "M-Klassifikation",
                                    "compareDescription": "M-Klassifikation",
                                    "isComparable": true
                                },
                                "a_planbeh_infopatdat": {
                                    "term": 1002,
                                    "description": "Datum för diagnosbesked och första behandlingsdiskussion",
                                    "technicalName": "T1782201",
                                    "sortOrder": 800,
                                    "label": "Datum för diagnosbesked och första behandlingsdiskussion",
                                    "compareDescription": "Datum för diagnosbesked och första behandlingsdiskussion",
                                    "isComparable": true
                                },
                                "a_omv_kssk": {
                                    "term": 7187,
                                    "description": "Kontaktsjuksköterska",
                                    "technicalName": "T1782234",
                                    "sortOrder": 810,
                                    "label": "Kontaktsjuksköterska",
                                    "compareDescription": "Kontaktsjuksköterska",
                                    "isComparable": true
                                },
                                "a_omv_indivplan": {
                                    "term": 7189,
                                    "description": "Individuell vårdplan",
                                    "technicalName": "T1782235",
                                    "sortOrder": 820,
                                    "label": "Individuell vårdplan",
                                    "compareDescription": "Individuell vårdplan",
                                    "isComparable": true
                                },
                                "a_mdk_preop": {
                                    "term": 7188,
                                    "description": "Preoperativ Multidisciplinär/Multiprofessionell konferens",
                                    "technicalName": "T1782236",
                                    "sortOrder": 830,
                                    "label": "Preoperativ Multidisciplinär/Multiprofessionell konferens",
                                    "compareDescription": "Preoperativ Multidisciplinär/Multiprofessionell konferens",
                                    "isComparable": true
                                },
                                "a_mdk_prekir": {
                                    "term": 1004,
                                    "description": "Kirurg",
                                    "technicalName": "T1782202",
                                    "sortOrder": 850,
                                    "label": "Kirurg",
                                    "compareDescription": "Kirurg",
                                    "isComparable": true
                                },
                                "a_mdk_preonk": {
                                    "term": 1004,
                                    "description": "Onkolog",
                                    "technicalName": "T1782203",
                                    "sortOrder": 860,
                                    "label": "Onkolog",
                                    "compareDescription": "Onkolog",
                                    "isComparable": true
                                },
                                "a_mdk_prepat": {
                                    "term": 1004,
                                    "description": "Patolog",
                                    "technicalName": "T1782204",
                                    "sortOrder": 870,
                                    "label": "Patolog",
                                    "compareDescription": "Patolog",
                                    "isComparable": true
                                },
                                "a_mdk_prerad": {
                                    "term": 1004,
                                    "description": "Radiolog",
                                    "technicalName": "T1782205",
                                    "sortOrder": 880,
                                    "label": "Radiolog",
                                    "compareDescription": "Radiolog",
                                    "isComparable": true
                                },
                                "a_mdk_pressk": {
                                    "term": 1004,
                                    "description": "Konsultsjuksköterska",
                                    "technicalName": "T1782206",
                                    "sortOrder": 890,
                                    "label": "Konsultsjuksköterska",
                                    "compareDescription": "Konsultsjuksköterska",
                                    "isComparable": true
                                },
                                "a_forsk_stud": {
                                    "term": 7186,
                                    "description": "Patienten ingår i studie",
                                    "technicalName": "T1782237",
                                    "sortOrder": 900,
                                    "label": "Patienten ingår i studie",
                                    "compareDescription": "Patienten ingår i studie",
                                    "isComparable": true
                                },
                                "a_planbeh_typ": {
                                    "term": 7200,
                                    "description": "Planerat vårdflöde",
                                    "technicalName": "T1782238",
                                    "sortOrder": 910,
                                    "label": "Planerat vårdflöde",
                                    "compareDescription": "Planerat vårdflöde",
                                    "isComparable": true
                                },
                                "a_planbeh_typejors": {
                                    "term": 7201,
                                    "description": "Ej operation, ange orsak",
                                    "technicalName": "T1782239",
                                    "sortOrder": 920,
                                    "label": "Ej operation, ange orsak",
                                    "compareDescription": "Ej operation, ange orsak",
                                    "isComparable": true
                                },
                                "a_planbeh_preopstral": {
                                    "term": 1004,
                                    "description": "Planerad Neoadjuvant Behandling - Strålbehandling",
                                    "technicalName": "T1782207",
                                    "sortOrder": 1000,
                                    "label": "Planerad Neoadjuvant Behandling - Strålbehandling",
                                    "compareDescription": "Planerad Neoadjuvant Behandling - Strålbehandling",
                                    "isComparable": true
                                },
                                "a_planbeh_preopendo": {
                                    "term": 1004,
                                    "description": "Planerad Neoadjuvant Behandling - Endokrin",
                                    "technicalName": "T1782208",
                                    "sortOrder": 1010,
                                    "label": "Planerad Neoadjuvant Behandling - Endokrin",
                                    "compareDescription": "Planerad Neoadjuvant Behandling - Endokrin",
                                    "isComparable": true
                                },
                                "a_planbeh_preopcyt": {
                                    "term": 1004,
                                    "description": "Planerad Neoadjuvant Behandling - Cytostatika",
                                    "technicalName": "T1782209",
                                    "sortOrder": 1020,
                                    "label": "Planerad Neoadjuvant Behandling - Cytostatika",
                                    "compareDescription": "Planerad Neoadjuvant Behandling - Cytostatika",
                                    "isComparable": true
                                },
                                "a_planbeh_preopantikropp": {
                                    "term": 1004,
                                    "description": "Planerad Neoadjuvant Behandling - Antikroppsbehandling",
                                    "technicalName": "T1782210",
                                    "sortOrder": 1030,
                                    "label": "Planerad Neoadjuvant Behandling - Antikroppsbehandling",
                                    "compareDescription": "Planerad Neoadjuvant Behandling - Antikroppsbehandling",
                                    "isComparable": true
                                },
                                "a_pad_typ": {
                                    "term": 7199,
                                    "description": "Patologiuppgifter från primär operation eller från mellan-/grovnålsbiopsi",
                                    "technicalName": "T1782225",
                                    "sortOrder": 1100,
                                    "label": "Patologiuppgifter från primär operation eller från mellan-/grovnålsbiopsi",
                                    "compareDescription": "Patologiuppgifter från primär operation eller från mellan-/grovnålsbiopsi",
                                    "isComparable": true
                                },
                                "a_pad_ej": {
                                    "term": 1004,
                                    "description": "PAD ej utfört",
                                    "technicalName": "T1782242",
                                    "sortOrder": 1110,
                                    "label": "PAD ej utfört",
                                    "compareDescription": "PAD ej utfört",
                                    "isComparable": true
                                },
                                "a_pad_pattxt2": {
                                    "term": 4075,
                                    "description": "Patologiavdelning",
                                    "technicalName": "T1782243",
                                    "sortOrder": 1120,
                                    "label": "Patologiavdelning",
                                    "compareDescription": "Patologiavdelning",
                                    "isComparable": true
                                },
                                "a_pad_patkod2": {
                                    "term": 4075,
                                    "description": "Patolog/cytolog kod",
                                    "technicalName": "T1782244",
                                    "sortOrder": 1130,
                                    "label": "Patolog/cytolog kod",
                                    "compareDescription": "Patolog/cytolog kod",
                                    "isComparable": true
                                },
                                "a_pad_prepnr2": {
                                    "term": 4075,
                                    "description": "Preparatnr",
                                    "technicalName": "T1782245",
                                    "sortOrder": 1140,
                                    "label": "Preparatnr",
                                    "compareDescription": "Preparatnr",
                                    "isComparable": true
                                },
                                "a_pad_prepar2": {
                                    "term": 4075,
                                    "description": "Preparatår",
                                    "technicalName": "T1782246",
                                    "sortOrder": 1150,
                                    "label": "Preparatår",
                                    "compareDescription": "Preparatår",
                                    "isComparable": true
                                },
                                "a_pad_invasiv": {
                                    "term": 7194,
                                    "description": "Invasivitet",
                                    "technicalName": "T1782220",
                                    "sortOrder": 1160,
                                    "label": "Invasivitet",
                                    "compareDescription": "Invasivitet",
                                    "isComparable": true
                                },
                                "a_pad_invasivhist": {
                                    "term": 7195,
                                    "description": "Typ av invasiv histopatologi",
                                    "technicalName": "T1782221",
                                    "sortOrder": 1170,
                                    "label": "Typ av invasiv histopatologi",
                                    "compareDescription": "Typ av invasiv histopatologi",
                                    "isComparable": true
                                },
                                "a_pad_invasivhistann": {
                                    "term": 4075,
                                    "description": "Annan invasiv cancer",
                                    "technicalName": "T1782240",
                                    "sortOrder": 1180,
                                    "label": "Annan invasiv cancer",
                                    "compareDescription": "Annan invasiv cancer",
                                    "isComparable": true
                                },
                                "a_pad_invasivsnomed": {
                                    "term": 4075,
                                    "description": "Invasiv snomedkod",
                                    "technicalName": "T1782241",
                                    "sortOrder": 1190,
                                    "label": "Invasiv snomedkod",
                                    "compareDescription": "Invasiv snomedkod",
                                    "isComparable": true
                                },
                                "a_pad_insituhist": {
                                    "term": 7193,
                                    "description": "Typ av cancer in situ, histopatologi",
                                    "technicalName": "T1782219",
                                    "sortOrder": 1200,
                                    "label": "Typ av cancer in situ, histopatologi",
                                    "compareDescription": "Typ av cancer in situ, histopatologi",
                                    "isComparable": true
                                },
                                "a_pad_insituhistann": {
                                    "term": 4075,
                                    "description": "Annan cancer in situ",
                                    "technicalName": "T1782250",
                                    "sortOrder": 1210,
                                    "label": "Annan cancer in situ",
                                    "compareDescription": "Annan cancer in situ",
                                    "isComparable": true
                                },
                                "a_pad_insitusnomed": {
                                    "term": 4075,
                                    "description": "Cancer in situ snomedkod",
                                    "technicalName": "T1782251",
                                    "sortOrder": 1220,
                                    "label": "Cancer in situ snomedkod",
                                    "compareDescription": "Cancer in situ snomedkod",
                                    "isComparable": true
                                },
                                "a_pad_nhg": {
                                    "term": 7197,
                                    "description": "Tumörgrad (invasiv) kärnatypigrad (cis)",
                                    "technicalName": "T1782223",
                                    "sortOrder": 1230,
                                    "label": "Tumörgrad (invasiv) kärnatypigrad (cis)",
                                    "compareDescription": "Tumörgrad (invasiv) kärnatypigrad (cis)",
                                    "isComparable": true
                                },
                                "a_pad_erproc": {
                                    "term": 1040,
                                    "description": "ER-status, procent",
                                    "technicalName": "T1782252",
                                    "sortOrder": 1240,
                                    "label": "ER-status, procent",
                                    "compareDescription": "ER-status, procent",
                                    "isComparable": true
                                },
                                "a_pad_er": {
                                    "term": 7190,
                                    "description": "ER-status, immunhistokemi",
                                    "technicalName": "T1782216",
                                    "sortOrder": 1250,
                                    "label": "ER-status, immunhistokemi",
                                    "compareDescription": "ER-status, immunhistokemi",
                                    "isComparable": true
                                },
                                "a_prlniva": {
                                    "term": 4477,
                                    "description": "s-Prolaktin koncentration",
                                    "technicalName": "T743660",
                                    "sortOrder": 190,
                                    "compareDescription": "s-Prolaktin koncentration",
                                    "isComparable": true
                                },
                                "a_ghmorgonensniva": {
                                    "term": 4480,
                                    "description": "s-GH (morgon/enstaka) koncentration",
                                    "technicalName": "T743619",
                                    "sortOrder": 220,
                                    "compareDescription": "s-GH (morgon/enstaka) koncentration",
                                    "isComparable": true
                                },
                                "a_pad_prproc": {
                                    "term": 1040,
                                    "description": "PgR-status, procent",
                                    "technicalName": "T1782253",
                                    "sortOrder": 1260,
                                    "label": "PgR-status, procent",
                                    "compareDescription": "PgR-status, procent",
                                    "isComparable": true
                                },
                                "a_pad_pr": {
                                    "term": 7198,
                                    "description": "PgR-status, immunhistokemi",
                                    "technicalName": "T1782224",
                                    "sortOrder": 1270,
                                    "label": "PgR-status, immunhistokemi",
                                    "compareDescription": "PgR-status, immunhistokemi",
                                    "isComparable": true
                                },
                                "a_pad_her2imm": {
                                    "term": 7191,
                                    "description": "HER2-neu, Immunhistokemi",
                                    "technicalName": "T1782217",
                                    "sortOrder": 1280,
                                    "label": "HER2-neu, Immunhistokemi",
                                    "compareDescription": "HER2-neu, Immunhistokemi",
                                    "isComparable": true
                                },
                                "a_pad_her2ish": {
                                    "term": 7192,
                                    "description": "HER2- neu, ISH-analys",
                                    "technicalName": "T1782218",
                                    "sortOrder": 1290,
                                    "label": "HER2- neu, ISH-analys",
                                    "compareDescription": "HER2- neu, ISH-analys",
                                    "isComparable": true
                                },
                                "a_pad_ki67proc": {
                                    "term": 1040,
                                    "description": "KI-status, procent",
                                    "technicalName": "T1782254",
                                    "sortOrder": 1300,
                                    "label": "KI-status, procent",
                                    "compareDescription": "KI-status, procent",
                                    "isComparable": true
                                },
                                "a_pad_ki67": {
                                    "term": 7196,
                                    "description": "KI-status (enl lokala cut-off)",
                                    "technicalName": "T1782222",
                                    "sortOrder": 1310,
                                    "label": "KI-status (enl lokala cut-off)",
                                    "compareDescription": "KI-status (enl lokala cut-off)",
                                    "isComparable": true
                                },
                                "a_adj_sjhklk": {
                                    "term": 7178,
                                    "description": "Sjukhus/klinik är ansvarigt för inrapportering av Adjuvant behandling",
                                    "technicalName": "T1782211",
                                    "sortOrder": 1400,
                                    "label": "Sjukhus/klinik är ansvarigt för inrapportering av Adjuvant behandling",
                                    "compareDescription": "Sjukhus/klinik är ansvarigt för inrapportering av Adjuvant behandling",
                                    "isComparable": true
                                },
                                "a_uppf_sjhklk": {
                                    "term": 7178,
                                    "description": "Sjukhus/klinik är ansvarigt för inrapportering av Uppföljning",
                                    "technicalName": "T1782212",
                                    "sortOrder": 1430,
                                    "label": "Sjukhus/klinik är ansvarigt för inrapportering av Uppföljning",
                                    "compareDescription": "Sjukhus/klinik är ansvarigt för inrapportering av Uppföljning",
                                    "isComparable": true
                                },
                                "a_uppf_sjhklkshowall": {
                                    "term": 1004,
                                    "description": "Visa alla sjukhus och kliniker",
                                    "technicalName": "T1782036",
                                    "sortOrder": 20,
                                    "label": "Visa alla sjukhus och kliniker",
                                    "compareDescription": "Visa alla sjukhus och kliniker",
                                    "isComparable": true
                                },
                                "a_uppf_ingen": {
                                    "term": 1004,
                                    "description": "Ingen fortsatt uppföljning",
                                    "technicalName": "T1782255",
                                    "sortOrder": 1460,
                                    "label": "Ingen fortsatt uppföljning",
                                    "compareDescription": "Ingen fortsatt uppföljning",
                                    "isComparable": true
                                },
                                "a_uppf_ingenors": {
                                    "term": 4075,
                                    "description": "Ingen fortsatt uppföljning - Ange orsak",
                                    "technicalName": "T1782248",
                                    "sortOrder": 1470,
                                    "label": "Ingen fortsatt uppföljning - Ange orsak",
                                    "compareDescription": "Ingen fortsatt uppföljning - Ange orsak",
                                    "isComparable": true
                                },
                                "a_uppf_sjhann": {
                                    "term": 1004,
                                    "description": "Annat uppföljande sjukhus",
                                    "technicalName": "T1782247",
                                    "sortOrder": 1480,
                                    "label": "Annat uppföljande sjukhus",
                                    "compareDescription": "Annat uppföljande sjukhus",
                                    "isComparable": true
                                },
                                "a_uppf_sjhanntxt": {
                                    "term": 4075,
                                    "description": "Vilket annat uppföljande sjukhus?",
                                    "technicalName": "T1782249",
                                    "sortOrder": 1490,
                                    "label": "Vilket annat uppföljande sjukhus?",
                                    "compareDescription": "Vilket annat uppföljande sjukhus?",
                                    "isComparable": true
                                }
                            },
                            "vdlists": {
                                "vd_anmalan_bc": {
                                    "term": 7179
                                }
                            },
                            "terms": {
                                "1002": {
                                    "name": "Datum",
                                    "shortName": "Datum",
                                    "description": "",
                                    "dataType": "datetime",
                                    "includeTime": false
                                },
                                "1004": {
                                    "name": "Kryssruta",
                                    "shortName": "Kryssruta",
                                    "description": "",
                                    "dataType": "boolean"
                                },
                                "1040": {
                                    "name": "Heltal",
                                    "shortName": "Heltal",
                                    "description": "",
                                    "dataType": "integer",
                                    "min": null,
                                    "max": null
                                },
                                "4075": {
                                    "name": "Text(8000)",
                                    "shortName": "TEXT",
                                    "description": "Textvariabel med möjlighet att lagra maximalt antal tecken (8000).",
                                    "dataType": "string",
                                    "maxLength": 8000,
                                    "validCharacters": null
                                },
                                "7170": {
                                    "name": "a_diag_grund",
                                    "shortName": "a_diag_grund",
                                    "description": "Diagnosgrund",
                                    "dataType": "list",
                                    "listValues": [
                                        {
                                            "id": 22857,
                                            "text": "Klinisk undersökning",
                                            "value": "1",
                                            "validFrom": "0001-01-01T00:00:00",
                                            "validTo": "9999-12-31T00:00:00"
                                        },
                                        {
                                            "id": 22858,
                                            "text": "Röntgen, scintigrafi, ultraljud, MR eller motsv. undersökning",
                                            "value": "2",
                                            "validFrom": "0001-01-01T00:00:00",
                                            "validTo": "9999-12-31T00:00:00"
                                        },
                                        {
                                            "id": 22859,
                                            "text": "PAD-verifierad diagnos (mellannål, grovnål, provexcision eller kirurgi med histopatologisk undersökning)",
                                            "value": "3",
                                            "validFrom": "0001-01-01T00:00:00",
                                            "validTo": "9999-12-31T00:00:00"
                                        },
                                        {
                                            "id": 22860,
                                            "text": "Cytologisk undersökning",
                                            "value": "5",
                                            "validFrom": "0001-01-01T00:00:00",
                                            "validTo": "9999-12-31T00:00:00"
                                        }
                                    ]
                                },
                                "7171": {
                                    "name": "a_diag_preopmorf",
                                    "shortName": "a_diag_preopmorf",
                                    "description": "Preoperativt fastställd malignitet",
                                    "dataType": "list",
                                    "listValues": [
                                        {
                                            "id": 22854,
                                            "text": "Nej",
                                            "value": "0",
                                            "validFrom": "0001-01-01T00:00:00",
                                            "validTo": "9999-12-31T00:00:00"
                                        },
                                        {
                                            "id": 22855,
                                            "text": "Ja",
                                            "value": "1",
                                            "validFrom": "0001-01-01T00:00:00",
                                            "validTo": "9999-12-31T00:00:00"
                                        },
                                        {
                                            "id": 22856,
                                            "text": "Okänt",
                                            "value": "9",
                                            "validFrom": "0001-01-01T00:00:00",
                                            "validTo": "9999-12-31T00:00:00"
                                        }
                                    ]
                                },
                                "4477": {
                                    "name": "A_PrlNiva",
                                    "shortName": "a_prlniva",
                                    "description": "s-Prolaktin koncentration",
                                    "dataType": "decimal",
                                    "precision": 1,
                                    "min": null,
                                    "max": null
                                },
                                "4480": {
                                    "name": "A_GHMorgonEnsNiva",
                                    "shortName": "a_ghmorgonensniva",
                                    "description": "s-GH (morgon/enstaka) koncentration",
                                    "dataType": "decimal",
                                    "precision": 2,
                                    "min": null,
                                    "max": null
                                },
                                "7172": {
                                    "name": "a_diag_screening",
                                    "shortName": "a_diag_screening",
                                    "description": "Screeningupptäckt",
                                    "dataType": "list",
                                    "listValues": [
                                        {
                                            "id": 22851,
                                            "text": "Nej",
                                            "value": "0",
                                            "validFrom": "0001-01-01T00:00:00",
                                            "validTo": "9999-12-31T00:00:00"
                                        },
                                        {
                                            "id": 22852,
                                            "text": "Ja",
                                            "value": "1",
                                            "validFrom": "0001-01-01T00:00:00",
                                            "validTo": "9999-12-31T00:00:00"
                                        },
                                        {
                                            "id": 22853,
                                            "text": "Okänt",
                                            "value": "9",
                                            "validFrom": "0001-01-01T00:00:00",
                                            "validTo": "9999-12-31T00:00:00"
                                        }
                                    ]
                                },
                                "7173": {
                                    "name": "a_pat_mensstat",
                                    "shortName": "a_pat_mensstat",
                                    "description": "Menstruationsstatus",
                                    "dataType": "list",
                                    "listValues": [
                                        {
                                            "id": 22861,
                                            "text": "Premenopaus (< 6 mån efter senaste mens)",
                                            "value": "1",
                                            "validFrom": "0001-01-01T00:00:00",
                                            "validTo": "9999-12-31T00:00:00"
                                        },
                                        {
                                            "id": 22862,
                                            "text": "Postmenopaus (> 5 år efter senaste mens)",
                                            "value": "2",
                                            "validFrom": "0001-01-01T00:00:00",
                                            "validTo": "9999-12-31T00:00:00"
                                        },
                                        {
                                            "id": 22863,
                                            "text": "Uppgift saknas/ej tillämplig (t ex för män)",
                                            "value": "98",
                                            "validFrom": "0001-01-01T00:00:00",
                                            "validTo": "9999-12-31T00:00:00"
                                        }
                                    ]
                                },
                                "7178": {
                                    "name": "VD_BC_Org",
                                    "shortName": "VD_BC_Org",
                                    "description": "Organisationsenheter",
                                    "dataType": "vd"
                                },
                                "7179": {
                                    "name": "VD_Anmälan_BC",
                                    "shortName": "vd_anmalan_bc",
                                    "description": "Värdedomän mot bröstcanceranmälan",
                                    "dataType": "vd"
                                },
                                "7186": {
                                    "name": "a_forsk_stud",
                                    "shortName": "a_forsk_stud",
                                    "description": "Patienten ingår i studie",
                                    "dataType": "list",
                                    "listValues": [
                                        {
                                            "id": 22891,
                                            "text": "Nej",
                                            "value": "0",
                                            "validFrom": "0001-01-01T00:00:00",
                                            "validTo": "9999-12-31T00:00:00"
                                        },
                                        {
                                            "id": 22892,
                                            "text": "Ja",
                                            "value": "1",
                                            "validFrom": "0001-01-01T00:00:00",
                                            "validTo": "9999-12-31T00:00:00"
                                        },
                                        {
                                            "id": 22893,
                                            "text": "Uppgift saknas",
                                            "value": "98",
                                            "validFrom": "0001-01-01T00:00:00",
                                            "validTo": "9999-12-31T00:00:00"
                                        }
                                    ]
                                },
                                "7187": {
                                    "name": "a_omv_kssk",
                                    "shortName": "a_omv_kssk",
                                    "description": "Kontaktsjuksköterska",
                                    "dataType": "list",
                                    "listValues": [
                                        {
                                            "id": 22882,
                                            "text": "Nej",
                                            "value": "0",
                                            "validFrom": "0001-01-01T00:00:00",
                                            "validTo": "9999-12-31T00:00:00"
                                        },
                                        {
                                            "id": 22883,
                                            "text": "Ja",
                                            "value": "1",
                                            "validFrom": "0001-01-01T00:00:00",
                                            "validTo": "9999-12-31T00:00:00"
                                        },
                                        {
                                            "id": 22884,
                                            "text": "Uppgift saknas",
                                            "value": "98",
                                            "validFrom": "0001-01-01T00:00:00",
                                            "validTo": "9999-12-31T00:00:00"
                                        }
                                    ]
                                },
                                "7188": {
                                    "name": "a_mdk_preop",
                                    "shortName": "a_mdk_preop",
                                    "description": "Preoperativ Multidisciplinär/Multiprofessionell konferens",
                                    "dataType": "list",
                                    "listValues": [
                                        {
                                            "id": 22888,
                                            "text": "Nej",
                                            "value": "0",
                                            "validFrom": "0001-01-01T00:00:00",
                                            "validTo": "9999-12-31T00:00:00"
                                        },
                                        {
                                            "id": 22889,
                                            "text": "Ja",
                                            "value": "1",
                                            "validFrom": "0001-01-01T00:00:00",
                                            "validTo": "9999-12-31T00:00:00"
                                        },
                                        {
                                            "id": 22890,
                                            "text": "Uppgift saknas",
                                            "value": "98",
                                            "validFrom": "0001-01-01T00:00:00",
                                            "validTo": "9999-12-31T00:00:00"
                                        }
                                    ]
                                },
                                "7189": {
                                    "name": "a_omv_indivplan",
                                    "shortName": "a_omv_indivplan",
                                    "description": "Individuell vårdplan",
                                    "dataType": "list",
                                    "listValues": [
                                        {
                                            "id": 22885,
                                            "text": "Nej",
                                            "value": "0",
                                            "validFrom": "0001-01-01T00:00:00",
                                            "validTo": "9999-12-31T00:00:00"
                                        },
                                        {
                                            "id": 22886,
                                            "text": "Ja",
                                            "value": "1",
                                            "validFrom": "0001-01-01T00:00:00",
                                            "validTo": "9999-12-31T00:00:00"
                                        },
                                        {
                                            "id": 22887,
                                            "text": "Uppgift saknas",
                                            "value": "98",
                                            "validFrom": "0001-01-01T00:00:00",
                                            "validTo": "9999-12-31T00:00:00"
                                        }
                                    ]
                                },
                                "7190": {
                                    "name": "a_pad_er",
                                    "shortName": "a_pad_er",
                                    "description": "ER status, immunhistokemi",
                                    "dataType": "list",
                                    "listValues": [
                                        {
                                            "id": 22948,
                                            "text": "Positiv",
                                            "value": "1",
                                            "validFrom": "0001-01-01T00:00:00",
                                            "validTo": "9999-12-31T00:00:00"
                                        },
                                        {
                                            "id": 22949,
                                            "text": "Negativ",
                                            "value": "2",
                                            "validFrom": "0001-01-01T00:00:00",
                                            "validTo": "9999-12-31T00:00:00"
                                        },
                                        {
                                            "id": 22950,
                                            "text": "Ej utfört",
                                            "value": "97",
                                            "validFrom": "0001-01-01T00:00:00",
                                            "validTo": "9999-12-31T00:00:00"
                                        },
                                        {
                                            "id": 22951,
                                            "text": "Ej bedömbart/Uppgift saknas",
                                            "value": "98",
                                            "validFrom": "0001-01-01T00:00:00",
                                            "validTo": "9999-12-31T00:00:00"
                                        }
                                    ]
                                },
                                "7191": {
                                    "name": "a_pad_her2imm",
                                    "shortName": "a_pad_her2imm",
                                    "description": "HER2 neu Immunhistokemi",
                                    "dataType": "list",
                                    "listValues": [
                                        {
                                            "id": 22956,
                                            "text": "0-1+",
                                            "value": "1",
                                            "validFrom": "0001-01-01T00:00:00",
                                            "validTo": "9999-12-31T00:00:00"
                                        },
                                        {
                                            "id": 22957,
                                            "text": "2+",
                                            "value": "2",
                                            "validFrom": "0001-01-01T00:00:00",
                                            "validTo": "9999-12-31T00:00:00"
                                        },
                                        {
                                            "id": 22958,
                                            "text": "3+",
                                            "value": "3",
                                            "validFrom": "0001-01-01T00:00:00",
                                            "validTo": "9999-12-31T00:00:00"
                                        },
                                        {
                                            "id": 22959,
                                            "text": "Ej utfört",
                                            "value": "97",
                                            "validFrom": "0001-01-01T00:00:00",
                                            "validTo": "9999-12-31T00:00:00"
                                        },
                                        {
                                            "id": 22960,
                                            "text": "Ej bedömbart/Uppgift saknas",
                                            "value": "98",
                                            "validFrom": "0001-01-01T00:00:00",
                                            "validTo": "9999-12-31T00:00:00"
                                        }
                                    ]
                                },
                                "7192": {
                                    "name": "a_pad_her2ish",
                                    "shortName": "a_pad_her2ish",
                                    "description": "HER2 neu ISH-analys",
                                    "dataType": "list",
                                    "listValues": [
                                        {
                                            "id": 22961,
                                            "text": "Amplifierat, terapiindikation",
                                            "value": "1",
                                            "validFrom": "0001-01-01T00:00:00",
                                            "validTo": "9999-12-31T00:00:00"
                                        },
                                        {
                                            "id": 22962,
                                            "text": "Ej amplifierat",
                                            "value": "2",
                                            "validFrom": "0001-01-01T00:00:00",
                                            "validTo": "9999-12-31T00:00:00"
                                        },
                                        {
                                            "id": 22963,
                                            "text": "Ej utfört",
                                            "value": "97",
                                            "validFrom": "0001-01-01T00:00:00",
                                            "validTo": "9999-12-31T00:00:00"
                                        },
                                        {
                                            "id": 22964,
                                            "text": "Ej bedömbart/Uppgift saknas",
                                            "value": "98",
                                            "validFrom": "0001-01-01T00:00:00",
                                            "validTo": "9999-12-31T00:00:00"
                                        }
                                    ]
                                },
                                "7193": {
                                    "name": "a_pad_insituhist",
                                    "shortName": "a_pad_insituhist",
                                    "description": "Typ av cancer in situ, histopatologi",
                                    "dataType": "list",
                                    "listValues": [
                                        {
                                            "id": 22930,
                                            "text": "DCIS (85002)",
                                            "value": "10",
                                            "validFrom": "0001-01-01T00:00:00",
                                            "validTo": "9999-12-31T00:00:00"
                                        },
                                        {
                                            "id": 22931,
                                            "text": "LCIS (85202)",
                                            "value": "20",
                                            "validFrom": "0001-01-01T00:00:00",
                                            "validTo": "9999-12-31T00:00:00"
                                        },
                                        {
                                            "id": 22932,
                                            "text": "DCIS + LCIS (85222)",
                                            "value": "40",
                                            "validFrom": "0001-01-01T00:00:00",
                                            "validTo": "9999-12-31T00:00:00"
                                        },
                                        {
                                            "id": 22933,
                                            "text": "Annan in situ cancer",
                                            "value": "50",
                                            "validFrom": "0001-01-01T00:00:00",
                                            "validTo": "9999-12-31T00:00:00"
                                        }
                                    ]
                                },
                                "7194": {
                                    "name": "a_pad_invasiv",
                                    "shortName": "a_pad_invasiv",
                                    "description": "Invasivitet",
                                    "dataType": "list",
                                    "listValues": [
                                        {
                                            "id": 22923,
                                            "text": "Invasiv cancer",
                                            "value": "1",
                                            "validFrom": "0001-01-01T00:00:00",
                                            "validTo": "9999-12-31T00:00:00"
                                        },
                                        {
                                            "id": 22924,
                                            "text": "Enbart cancer in situ",
                                            "value": "2",
                                            "validFrom": "0001-01-01T00:00:00",
                                            "validTo": "9999-12-31T00:00:00"
                                        },
                                        {
                                            "id": 22925,
                                            "text": "Bedömning ej möjlig",
                                            "value": "4",
                                            "validFrom": "0001-01-01T00:00:00",
                                            "validTo": "9999-12-31T00:00:00"
                                        }
                                    ]
                                },
                                "7195": {
                                    "name": "a_pad_invasivhist",
                                    "shortName": "a_pad_invasivhist",
                                    "description": "Typ av invasiv histopatologi",
                                    "dataType": "list",
                                    "listValues": [
                                        {
                                            "id": 22926,
                                            "text": "Ductal cancer",
                                            "value": "10",
                                            "validFrom": "0001-01-01T00:00:00",
                                            "validTo": "9999-12-31T00:00:00"
                                        },
                                        {
                                            "id": 22927,
                                            "text": "Både ductal och lobulär cancer",
                                            "value": "20",
                                            "validFrom": "0001-01-01T00:00:00",
                                            "validTo": "9999-12-31T00:00:00"
                                        },
                                        {
                                            "id": 22928,
                                            "text": "Lobulär cancer",
                                            "value": "40",
                                            "validFrom": "0001-01-01T00:00:00",
                                            "validTo": "9999-12-31T00:00:00"
                                        },
                                        {
                                            "id": 22929,
                                            "text": "Annan invasiv cancer",
                                            "value": "70",
                                            "validFrom": "0001-01-01T00:00:00",
                                            "validTo": "9999-12-31T00:00:00"
                                        }
                                    ]
                                },
                                "7196": {
                                    "name": "a_pad_ki67",
                                    "shortName": "a_pad_ki67",
                                    "description": "KI-status (enl lokala cut-off)",
                                    "dataType": "list",
                                    "listValues": [
                                        {
                                            "id": 22965,
                                            "text": "Låg (under 10 %)",
                                            "value": "1",
                                            "validFrom": "0001-01-01T00:00:00",
                                            "validTo": "9999-12-31T00:00:00"
                                        },
                                        {
                                            "id": 23002,
                                            "text": "Medel",
                                            "value": "2",
                                            "validFrom": "0001-01-01T00:00:00",
                                            "validTo": "9999-12-31T00:00:00"
                                        },
                                        {
                                            "id": 22966,
                                            "text": "Hög (över 20%)",
                                            "value": "3",
                                            "validFrom": "0001-01-01T00:00:00",
                                            "validTo": "9999-12-31T00:00:00"
                                        },
                                        {
                                            "id": 22967,
                                            "text": "Ej utfört",
                                            "value": "97",
                                            "validFrom": "0001-01-01T00:00:00",
                                            "validTo": "9999-12-31T00:00:00"
                                        },
                                        {
                                            "id": 22968,
                                            "text": "Ej bedömbart/Uppgift saknas",
                                            "value": "98",
                                            "validFrom": "0001-01-01T00:00:00",
                                            "validTo": "9999-12-31T00:00:00"
                                        }
                                    ]
                                },
                                "7197": {
                                    "name": "a_pad_nhg",
                                    "shortName": "a_pad_nhg",
                                    "description": "Tumörgrad (invasiv) kärnatypigrad (cis)",
                                    "dataType": "list",
                                    "listValues": [
                                        {
                                            "id": 22943,
                                            "text": "Grad 1",
                                            "value": "1",
                                            "validFrom": "0001-01-01T00:00:00",
                                            "validTo": "9999-12-31T00:00:00"
                                        },
                                        {
                                            "id": 22944,
                                            "text": "Grad 2",
                                            "value": "2",
                                            "validFrom": "0001-01-01T00:00:00",
                                            "validTo": "9999-12-31T00:00:00"
                                        },
                                        {
                                            "id": 22945,
                                            "text": "Grad 3",
                                            "value": "3",
                                            "validFrom": "0001-01-01T00:00:00",
                                            "validTo": "9999-12-31T00:00:00"
                                        },
                                        {
                                            "id": 22946,
                                            "text": "Ej utfört",
                                            "value": "97",
                                            "validFrom": "0001-01-01T00:00:00",
                                            "validTo": "9999-12-31T00:00:00"
                                        },
                                        {
                                            "id": 22947,
                                            "text": "Ej bedömbart/Uppgift saknas",
                                            "value": "98",
                                            "validFrom": "0001-01-01T00:00:00",
                                            "validTo": "9999-12-31T00:00:00"
                                        }
                                    ]
                                },
                                "7198": {
                                    "name": "a_pad_pr",
                                    "shortName": "a_pad_pr",
                                    "description": "PgR status, immunhistokemi",
                                    "dataType": "list",
                                    "listValues": [
                                        {
                                            "id": 22952,
                                            "text": "Positiv",
                                            "value": "1",
                                            "validFrom": "0001-01-01T00:00:00",
                                            "validTo": "9999-12-31T00:00:00"
                                        },
                                        {
                                            "id": 22953,
                                            "text": "Negativ",
                                            "value": "2",
                                            "validFrom": "0001-01-01T00:00:00",
                                            "validTo": "9999-12-31T00:00:00"
                                        },
                                        {
                                            "id": 22954,
                                            "text": "Ej utfört",
                                            "value": "97",
                                            "validFrom": "0001-01-01T00:00:00",
                                            "validTo": "9999-12-31T00:00:00"
                                        },
                                        {
                                            "id": 22955,
                                            "text": "Ej bedömbart/Uppgift saknas",
                                            "value": "98",
                                            "validFrom": "0001-01-01T00:00:00",
                                            "validTo": "9999-12-31T00:00:00"
                                        }
                                    ]
                                },
                                "7199": {
                                    "name": "a_pad_typ",
                                    "shortName": "a_pad_typ",
                                    "description": "Patologiuppgifter från primär operation eller från mellan-/grovnålsbiopsi",
                                    "dataType": "list",
                                    "listValues": [
                                        {
                                            "id": 22996,
                                            "text": "PAD från primär operation",
                                            "value": "1",
                                            "validFrom": "0001-01-01T00:00:00",
                                            "validTo": "9999-12-31T00:00:00"
                                        },
                                        {
                                            "id": 22997,
                                            "text": "PAD från mellannåls-/grovnålsbiopsi",
                                            "value": "2",
                                            "validFrom": "0001-01-01T00:00:00",
                                            "validTo": "9999-12-31T00:00:00"
                                        },
                                        {
                                            "id": 22998,
                                            "text": "PAD ej utförd",
                                            "value": "97",
                                            "validFrom": "0001-01-01T00:00:00",
                                            "validTo": "9999-12-31T00:00:00"
                                        }
                                    ]
                                },
                                "7200": {
                                    "name": "a_planbeh_typ",
                                    "shortName": "a_planbeh_typ",
                                    "description": "Planerat vårdflöde",
                                    "dataType": "list",
                                    "listValues": [
                                        {
                                            "id": 22999,
                                            "text": "Primär operation",
                                            "value": "1",
                                            "validFrom": "0001-01-01T00:00:00",
                                            "validTo": "9999-12-31T00:00:00"
                                        },
                                        {
                                            "id": 23000,
                                            "text": "Neoadjuvant behandling",
                                            "value": "2",
                                            "validFrom": "0001-01-01T00:00:00",
                                            "validTo": "9999-12-31T00:00:00"
                                        },
                                        {
                                            "id": 23001,
                                            "text": "Ej operation/konservativ endokrin behandling",
                                            "value": "3",
                                            "validFrom": "0001-01-01T00:00:00",
                                            "validTo": "9999-12-31T00:00:00"
                                        }
                                    ]
                                },
                                "7201": {
                                    "name": "a_planbeh_typejors",
                                    "shortName": "a_planbeh_typejors",
                                    "description": "Ej operation, ange orsak",
                                    "dataType": "list",
                                    "listValues": [
                                        {
                                            "id": 22990,
                                            "text": "Lokalt avancerad sjukdom",
                                            "value": "1",
                                            "validFrom": "0001-01-01T00:00:00",
                                            "validTo": "9999-12-31T00:00:00"
                                        },
                                        {
                                            "id": 22991,
                                            "text": "Fjärrmetastaser",
                                            "value": "2",
                                            "validFrom": "0001-01-01T00:00:00",
                                            "validTo": "9999-12-31T00:00:00"
                                        },
                                        {
                                            "id": 22992,
                                            "text": "Annan orsak",
                                            "value": "3",
                                            "validFrom": "0001-01-01T00:00:00",
                                            "validTo": "9999-12-31T00:00:00"
                                        },
                                        {
                                            "id": 22993,
                                            "text": "Konservativ endokrin terapi",
                                            "value": "4",
                                            "validFrom": "0001-01-01T00:00:00",
                                            "validTo": "9999-12-31T00:00:00"
                                        }
                                    ]
                                },
                                "7202": {
                                    "name": "a_pat_sida",
                                    "shortName": "a_pat_sida",
                                    "description": "Sida",
                                    "dataType": "list",
                                    "listValues": [
                                        {
                                            "id": 22849,
                                            "text": "Höger",
                                            "value": "1",
                                            "validFrom": "0001-01-01T00:00:00",
                                            "validTo": "9999-12-31T00:00:00"
                                        },
                                        {
                                            "id": 22850,
                                            "text": "Vänster",
                                            "value": "2",
                                            "validFrom": "0001-01-01T00:00:00",
                                            "validTo": "9999-12-31T00:00:00"
                                        }
                                    ]
                                },
                                "7203": {
                                    "name": "a_tnm_nklass",
                                    "shortName": "a_tnm_nklass",
                                    "description": "N-Klassifikation - Tumör i regionala lymfkörtlar",
                                    "dataType": "list",
                                    "listValues": [
                                        {
                                            "id": 22874,
                                            "text": "N0 Inga regionala lymfkörtelmetastaser",
                                            "value": "00",
                                            "validFrom": "0001-01-01T00:00:00",
                                            "validTo": "9999-12-31T00:00:00"
                                        },
                                        {
                                            "id": 22875,
                                            "text": "N1 Friliggande lymfkörtelmetastas(-er) i axill ipsilateralt",
                                            "value": "10",
                                            "validFrom": "0001-01-01T00:00:00",
                                            "validTo": "9999-12-31T00:00:00"
                                        },
                                        {
                                            "id": 22876,
                                            "text": "N2 Fixerad(-e) lymfkörtelmetastas(-er) i axill ipsilateralt eller intramammärt utan axillmetastaser",
                                            "value": "20",
                                            "validFrom": "0001-01-01T00:00:00",
                                            "validTo": "9999-12-31T00:00:00"
                                        },
                                        {
                                            "id": 22877,
                                            "text": "N3 Lymfkörtelmetastas(-er) i fossa scl/icl ipsilateralt eller intramammärt i kombination med axillmetastaser",
                                            "value": "30",
                                            "validFrom": "0001-01-01T00:00:00",
                                            "validTo": "9999-12-31T00:00:00"
                                        }
                                    ]
                                },
                                "7204": {
                                    "name": "a_tnm_mklass",
                                    "shortName": "a_tnm_mklass",
                                    "description": "M-Klassifikation",
                                    "dataType": "list",
                                    "listValues": [
                                        {
                                            "id": 22878,
                                            "text": "M0 Inga kända fjärrmetastaser",
                                            "value": "00",
                                            "validFrom": "0001-01-01T00:00:00",
                                            "validTo": "9999-12-31T00:00:00"
                                        },
                                        {
                                            "id": 22879,
                                            "text": "M1 Fjärrmetastaser finns",
                                            "value": "10",
                                            "validFrom": "0001-01-01T00:00:00",
                                            "validTo": "9999-12-31T00:00:00"
                                        }
                                    ]
                                },
                                "7205": {
                                    "name": "a_tnm_tklass",
                                    "shortName": "a_tnm_tklass",
                                    "description": "T-Klassifikation - Primärtumörer",
                                    "dataType": "list",
                                    "listValues": [
                                        {
                                            "id": 22864,
                                            "text": "Ingen uppenbar primärtumör",
                                            "value": "00",
                                            "validFrom": "0001-01-01T00:00:00",
                                            "validTo": "9999-12-31T00:00:00"
                                        },
                                        {
                                            "id": 22865,
                                            "text": "Tis Cancer in Situ",
                                            "value": "05",
                                            "validFrom": "0001-01-01T00:00:00",
                                            "validTo": "9999-12-31T00:00:00"
                                        },
                                        {
                                            "id": 22866,
                                            "text": "T1 Tumör ≤ 20 mm",
                                            "value": "10",
                                            "validFrom": "0001-01-01T00:00:00",
                                            "validTo": "9999-12-31T00:00:00"
                                        },
                                        {
                                            "id": 22867,
                                            "text": "T2 Tumör > 20 mm och ≤ 50 mm",
                                            "value": "20",
                                            "validFrom": "0001-01-01T00:00:00",
                                            "validTo": "9999-12-31T00:00:00"
                                        },
                                        {
                                            "id": 22868,
                                            "text": "T3 Tumör > 50 mm",
                                            "value": "30",
                                            "validFrom": "0001-01-01T00:00:00",
                                            "validTo": "9999-12-31T00:00:00"
                                        },
                                        {
                                            "id": 22869,
                                            "text": "T4a Fixerad mot bröstkorgsväggen",
                                            "value": "42",
                                            "validFrom": "0001-01-01T00:00:00",
                                            "validTo": "9999-12-31T00:00:00"
                                        },
                                        {
                                            "id": 22870,
                                            "text": "T4b Ulceration, ipsilateral kutan satellittumör eller hudödem (inkl peau d'orange)",
                                            "value": "44",
                                            "validFrom": "0001-01-01T00:00:00",
                                            "validTo": "9999-12-31T00:00:00"
                                        },
                                        {
                                            "id": 22871,
                                            "text": "T4c Både T4a och T4b",
                                            "value": "45",
                                            "validFrom": "0001-01-01T00:00:00",
                                            "validTo": "9999-12-31T00:00:00"
                                        },
                                        {
                                            "id": 22872,
                                            "text": "T4d Inflammatorisk bröstcancer",
                                            "value": "46",
                                            "validFrom": "0001-01-01T00:00:00",
                                            "validTo": "9999-12-31T00:00:00"
                                        },
                                        {
                                            "id": 22873,
                                            "text": "TX Primär tumör kan ej bedöma",
                                            "value": "50",
                                            "validFrom": "0001-01-01T00:00:00",
                                            "validTo": "9999-12-31T00:00:00"
                                        }
                                    ]
                                }
                            },
                            "treeLevel": 1
                        }
                    },
                    "designedFormId": 6867,
                    "data": {
                        "id": 10,
                        "subTables": {},
                        "regvars": {
                            "a_diag_besdat": {
                                "value": null,
                                "include": true
                            },
                            "a_diag_dat": {
                                "value": null,
                                "include": true
                            },
                            "a_diag_misscadat": {
                                "value": null,
                                "include": true
                            },
                            "a_inr_allkom": {
                                "value": null,
                                "include": true
                            },
                            "a_inr_anmlak": {
                                "value": null,
                                "include": true
                            },
                            "a_inr_dat": {
                                "value": null,
                                "include": true
                            },
                            "a_inr_enh": {
                                "value": "OC Demo (0) - KS-Solna Demo (011001) - Demo Klinik (111)",
                                "include": true
                            },
                            "a_inr_initav": {
                                "value": "Helena Larsson",
                                "include": true
                            },
                            "a_inr_inrapportor": {
                                "value": null,
                                "include": true
                            },
                            "a_inr_klkkod": {
                                "value": "111",
                                "include": true
                            },
                            "a_inr_komplett": {
                                "value": null,
                                "include": true
                            },
                            "a_inr_monkom": {
                                "value": null,
                                "include": true
                            },
                            "a_inr_sjhkod": {
                                "value": "011001",
                                "include": true
                            },
                            "a_kir_klkkod": {
                                "value": "301",
                                "include": true
                            },
                            "a_kir_sjhklk": {
                                "value": 217,
                                "include": true
                            },
                            "a_kir_sjhklkshowall": {
                                "value": null,
                                "include": true
                            },
                            "a_kir_sjhkod": {
                                "value": "540100",
                                "include": true
                            },
                            "a_uppf_sjhklkkirsamma": {
                                "value": null,
                                "include": true
                            },
                            "a_beh_sjhklkonksamma": {
                                "value": null,
                                "include": true
                            },
                            "a_onk_klkkod": {
                                "value": null,
                                "include": true
                            },
                            "a_onk_sjhklk": {
                                "include": true
                            },
                            "a_onk_sjhkod": {
                                "value": null,
                                "include": true
                            },
                            "a_onk_sjhklkshowall": {
                                "value": null,
                                "include": true
                            },
                            "a_uppf_sjhklkonksamma": {
                                "value": null,
                                "include": true
                            },
                            "a_pad_pattxt": {
                                "value": null,
                                "include": true
                            },
                            "a_pad_patkod": {
                                "value": null,
                                "include": true
                            },
                            "a_pad_prepar": {
                                "value": null,
                                "include": true
                            },
                            "a_pad_prepnr": {
                                "value": null,
                                "include": true
                            },
                            "a_pat_alder": {
                                "value": null,
                                "include": true
                            },
                            "a_pat_lkfdia": {
                                "value": "148001",
                                "include": true
                            },
                            "a_beh_sjhklkkirsamma": {
                                "value": null,
                                "include": true
                            },
                            "a_adj_klkkod": {
                                "value": null,
                                "include": true
                            },
                            "a_adj_sjhkod": {
                                "value": null,
                                "include": true
                            },
                            "a_uppf_klkkod": {
                                "value": null,
                                "include": true
                            },
                            "a_uppf_sjhkod": {
                                "value": null,
                                "include": true
                            },
                            "a_pat_sida": {
                                "value": 22849,
                                "include": true
                            },
                            "a_diag_screening": {
                                "value": null,
                                "include": true
                            },
                            "a_diag_preopmorf": {
                                "value": null,
                                "include": true
                            },
                            "a_diag_grund": {
                                "value": null,
                                "include": true
                            },
                            "a_pat_mensstat": {
                                "value": null,
                                "include": true
                            },
                            "a_tnm_tklass": {
                                "value": null,
                                "include": true
                            },
                            "a_tnm_nklass": {
                                "value": null,
                                "include": true
                            },
                            "a_tnm_mklass": {
                                "value": null,
                                "include": true
                            },
                            "a_planbeh_infopatdat": {
                                "value": null,
                                "include": true
                            },
                            "a_omv_kssk": {
                                "value": null,
                                "include": true
                            },
                            "a_omv_indivplan": {
                                "value": null,
                                "include": true
                            },
                            "a_mdk_preop": {
                                "value": null,
                                "include": true
                            },
                            "a_mdk_prekir": {
                                "value": null,
                                "include": true
                            },
                            "a_mdk_preonk": {
                                "value": null,
                                "include": true
                            },
                            "a_mdk_prepat": {
                                "value": null,
                                "include": true
                            },
                            "a_mdk_prerad": {
                                "value": null,
                                "include": true
                            },
                            "a_mdk_pressk": {
                                "value": null,
                                "include": true
                            },
                            "a_prlniva": {
                                "value": null,
                                "include": true
                            },
                            "a_ghmorgonensniva": {
                                "value": null,
                                "include": true
                            },
                            "a_forsk_stud": {
                                "value": null,
                                "include": true
                            },
                            "a_planbeh_typ": {
                                "value": null,
                                "include": true
                            },
                            "a_planbeh_typejors": {
                                "value": null,
                                "include": true
                            },
                            "a_planbeh_preopstral": {
                                "value": null,
                                "include": true
                            },
                            "a_planbeh_preopendo": {
                                "value": null,
                                "include": true
                            },
                            "a_planbeh_preopcyt": {
                                "value": null,
                                "include": true
                            },
                            "a_planbeh_preopantikropp": {
                                "value": null,
                                "include": true
                            },
                            "a_pad_typ": {
                                "value": null,
                                "include": true
                            },
                            "a_pad_ej": {
                                "value": null,
                                "include": true
                            },
                            "a_pad_pattxt2": {
                                "value": null,
                                "include": true
                            },
                            "a_pad_patkod2": {
                                "value": null,
                                "include": true
                            },
                            "a_pad_prepnr2": {
                                "value": null,
                                "include": true
                            },
                            "a_pad_prepar2": {
                                "value": null,
                                "include": true
                            },
                            "a_pad_invasiv": {
                                "value": null,
                                "include": true
                            },
                            "a_pad_invasivhist": {
                                "value": null,
                                "include": true
                            },
                            "a_pad_invasivhistann": {
                                "value": null,
                                "include": true
                            },
                            "a_pad_invasivsnomed": {
                                "value": null,
                                "include": true
                            },
                            "a_pad_insituhist": {
                                "value": null,
                                "include": true
                            },
                            "a_pad_insituhistann": {
                                "value": null,
                                "include": true
                            },
                            "a_pad_insitusnomed": {
                                "value": null,
                                "include": true
                            },
                            "a_pad_nhg": {
                                "value": null,
                                "include": true
                            },
                            "a_pad_erproc": {
                                "value": null,
                                "include": true
                            },
                            "a_pad_er": {
                                "value": null,
                                "include": true
                            },
                            "a_pad_prproc": {
                                "value": null,
                                "include": true
                            },
                            "a_pad_pr": {
                                "value": null,
                                "include": true
                            },
                            "a_pad_her2imm": {
                                "value": null,
                                "include": true
                            },
                            "a_pad_her2ish": {
                                "value": null,
                                "include": true
                            },
                            "a_pad_ki67proc": {
                                "value": null,
                                "include": true
                            },
                            "a_pad_ki67": {
                                "value": null,
                                "include": true
                            },
                            "a_adj_sjhklk": {
                                "value": null,
                                "include": true
                            },
                            "a_uppf_sjhklk": {
                                "value": null,
                                "include": true
                            },
                            "a_uppf_sjhklkshowall": {
                                "value": 217,
                                "include": true
                            },
                            "a_uppf_ingen": {
                                "value": null,
                                "include": true
                            },
                            "a_uppf_ingenors": {
                                "value": null,
                                "include": true
                            },
                            "a_uppf_sjhann": {
                                "value": null,
                                "include": true
                            },
                            "a_uppf_sjhanntxt": {
                                "value": null,
                                "include": true
                            }
                        },
                        "vdlists": {
                            "vd_anmalan_bc": {
                                "value": null,
                                "include": true
                            }
                        },
                        "errandId": 3031315
                    },
                    "listValuesValidFrom": "0001-01-01",
                    "listValuesValidTo": "9999-12-31",
                    "env": {
                        "_INREPNAME": "Helena Larsson",
                        "_INUNITNAME": "OC Demo (0) - KS-Solna Demo (011001) - Demo Klinik (111)",
                        "_REPORTERNAME": "Helena Larsson",
                        "_ADDRESS": "AVENYN",
                        "_AVREGDATUM": "1292-11-11",
                        "_AVREGORSAK": "AV",
                        "_BEFOLKNINGSREGISTER": "Demo Befolkningsregister",
                        "_CITY": "GÖTEBORG",
                        "_DATEOFDEATH": "1292-11-11",
                        "_DISTRICT": "",
                        "_DODSORSAK": "",
                        "_DODSORSAKSBESKR": "",
                        "_FIRSTNAME": "FIKTIV",
                        "_FODELSEDATUM": "1212-12-12",
                        "_FORSAMLING": "01",
                        "_KOMMUN": "80",
                        "_LAN": "14",
                        "_LAND": "Sweden",
                        "_LKF": "148001",
                        "_NAVETDATUM": "2012-05-08",
                        "_PATIENTID": "110",
                        "_PERSNR": "12121212-1212",
                        "_POSTALCODE": "30000",
                        "_SECRET": "False",
                        "_SEX": "M",
                        "_SURNAME": "PERSON"
                    },
                    "isReadOnly": false,
                    "useHtmlLayout": true,
                    "width": 850,
                    "name": "Anmälan"
                },
                "overview": {
                    "supportsTabs": true,
                    "tabs": {}
                }
            },
            "recording": {
                "getValueDomainValues": {
                    "[[\"error\",null],[\"parameters\",[[\"patientid\",\"110\"]]],[\"success\",null],[\"vdlist\",\"VD_vd_anmalan_bc\"]]": [
                        []
                    ],
                    "[[\"error\",null],[\"success\",null],[\"vdlist\",\"a_kir_sjhklk\"]]": [
                        [
                            {
                                "text": "Organisationsenhet(kod):0 - 0650120 - 301,Organisationsenhet(namn och kod):OC Demo - Gällivare - Kir klin",
                                "id": 197,
                                "data": {
                                    "PosId": 197,
                                    "PosCode": "0 - 0650120 - 301",
                                    "PosName": "OC Demo - Gällivare - Kir klin",
                                    "PosNameWithCode": "OC Demo (0) - Gällivare (0650120) - Kir klin (301)",
                                    "PosId4": 197,
                                    "PosLevel": 3,
                                    "UnitCode": "301",
                                    "TopPosId": 33,
                                    "TopPosCode": "0"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):0 - 540100 - 301,Organisationsenhet(namn och kod):OC Demo - Karlstad - Kir klinik",
                                "id": 217,
                                "data": {
                                    "PosId": 217,
                                    "PosCode": "0 - 540100 - 301",
                                    "PosName": "OC Demo - Karlstad - Kir klinik",
                                    "PosNameWithCode": "OC Demo (0) - Karlstad (540100) - Kir klinik (301)",
                                    "PosId4": 217,
                                    "PosLevel": 3,
                                    "UnitCode": "301",
                                    "TopPosId": 33,
                                    "TopPosCode": "0"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):0 - 065015 - 301,Organisationsenhet(namn och kod):OC Demo - Kiruna - Kir klin",
                                "id": 203,
                                "data": {
                                    "PosId": 203,
                                    "PosCode": "0 - 065015 - 301",
                                    "PosName": "OC Demo - Kiruna - Kir klin",
                                    "PosNameWithCode": "OC Demo (0) - Kiruna (065015) - Kir klin (301)",
                                    "PosId4": 203,
                                    "PosLevel": 3,
                                    "UnitCode": "301",
                                    "TopPosId": 33,
                                    "TopPosCode": "0"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):0 - 0111001 - 741,Organisationsenhet(namn och kod):OC Demo - KS Solna - Demo Onkologen",
                                "id": 1288,
                                "data": {
                                    "PosId": 1288,
                                    "PosCode": "0 - 0111001 - 741",
                                    "PosName": "OC Demo - KS Solna - Demo Onkologen",
                                    "PosNameWithCode": "OC Demo (0) - KS Solna (0111001) - Demo Onkologen (741)",
                                    "PosId4": 1288,
                                    "PosLevel": 3,
                                    "UnitCode": "741",
                                    "TopPosId": 33,
                                    "TopPosCode": "0"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):0 - 011001 - 111,Organisationsenhet(namn och kod):OC Demo - KS-Solna Demo - Demo Klinik",
                                "id": 1292,
                                "data": {
                                    "PosId": 1292,
                                    "PosCode": "0 - 011001 - 111",
                                    "PosName": "OC Demo - KS-Solna Demo - Demo Klinik",
                                    "PosNameWithCode": "OC Demo (0) - KS-Solna Demo (011001) - Demo Klinik (111)",
                                    "PosId4": 1292,
                                    "PosLevel": 3,
                                    "UnitCode": "111",
                                    "TopPosId": 33,
                                    "TopPosCode": "0"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):0 - 210010 - 301,Organisationsenhet(namn och kod):OC Demo - Linköping US - Kirurgkliniken",
                                "id": 136,
                                "data": {
                                    "PosId": 136,
                                    "PosCode": "0 - 210010 - 301",
                                    "PosName": "OC Demo - Linköping US - Kirurgkliniken",
                                    "PosNameWithCode": "OC Demo (0) - Linköping US (210010) - Kirurgkliniken (301)",
                                    "PosId4": 136,
                                    "PosLevel": 3,
                                    "UnitCode": "301",
                                    "TopPosId": 33,
                                    "TopPosCode": "0"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):0 - 210010 - 111,Organisationsenhet(namn och kod):OC Demo - Linköping US - Lungmed",
                                "id": 989,
                                "data": {
                                    "PosId": 989,
                                    "PosCode": "0 - 210010 - 111",
                                    "PosName": "OC Demo - Linköping US - Lungmed",
                                    "PosNameWithCode": "OC Demo (0) - Linköping US (210010) - Lungmed (111)",
                                    "PosId4": 989,
                                    "PosLevel": 3,
                                    "UnitCode": "111",
                                    "TopPosId": 33,
                                    "TopPosCode": "0"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):0 - 210010 - 521,Organisationsenhet(namn och kod):OC Demo - Linköping US - ÖNH",
                                "id": 540,
                                "data": {
                                    "PosId": 540,
                                    "PosCode": "0 - 210010 - 521",
                                    "PosName": "OC Demo - Linköping US - ÖNH",
                                    "PosNameWithCode": "OC Demo (0) - Linköping US (210010) - ÖNH (521)",
                                    "PosId4": 540,
                                    "PosLevel": 3,
                                    "UnitCode": "521",
                                    "TopPosId": 33,
                                    "TopPosCode": "0"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):0 - 410010 - 105,Organisationsenhet(namn och kod):OC Demo - Lund - Hematologiska klinken",
                                "id": 459,
                                "data": {
                                    "PosId": 459,
                                    "PosCode": "0 - 410010 - 105",
                                    "PosName": "OC Demo - Lund - Hematologiska klinken",
                                    "PosNameWithCode": "OC Demo (0) - Lund (410010) - Hematologiska klinken (105)",
                                    "PosId4": 459,
                                    "PosLevel": 3,
                                    "UnitCode": "105",
                                    "TopPosId": 33,
                                    "TopPosCode": "0"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):0 - 410010 - 301,Organisationsenhet(namn och kod):OC Demo - Lund - Kirurgen",
                                "id": 121,
                                "data": {
                                    "PosId": 121,
                                    "PosCode": "0 - 410010 - 301",
                                    "PosName": "OC Demo - Lund - Kirurgen",
                                    "PosNameWithCode": "OC Demo (0) - Lund (410010) - Kirurgen (301)",
                                    "PosId4": 121,
                                    "PosLevel": 3,
                                    "UnitCode": "301",
                                    "TopPosId": 33,
                                    "TopPosCode": "0"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):0 - 410010 - 451,Organisationsenhet(namn och kod):OC Demo - Lund - kvinnoklinik",
                                "id": 1111,
                                "data": {
                                    "PosId": 1111,
                                    "PosCode": "0 - 410010 - 451",
                                    "PosName": "OC Demo - Lund - kvinnoklinik",
                                    "PosNameWithCode": "OC Demo (0) - Lund (410010) - kvinnoklinik (451)",
                                    "PosId4": 1111,
                                    "PosLevel": 3,
                                    "UnitCode": "451",
                                    "TopPosId": 33,
                                    "TopPosCode": "0"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):0 - 410010 - 111,Organisationsenhet(namn och kod):OC Demo - Lund - Lungmedicin",
                                "id": 1109,
                                "data": {
                                    "PosId": 1109,
                                    "PosCode": "0 - 410010 - 111",
                                    "PosName": "OC Demo - Lund - Lungmedicin",
                                    "PosNameWithCode": "OC Demo (0) - Lund (410010) - Lungmedicin (111)",
                                    "PosId4": 1109,
                                    "PosLevel": 3,
                                    "UnitCode": "111",
                                    "TopPosId": 33,
                                    "TopPosCode": "0"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):0 - 410010 - 101,Organisationsenhet(namn och kod):OC Demo - Lund - Medicinska kliniken",
                                "id": 1112,
                                "data": {
                                    "PosId": 1112,
                                    "PosCode": "0 - 410010 - 101",
                                    "PosName": "OC Demo - Lund - Medicinska kliniken",
                                    "PosNameWithCode": "OC Demo (0) - Lund (410010) - Medicinska kliniken (101)",
                                    "PosId4": 1112,
                                    "PosLevel": 3,
                                    "UnitCode": "101",
                                    "TopPosId": 33,
                                    "TopPosCode": "0"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):0 - 410010 - 741,Organisationsenhet(namn och kod):OC Demo - Lund - Onkologiska kliniken",
                                "id": 1105,
                                "data": {
                                    "PosId": 1105,
                                    "PosCode": "0 - 410010 - 741",
                                    "PosName": "OC Demo - Lund - Onkologiska kliniken",
                                    "PosNameWithCode": "OC Demo (0) - Lund (410010) - Onkologiska kliniken (741)",
                                    "PosId4": 1105,
                                    "PosLevel": 3,
                                    "UnitCode": "741",
                                    "TopPosId": 33,
                                    "TopPosCode": "0"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):0 - 410010 - 361,Organisationsenhet(namn och kod):OC Demo - Lund - Uro klin",
                                "id": 1107,
                                "data": {
                                    "PosId": 1107,
                                    "PosCode": "0 - 410010 - 361",
                                    "PosName": "OC Demo - Lund - Uro klin",
                                    "PosNameWithCode": "OC Demo (0) - Lund (410010) - Uro klin (361)",
                                    "PosId4": 1107,
                                    "PosLevel": 3,
                                    "UnitCode": "361",
                                    "TopPosId": 33,
                                    "TopPosCode": "0"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):0 - 410010 - 521,Organisationsenhet(namn och kod):OC Demo - Lund - ÖNH",
                                "id": 1110,
                                "data": {
                                    "PosId": 1110,
                                    "PosCode": "0 - 410010 - 521",
                                    "PosName": "OC Demo - Lund - ÖNH",
                                    "PosNameWithCode": "OC Demo (0) - Lund (410010) - ÖNH (521)",
                                    "PosId4": 1110,
                                    "PosLevel": 3,
                                    "UnitCode": "521",
                                    "TopPosId": 33,
                                    "TopPosCode": "0"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):0 - 41001 - 391,Organisationsenhet(namn och kod):OC Demo - Lunds Universitetssjukhuset - Demo kirugen",
                                "id": 1114,
                                "data": {
                                    "PosId": 1114,
                                    "PosCode": "0 - 41001 - 391",
                                    "PosName": "OC Demo - Lunds Universitetssjukhuset - Demo kirugen",
                                    "PosNameWithCode": "OC Demo (0) - Lunds Universitetssjukhuset (41001) - Demo kirugen (391)",
                                    "PosId4": 1114,
                                    "PosLevel": 3,
                                    "UnitCode": "391",
                                    "TopPosId": 33,
                                    "TopPosCode": "0"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):0 - 41001 - 392,Organisationsenhet(namn och kod):OC Demo - Lunds Universitetssjukhuset - Demo kirurgiska kliniken",
                                "id": 1120,
                                "data": {
                                    "PosId": 1120,
                                    "PosCode": "0 - 41001 - 392",
                                    "PosName": "OC Demo - Lunds Universitetssjukhuset - Demo kirurgiska kliniken",
                                    "PosNameWithCode": "OC Demo (0) - Lunds Universitetssjukhuset (41001) - Demo kirurgiska kliniken (392)",
                                    "PosId4": 1120,
                                    "PosLevel": 3,
                                    "UnitCode": "392",
                                    "TopPosId": 33,
                                    "TopPosCode": "0"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):0 - 41001 - 491,Organisationsenhet(namn och kod):OC Demo - Lunds Universitetssjukhuset - demo KK",
                                "id": 1121,
                                "data": {
                                    "PosId": 1121,
                                    "PosCode": "0 - 41001 - 491",
                                    "PosName": "OC Demo - Lunds Universitetssjukhuset - demo KK",
                                    "PosNameWithCode": "OC Demo (0) - Lunds Universitetssjukhuset (41001) - demo KK (491)",
                                    "PosId4": 1121,
                                    "PosLevel": 3,
                                    "UnitCode": "491",
                                    "TopPosId": 33,
                                    "TopPosCode": "0"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):0 - 41001 - 492,Organisationsenhet(namn och kod):OC Demo - Lunds Universitetssjukhuset - Demo kvinnoklinen",
                                "id": 1118,
                                "data": {
                                    "PosId": 1118,
                                    "PosCode": "0 - 41001 - 492",
                                    "PosName": "OC Demo - Lunds Universitetssjukhuset - Demo kvinnoklinen",
                                    "PosNameWithCode": "OC Demo (0) - Lunds Universitetssjukhuset (41001) - Demo kvinnoklinen (492)",
                                    "PosId4": 1118,
                                    "PosLevel": 3,
                                    "UnitCode": "492",
                                    "TopPosId": 33,
                                    "TopPosCode": "0"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):0 - 41001 - 101,Organisationsenhet(namn och kod):OC Demo - Lunds Universitetssjukhuset - Demo med klin",
                                "id": 1115,
                                "data": {
                                    "PosId": 1115,
                                    "PosCode": "0 - 41001 - 101",
                                    "PosName": "OC Demo - Lunds Universitetssjukhuset - Demo med klin",
                                    "PosNameWithCode": "OC Demo (0) - Lunds Universitetssjukhuset (41001) - Demo med klin (101)",
                                    "PosId4": 1115,
                                    "PosLevel": 3,
                                    "UnitCode": "101",
                                    "TopPosId": 33,
                                    "TopPosCode": "0"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):0 - 41001 - 791,Organisationsenhet(namn och kod):OC Demo - Lunds Universitetssjukhuset - Demo onkologen",
                                "id": 1124,
                                "data": {
                                    "PosId": 1124,
                                    "PosCode": "0 - 41001 - 791",
                                    "PosName": "OC Demo - Lunds Universitetssjukhuset - Demo onkologen",
                                    "PosNameWithCode": "OC Demo (0) - Lunds Universitetssjukhuset (41001) - Demo onkologen (791)",
                                    "PosId4": 1124,
                                    "PosLevel": 3,
                                    "UnitCode": "791",
                                    "TopPosId": 33,
                                    "TopPosCode": "0"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):0 - 41001 - 792,Organisationsenhet(namn och kod):OC Demo - Lunds Universitetssjukhuset - Demo onkologiska kliniken",
                                "id": 1117,
                                "data": {
                                    "PosId": 1117,
                                    "PosCode": "0 - 41001 - 792",
                                    "PosName": "OC Demo - Lunds Universitetssjukhuset - Demo onkologiska kliniken",
                                    "PosNameWithCode": "OC Demo (0) - Lunds Universitetssjukhuset (41001) - Demo onkologiska kliniken (792)",
                                    "PosId4": 1117,
                                    "PosLevel": 3,
                                    "UnitCode": "792",
                                    "TopPosId": 33,
                                    "TopPosCode": "0"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):0 - 41001 - 393,Organisationsenhet(namn och kod):OC Demo - Lunds Universitetssjukhuset - demo urologen",
                                "id": 1122,
                                "data": {
                                    "PosId": 1122,
                                    "PosCode": "0 - 41001 - 393",
                                    "PosName": "OC Demo - Lunds Universitetssjukhuset - demo urologen",
                                    "PosNameWithCode": "OC Demo (0) - Lunds Universitetssjukhuset (41001) - demo urologen (393)",
                                    "PosId4": 1122,
                                    "PosLevel": 3,
                                    "UnitCode": "393",
                                    "TopPosId": 33,
                                    "TopPosCode": "0"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):0 - 41001 - 394,Organisationsenhet(namn och kod):OC Demo - Lunds Universitetssjukhuset - Demo urologen",
                                "id": 1119,
                                "data": {
                                    "PosId": 1119,
                                    "PosCode": "0 - 41001 - 394",
                                    "PosName": "OC Demo - Lunds Universitetssjukhuset - Demo urologen",
                                    "PosNameWithCode": "OC Demo (0) - Lunds Universitetssjukhuset (41001) - Demo urologen (394)",
                                    "PosId4": 1119,
                                    "PosLevel": 3,
                                    "UnitCode": "394",
                                    "TopPosId": 33,
                                    "TopPosCode": "0"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):0 - 41001 - 591,Organisationsenhet(namn och kod):OC Demo - Lunds Universitetssjukhuset - Demo ÖNH",
                                "id": 1123,
                                "data": {
                                    "PosId": 1123,
                                    "PosCode": "0 - 41001 - 591",
                                    "PosName": "OC Demo - Lunds Universitetssjukhuset - Demo ÖNH",
                                    "PosNameWithCode": "OC Demo (0) - Lunds Universitetssjukhuset (41001) - Demo ÖNH (591)",
                                    "PosId4": 1123,
                                    "PosLevel": 3,
                                    "UnitCode": "591",
                                    "TopPosId": 33,
                                    "TopPosCode": "0"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):0 - 41001 - 592,Organisationsenhet(namn och kod):OC Demo - Lunds Universitetssjukhuset - demo öron och hals",
                                "id": 1116,
                                "data": {
                                    "PosId": 1116,
                                    "PosCode": "0 - 41001 - 592",
                                    "PosName": "OC Demo - Lunds Universitetssjukhuset - demo öron och hals",
                                    "PosNameWithCode": "OC Demo (0) - Lunds Universitetssjukhuset (41001) - demo öron och hals (592)",
                                    "PosId4": 1116,
                                    "PosLevel": 3,
                                    "UnitCode": "592",
                                    "TopPosId": 33,
                                    "TopPosCode": "0"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):0 - 064011 - 301,Organisationsenhet(namn och kod):OC Demo - Lycksele - Kir klin",
                                "id": 191,
                                "data": {
                                    "PosId": 191,
                                    "PosCode": "0 - 064011 - 301",
                                    "PosName": "OC Demo - Lycksele - Kir klin",
                                    "PosNameWithCode": "OC Demo (0) - Lycksele (064011) - Kir klin (301)",
                                    "PosId4": 191,
                                    "PosLevel": 3,
                                    "UnitCode": "301",
                                    "TopPosId": 33,
                                    "TopPosCode": "0"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):0 - 064001 - 301,Organisationsenhet(namn och kod):OC Demo - NUS - Kirurgen",
                                "id": 123,
                                "data": {
                                    "PosId": 123,
                                    "PosCode": "0 - 064001 - 301",
                                    "PosName": "OC Demo - NUS - Kirurgen",
                                    "PosNameWithCode": "OC Demo (0) - NUS (064001) - Kirurgen (301)",
                                    "PosId4": 123,
                                    "PosLevel": 3,
                                    "UnitCode": "301",
                                    "TopPosId": 33,
                                    "TopPosCode": "0"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):0 - 064001 - 361,Organisationsenhet(namn och kod):OC Demo - NUS - Urol klin",
                                "id": 129,
                                "data": {
                                    "PosId": 129,
                                    "PosCode": "0 - 064001 - 361",
                                    "PosName": "OC Demo - NUS - Urol klin",
                                    "PosNameWithCode": "OC Demo (0) - NUS (064001) - Urol klin (361)",
                                    "PosId4": 129,
                                    "PosLevel": 3,
                                    "UnitCode": "361",
                                    "TopPosId": 33,
                                    "TopPosCode": "0"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):0 - 0650130 - 301,Organisationsenhet(namn och kod):OC Demo - Piteå - Kir klin",
                                "id": 199,
                                "data": {
                                    "PosId": 199,
                                    "PosCode": "0 - 0650130 - 301",
                                    "PosName": "OC Demo - Piteå - Kir klin",
                                    "PosNameWithCode": "OC Demo (0) - Piteå (0650130) - Kir klin (301)",
                                    "PosId4": 199,
                                    "PosLevel": 3,
                                    "UnitCode": "301",
                                    "TopPosId": 33,
                                    "TopPosCode": "0"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):0 - 0640100 - 301,Organisationsenhet(namn och kod):OC Demo - Skellefteå - Kir klin",
                                "id": 189,
                                "data": {
                                    "PosId": 189,
                                    "PosCode": "0 - 0640100 - 301",
                                    "PosName": "OC Demo - Skellefteå - Kir klin",
                                    "PosNameWithCode": "OC Demo (0) - Skellefteå (0640100) - Kir klin (301)",
                                    "PosId4": 189,
                                    "PosLevel": 3,
                                    "UnitCode": "301",
                                    "TopPosId": 33,
                                    "TopPosCode": "0"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):0 - 530130 - 301,Organisationsenhet(namn och kod):OC Demo - Skövde - Kirurgen",
                                "id": 41,
                                "data": {
                                    "PosId": 41,
                                    "PosCode": "0 - 530130 - 301",
                                    "PosName": "OC Demo - Skövde - Kirurgen",
                                    "PosNameWithCode": "OC Demo (0) - Skövde (530130) - Kirurgen (301)",
                                    "PosId4": 41,
                                    "PosLevel": 3,
                                    "UnitCode": "301",
                                    "TopPosId": 33,
                                    "TopPosCode": "0"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):0 - 530130 - 101,Organisationsenhet(namn och kod):OC Demo - Skövde - Medicinkliniken",
                                "id": 40,
                                "data": {
                                    "PosId": 40,
                                    "PosCode": "0 - 530130 - 101",
                                    "PosName": "OC Demo - Skövde - Medicinkliniken",
                                    "PosNameWithCode": "OC Demo (0) - Skövde (530130) - Medicinkliniken (101)",
                                    "PosId4": 40,
                                    "PosLevel": 3,
                                    "UnitCode": "101",
                                    "TopPosId": 33,
                                    "TopPosCode": "0"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):0 - 530130 - 521,Organisationsenhet(namn och kod):OC Demo - Skövde - ÖNH",
                                "id": 42,
                                "data": {
                                    "PosId": 42,
                                    "PosCode": "0 - 530130 - 521",
                                    "PosName": "OC Demo - Skövde - ÖNH",
                                    "PosNameWithCode": "OC Demo (0) - Skövde (530130) - ÖNH (521)",
                                    "PosId4": 42,
                                    "PosLevel": 3,
                                    "UnitCode": "521",
                                    "TopPosId": 33,
                                    "TopPosCode": "0"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):0 - 0620130 - 301,Organisationsenhet(namn och kod):OC Demo - Sollefteå - Kir klin",
                                "id": 525,
                                "data": {
                                    "PosId": 525,
                                    "PosCode": "0 - 0620130 - 301",
                                    "PosName": "OC Demo - Sollefteå - Kir klin",
                                    "PosNameWithCode": "OC Demo (0) - Sollefteå (0620130) - Kir klin (301)",
                                    "PosId4": 525,
                                    "PosLevel": 3,
                                    "UnitCode": "301",
                                    "TopPosId": 33,
                                    "TopPosCode": "0"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):0 - 500010 - 301,Organisationsenhet(namn och kod):OC Demo - SU/Sahlgrenska - Kirurgen",
                                "id": 38,
                                "data": {
                                    "PosId": 38,
                                    "PosCode": "0 - 500010 - 301",
                                    "PosName": "OC Demo - SU/Sahlgrenska - Kirurgen",
                                    "PosNameWithCode": "OC Demo (0) - SU/Sahlgrenska (500010) - Kirurgen (301)",
                                    "PosId4": 38,
                                    "PosLevel": 3,
                                    "UnitCode": "301",
                                    "TopPosId": 33,
                                    "TopPosCode": "0"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):0 - 500010 - 101,Organisationsenhet(namn och kod):OC Demo - SU/Sahlgrenska - Medicinkliniken",
                                "id": 36,
                                "data": {
                                    "PosId": 36,
                                    "PosCode": "0 - 500010 - 101",
                                    "PosName": "OC Demo - SU/Sahlgrenska - Medicinkliniken",
                                    "PosNameWithCode": "OC Demo (0) - SU/Sahlgrenska (500010) - Medicinkliniken (101)",
                                    "PosId4": 36,
                                    "PosLevel": 3,
                                    "UnitCode": "101",
                                    "TopPosId": 33,
                                    "TopPosCode": "0"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):0 - 500010 - 741,Organisationsenhet(namn och kod):OC Demo - SU/Sahlgrenska - Onkologiska kliniken",
                                "id": 39,
                                "data": {
                                    "PosId": 39,
                                    "PosCode": "0 - 500010 - 741",
                                    "PosName": "OC Demo - SU/Sahlgrenska - Onkologiska kliniken",
                                    "PosNameWithCode": "OC Demo (0) - SU/Sahlgrenska (500010) - Onkologiska kliniken (741)",
                                    "PosId4": 39,
                                    "PosLevel": 3,
                                    "UnitCode": "741",
                                    "TopPosId": 33,
                                    "TopPosCode": "0"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):0 - 500010 - 361,Organisationsenhet(namn och kod):OC Demo - SU/Sahlgrenska - Urol klin",
                                "id": 130,
                                "data": {
                                    "PosId": 130,
                                    "PosCode": "0 - 500010 - 361",
                                    "PosName": "OC Demo - SU/Sahlgrenska - Urol klin",
                                    "PosNameWithCode": "OC Demo (0) - SU/Sahlgrenska (500010) - Urol klin (361)",
                                    "PosId4": 130,
                                    "PosLevel": 3,
                                    "UnitCode": "361",
                                    "TopPosId": 33,
                                    "TopPosCode": "0"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):0 - 500010 - 521,Organisationsenhet(namn och kod):OC Demo - SU/Sahlgrenska - ÖNH",
                                "id": 37,
                                "data": {
                                    "PosId": 37,
                                    "PosCode": "0 - 500010 - 521",
                                    "PosName": "OC Demo - SU/Sahlgrenska - ÖNH",
                                    "PosNameWithCode": "OC Demo (0) - SU/Sahlgrenska (500010) - ÖNH (521)",
                                    "PosId4": 37,
                                    "PosLevel": 3,
                                    "UnitCode": "521",
                                    "TopPosId": 33,
                                    "TopPosCode": "0"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):0 - 0650160 - 301,Organisationsenhet(namn och kod):OC Demo - Sunderby sjukhus - Kir klin",
                                "id": 526,
                                "data": {
                                    "PosId": 526,
                                    "PosCode": "0 - 0650160 - 301",
                                    "PosName": "OC Demo - Sunderby sjukhus - Kir klin",
                                    "PosNameWithCode": "OC Demo (0) - Sunderby sjukhus (0650160) - Kir klin (301)",
                                    "PosId4": 526,
                                    "PosLevel": 3,
                                    "UnitCode": "301",
                                    "TopPosId": 33,
                                    "TopPosCode": "0"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):0 - 0650160 - 361,Organisationsenhet(namn och kod):OC Demo - Sunderby sjukhus - Urol klin",
                                "id": 195,
                                "data": {
                                    "PosId": 195,
                                    "PosCode": "0 - 0650160 - 361",
                                    "PosName": "OC Demo - Sunderby sjukhus - Urol klin",
                                    "PosNameWithCode": "OC Demo (0) - Sunderby sjukhus (0650160) - Urol klin (361)",
                                    "PosId4": 195,
                                    "PosLevel": 3,
                                    "UnitCode": "361",
                                    "TopPosId": 33,
                                    "TopPosCode": "0"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):0 - 0620100 - 301,Organisationsenhet(namn och kod):OC Demo - Sundsvall - Kir klin",
                                "id": 523,
                                "data": {
                                    "PosId": 523,
                                    "PosCode": "0 - 0620100 - 301",
                                    "PosName": "OC Demo - Sundsvall - Kir klin",
                                    "PosNameWithCode": "OC Demo (0) - Sundsvall (0620100) - Kir klin (301)",
                                    "PosId4": 523,
                                    "PosLevel": 3,
                                    "UnitCode": "301",
                                    "TopPosId": 33,
                                    "TopPosCode": "0"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):0 - 0620100 - 361,Organisationsenhet(namn och kod):OC Demo - Sundsvall - Urol klin",
                                "id": 205,
                                "data": {
                                    "PosId": 205,
                                    "PosCode": "0 - 0620100 - 361",
                                    "PosName": "OC Demo - Sundsvall - Urol klin",
                                    "PosNameWithCode": "OC Demo (0) - Sundsvall (0620100) - Urol klin (361)",
                                    "PosId4": 205,
                                    "PosLevel": 3,
                                    "UnitCode": "361",
                                    "TopPosId": 33,
                                    "TopPosCode": "0"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):0 - 120010 - 301,Organisationsenhet(namn och kod):OC Demo - UAS - Kir klin",
                                "id": 140,
                                "data": {
                                    "PosId": 140,
                                    "PosCode": "0 - 120010 - 301",
                                    "PosName": "OC Demo - UAS - Kir klin",
                                    "PosNameWithCode": "OC Demo (0) - UAS (120010) - Kir klin (301)",
                                    "PosId4": 140,
                                    "PosLevel": 3,
                                    "UnitCode": "301",
                                    "TopPosId": 33,
                                    "TopPosCode": "0"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):0 - 120010 - 666,Organisationsenhet(namn och kod):OC Demo - UAS - Lunga",
                                "id": 664,
                                "data": {
                                    "PosId": 664,
                                    "PosCode": "0 - 120010 - 666",
                                    "PosName": "OC Demo - UAS - Lunga",
                                    "PosNameWithCode": "OC Demo (0) - UAS (120010) - Lunga (666)",
                                    "PosId4": 664,
                                    "PosLevel": 3,
                                    "UnitCode": "666",
                                    "TopPosId": 33,
                                    "TopPosCode": "0"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):0 - 120010 - 731,Organisationsenhet(namn och kod):OC Demo - UAS - Radiologiska kiniken",
                                "id": 1414,
                                "data": {
                                    "PosId": 1414,
                                    "PosCode": "0 - 120010 - 731",
                                    "PosName": "OC Demo - UAS - Radiologiska kiniken",
                                    "PosNameWithCode": "OC Demo (0) - UAS (120010) - Radiologiska kiniken (731)",
                                    "PosId4": 1414,
                                    "PosLevel": 3,
                                    "UnitCode": "731",
                                    "TopPosId": 33,
                                    "TopPosCode": "0"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):0 - 120010 - 361,Organisationsenhet(namn och kod):OC Demo - UAS - Uro klin",
                                "id": 16823,
                                "data": {
                                    "PosId": 16823,
                                    "PosCode": "0 - 120010 - 361",
                                    "PosName": "OC Demo - UAS - Uro klin",
                                    "PosNameWithCode": "OC Demo (0) - UAS (120010) - Uro klin (361)",
                                    "PosId4": 16823,
                                    "PosLevel": 3,
                                    "UnitCode": "361",
                                    "TopPosId": 33,
                                    "TopPosCode": "0"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):0 - 510100 - 521,Organisationsenhet(namn och kod):OC Demo - Uddevalla - ÖNH",
                                "id": 517,
                                "data": {
                                    "PosId": 517,
                                    "PosCode": "0 - 510100 - 521",
                                    "PosName": "OC Demo - Uddevalla - ÖNH",
                                    "PosNameWithCode": "OC Demo (0) - Uddevalla (510100) - ÖNH (521)",
                                    "PosId4": 517,
                                    "PosLevel": 3,
                                    "UnitCode": "521",
                                    "TopPosId": 33,
                                    "TopPosCode": "0"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):0 - 560100 - 301,Organisationsenhet(namn och kod):OC Demo - Västerås - Kir klin",
                                "id": 125,
                                "data": {
                                    "PosId": 125,
                                    "PosCode": "0 - 560100 - 301",
                                    "PosName": "OC Demo - Västerås - Kir klin",
                                    "PosNameWithCode": "OC Demo (0) - Västerås (560100) - Kir klin (301)",
                                    "PosId4": 125,
                                    "PosLevel": 3,
                                    "UnitCode": "301",
                                    "TopPosId": 33,
                                    "TopPosCode": "0"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):0 - 0620110 - 301,Organisationsenhet(namn och kod):OC Demo - Örnsköldsvik - Kir klin",
                                "id": 528,
                                "data": {
                                    "PosId": 528,
                                    "PosCode": "0 - 0620110 - 301",
                                    "PosName": "OC Demo - Örnsköldsvik - Kir klin",
                                    "PosNameWithCode": "OC Demo (0) - Örnsköldsvik (0620110) - Kir klin (301)",
                                    "PosId4": 528,
                                    "PosLevel": 3,
                                    "UnitCode": "301",
                                    "TopPosId": 33,
                                    "TopPosCode": "0"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):0 - 0630100 - 301,Organisationsenhet(namn och kod):OC Demo - Östersund - Kir klin",
                                "id": 193,
                                "data": {
                                    "PosId": 193,
                                    "PosCode": "0 - 0630100 - 301",
                                    "PosName": "OC Demo - Östersund - Kir klin",
                                    "PosNameWithCode": "OC Demo (0) - Östersund (0630100) - Kir klin (301)",
                                    "PosId4": 193,
                                    "PosLevel": 3,
                                    "UnitCode": "301",
                                    "TopPosId": 33,
                                    "TopPosCode": "0"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):6 - 065012 - 301,Organisationsenhet(namn och kod):OC Norr - Gällivare sjukhus - Kir klin",
                                "id": 102,
                                "data": {
                                    "PosId": 102,
                                    "PosCode": "6 - 065012 - 301",
                                    "PosName": "OC Norr - Gällivare sjukhus - Kir klin",
                                    "PosNameWithCode": "OC Norr (6) - Gällivare sjukhus (065012) - Kir klin (301)",
                                    "PosId4": 102,
                                    "PosLevel": 3,
                                    "UnitCode": "301",
                                    "TopPosId": 1,
                                    "TopPosCode": "6"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):6 - 065012 - 451,Organisationsenhet(namn och kod):OC Norr - Gällivare sjukhus - Kvinnoklinik",
                                "id": 947,
                                "data": {
                                    "PosId": 947,
                                    "PosCode": "6 - 065012 - 451",
                                    "PosName": "OC Norr - Gällivare sjukhus - Kvinnoklinik",
                                    "PosNameWithCode": "OC Norr (6) - Gällivare sjukhus (065012) - Kvinnoklinik (451)",
                                    "PosId4": 947,
                                    "PosLevel": 3,
                                    "UnitCode": "451",
                                    "TopPosId": 1,
                                    "TopPosCode": "6"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):6 - 065012 - 101,Organisationsenhet(namn och kod):OC Norr - Gällivare sjukhus - Med klin",
                                "id": 416,
                                "data": {
                                    "PosId": 416,
                                    "PosCode": "6 - 065012 - 101",
                                    "PosName": "OC Norr - Gällivare sjukhus - Med klin",
                                    "PosNameWithCode": "OC Norr (6) - Gällivare sjukhus (065012) - Med klin (101)",
                                    "PosId4": 416,
                                    "PosLevel": 3,
                                    "UnitCode": "101",
                                    "TopPosId": 1,
                                    "TopPosCode": "6"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):6 - 065012 - 731,Organisationsenhet(namn och kod):OC Norr - Gällivare sjukhus - Röntgen",
                                "id": 1498,
                                "data": {
                                    "PosId": 1498,
                                    "PosCode": "6 - 065012 - 731",
                                    "PosName": "OC Norr - Gällivare sjukhus - Röntgen",
                                    "PosNameWithCode": "OC Norr (6) - Gällivare sjukhus (065012) - Röntgen (731)",
                                    "PosId4": 1498,
                                    "PosLevel": 3,
                                    "UnitCode": "731",
                                    "TopPosId": 1,
                                    "TopPosCode": "6"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):6 - 065012 - 521,Organisationsenhet(namn och kod):OC Norr - Gällivare sjukhus - ÖNH klin",
                                "id": 720,
                                "data": {
                                    "PosId": 720,
                                    "PosCode": "6 - 065012 - 521",
                                    "PosName": "OC Norr - Gällivare sjukhus - ÖNH klin",
                                    "PosNameWithCode": "OC Norr (6) - Gällivare sjukhus (065012) - ÖNH klin (521)",
                                    "PosId4": 720,
                                    "PosLevel": 3,
                                    "UnitCode": "521",
                                    "TopPosId": 1,
                                    "TopPosCode": "6"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):6 - 062012 - 301,Organisationsenhet(namn och kod):OC Norr - Härnösands sjukhus - Kir klin",
                                "id": 107,
                                "data": {
                                    "PosId": 107,
                                    "PosCode": "6 - 062012 - 301",
                                    "PosName": "OC Norr - Härnösands sjukhus - Kir klin",
                                    "PosNameWithCode": "OC Norr (6) - Härnösands sjukhus (062012) - Kir klin (301)",
                                    "PosId4": 107,
                                    "PosLevel": 3,
                                    "UnitCode": "301",
                                    "TopPosId": 1,
                                    "TopPosCode": "6"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):6 - 062012 - 101,Organisationsenhet(namn och kod):OC Norr - Härnösands sjukhus - Med klin",
                                "id": 411,
                                "data": {
                                    "PosId": 411,
                                    "PosCode": "6 - 062012 - 101",
                                    "PosName": "OC Norr - Härnösands sjukhus - Med klin",
                                    "PosNameWithCode": "OC Norr (6) - Härnösands sjukhus (062012) - Med klin (101)",
                                    "PosId4": 411,
                                    "PosLevel": 3,
                                    "UnitCode": "101",
                                    "TopPosId": 1,
                                    "TopPosCode": "6"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):6 - 062012 - 731,Organisationsenhet(namn och kod):OC Norr - Härnösands sjukhus - Röntgen",
                                "id": 1499,
                                "data": {
                                    "PosId": 1499,
                                    "PosCode": "6 - 062012 - 731",
                                    "PosName": "OC Norr - Härnösands sjukhus - Röntgen",
                                    "PosNameWithCode": "OC Norr (6) - Härnösands sjukhus (062012) - Röntgen (731)",
                                    "PosId4": 1499,
                                    "PosLevel": 3,
                                    "UnitCode": "731",
                                    "TopPosId": 1,
                                    "TopPosCode": "6"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):6 - 062012 - 521,Organisationsenhet(namn och kod):OC Norr - Härnösands sjukhus - ÖNH klin",
                                "id": 719,
                                "data": {
                                    "PosId": 719,
                                    "PosCode": "6 - 062012 - 521",
                                    "PosName": "OC Norr - Härnösands sjukhus - ÖNH klin",
                                    "PosNameWithCode": "OC Norr (6) - Härnösands sjukhus (062012) - ÖNH klin (521)",
                                    "PosId4": 719,
                                    "PosLevel": 3,
                                    "UnitCode": "521",
                                    "TopPosId": 1,
                                    "TopPosCode": "6"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):6 - 065014 - 301,Organisationsenhet(namn och kod):OC Norr - Kalix sjukhus - Kir klin",
                                "id": 101,
                                "data": {
                                    "PosId": 101,
                                    "PosCode": "6 - 065014 - 301",
                                    "PosName": "OC Norr - Kalix sjukhus - Kir klin",
                                    "PosNameWithCode": "OC Norr (6) - Kalix sjukhus (065014) - Kir klin (301)",
                                    "PosId4": 101,
                                    "PosLevel": 3,
                                    "UnitCode": "301",
                                    "TopPosId": 1,
                                    "TopPosCode": "6"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):6 - 065014 - 451,Organisationsenhet(namn och kod):OC Norr - Kalix sjukhus - Kvinnoklinik",
                                "id": 1031,
                                "data": {
                                    "PosId": 1031,
                                    "PosCode": "6 - 065014 - 451",
                                    "PosName": "OC Norr - Kalix sjukhus - Kvinnoklinik",
                                    "PosNameWithCode": "OC Norr (6) - Kalix sjukhus (065014) - Kvinnoklinik (451)",
                                    "PosId4": 1031,
                                    "PosLevel": 3,
                                    "UnitCode": "451",
                                    "TopPosId": 1,
                                    "TopPosCode": "6"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):6 - 065014 - 101,Organisationsenhet(namn och kod):OC Norr - Kalix sjukhus - Med klin",
                                "id": 418,
                                "data": {
                                    "PosId": 418,
                                    "PosCode": "6 - 065014 - 101",
                                    "PosName": "OC Norr - Kalix sjukhus - Med klin",
                                    "PosNameWithCode": "OC Norr (6) - Kalix sjukhus (065014) - Med klin (101)",
                                    "PosId4": 418,
                                    "PosLevel": 3,
                                    "UnitCode": "101",
                                    "TopPosId": 1,
                                    "TopPosCode": "6"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):6 - 065014 - 731,Organisationsenhet(namn och kod):OC Norr - Kalix sjukhus - Röntgen",
                                "id": 1500,
                                "data": {
                                    "PosId": 1500,
                                    "PosCode": "6 - 065014 - 731",
                                    "PosName": "OC Norr - Kalix sjukhus - Röntgen",
                                    "PosNameWithCode": "OC Norr (6) - Kalix sjukhus (065014) - Röntgen (731)",
                                    "PosId4": 1500,
                                    "PosLevel": 3,
                                    "UnitCode": "731",
                                    "TopPosId": 1,
                                    "TopPosCode": "6"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):6 - 065015 - 301,Organisationsenhet(namn och kod):OC Norr - Kiruna sjukhus - Kir klin",
                                "id": 105,
                                "data": {
                                    "PosId": 105,
                                    "PosCode": "6 - 065015 - 301",
                                    "PosName": "OC Norr - Kiruna sjukhus - Kir klin",
                                    "PosNameWithCode": "OC Norr (6) - Kiruna sjukhus (065015) - Kir klin (301)",
                                    "PosId4": 105,
                                    "PosLevel": 3,
                                    "UnitCode": "301",
                                    "TopPosId": 1,
                                    "TopPosCode": "6"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):6 - 065015 - 451,Organisationsenhet(namn och kod):OC Norr - Kiruna sjukhus - Kvinnoklinik",
                                "id": 1235,
                                "data": {
                                    "PosId": 1235,
                                    "PosCode": "6 - 065015 - 451",
                                    "PosName": "OC Norr - Kiruna sjukhus - Kvinnoklinik",
                                    "PosNameWithCode": "OC Norr (6) - Kiruna sjukhus (065015) - Kvinnoklinik (451)",
                                    "PosId4": 1235,
                                    "PosLevel": 3,
                                    "UnitCode": "451",
                                    "TopPosId": 1,
                                    "TopPosCode": "6"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):6 - 065015 - 101,Organisationsenhet(namn och kod):OC Norr - Kiruna sjukhus - Med klin",
                                "id": 419,
                                "data": {
                                    "PosId": 419,
                                    "PosCode": "6 - 065015 - 101",
                                    "PosName": "OC Norr - Kiruna sjukhus - Med klin",
                                    "PosNameWithCode": "OC Norr (6) - Kiruna sjukhus (065015) - Med klin (101)",
                                    "PosId4": 419,
                                    "PosLevel": 3,
                                    "UnitCode": "101",
                                    "TopPosId": 1,
                                    "TopPosCode": "6"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):6 - 065015 - 731,Organisationsenhet(namn och kod):OC Norr - Kiruna sjukhus - Röntgen",
                                "id": 1501,
                                "data": {
                                    "PosId": 1501,
                                    "PosCode": "6 - 065015 - 731",
                                    "PosName": "OC Norr - Kiruna sjukhus - Röntgen",
                                    "PosNameWithCode": "OC Norr (6) - Kiruna sjukhus (065015) - Röntgen (731)",
                                    "PosId4": 1501,
                                    "PosLevel": 3,
                                    "UnitCode": "731",
                                    "TopPosId": 1,
                                    "TopPosCode": "6"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):6 - 064011 - 241,Organisationsenhet(namn och kod):OC Norr - Lycksele lasarett - Geriatrisk klin",
                                "id": 800,
                                "data": {
                                    "PosId": 800,
                                    "PosCode": "6 - 064011 - 241",
                                    "PosName": "OC Norr - Lycksele lasarett - Geriatrisk klin",
                                    "PosNameWithCode": "OC Norr (6) - Lycksele lasarett (064011) - Geriatrisk klin (241)",
                                    "PosId4": 800,
                                    "PosLevel": 3,
                                    "UnitCode": "241",
                                    "TopPosId": 1,
                                    "TopPosCode": "6"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):6 - 064011 - 301,Organisationsenhet(namn och kod):OC Norr - Lycksele lasarett - Kir klin",
                                "id": 104,
                                "data": {
                                    "PosId": 104,
                                    "PosCode": "6 - 064011 - 301",
                                    "PosName": "OC Norr - Lycksele lasarett - Kir klin",
                                    "PosNameWithCode": "OC Norr (6) - Lycksele lasarett (064011) - Kir klin (301)",
                                    "PosId4": 104,
                                    "PosLevel": 3,
                                    "UnitCode": "301",
                                    "TopPosId": 1,
                                    "TopPosCode": "6"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):6 - 064011 - 451,Organisationsenhet(namn och kod):OC Norr - Lycksele lasarett - Kvinnoklinik",
                                "id": 946,
                                "data": {
                                    "PosId": 946,
                                    "PosCode": "6 - 064011 - 451",
                                    "PosName": "OC Norr - Lycksele lasarett - Kvinnoklinik",
                                    "PosNameWithCode": "OC Norr (6) - Lycksele lasarett (064011) - Kvinnoklinik (451)",
                                    "PosId4": 946,
                                    "PosLevel": 3,
                                    "UnitCode": "451",
                                    "TopPosId": 1,
                                    "TopPosCode": "6"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):6 - 064011 - 101,Organisationsenhet(namn och kod):OC Norr - Lycksele lasarett - Med klin",
                                "id": 415,
                                "data": {
                                    "PosId": 415,
                                    "PosCode": "6 - 064011 - 101",
                                    "PosName": "OC Norr - Lycksele lasarett - Med klin",
                                    "PosNameWithCode": "OC Norr (6) - Lycksele lasarett (064011) - Med klin (101)",
                                    "PosId4": 415,
                                    "PosLevel": 3,
                                    "UnitCode": "101",
                                    "TopPosId": 1,
                                    "TopPosCode": "6"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):6 - 064011 - 731,Organisationsenhet(namn och kod):OC Norr - Lycksele lasarett - Röntgen",
                                "id": 1502,
                                "data": {
                                    "PosId": 1502,
                                    "PosCode": "6 - 064011 - 731",
                                    "PosName": "OC Norr - Lycksele lasarett - Röntgen",
                                    "PosNameWithCode": "OC Norr (6) - Lycksele lasarett (064011) - Röntgen (731)",
                                    "PosId4": 1502,
                                    "PosLevel": 3,
                                    "UnitCode": "731",
                                    "TopPosId": 1,
                                    "TopPosCode": "6"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):6 - 064001 - 201,Organisationsenhet(namn och kod):OC Norr - NUS Umeå - Barnmedicin klin",
                                "id": 1421,
                                "data": {
                                    "PosId": 1421,
                                    "PosCode": "6 - 064001 - 201",
                                    "PosName": "OC Norr - NUS Umeå - Barnmedicin klin",
                                    "PosNameWithCode": "OC Norr (6) - NUS Umeå (064001) - Barnmedicin klin (201)",
                                    "PosId4": 1421,
                                    "PosLevel": 3,
                                    "UnitCode": "201",
                                    "TopPosId": 1,
                                    "TopPosCode": "6"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):6 - 064001 - 241,Organisationsenhet(namn och kod):OC Norr - NUS Umeå - Geriatrisk klin",
                                "id": 743,
                                "data": {
                                    "PosId": 743,
                                    "PosCode": "6 - 064001 - 241",
                                    "PosName": "OC Norr - NUS Umeå - Geriatrisk klin",
                                    "PosNameWithCode": "OC Norr (6) - NUS Umeå (064001) - Geriatrisk klin (241)",
                                    "PosId4": 743,
                                    "PosLevel": 3,
                                    "UnitCode": "241",
                                    "TopPosId": 1,
                                    "TopPosCode": "6"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):6 - 064001 - 211,Organisationsenhet(namn och kod):OC Norr - NUS Umeå - Hud klin",
                                "id": 874,
                                "data": {
                                    "PosId": 874,
                                    "PosCode": "6 - 064001 - 211",
                                    "PosName": "OC Norr - NUS Umeå - Hud klin",
                                    "PosNameWithCode": "OC Norr (6) - NUS Umeå (064001) - Hud klin (211)",
                                    "PosId4": 874,
                                    "PosLevel": 3,
                                    "UnitCode": "211",
                                    "TopPosId": 1,
                                    "TopPosCode": "6"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):6 - 064001 - 301,Organisationsenhet(namn och kod):OC Norr - NUS Umeå - Kir klin",
                                "id": 24,
                                "data": {
                                    "PosId": 24,
                                    "PosCode": "6 - 064001 - 301",
                                    "PosName": "OC Norr - NUS Umeå - Kir klin",
                                    "PosNameWithCode": "OC Norr (6) - NUS Umeå (064001) - Kir klin (301)",
                                    "PosId4": 24,
                                    "PosLevel": 3,
                                    "UnitCode": "301",
                                    "TopPosId": 1,
                                    "TopPosCode": "6"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):6 - 064001 - 451,Organisationsenhet(namn och kod):OC Norr - NUS Umeå - Kvinnoklinik",
                                "id": 941,
                                "data": {
                                    "PosId": 941,
                                    "PosCode": "6 - 064001 - 451",
                                    "PosName": "OC Norr - NUS Umeå - Kvinnoklinik",
                                    "PosNameWithCode": "OC Norr (6) - NUS Umeå (064001) - Kvinnoklinik (451)",
                                    "PosId4": 941,
                                    "PosLevel": 3,
                                    "UnitCode": "451",
                                    "TopPosId": 1,
                                    "TopPosCode": "6"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):6 - 064001 - 111,Organisationsenhet(namn och kod):OC Norr - NUS Umeå - Lung klin",
                                "id": 711,
                                "data": {
                                    "PosId": 711,
                                    "PosCode": "6 - 064001 - 111",
                                    "PosName": "OC Norr - NUS Umeå - Lung klin",
                                    "PosNameWithCode": "OC Norr (6) - NUS Umeå (064001) - Lung klin (111)",
                                    "PosId4": 711,
                                    "PosLevel": 3,
                                    "UnitCode": "111",
                                    "TopPosId": 1,
                                    "TopPosCode": "6"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):6 - 064001 - 101,Organisationsenhet(namn och kod):OC Norr - NUS Umeå - Med klin",
                                "id": 12,
                                "data": {
                                    "PosId": 12,
                                    "PosCode": "6 - 064001 - 101",
                                    "PosName": "OC Norr - NUS Umeå - Med klin",
                                    "PosNameWithCode": "OC Norr (6) - NUS Umeå (064001) - Med klin (101)",
                                    "PosId4": 12,
                                    "PosLevel": 3,
                                    "UnitCode": "101",
                                    "TopPosId": 1,
                                    "TopPosCode": "6"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):6 - 064001 - 331,Organisationsenhet(namn och kod):OC Norr - NUS Umeå - Neurokir klin",
                                "id": 807,
                                "data": {
                                    "PosId": 807,
                                    "PosCode": "6 - 064001 - 331",
                                    "PosName": "OC Norr - NUS Umeå - Neurokir klin",
                                    "PosNameWithCode": "OC Norr (6) - NUS Umeå (064001) - Neurokir klin (331)",
                                    "PosId4": 807,
                                    "PosLevel": 3,
                                    "UnitCode": "331",
                                    "TopPosId": 1,
                                    "TopPosCode": "6"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):6 - 064001 - 741,Organisationsenhet(namn och kod):OC Norr - NUS Umeå - Onk klin",
                                "id": 430,
                                "data": {
                                    "PosId": 430,
                                    "PosCode": "6 - 064001 - 741",
                                    "PosName": "OC Norr - NUS Umeå - Onk klin",
                                    "PosNameWithCode": "OC Norr (6) - NUS Umeå (064001) - Onk klin (741)",
                                    "PosId4": 430,
                                    "PosLevel": 3,
                                    "UnitCode": "741",
                                    "TopPosId": 1,
                                    "TopPosCode": "6"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):6 - 064001 - 311,Organisationsenhet(namn och kod):OC Norr - NUS Umeå - Ortoped klin",
                                "id": 914,
                                "data": {
                                    "PosId": 914,
                                    "PosCode": "6 - 064001 - 311",
                                    "PosName": "OC Norr - NUS Umeå - Ortoped klin",
                                    "PosNameWithCode": "OC Norr (6) - NUS Umeå (064001) - Ortoped klin (311)",
                                    "PosId4": 914,
                                    "PosLevel": 3,
                                    "UnitCode": "311",
                                    "TopPosId": 1,
                                    "TopPosCode": "6"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):6 - 064001 - 351,Organisationsenhet(namn och kod):OC Norr - NUS Umeå - Plastikkir klin",
                                "id": 909,
                                "data": {
                                    "PosId": 909,
                                    "PosCode": "6 - 064001 - 351",
                                    "PosName": "OC Norr - NUS Umeå - Plastikkir klin",
                                    "PosNameWithCode": "OC Norr (6) - NUS Umeå (064001) - Plastikkir klin (351)",
                                    "PosId4": 909,
                                    "PosLevel": 3,
                                    "UnitCode": "351",
                                    "TopPosId": 1,
                                    "TopPosCode": "6"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):6 - 064001 - 731,Organisationsenhet(namn och kod):OC Norr - NUS Umeå - Röntgen",
                                "id": 1503,
                                "data": {
                                    "PosId": 1503,
                                    "PosCode": "6 - 064001 - 731",
                                    "PosName": "OC Norr - NUS Umeå - Röntgen",
                                    "PosNameWithCode": "OC Norr (6) - NUS Umeå (064001) - Röntgen (731)",
                                    "PosId4": 1503,
                                    "PosLevel": 3,
                                    "UnitCode": "731",
                                    "TopPosId": 1,
                                    "TopPosCode": "6"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):6 - 064001 - 341,Organisationsenhet(namn och kod):OC Norr - NUS Umeå - Thorax klin",
                                "id": 982,
                                "data": {
                                    "PosId": 982,
                                    "PosCode": "6 - 064001 - 341",
                                    "PosName": "OC Norr - NUS Umeå - Thorax klin",
                                    "PosNameWithCode": "OC Norr (6) - NUS Umeå (064001) - Thorax klin (341)",
                                    "PosId4": 982,
                                    "PosLevel": 3,
                                    "UnitCode": "341",
                                    "TopPosId": 1,
                                    "TopPosCode": "6"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):6 - 064001 - 361,Organisationsenhet(namn och kod):OC Norr - NUS Umeå - Urol klin",
                                "id": 81,
                                "data": {
                                    "PosId": 81,
                                    "PosCode": "6 - 064001 - 361",
                                    "PosName": "OC Norr - NUS Umeå - Urol klin",
                                    "PosNameWithCode": "OC Norr (6) - NUS Umeå (064001) - Urol klin (361)",
                                    "PosId4": 81,
                                    "PosLevel": 3,
                                    "UnitCode": "361",
                                    "TopPosId": 1,
                                    "TopPosCode": "6"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):6 - 064001 - 511,Organisationsenhet(namn och kod):OC Norr - NUS Umeå - Ögon klin",
                                "id": 1160,
                                "data": {
                                    "PosId": 1160,
                                    "PosCode": "6 - 064001 - 511",
                                    "PosName": "OC Norr - NUS Umeå - Ögon klin",
                                    "PosNameWithCode": "OC Norr (6) - NUS Umeå (064001) - Ögon klin (511)",
                                    "PosId4": 1160,
                                    "PosLevel": 3,
                                    "UnitCode": "511",
                                    "TopPosId": 1,
                                    "TopPosCode": "6"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):6 - 064001 - 521,Organisationsenhet(namn och kod):OC Norr - NUS Umeå - ÖNH klin",
                                "id": 455,
                                "data": {
                                    "PosId": 455,
                                    "PosCode": "6 - 064001 - 521",
                                    "PosName": "OC Norr - NUS Umeå - ÖNH klin",
                                    "PosNameWithCode": "OC Norr (6) - NUS Umeå (064001) - ÖNH klin (521)",
                                    "PosId4": 455,
                                    "PosLevel": 3,
                                    "UnitCode": "521",
                                    "TopPosId": 1,
                                    "TopPosCode": "6"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):6 - 065013 - 301,Organisationsenhet(namn och kod):OC Norr - Piteå älvdals sjukhus - Kir klin",
                                "id": 95,
                                "data": {
                                    "PosId": 95,
                                    "PosCode": "6 - 065013 - 301",
                                    "PosName": "OC Norr - Piteå älvdals sjukhus - Kir klin",
                                    "PosNameWithCode": "OC Norr (6) - Piteå älvdals sjukhus (065013) - Kir klin (301)",
                                    "PosId4": 95,
                                    "PosLevel": 3,
                                    "UnitCode": "301",
                                    "TopPosId": 1,
                                    "TopPosCode": "6"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):6 - 065013 - 451,Organisationsenhet(namn och kod):OC Norr - Piteå älvdals sjukhus - Kvinnoklinik",
                                "id": 958,
                                "data": {
                                    "PosId": 958,
                                    "PosCode": "6 - 065013 - 451",
                                    "PosName": "OC Norr - Piteå älvdals sjukhus - Kvinnoklinik",
                                    "PosNameWithCode": "OC Norr (6) - Piteå älvdals sjukhus (065013) - Kvinnoklinik (451)",
                                    "PosId4": 958,
                                    "PosLevel": 3,
                                    "UnitCode": "451",
                                    "TopPosId": 1,
                                    "TopPosCode": "6"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):6 - 065013 - 101,Organisationsenhet(namn och kod):OC Norr - Piteå älvdals sjukhus - Med klin",
                                "id": 417,
                                "data": {
                                    "PosId": 417,
                                    "PosCode": "6 - 065013 - 101",
                                    "PosName": "OC Norr - Piteå älvdals sjukhus - Med klin",
                                    "PosNameWithCode": "OC Norr (6) - Piteå älvdals sjukhus (065013) - Med klin (101)",
                                    "PosId4": 417,
                                    "PosLevel": 3,
                                    "UnitCode": "101",
                                    "TopPosId": 1,
                                    "TopPosCode": "6"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):6 - 065013 - 731,Organisationsenhet(namn och kod):OC Norr - Piteå älvdals sjukhus - Röntgen",
                                "id": 1504,
                                "data": {
                                    "PosId": 1504,
                                    "PosCode": "6 - 065013 - 731",
                                    "PosName": "OC Norr - Piteå älvdals sjukhus - Röntgen",
                                    "PosNameWithCode": "OC Norr (6) - Piteå älvdals sjukhus (065013) - Röntgen (731)",
                                    "PosId4": 1504,
                                    "PosLevel": 3,
                                    "UnitCode": "731",
                                    "TopPosId": 1,
                                    "TopPosCode": "6"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):6 - 065013 - 521,Organisationsenhet(namn och kod):OC Norr - Piteå älvdals sjukhus - ÖNH klin",
                                "id": 721,
                                "data": {
                                    "PosId": 721,
                                    "PosCode": "6 - 065013 - 521",
                                    "PosName": "OC Norr - Piteå älvdals sjukhus - ÖNH klin",
                                    "PosNameWithCode": "OC Norr (6) - Piteå älvdals sjukhus (065013) - ÖNH klin (521)",
                                    "PosId4": 721,
                                    "PosLevel": 3,
                                    "UnitCode": "521",
                                    "TopPosId": 1,
                                    "TopPosCode": "6"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):6 - 063199 - 999,Organisationsenhet(namn och kod):OC Norr - Privatläkare Jämtland - Klinik saknas",
                                "id": 935,
                                "data": {
                                    "PosId": 935,
                                    "PosCode": "6 - 063199 - 999",
                                    "PosName": "OC Norr - Privatläkare Jämtland - Klinik saknas",
                                    "PosNameWithCode": "OC Norr (6) - Privatläkare Jämtland (063199) - Klinik saknas (999)",
                                    "PosId4": 935,
                                    "PosLevel": 3,
                                    "UnitCode": "999",
                                    "TopPosId": 1,
                                    "TopPosCode": "6"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):6 - 065199 - 999,Organisationsenhet(namn och kod):OC Norr - Privatläkare Norrbotten - Klinik saknas",
                                "id": 931,
                                "data": {
                                    "PosId": 931,
                                    "PosCode": "6 - 065199 - 999",
                                    "PosName": "OC Norr - Privatläkare Norrbotten - Klinik saknas",
                                    "PosNameWithCode": "OC Norr (6) - Privatläkare Norrbotten (065199) - Klinik saknas (999)",
                                    "PosId4": 931,
                                    "PosLevel": 3,
                                    "UnitCode": "999",
                                    "TopPosId": 1,
                                    "TopPosCode": "6"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):6 - 064199 - 999,Organisationsenhet(namn och kod):OC Norr - Privatläkare Västerbotten - Klinik saknas",
                                "id": 933,
                                "data": {
                                    "PosId": 933,
                                    "PosCode": "6 - 064199 - 999",
                                    "PosName": "OC Norr - Privatläkare Västerbotten - Klinik saknas",
                                    "PosNameWithCode": "OC Norr (6) - Privatläkare Västerbotten (064199) - Klinik saknas (999)",
                                    "PosId4": 933,
                                    "PosLevel": 3,
                                    "UnitCode": "999",
                                    "TopPosId": 1,
                                    "TopPosCode": "6"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):6 - 062199 - 999,Organisationsenhet(namn och kod):OC Norr - Privatläkare Västernorrland - Klinik saknas",
                                "id": 937,
                                "data": {
                                    "PosId": 937,
                                    "PosCode": "6 - 062199 - 999",
                                    "PosName": "OC Norr - Privatläkare Västernorrland - Klinik saknas",
                                    "PosNameWithCode": "OC Norr (6) - Privatläkare Västernorrland (062199) - Klinik saknas (999)",
                                    "PosId4": 937,
                                    "PosLevel": 3,
                                    "UnitCode": "999",
                                    "TopPosId": 1,
                                    "TopPosCode": "6"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):6 - 064010 - 301,Organisationsenhet(namn och kod):OC Norr - Skellefteå lasarett - Kir klin",
                                "id": 103,
                                "data": {
                                    "PosId": 103,
                                    "PosCode": "6 - 064010 - 301",
                                    "PosName": "OC Norr - Skellefteå lasarett - Kir klin",
                                    "PosNameWithCode": "OC Norr (6) - Skellefteå lasarett (064010) - Kir klin (301)",
                                    "PosId4": 103,
                                    "PosLevel": 3,
                                    "UnitCode": "301",
                                    "TopPosId": 1,
                                    "TopPosCode": "6"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):6 - 064010 - 451,Organisationsenhet(namn och kod):OC Norr - Skellefteå lasarett - Kvinnoklinik",
                                "id": 945,
                                "data": {
                                    "PosId": 945,
                                    "PosCode": "6 - 064010 - 451",
                                    "PosName": "OC Norr - Skellefteå lasarett - Kvinnoklinik",
                                    "PosNameWithCode": "OC Norr (6) - Skellefteå lasarett (064010) - Kvinnoklinik (451)",
                                    "PosId4": 945,
                                    "PosLevel": 3,
                                    "UnitCode": "451",
                                    "TopPosId": 1,
                                    "TopPosCode": "6"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):6 - 064010 - 101,Organisationsenhet(namn och kod):OC Norr - Skellefteå lasarett - Med klin",
                                "id": 414,
                                "data": {
                                    "PosId": 414,
                                    "PosCode": "6 - 064010 - 101",
                                    "PosName": "OC Norr - Skellefteå lasarett - Med klin",
                                    "PosNameWithCode": "OC Norr (6) - Skellefteå lasarett (064010) - Med klin (101)",
                                    "PosId4": 414,
                                    "PosLevel": 3,
                                    "UnitCode": "101",
                                    "TopPosId": 1,
                                    "TopPosCode": "6"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):6 - 064010 - 731,Organisationsenhet(namn och kod):OC Norr - Skellefteå lasarett - Röntgen",
                                "id": 1505,
                                "data": {
                                    "PosId": 1505,
                                    "PosCode": "6 - 064010 - 731",
                                    "PosName": "OC Norr - Skellefteå lasarett - Röntgen",
                                    "PosNameWithCode": "OC Norr (6) - Skellefteå lasarett (064010) - Röntgen (731)",
                                    "PosId4": 1505,
                                    "PosLevel": 3,
                                    "UnitCode": "731",
                                    "TopPosId": 1,
                                    "TopPosCode": "6"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):6 - 064010 - 511,Organisationsenhet(namn och kod):OC Norr - Skellefteå lasarett - Ögon klin",
                                "id": 1159,
                                "data": {
                                    "PosId": 1159,
                                    "PosCode": "6 - 064010 - 511",
                                    "PosName": "OC Norr - Skellefteå lasarett - Ögon klin",
                                    "PosNameWithCode": "OC Norr (6) - Skellefteå lasarett (064010) - Ögon klin (511)",
                                    "PosId4": 1159,
                                    "PosLevel": 3,
                                    "UnitCode": "511",
                                    "TopPosId": 1,
                                    "TopPosCode": "6"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):6 - 064010 - 521,Organisationsenhet(namn och kod):OC Norr - Skellefteå lasarett - ÖNH klin",
                                "id": 938,
                                "data": {
                                    "PosId": 938,
                                    "PosCode": "6 - 064010 - 521",
                                    "PosName": "OC Norr - Skellefteå lasarett - ÖNH klin",
                                    "PosNameWithCode": "OC Norr (6) - Skellefteå lasarett (064010) - ÖNH klin (521)",
                                    "PosId4": 938,
                                    "PosLevel": 3,
                                    "UnitCode": "521",
                                    "TopPosId": 1,
                                    "TopPosCode": "6"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):6 - 062013 - 301,Organisationsenhet(namn och kod):OC Norr - Sollefteå sjukhus - Kir klin",
                                "id": 94,
                                "data": {
                                    "PosId": 94,
                                    "PosCode": "6 - 062013 - 301",
                                    "PosName": "OC Norr - Sollefteå sjukhus - Kir klin",
                                    "PosNameWithCode": "OC Norr (6) - Sollefteå sjukhus (062013) - Kir klin (301)",
                                    "PosId4": 94,
                                    "PosLevel": 3,
                                    "UnitCode": "301",
                                    "TopPosId": 1,
                                    "TopPosCode": "6"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):6 - 062013 - 451,Organisationsenhet(namn och kod):OC Norr - Sollefteå sjukhus - Kvinnoklinik",
                                "id": 943,
                                "data": {
                                    "PosId": 943,
                                    "PosCode": "6 - 062013 - 451",
                                    "PosName": "OC Norr - Sollefteå sjukhus - Kvinnoklinik",
                                    "PosNameWithCode": "OC Norr (6) - Sollefteå sjukhus (062013) - Kvinnoklinik (451)",
                                    "PosId4": 943,
                                    "PosLevel": 3,
                                    "UnitCode": "451",
                                    "TopPosId": 1,
                                    "TopPosCode": "6"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):6 - 062013 - 101,Organisationsenhet(namn och kod):OC Norr - Sollefteå sjukhus - Med klin",
                                "id": 412,
                                "data": {
                                    "PosId": 412,
                                    "PosCode": "6 - 062013 - 101",
                                    "PosName": "OC Norr - Sollefteå sjukhus - Med klin",
                                    "PosNameWithCode": "OC Norr (6) - Sollefteå sjukhus (062013) - Med klin (101)",
                                    "PosId4": 412,
                                    "PosLevel": 3,
                                    "UnitCode": "101",
                                    "TopPosId": 1,
                                    "TopPosCode": "6"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):6 - 062013 - 731,Organisationsenhet(namn och kod):OC Norr - Sollefteå sjukhus - Röntgen",
                                "id": 1506,
                                "data": {
                                    "PosId": 1506,
                                    "PosCode": "6 - 062013 - 731",
                                    "PosName": "OC Norr - Sollefteå sjukhus - Röntgen",
                                    "PosNameWithCode": "OC Norr (6) - Sollefteå sjukhus (062013) - Röntgen (731)",
                                    "PosId4": 1506,
                                    "PosLevel": 3,
                                    "UnitCode": "731",
                                    "TopPosId": 1,
                                    "TopPosCode": "6"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):6 - 062013 - 521,Organisationsenhet(namn och kod):OC Norr - Sollefteå sjukhus - ÖNH klin",
                                "id": 939,
                                "data": {
                                    "PosId": 939,
                                    "PosCode": "6 - 062013 - 521",
                                    "PosName": "OC Norr - Sollefteå sjukhus - ÖNH klin",
                                    "PosNameWithCode": "OC Norr (6) - Sollefteå sjukhus (062013) - ÖNH klin (521)",
                                    "PosId4": 939,
                                    "PosLevel": 3,
                                    "UnitCode": "521",
                                    "TopPosId": 1,
                                    "TopPosCode": "6"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):6 - 065016 - 241,Organisationsenhet(namn och kod):OC Norr - Sunderby sjukhus - Geriatrisk klin",
                                "id": 912,
                                "data": {
                                    "PosId": 912,
                                    "PosCode": "6 - 065016 - 241",
                                    "PosName": "OC Norr - Sunderby sjukhus - Geriatrisk klin",
                                    "PosNameWithCode": "OC Norr (6) - Sunderby sjukhus (065016) - Geriatrisk klin (241)",
                                    "PosId4": 912,
                                    "PosLevel": 3,
                                    "UnitCode": "241",
                                    "TopPosId": 1,
                                    "TopPosCode": "6"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):6 - 065016 - 211,Organisationsenhet(namn och kod):OC Norr - Sunderby sjukhus - Hud klin",
                                "id": 911,
                                "data": {
                                    "PosId": 911,
                                    "PosCode": "6 - 065016 - 211",
                                    "PosName": "OC Norr - Sunderby sjukhus - Hud klin",
                                    "PosNameWithCode": "OC Norr (6) - Sunderby sjukhus (065016) - Hud klin (211)",
                                    "PosId4": 911,
                                    "PosLevel": 3,
                                    "UnitCode": "211",
                                    "TopPosId": 1,
                                    "TopPosCode": "6"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):6 - 065016 - 301,Organisationsenhet(namn och kod):OC Norr - Sunderby sjukhus - Kir klin",
                                "id": 98,
                                "data": {
                                    "PosId": 98,
                                    "PosCode": "6 - 065016 - 301",
                                    "PosName": "OC Norr - Sunderby sjukhus - Kir klin",
                                    "PosNameWithCode": "OC Norr (6) - Sunderby sjukhus (065016) - Kir klin (301)",
                                    "PosId4": 98,
                                    "PosLevel": 3,
                                    "UnitCode": "301",
                                    "TopPosId": 1,
                                    "TopPosCode": "6"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):6 - 065016 - 451,Organisationsenhet(namn och kod):OC Norr - Sunderby sjukhus - Kvinnoklinik",
                                "id": 948,
                                "data": {
                                    "PosId": 948,
                                    "PosCode": "6 - 065016 - 451",
                                    "PosName": "OC Norr - Sunderby sjukhus - Kvinnoklinik",
                                    "PosNameWithCode": "OC Norr (6) - Sunderby sjukhus (065016) - Kvinnoklinik (451)",
                                    "PosId4": 948,
                                    "PosLevel": 3,
                                    "UnitCode": "451",
                                    "TopPosId": 1,
                                    "TopPosCode": "6"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):6 - 065016 - 111,Organisationsenhet(namn och kod):OC Norr - Sunderby sjukhus - Lung klin",
                                "id": 714,
                                "data": {
                                    "PosId": 714,
                                    "PosCode": "6 - 065016 - 111",
                                    "PosName": "OC Norr - Sunderby sjukhus - Lung klin",
                                    "PosNameWithCode": "OC Norr (6) - Sunderby sjukhus (065016) - Lung klin (111)",
                                    "PosId4": 714,
                                    "PosLevel": 3,
                                    "UnitCode": "111",
                                    "TopPosId": 1,
                                    "TopPosCode": "6"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):6 - 065016 - 101,Organisationsenhet(namn och kod):OC Norr - Sunderby sjukhus - Med klin",
                                "id": 420,
                                "data": {
                                    "PosId": 420,
                                    "PosCode": "6 - 065016 - 101",
                                    "PosName": "OC Norr - Sunderby sjukhus - Med klin",
                                    "PosNameWithCode": "OC Norr (6) - Sunderby sjukhus (065016) - Med klin (101)",
                                    "PosId4": 420,
                                    "PosLevel": 3,
                                    "UnitCode": "101",
                                    "TopPosId": 1,
                                    "TopPosCode": "6"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):6 - 065016 - 731,Organisationsenhet(namn och kod):OC Norr - Sunderby sjukhus - Röntgen",
                                "id": 1507,
                                "data": {
                                    "PosId": 1507,
                                    "PosCode": "6 - 065016 - 731",
                                    "PosName": "OC Norr - Sunderby sjukhus - Röntgen",
                                    "PosNameWithCode": "OC Norr (6) - Sunderby sjukhus (065016) - Röntgen (731)",
                                    "PosId4": 1507,
                                    "PosLevel": 3,
                                    "UnitCode": "731",
                                    "TopPosId": 1,
                                    "TopPosCode": "6"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):6 - 065016 - 361,Organisationsenhet(namn och kod):OC Norr - Sunderby sjukhus - Urol klin",
                                "id": 99,
                                "data": {
                                    "PosId": 99,
                                    "PosCode": "6 - 065016 - 361",
                                    "PosName": "OC Norr - Sunderby sjukhus - Urol klin",
                                    "PosNameWithCode": "OC Norr (6) - Sunderby sjukhus (065016) - Urol klin (361)",
                                    "PosId4": 99,
                                    "PosLevel": 3,
                                    "UnitCode": "361",
                                    "TopPosId": 1,
                                    "TopPosCode": "6"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):6 - 065016 - 521,Organisationsenhet(namn och kod):OC Norr - Sunderby sjukhus - ÖNH klin",
                                "id": 458,
                                "data": {
                                    "PosId": 458,
                                    "PosCode": "6 - 065016 - 521",
                                    "PosName": "OC Norr - Sunderby sjukhus - ÖNH klin",
                                    "PosNameWithCode": "OC Norr (6) - Sunderby sjukhus (065016) - ÖNH klin (521)",
                                    "PosId4": 458,
                                    "PosLevel": 3,
                                    "UnitCode": "521",
                                    "TopPosId": 1,
                                    "TopPosCode": "6"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):6 - 062010 - 241,Organisationsenhet(namn och kod):OC Norr - Sundsvalls sjukhus - Geriatrisk klin",
                                "id": 913,
                                "data": {
                                    "PosId": 913,
                                    "PosCode": "6 - 062010 - 241",
                                    "PosName": "OC Norr - Sundsvalls sjukhus - Geriatrisk klin",
                                    "PosNameWithCode": "OC Norr (6) - Sundsvalls sjukhus (062010) - Geriatrisk klin (241)",
                                    "PosId4": 913,
                                    "PosLevel": 3,
                                    "UnitCode": "241",
                                    "TopPosId": 1,
                                    "TopPosCode": "6"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):6 - 062010 - 211,Organisationsenhet(namn och kod):OC Norr - Sundsvalls sjukhus - Hud klin",
                                "id": 895,
                                "data": {
                                    "PosId": 895,
                                    "PosCode": "6 - 062010 - 211",
                                    "PosName": "OC Norr - Sundsvalls sjukhus - Hud klin",
                                    "PosNameWithCode": "OC Norr (6) - Sundsvalls sjukhus (062010) - Hud klin (211)",
                                    "PosId4": 895,
                                    "PosLevel": 3,
                                    "UnitCode": "211",
                                    "TopPosId": 1,
                                    "TopPosCode": "6"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):6 - 062010 - 301,Organisationsenhet(namn och kod):OC Norr - Sundsvalls sjukhus - Kir klin",
                                "id": 96,
                                "data": {
                                    "PosId": 96,
                                    "PosCode": "6 - 062010 - 301",
                                    "PosName": "OC Norr - Sundsvalls sjukhus - Kir klin",
                                    "PosNameWithCode": "OC Norr (6) - Sundsvalls sjukhus (062010) - Kir klin (301)",
                                    "PosId4": 96,
                                    "PosLevel": 3,
                                    "UnitCode": "301",
                                    "TopPosId": 1,
                                    "TopPosCode": "6"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):6 - 062010 - 451,Organisationsenhet(namn och kod):OC Norr - Sundsvalls sjukhus - Kvinnoklinik",
                                "id": 940,
                                "data": {
                                    "PosId": 940,
                                    "PosCode": "6 - 062010 - 451",
                                    "PosName": "OC Norr - Sundsvalls sjukhus - Kvinnoklinik",
                                    "PosNameWithCode": "OC Norr (6) - Sundsvalls sjukhus (062010) - Kvinnoklinik (451)",
                                    "PosId4": 940,
                                    "PosLevel": 3,
                                    "UnitCode": "451",
                                    "TopPosId": 1,
                                    "TopPosCode": "6"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):6 - 062010 - 111,Organisationsenhet(namn och kod):OC Norr - Sundsvalls sjukhus - Lung klin",
                                "id": 712,
                                "data": {
                                    "PosId": 712,
                                    "PosCode": "6 - 062010 - 111",
                                    "PosName": "OC Norr - Sundsvalls sjukhus - Lung klin",
                                    "PosNameWithCode": "OC Norr (6) - Sundsvalls sjukhus (062010) - Lung klin (111)",
                                    "PosId4": 712,
                                    "PosLevel": 3,
                                    "UnitCode": "111",
                                    "TopPosId": 1,
                                    "TopPosCode": "6"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):6 - 062010 - 101,Organisationsenhet(namn och kod):OC Norr - Sundsvalls sjukhus - Med klin",
                                "id": 409,
                                "data": {
                                    "PosId": 409,
                                    "PosCode": "6 - 062010 - 101",
                                    "PosName": "OC Norr - Sundsvalls sjukhus - Med klin",
                                    "PosNameWithCode": "OC Norr (6) - Sundsvalls sjukhus (062010) - Med klin (101)",
                                    "PosId4": 409,
                                    "PosLevel": 3,
                                    "UnitCode": "101",
                                    "TopPosId": 1,
                                    "TopPosCode": "6"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):6 - 062010 - 221,Organisationsenhet(namn och kod):OC Norr - Sundsvalls sjukhus - Neurol klin",
                                "id": 1386,
                                "data": {
                                    "PosId": 1386,
                                    "PosCode": "6 - 062010 - 221",
                                    "PosName": "OC Norr - Sundsvalls sjukhus - Neurol klin",
                                    "PosNameWithCode": "OC Norr (6) - Sundsvalls sjukhus (062010) - Neurol klin (221)",
                                    "PosId4": 1386,
                                    "PosLevel": 3,
                                    "UnitCode": "221",
                                    "TopPosId": 1,
                                    "TopPosCode": "6"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):6 - 062010 - 552,Organisationsenhet(namn och kod):OC Norr - Sundsvalls sjukhus - Neurologisk rehab",
                                "id": 1396,
                                "data": {
                                    "PosId": 1396,
                                    "PosCode": "6 - 062010 - 552",
                                    "PosName": "OC Norr - Sundsvalls sjukhus - Neurologisk rehab",
                                    "PosNameWithCode": "OC Norr (6) - Sundsvalls sjukhus (062010) - Neurologisk rehab (552)",
                                    "PosId4": 1396,
                                    "PosLevel": 3,
                                    "UnitCode": "552",
                                    "TopPosId": 1,
                                    "TopPosCode": "6"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):6 - 062010 - 741,Organisationsenhet(namn och kod):OC Norr - Sundsvalls sjukhus - Onk klin",
                                "id": 445,
                                "data": {
                                    "PosId": 445,
                                    "PosCode": "6 - 062010 - 741",
                                    "PosName": "OC Norr - Sundsvalls sjukhus - Onk klin",
                                    "PosNameWithCode": "OC Norr (6) - Sundsvalls sjukhus (062010) - Onk klin (741)",
                                    "PosId4": 445,
                                    "PosLevel": 3,
                                    "UnitCode": "741",
                                    "TopPosId": 1,
                                    "TopPosCode": "6"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):6 - 062010 - 731,Organisationsenhet(namn och kod):OC Norr - Sundsvalls sjukhus - Röntgen",
                                "id": 1508,
                                "data": {
                                    "PosId": 1508,
                                    "PosCode": "6 - 062010 - 731",
                                    "PosName": "OC Norr - Sundsvalls sjukhus - Röntgen",
                                    "PosNameWithCode": "OC Norr (6) - Sundsvalls sjukhus (062010) - Röntgen (731)",
                                    "PosId4": 1508,
                                    "PosLevel": 3,
                                    "UnitCode": "731",
                                    "TopPosId": 1,
                                    "TopPosCode": "6"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):6 - 062010 - 361,Organisationsenhet(namn och kod):OC Norr - Sundsvalls sjukhus - Urol klin",
                                "id": 97,
                                "data": {
                                    "PosId": 97,
                                    "PosCode": "6 - 062010 - 361",
                                    "PosName": "OC Norr - Sundsvalls sjukhus - Urol klin",
                                    "PosNameWithCode": "OC Norr (6) - Sundsvalls sjukhus (062010) - Urol klin (361)",
                                    "PosId4": 97,
                                    "PosLevel": 3,
                                    "UnitCode": "361",
                                    "TopPosId": 1,
                                    "TopPosCode": "6"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):6 - 062010 - 521,Organisationsenhet(namn och kod):OC Norr - Sundsvalls sjukhus - ÖNH klin",
                                "id": 454,
                                "data": {
                                    "PosId": 454,
                                    "PosCode": "6 - 062010 - 521",
                                    "PosName": "OC Norr - Sundsvalls sjukhus - ÖNH klin",
                                    "PosNameWithCode": "OC Norr (6) - Sundsvalls sjukhus (062010) - ÖNH klin (521)",
                                    "PosId4": 454,
                                    "PosLevel": 3,
                                    "UnitCode": "521",
                                    "TopPosId": 1,
                                    "TopPosCode": "6"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):6 - 600099 - 999,Organisationsenhet(namn och kod):OC Norr - Vårdcentral norra regionen - Klinik saknas",
                                "id": 926,
                                "data": {
                                    "PosId": 926,
                                    "PosCode": "6 - 600099 - 999",
                                    "PosName": "OC Norr - Vårdcentral norra regionen - Klinik saknas",
                                    "PosNameWithCode": "OC Norr (6) - Vårdcentral norra regionen (600099) - Klinik saknas (999)",
                                    "PosId4": 926,
                                    "PosLevel": 3,
                                    "UnitCode": "999",
                                    "TopPosId": 1,
                                    "TopPosCode": "6"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):6 - 063010 - 211,Organisationsenhet(namn och kod):OC Norr - Östersunds sjukhus - Hud klin",
                                "id": 910,
                                "data": {
                                    "PosId": 910,
                                    "PosCode": "6 - 063010 - 211",
                                    "PosName": "OC Norr - Östersunds sjukhus - Hud klin",
                                    "PosNameWithCode": "OC Norr (6) - Östersunds sjukhus (063010) - Hud klin (211)",
                                    "PosId4": 910,
                                    "PosLevel": 3,
                                    "UnitCode": "211",
                                    "TopPosId": 1,
                                    "TopPosCode": "6"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):6 - 063010 - 121,Organisationsenhet(namn och kod):OC Norr - Östersunds sjukhus - Infektionssjukdomar",
                                "id": 1236,
                                "data": {
                                    "PosId": 1236,
                                    "PosCode": "6 - 063010 - 121",
                                    "PosName": "OC Norr - Östersunds sjukhus - Infektionssjukdomar",
                                    "PosNameWithCode": "OC Norr (6) - Östersunds sjukhus (063010) - Infektionssjukdomar (121)",
                                    "PosId4": 1236,
                                    "PosLevel": 3,
                                    "UnitCode": "121",
                                    "TopPosId": 1,
                                    "TopPosCode": "6"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):6 - 063010 - 301,Organisationsenhet(namn och kod):OC Norr - Östersunds sjukhus - Kir klin",
                                "id": 100,
                                "data": {
                                    "PosId": 100,
                                    "PosCode": "6 - 063010 - 301",
                                    "PosName": "OC Norr - Östersunds sjukhus - Kir klin",
                                    "PosNameWithCode": "OC Norr (6) - Östersunds sjukhus (063010) - Kir klin (301)",
                                    "PosId4": 100,
                                    "PosLevel": 3,
                                    "UnitCode": "301",
                                    "TopPosId": 1,
                                    "TopPosCode": "6"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):6 - 063010 - 451,Organisationsenhet(namn och kod):OC Norr - Östersunds sjukhus - Kvinnoklinik",
                                "id": 944,
                                "data": {
                                    "PosId": 944,
                                    "PosCode": "6 - 063010 - 451",
                                    "PosName": "OC Norr - Östersunds sjukhus - Kvinnoklinik",
                                    "PosNameWithCode": "OC Norr (6) - Östersunds sjukhus (063010) - Kvinnoklinik (451)",
                                    "PosId4": 944,
                                    "PosLevel": 3,
                                    "UnitCode": "451",
                                    "TopPosId": 1,
                                    "TopPosCode": "6"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):6 - 063010 - 111,Organisationsenhet(namn och kod):OC Norr - Östersunds sjukhus - Lung klin",
                                "id": 713,
                                "data": {
                                    "PosId": 713,
                                    "PosCode": "6 - 063010 - 111",
                                    "PosName": "OC Norr - Östersunds sjukhus - Lung klin",
                                    "PosNameWithCode": "OC Norr (6) - Östersunds sjukhus (063010) - Lung klin (111)",
                                    "PosId4": 713,
                                    "PosLevel": 3,
                                    "UnitCode": "111",
                                    "TopPosId": 1,
                                    "TopPosCode": "6"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):6 - 063010 - 101,Organisationsenhet(namn och kod):OC Norr - Östersunds sjukhus - Med klin",
                                "id": 413,
                                "data": {
                                    "PosId": 413,
                                    "PosCode": "6 - 063010 - 101",
                                    "PosName": "OC Norr - Östersunds sjukhus - Med klin",
                                    "PosNameWithCode": "OC Norr (6) - Östersunds sjukhus (063010) - Med klin (101)",
                                    "PosId4": 413,
                                    "PosLevel": 3,
                                    "UnitCode": "101",
                                    "TopPosId": 1,
                                    "TopPosCode": "6"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):6 - 063010 - 741,Organisationsenhet(namn och kod):OC Norr - Östersunds sjukhus - Onkologmottagning",
                                "id": 1210,
                                "data": {
                                    "PosId": 1210,
                                    "PosCode": "6 - 063010 - 741",
                                    "PosName": "OC Norr - Östersunds sjukhus - Onkologmottagning",
                                    "PosNameWithCode": "OC Norr (6) - Östersunds sjukhus (063010) - Onkologmottagning (741)",
                                    "PosId4": 1210,
                                    "PosLevel": 3,
                                    "UnitCode": "741",
                                    "TopPosId": 1,
                                    "TopPosCode": "6"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):6 - 063010 - 311,Organisationsenhet(namn och kod):OC Norr - Östersunds sjukhus - Ortoped klin",
                                "id": 1248,
                                "data": {
                                    "PosId": 1248,
                                    "PosCode": "6 - 063010 - 311",
                                    "PosName": "OC Norr - Östersunds sjukhus - Ortoped klin",
                                    "PosNameWithCode": "OC Norr (6) - Östersunds sjukhus (063010) - Ortoped klin (311)",
                                    "PosId4": 1248,
                                    "PosLevel": 3,
                                    "UnitCode": "311",
                                    "TopPosId": 1,
                                    "TopPosCode": "6"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):6 - 063010 - 731,Organisationsenhet(namn och kod):OC Norr - Östersunds sjukhus - Röntgen",
                                "id": 1509,
                                "data": {
                                    "PosId": 1509,
                                    "PosCode": "6 - 063010 - 731",
                                    "PosName": "OC Norr - Östersunds sjukhus - Röntgen",
                                    "PosNameWithCode": "OC Norr (6) - Östersunds sjukhus (063010) - Röntgen (731)",
                                    "PosId4": 1509,
                                    "PosLevel": 3,
                                    "UnitCode": "731",
                                    "TopPosId": 1,
                                    "TopPosCode": "6"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):6 - 063010 - 511,Organisationsenhet(namn och kod):OC Norr - Östersunds sjukhus - Ögon klin",
                                "id": 1252,
                                "data": {
                                    "PosId": 1252,
                                    "PosCode": "6 - 063010 - 511",
                                    "PosName": "OC Norr - Östersunds sjukhus - Ögon klin",
                                    "PosNameWithCode": "OC Norr (6) - Östersunds sjukhus (063010) - Ögon klin (511)",
                                    "PosId4": 1252,
                                    "PosLevel": 3,
                                    "UnitCode": "511",
                                    "TopPosId": 1,
                                    "TopPosCode": "6"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):6 - 063010 - 521,Organisationsenhet(namn och kod):OC Norr - Östersunds sjukhus - ÖNH klin",
                                "id": 453,
                                "data": {
                                    "PosId": 453,
                                    "PosCode": "6 - 063010 - 521",
                                    "PosName": "OC Norr - Östersunds sjukhus - ÖNH klin",
                                    "PosNameWithCode": "OC Norr (6) - Östersunds sjukhus (063010) - ÖNH klin (521)",
                                    "PosId4": 453,
                                    "PosLevel": 3,
                                    "UnitCode": "521",
                                    "TopPosId": 1,
                                    "TopPosCode": "6"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):6 - 062011 - 301,Organisationsenhet(namn och kod):OC Norr - Öviks sjukhus - Kir klin",
                                "id": 106,
                                "data": {
                                    "PosId": 106,
                                    "PosCode": "6 - 062011 - 301",
                                    "PosName": "OC Norr - Öviks sjukhus - Kir klin",
                                    "PosNameWithCode": "OC Norr (6) - Öviks sjukhus (062011) - Kir klin (301)",
                                    "PosId4": 106,
                                    "PosLevel": 3,
                                    "UnitCode": "301",
                                    "TopPosId": 1,
                                    "TopPosCode": "6"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):6 - 062011 - 451,Organisationsenhet(namn och kod):OC Norr - Öviks sjukhus - Kvinnoklinik",
                                "id": 942,
                                "data": {
                                    "PosId": 942,
                                    "PosCode": "6 - 062011 - 451",
                                    "PosName": "OC Norr - Öviks sjukhus - Kvinnoklinik",
                                    "PosNameWithCode": "OC Norr (6) - Öviks sjukhus (062011) - Kvinnoklinik (451)",
                                    "PosId4": 942,
                                    "PosLevel": 3,
                                    "UnitCode": "451",
                                    "TopPosId": 1,
                                    "TopPosCode": "6"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):6 - 062011 - 101,Organisationsenhet(namn och kod):OC Norr - Öviks sjukhus - Med klin",
                                "id": 410,
                                "data": {
                                    "PosId": 410,
                                    "PosCode": "6 - 062011 - 101",
                                    "PosName": "OC Norr - Öviks sjukhus - Med klin",
                                    "PosNameWithCode": "OC Norr (6) - Öviks sjukhus (062011) - Med klin (101)",
                                    "PosId4": 410,
                                    "PosLevel": 3,
                                    "UnitCode": "101",
                                    "TopPosId": 1,
                                    "TopPosCode": "6"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):6 - 062011 - 311,Organisationsenhet(namn och kod):OC Norr - Öviks sjukhus - Ortoped klin",
                                "id": 1375,
                                "data": {
                                    "PosId": 1375,
                                    "PosCode": "6 - 062011 - 311",
                                    "PosName": "OC Norr - Öviks sjukhus - Ortoped klin",
                                    "PosNameWithCode": "OC Norr (6) - Öviks sjukhus (062011) - Ortoped klin (311)",
                                    "PosId4": 1375,
                                    "PosLevel": 3,
                                    "UnitCode": "311",
                                    "TopPosId": 1,
                                    "TopPosCode": "6"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):6 - 062011 - 731,Organisationsenhet(namn och kod):OC Norr - Öviks sjukhus - Röntgen",
                                "id": 1510,
                                "data": {
                                    "PosId": 1510,
                                    "PosCode": "6 - 062011 - 731",
                                    "PosName": "OC Norr - Öviks sjukhus - Röntgen",
                                    "PosNameWithCode": "OC Norr (6) - Öviks sjukhus (062011) - Röntgen (731)",
                                    "PosId4": 1510,
                                    "PosLevel": 3,
                                    "UnitCode": "731",
                                    "TopPosId": 1,
                                    "TopPosCode": "6"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):6 - 062011 - 521,Organisationsenhet(namn och kod):OC Norr - Öviks sjukhus - ÖNH klin",
                                "id": 718,
                                "data": {
                                    "PosId": 718,
                                    "PosCode": "6 - 062011 - 521",
                                    "PosName": "OC Norr - Öviks sjukhus - ÖNH klin",
                                    "PosNameWithCode": "OC Norr (6) - Öviks sjukhus (062011) - ÖNH klin (521)",
                                    "PosId4": 718,
                                    "PosLevel": 3,
                                    "UnitCode": "521",
                                    "TopPosId": 1,
                                    "TopPosCode": "6"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):1 - 099934 - 010,Organisationsenhet(namn och kod):OC Sthlm/Gotland - Abbi Hosseini - Privatläkare",
                                "id": 1347,
                                "data": {
                                    "PosId": 1347,
                                    "PosCode": "1 - 099934 - 010",
                                    "PosName": "OC Sthlm/Gotland - Abbi Hosseini - Privatläkare",
                                    "PosNameWithCode": "OC Sthlm/Gotland (1) - Abbi Hosseini (099934) - Privatläkare (010)",
                                    "PosId4": 1347,
                                    "PosLevel": 3,
                                    "UnitCode": "010",
                                    "TopPosId": 6,
                                    "TopPosCode": "1"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):1 - 011335 - 361,Organisationsenhet(namn och kod):OC Sthlm/Gotland - Aleris Specialistvård Handen - Urologiska kliniken",
                                "id": 1309,
                                "data": {
                                    "PosId": 1309,
                                    "PosCode": "1 - 011335 - 361",
                                    "PosName": "OC Sthlm/Gotland - Aleris Specialistvård Handen - Urologiska kliniken",
                                    "PosNameWithCode": "OC Sthlm/Gotland (1) - Aleris Specialistvård Handen (011335) - Urologiska kliniken (361)",
                                    "PosId4": 1309,
                                    "PosLevel": 3,
                                    "UnitCode": "361",
                                    "TopPosId": 6,
                                    "TopPosCode": "1"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):1 - 010010 - 431,Organisationsenhet(namn och kod):OC Sthlm/Gotland - Aleris Specialistvård Sabbatsberg - Gynmottagning",
                                "id": 1359,
                                "data": {
                                    "PosId": 1359,
                                    "PosCode": "1 - 010010 - 431",
                                    "PosName": "OC Sthlm/Gotland - Aleris Specialistvård Sabbatsberg - Gynmottagning",
                                    "PosNameWithCode": "OC Sthlm/Gotland (1) - Aleris Specialistvård Sabbatsberg (010010) - Gynmottagning (431)",
                                    "PosId4": 1359,
                                    "PosLevel": 3,
                                    "UnitCode": "431",
                                    "TopPosId": 6,
                                    "TopPosCode": "1"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):1 - 096025 - 010,Organisationsenhet(namn och kod):OC Sthlm/Gotland - Anette Sjösten - Privat läkare",
                                "id": 1261,
                                "data": {
                                    "PosId": 1261,
                                    "PosCode": "1 - 096025 - 010",
                                    "PosName": "OC Sthlm/Gotland - Anette Sjösten - Privat läkare",
                                    "PosNameWithCode": "OC Sthlm/Gotland (1) - Anette Sjösten (096025) - Privat läkare (010)",
                                    "PosId4": 1261,
                                    "PosLevel": 3,
                                    "UnitCode": "010",
                                    "TopPosId": 6,
                                    "TopPosCode": "1"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):1 - 097954 - 010,Organisationsenhet(namn och kod):OC Sthlm/Gotland - Barbara Merzan/Aurorakliniken - Privatläkare",
                                "id": 1225,
                                "data": {
                                    "PosId": 1225,
                                    "PosCode": "1 - 097954 - 010",
                                    "PosName": "OC Sthlm/Gotland - Barbara Merzan/Aurorakliniken - Privatläkare",
                                    "PosNameWithCode": "OC Sthlm/Gotland (1) - Barbara Merzan/Aurorakliniken (097954) - Privatläkare (010)",
                                    "PosId4": 1225,
                                    "PosLevel": 3,
                                    "UnitCode": "010",
                                    "TopPosId": 6,
                                    "TopPosCode": "1"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):1 - 099702 - 010,Organisationsenhet(namn och kod):OC Sthlm/Gotland - Bengt-Erik Carbin - Privatläkare",
                                "id": 1345,
                                "data": {
                                    "PosId": 1345,
                                    "PosCode": "1 - 099702 - 010",
                                    "PosName": "OC Sthlm/Gotland - Bengt-Erik Carbin - Privatläkare",
                                    "PosNameWithCode": "OC Sthlm/Gotland (1) - Bengt-Erik Carbin (099702) - Privatläkare (010)",
                                    "PosId4": 1345,
                                    "PosLevel": 3,
                                    "UnitCode": "010",
                                    "TopPosId": 6,
                                    "TopPosCode": "1"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):1 - 099585 - 010,Organisationsenhet(namn och kod):OC Sthlm/Gotland - Berry Katzenstein - Privatläkare",
                                "id": 1339,
                                "data": {
                                    "PosId": 1339,
                                    "PosCode": "1 - 099585 - 010",
                                    "PosName": "OC Sthlm/Gotland - Berry Katzenstein - Privatläkare",
                                    "PosNameWithCode": "OC Sthlm/Gotland (1) - Berry Katzenstein (099585) - Privatläkare (010)",
                                    "PosId4": 1339,
                                    "PosLevel": 3,
                                    "UnitCode": "010",
                                    "TopPosId": 6,
                                    "TopPosCode": "1"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):1 - 010462 - 431,Organisationsenhet(namn och kod):OC Sthlm/Gotland - Betania Gynmottagningen - Gynmottagning",
                                "id": 1243,
                                "data": {
                                    "PosId": 1243,
                                    "PosCode": "1 - 010462 - 431",
                                    "PosName": "OC Sthlm/Gotland - Betania Gynmottagningen - Gynmottagning",
                                    "PosNameWithCode": "OC Sthlm/Gotland (1) - Betania Gynmottagningen (010462) - Gynmottagning (431)",
                                    "PosId4": 1243,
                                    "PosLevel": 3,
                                    "UnitCode": "431",
                                    "TopPosId": 6,
                                    "TopPosCode": "1"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):1 - 099945 - 010,Organisationsenhet(namn och kod):OC Sthlm/Gotland - Bo Bergman - Privatläkare",
                                "id": 1349,
                                "data": {
                                    "PosId": 1349,
                                    "PosCode": "1 - 099945 - 010",
                                    "PosName": "OC Sthlm/Gotland - Bo Bergman - Privatläkare",
                                    "PosNameWithCode": "OC Sthlm/Gotland (1) - Bo Bergman (099945) - Privatläkare (010)",
                                    "PosId4": 1349,
                                    "PosLevel": 3,
                                    "UnitCode": "010",
                                    "TopPosId": 6,
                                    "TopPosCode": "1"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):1 - 099123 - 010,Organisationsenhet(namn och kod):OC Sthlm/Gotland - Bo Jacobsson - Privatläkare",
                                "id": 1343,
                                "data": {
                                    "PosId": 1343,
                                    "PosCode": "1 - 099123 - 010",
                                    "PosName": "OC Sthlm/Gotland - Bo Jacobsson - Privatläkare",
                                    "PosNameWithCode": "OC Sthlm/Gotland (1) - Bo Jacobsson (099123) - Privatläkare (010)",
                                    "PosId4": 1343,
                                    "PosLevel": 3,
                                    "UnitCode": "010",
                                    "TopPosId": 6,
                                    "TopPosCode": "1"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):1 - 097563 - 010,Organisationsenhet(namn och kod):OC Sthlm/Gotland - Bröstmott. Christinaklin. Sophiahemmet - Privatklinik",
                                "id": 1381,
                                "data": {
                                    "PosId": 1381,
                                    "PosCode": "1 - 097563 - 010",
                                    "PosName": "OC Sthlm/Gotland - Bröstmott. Christinaklin. Sophiahemmet - Privatklinik",
                                    "PosNameWithCode": "OC Sthlm/Gotland (1) - Bröstmott. Christinaklin. Sophiahemmet (097563) - Privatklinik (010)",
                                    "PosId4": 1381,
                                    "PosLevel": 3,
                                    "UnitCode": "010",
                                    "TopPosId": 6,
                                    "TopPosCode": "1"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):1 - 030166 - 010,Organisationsenhet(namn och kod):OC Sthlm/Gotland - Caroline Elmér - Privatläkare",
                                "id": 1314,
                                "data": {
                                    "PosId": 1314,
                                    "PosCode": "1 - 030166 - 010",
                                    "PosName": "OC Sthlm/Gotland - Caroline Elmér - Privatläkare",
                                    "PosNameWithCode": "OC Sthlm/Gotland (1) - Caroline Elmér (030166) - Privatläkare (010)",
                                    "PosId4": 1314,
                                    "PosLevel": 3,
                                    "UnitCode": "010",
                                    "TopPosId": 6,
                                    "TopPosCode": "1"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):1 - 097406 - 010,Organisationsenhet(namn och kod):OC Sthlm/Gotland - Catharina Nelson - Privatläkare",
                                "id": 1327,
                                "data": {
                                    "PosId": 1327,
                                    "PosCode": "1 - 097406 - 010",
                                    "PosName": "OC Sthlm/Gotland - Catharina Nelson - Privatläkare",
                                    "PosNameWithCode": "OC Sthlm/Gotland (1) - Catharina Nelson (097406) - Privatläkare (010)",
                                    "PosId4": 1327,
                                    "PosLevel": 3,
                                    "UnitCode": "010",
                                    "TopPosId": 6,
                                    "TopPosCode": "1"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):1 - 090410 - 010,Organisationsenhet(namn och kod):OC Sthlm/Gotland - Christer Kihlfors - Privatläkare",
                                "id": 1144,
                                "data": {
                                    "PosId": 1144,
                                    "PosCode": "1 - 090410 - 010",
                                    "PosName": "OC Sthlm/Gotland - Christer Kihlfors - Privatläkare",
                                    "PosNameWithCode": "OC Sthlm/Gotland (1) - Christer Kihlfors (090410) - Privatläkare (010)",
                                    "PosId4": 1144,
                                    "PosLevel": 3,
                                    "UnitCode": "010",
                                    "TopPosId": 6,
                                    "TopPosCode": "1"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):1 - 098838 - 010,Organisationsenhet(namn och kod):OC Sthlm/Gotland - Clas Hellström - Privatläkare",
                                "id": 1067,
                                "data": {
                                    "PosId": 1067,
                                    "PosCode": "1 - 098838 - 010",
                                    "PosName": "OC Sthlm/Gotland - Clas Hellström - Privatläkare",
                                    "PosNameWithCode": "OC Sthlm/Gotland (1) - Clas Hellström (098838) - Privatläkare (010)",
                                    "PosId4": 1067,
                                    "PosLevel": 3,
                                    "UnitCode": "010",
                                    "TopPosId": 6,
                                    "TopPosCode": "1"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):1 - 010485 - 301,Organisationsenhet(namn och kod):OC Sthlm/Gotland - Dalens Närssjukhus - Kirurgkliniken",
                                "id": 478,
                                "data": {
                                    "PosId": 478,
                                    "PosCode": "1 - 010485 - 301",
                                    "PosName": "OC Sthlm/Gotland - Dalens Närssjukhus - Kirurgkliniken",
                                    "PosNameWithCode": "OC Sthlm/Gotland (1) - Dalens Närssjukhus (010485) - Kirurgkliniken (301)",
                                    "PosId4": 478,
                                    "PosLevel": 3,
                                    "UnitCode": "301",
                                    "TopPosId": 6,
                                    "TopPosCode": "1"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):1 - 011010 - 161,Organisationsenhet(namn och kod):OC Sthlm/Gotland - Danderyds sjukhus - Endokrinologen",
                                "id": 1545,
                                "data": {
                                    "PosId": 1545,
                                    "PosCode": "1 - 011010 - 161",
                                    "PosName": "OC Sthlm/Gotland - Danderyds sjukhus - Endokrinologen",
                                    "PosNameWithCode": "OC Sthlm/Gotland (1) - Danderyds sjukhus (011010) - Endokrinologen (161)",
                                    "PosId4": 1545,
                                    "PosLevel": 3,
                                    "UnitCode": "161",
                                    "TopPosId": 6,
                                    "TopPosCode": "1"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):1 - 011010 - 240,Organisationsenhet(namn och kod):OC Sthlm/Gotland - Danderyds sjukhus - Geriatrik",
                                "id": 1012,
                                "data": {
                                    "PosId": 1012,
                                    "PosCode": "1 - 011010 - 240",
                                    "PosName": "OC Sthlm/Gotland - Danderyds sjukhus - Geriatrik",
                                    "PosNameWithCode": "OC Sthlm/Gotland (1) - Danderyds sjukhus (011010) - Geriatrik (240)",
                                    "PosId4": 1012,
                                    "PosLevel": 3,
                                    "UnitCode": "240",
                                    "TopPosId": 6,
                                    "TopPosCode": "1"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):1 - 011010 - 107,Organisationsenhet(namn och kod):OC Sthlm/Gotland - Danderyds sjukhus - Hematologkliniken",
                                "id": 428,
                                "data": {
                                    "PosId": 428,
                                    "PosCode": "1 - 011010 - 107",
                                    "PosName": "OC Sthlm/Gotland - Danderyds sjukhus - Hematologkliniken",
                                    "PosNameWithCode": "OC Sthlm/Gotland (1) - Danderyds sjukhus (011010) - Hematologkliniken (107)",
                                    "PosId4": 428,
                                    "PosLevel": 3,
                                    "UnitCode": "107",
                                    "TopPosId": 6,
                                    "TopPosCode": "1"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):1 - 011010 - 211,Organisationsenhet(namn och kod):OC Sthlm/Gotland - Danderyds sjukhus - Hudkliniken",
                                "id": 957,
                                "data": {
                                    "PosId": 957,
                                    "PosCode": "1 - 011010 - 211",
                                    "PosName": "OC Sthlm/Gotland - Danderyds sjukhus - Hudkliniken",
                                    "PosNameWithCode": "OC Sthlm/Gotland (1) - Danderyds sjukhus (011010) - Hudkliniken (211)",
                                    "PosId4": 957,
                                    "PosLevel": 3,
                                    "UnitCode": "211",
                                    "TopPosId": 6,
                                    "TopPosCode": "1"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):1 - 011010 - 301,Organisationsenhet(namn och kod):OC Sthlm/Gotland - Danderyds sjukhus - Kirurgkliniken",
                                "id": 225,
                                "data": {
                                    "PosId": 225,
                                    "PosCode": "1 - 011010 - 301",
                                    "PosName": "OC Sthlm/Gotland - Danderyds sjukhus - Kirurgkliniken",
                                    "PosNameWithCode": "OC Sthlm/Gotland (1) - Danderyds sjukhus (011010) - Kirurgkliniken (301)",
                                    "PosId4": 225,
                                    "PosLevel": 3,
                                    "UnitCode": "301",
                                    "TopPosId": 6,
                                    "TopPosCode": "1"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):1 - 011010 - 451,Organisationsenhet(namn och kod):OC Sthlm/Gotland - Danderyds sjukhus - Kvinnoklinik",
                                "id": 613,
                                "data": {
                                    "PosId": 613,
                                    "PosCode": "1 - 011010 - 451",
                                    "PosName": "OC Sthlm/Gotland - Danderyds sjukhus - Kvinnoklinik",
                                    "PosNameWithCode": "OC Sthlm/Gotland (1) - Danderyds sjukhus (011010) - Kvinnoklinik (451)",
                                    "PosId4": 613,
                                    "PosLevel": 3,
                                    "UnitCode": "451",
                                    "TopPosId": 6,
                                    "TopPosCode": "1"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):1 - 011010 - 111,Organisationsenhet(namn och kod):OC Sthlm/Gotland - Danderyds sjukhus - Lungkliniken",
                                "id": 1372,
                                "data": {
                                    "PosId": 1372,
                                    "PosCode": "1 - 011010 - 111",
                                    "PosName": "OC Sthlm/Gotland - Danderyds sjukhus - Lungkliniken",
                                    "PosNameWithCode": "OC Sthlm/Gotland (1) - Danderyds sjukhus (011010) - Lungkliniken (111)",
                                    "PosId4": 1372,
                                    "PosLevel": 3,
                                    "UnitCode": "111",
                                    "TopPosId": 6,
                                    "TopPosCode": "1"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):1 - 011010 - 301010,Organisationsenhet(namn och kod):OC Sthlm/Gotland - Danderyds sjukhus - Löwet specialistmottagning",
                                "id": 575,
                                "data": {
                                    "PosId": 575,
                                    "PosCode": "1 - 011010 - 301010",
                                    "PosName": "OC Sthlm/Gotland - Danderyds sjukhus - Löwet specialistmottagning",
                                    "PosNameWithCode": "OC Sthlm/Gotland (1) - Danderyds sjukhus (011010) - Löwet specialistmottagning (301010)",
                                    "PosId4": 575,
                                    "PosLevel": 3,
                                    "UnitCode": "301010",
                                    "TopPosId": 6,
                                    "TopPosCode": "1"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):1 - 011010 - 101,Organisationsenhet(namn och kod):OC Sthlm/Gotland - Danderyds sjukhus - Medicinkliniken",
                                "id": 985,
                                "data": {
                                    "PosId": 985,
                                    "PosCode": "1 - 011010 - 101",
                                    "PosName": "OC Sthlm/Gotland - Danderyds sjukhus - Medicinkliniken",
                                    "PosNameWithCode": "OC Sthlm/Gotland (1) - Danderyds sjukhus (011010) - Medicinkliniken (101)",
                                    "PosId4": 985,
                                    "PosLevel": 3,
                                    "UnitCode": "101",
                                    "TopPosId": 6,
                                    "TopPosCode": "1"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):1 - 011010 - 151,Organisationsenhet(namn och kod):OC Sthlm/Gotland - Danderyds sjukhus - Njurmedicin",
                                "id": 1407,
                                "data": {
                                    "PosId": 1407,
                                    "PosCode": "1 - 011010 - 151",
                                    "PosName": "OC Sthlm/Gotland - Danderyds sjukhus - Njurmedicin",
                                    "PosNameWithCode": "OC Sthlm/Gotland (1) - Danderyds sjukhus (011010) - Njurmedicin (151)",
                                    "PosId4": 1407,
                                    "PosLevel": 3,
                                    "UnitCode": "151",
                                    "TopPosId": 6,
                                    "TopPosCode": "1"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):1 - 011010 - 741,Organisationsenhet(namn och kod):OC Sthlm/Gotland - Danderyds sjukhus - Onkologkliniken",
                                "id": 529,
                                "data": {
                                    "PosId": 529,
                                    "PosCode": "1 - 011010 - 741",
                                    "PosName": "OC Sthlm/Gotland - Danderyds sjukhus - Onkologkliniken",
                                    "PosNameWithCode": "OC Sthlm/Gotland (1) - Danderyds sjukhus (011010) - Onkologkliniken (741)",
                                    "PosId4": 529,
                                    "PosLevel": 3,
                                    "UnitCode": "741",
                                    "TopPosId": 6,
                                    "TopPosCode": "1"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):1 - 011010 - 731,Organisationsenhet(namn och kod):OC Sthlm/Gotland - Danderyds sjukhus - Röntgenmottagning",
                                "id": 1467,
                                "data": {
                                    "PosId": 1467,
                                    "PosCode": "1 - 011010 - 731",
                                    "PosName": "OC Sthlm/Gotland - Danderyds sjukhus - Röntgenmottagning",
                                    "PosNameWithCode": "OC Sthlm/Gotland (1) - Danderyds sjukhus (011010) - Röntgenmottagning (731)",
                                    "PosId4": 1467,
                                    "PosLevel": 3,
                                    "UnitCode": "731",
                                    "TopPosId": 6,
                                    "TopPosCode": "1"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):1 - 011010 - 361,Organisationsenhet(namn och kod):OC Sthlm/Gotland - Danderyds sjukhus - Urologkliniken",
                                "id": 535,
                                "data": {
                                    "PosId": 535,
                                    "PosCode": "1 - 011010 - 361",
                                    "PosName": "OC Sthlm/Gotland - Danderyds sjukhus - Urologkliniken",
                                    "PosNameWithCode": "OC Sthlm/Gotland (1) - Danderyds sjukhus (011010) - Urologkliniken (361)",
                                    "PosId4": 535,
                                    "PosLevel": 3,
                                    "UnitCode": "361",
                                    "TopPosId": 6,
                                    "TopPosCode": "1"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):1 - 090952 - 010,Organisationsenhet(namn och kod):OC Sthlm/Gotland - Dushanka Kristiansson - Privatläkare",
                                "id": 1297,
                                "data": {
                                    "PosId": 1297,
                                    "PosCode": "1 - 090952 - 010",
                                    "PosName": "OC Sthlm/Gotland - Dushanka Kristiansson - Privatläkare",
                                    "PosNameWithCode": "OC Sthlm/Gotland (1) - Dushanka Kristiansson (090952) - Privatläkare (010)",
                                    "PosId4": 1297,
                                    "PosLevel": 3,
                                    "UnitCode": "010",
                                    "TopPosId": 6,
                                    "TopPosCode": "1"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):1 - 010481 - 431,Organisationsenhet(namn och kod):OC Sthlm/Gotland - Ersta sjukhus - Gynmottagning",
                                "id": 1217,
                                "data": {
                                    "PosId": 1217,
                                    "PosCode": "1 - 010481 - 431",
                                    "PosName": "OC Sthlm/Gotland - Ersta sjukhus - Gynmottagning",
                                    "PosNameWithCode": "OC Sthlm/Gotland (1) - Ersta sjukhus (010481) - Gynmottagning (431)",
                                    "PosId4": 1217,
                                    "PosLevel": 3,
                                    "UnitCode": "431",
                                    "TopPosId": 6,
                                    "TopPosCode": "1"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):1 - 010481 - 301,Organisationsenhet(namn och kod):OC Sthlm/Gotland - Ersta sjukhus - Kirurgkliniken",
                                "id": 227,
                                "data": {
                                    "PosId": 227,
                                    "PosCode": "1 - 010481 - 301",
                                    "PosName": "OC Sthlm/Gotland - Ersta sjukhus - Kirurgkliniken",
                                    "PosNameWithCode": "OC Sthlm/Gotland (1) - Ersta sjukhus (010481) - Kirurgkliniken (301)",
                                    "PosId4": 227,
                                    "PosLevel": 3,
                                    "UnitCode": "301",
                                    "TopPosId": 6,
                                    "TopPosCode": "1"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):1 - 010481 - 731,Organisationsenhet(namn och kod):OC Sthlm/Gotland - Ersta sjukhus - Röntgenmottagning",
                                "id": 1468,
                                "data": {
                                    "PosId": 1468,
                                    "PosCode": "1 - 010481 - 731",
                                    "PosName": "OC Sthlm/Gotland - Ersta sjukhus - Röntgenmottagning",
                                    "PosNameWithCode": "OC Sthlm/Gotland (1) - Ersta sjukhus (010481) - Röntgenmottagning (731)",
                                    "PosId4": 1468,
                                    "PosLevel": 3,
                                    "UnitCode": "731",
                                    "TopPosId": 6,
                                    "TopPosCode": "1"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):1 - 030473 - 010,Organisationsenhet(namn och kod):OC Sthlm/Gotland - Eugen Wang - Privatläkare",
                                "id": 1319,
                                "data": {
                                    "PosId": 1319,
                                    "PosCode": "1 - 030473 - 010",
                                    "PosName": "OC Sthlm/Gotland - Eugen Wang - Privatläkare",
                                    "PosNameWithCode": "OC Sthlm/Gotland (1) - Eugen Wang (030473) - Privatläkare (010)",
                                    "PosId4": 1319,
                                    "PosLevel": 3,
                                    "UnitCode": "010",
                                    "TopPosId": 6,
                                    "TopPosCode": "1"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):1 - 097381 - 211M01,Organisationsenhet(namn och kod):OC Sthlm/Gotland - Farsta Läkarhus - Hudkliniken",
                                "id": 987,
                                "data": {
                                    "PosId": 987,
                                    "PosCode": "1 - 097381 - 211M01",
                                    "PosName": "OC Sthlm/Gotland - Farsta Läkarhus - Hudkliniken",
                                    "PosNameWithCode": "OC Sthlm/Gotland (1) - Farsta Läkarhus (097381) - Hudkliniken (211M01)",
                                    "PosId4": 987,
                                    "PosLevel": 3,
                                    "UnitCode": "211M01",
                                    "TopPosId": 6,
                                    "TopPosCode": "1"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):1 - 097381 - 010,Organisationsenhet(namn och kod):OC Sthlm/Gotland - Farsta Läkarhus - Privatläkare",
                                "id": 1231,
                                "data": {
                                    "PosId": 1231,
                                    "PosCode": "1 - 097381 - 010",
                                    "PosName": "OC Sthlm/Gotland - Farsta Läkarhus - Privatläkare",
                                    "PosNameWithCode": "OC Sthlm/Gotland (1) - Farsta Läkarhus (097381) - Privatläkare (010)",
                                    "PosId4": 1231,
                                    "PosLevel": 3,
                                    "UnitCode": "010",
                                    "TopPosId": 6,
                                    "TopPosCode": "1"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):1 - 097394 - 211,Organisationsenhet(namn och kod):OC Sthlm/Gotland - Fruängens läkarhus - Hudklinik",
                                "id": 1246,
                                "data": {
                                    "PosId": 1246,
                                    "PosCode": "1 - 097394 - 211",
                                    "PosName": "OC Sthlm/Gotland - Fruängens läkarhus - Hudklinik",
                                    "PosNameWithCode": "OC Sthlm/Gotland (1) - Fruängens läkarhus (097394) - Hudklinik (211)",
                                    "PosId4": 1246,
                                    "PosLevel": 3,
                                    "UnitCode": "211",
                                    "TopPosId": 6,
                                    "TopPosCode": "1"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):1 - 097394 - 301,Organisationsenhet(namn och kod):OC Sthlm/Gotland - Fruängens läkarhus - Kirurgkliniken",
                                "id": 468,
                                "data": {
                                    "PosId": 468,
                                    "PosCode": "1 - 097394 - 301",
                                    "PosName": "OC Sthlm/Gotland - Fruängens läkarhus - Kirurgkliniken",
                                    "PosNameWithCode": "OC Sthlm/Gotland (1) - Fruängens läkarhus (097394) - Kirurgkliniken (301)",
                                    "PosId4": 468,
                                    "PosLevel": 3,
                                    "UnitCode": "301",
                                    "TopPosId": 6,
                                    "TopPosCode": "1"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):1 - 010783 - 010,Organisationsenhet(namn och kod):OC Sthlm/Gotland - Försäkringsmott, Sophiahemmet - Privatklinik",
                                "id": 1379,
                                "data": {
                                    "PosId": 1379,
                                    "PosCode": "1 - 010783 - 010",
                                    "PosName": "OC Sthlm/Gotland - Försäkringsmott, Sophiahemmet - Privatklinik",
                                    "PosNameWithCode": "OC Sthlm/Gotland (1) - Försäkringsmott, Sophiahemmet (010783) - Privatklinik (010)",
                                    "PosId4": 1379,
                                    "PosLevel": 3,
                                    "UnitCode": "010",
                                    "TopPosId": 6,
                                    "TopPosCode": "1"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):1 - 030369 - 010,Organisationsenhet(namn och kod):OC Sthlm/Gotland - Gennady Sarkissov - Privatläkare",
                                "id": 1316,
                                "data": {
                                    "PosId": 1316,
                                    "PosCode": "1 - 030369 - 010",
                                    "PosName": "OC Sthlm/Gotland - Gennady Sarkissov - Privatläkare",
                                    "PosNameWithCode": "OC Sthlm/Gotland (1) - Gennady Sarkissov (030369) - Privatläkare (010)",
                                    "PosId4": 1316,
                                    "PosLevel": 3,
                                    "UnitCode": "010",
                                    "TopPosId": 6,
                                    "TopPosCode": "1"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):1 - 010337 - 431,Organisationsenhet(namn och kod):OC Sthlm/Gotland - Gyn Stockholm Jakosberg - Gynmottagning",
                                "id": 1364,
                                "data": {
                                    "PosId": 1364,
                                    "PosCode": "1 - 010337 - 431",
                                    "PosName": "OC Sthlm/Gotland - Gyn Stockholm Jakosberg - Gynmottagning",
                                    "PosNameWithCode": "OC Sthlm/Gotland (1) - Gyn Stockholm Jakosberg (010337) - Gynmottagning (431)",
                                    "PosId4": 1364,
                                    "PosLevel": 3,
                                    "UnitCode": "431",
                                    "TopPosId": 6,
                                    "TopPosCode": "1"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):1 - 011335 - 431,Organisationsenhet(namn och kod):OC Sthlm/Gotland - Handens sjukhus - Gynmottagning",
                                "id": 1267,
                                "data": {
                                    "PosId": 1267,
                                    "PosCode": "1 - 011335 - 431",
                                    "PosName": "OC Sthlm/Gotland - Handens sjukhus - Gynmottagning",
                                    "PosNameWithCode": "OC Sthlm/Gotland (1) - Handens sjukhus (011335) - Gynmottagning (431)",
                                    "PosId4": 1267,
                                    "PosLevel": 3,
                                    "UnitCode": "431",
                                    "TopPosId": 6,
                                    "TopPosCode": "1"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):1 - 011335 - 301,Organisationsenhet(namn och kod):OC Sthlm/Gotland - Handens sjukhus - Kirurgkliniken",
                                "id": 466,
                                "data": {
                                    "PosId": 466,
                                    "PosCode": "1 - 011335 - 301",
                                    "PosName": "OC Sthlm/Gotland - Handens sjukhus - Kirurgkliniken",
                                    "PosNameWithCode": "OC Sthlm/Gotland (1) - Handens sjukhus (011335) - Kirurgkliniken (301)",
                                    "PosId4": 466,
                                    "PosLevel": 3,
                                    "UnitCode": "301",
                                    "TopPosId": 6,
                                    "TopPosCode": "1"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):1 - 011335 - 361,Organisationsenhet(namn och kod):OC Sthlm/Gotland - Handens sjukhus - Urologkliniken",
                                "id": 1081,
                                "data": {
                                    "PosId": 1081,
                                    "PosCode": "1 - 011335 - 361",
                                    "PosName": "OC Sthlm/Gotland - Handens sjukhus - Urologkliniken",
                                    "PosNameWithCode": "OC Sthlm/Gotland (1) - Handens sjukhus (011335) - Urologkliniken (361)",
                                    "PosId4": 1081,
                                    "PosLevel": 3,
                                    "UnitCode": "361",
                                    "TopPosId": 6,
                                    "TopPosCode": "1"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):1 - 099127 - 010,Organisationsenhet(namn och kod):OC Sthlm/Gotland - Hans Liedberg - Privatläkare",
                                "id": 1075,
                                "data": {
                                    "PosId": 1075,
                                    "PosCode": "1 - 099127 - 010",
                                    "PosName": "OC Sthlm/Gotland - Hans Liedberg - Privatläkare",
                                    "PosNameWithCode": "OC Sthlm/Gotland (1) - Hans Liedberg (099127) - Privatläkare (010)",
                                    "PosId4": 1075,
                                    "PosLevel": 3,
                                    "UnitCode": "010",
                                    "TopPosId": 6,
                                    "TopPosCode": "1"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):1 - 090724 - 010,Organisationsenhet(namn och kod):OC Sthlm/Gotland - Henrik Zetterquist - Privatläkare",
                                "id": 1557,
                                "data": {
                                    "PosId": 1557,
                                    "PosCode": "1 - 090724 - 010",
                                    "PosName": "OC Sthlm/Gotland - Henrik Zetterquist - Privatläkare",
                                    "PosNameWithCode": "OC Sthlm/Gotland (1) - Henrik Zetterquist (090724) - Privatläkare (010)",
                                    "PosId4": 1557,
                                    "PosLevel": 3,
                                    "UnitCode": "010",
                                    "TopPosId": 6,
                                    "TopPosCode": "1"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):1 - 094080 - 010,Organisationsenhet(namn och kod):OC Sthlm/Gotland - Hudcentrum Hagastaden - Privatläkare",
                                "id": 1390,
                                "data": {
                                    "PosId": 1390,
                                    "PosCode": "1 - 094080 - 010",
                                    "PosName": "OC Sthlm/Gotland - Hudcentrum Hagastaden - Privatläkare",
                                    "PosNameWithCode": "OC Sthlm/Gotland (1) - Hudcentrum Hagastaden (094080) - Privatläkare (010)",
                                    "PosId4": 1390,
                                    "PosLevel": 3,
                                    "UnitCode": "010",
                                    "TopPosId": 6,
                                    "TopPosCode": "1"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):1 - 099410 - 010,Organisationsenhet(namn och kod):OC Sthlm/Gotland - Håkan Ageheim - Privatläkare",
                                "id": 1337,
                                "data": {
                                    "PosId": 1337,
                                    "PosCode": "1 - 099410 - 010",
                                    "PosName": "OC Sthlm/Gotland - Håkan Ageheim - Privatläkare",
                                    "PosNameWithCode": "OC Sthlm/Gotland (1) - Håkan Ageheim (099410) - Privatläkare (010)",
                                    "PosId4": 1337,
                                    "PosLevel": 3,
                                    "UnitCode": "010",
                                    "TopPosId": 6,
                                    "TopPosCode": "1"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):1 - 098202 - 010,Organisationsenhet(namn och kod):OC Sthlm/Gotland - Håkan Wallberg - Privatläkare",
                                "id": 1156,
                                "data": {
                                    "PosId": 1156,
                                    "PosCode": "1 - 098202 - 010",
                                    "PosName": "OC Sthlm/Gotland - Håkan Wallberg - Privatläkare",
                                    "PosNameWithCode": "OC Sthlm/Gotland (1) - Håkan Wallberg (098202) - Privatläkare (010)",
                                    "PosId4": 1156,
                                    "PosLevel": 3,
                                    "UnitCode": "010",
                                    "TopPosId": 6,
                                    "TopPosCode": "1"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):1 - 099126 - 010,Organisationsenhet(namn och kod):OC Sthlm/Gotland - Jan Brolin - Privatläkare",
                                "id": 1335,
                                "data": {
                                    "PosId": 1335,
                                    "PosCode": "1 - 099126 - 010",
                                    "PosName": "OC Sthlm/Gotland - Jan Brolin - Privatläkare",
                                    "PosNameWithCode": "OC Sthlm/Gotland (1) - Jan Brolin (099126) - Privatläkare (010)",
                                    "PosId4": 1335,
                                    "PosLevel": 3,
                                    "UnitCode": "010",
                                    "TopPosId": 6,
                                    "TopPosCode": "1"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):1 - 099047 - 010,Organisationsenhet(namn och kod):OC Sthlm/Gotland - Jan Svensson - Privatläkare",
                                "id": 1341,
                                "data": {
                                    "PosId": 1341,
                                    "PosCode": "1 - 099047 - 010",
                                    "PosName": "OC Sthlm/Gotland - Jan Svensson - Privatläkare",
                                    "PosNameWithCode": "OC Sthlm/Gotland (1) - Jan Svensson (099047) - Privatläkare (010)",
                                    "PosId4": 1341,
                                    "PosLevel": 3,
                                    "UnitCode": "010",
                                    "TopPosId": 6,
                                    "TopPosCode": "1"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):1 - 010493 - 301,Organisationsenhet(namn och kod):OC Sthlm/Gotland - Järva Närsjukhus - Kirurgkliniken",
                                "id": 480,
                                "data": {
                                    "PosId": 480,
                                    "PosCode": "1 - 010493 - 301",
                                    "PosName": "OC Sthlm/Gotland - Järva Närsjukhus - Kirurgkliniken",
                                    "PosNameWithCode": "OC Sthlm/Gotland (1) - Järva Närsjukhus (010493) - Kirurgkliniken (301)",
                                    "PosId4": 480,
                                    "PosLevel": 3,
                                    "UnitCode": "301",
                                    "TopPosId": 6,
                                    "TopPosCode": "1"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):1 - 098457 - 010,Organisationsenhet(namn och kod):OC Sthlm/Gotland - Karin Etzler/Gyn.mott AB/ Vällingby - Privatläkare",
                                "id": 1223,
                                "data": {
                                    "PosId": 1223,
                                    "PosCode": "1 - 098457 - 010",
                                    "PosName": "OC Sthlm/Gotland - Karin Etzler/Gyn.mott AB/ Vällingby - Privatläkare",
                                    "PosNameWithCode": "OC Sthlm/Gotland (1) - Karin Etzler/Gyn.mott AB/ Vällingby (098457) - Privatläkare (010)",
                                    "PosId4": 1223,
                                    "PosLevel": 3,
                                    "UnitCode": "010",
                                    "TopPosId": 6,
                                    "TopPosCode": "1"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):1 - 096921 - 010,Organisationsenhet(namn och kod):OC Sthlm/Gotland - Karl-Erik Tronner - Privatläkare",
                                "id": 1325,
                                "data": {
                                    "PosId": 1325,
                                    "PosCode": "1 - 096921 - 010",
                                    "PosName": "OC Sthlm/Gotland - Karl-Erik Tronner - Privatläkare",
                                    "PosNameWithCode": "OC Sthlm/Gotland (1) - Karl-Erik Tronner (096921) - Privatläkare (010)",
                                    "PosId4": 1325,
                                    "PosLevel": 3,
                                    "UnitCode": "010",
                                    "TopPosId": 6,
                                    "TopPosCode": "1"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):1 - 095011 - 010,Organisationsenhet(namn och kod):OC Sthlm/Gotland - Katarina Högset - Privatläkare",
                                "id": 1368,
                                "data": {
                                    "PosId": 1368,
                                    "PosCode": "1 - 095011 - 010",
                                    "PosName": "OC Sthlm/Gotland - Katarina Högset - Privatläkare",
                                    "PosNameWithCode": "OC Sthlm/Gotland (1) - Katarina Högset (095011) - Privatläkare (010)",
                                    "PosId4": 1368,
                                    "PosLevel": 3,
                                    "UnitCode": "010",
                                    "TopPosId": 6,
                                    "TopPosCode": "1"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):1 - 011002 - 240,Organisationsenhet(namn och kod):OC Sthlm/Gotland - KS Huddinge - Geriatrik",
                                "id": 1011,
                                "data": {
                                    "PosId": 1011,
                                    "PosCode": "1 - 011002 - 240",
                                    "PosName": "OC Sthlm/Gotland - KS Huddinge - Geriatrik",
                                    "PosNameWithCode": "OC Sthlm/Gotland (1) - KS Huddinge (011002) - Geriatrik (240)",
                                    "PosId4": 1011,
                                    "PosLevel": 3,
                                    "UnitCode": "240",
                                    "TopPosId": 6,
                                    "TopPosCode": "1"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):1 - 011002 - 107,Organisationsenhet(namn och kod):OC Sthlm/Gotland - KS Huddinge - Hematologklinik",
                                "id": 343,
                                "data": {
                                    "PosId": 343,
                                    "PosCode": "1 - 011002 - 107",
                                    "PosName": "OC Sthlm/Gotland - KS Huddinge - Hematologklinik",
                                    "PosNameWithCode": "OC Sthlm/Gotland (1) - KS Huddinge (011002) - Hematologklinik (107)",
                                    "PosId4": 343,
                                    "PosLevel": 3,
                                    "UnitCode": "107",
                                    "TopPosId": 6,
                                    "TopPosCode": "1"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):1 - 011002 - 211,Organisationsenhet(namn och kod):OC Sthlm/Gotland - KS Huddinge - Hudklinik",
                                "id": 1072,
                                "data": {
                                    "PosId": 1072,
                                    "PosCode": "1 - 011002 - 211",
                                    "PosName": "OC Sthlm/Gotland - KS Huddinge - Hudklinik",
                                    "PosNameWithCode": "OC Sthlm/Gotland (1) - KS Huddinge (011002) - Hudklinik (211)",
                                    "PosId4": 1072,
                                    "PosLevel": 3,
                                    "UnitCode": "211",
                                    "TopPosId": 6,
                                    "TopPosCode": "1"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):1 - 011002 - 301,Organisationsenhet(namn och kod):OC Sthlm/Gotland - KS Huddinge - Kirurgkliniken",
                                "id": 138,
                                "data": {
                                    "PosId": 138,
                                    "PosCode": "1 - 011002 - 301",
                                    "PosName": "OC Sthlm/Gotland - KS Huddinge - Kirurgkliniken",
                                    "PosNameWithCode": "OC Sthlm/Gotland (1) - KS Huddinge (011002) - Kirurgkliniken (301)",
                                    "PosId4": 138,
                                    "PosLevel": 3,
                                    "UnitCode": "301",
                                    "TopPosId": 6,
                                    "TopPosCode": "1"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):1 - 011002 - 451,Organisationsenhet(namn och kod):OC Sthlm/Gotland - KS Huddinge - Kvinnoklinik",
                                "id": 612,
                                "data": {
                                    "PosId": 612,
                                    "PosCode": "1 - 011002 - 451",
                                    "PosName": "OC Sthlm/Gotland - KS Huddinge - Kvinnoklinik",
                                    "PosNameWithCode": "OC Sthlm/Gotland (1) - KS Huddinge (011002) - Kvinnoklinik (451)",
                                    "PosId4": 612,
                                    "PosLevel": 3,
                                    "UnitCode": "451",
                                    "TopPosId": 6,
                                    "TopPosCode": "1"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):1 - 011002 - 111,Organisationsenhet(namn och kod):OC Sthlm/Gotland - KS Huddinge - Lungkliniken",
                                "id": 650,
                                "data": {
                                    "PosId": 650,
                                    "PosCode": "1 - 011002 - 111",
                                    "PosName": "OC Sthlm/Gotland - KS Huddinge - Lungkliniken",
                                    "PosNameWithCode": "OC Sthlm/Gotland (1) - KS Huddinge (011002) - Lungkliniken (111)",
                                    "PosId4": 650,
                                    "PosLevel": 3,
                                    "UnitCode": "111",
                                    "TopPosId": 6,
                                    "TopPosCode": "1"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):1 - 011002 - 101,Organisationsenhet(namn och kod):OC Sthlm/Gotland - KS Huddinge - Medicinkliniken",
                                "id": 984,
                                "data": {
                                    "PosId": 984,
                                    "PosCode": "1 - 011002 - 101",
                                    "PosName": "OC Sthlm/Gotland - KS Huddinge - Medicinkliniken",
                                    "PosNameWithCode": "OC Sthlm/Gotland (1) - KS Huddinge (011002) - Medicinkliniken (101)",
                                    "PosId4": 984,
                                    "PosLevel": 3,
                                    "UnitCode": "101",
                                    "TopPosId": 6,
                                    "TopPosCode": "1"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):1 - 011002 - 151,Organisationsenhet(namn och kod):OC Sthlm/Gotland - KS Huddinge - Njurmedicin",
                                "id": 1008,
                                "data": {
                                    "PosId": 1008,
                                    "PosCode": "1 - 011002 - 151",
                                    "PosName": "OC Sthlm/Gotland - KS Huddinge - Njurmedicin",
                                    "PosNameWithCode": "OC Sthlm/Gotland (1) - KS Huddinge (011002) - Njurmedicin (151)",
                                    "PosId4": 1008,
                                    "PosLevel": 3,
                                    "UnitCode": "151",
                                    "TopPosId": 6,
                                    "TopPosCode": "1"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):1 - 011002 - 731,Organisationsenhet(namn och kod):OC Sthlm/Gotland - KS Huddinge - Röntgenmottagning",
                                "id": 1469,
                                "data": {
                                    "PosId": 1469,
                                    "PosCode": "1 - 011002 - 731",
                                    "PosName": "OC Sthlm/Gotland - KS Huddinge - Röntgenmottagning",
                                    "PosNameWithCode": "OC Sthlm/Gotland (1) - KS Huddinge (011002) - Röntgenmottagning (731)",
                                    "PosId4": 1469,
                                    "PosLevel": 3,
                                    "UnitCode": "731",
                                    "TopPosId": 6,
                                    "TopPosCode": "1"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):1 - 011002 - 371,Organisationsenhet(namn och kod):OC Sthlm/Gotland - KS Huddinge - Transplantationskliniken",
                                "id": 704,
                                "data": {
                                    "PosId": 704,
                                    "PosCode": "1 - 011002 - 371",
                                    "PosName": "OC Sthlm/Gotland - KS Huddinge - Transplantationskliniken",
                                    "PosNameWithCode": "OC Sthlm/Gotland (1) - KS Huddinge (011002) - Transplantationskliniken (371)",
                                    "PosId4": 704,
                                    "PosLevel": 3,
                                    "UnitCode": "371",
                                    "TopPosId": 6,
                                    "TopPosCode": "1"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):1 - 011002 - 361,Organisationsenhet(namn och kod):OC Sthlm/Gotland - KS Huddinge - Urologkliniken",
                                "id": 187,
                                "data": {
                                    "PosId": 187,
                                    "PosCode": "1 - 011002 - 361",
                                    "PosName": "OC Sthlm/Gotland - KS Huddinge - Urologkliniken",
                                    "PosNameWithCode": "OC Sthlm/Gotland (1) - KS Huddinge (011002) - Urologkliniken (361)",
                                    "PosId4": 187,
                                    "PosLevel": 3,
                                    "UnitCode": "361",
                                    "TopPosId": 6,
                                    "TopPosCode": "1"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):1 - 011001 - 203,Organisationsenhet(namn och kod):OC Sthlm/Gotland - KS Solna - Barnonkologi",
                                "id": 1006,
                                "data": {
                                    "PosId": 1006,
                                    "PosCode": "1 - 011001 - 203",
                                    "PosName": "OC Sthlm/Gotland - KS Solna - Barnonkologi",
                                    "PosNameWithCode": "OC Sthlm/Gotland (1) - KS Solna (011001) - Barnonkologi (203)",
                                    "PosId4": 1006,
                                    "PosLevel": 3,
                                    "UnitCode": "203",
                                    "TopPosId": 6,
                                    "TopPosCode": "1"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):1 - 011001 - 301M10,Organisationsenhet(namn och kod):OC Sthlm/Gotland - KS Solna - Bröstcentrum",
                                "id": 119,
                                "data": {
                                    "PosId": 119,
                                    "PosCode": "1 - 011001 - 301M10",
                                    "PosName": "OC Sthlm/Gotland - KS Solna - Bröstcentrum",
                                    "PosNameWithCode": "OC Sthlm/Gotland (1) - KS Solna (011001) - Bröstcentrum (301M10)",
                                    "PosId4": 119,
                                    "PosLevel": 3,
                                    "UnitCode": "301M10",
                                    "TopPosId": 6,
                                    "TopPosCode": "1"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):1 - 011001 - 161,Organisationsenhet(namn och kod):OC Sthlm/Gotland - KS Solna - Endokrinologen",
                                "id": 1184,
                                "data": {
                                    "PosId": 1184,
                                    "PosCode": "1 - 011001 - 161",
                                    "PosName": "OC Sthlm/Gotland - KS Solna - Endokrinologen",
                                    "PosNameWithCode": "OC Sthlm/Gotland (1) - KS Solna (011001) - Endokrinologen (161)",
                                    "PosId4": 1184,
                                    "PosLevel": 3,
                                    "UnitCode": "161",
                                    "TopPosId": 6,
                                    "TopPosCode": "1"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):1 - 011001 - 105,Organisationsenhet(namn och kod):OC Sthlm/Gotland - KS Solna - Gastromedicin",
                                "id": 1005,
                                "data": {
                                    "PosId": 1005,
                                    "PosCode": "1 - 011001 - 105",
                                    "PosName": "OC Sthlm/Gotland - KS Solna - Gastromedicin",
                                    "PosNameWithCode": "OC Sthlm/Gotland (1) - KS Solna (011001) - Gastromedicin (105)",
                                    "PosId4": 1005,
                                    "PosLevel": 3,
                                    "UnitCode": "105",
                                    "TopPosId": 6,
                                    "TopPosCode": "1"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):1 - 011001 - 107,Organisationsenhet(namn och kod):OC Sthlm/Gotland - KS Solna - Hematologkliniken",
                                "id": 342,
                                "data": {
                                    "PosId": 342,
                                    "PosCode": "1 - 011001 - 107",
                                    "PosName": "OC Sthlm/Gotland - KS Solna - Hematologkliniken",
                                    "PosNameWithCode": "OC Sthlm/Gotland (1) - KS Solna (011001) - Hematologkliniken (107)",
                                    "PosId4": 342,
                                    "PosLevel": 3,
                                    "UnitCode": "107",
                                    "TopPosId": 6,
                                    "TopPosCode": "1"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):1 - 011001 - 211,Organisationsenhet(namn och kod):OC Sthlm/Gotland - KS Solna - Hudklinik",
                                "id": 802,
                                "data": {
                                    "PosId": 802,
                                    "PosCode": "1 - 011001 - 211",
                                    "PosName": "OC Sthlm/Gotland - KS Solna - Hudklinik",
                                    "PosNameWithCode": "OC Sthlm/Gotland (1) - KS Solna (011001) - Hudklinik (211)",
                                    "PosId4": 802,
                                    "PosLevel": 3,
                                    "UnitCode": "211",
                                    "TopPosId": 6,
                                    "TopPosCode": "1"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):1 - 011001 - 301,Organisationsenhet(namn och kod):OC Sthlm/Gotland - KS Solna - Kirurgkliniken",
                                "id": 19,
                                "data": {
                                    "PosId": 19,
                                    "PosCode": "1 - 011001 - 301",
                                    "PosName": "OC Sthlm/Gotland - KS Solna - Kirurgkliniken",
                                    "PosNameWithCode": "OC Sthlm/Gotland (1) - KS Solna (011001) - Kirurgkliniken (301)",
                                    "PosId4": 19,
                                    "PosLevel": 3,
                                    "UnitCode": "301",
                                    "TopPosId": 6,
                                    "TopPosCode": "1"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):1 - 011001 - 451,Organisationsenhet(namn och kod):OC Sthlm/Gotland - KS Solna - Kvinnoklinik",
                                "id": 609,
                                "data": {
                                    "PosId": 609,
                                    "PosCode": "1 - 011001 - 451",
                                    "PosName": "OC Sthlm/Gotland - KS Solna - Kvinnoklinik",
                                    "PosNameWithCode": "OC Sthlm/Gotland (1) - KS Solna (011001) - Kvinnoklinik (451)",
                                    "PosId4": 609,
                                    "PosLevel": 3,
                                    "UnitCode": "451",
                                    "TopPosId": 6,
                                    "TopPosCode": "1"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):1 - 011001 - 342,Organisationsenhet(namn och kod):OC Sthlm/Gotland - KS Solna - Kärlkirurg",
                                "id": 1004,
                                "data": {
                                    "PosId": 1004,
                                    "PosCode": "1 - 011001 - 342",
                                    "PosName": "OC Sthlm/Gotland - KS Solna - Kärlkirurg",
                                    "PosNameWithCode": "OC Sthlm/Gotland (1) - KS Solna (011001) - Kärlkirurg (342)",
                                    "PosId4": 1004,
                                    "PosLevel": 3,
                                    "UnitCode": "342",
                                    "TopPosId": 6,
                                    "TopPosCode": "1"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):1 - 011001 - 111,Organisationsenhet(namn och kod):OC Sthlm/Gotland - KS Solna - Lungkliniken",
                                "id": 648,
                                "data": {
                                    "PosId": 648,
                                    "PosCode": "1 - 011001 - 111",
                                    "PosName": "OC Sthlm/Gotland - KS Solna - Lungkliniken",
                                    "PosNameWithCode": "OC Sthlm/Gotland (1) - KS Solna (011001) - Lungkliniken (111)",
                                    "PosId4": 648,
                                    "PosLevel": 3,
                                    "UnitCode": "111",
                                    "TopPosId": 6,
                                    "TopPosCode": "1"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):1 - 011001 - 101,Organisationsenhet(namn och kod):OC Sthlm/Gotland - KS Solna - Medicinkliniken",
                                "id": 20,
                                "data": {
                                    "PosId": 20,
                                    "PosCode": "1 - 011001 - 101",
                                    "PosName": "OC Sthlm/Gotland - KS Solna - Medicinkliniken",
                                    "PosNameWithCode": "OC Sthlm/Gotland (1) - KS Solna (011001) - Medicinkliniken (101)",
                                    "PosId4": 20,
                                    "PosLevel": 3,
                                    "UnitCode": "101",
                                    "TopPosId": 6,
                                    "TopPosCode": "1"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):1 - 011001 - 331,Organisationsenhet(namn och kod):OC Sthlm/Gotland - KS Solna - Neurokirurgiska kliniken",
                                "id": 806,
                                "data": {
                                    "PosId": 806,
                                    "PosCode": "1 - 011001 - 331",
                                    "PosName": "OC Sthlm/Gotland - KS Solna - Neurokirurgiska kliniken",
                                    "PosNameWithCode": "OC Sthlm/Gotland (1) - KS Solna (011001) - Neurokirurgiska kliniken (331)",
                                    "PosId4": 806,
                                    "PosLevel": 3,
                                    "UnitCode": "331",
                                    "TopPosId": 6,
                                    "TopPosCode": "1"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):1 - 011001 - 221,Organisationsenhet(namn och kod):OC Sthlm/Gotland - KS Solna - Neurologiska kliniken",
                                "id": 805,
                                "data": {
                                    "PosId": 805,
                                    "PosCode": "1 - 011001 - 221",
                                    "PosName": "OC Sthlm/Gotland - KS Solna - Neurologiska kliniken",
                                    "PosNameWithCode": "OC Sthlm/Gotland (1) - KS Solna (011001) - Neurologiska kliniken (221)",
                                    "PosId4": 805,
                                    "PosLevel": 3,
                                    "UnitCode": "221",
                                    "TopPosId": 6,
                                    "TopPosCode": "1"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):1 - 011001 - 123,Organisationsenhet(namn och kod):OC Sthlm/Gotland - KS Solna - Neurologiska/Neurokirurgiska Kliniken",
                                "id": 1019,
                                "data": {
                                    "PosId": 1019,
                                    "PosCode": "1 - 011001 - 123",
                                    "PosName": "OC Sthlm/Gotland - KS Solna - Neurologiska/Neurokirurgiska Kliniken",
                                    "PosNameWithCode": "OC Sthlm/Gotland (1) - KS Solna (011001) - Neurologiska/Neurokirurgiska Kliniken (123)",
                                    "PosId4": 1019,
                                    "PosLevel": 3,
                                    "UnitCode": "123",
                                    "TopPosId": 6,
                                    "TopPosCode": "1"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):1 - 011001 - 751,Organisationsenhet(namn och kod):OC Sthlm/Gotland - KS Solna - Onkologi Gyn",
                                "id": 1002,
                                "data": {
                                    "PosId": 1002,
                                    "PosCode": "1 - 011001 - 751",
                                    "PosName": "OC Sthlm/Gotland - KS Solna - Onkologi Gyn",
                                    "PosNameWithCode": "OC Sthlm/Gotland (1) - KS Solna (011001) - Onkologi Gyn (751)",
                                    "PosId4": 1002,
                                    "PosLevel": 3,
                                    "UnitCode": "751",
                                    "TopPosId": 6,
                                    "TopPosCode": "1"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):1 - 011001 - 741,Organisationsenhet(namn och kod):OC Sthlm/Gotland - KS Solna - Onkologkliniken",
                                "id": 518,
                                "data": {
                                    "PosId": 518,
                                    "PosCode": "1 - 011001 - 741",
                                    "PosName": "OC Sthlm/Gotland - KS Solna - Onkologkliniken",
                                    "PosNameWithCode": "OC Sthlm/Gotland (1) - KS Solna (011001) - Onkologkliniken (741)",
                                    "PosId4": 518,
                                    "PosLevel": 3,
                                    "UnitCode": "741",
                                    "TopPosId": 6,
                                    "TopPosCode": "1"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):1 - 011001 - 311,Organisationsenhet(namn och kod):OC Sthlm/Gotland - KS Solna - Ortopedkirurg",
                                "id": 1003,
                                "data": {
                                    "PosId": 1003,
                                    "PosCode": "1 - 011001 - 311",
                                    "PosName": "OC Sthlm/Gotland - KS Solna - Ortopedkirurg",
                                    "PosNameWithCode": "OC Sthlm/Gotland (1) - KS Solna (011001) - Ortopedkirurg (311)",
                                    "PosId4": 1003,
                                    "PosLevel": 3,
                                    "UnitCode": "311",
                                    "TopPosId": 6,
                                    "TopPosCode": "1"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):1 - 011001 - 351,Organisationsenhet(namn och kod):OC Sthlm/Gotland - KS Solna - Plastikkir. klin",
                                "id": 956,
                                "data": {
                                    "PosId": 956,
                                    "PosCode": "1 - 011001 - 351",
                                    "PosName": "OC Sthlm/Gotland - KS Solna - Plastikkir. klin",
                                    "PosNameWithCode": "OC Sthlm/Gotland (1) - KS Solna (011001) - Plastikkir. klin (351)",
                                    "PosId4": 956,
                                    "PosLevel": 3,
                                    "UnitCode": "351",
                                    "TopPosId": 6,
                                    "TopPosCode": "1"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):1 - 011001 - 731,Organisationsenhet(namn och kod):OC Sthlm/Gotland - KS Solna - Röntgenmottagning",
                                "id": 1470,
                                "data": {
                                    "PosId": 1470,
                                    "PosCode": "1 - 011001 - 731",
                                    "PosName": "OC Sthlm/Gotland - KS Solna - Röntgenmottagning",
                                    "PosNameWithCode": "OC Sthlm/Gotland (1) - KS Solna (011001) - Röntgenmottagning (731)",
                                    "PosId4": 1470,
                                    "PosLevel": 3,
                                    "UnitCode": "731",
                                    "TopPosId": 6,
                                    "TopPosCode": "1"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):1 - 011001 - 341,Organisationsenhet(namn och kod):OC Sthlm/Gotland - KS Solna - Thoraxkliniken",
                                "id": 1549,
                                "data": {
                                    "PosId": 1549,
                                    "PosCode": "1 - 011001 - 341",
                                    "PosName": "OC Sthlm/Gotland - KS Solna - Thoraxkliniken",
                                    "PosNameWithCode": "OC Sthlm/Gotland (1) - KS Solna (011001) - Thoraxkliniken (341)",
                                    "PosId4": 1549,
                                    "PosLevel": 3,
                                    "UnitCode": "341",
                                    "TopPosId": 6,
                                    "TopPosCode": "1"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):1 - 011001 - 361,Organisationsenhet(namn och kod):OC Sthlm/Gotland - KS Solna - Urologkliniken",
                                "id": 186,
                                "data": {
                                    "PosId": 186,
                                    "PosCode": "1 - 011001 - 361",
                                    "PosName": "OC Sthlm/Gotland - KS Solna - Urologkliniken",
                                    "PosNameWithCode": "OC Sthlm/Gotland (1) - KS Solna (011001) - Urologkliniken (361)",
                                    "PosId4": 186,
                                    "PosLevel": 3,
                                    "UnitCode": "361",
                                    "TopPosId": 6,
                                    "TopPosCode": "1"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):1 - 011001 - 521,Organisationsenhet(namn och kod):OC Sthlm/Gotland - KS Solna - ÖNH",
                                "id": 566,
                                "data": {
                                    "PosId": 566,
                                    "PosCode": "1 - 011001 - 521",
                                    "PosName": "OC Sthlm/Gotland - KS Solna - ÖNH",
                                    "PosNameWithCode": "OC Sthlm/Gotland (1) - KS Solna (011001) - ÖNH (521)",
                                    "PosId4": 566,
                                    "PosLevel": 3,
                                    "UnitCode": "521",
                                    "TopPosId": 6,
                                    "TopPosCode": "1"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):1 - 097996 - 010,Organisationsenhet(namn och kod):OC Sthlm/Gotland - Lars Rönström - Privatläkare",
                                "id": 1329,
                                "data": {
                                    "PosId": 1329,
                                    "PosCode": "1 - 097996 - 010",
                                    "PosName": "OC Sthlm/Gotland - Lars Rönström - Privatläkare",
                                    "PosNameWithCode": "OC Sthlm/Gotland (1) - Lars Rönström (097996) - Privatläkare (010)",
                                    "PosId4": 1329,
                                    "PosLevel": 3,
                                    "UnitCode": "010",
                                    "TopPosId": 6,
                                    "TopPosCode": "1"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):1 - 097090 - 010,Organisationsenhet(namn och kod):OC Sthlm/Gotland - Lars Stånge - Privatläkare",
                                "id": 1385,
                                "data": {
                                    "PosId": 1385,
                                    "PosCode": "1 - 097090 - 010",
                                    "PosName": "OC Sthlm/Gotland - Lars Stånge - Privatläkare",
                                    "PosNameWithCode": "OC Sthlm/Gotland (1) - Lars Stånge (097090) - Privatläkare (010)",
                                    "PosId4": 1385,
                                    "PosLevel": 3,
                                    "UnitCode": "010",
                                    "TopPosId": 6,
                                    "TopPosCode": "1"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):1 - 090667 - 010,Organisationsenhet(namn och kod):OC Sthlm/Gotland - Lennart Wagrell - Privatläkare",
                                "id": 981,
                                "data": {
                                    "PosId": 981,
                                    "PosCode": "1 - 090667 - 010",
                                    "PosName": "OC Sthlm/Gotland - Lennart Wagrell - Privatläkare",
                                    "PosNameWithCode": "OC Sthlm/Gotland (1) - Lennart Wagrell (090667) - Privatläkare (010)",
                                    "PosId4": 981,
                                    "PosLevel": 3,
                                    "UnitCode": "010",
                                    "TopPosId": 6,
                                    "TopPosCode": "1"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):1 - 010519 - 431,Organisationsenhet(namn och kod):OC Sthlm/Gotland - Läkargruppen Viktoria - Gyn mott",
                                "id": 1273,
                                "data": {
                                    "PosId": 1273,
                                    "PosCode": "1 - 010519 - 431",
                                    "PosName": "OC Sthlm/Gotland - Läkargruppen Viktoria - Gyn mott",
                                    "PosNameWithCode": "OC Sthlm/Gotland (1) - Läkargruppen Viktoria (010519) - Gyn mott (431)",
                                    "PosId4": 1273,
                                    "PosLevel": 3,
                                    "UnitCode": "431",
                                    "TopPosId": 6,
                                    "TopPosCode": "1"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):1 - 011013 - 301,Organisationsenhet(namn och kod):OC Sthlm/Gotland - Löwenströmska sjukhuset - Kirurgkliniken",
                                "id": 472,
                                "data": {
                                    "PosId": 472,
                                    "PosCode": "1 - 011013 - 301",
                                    "PosName": "OC Sthlm/Gotland - Löwenströmska sjukhuset - Kirurgkliniken",
                                    "PosNameWithCode": "OC Sthlm/Gotland (1) - Löwenströmska sjukhuset (011013) - Kirurgkliniken (301)",
                                    "PosId4": 472,
                                    "PosLevel": 3,
                                    "UnitCode": "301",
                                    "TopPosId": 6,
                                    "TopPosCode": "1"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):1 - 011013 - 361,Organisationsenhet(namn och kod):OC Sthlm/Gotland - Löwenströmska sjukhuset - Urologkliniken",
                                "id": 1080,
                                "data": {
                                    "PosId": 1080,
                                    "PosCode": "1 - 011013 - 361",
                                    "PosName": "OC Sthlm/Gotland - Löwenströmska sjukhuset - Urologkliniken",
                                    "PosNameWithCode": "OC Sthlm/Gotland (1) - Löwenströmska sjukhuset (011013) - Urologkliniken (361)",
                                    "PosId4": 1080,
                                    "PosLevel": 3,
                                    "UnitCode": "361",
                                    "TopPosId": 6,
                                    "TopPosCode": "1"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):1 - 011015 - 999,Organisationsenhet(namn och kod):OC Sthlm/Gotland - Löwet Specialistmottagning - Specialistmottagning",
                                "id": 600,
                                "data": {
                                    "PosId": 600,
                                    "PosCode": "1 - 011015 - 999",
                                    "PosName": "OC Sthlm/Gotland - Löwet Specialistmottagning - Specialistmottagning",
                                    "PosNameWithCode": "OC Sthlm/Gotland (1) - Löwet Specialistmottagning (011015) - Specialistmottagning (999)",
                                    "PosId4": 600,
                                    "PosLevel": 3,
                                    "UnitCode": "999",
                                    "TopPosId": 6,
                                    "TopPosCode": "1"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):1 - 030558 - 010,Organisationsenhet(namn och kod):OC Sthlm/Gotland - Magnus Annerstedt - Privatläkare",
                                "id": 1307,
                                "data": {
                                    "PosId": 1307,
                                    "PosCode": "1 - 030558 - 010",
                                    "PosName": "OC Sthlm/Gotland - Magnus Annerstedt - Privatläkare",
                                    "PosNameWithCode": "OC Sthlm/Gotland (1) - Magnus Annerstedt (030558) - Privatläkare (010)",
                                    "PosId4": 1307,
                                    "PosLevel": 3,
                                    "UnitCode": "010",
                                    "TopPosId": 6,
                                    "TopPosCode": "1"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):1 - 099406 - 010,Organisationsenhet(namn och kod):OC Sthlm/Gotland - Magnus Stangenberg - Privatläkare",
                                "id": 1366,
                                "data": {
                                    "PosId": 1366,
                                    "PosCode": "1 - 099406 - 010",
                                    "PosName": "OC Sthlm/Gotland - Magnus Stangenberg - Privatläkare",
                                    "PosNameWithCode": "OC Sthlm/Gotland (1) - Magnus Stangenberg (099406) - Privatläkare (010)",
                                    "PosId4": 1366,
                                    "PosLevel": 3,
                                    "UnitCode": "010",
                                    "TopPosId": 6,
                                    "TopPosCode": "1"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):1 - 090982 - 010,Organisationsenhet(namn och kod):OC Sthlm/Gotland - Magnus Törnblom - Privatläkare",
                                "id": 995,
                                "data": {
                                    "PosId": 995,
                                    "PosCode": "1 - 090982 - 010",
                                    "PosName": "OC Sthlm/Gotland - Magnus Törnblom - Privatläkare",
                                    "PosNameWithCode": "OC Sthlm/Gotland (1) - Magnus Törnblom (090982) - Privatläkare (010)",
                                    "PosId4": 995,
                                    "PosLevel": 3,
                                    "UnitCode": "010",
                                    "TopPosId": 6,
                                    "TopPosCode": "1"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):1 - 011014 - 431,Organisationsenhet(namn och kod):OC Sthlm/Gotland - Nacka Närsjh Proxima - Gynmottagning",
                                "id": 1180,
                                "data": {
                                    "PosId": 1180,
                                    "PosCode": "1 - 011014 - 431",
                                    "PosName": "OC Sthlm/Gotland - Nacka Närsjh Proxima - Gynmottagning",
                                    "PosNameWithCode": "OC Sthlm/Gotland (1) - Nacka Närsjh Proxima (011014) - Gynmottagning (431)",
                                    "PosId4": 1180,
                                    "PosLevel": 3,
                                    "UnitCode": "431",
                                    "TopPosId": 6,
                                    "TopPosCode": "1"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):1 - 011014 - 211,Organisationsenhet(namn och kod):OC Sthlm/Gotland - Nacka Närsjh Proxima - Hudkliniken",
                                "id": 991,
                                "data": {
                                    "PosId": 991,
                                    "PosCode": "1 - 011014 - 211",
                                    "PosName": "OC Sthlm/Gotland - Nacka Närsjh Proxima - Hudkliniken",
                                    "PosNameWithCode": "OC Sthlm/Gotland (1) - Nacka Närsjh Proxima (011014) - Hudkliniken (211)",
                                    "PosId4": 991,
                                    "PosLevel": 3,
                                    "UnitCode": "211",
                                    "TopPosId": 6,
                                    "TopPosCode": "1"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):1 - 011014 - 301,Organisationsenhet(namn och kod):OC Sthlm/Gotland - Nacka Närsjh Proxima - Kirurgkliniken",
                                "id": 470,
                                "data": {
                                    "PosId": 470,
                                    "PosCode": "1 - 011014 - 301",
                                    "PosName": "OC Sthlm/Gotland - Nacka Närsjh Proxima - Kirurgkliniken",
                                    "PosNameWithCode": "OC Sthlm/Gotland (1) - Nacka Närsjh Proxima (011014) - Kirurgkliniken (301)",
                                    "PosId4": 470,
                                    "PosLevel": 3,
                                    "UnitCode": "301",
                                    "TopPosId": 6,
                                    "TopPosCode": "1"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):1 - 011014 - 361,Organisationsenhet(namn och kod):OC Sthlm/Gotland - Nacka Närsjh Proxima - Urologkliniken",
                                "id": 1284,
                                "data": {
                                    "PosId": 1284,
                                    "PosCode": "1 - 011014 - 361",
                                    "PosName": "OC Sthlm/Gotland - Nacka Närsjh Proxima - Urologkliniken",
                                    "PosNameWithCode": "OC Sthlm/Gotland (1) - Nacka Närsjh Proxima (011014) - Urologkliniken (361)",
                                    "PosId4": 1284,
                                    "PosLevel": 3,
                                    "UnitCode": "361",
                                    "TopPosId": 6,
                                    "TopPosCode": "1"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):1 - 099871 - 010,Organisationsenhet(namn och kod):OC Sthlm/Gotland - Ninna Robeus/ Mörby C - Privatläkare",
                                "id": 1221,
                                "data": {
                                    "PosId": 1221,
                                    "PosCode": "1 - 099871 - 010",
                                    "PosName": "OC Sthlm/Gotland - Ninna Robeus/ Mörby C - Privatläkare",
                                    "PosNameWithCode": "OC Sthlm/Gotland (1) - Ninna Robeus/ Mörby C (099871) - Privatläkare (010)",
                                    "PosId4": 1221,
                                    "PosLevel": 3,
                                    "UnitCode": "010",
                                    "TopPosId": 6,
                                    "TopPosCode": "1"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):1 - 015141 - 011,Organisationsenhet(namn och kod):OC Sthlm/Gotland - Norrtälje norra vårdcentralen - Vårdcentral",
                                "id": 1035,
                                "data": {
                                    "PosId": 1035,
                                    "PosCode": "1 - 015141 - 011",
                                    "PosName": "OC Sthlm/Gotland - Norrtälje norra vårdcentralen - Vårdcentral",
                                    "PosNameWithCode": "OC Sthlm/Gotland (1) - Norrtälje norra vårdcentralen (015141) - Vårdcentral (011)",
                                    "PosId4": 1035,
                                    "PosLevel": 3,
                                    "UnitCode": "011",
                                    "TopPosId": 6,
                                    "TopPosCode": "1"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):1 - 011012 - 211,Organisationsenhet(namn och kod):OC Sthlm/Gotland - Norrtälje sjukhus - Hudkliniken",
                                "id": 1065,
                                "data": {
                                    "PosId": 1065,
                                    "PosCode": "1 - 011012 - 211",
                                    "PosName": "OC Sthlm/Gotland - Norrtälje sjukhus - Hudkliniken",
                                    "PosNameWithCode": "OC Sthlm/Gotland (1) - Norrtälje sjukhus (011012) - Hudkliniken (211)",
                                    "PosId4": 1065,
                                    "PosLevel": 3,
                                    "UnitCode": "211",
                                    "TopPosId": 6,
                                    "TopPosCode": "1"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):1 - 011012 - 301,Organisationsenhet(namn och kod):OC Sthlm/Gotland - Norrtälje sjukhus - kirurgkliniken",
                                "id": 321,
                                "data": {
                                    "PosId": 321,
                                    "PosCode": "1 - 011012 - 301",
                                    "PosName": "OC Sthlm/Gotland - Norrtälje sjukhus - kirurgkliniken",
                                    "PosNameWithCode": "OC Sthlm/Gotland (1) - Norrtälje sjukhus (011012) - kirurgkliniken (301)",
                                    "PosId4": 321,
                                    "PosLevel": 3,
                                    "UnitCode": "301",
                                    "TopPosId": 6,
                                    "TopPosCode": "1"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):1 - 011012 - 451,Organisationsenhet(namn och kod):OC Sthlm/Gotland - Norrtälje sjukhus - Kvinnoklinik",
                                "id": 614,
                                "data": {
                                    "PosId": 614,
                                    "PosCode": "1 - 011012 - 451",
                                    "PosName": "OC Sthlm/Gotland - Norrtälje sjukhus - Kvinnoklinik",
                                    "PosNameWithCode": "OC Sthlm/Gotland (1) - Norrtälje sjukhus (011012) - Kvinnoklinik (451)",
                                    "PosId4": 614,
                                    "PosLevel": 3,
                                    "UnitCode": "451",
                                    "TopPosId": 6,
                                    "TopPosCode": "1"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):1 - 011012 - 101,Organisationsenhet(namn och kod):OC Sthlm/Gotland - Norrtälje sjukhus - Medicinkliniken",
                                "id": 1016,
                                "data": {
                                    "PosId": 1016,
                                    "PosCode": "1 - 011012 - 101",
                                    "PosName": "OC Sthlm/Gotland - Norrtälje sjukhus - Medicinkliniken",
                                    "PosNameWithCode": "OC Sthlm/Gotland (1) - Norrtälje sjukhus (011012) - Medicinkliniken (101)",
                                    "PosId4": 1016,
                                    "PosLevel": 3,
                                    "UnitCode": "101",
                                    "TopPosId": 6,
                                    "TopPosCode": "1"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):1 - 011012 - 731,Organisationsenhet(namn och kod):OC Sthlm/Gotland - Norrtälje sjukhus - Röntgenmottagning",
                                "id": 1471,
                                "data": {
                                    "PosId": 1471,
                                    "PosCode": "1 - 011012 - 731",
                                    "PosName": "OC Sthlm/Gotland - Norrtälje sjukhus - Röntgenmottagning",
                                    "PosNameWithCode": "OC Sthlm/Gotland (1) - Norrtälje sjukhus (011012) - Röntgenmottagning (731)",
                                    "PosId4": 1471,
                                    "PosLevel": 3,
                                    "UnitCode": "731",
                                    "TopPosId": 6,
                                    "TopPosCode": "1"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):1 - 090414 - 010,Organisationsenhet(namn och kod):OC Sthlm/Gotland - Octaviakliniken - Privatläkare",
                                "id": 1227,
                                "data": {
                                    "PosId": 1227,
                                    "PosCode": "1 - 090414 - 010",
                                    "PosName": "OC Sthlm/Gotland - Octaviakliniken - Privatläkare",
                                    "PosNameWithCode": "OC Sthlm/Gotland (1) - Octaviakliniken (090414) - Privatläkare (010)",
                                    "PosId4": 1227,
                                    "PosLevel": 3,
                                    "UnitCode": "010",
                                    "TopPosId": 6,
                                    "TopPosCode": "1"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):1 - 097371 - 211,Organisationsenhet(namn och kod):OC Sthlm/Gotland - Odenplans läkarhus - Hudklinik",
                                "id": 1494,
                                "data": {
                                    "PosId": 1494,
                                    "PosCode": "1 - 097371 - 211",
                                    "PosName": "OC Sthlm/Gotland - Odenplans läkarhus - Hudklinik",
                                    "PosNameWithCode": "OC Sthlm/Gotland (1) - Odenplans läkarhus (097371) - Hudklinik (211)",
                                    "PosId4": 1494,
                                    "PosLevel": 3,
                                    "UnitCode": "211",
                                    "TopPosId": 6,
                                    "TopPosCode": "1"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):1 - 097371 - 431,Organisationsenhet(namn och kod):OC Sthlm/Gotland - Odenplans läkarhus - Ultragyn",
                                "id": 1129,
                                "data": {
                                    "PosId": 1129,
                                    "PosCode": "1 - 097371 - 431",
                                    "PosName": "OC Sthlm/Gotland - Odenplans läkarhus - Ultragyn",
                                    "PosNameWithCode": "OC Sthlm/Gotland (1) - Odenplans läkarhus (097371) - Ultragyn (431)",
                                    "PosId4": 1129,
                                    "PosLevel": 3,
                                    "UnitCode": "431",
                                    "TopPosId": 6,
                                    "TopPosCode": "1"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):1 - 090518 - 010,Organisationsenhet(namn och kod):OC Sthlm/Gotland - Olof Jansson - Privatläkare",
                                "id": 964,
                                "data": {
                                    "PosId": 964,
                                    "PosCode": "1 - 090518 - 010",
                                    "PosName": "OC Sthlm/Gotland - Olof Jansson - Privatläkare",
                                    "PosNameWithCode": "OC Sthlm/Gotland (1) - Olof Jansson (090518) - Privatläkare (010)",
                                    "PosId4": 964,
                                    "PosLevel": 3,
                                    "UnitCode": "010",
                                    "TopPosId": 6,
                                    "TopPosCode": "1"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):1 - 013199 - 431,Organisationsenhet(namn och kod):OC Sthlm/Gotland - Oxbackskliniken - Gynmottagning",
                                "id": 1245,
                                "data": {
                                    "PosId": 1245,
                                    "PosCode": "1 - 013199 - 431",
                                    "PosName": "OC Sthlm/Gotland - Oxbackskliniken - Gynmottagning",
                                    "PosNameWithCode": "OC Sthlm/Gotland (1) - Oxbackskliniken (013199) - Gynmottagning (431)",
                                    "PosId4": 1245,
                                    "PosLevel": 3,
                                    "UnitCode": "431",
                                    "TopPosId": 6,
                                    "TopPosCode": "1"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):1 - 098376 - 010,Organisationsenhet(namn och kod):OC Sthlm/Gotland - Per-Olov Boija - Privatläkare",
                                "id": 1333,
                                "data": {
                                    "PosId": 1333,
                                    "PosCode": "1 - 098376 - 010",
                                    "PosName": "OC Sthlm/Gotland - Per-Olov Boija - Privatläkare",
                                    "PosNameWithCode": "OC Sthlm/Gotland (1) - Per-Olov Boija (098376) - Privatläkare (010)",
                                    "PosId4": 1333,
                                    "PosLevel": 3,
                                    "UnitCode": "010",
                                    "TopPosId": 6,
                                    "TopPosCode": "1"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):1 - 097229 - 010,Organisationsenhet(namn och kod):OC Sthlm/Gotland - Peter Gisslén - Privatläkare",
                                "id": 1392,
                                "data": {
                                    "PosId": 1392,
                                    "PosCode": "1 - 097229 - 010",
                                    "PosName": "OC Sthlm/Gotland - Peter Gisslén - Privatläkare",
                                    "PosNameWithCode": "OC Sthlm/Gotland (1) - Peter Gisslén (097229) - Privatläkare (010)",
                                    "PosId4": 1392,
                                    "PosLevel": 3,
                                    "UnitCode": "010",
                                    "TopPosId": 6,
                                    "TopPosCode": "1"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):1 -  - 999,Organisationsenhet(namn och kod):OC Sthlm/Gotland - Privata sjukhus - Okänd klinik",
                                "id": 436,
                                "data": {
                                    "PosId": 436,
                                    "PosCode": "1 -  - 999",
                                    "PosName": "OC Sthlm/Gotland - Privata sjukhus - Okänd klinik",
                                    "PosNameWithCode": "OC Sthlm/Gotland (1) - Privata sjukhus () - Okänd klinik (999)",
                                    "PosId4": 436,
                                    "PosLevel": 3,
                                    "UnitCode": "999",
                                    "TopPosId": 6,
                                    "TopPosCode": "1"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):1 - 011199 - 999,Organisationsenhet(namn och kod):OC Sthlm/Gotland - Privatläkare - Okänd klinik",
                                "id": 434,
                                "data": {
                                    "PosId": 434,
                                    "PosCode": "1 - 011199 - 999",
                                    "PosName": "OC Sthlm/Gotland - Privatläkare - Okänd klinik",
                                    "PosNameWithCode": "OC Sthlm/Gotland (1) - Privatläkare (011199) - Okänd klinik (999)",
                                    "PosId4": 434,
                                    "PosLevel": 3,
                                    "UnitCode": "999",
                                    "TopPosId": 6,
                                    "TopPosCode": "1"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):1 - 030401 - 010,Organisationsenhet(namn och kod):OC Sthlm/Gotland - Rajne Söderberg - Privatläkare",
                                "id": 1351,
                                "data": {
                                    "PosId": 1351,
                                    "PosCode": "1 - 030401 - 010",
                                    "PosName": "OC Sthlm/Gotland - Rajne Söderberg - Privatläkare",
                                    "PosNameWithCode": "OC Sthlm/Gotland (1) - Rajne Söderberg (030401) - Privatläkare (010)",
                                    "PosId4": 1351,
                                    "PosLevel": 3,
                                    "UnitCode": "010",
                                    "TopPosId": 6,
                                    "TopPosCode": "1"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):1 - 010306 - 240,Organisationsenhet(namn och kod):OC Sthlm/Gotland - Rosenlunds sjukhus - Geriatrik",
                                "id": 1042,
                                "data": {
                                    "PosId": 1042,
                                    "PosCode": "1 - 010306 - 240",
                                    "PosName": "OC Sthlm/Gotland - Rosenlunds sjukhus - Geriatrik",
                                    "PosNameWithCode": "OC Sthlm/Gotland (1) - Rosenlunds sjukhus (010306) - Geriatrik (240)",
                                    "PosId4": 1042,
                                    "PosLevel": 3,
                                    "UnitCode": "240",
                                    "TopPosId": 6,
                                    "TopPosCode": "1"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):1 - 010012 - 211,Organisationsenhet(namn och kod):OC Sthlm/Gotland - S:t Eriks ögonsjukhus - Hudkliniken",
                                "id": 1411,
                                "data": {
                                    "PosId": 1411,
                                    "PosCode": "1 - 010012 - 211",
                                    "PosName": "OC Sthlm/Gotland - S:t Eriks ögonsjukhus - Hudkliniken",
                                    "PosNameWithCode": "OC Sthlm/Gotland (1) - S:t Eriks ögonsjukhus (010012) - Hudkliniken (211)",
                                    "PosId4": 1411,
                                    "PosLevel": 3,
                                    "UnitCode": "211",
                                    "TopPosId": 6,
                                    "TopPosCode": "1"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):1 - 010011 - 301,Organisationsenhet(namn och kod):OC Sthlm/Gotland - S:t Görans sjukhus - Kirurgkliniken",
                                "id": 23,
                                "data": {
                                    "PosId": 23,
                                    "PosCode": "1 - 010011 - 301",
                                    "PosName": "OC Sthlm/Gotland - S:t Görans sjukhus - Kirurgkliniken",
                                    "PosNameWithCode": "OC Sthlm/Gotland (1) - S:t Görans sjukhus (010011) - Kirurgkliniken (301)",
                                    "PosId4": 23,
                                    "PosLevel": 3,
                                    "UnitCode": "301",
                                    "TopPosId": 6,
                                    "TopPosCode": "1"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):1 - 010011 - 101,Organisationsenhet(namn och kod):OC Sthlm/Gotland - S:t Görans sjukhus - Medicinkliniken",
                                "id": 424,
                                "data": {
                                    "PosId": 424,
                                    "PosCode": "1 - 010011 - 101",
                                    "PosName": "OC Sthlm/Gotland - S:t Görans sjukhus - Medicinkliniken",
                                    "PosNameWithCode": "OC Sthlm/Gotland (1) - S:t Görans sjukhus (010011) - Medicinkliniken (101)",
                                    "PosId4": 424,
                                    "PosLevel": 3,
                                    "UnitCode": "101",
                                    "TopPosId": 6,
                                    "TopPosCode": "1"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):1 - 010011 - 741,Organisationsenhet(namn och kod):OC Sthlm/Gotland - S:t Görans sjukhus - Onkologkliniken",
                                "id": 16824,
                                "data": {
                                    "PosId": 16824,
                                    "PosCode": "1 - 010011 - 741",
                                    "PosName": "OC Sthlm/Gotland - S:t Görans sjukhus - Onkologkliniken",
                                    "PosNameWithCode": "OC Sthlm/Gotland (1) - S:t Görans sjukhus (010011) - Onkologkliniken (741)",
                                    "PosId4": 16824,
                                    "PosLevel": 3,
                                    "UnitCode": "741",
                                    "TopPosId": 6,
                                    "TopPosCode": "1"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):1 - 010011 - 311,Organisationsenhet(namn och kod):OC Sthlm/Gotland - S:t Görans sjukhus - Ortopedkirurg",
                                "id": 1007,
                                "data": {
                                    "PosId": 1007,
                                    "PosCode": "1 - 010011 - 311",
                                    "PosName": "OC Sthlm/Gotland - S:t Görans sjukhus - Ortopedkirurg",
                                    "PosNameWithCode": "OC Sthlm/Gotland (1) - S:t Görans sjukhus (010011) - Ortopedkirurg (311)",
                                    "PosId4": 1007,
                                    "PosLevel": 3,
                                    "UnitCode": "311",
                                    "TopPosId": 6,
                                    "TopPosCode": "1"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):1 - 010011 - 731,Organisationsenhet(namn och kod):OC Sthlm/Gotland - S:t Görans sjukhus - Röntgenmottagning",
                                "id": 1472,
                                "data": {
                                    "PosId": 1472,
                                    "PosCode": "1 - 010011 - 731",
                                    "PosName": "OC Sthlm/Gotland - S:t Görans sjukhus - Röntgenmottagning",
                                    "PosNameWithCode": "OC Sthlm/Gotland (1) - S:t Görans sjukhus (010011) - Röntgenmottagning (731)",
                                    "PosId4": 1472,
                                    "PosLevel": 3,
                                    "UnitCode": "731",
                                    "TopPosId": 6,
                                    "TopPosCode": "1"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):1 - 010011 - 361,Organisationsenhet(namn och kod):OC Sthlm/Gotland - S:t Görans sjukhus - Urologkliniken",
                                "id": 534,
                                "data": {
                                    "PosId": 534,
                                    "PosCode": "1 - 010011 - 361",
                                    "PosName": "OC Sthlm/Gotland - S:t Görans sjukhus - Urologkliniken",
                                    "PosNameWithCode": "OC Sthlm/Gotland (1) - S:t Görans sjukhus (010011) - Urologkliniken (361)",
                                    "PosId4": 534,
                                    "PosLevel": 3,
                                    "UnitCode": "361",
                                    "TopPosId": 6,
                                    "TopPosCode": "1"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):1 - 010011 - 362,Organisationsenhet(namn och kod):OC Sthlm/Gotland - S:t Görans sjukhus - Urologsektionen kirklin",
                                "id": 955,
                                "data": {
                                    "PosId": 955,
                                    "PosCode": "1 - 010011 - 362",
                                    "PosName": "OC Sthlm/Gotland - S:t Görans sjukhus - Urologsektionen kirklin",
                                    "PosNameWithCode": "OC Sthlm/Gotland (1) - S:t Görans sjukhus (010011) - Urologsektionen kirklin (362)",
                                    "PosId4": 955,
                                    "PosLevel": 3,
                                    "UnitCode": "362",
                                    "TopPosId": 6,
                                    "TopPosCode": "1"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):1 - 010484 - 301,Organisationsenhet(namn och kod):OC Sthlm/Gotland - Sabbsberg närsjukhus - Kirurgkliniken",
                                "id": 465,
                                "data": {
                                    "PosId": 465,
                                    "PosCode": "1 - 010484 - 301",
                                    "PosName": "OC Sthlm/Gotland - Sabbsberg närsjukhus - Kirurgkliniken",
                                    "PosNameWithCode": "OC Sthlm/Gotland (1) - Sabbsberg närsjukhus (010484) - Kirurgkliniken (301)",
                                    "PosId4": 465,
                                    "PosLevel": 3,
                                    "UnitCode": "301",
                                    "TopPosId": 6,
                                    "TopPosCode": "1"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):1 - 010484 - 010,Organisationsenhet(namn och kod):OC Sthlm/Gotland - Sabbsberg närsjukhus - Privatläkare",
                                "id": 1071,
                                "data": {
                                    "PosId": 1071,
                                    "PosCode": "1 - 010484 - 010",
                                    "PosName": "OC Sthlm/Gotland - Sabbsberg närsjukhus - Privatläkare",
                                    "PosNameWithCode": "OC Sthlm/Gotland (1) - Sabbsberg närsjukhus (010484) - Privatläkare (010)",
                                    "PosId4": 1071,
                                    "PosLevel": 3,
                                    "UnitCode": "010",
                                    "TopPosId": 6,
                                    "TopPosCode": "1"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):1 - 010484 - 361,Organisationsenhet(namn och kod):OC Sthlm/Gotland - Sabbsberg närsjukhus - Urologen",
                                "id": 1070,
                                "data": {
                                    "PosId": 1070,
                                    "PosCode": "1 - 010484 - 361",
                                    "PosName": "OC Sthlm/Gotland - Sabbsberg närsjukhus - Urologen",
                                    "PosNameWithCode": "OC Sthlm/Gotland (1) - Sabbsberg närsjukhus (010484) - Urologen (361)",
                                    "PosId4": 1070,
                                    "PosLevel": 3,
                                    "UnitCode": "361",
                                    "TopPosId": 6,
                                    "TopPosCode": "1"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):1 - 090663 - 010,Organisationsenhet(namn och kod):OC Sthlm/Gotland - Said Moazzez - Privatläkare",
                                "id": 1323,
                                "data": {
                                    "PosId": 1323,
                                    "PosCode": "1 - 090663 - 010",
                                    "PosName": "OC Sthlm/Gotland - Said Moazzez - Privatläkare",
                                    "PosNameWithCode": "OC Sthlm/Gotland (1) - Said Moazzez (090663) - Privatläkare (010)",
                                    "PosId4": 1323,
                                    "PosLevel": 3,
                                    "UnitCode": "010",
                                    "TopPosId": 6,
                                    "TopPosCode": "1"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):1 - 099578 - 010,Organisationsenhet(namn och kod):OC Sthlm/Gotland - Sam Al-Sabori - Sam Al-Sabori",
                                "id": 1290,
                                "data": {
                                    "PosId": 1290,
                                    "PosCode": "1 - 099578 - 010",
                                    "PosName": "OC Sthlm/Gotland - Sam Al-Sabori - Sam Al-Sabori",
                                    "PosNameWithCode": "OC Sthlm/Gotland (1) - Sam Al-Sabori (099578) - Sam Al-Sabori (010)",
                                    "PosId4": 1290,
                                    "PosLevel": 3,
                                    "UnitCode": "010",
                                    "TopPosId": 6,
                                    "TopPosCode": "1"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):1 - 017268 - 431,Organisationsenhet(namn och kod):OC Sthlm/Gotland - Serafens Specialistmottagning - Gynmottagning",
                                "id": 1362,
                                "data": {
                                    "PosId": 1362,
                                    "PosCode": "1 - 017268 - 431",
                                    "PosName": "OC Sthlm/Gotland - Serafens Specialistmottagning - Gynmottagning",
                                    "PosNameWithCode": "OC Sthlm/Gotland (1) - Serafens Specialistmottagning (017268) - Gynmottagning (431)",
                                    "PosId4": 1362,
                                    "PosLevel": 3,
                                    "UnitCode": "431",
                                    "TopPosId": 6,
                                    "TopPosCode": "1"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):1 - 017268 - 010,Organisationsenhet(namn och kod):OC Sthlm/Gotland - Serafens Specialistmottagning - Privatläkare",
                                "id": 1170,
                                "data": {
                                    "PosId": 1170,
                                    "PosCode": "1 - 017268 - 010",
                                    "PosName": "OC Sthlm/Gotland - Serafens Specialistmottagning - Privatläkare",
                                    "PosNameWithCode": "OC Sthlm/Gotland (1) - Serafens Specialistmottagning (017268) - Privatläkare (010)",
                                    "PosId4": 1170,
                                    "PosLevel": 3,
                                    "UnitCode": "010",
                                    "TopPosId": 6,
                                    "TopPosCode": "1"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):1 - 016108 - 010,Organisationsenhet(namn och kod):OC Sthlm/Gotland - Sigtuna läkarhus/gyn mott - Privatläkare",
                                "id": 1219,
                                "data": {
                                    "PosId": 1219,
                                    "PosCode": "1 - 016108 - 010",
                                    "PosName": "OC Sthlm/Gotland - Sigtuna läkarhus/gyn mott - Privatläkare",
                                    "PosNameWithCode": "OC Sthlm/Gotland (1) - Sigtuna läkarhus/gyn mott (016108) - Privatläkare (010)",
                                    "PosId4": 1219,
                                    "PosLevel": 3,
                                    "UnitCode": "010",
                                    "TopPosId": 6,
                                    "TopPosCode": "1"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):1 - 098698 - 431,Organisationsenhet(namn och kod):OC Sthlm/Gotland - Skeppsbrons Läkarpraktik - Gynmottagning",
                                "id": 1241,
                                "data": {
                                    "PosId": 1241,
                                    "PosCode": "1 - 098698 - 431",
                                    "PosName": "OC Sthlm/Gotland - Skeppsbrons Läkarpraktik - Gynmottagning",
                                    "PosNameWithCode": "OC Sthlm/Gotland (1) - Skeppsbrons Läkarpraktik (098698) - Gynmottagning (431)",
                                    "PosId4": 1241,
                                    "PosLevel": 3,
                                    "UnitCode": "431",
                                    "TopPosId": 6,
                                    "TopPosCode": "1"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):1 - 097395 - 301,Organisationsenhet(namn och kod):OC Sthlm/Gotland - Skärholmens läkarhuset - Kirurgkliniken",
                                "id": 902,
                                "data": {
                                    "PosId": 902,
                                    "PosCode": "1 - 097395 - 301",
                                    "PosName": "OC Sthlm/Gotland - Skärholmens läkarhuset - Kirurgkliniken",
                                    "PosNameWithCode": "OC Sthlm/Gotland (1) - Skärholmens läkarhuset (097395) - Kirurgkliniken (301)",
                                    "PosId4": 902,
                                    "PosLevel": 3,
                                    "UnitCode": "301",
                                    "TopPosId": 6,
                                    "TopPosCode": "1"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):1 - 010436 - 431,Organisationsenhet(namn och kod):OC Sthlm/Gotland - Skärholmens Specialistmottagning - Gynmottagning",
                                "id": 1370,
                                "data": {
                                    "PosId": 1370,
                                    "PosCode": "1 - 010436 - 431",
                                    "PosName": "OC Sthlm/Gotland - Skärholmens Specialistmottagning - Gynmottagning",
                                    "PosNameWithCode": "OC Sthlm/Gotland (1) - Skärholmens Specialistmottagning (010436) - Gynmottagning (431)",
                                    "PosId4": 1370,
                                    "PosLevel": 3,
                                    "UnitCode": "431",
                                    "TopPosId": 6,
                                    "TopPosCode": "1"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):1 - 017121 - 431,Organisationsenhet(namn och kod):OC Sthlm/Gotland - Slussenkliniken - Gynmottagning",
                                "id": 1265,
                                "data": {
                                    "PosId": 1265,
                                    "PosCode": "1 - 017121 - 431",
                                    "PosName": "OC Sthlm/Gotland - Slussenkliniken - Gynmottagning",
                                    "PosNameWithCode": "OC Sthlm/Gotland (1) - Slussenkliniken (017121) - Gynmottagning (431)",
                                    "PosId4": 1265,
                                    "PosLevel": 3,
                                    "UnitCode": "431",
                                    "TopPosId": 6,
                                    "TopPosCode": "1"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):1 - 010424 - 301,Organisationsenhet(namn och kod):OC Sthlm/Gotland - Sollentuna Specialistklinik - Kirurgikliniken",
                                "id": 1054,
                                "data": {
                                    "PosId": 1054,
                                    "PosCode": "1 - 010424 - 301",
                                    "PosName": "OC Sthlm/Gotland - Sollentuna Specialistklinik - Kirurgikliniken",
                                    "PosNameWithCode": "OC Sthlm/Gotland (1) - Sollentuna Specialistklinik (010424) - Kirurgikliniken (301)",
                                    "PosId4": 1054,
                                    "PosLevel": 3,
                                    "UnitCode": "301",
                                    "TopPosId": 6,
                                    "TopPosCode": "1"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):1 - 010424 - 361,Organisationsenhet(namn och kod):OC Sthlm/Gotland - Sollentuna Specialistklinik - Urologklinik",
                                "id": 1216,
                                "data": {
                                    "PosId": 1216,
                                    "PosCode": "1 - 010424 - 361",
                                    "PosName": "OC Sthlm/Gotland - Sollentuna Specialistklinik - Urologklinik",
                                    "PosNameWithCode": "OC Sthlm/Gotland (1) - Sollentuna Specialistklinik (010424) - Urologklinik (361)",
                                    "PosId4": 1216,
                                    "PosLevel": 3,
                                    "UnitCode": "361",
                                    "TopPosId": 6,
                                    "TopPosCode": "1"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):1 - 010483 - 211,Organisationsenhet(namn och kod):OC Sthlm/Gotland - Sophiahemmet - Hudklinik",
                                "id": 1069,
                                "data": {
                                    "PosId": 1069,
                                    "PosCode": "1 - 010483 - 211",
                                    "PosName": "OC Sthlm/Gotland - Sophiahemmet - Hudklinik",
                                    "PosNameWithCode": "OC Sthlm/Gotland (1) - Sophiahemmet (010483) - Hudklinik (211)",
                                    "PosId4": 1069,
                                    "PosLevel": 3,
                                    "UnitCode": "211",
                                    "TopPosId": 6,
                                    "TopPosCode": "1"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):1 - 010483 - 301,Organisationsenhet(namn och kod):OC Sthlm/Gotland - Sophiahemmet - Kirurgkliniken",
                                "id": 474,
                                "data": {
                                    "PosId": 474,
                                    "PosCode": "1 - 010483 - 301",
                                    "PosName": "OC Sthlm/Gotland - Sophiahemmet - Kirurgkliniken",
                                    "PosNameWithCode": "OC Sthlm/Gotland (1) - Sophiahemmet (010483) - Kirurgkliniken (301)",
                                    "PosId4": 474,
                                    "PosLevel": 3,
                                    "UnitCode": "301",
                                    "TopPosId": 6,
                                    "TopPosCode": "1"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):1 - 010483 - 101,Organisationsenhet(namn och kod):OC Sthlm/Gotland - Sophiahemmet - Medicinkliniken",
                                "id": 1130,
                                "data": {
                                    "PosId": 1130,
                                    "PosCode": "1 - 010483 - 101",
                                    "PosName": "OC Sthlm/Gotland - Sophiahemmet - Medicinkliniken",
                                    "PosNameWithCode": "OC Sthlm/Gotland (1) - Sophiahemmet (010483) - Medicinkliniken (101)",
                                    "PosId4": 1130,
                                    "PosLevel": 3,
                                    "UnitCode": "101",
                                    "TopPosId": 6,
                                    "TopPosCode": "1"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):1 - 010483 - 361,Organisationsenhet(namn och kod):OC Sthlm/Gotland - Sophiahemmet - Urologen",
                                "id": 1068,
                                "data": {
                                    "PosId": 1068,
                                    "PosCode": "1 - 010483 - 361",
                                    "PosName": "OC Sthlm/Gotland - Sophiahemmet - Urologen",
                                    "PosNameWithCode": "OC Sthlm/Gotland (1) - Sophiahemmet (010483) - Urologen (361)",
                                    "PosId4": 1068,
                                    "PosLevel": 3,
                                    "UnitCode": "361",
                                    "TopPosId": 6,
                                    "TopPosCode": "1"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):1 - 095099 - 010,Organisationsenhet(namn och kod):OC Sthlm/Gotland - Stadions läkarmottagning - Privatläkare",
                                "id": 1383,
                                "data": {
                                    "PosId": 1383,
                                    "PosCode": "1 - 095099 - 010",
                                    "PosName": "OC Sthlm/Gotland - Stadions läkarmottagning - Privatläkare",
                                    "PosNameWithCode": "OC Sthlm/Gotland (1) - Stadions läkarmottagning (095099) - Privatläkare (010)",
                                    "PosId4": 1383,
                                    "PosLevel": 3,
                                    "UnitCode": "010",
                                    "TopPosId": 6,
                                    "TopPosCode": "1"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):1 - 030379 - 010,Organisationsenhet(namn och kod):OC Sthlm/Gotland - Staffan Gedda - Privatläkare",
                                "id": 1321,
                                "data": {
                                    "PosId": 1321,
                                    "PosCode": "1 - 030379 - 010",
                                    "PosName": "OC Sthlm/Gotland - Staffan Gedda - Privatläkare",
                                    "PosNameWithCode": "OC Sthlm/Gotland (1) - Staffan Gedda (030379) - Privatläkare (010)",
                                    "PosId4": 1321,
                                    "PosLevel": 3,
                                    "UnitCode": "010",
                                    "TopPosId": 6,
                                    "TopPosCode": "1"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):1 - 030716 - 010,Organisationsenhet(namn och kod):OC Sthlm/Gotland - Stefan Knutsson - Privatläkare",
                                "id": 1553,
                                "data": {
                                    "PosId": 1553,
                                    "PosCode": "1 - 030716 - 010",
                                    "PosName": "OC Sthlm/Gotland - Stefan Knutsson - Privatläkare",
                                    "PosNameWithCode": "OC Sthlm/Gotland (1) - Stefan Knutsson (030716) - Privatläkare (010)",
                                    "PosId4": 1553,
                                    "PosLevel": 3,
                                    "UnitCode": "010",
                                    "TopPosId": 6,
                                    "TopPosCode": "1"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):1 - 010488 - 010,Organisationsenhet(namn och kod):OC Sthlm/Gotland - Stockholms UroGyn mottagning - Privatläkare",
                                "id": 1305,
                                "data": {
                                    "PosId": 1305,
                                    "PosCode": "1 - 010488 - 010",
                                    "PosName": "OC Sthlm/Gotland - Stockholms UroGyn mottagning - Privatläkare",
                                    "PosNameWithCode": "OC Sthlm/Gotland (1) - Stockholms UroGyn mottagning (010488) - Privatläkare (010)",
                                    "PosId4": 1305,
                                    "PosLevel": 3,
                                    "UnitCode": "010",
                                    "TopPosId": 6,
                                    "TopPosCode": "1"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):1 - 030055 - 010,Organisationsenhet(namn och kod):OC Sthlm/Gotland - Süleyman Köksal - Privatläkare",
                                "id": 1312,
                                "data": {
                                    "PosId": 1312,
                                    "PosCode": "1 - 030055 - 010",
                                    "PosName": "OC Sthlm/Gotland - Süleyman Köksal - Privatläkare",
                                    "PosNameWithCode": "OC Sthlm/Gotland (1) - Süleyman Köksal (030055) - Privatläkare (010)",
                                    "PosId4": 1312,
                                    "PosLevel": 3,
                                    "UnitCode": "010",
                                    "TopPosId": 6,
                                    "TopPosCode": "1"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):1 - 090412 - 211,Organisationsenhet(namn och kod):OC Sthlm/Gotland - Södermalms läkarhus - Hudmottagning",
                                "id": 1251,
                                "data": {
                                    "PosId": 1251,
                                    "PosCode": "1 - 090412 - 211",
                                    "PosName": "OC Sthlm/Gotland - Södermalms läkarhus - Hudmottagning",
                                    "PosNameWithCode": "OC Sthlm/Gotland (1) - Södermalms läkarhus (090412) - Hudmottagning (211)",
                                    "PosId4": 1251,
                                    "PosLevel": 3,
                                    "UnitCode": "211",
                                    "TopPosId": 6,
                                    "TopPosCode": "1"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):1 - 010013 - 161,Organisationsenhet(namn och kod):OC Sthlm/Gotland - Södersjukhuset - Endokrinologen",
                                "id": 1546,
                                "data": {
                                    "PosId": 1546,
                                    "PosCode": "1 - 010013 - 161",
                                    "PosName": "OC Sthlm/Gotland - Södersjukhuset - Endokrinologen",
                                    "PosNameWithCode": "OC Sthlm/Gotland (1) - Södersjukhuset (010013) - Endokrinologen (161)",
                                    "PosId4": 1546,
                                    "PosLevel": 3,
                                    "UnitCode": "161",
                                    "TopPosId": 6,
                                    "TopPosCode": "1"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):1 - 010013 - 107,Organisationsenhet(namn och kod):OC Sthlm/Gotland - Södersjukhuset - Hematologkliniken",
                                "id": 425,
                                "data": {
                                    "PosId": 425,
                                    "PosCode": "1 - 010013 - 107",
                                    "PosName": "OC Sthlm/Gotland - Södersjukhuset - Hematologkliniken",
                                    "PosNameWithCode": "OC Sthlm/Gotland (1) - Södersjukhuset (010013) - Hematologkliniken (107)",
                                    "PosId4": 425,
                                    "PosLevel": 3,
                                    "UnitCode": "107",
                                    "TopPosId": 6,
                                    "TopPosCode": "1"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):1 - 010013 - 211,Organisationsenhet(namn och kod):OC Sthlm/Gotland - Södersjukhuset - Hudkliniken",
                                "id": 786,
                                "data": {
                                    "PosId": 786,
                                    "PosCode": "1 - 010013 - 211",
                                    "PosName": "OC Sthlm/Gotland - Södersjukhuset - Hudkliniken",
                                    "PosNameWithCode": "OC Sthlm/Gotland (1) - Södersjukhuset (010013) - Hudkliniken (211)",
                                    "PosId4": 786,
                                    "PosLevel": 3,
                                    "UnitCode": "211",
                                    "TopPosId": 6,
                                    "TopPosCode": "1"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):1 - 010013 - 301,Organisationsenhet(namn och kod):OC Sthlm/Gotland - Södersjukhuset - Kirurgkliniken",
                                "id": 228,
                                "data": {
                                    "PosId": 228,
                                    "PosCode": "1 - 010013 - 301",
                                    "PosName": "OC Sthlm/Gotland - Södersjukhuset - Kirurgkliniken",
                                    "PosNameWithCode": "OC Sthlm/Gotland (1) - Södersjukhuset (010013) - Kirurgkliniken (301)",
                                    "PosId4": 228,
                                    "PosLevel": 3,
                                    "UnitCode": "301",
                                    "TopPosId": 6,
                                    "TopPosCode": "1"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):1 - 010013 - 451,Organisationsenhet(namn och kod):OC Sthlm/Gotland - Södersjukhuset - Kvinnoklinik",
                                "id": 611,
                                "data": {
                                    "PosId": 611,
                                    "PosCode": "1 - 010013 - 451",
                                    "PosName": "OC Sthlm/Gotland - Södersjukhuset - Kvinnoklinik",
                                    "PosNameWithCode": "OC Sthlm/Gotland (1) - Södersjukhuset (010013) - Kvinnoklinik (451)",
                                    "PosId4": 611,
                                    "PosLevel": 3,
                                    "UnitCode": "451",
                                    "TopPosId": 6,
                                    "TopPosCode": "1"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):1 - 010013 - 101,Organisationsenhet(namn och kod):OC Sthlm/Gotland - Södersjukhuset - Medicinkliniken",
                                "id": 983,
                                "data": {
                                    "PosId": 983,
                                    "PosCode": "1 - 010013 - 101",
                                    "PosName": "OC Sthlm/Gotland - Södersjukhuset - Medicinkliniken",
                                    "PosNameWithCode": "OC Sthlm/Gotland (1) - Södersjukhuset (010013) - Medicinkliniken (101)",
                                    "PosId4": 983,
                                    "PosLevel": 3,
                                    "UnitCode": "101",
                                    "TopPosId": 6,
                                    "TopPosCode": "1"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):1 - 010013 - 741,Organisationsenhet(namn och kod):OC Sthlm/Gotland - Södersjukhuset - Onkologkliniken",
                                "id": 118,
                                "data": {
                                    "PosId": 118,
                                    "PosCode": "1 - 010013 - 741",
                                    "PosName": "OC Sthlm/Gotland - Södersjukhuset - Onkologkliniken",
                                    "PosNameWithCode": "OC Sthlm/Gotland (1) - Södersjukhuset (010013) - Onkologkliniken (741)",
                                    "PosId4": 118,
                                    "PosLevel": 3,
                                    "UnitCode": "741",
                                    "TopPosId": 6,
                                    "TopPosCode": "1"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):1 - 010013 - 731,Organisationsenhet(namn och kod):OC Sthlm/Gotland - Södersjukhuset - Röntgenmottagning",
                                "id": 1473,
                                "data": {
                                    "PosId": 1473,
                                    "PosCode": "1 - 010013 - 731",
                                    "PosName": "OC Sthlm/Gotland - Södersjukhuset - Röntgenmottagning",
                                    "PosNameWithCode": "OC Sthlm/Gotland (1) - Södersjukhuset (010013) - Röntgenmottagning (731)",
                                    "PosId4": 1473,
                                    "PosLevel": 3,
                                    "UnitCode": "731",
                                    "TopPosId": 6,
                                    "TopPosCode": "1"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):1 - 010013 - 361,Organisationsenhet(namn och kod):OC Sthlm/Gotland - Södersjukhuset - Urologkliniken",
                                "id": 536,
                                "data": {
                                    "PosId": 536,
                                    "PosCode": "1 - 010013 - 361",
                                    "PosName": "OC Sthlm/Gotland - Södersjukhuset - Urologkliniken",
                                    "PosNameWithCode": "OC Sthlm/Gotland (1) - Södersjukhuset (010013) - Urologkliniken (361)",
                                    "PosId4": 536,
                                    "PosLevel": 3,
                                    "UnitCode": "361",
                                    "TopPosId": 6,
                                    "TopPosCode": "1"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):1 - 011011 - 301,Organisationsenhet(namn och kod):OC Sthlm/Gotland - Södertälje sjukhus - Kirurgkliniken",
                                "id": 323,
                                "data": {
                                    "PosId": 323,
                                    "PosCode": "1 - 011011 - 301",
                                    "PosName": "OC Sthlm/Gotland - Södertälje sjukhus - Kirurgkliniken",
                                    "PosNameWithCode": "OC Sthlm/Gotland (1) - Södertälje sjukhus (011011) - Kirurgkliniken (301)",
                                    "PosId4": 323,
                                    "PosLevel": 3,
                                    "UnitCode": "301",
                                    "TopPosId": 6,
                                    "TopPosCode": "1"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):1 - 011011 - 451,Organisationsenhet(namn och kod):OC Sthlm/Gotland - Södertälje sjukhus - Kvinnoklinik",
                                "id": 610,
                                "data": {
                                    "PosId": 610,
                                    "PosCode": "1 - 011011 - 451",
                                    "PosName": "OC Sthlm/Gotland - Södertälje sjukhus - Kvinnoklinik",
                                    "PosNameWithCode": "OC Sthlm/Gotland (1) - Södertälje sjukhus (011011) - Kvinnoklinik (451)",
                                    "PosId4": 610,
                                    "PosLevel": 3,
                                    "UnitCode": "451",
                                    "TopPosId": 6,
                                    "TopPosCode": "1"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):1 - 011011 - 101,Organisationsenhet(namn och kod):OC Sthlm/Gotland - Södertälje sjukhus - Medicinkliniken",
                                "id": 1001,
                                "data": {
                                    "PosId": 1001,
                                    "PosCode": "1 - 011011 - 101",
                                    "PosName": "OC Sthlm/Gotland - Södertälje sjukhus - Medicinkliniken",
                                    "PosNameWithCode": "OC Sthlm/Gotland (1) - Södertälje sjukhus (011011) - Medicinkliniken (101)",
                                    "PosId4": 1001,
                                    "PosLevel": 3,
                                    "UnitCode": "101",
                                    "TopPosId": 6,
                                    "TopPosCode": "1"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):1 - 011011 - 731,Organisationsenhet(namn och kod):OC Sthlm/Gotland - Södertälje sjukhus - Röntgenmottagning",
                                "id": 1474,
                                "data": {
                                    "PosId": 1474,
                                    "PosCode": "1 - 011011 - 731",
                                    "PosName": "OC Sthlm/Gotland - Södertälje sjukhus - Röntgenmottagning",
                                    "PosNameWithCode": "OC Sthlm/Gotland (1) - Södertälje sjukhus (011011) - Röntgenmottagning (731)",
                                    "PosId4": 1474,
                                    "PosLevel": 3,
                                    "UnitCode": "731",
                                    "TopPosId": 6,
                                    "TopPosCode": "1"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):1 - 011011 - 361,Organisationsenhet(namn och kod):OC Sthlm/Gotland - Södertälje sjukhus - Urologen",
                                "id": 1073,
                                "data": {
                                    "PosId": 1073,
                                    "PosCode": "1 - 011011 - 361",
                                    "PosName": "OC Sthlm/Gotland - Södertälje sjukhus - Urologen",
                                    "PosNameWithCode": "OC Sthlm/Gotland (1) - Södertälje sjukhus (011011) - Urologen (361)",
                                    "PosId4": 1073,
                                    "PosLevel": 3,
                                    "UnitCode": "361",
                                    "TopPosId": 6,
                                    "TopPosCode": "1"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):1 - 010487 - 431,Organisationsenhet(namn och kod):OC Sthlm/Gotland - Täby Närssjukhus - Gyn mott",
                                "id": 1274,
                                "data": {
                                    "PosId": 1274,
                                    "PosCode": "1 - 010487 - 431",
                                    "PosName": "OC Sthlm/Gotland - Täby Närssjukhus - Gyn mott",
                                    "PosNameWithCode": "OC Sthlm/Gotland (1) - Täby Närssjukhus (010487) - Gyn mott (431)",
                                    "PosId4": 1274,
                                    "PosLevel": 3,
                                    "UnitCode": "431",
                                    "TopPosId": 6,
                                    "TopPosCode": "1"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):1 - 010487 - 301,Organisationsenhet(namn och kod):OC Sthlm/Gotland - Täby Närssjukhus - Kirurgkliniken",
                                "id": 476,
                                "data": {
                                    "PosId": 476,
                                    "PosCode": "1 - 010487 - 301",
                                    "PosName": "OC Sthlm/Gotland - Täby Närssjukhus - Kirurgkliniken",
                                    "PosNameWithCode": "OC Sthlm/Gotland (1) - Täby Närssjukhus (010487) - Kirurgkliniken (301)",
                                    "PosId4": 476,
                                    "PosLevel": 3,
                                    "UnitCode": "301",
                                    "TopPosId": 6,
                                    "TopPosCode": "1"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):1 - 010487 - 361,Organisationsenhet(namn och kod):OC Sthlm/Gotland - Täby Närssjukhus - Urologkliniken",
                                "id": 1205,
                                "data": {
                                    "PosId": 1205,
                                    "PosCode": "1 - 010487 - 361",
                                    "PosName": "OC Sthlm/Gotland - Täby Närssjukhus - Urologkliniken",
                                    "PosNameWithCode": "OC Sthlm/Gotland (1) - Täby Närssjukhus (010487) - Urologkliniken (361)",
                                    "PosId4": 1205,
                                    "PosLevel": 3,
                                    "UnitCode": "361",
                                    "TopPosId": 6,
                                    "TopPosCode": "1"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):1 - 098272 - 010,Organisationsenhet(namn och kod):OC Sthlm/Gotland - Ulf Bergerheim - Privatläkare",
                                "id": 1331,
                                "data": {
                                    "PosId": 1331,
                                    "PosCode": "1 - 098272 - 010",
                                    "PosName": "OC Sthlm/Gotland - Ulf Bergerheim - Privatläkare",
                                    "PosNameWithCode": "OC Sthlm/Gotland (1) - Ulf Bergerheim (098272) - Privatläkare (010)",
                                    "PosId4": 1331,
                                    "PosLevel": 3,
                                    "UnitCode": "010",
                                    "TopPosId": 6,
                                    "TopPosCode": "1"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):1 - 190518 - 010,Organisationsenhet(namn och kod):OC Sthlm/Gotland - UroClinic Sophiahemmet - Privatläkare",
                                "id": 1357,
                                "data": {
                                    "PosId": 1357,
                                    "PosCode": "1 - 190518 - 010",
                                    "PosName": "OC Sthlm/Gotland - UroClinic Sophiahemmet - Privatläkare",
                                    "PosNameWithCode": "OC Sthlm/Gotland (1) - UroClinic Sophiahemmet (190518) - Privatläkare (010)",
                                    "PosId4": 1357,
                                    "PosLevel": 3,
                                    "UnitCode": "010",
                                    "TopPosId": 6,
                                    "TopPosCode": "1"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):1 - 011198 - 999,Organisationsenhet(namn och kod):OC Sthlm/Gotland - VC/ Tjänsteläkare - Okänd klinik",
                                "id": 435,
                                "data": {
                                    "PosId": 435,
                                    "PosCode": "1 - 011198 - 999",
                                    "PosName": "OC Sthlm/Gotland - VC/ Tjänsteläkare - Okänd klinik",
                                    "PosNameWithCode": "OC Sthlm/Gotland (1) - VC/ Tjänsteläkare (011198) - Okänd klinik (999)",
                                    "PosId4": 435,
                                    "PosLevel": 3,
                                    "UnitCode": "999",
                                    "TopPosId": 6,
                                    "TopPosCode": "1"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):1 - 016200 - 010,Organisationsenhet(namn och kod):OC Sthlm/Gotland - Veritaskliniken - Privatläkare",
                                "id": 1201,
                                "data": {
                                    "PosId": 1201,
                                    "PosCode": "1 - 016200 - 010",
                                    "PosName": "OC Sthlm/Gotland - Veritaskliniken - Privatläkare",
                                    "PosNameWithCode": "OC Sthlm/Gotland (1) - Veritaskliniken (016200) - Privatläkare (010)",
                                    "PosId4": 1201,
                                    "PosLevel": 3,
                                    "UnitCode": "010",
                                    "TopPosId": 6,
                                    "TopPosCode": "1"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):1 - 026010 - 161,Organisationsenhet(namn och kod):OC Sthlm/Gotland - Visby Lasarett - Endokrinologen",
                                "id": 1547,
                                "data": {
                                    "PosId": 1547,
                                    "PosCode": "1 - 026010 - 161",
                                    "PosName": "OC Sthlm/Gotland - Visby Lasarett - Endokrinologen",
                                    "PosNameWithCode": "OC Sthlm/Gotland (1) - Visby Lasarett (026010) - Endokrinologen (161)",
                                    "PosId4": 1547,
                                    "PosLevel": 3,
                                    "UnitCode": "161",
                                    "TopPosId": 6,
                                    "TopPosCode": "1"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):1 - 026010 - 107,Organisationsenhet(namn och kod):OC Sthlm/Gotland - Visby Lasarett - Hematologkliniken",
                                "id": 427,
                                "data": {
                                    "PosId": 427,
                                    "PosCode": "1 - 026010 - 107",
                                    "PosName": "OC Sthlm/Gotland - Visby Lasarett - Hematologkliniken",
                                    "PosNameWithCode": "OC Sthlm/Gotland (1) - Visby Lasarett (026010) - Hematologkliniken (107)",
                                    "PosId4": 427,
                                    "PosLevel": 3,
                                    "UnitCode": "107",
                                    "TopPosId": 6,
                                    "TopPosCode": "1"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):1 - 026010 - 211,Organisationsenhet(namn och kod):OC Sthlm/Gotland - Visby Lasarett - Hudklinik",
                                "id": 1082,
                                "data": {
                                    "PosId": 1082,
                                    "PosCode": "1 - 026010 - 211",
                                    "PosName": "OC Sthlm/Gotland - Visby Lasarett - Hudklinik",
                                    "PosNameWithCode": "OC Sthlm/Gotland (1) - Visby Lasarett (026010) - Hudklinik (211)",
                                    "PosId4": 1082,
                                    "PosLevel": 3,
                                    "UnitCode": "211",
                                    "TopPosId": 6,
                                    "TopPosCode": "1"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):1 - 026010 - 121,Organisationsenhet(namn och kod):OC Sthlm/Gotland - Visby Lasarett - Infektionskliniken",
                                "id": 651,
                                "data": {
                                    "PosId": 651,
                                    "PosCode": "1 - 026010 - 121",
                                    "PosName": "OC Sthlm/Gotland - Visby Lasarett - Infektionskliniken",
                                    "PosNameWithCode": "OC Sthlm/Gotland (1) - Visby Lasarett (026010) - Infektionskliniken (121)",
                                    "PosId4": 651,
                                    "PosLevel": 3,
                                    "UnitCode": "121",
                                    "TopPosId": 6,
                                    "TopPosCode": "1"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):1 - 026010 - 301,Organisationsenhet(namn och kod):OC Sthlm/Gotland - Visby Lasarett - Kirurgkliniken",
                                "id": 462,
                                "data": {
                                    "PosId": 462,
                                    "PosCode": "1 - 026010 - 301",
                                    "PosName": "OC Sthlm/Gotland - Visby Lasarett - Kirurgkliniken",
                                    "PosNameWithCode": "OC Sthlm/Gotland (1) - Visby Lasarett (026010) - Kirurgkliniken (301)",
                                    "PosId4": 462,
                                    "PosLevel": 3,
                                    "UnitCode": "301",
                                    "TopPosId": 6,
                                    "TopPosCode": "1"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):1 - 026010 - 451,Organisationsenhet(namn och kod):OC Sthlm/Gotland - Visby Lasarett - Kvinnoklinik",
                                "id": 1036,
                                "data": {
                                    "PosId": 1036,
                                    "PosCode": "1 - 026010 - 451",
                                    "PosName": "OC Sthlm/Gotland - Visby Lasarett - Kvinnoklinik",
                                    "PosNameWithCode": "OC Sthlm/Gotland (1) - Visby Lasarett (026010) - Kvinnoklinik (451)",
                                    "PosId4": 1036,
                                    "PosLevel": 3,
                                    "UnitCode": "451",
                                    "TopPosId": 6,
                                    "TopPosCode": "1"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):1 - 026010 - 741,Organisationsenhet(namn och kod):OC Sthlm/Gotland - Visby Lasarett - Onkologiska kliniken",
                                "id": 1259,
                                "data": {
                                    "PosId": 1259,
                                    "PosCode": "1 - 026010 - 741",
                                    "PosName": "OC Sthlm/Gotland - Visby Lasarett - Onkologiska kliniken",
                                    "PosNameWithCode": "OC Sthlm/Gotland (1) - Visby Lasarett (026010) - Onkologiska kliniken (741)",
                                    "PosId4": 1259,
                                    "PosLevel": 3,
                                    "UnitCode": "741",
                                    "TopPosId": 6,
                                    "TopPosCode": "1"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):1 - 026010 - 731,Organisationsenhet(namn och kod):OC Sthlm/Gotland - Visby Lasarett - Röntgenmottagning",
                                "id": 1475,
                                "data": {
                                    "PosId": 1475,
                                    "PosCode": "1 - 026010 - 731",
                                    "PosName": "OC Sthlm/Gotland - Visby Lasarett - Röntgenmottagning",
                                    "PosNameWithCode": "OC Sthlm/Gotland (1) - Visby Lasarett (026010) - Röntgenmottagning (731)",
                                    "PosId4": 1475,
                                    "PosLevel": 3,
                                    "UnitCode": "731",
                                    "TopPosId": 6,
                                    "TopPosCode": "1"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):1 - 026010 - 361,Organisationsenhet(namn och kod):OC Sthlm/Gotland - Visby Lasarett - Urologkliniken",
                                "id": 717,
                                "data": {
                                    "PosId": 717,
                                    "PosCode": "1 - 026010 - 361",
                                    "PosName": "OC Sthlm/Gotland - Visby Lasarett - Urologkliniken",
                                    "PosNameWithCode": "OC Sthlm/Gotland (1) - Visby Lasarett (026010) - Urologkliniken (361)",
                                    "PosId4": 717,
                                    "PosLevel": 3,
                                    "UnitCode": "361",
                                    "TopPosId": 6,
                                    "TopPosCode": "1"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):1 - 026010 - 521,Organisationsenhet(namn och kod):OC Sthlm/Gotland - Visby Lasarett - ÖNH",
                                "id": 915,
                                "data": {
                                    "PosId": 915,
                                    "PosCode": "1 - 026010 - 521",
                                    "PosName": "OC Sthlm/Gotland - Visby Lasarett - ÖNH",
                                    "PosNameWithCode": "OC Sthlm/Gotland (1) - Visby Lasarett (026010) - ÖNH (521)",
                                    "PosId4": 915,
                                    "PosLevel": 3,
                                    "UnitCode": "521",
                                    "TopPosId": 6,
                                    "TopPosCode": "1"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):1 - 095065 - 010,Organisationsenhet(namn och kod):OC Sthlm/Gotland - Ylva Sonesson - Privat läkare",
                                "id": 1263,
                                "data": {
                                    "PosId": 1263,
                                    "PosCode": "1 - 095065 - 010",
                                    "PosName": "OC Sthlm/Gotland - Ylva Sonesson - Privat läkare",
                                    "PosNameWithCode": "OC Sthlm/Gotland (1) - Ylva Sonesson (095065) - Privat läkare (010)",
                                    "PosId4": 1263,
                                    "PosLevel": 3,
                                    "UnitCode": "010",
                                    "TopPosId": 6,
                                    "TopPosCode": "1"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):1 - 011353 - 431,Organisationsenhet(namn och kod):OC Sthlm/Gotland - Åkersberga sjukhus - Gynmottagning",
                                "id": 1361,
                                "data": {
                                    "PosId": 1361,
                                    "PosCode": "1 - 011353 - 431",
                                    "PosName": "OC Sthlm/Gotland - Åkersberga sjukhus - Gynmottagning",
                                    "PosNameWithCode": "OC Sthlm/Gotland (1) - Åkersberga sjukhus (011353) - Gynmottagning (431)",
                                    "PosId4": 1361,
                                    "PosLevel": 3,
                                    "UnitCode": "431",
                                    "TopPosId": 6,
                                    "TopPosCode": "1"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):4 - 27011 - 211,Organisationsenhet(namn och kod):OC Syd - Blekingesjukhuset i Karlshamn - Hudkliniken",
                                "id": 683,
                                "data": {
                                    "PosId": 683,
                                    "PosCode": "4 - 27011 - 211",
                                    "PosName": "OC Syd - Blekingesjukhuset i Karlshamn - Hudkliniken",
                                    "PosNameWithCode": "OC Syd (4) - Blekingesjukhuset i Karlshamn (27011) - Hudkliniken (211)",
                                    "PosId4": 683,
                                    "PosLevel": 3,
                                    "UnitCode": "211",
                                    "TopPosId": 3,
                                    "TopPosCode": "4"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):4 - 27011 - 301,Organisationsenhet(namn och kod):OC Syd - Blekingesjukhuset i Karlshamn - Kirurgiska kliniken",
                                "id": 262,
                                "data": {
                                    "PosId": 262,
                                    "PosCode": "4 - 27011 - 301",
                                    "PosName": "OC Syd - Blekingesjukhuset i Karlshamn - Kirurgiska kliniken",
                                    "PosNameWithCode": "OC Syd (4) - Blekingesjukhuset i Karlshamn (27011) - Kirurgiska kliniken (301)",
                                    "PosId4": 262,
                                    "PosLevel": 3,
                                    "UnitCode": "301",
                                    "TopPosId": 3,
                                    "TopPosCode": "4"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):4 - 27011 - 101,Organisationsenhet(namn och kod):OC Syd - Blekingesjukhuset i Karlshamn - Medicinkliniken",
                                "id": 76,
                                "data": {
                                    "PosId": 76,
                                    "PosCode": "4 - 27011 - 101",
                                    "PosName": "OC Syd - Blekingesjukhuset i Karlshamn - Medicinkliniken",
                                    "PosNameWithCode": "OC Syd (4) - Blekingesjukhuset i Karlshamn (27011) - Medicinkliniken (101)",
                                    "PosId4": 76,
                                    "PosLevel": 3,
                                    "UnitCode": "101",
                                    "TopPosId": 3,
                                    "TopPosCode": "4"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):4 - 27011 - 361,Organisationsenhet(namn och kod):OC Syd - Blekingesjukhuset i Karlshamn - Urologiska kliniken",
                                "id": 796,
                                "data": {
                                    "PosId": 796,
                                    "PosCode": "4 - 27011 - 361",
                                    "PosName": "OC Syd - Blekingesjukhuset i Karlshamn - Urologiska kliniken",
                                    "PosNameWithCode": "OC Syd (4) - Blekingesjukhuset i Karlshamn (27011) - Urologiska kliniken (361)",
                                    "PosId4": 796,
                                    "PosLevel": 3,
                                    "UnitCode": "361",
                                    "TopPosId": 3,
                                    "TopPosCode": "4"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):4 - 27011 - 521,Organisationsenhet(namn och kod):OC Syd - Blekingesjukhuset i Karlshamn - ÖNH",
                                "id": 596,
                                "data": {
                                    "PosId": 596,
                                    "PosCode": "4 - 27011 - 521",
                                    "PosName": "OC Syd - Blekingesjukhuset i Karlshamn - ÖNH",
                                    "PosNameWithCode": "OC Syd (4) - Blekingesjukhuset i Karlshamn (27011) - ÖNH (521)",
                                    "PosId4": 596,
                                    "PosLevel": 3,
                                    "UnitCode": "521",
                                    "TopPosId": 3,
                                    "TopPosCode": "4"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):4 - 27010 - 211,Organisationsenhet(namn och kod):OC Syd - Blekingesjukhuset i Karlskrona - Hudkliniken",
                                "id": 674,
                                "data": {
                                    "PosId": 674,
                                    "PosCode": "4 - 27010 - 211",
                                    "PosName": "OC Syd - Blekingesjukhuset i Karlskrona - Hudkliniken",
                                    "PosNameWithCode": "OC Syd (4) - Blekingesjukhuset i Karlskrona (27010) - Hudkliniken (211)",
                                    "PosId4": 674,
                                    "PosLevel": 3,
                                    "UnitCode": "211",
                                    "TopPosId": 3,
                                    "TopPosCode": "4"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):4 - 27010 - 301,Organisationsenhet(namn och kod):OC Syd - Blekingesjukhuset i Karlskrona - Kirurgiska kliniken",
                                "id": 257,
                                "data": {
                                    "PosId": 257,
                                    "PosCode": "4 - 27010 - 301",
                                    "PosName": "OC Syd - Blekingesjukhuset i Karlskrona - Kirurgiska kliniken",
                                    "PosNameWithCode": "OC Syd (4) - Blekingesjukhuset i Karlskrona (27010) - Kirurgiska kliniken (301)",
                                    "PosId4": 257,
                                    "PosLevel": 3,
                                    "UnitCode": "301",
                                    "TopPosId": 3,
                                    "TopPosCode": "4"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):4 - 27010 - 451,Organisationsenhet(namn och kod):OC Syd - Blekingesjukhuset i Karlskrona - Kvinnoklinik",
                                "id": 727,
                                "data": {
                                    "PosId": 727,
                                    "PosCode": "4 - 27010 - 451",
                                    "PosName": "OC Syd - Blekingesjukhuset i Karlskrona - Kvinnoklinik",
                                    "PosNameWithCode": "OC Syd (4) - Blekingesjukhuset i Karlskrona (27010) - Kvinnoklinik (451)",
                                    "PosId4": 727,
                                    "PosLevel": 3,
                                    "UnitCode": "451",
                                    "TopPosId": 3,
                                    "TopPosCode": "4"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):4 - 27010 - 101,Organisationsenhet(namn och kod):OC Syd - Blekingesjukhuset i Karlskrona - Medicinkliniken",
                                "id": 77,
                                "data": {
                                    "PosId": 77,
                                    "PosCode": "4 - 27010 - 101",
                                    "PosName": "OC Syd - Blekingesjukhuset i Karlskrona - Medicinkliniken",
                                    "PosNameWithCode": "OC Syd (4) - Blekingesjukhuset i Karlskrona (27010) - Medicinkliniken (101)",
                                    "PosId4": 77,
                                    "PosLevel": 3,
                                    "UnitCode": "101",
                                    "TopPosId": 3,
                                    "TopPosCode": "4"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):4 - 27010 - 311,Organisationsenhet(namn och kod):OC Syd - Blekingesjukhuset i Karlskrona - Ortopediska kliniken",
                                "id": 1157,
                                "data": {
                                    "PosId": 1157,
                                    "PosCode": "4 - 27010 - 311",
                                    "PosName": "OC Syd - Blekingesjukhuset i Karlskrona - Ortopediska kliniken",
                                    "PosNameWithCode": "OC Syd (4) - Blekingesjukhuset i Karlskrona (27010) - Ortopediska kliniken (311)",
                                    "PosId4": 1157,
                                    "PosLevel": 3,
                                    "UnitCode": "311",
                                    "TopPosId": 3,
                                    "TopPosCode": "4"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):4 - 27010 - 731,Organisationsenhet(namn och kod):OC Syd - Blekingesjukhuset i Karlskrona - Röntgenavdelningen",
                                "id": 1480,
                                "data": {
                                    "PosId": 1480,
                                    "PosCode": "4 - 27010 - 731",
                                    "PosName": "OC Syd - Blekingesjukhuset i Karlskrona - Röntgenavdelningen",
                                    "PosNameWithCode": "OC Syd (4) - Blekingesjukhuset i Karlskrona (27010) - Röntgenavdelningen (731)",
                                    "PosId4": 1480,
                                    "PosLevel": 3,
                                    "UnitCode": "731",
                                    "TopPosId": 3,
                                    "TopPosCode": "4"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):4 - 27010 - 341,Organisationsenhet(namn och kod):OC Syd - Blekingesjukhuset i Karlskrona - Thorax Klin",
                                "id": 686,
                                "data": {
                                    "PosId": 686,
                                    "PosCode": "4 - 27010 - 341",
                                    "PosName": "OC Syd - Blekingesjukhuset i Karlskrona - Thorax Klin",
                                    "PosNameWithCode": "OC Syd (4) - Blekingesjukhuset i Karlskrona (27010) - Thorax Klin (341)",
                                    "PosId4": 686,
                                    "PosLevel": 3,
                                    "UnitCode": "341",
                                    "TopPosId": 3,
                                    "TopPosCode": "4"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):4 - 27010 - 361,Organisationsenhet(namn och kod):OC Syd - Blekingesjukhuset i Karlskrona - Urologiska kliniken",
                                "id": 790,
                                "data": {
                                    "PosId": 790,
                                    "PosCode": "4 - 27010 - 361",
                                    "PosName": "OC Syd - Blekingesjukhuset i Karlskrona - Urologiska kliniken",
                                    "PosNameWithCode": "OC Syd (4) - Blekingesjukhuset i Karlskrona (27010) - Urologiska kliniken (361)",
                                    "PosId4": 790,
                                    "PosLevel": 3,
                                    "UnitCode": "361",
                                    "TopPosId": 3,
                                    "TopPosCode": "4"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):4 - 27010 - 511,Organisationsenhet(namn och kod):OC Syd - Blekingesjukhuset i Karlskrona - ögon klinik",
                                "id": 1172,
                                "data": {
                                    "PosId": 1172,
                                    "PosCode": "4 - 27010 - 511",
                                    "PosName": "OC Syd - Blekingesjukhuset i Karlskrona - ögon klinik",
                                    "PosNameWithCode": "OC Syd (4) - Blekingesjukhuset i Karlskrona (27010) - ögon klinik (511)",
                                    "PosId4": 1172,
                                    "PosLevel": 3,
                                    "UnitCode": "511",
                                    "TopPosId": 3,
                                    "TopPosCode": "4"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):4 - 27010 - 521,Organisationsenhet(namn och kod):OC Syd - Blekingesjukhuset i Karlskrona - ÖNH",
                                "id": 587,
                                "data": {
                                    "PosId": 587,
                                    "PosCode": "4 - 27010 - 521",
                                    "PosName": "OC Syd - Blekingesjukhuset i Karlskrona - ÖNH",
                                    "PosNameWithCode": "OC Syd (4) - Blekingesjukhuset i Karlskrona (27010) - ÖNH (521)",
                                    "PosId4": 587,
                                    "PosLevel": 3,
                                    "UnitCode": "521",
                                    "TopPosId": 3,
                                    "TopPosCode": "4"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):4 - 23010 - 211,Organisationsenhet(namn och kod):OC Syd - Centrallasarettet i Växjö - Hudkliniken",
                                "id": 672,
                                "data": {
                                    "PosId": 672,
                                    "PosCode": "4 - 23010 - 211",
                                    "PosName": "OC Syd - Centrallasarettet i Växjö - Hudkliniken",
                                    "PosNameWithCode": "OC Syd (4) - Centrallasarettet i Växjö (23010) - Hudkliniken (211)",
                                    "PosId4": 672,
                                    "PosLevel": 3,
                                    "UnitCode": "211",
                                    "TopPosId": 3,
                                    "TopPosCode": "4"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):4 - 23010 - 301,Organisationsenhet(namn och kod):OC Syd - Centrallasarettet i Växjö - Kirurgiska kliniken",
                                "id": 254,
                                "data": {
                                    "PosId": 254,
                                    "PosCode": "4 - 23010 - 301",
                                    "PosName": "OC Syd - Centrallasarettet i Växjö - Kirurgiska kliniken",
                                    "PosNameWithCode": "OC Syd (4) - Centrallasarettet i Växjö (23010) - Kirurgiska kliniken (301)",
                                    "PosId4": 254,
                                    "PosLevel": 3,
                                    "UnitCode": "301",
                                    "TopPosId": 3,
                                    "TopPosCode": "4"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):4 - 23010 - 451,Organisationsenhet(namn och kod):OC Syd - Centrallasarettet i Växjö - Kvinnoklinik",
                                "id": 725,
                                "data": {
                                    "PosId": 725,
                                    "PosCode": "4 - 23010 - 451",
                                    "PosName": "OC Syd - Centrallasarettet i Växjö - Kvinnoklinik",
                                    "PosNameWithCode": "OC Syd (4) - Centrallasarettet i Växjö (23010) - Kvinnoklinik (451)",
                                    "PosId4": 725,
                                    "PosLevel": 3,
                                    "UnitCode": "451",
                                    "TopPosId": 3,
                                    "TopPosCode": "4"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):4 - 23010 - 101,Organisationsenhet(namn och kod):OC Syd - Centrallasarettet i Växjö - Medicinkliniken",
                                "id": 79,
                                "data": {
                                    "PosId": 79,
                                    "PosCode": "4 - 23010 - 101",
                                    "PosName": "OC Syd - Centrallasarettet i Växjö - Medicinkliniken",
                                    "PosNameWithCode": "OC Syd (4) - Centrallasarettet i Växjö (23010) - Medicinkliniken (101)",
                                    "PosId4": 79,
                                    "PosLevel": 3,
                                    "UnitCode": "101",
                                    "TopPosId": 3,
                                    "TopPosCode": "4"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):4 - 23010 - 741,Organisationsenhet(namn och kod):OC Syd - Centrallasarettet i Växjö - Onkologiska kliniken",
                                "id": 311,
                                "data": {
                                    "PosId": 311,
                                    "PosCode": "4 - 23010 - 741",
                                    "PosName": "OC Syd - Centrallasarettet i Växjö - Onkologiska kliniken",
                                    "PosNameWithCode": "OC Syd (4) - Centrallasarettet i Växjö (23010) - Onkologiska kliniken (741)",
                                    "PosId4": 311,
                                    "PosLevel": 3,
                                    "UnitCode": "741",
                                    "TopPosId": 3,
                                    "TopPosCode": "4"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):4 - 23010 - 511,Organisationsenhet(namn och kod):OC Syd - Centrallasarettet i Växjö - ögon klinik",
                                "id": 1171,
                                "data": {
                                    "PosId": 1171,
                                    "PosCode": "4 - 23010 - 511",
                                    "PosName": "OC Syd - Centrallasarettet i Växjö - ögon klinik",
                                    "PosNameWithCode": "OC Syd (4) - Centrallasarettet i Växjö (23010) - ögon klinik (511)",
                                    "PosId4": 1171,
                                    "PosLevel": 3,
                                    "UnitCode": "511",
                                    "TopPosId": 3,
                                    "TopPosCode": "4"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):4 - 23010 - 521,Organisationsenhet(namn och kod):OC Syd - Centrallasarettet i Växjö - ÖNH",
                                "id": 585,
                                "data": {
                                    "PosId": 585,
                                    "PosCode": "4 - 23010 - 521",
                                    "PosName": "OC Syd - Centrallasarettet i Växjö - ÖNH",
                                    "PosNameWithCode": "OC Syd (4) - Centrallasarettet i Växjö (23010) - ÖNH (521)",
                                    "PosId4": 585,
                                    "PosLevel": 3,
                                    "UnitCode": "521",
                                    "TopPosId": 3,
                                    "TopPosCode": "4"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):4 - 28010 - 211,Organisationsenhet(namn och kod):OC Syd - Centralsjukhuset i Kristianstad - Hudkliniken",
                                "id": 675,
                                "data": {
                                    "PosId": 675,
                                    "PosCode": "4 - 28010 - 211",
                                    "PosName": "OC Syd - Centralsjukhuset i Kristianstad - Hudkliniken",
                                    "PosNameWithCode": "OC Syd (4) - Centralsjukhuset i Kristianstad (28010) - Hudkliniken (211)",
                                    "PosId4": 675,
                                    "PosLevel": 3,
                                    "UnitCode": "211",
                                    "TopPosId": 3,
                                    "TopPosCode": "4"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):4 - 28010 - 301,Organisationsenhet(namn och kod):OC Syd - Centralsjukhuset i Kristianstad - Kirurgiska kliniken",
                                "id": 251,
                                "data": {
                                    "PosId": 251,
                                    "PosCode": "4 - 28010 - 301",
                                    "PosName": "OC Syd - Centralsjukhuset i Kristianstad - Kirurgiska kliniken",
                                    "PosNameWithCode": "OC Syd (4) - Centralsjukhuset i Kristianstad (28010) - Kirurgiska kliniken (301)",
                                    "PosId4": 251,
                                    "PosLevel": 3,
                                    "UnitCode": "301",
                                    "TopPosId": 3,
                                    "TopPosCode": "4"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):4 - 28010 - 451,Organisationsenhet(namn och kod):OC Syd - Centralsjukhuset i Kristianstad - Kvinnoklinik",
                                "id": 728,
                                "data": {
                                    "PosId": 728,
                                    "PosCode": "4 - 28010 - 451",
                                    "PosName": "OC Syd - Centralsjukhuset i Kristianstad - Kvinnoklinik",
                                    "PosNameWithCode": "OC Syd (4) - Centralsjukhuset i Kristianstad (28010) - Kvinnoklinik (451)",
                                    "PosId4": 728,
                                    "PosLevel": 3,
                                    "UnitCode": "451",
                                    "TopPosId": 3,
                                    "TopPosCode": "4"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):4 - 28010 - 101,Organisationsenhet(namn och kod):OC Syd - Centralsjukhuset i Kristianstad - Medicinkliniken",
                                "id": 75,
                                "data": {
                                    "PosId": 75,
                                    "PosCode": "4 - 28010 - 101",
                                    "PosName": "OC Syd - Centralsjukhuset i Kristianstad - Medicinkliniken",
                                    "PosNameWithCode": "OC Syd (4) - Centralsjukhuset i Kristianstad (28010) - Medicinkliniken (101)",
                                    "PosId4": 75,
                                    "PosLevel": 3,
                                    "UnitCode": "101",
                                    "TopPosId": 3,
                                    "TopPosCode": "4"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):4 - 28010 - 741,Organisationsenhet(namn och kod):OC Syd - Centralsjukhuset i Kristianstad - Onkologen",
                                "id": 1185,
                                "data": {
                                    "PosId": 1185,
                                    "PosCode": "4 - 28010 - 741",
                                    "PosName": "OC Syd - Centralsjukhuset i Kristianstad - Onkologen",
                                    "PosNameWithCode": "OC Syd (4) - Centralsjukhuset i Kristianstad (28010) - Onkologen (741)",
                                    "PosId4": 1185,
                                    "PosLevel": 3,
                                    "UnitCode": "741",
                                    "TopPosId": 3,
                                    "TopPosCode": "4"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):4 - 28010 - 731,Organisationsenhet(namn och kod):OC Syd - Centralsjukhuset i Kristianstad - Röntgenavdelningen",
                                "id": 1477,
                                "data": {
                                    "PosId": 1477,
                                    "PosCode": "4 - 28010 - 731",
                                    "PosName": "OC Syd - Centralsjukhuset i Kristianstad - Röntgenavdelningen",
                                    "PosNameWithCode": "OC Syd (4) - Centralsjukhuset i Kristianstad (28010) - Röntgenavdelningen (731)",
                                    "PosId4": 1477,
                                    "PosLevel": 3,
                                    "UnitCode": "731",
                                    "TopPosId": 3,
                                    "TopPosCode": "4"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):4 - 28010 - 361,Organisationsenhet(namn och kod):OC Syd - Centralsjukhuset i Kristianstad - Urologiska kliniken",
                                "id": 797,
                                "data": {
                                    "PosId": 797,
                                    "PosCode": "4 - 28010 - 361",
                                    "PosName": "OC Syd - Centralsjukhuset i Kristianstad - Urologiska kliniken",
                                    "PosNameWithCode": "OC Syd (4) - Centralsjukhuset i Kristianstad (28010) - Urologiska kliniken (361)",
                                    "PosId4": 797,
                                    "PosLevel": 3,
                                    "UnitCode": "361",
                                    "TopPosId": 3,
                                    "TopPosCode": "4"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):4 - 28010 - 511,Organisationsenhet(namn och kod):OC Syd - Centralsjukhuset i Kristianstad - Ögonkliniken",
                                "id": 1354,
                                "data": {
                                    "PosId": 1354,
                                    "PosCode": "4 - 28010 - 511",
                                    "PosName": "OC Syd - Centralsjukhuset i Kristianstad - Ögonkliniken",
                                    "PosNameWithCode": "OC Syd (4) - Centralsjukhuset i Kristianstad (28010) - Ögonkliniken (511)",
                                    "PosId4": 1354,
                                    "PosLevel": 3,
                                    "UnitCode": "511",
                                    "TopPosId": 3,
                                    "TopPosCode": "4"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):4 - 28010 - 521,Organisationsenhet(namn och kod):OC Syd - Centralsjukhuset i Kristianstad - ÖNH",
                                "id": 588,
                                "data": {
                                    "PosId": 588,
                                    "PosCode": "4 - 28010 - 521",
                                    "PosName": "OC Syd - Centralsjukhuset i Kristianstad - ÖNH",
                                    "PosNameWithCode": "OC Syd (4) - Centralsjukhuset i Kristianstad (28010) - ÖNH (521)",
                                    "PosId4": 588,
                                    "PosLevel": 3,
                                    "UnitCode": "521",
                                    "TopPosId": 3,
                                    "TopPosCode": "4"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):4 - 49999 - 999,Organisationsenhet(namn och kod):OC Syd - Enhet utan INCA-inrapportör - Kliniken saknas",
                                "id": 440,
                                "data": {
                                    "PosId": 440,
                                    "PosCode": "4 - 49999 - 999",
                                    "PosName": "OC Syd - Enhet utan INCA-inrapportör - Kliniken saknas",
                                    "PosNameWithCode": "OC Syd (4) - Enhet utan INCA-inrapportör (49999) - Kliniken saknas (999)",
                                    "PosId4": 440,
                                    "PosLevel": 3,
                                    "UnitCode": "999",
                                    "TopPosId": 3,
                                    "TopPosCode": "4"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):4 - 41012 - 211,Organisationsenhet(namn och kod):OC Syd - Helsingborgs lasarett - Hudkliniken",
                                "id": 681,
                                "data": {
                                    "PosId": 681,
                                    "PosCode": "4 - 41012 - 211",
                                    "PosName": "OC Syd - Helsingborgs lasarett - Hudkliniken",
                                    "PosNameWithCode": "OC Syd (4) - Helsingborgs lasarett (41012) - Hudkliniken (211)",
                                    "PosId4": 681,
                                    "PosLevel": 3,
                                    "UnitCode": "211",
                                    "TopPosId": 3,
                                    "TopPosCode": "4"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):4 - 41012 - 301,Organisationsenhet(namn och kod):OC Syd - Helsingborgs lasarett - Kirurgiska kliniken",
                                "id": 260,
                                "data": {
                                    "PosId": 260,
                                    "PosCode": "4 - 41012 - 301",
                                    "PosName": "OC Syd - Helsingborgs lasarett - Kirurgiska kliniken",
                                    "PosNameWithCode": "OC Syd (4) - Helsingborgs lasarett (41012) - Kirurgiska kliniken (301)",
                                    "PosId4": 260,
                                    "PosLevel": 3,
                                    "UnitCode": "301",
                                    "TopPosId": 3,
                                    "TopPosCode": "4"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):4 - 41012 - 451,Organisationsenhet(namn och kod):OC Syd - Helsingborgs lasarett - Kvinnoklinik",
                                "id": 731,
                                "data": {
                                    "PosId": 731,
                                    "PosCode": "4 - 41012 - 451",
                                    "PosName": "OC Syd - Helsingborgs lasarett - Kvinnoklinik",
                                    "PosNameWithCode": "OC Syd (4) - Helsingborgs lasarett (41012) - Kvinnoklinik (451)",
                                    "PosId4": 731,
                                    "PosLevel": 3,
                                    "UnitCode": "451",
                                    "TopPosId": 3,
                                    "TopPosCode": "4"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):4 - 41012 - 101,Organisationsenhet(namn och kod):OC Syd - Helsingborgs lasarett - Medicinkliniken",
                                "id": 70,
                                "data": {
                                    "PosId": 70,
                                    "PosCode": "4 - 41012 - 101",
                                    "PosName": "OC Syd - Helsingborgs lasarett - Medicinkliniken",
                                    "PosNameWithCode": "OC Syd (4) - Helsingborgs lasarett (41012) - Medicinkliniken (101)",
                                    "PosId4": 70,
                                    "PosLevel": 3,
                                    "UnitCode": "101",
                                    "TopPosId": 3,
                                    "TopPosCode": "4"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):4 - 41012 - 741,Organisationsenhet(namn och kod):OC Syd - Helsingborgs lasarett - Onkologen",
                                "id": 1186,
                                "data": {
                                    "PosId": 1186,
                                    "PosCode": "4 - 41012 - 741",
                                    "PosName": "OC Syd - Helsingborgs lasarett - Onkologen",
                                    "PosNameWithCode": "OC Syd (4) - Helsingborgs lasarett (41012) - Onkologen (741)",
                                    "PosId4": 1186,
                                    "PosLevel": 3,
                                    "UnitCode": "741",
                                    "TopPosId": 3,
                                    "TopPosCode": "4"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):4 - 41012 - 731,Organisationsenhet(namn och kod):OC Syd - Helsingborgs lasarett - Röntgenavdelningen",
                                "id": 1479,
                                "data": {
                                    "PosId": 1479,
                                    "PosCode": "4 - 41012 - 731",
                                    "PosName": "OC Syd - Helsingborgs lasarett - Röntgenavdelningen",
                                    "PosNameWithCode": "OC Syd (4) - Helsingborgs lasarett (41012) - Röntgenavdelningen (731)",
                                    "PosId4": 1479,
                                    "PosLevel": 3,
                                    "UnitCode": "731",
                                    "TopPosId": 3,
                                    "TopPosCode": "4"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):4 - 41012 - 361,Organisationsenhet(namn och kod):OC Syd - Helsingborgs lasarett - Urologiska kliniken",
                                "id": 794,
                                "data": {
                                    "PosId": 794,
                                    "PosCode": "4 - 41012 - 361",
                                    "PosName": "OC Syd - Helsingborgs lasarett - Urologiska kliniken",
                                    "PosNameWithCode": "OC Syd (4) - Helsingborgs lasarett (41012) - Urologiska kliniken (361)",
                                    "PosId4": 794,
                                    "PosLevel": 3,
                                    "UnitCode": "361",
                                    "TopPosId": 3,
                                    "TopPosCode": "4"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):4 - 41012 - 521,Organisationsenhet(namn och kod):OC Syd - Helsingborgs lasarett - Öron-Näs-Hals kliniken",
                                "id": 313,
                                "data": {
                                    "PosId": 313,
                                    "PosCode": "4 - 41012 - 521",
                                    "PosName": "OC Syd - Helsingborgs lasarett - Öron-Näs-Hals kliniken",
                                    "PosNameWithCode": "OC Syd (4) - Helsingborgs lasarett (41012) - Öron-Näs-Hals kliniken (521)",
                                    "PosId4": 313,
                                    "PosLevel": 3,
                                    "UnitCode": "521",
                                    "TopPosId": 3,
                                    "TopPosCode": "4"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):4 - 41010 - 211,Organisationsenhet(namn och kod):OC Syd - Landskrona lasarett - Hudkliniken",
                                "id": 679,
                                "data": {
                                    "PosId": 679,
                                    "PosCode": "4 - 41010 - 211",
                                    "PosName": "OC Syd - Landskrona lasarett - Hudkliniken",
                                    "PosNameWithCode": "OC Syd (4) - Landskrona lasarett (41010) - Hudkliniken (211)",
                                    "PosId4": 679,
                                    "PosLevel": 3,
                                    "UnitCode": "211",
                                    "TopPosId": 3,
                                    "TopPosCode": "4"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):4 - 41010 - 301,Organisationsenhet(namn och kod):OC Syd - Landskrona lasarett - Kirurgiska kliniken",
                                "id": 259,
                                "data": {
                                    "PosId": 259,
                                    "PosCode": "4 - 41010 - 301",
                                    "PosName": "OC Syd - Landskrona lasarett - Kirurgiska kliniken",
                                    "PosNameWithCode": "OC Syd (4) - Landskrona lasarett (41010) - Kirurgiska kliniken (301)",
                                    "PosId4": 259,
                                    "PosLevel": 3,
                                    "UnitCode": "301",
                                    "TopPosId": 3,
                                    "TopPosCode": "4"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):4 - 41010 - 101,Organisationsenhet(namn och kod):OC Syd - Landskrona lasarett - Medicinkliniken",
                                "id": 72,
                                "data": {
                                    "PosId": 72,
                                    "PosCode": "4 - 41010 - 101",
                                    "PosName": "OC Syd - Landskrona lasarett - Medicinkliniken",
                                    "PosNameWithCode": "OC Syd (4) - Landskrona lasarett (41010) - Medicinkliniken (101)",
                                    "PosId4": 72,
                                    "PosLevel": 3,
                                    "UnitCode": "101",
                                    "TopPosId": 3,
                                    "TopPosCode": "4"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):4 - 41010 - 361,Organisationsenhet(namn och kod):OC Syd - Landskrona lasarett - Urologiska kliniken",
                                "id": 792,
                                "data": {
                                    "PosId": 792,
                                    "PosCode": "4 - 41010 - 361",
                                    "PosName": "OC Syd - Landskrona lasarett - Urologiska kliniken",
                                    "PosNameWithCode": "OC Syd (4) - Landskrona lasarett (41010) - Urologiska kliniken (361)",
                                    "PosId4": 792,
                                    "PosLevel": 3,
                                    "UnitCode": "361",
                                    "TopPosId": 3,
                                    "TopPosCode": "4"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):4 - 41010 - 521,Organisationsenhet(namn och kod):OC Syd - Landskrona lasarett - ÖNH",
                                "id": 593,
                                "data": {
                                    "PosId": 593,
                                    "PosCode": "4 - 41010 - 521",
                                    "PosName": "OC Syd - Landskrona lasarett - ÖNH",
                                    "PosNameWithCode": "OC Syd (4) - Landskrona lasarett (41010) - ÖNH (521)",
                                    "PosId4": 593,
                                    "PosLevel": 3,
                                    "UnitCode": "521",
                                    "TopPosId": 3,
                                    "TopPosCode": "4"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):4 - 23011 - 211,Organisationsenhet(namn och kod):OC Syd - Ljungby lasarett - Hudkliniken",
                                "id": 673,
                                "data": {
                                    "PosId": 673,
                                    "PosCode": "4 - 23011 - 211",
                                    "PosName": "OC Syd - Ljungby lasarett - Hudkliniken",
                                    "PosNameWithCode": "OC Syd (4) - Ljungby lasarett (23011) - Hudkliniken (211)",
                                    "PosId4": 673,
                                    "PosLevel": 3,
                                    "UnitCode": "211",
                                    "TopPosId": 3,
                                    "TopPosCode": "4"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):4 - 23011 - 301,Organisationsenhet(namn och kod):OC Syd - Ljungby lasarett - Kirurgiska kliniken",
                                "id": 256,
                                "data": {
                                    "PosId": 256,
                                    "PosCode": "4 - 23011 - 301",
                                    "PosName": "OC Syd - Ljungby lasarett - Kirurgiska kliniken",
                                    "PosNameWithCode": "OC Syd (4) - Ljungby lasarett (23011) - Kirurgiska kliniken (301)",
                                    "PosId4": 256,
                                    "PosLevel": 3,
                                    "UnitCode": "301",
                                    "TopPosId": 3,
                                    "TopPosCode": "4"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):4 - 23011 - 451,Organisationsenhet(namn och kod):OC Syd - Ljungby lasarett - Kvinnoklinik",
                                "id": 726,
                                "data": {
                                    "PosId": 726,
                                    "PosCode": "4 - 23011 - 451",
                                    "PosName": "OC Syd - Ljungby lasarett - Kvinnoklinik",
                                    "PosNameWithCode": "OC Syd (4) - Ljungby lasarett (23011) - Kvinnoklinik (451)",
                                    "PosId4": 726,
                                    "PosLevel": 3,
                                    "UnitCode": "451",
                                    "TopPosId": 3,
                                    "TopPosCode": "4"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):4 - 23011 - 101,Organisationsenhet(namn och kod):OC Syd - Ljungby lasarett - Medicinkliniken",
                                "id": 78,
                                "data": {
                                    "PosId": 78,
                                    "PosCode": "4 - 23011 - 101",
                                    "PosName": "OC Syd - Ljungby lasarett - Medicinkliniken",
                                    "PosNameWithCode": "OC Syd (4) - Ljungby lasarett (23011) - Medicinkliniken (101)",
                                    "PosId4": 78,
                                    "PosLevel": 3,
                                    "UnitCode": "101",
                                    "TopPosId": 3,
                                    "TopPosCode": "4"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):4 - 23011 - 521,Organisationsenhet(namn och kod):OC Syd - Ljungby lasarett - ÖNH",
                                "id": 586,
                                "data": {
                                    "PosId": 586,
                                    "PosCode": "4 - 23011 - 521",
                                    "PosName": "OC Syd - Ljungby lasarett - ÖNH",
                                    "PosNameWithCode": "OC Syd (4) - Ljungby lasarett (23011) - ÖNH (521)",
                                    "PosId4": 586,
                                    "PosLevel": 3,
                                    "UnitCode": "521",
                                    "TopPosId": 3,
                                    "TopPosCode": "4"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):4 - 42010 - 211,Organisationsenhet(namn och kod):OC Syd - Länssjukhuset i Halmstad - Hudkliniken",
                                "id": 671,
                                "data": {
                                    "PosId": 671,
                                    "PosCode": "4 - 42010 - 211",
                                    "PosName": "OC Syd - Länssjukhuset i Halmstad - Hudkliniken",
                                    "PosNameWithCode": "OC Syd (4) - Länssjukhuset i Halmstad (42010) - Hudkliniken (211)",
                                    "PosId4": 671,
                                    "PosLevel": 3,
                                    "UnitCode": "211",
                                    "TopPosId": 3,
                                    "TopPosCode": "4"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):4 - 42010 - 301,Organisationsenhet(namn och kod):OC Syd - Länssjukhuset i Halmstad - Kirurgiska kliniken",
                                "id": 250,
                                "data": {
                                    "PosId": 250,
                                    "PosCode": "4 - 42010 - 301",
                                    "PosName": "OC Syd - Länssjukhuset i Halmstad - Kirurgiska kliniken",
                                    "PosNameWithCode": "OC Syd (4) - Länssjukhuset i Halmstad (42010) - Kirurgiska kliniken (301)",
                                    "PosId4": 250,
                                    "PosLevel": 3,
                                    "UnitCode": "301",
                                    "TopPosId": 3,
                                    "TopPosCode": "4"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):4 - 42010 - 451,Organisationsenhet(namn och kod):OC Syd - Länssjukhuset i Halmstad - Kvinnoklinik",
                                "id": 724,
                                "data": {
                                    "PosId": 724,
                                    "PosCode": "4 - 42010 - 451",
                                    "PosName": "OC Syd - Länssjukhuset i Halmstad - Kvinnoklinik",
                                    "PosNameWithCode": "OC Syd (4) - Länssjukhuset i Halmstad (42010) - Kvinnoklinik (451)",
                                    "PosId4": 724,
                                    "PosLevel": 3,
                                    "UnitCode": "451",
                                    "TopPosId": 3,
                                    "TopPosCode": "4"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):4 - 42010 - 111,Organisationsenhet(namn och kod):OC Syd - Länssjukhuset i Halmstad - Lungmedicin",
                                "id": 695,
                                "data": {
                                    "PosId": 695,
                                    "PosCode": "4 - 42010 - 111",
                                    "PosName": "OC Syd - Länssjukhuset i Halmstad - Lungmedicin",
                                    "PosNameWithCode": "OC Syd (4) - Länssjukhuset i Halmstad (42010) - Lungmedicin (111)",
                                    "PosId4": 695,
                                    "PosLevel": 3,
                                    "UnitCode": "111",
                                    "TopPosId": 3,
                                    "TopPosCode": "4"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):4 - 42010 - 101,Organisationsenhet(namn och kod):OC Syd - Länssjukhuset i Halmstad - Medicinkliniken",
                                "id": 68,
                                "data": {
                                    "PosId": 68,
                                    "PosCode": "4 - 42010 - 101",
                                    "PosName": "OC Syd - Länssjukhuset i Halmstad - Medicinkliniken",
                                    "PosNameWithCode": "OC Syd (4) - Länssjukhuset i Halmstad (42010) - Medicinkliniken (101)",
                                    "PosId4": 68,
                                    "PosLevel": 3,
                                    "UnitCode": "101",
                                    "TopPosId": 3,
                                    "TopPosCode": "4"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):4 - 42010 - 731,Organisationsenhet(namn och kod):OC Syd - Länssjukhuset i Halmstad - Röntgenavdelningen",
                                "id": 1405,
                                "data": {
                                    "PosId": 1405,
                                    "PosCode": "4 - 42010 - 731",
                                    "PosName": "OC Syd - Länssjukhuset i Halmstad - Röntgenavdelningen",
                                    "PosNameWithCode": "OC Syd (4) - Länssjukhuset i Halmstad (42010) - Röntgenavdelningen (731)",
                                    "PosId4": 1405,
                                    "PosLevel": 3,
                                    "UnitCode": "731",
                                    "TopPosId": 3,
                                    "TopPosCode": "4"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):4 - 42010 - 361,Organisationsenhet(namn och kod):OC Syd - Länssjukhuset i Halmstad - Urologiska kliniken",
                                "id": 252,
                                "data": {
                                    "PosId": 252,
                                    "PosCode": "4 - 42010 - 361",
                                    "PosName": "OC Syd - Länssjukhuset i Halmstad - Urologiska kliniken",
                                    "PosNameWithCode": "OC Syd (4) - Länssjukhuset i Halmstad (42010) - Urologiska kliniken (361)",
                                    "PosId4": 252,
                                    "PosLevel": 3,
                                    "UnitCode": "361",
                                    "TopPosId": 3,
                                    "TopPosCode": "4"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):4 - 42010 - 521,Organisationsenhet(namn och kod):OC Syd - Länssjukhuset i Halmstad - ÖNH",
                                "id": 583,
                                "data": {
                                    "PosId": 583,
                                    "PosCode": "4 - 42010 - 521",
                                    "PosName": "OC Syd - Länssjukhuset i Halmstad - ÖNH",
                                    "PosNameWithCode": "OC Syd (4) - Länssjukhuset i Halmstad (42010) - ÖNH (521)",
                                    "PosId4": 583,
                                    "PosLevel": 3,
                                    "UnitCode": "521",
                                    "TopPosId": 3,
                                    "TopPosCode": "4"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):4 - 27199 - 999,Organisationsenhet(namn och kod):OC Syd - Privatläkare Blekinge län - Kliniken saknas",
                                "id": 499,
                                "data": {
                                    "PosId": 499,
                                    "PosCode": "4 - 27199 - 999",
                                    "PosName": "OC Syd - Privatläkare Blekinge län - Kliniken saknas",
                                    "PosNameWithCode": "OC Syd (4) - Privatläkare Blekinge län (27199) - Kliniken saknas (999)",
                                    "PosId4": 499,
                                    "PosLevel": 3,
                                    "UnitCode": "999",
                                    "TopPosId": 3,
                                    "TopPosCode": "4"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):4 - 27199 - 998,Organisationsenhet(namn och kod):OC Syd - Privatläkare Blekinge län - Urologen i Carlshamns specialistklinik AB",
                                "id": 500,
                                "data": {
                                    "PosId": 500,
                                    "PosCode": "4 - 27199 - 998",
                                    "PosName": "OC Syd - Privatläkare Blekinge län - Urologen i Carlshamns specialistklinik AB",
                                    "PosNameWithCode": "OC Syd (4) - Privatläkare Blekinge län (27199) - Urologen i Carlshamns specialistklinik AB (998)",
                                    "PosId4": 500,
                                    "PosLevel": 3,
                                    "UnitCode": "998",
                                    "TopPosId": 3,
                                    "TopPosCode": "4"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):4 - 28199 - 997,Organisationsenhet(namn och kod):OC Syd - Privatläkare gamla Kristianstads län - Göingekliniken",
                                "id": 1376,
                                "data": {
                                    "PosId": 1376,
                                    "PosCode": "4 - 28199 - 997",
                                    "PosName": "OC Syd - Privatläkare gamla Kristianstads län - Göingekliniken",
                                    "PosNameWithCode": "OC Syd (4) - Privatläkare gamla Kristianstads län (28199) - Göingekliniken (997)",
                                    "PosId4": 1376,
                                    "PosLevel": 3,
                                    "UnitCode": "997",
                                    "TopPosId": 3,
                                    "TopPosCode": "4"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):4 - 28199 - 999,Organisationsenhet(namn och kod):OC Syd - Privatläkare gamla Kristianstads län - Kliniken saknas",
                                "id": 497,
                                "data": {
                                    "PosId": 497,
                                    "PosCode": "4 - 28199 - 999",
                                    "PosName": "OC Syd - Privatläkare gamla Kristianstads län - Kliniken saknas",
                                    "PosNameWithCode": "OC Syd (4) - Privatläkare gamla Kristianstads län (28199) - Kliniken saknas (999)",
                                    "PosId4": 497,
                                    "PosLevel": 3,
                                    "UnitCode": "999",
                                    "TopPosId": 3,
                                    "TopPosCode": "4"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):4 - 28199 - 998,Organisationsenhet(namn och kod):OC Syd - Privatläkare gamla Kristianstads län - Urologen i Kristianstadskliniken",
                                "id": 498,
                                "data": {
                                    "PosId": 498,
                                    "PosCode": "4 - 28199 - 998",
                                    "PosName": "OC Syd - Privatläkare gamla Kristianstads län - Urologen i Kristianstadskliniken",
                                    "PosNameWithCode": "OC Syd (4) - Privatläkare gamla Kristianstads län (28199) - Urologen i Kristianstadskliniken (998)",
                                    "PosId4": 498,
                                    "PosLevel": 3,
                                    "UnitCode": "998",
                                    "TopPosId": 3,
                                    "TopPosCode": "4"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):4 - 41199 - 998,Organisationsenhet(namn och kod):OC Syd - Privatläkare gamla Malmöhus län - Capio Cityklinken",
                                "id": 460,
                                "data": {
                                    "PosId": 460,
                                    "PosCode": "4 - 41199 - 998",
                                    "PosName": "OC Syd - Privatläkare gamla Malmöhus län - Capio Cityklinken",
                                    "PosNameWithCode": "OC Syd (4) - Privatläkare gamla Malmöhus län (41199) - Capio Cityklinken (998)",
                                    "PosId4": 460,
                                    "PosLevel": 3,
                                    "UnitCode": "998",
                                    "TopPosId": 3,
                                    "TopPosCode": "4"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):4 - 41199 - 989,Organisationsenhet(namn och kod):OC Syd - Privatläkare gamla Malmöhus län - Capio, Eslöv",
                                "id": 1214,
                                "data": {
                                    "PosId": 1214,
                                    "PosCode": "4 - 41199 - 989",
                                    "PosName": "OC Syd - Privatläkare gamla Malmöhus län - Capio, Eslöv",
                                    "PosNameWithCode": "OC Syd (4) - Privatläkare gamla Malmöhus län (41199) - Capio, Eslöv (989)",
                                    "PosId4": 1214,
                                    "PosLevel": 3,
                                    "UnitCode": "989",
                                    "TopPosId": 3,
                                    "TopPosCode": "4"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):4 - 41199 - 994,Organisationsenhet(namn och kod):OC Syd - Privatläkare gamla Malmöhus län - Carema, Eslöv",
                                "id": 483,
                                "data": {
                                    "PosId": 483,
                                    "PosCode": "4 - 41199 - 994",
                                    "PosName": "OC Syd - Privatläkare gamla Malmöhus län - Carema, Eslöv",
                                    "PosNameWithCode": "OC Syd (4) - Privatläkare gamla Malmöhus län (41199) - Carema, Eslöv (994)",
                                    "PosId4": 483,
                                    "PosLevel": 3,
                                    "UnitCode": "994",
                                    "TopPosId": 3,
                                    "TopPosCode": "4"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):4 - 41199 - 991,Organisationsenhet(namn och kod):OC Syd - Privatläkare gamla Malmöhus län - Carema, Lund",
                                "id": 521,
                                "data": {
                                    "PosId": 521,
                                    "PosCode": "4 - 41199 - 991",
                                    "PosName": "OC Syd - Privatläkare gamla Malmöhus län - Carema, Lund",
                                    "PosNameWithCode": "OC Syd (4) - Privatläkare gamla Malmöhus län (41199) - Carema, Lund (991)",
                                    "PosId4": 521,
                                    "PosLevel": 3,
                                    "UnitCode": "991",
                                    "TopPosId": 3,
                                    "TopPosCode": "4"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):4 - 41199 - 990,Organisationsenhet(namn och kod):OC Syd - Privatläkare gamla Malmöhus län - Gastro Center Skåne, Lund",
                                "id": 1202,
                                "data": {
                                    "PosId": 1202,
                                    "PosCode": "4 - 41199 - 990",
                                    "PosName": "OC Syd - Privatläkare gamla Malmöhus län - Gastro Center Skåne, Lund",
                                    "PosNameWithCode": "OC Syd (4) - Privatläkare gamla Malmöhus län (41199) - Gastro Center Skåne, Lund (990)",
                                    "PosId4": 1202,
                                    "PosLevel": 3,
                                    "UnitCode": "990",
                                    "TopPosId": 3,
                                    "TopPosCode": "4"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):4 - 41199 - 986,Organisationsenhet(namn och kod):OC Syd - Privatläkare gamla Malmöhus län - Hudläkarmottagningen vid Lundagårdsläkargrupp",
                                "id": 1463,
                                "data": {
                                    "PosId": 1463,
                                    "PosCode": "4 - 41199 - 986",
                                    "PosName": "OC Syd - Privatläkare gamla Malmöhus län - Hudläkarmottagningen vid Lundagårdsläkargrupp",
                                    "PosNameWithCode": "OC Syd (4) - Privatläkare gamla Malmöhus län (41199) - Hudläkarmottagningen vid Lundagårdsläkargrupp (986)",
                                    "PosId4": 1463,
                                    "PosLevel": 3,
                                    "UnitCode": "986",
                                    "TopPosId": 3,
                                    "TopPosCode": "4"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):4 - 41199 - 999,Organisationsenhet(namn och kod):OC Syd - Privatläkare gamla Malmöhus län - Kliniken saknas",
                                "id": 438,
                                "data": {
                                    "PosId": 438,
                                    "PosCode": "4 - 41199 - 999",
                                    "PosName": "OC Syd - Privatläkare gamla Malmöhus län - Kliniken saknas",
                                    "PosNameWithCode": "OC Syd (4) - Privatläkare gamla Malmöhus län (41199) - Kliniken saknas (999)",
                                    "PosId4": 438,
                                    "PosLevel": 3,
                                    "UnitCode": "999",
                                    "TopPosId": 3,
                                    "TopPosCode": "4"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):4 - 41199 - 996,Organisationsenhet(namn och kod):OC Syd - Privatläkare gamla Malmöhus län - Läkargruppen i Vellinge",
                                "id": 481,
                                "data": {
                                    "PosId": 481,
                                    "PosCode": "4 - 41199 - 996",
                                    "PosName": "OC Syd - Privatläkare gamla Malmöhus län - Läkargruppen i Vellinge",
                                    "PosNameWithCode": "OC Syd (4) - Privatläkare gamla Malmöhus län (41199) - Läkargruppen i Vellinge (996)",
                                    "PosId4": 481,
                                    "PosLevel": 3,
                                    "UnitCode": "996",
                                    "TopPosId": 3,
                                    "TopPosCode": "4"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):4 - 41199 - 995,Organisationsenhet(namn och kod):OC Syd - Privatläkare gamla Malmöhus län - Specialistläkarmottagningen i Lund",
                                "id": 482,
                                "data": {
                                    "PosId": 482,
                                    "PosCode": "4 - 41199 - 995",
                                    "PosName": "OC Syd - Privatläkare gamla Malmöhus län - Specialistläkarmottagningen i Lund",
                                    "PosNameWithCode": "OC Syd (4) - Privatläkare gamla Malmöhus län (41199) - Specialistläkarmottagningen i Lund (995)",
                                    "PosId4": 482,
                                    "PosLevel": 3,
                                    "UnitCode": "995",
                                    "TopPosId": 3,
                                    "TopPosCode": "4"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):4 - 41199 - 987,Organisationsenhet(namn och kod):OC Syd - Privatläkare gamla Malmöhus län - Specialistläkarna, LUND",
                                "id": 1409,
                                "data": {
                                    "PosId": 1409,
                                    "PosCode": "4 - 41199 - 987",
                                    "PosName": "OC Syd - Privatläkare gamla Malmöhus län - Specialistläkarna, LUND",
                                    "PosNameWithCode": "OC Syd (4) - Privatläkare gamla Malmöhus län (41199) - Specialistläkarna, LUND (987)",
                                    "PosId4": 1409,
                                    "PosLevel": 3,
                                    "UnitCode": "987",
                                    "TopPosId": 3,
                                    "TopPosCode": "4"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):4 - 41199 - 988,Organisationsenhet(namn och kod):OC Syd - Privatläkare gamla Malmöhus län - Wullt läkarmottagning",
                                "id": 1125,
                                "data": {
                                    "PosId": 1125,
                                    "PosCode": "4 - 41199 - 988",
                                    "PosName": "OC Syd - Privatläkare gamla Malmöhus län - Wullt läkarmottagning",
                                    "PosNameWithCode": "OC Syd (4) - Privatläkare gamla Malmöhus län (41199) - Wullt läkarmottagning (988)",
                                    "PosId4": 1125,
                                    "PosLevel": 3,
                                    "UnitCode": "988",
                                    "TopPosId": 3,
                                    "TopPosCode": "4"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):4 - 42199 - 999,Organisationsenhet(namn och kod):OC Syd - Privatläkare Hallands län - Kliniken saknas",
                                "id": 502,
                                "data": {
                                    "PosId": 502,
                                    "PosCode": "4 - 42199 - 999",
                                    "PosName": "OC Syd - Privatläkare Hallands län - Kliniken saknas",
                                    "PosNameWithCode": "OC Syd (4) - Privatläkare Hallands län (42199) - Kliniken saknas (999)",
                                    "PosId4": 502,
                                    "PosLevel": 3,
                                    "UnitCode": "999",
                                    "TopPosId": 3,
                                    "TopPosCode": "4"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):4 - 42199 - 998,Organisationsenhet(namn och kod):OC Syd - Privatläkare Hallands län - Specialist mottagning i urologi och gyn",
                                "id": 924,
                                "data": {
                                    "PosId": 924,
                                    "PosCode": "4 - 42199 - 998",
                                    "PosName": "OC Syd - Privatläkare Hallands län - Specialist mottagning i urologi och gyn",
                                    "PosNameWithCode": "OC Syd (4) - Privatläkare Hallands län (42199) - Specialist mottagning i urologi och gyn (998)",
                                    "PosId4": 924,
                                    "PosLevel": 3,
                                    "UnitCode": "998",
                                    "TopPosId": 3,
                                    "TopPosCode": "4"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):4 - 23199 - 998,Organisationsenhet(namn och kod):OC Syd - Privatläkare Kronobergs län - Gränsbygdsklinken AB",
                                "id": 1179,
                                "data": {
                                    "PosId": 1179,
                                    "PosCode": "4 - 23199 - 998",
                                    "PosName": "OC Syd - Privatläkare Kronobergs län - Gränsbygdsklinken AB",
                                    "PosNameWithCode": "OC Syd (4) - Privatläkare Kronobergs län (23199) - Gränsbygdsklinken AB (998)",
                                    "PosId4": 1179,
                                    "PosLevel": 3,
                                    "UnitCode": "998",
                                    "TopPosId": 3,
                                    "TopPosCode": "4"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):4 - 23199 - 999,Organisationsenhet(namn och kod):OC Syd - Privatläkare Kronobergs län - Kliniken saknas",
                                "id": 501,
                                "data": {
                                    "PosId": 501,
                                    "PosCode": "4 - 23199 - 999",
                                    "PosName": "OC Syd - Privatläkare Kronobergs län - Kliniken saknas",
                                    "PosNameWithCode": "OC Syd (4) - Privatläkare Kronobergs län (23199) - Kliniken saknas (999)",
                                    "PosId4": 501,
                                    "PosLevel": 3,
                                    "UnitCode": "999",
                                    "TopPosId": 3,
                                    "TopPosCode": "4"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):4 - 30199 - 996,Organisationsenhet(namn och kod):OC Syd - Privatläkare Malmö stad - Diagnostiskt centrum HUD",
                                "id": 1413,
                                "data": {
                                    "PosId": 1413,
                                    "PosCode": "4 - 30199 - 996",
                                    "PosName": "OC Syd - Privatläkare Malmö stad - Diagnostiskt centrum HUD",
                                    "PosNameWithCode": "OC Syd (4) - Privatläkare Malmö stad (30199) - Diagnostiskt centrum HUD (996)",
                                    "PosId4": 1413,
                                    "PosLevel": 3,
                                    "UnitCode": "996",
                                    "TopPosId": 3,
                                    "TopPosCode": "4"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):4 - 30199 - 999,Organisationsenhet(namn och kod):OC Syd - Privatläkare Malmö stad - Kliniken saknas",
                                "id": 495,
                                "data": {
                                    "PosId": 495,
                                    "PosCode": "4 - 30199 - 999",
                                    "PosName": "OC Syd - Privatläkare Malmö stad - Kliniken saknas",
                                    "PosNameWithCode": "OC Syd (4) - Privatläkare Malmö stad (30199) - Kliniken saknas (999)",
                                    "PosId4": 495,
                                    "PosLevel": 3,
                                    "UnitCode": "999",
                                    "TopPosId": 3,
                                    "TopPosCode": "4"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):4 - 30199 - 998,Organisationsenhet(namn och kod):OC Syd - Privatläkare Malmö stad - Läkarhuset Ellenbogen",
                                "id": 496,
                                "data": {
                                    "PosId": 496,
                                    "PosCode": "4 - 30199 - 998",
                                    "PosName": "OC Syd - Privatläkare Malmö stad - Läkarhuset Ellenbogen",
                                    "PosNameWithCode": "OC Syd (4) - Privatläkare Malmö stad (30199) - Läkarhuset Ellenbogen (998)",
                                    "PosId4": 496,
                                    "PosLevel": 3,
                                    "UnitCode": "998",
                                    "TopPosId": 3,
                                    "TopPosCode": "4"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):4 - 30199 - 997,Organisationsenhet(namn och kod):OC Syd - Privatläkare Malmö stad - Slottsstadens läkarhus",
                                "id": 530,
                                "data": {
                                    "PosId": 530,
                                    "PosCode": "4 - 30199 - 997",
                                    "PosName": "OC Syd - Privatläkare Malmö stad - Slottsstadens läkarhus",
                                    "PosNameWithCode": "OC Syd (4) - Privatläkare Malmö stad (30199) - Slottsstadens läkarhus (997)",
                                    "PosId4": 530,
                                    "PosLevel": 3,
                                    "UnitCode": "997",
                                    "TopPosId": 3,
                                    "TopPosCode": "4"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):4 - 28013 - 211,Organisationsenhet(namn och kod):OC Syd - Simrishamns praktiker tjänst - Hudkliniken",
                                "id": 677,
                                "data": {
                                    "PosId": 677,
                                    "PosCode": "4 - 28013 - 211",
                                    "PosName": "OC Syd - Simrishamns praktiker tjänst - Hudkliniken",
                                    "PosNameWithCode": "OC Syd (4) - Simrishamns praktiker tjänst (28013) - Hudkliniken (211)",
                                    "PosId4": 677,
                                    "PosLevel": 3,
                                    "UnitCode": "211",
                                    "TopPosId": 3,
                                    "TopPosCode": "4"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):4 - 28013 - 301,Organisationsenhet(namn och kod):OC Syd - Simrishamns praktiker tjänst - Kirurgiska kliniken",
                                "id": 316,
                                "data": {
                                    "PosId": 316,
                                    "PosCode": "4 - 28013 - 301",
                                    "PosName": "OC Syd - Simrishamns praktiker tjänst - Kirurgiska kliniken",
                                    "PosNameWithCode": "OC Syd (4) - Simrishamns praktiker tjänst (28013) - Kirurgiska kliniken (301)",
                                    "PosId4": 316,
                                    "PosLevel": 3,
                                    "UnitCode": "301",
                                    "TopPosId": 3,
                                    "TopPosCode": "4"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):4 - 28013 - 101,Organisationsenhet(namn och kod):OC Syd - Simrishamns praktiker tjänst - Medicinkliniken",
                                "id": 73,
                                "data": {
                                    "PosId": 73,
                                    "PosCode": "4 - 28013 - 101",
                                    "PosName": "OC Syd - Simrishamns praktiker tjänst - Medicinkliniken",
                                    "PosNameWithCode": "OC Syd (4) - Simrishamns praktiker tjänst (28013) - Medicinkliniken (101)",
                                    "PosId4": 73,
                                    "PosLevel": 3,
                                    "UnitCode": "101",
                                    "TopPosId": 3,
                                    "TopPosCode": "4"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):4 - 28013 - 521,Organisationsenhet(namn och kod):OC Syd - Simrishamns praktiker tjänst - ÖNH",
                                "id": 592,
                                "data": {
                                    "PosId": 592,
                                    "PosCode": "4 - 28013 - 521",
                                    "PosName": "OC Syd - Simrishamns praktiker tjänst - ÖNH",
                                    "PosNameWithCode": "OC Syd (4) - Simrishamns praktiker tjänst (28013) - ÖNH (521)",
                                    "PosId4": 592,
                                    "PosLevel": 3,
                                    "UnitCode": "521",
                                    "TopPosId": 3,
                                    "TopPosCode": "4"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):4 - 28012 - 211,Organisationsenhet(namn och kod):OC Syd - Sjukhuset i Hässleholm - Hudkliniken",
                                "id": 684,
                                "data": {
                                    "PosId": 684,
                                    "PosCode": "4 - 28012 - 211",
                                    "PosName": "OC Syd - Sjukhuset i Hässleholm - Hudkliniken",
                                    "PosNameWithCode": "OC Syd (4) - Sjukhuset i Hässleholm (28012) - Hudkliniken (211)",
                                    "PosId4": 684,
                                    "PosLevel": 3,
                                    "UnitCode": "211",
                                    "TopPosId": 3,
                                    "TopPosCode": "4"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):4 - 28012 - 301,Organisationsenhet(namn och kod):OC Syd - Sjukhuset i Hässleholm - Kirurgiska kliniken",
                                "id": 314,
                                "data": {
                                    "PosId": 314,
                                    "PosCode": "4 - 28012 - 301",
                                    "PosName": "OC Syd - Sjukhuset i Hässleholm - Kirurgiska kliniken",
                                    "PosNameWithCode": "OC Syd (4) - Sjukhuset i Hässleholm (28012) - Kirurgiska kliniken (301)",
                                    "PosId4": 314,
                                    "PosLevel": 3,
                                    "UnitCode": "301",
                                    "TopPosId": 3,
                                    "TopPosCode": "4"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):4 - 28012 - 101,Organisationsenhet(namn och kod):OC Syd - Sjukhuset i Hässleholm - Medicinkliniken",
                                "id": 80,
                                "data": {
                                    "PosId": 80,
                                    "PosCode": "4 - 28012 - 101",
                                    "PosName": "OC Syd - Sjukhuset i Hässleholm - Medicinkliniken",
                                    "PosNameWithCode": "OC Syd (4) - Sjukhuset i Hässleholm (28012) - Medicinkliniken (101)",
                                    "PosId4": 80,
                                    "PosLevel": 3,
                                    "UnitCode": "101",
                                    "TopPosId": 3,
                                    "TopPosCode": "4"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):4 - 28012 - 521,Organisationsenhet(namn och kod):OC Syd - Sjukhuset i Hässleholm - ÖNH",
                                "id": 598,
                                "data": {
                                    "PosId": 598,
                                    "PosCode": "4 - 28012 - 521",
                                    "PosName": "OC Syd - Sjukhuset i Hässleholm - ÖNH",
                                    "PosNameWithCode": "OC Syd (4) - Sjukhuset i Hässleholm (28012) - ÖNH (521)",
                                    "PosId4": 598,
                                    "PosLevel": 3,
                                    "UnitCode": "521",
                                    "TopPosId": 3,
                                    "TopPosCode": "4"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):4 - 41001 - 103,Organisationsenhet(namn och kod):OC Syd - Skånes universitetssjukhus-Lund - endokrinologisk klinik",
                                "id": 1177,
                                "data": {
                                    "PosId": 1177,
                                    "PosCode": "4 - 41001 - 103",
                                    "PosName": "OC Syd - Skånes universitetssjukhus-Lund - endokrinologisk klinik",
                                    "PosNameWithCode": "OC Syd (4) - Skånes universitetssjukhus-Lund (41001) - endokrinologisk klinik (103)",
                                    "PosId4": 1177,
                                    "PosLevel": 3,
                                    "UnitCode": "103",
                                    "TopPosId": 3,
                                    "TopPosCode": "4"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):4 - 41001 - 104,Organisationsenhet(namn och kod):OC Syd - Skånes universitetssjukhus-Lund - Gastroenterologi",
                                "id": 456,
                                "data": {
                                    "PosId": 456,
                                    "PosCode": "4 - 41001 - 104",
                                    "PosName": "OC Syd - Skånes universitetssjukhus-Lund - Gastroenterologi",
                                    "PosNameWithCode": "OC Syd (4) - Skånes universitetssjukhus-Lund (41001) - Gastroenterologi (104)",
                                    "PosId4": 456,
                                    "PosLevel": 3,
                                    "UnitCode": "104",
                                    "TopPosId": 3,
                                    "TopPosCode": "4"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):4 - 41001 - 105,Organisationsenhet(namn och kod):OC Syd - Skånes universitetssjukhus-Lund - Hematologiska kliniken",
                                "id": 59,
                                "data": {
                                    "PosId": 59,
                                    "PosCode": "4 - 41001 - 105",
                                    "PosName": "OC Syd - Skånes universitetssjukhus-Lund - Hematologiska kliniken",
                                    "PosNameWithCode": "OC Syd (4) - Skånes universitetssjukhus-Lund (41001) - Hematologiska kliniken (105)",
                                    "PosId4": 59,
                                    "PosLevel": 3,
                                    "UnitCode": "105",
                                    "TopPosId": 3,
                                    "TopPosCode": "4"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):4 - 41001 - 211,Organisationsenhet(namn och kod):OC Syd - Skånes universitetssjukhus-Lund - Hudkliniken",
                                "id": 670,
                                "data": {
                                    "PosId": 670,
                                    "PosCode": "4 - 41001 - 211",
                                    "PosName": "OC Syd - Skånes universitetssjukhus-Lund - Hudkliniken",
                                    "PosNameWithCode": "OC Syd (4) - Skånes universitetssjukhus-Lund (41001) - Hudkliniken (211)",
                                    "PosId4": 670,
                                    "PosLevel": 3,
                                    "UnitCode": "211",
                                    "TopPosId": 3,
                                    "TopPosCode": "4"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):4 - 41001 - 301,Organisationsenhet(namn och kod):OC Syd - Skånes universitetssjukhus-Lund - Kirurgiska kliniken",
                                "id": 132,
                                "data": {
                                    "PosId": 132,
                                    "PosCode": "4 - 41001 - 301",
                                    "PosName": "OC Syd - Skånes universitetssjukhus-Lund - Kirurgiska kliniken",
                                    "PosNameWithCode": "OC Syd (4) - Skånes universitetssjukhus-Lund (41001) - Kirurgiska kliniken (301)",
                                    "PosId4": 132,
                                    "PosLevel": 3,
                                    "UnitCode": "301",
                                    "TopPosId": 3,
                                    "TopPosCode": "4"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):4 - 41001 - 451,Organisationsenhet(namn och kod):OC Syd - Skånes universitetssjukhus-Lund - Kvinnoklinik",
                                "id": 702,
                                "data": {
                                    "PosId": 702,
                                    "PosCode": "4 - 41001 - 451",
                                    "PosName": "OC Syd - Skånes universitetssjukhus-Lund - Kvinnoklinik",
                                    "PosNameWithCode": "OC Syd (4) - Skånes universitetssjukhus-Lund (41001) - Kvinnoklinik (451)",
                                    "PosId4": 702,
                                    "PosLevel": 3,
                                    "UnitCode": "451",
                                    "TopPosId": 3,
                                    "TopPosCode": "4"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):4 - 41001 - 107,Organisationsenhet(namn och kod):OC Syd - Skånes universitetssjukhus-Lund - Lung Klin",
                                "id": 685,
                                "data": {
                                    "PosId": 685,
                                    "PosCode": "4 - 41001 - 107",
                                    "PosName": "OC Syd - Skånes universitetssjukhus-Lund - Lung Klin",
                                    "PosNameWithCode": "OC Syd (4) - Skånes universitetssjukhus-Lund (41001) - Lung Klin (107)",
                                    "PosId4": 685,
                                    "PosLevel": 3,
                                    "UnitCode": "107",
                                    "TopPosId": 3,
                                    "TopPosCode": "4"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):4 - 41001 - 331,Organisationsenhet(namn och kod):OC Syd - Skånes universitetssjukhus-Lund - Neurokirurgklin",
                                "id": 669,
                                "data": {
                                    "PosId": 669,
                                    "PosCode": "4 - 41001 - 331",
                                    "PosName": "OC Syd - Skånes universitetssjukhus-Lund - Neurokirurgklin",
                                    "PosNameWithCode": "OC Syd (4) - Skånes universitetssjukhus-Lund (41001) - Neurokirurgklin (331)",
                                    "PosId4": 669,
                                    "PosLevel": 3,
                                    "UnitCode": "331",
                                    "TopPosId": 3,
                                    "TopPosCode": "4"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):4 - 41001 - 221,Organisationsenhet(namn och kod):OC Syd - Skånes universitetssjukhus-Lund - Neurologen",
                                "id": 1278,
                                "data": {
                                    "PosId": 1278,
                                    "PosCode": "4 - 41001 - 221",
                                    "PosName": "OC Syd - Skånes universitetssjukhus-Lund - Neurologen",
                                    "PosNameWithCode": "OC Syd (4) - Skånes universitetssjukhus-Lund (41001) - Neurologen (221)",
                                    "PosId4": 1278,
                                    "PosLevel": 3,
                                    "UnitCode": "221",
                                    "TopPosId": 3,
                                    "TopPosCode": "4"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):4 - 41001 - 000,Organisationsenhet(namn och kod):OC Syd - Skånes universitetssjukhus-Lund - Onk forskning avd/ barnonkologen",
                                "id": 701,
                                "data": {
                                    "PosId": 701,
                                    "PosCode": "4 - 41001 - 000",
                                    "PosName": "OC Syd - Skånes universitetssjukhus-Lund - Onk forskning avd/ barnonkologen",
                                    "PosNameWithCode": "OC Syd (4) - Skånes universitetssjukhus-Lund (41001) - Onk forskning avd/ barnonkologen (000)",
                                    "PosId4": 701,
                                    "PosLevel": 3,
                                    "UnitCode": "000",
                                    "TopPosId": 3,
                                    "TopPosCode": "4"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):4 - 41001 - 741,Organisationsenhet(namn och kod):OC Syd - Skånes universitetssjukhus-Lund - Onkologiska kliniken",
                                "id": 65,
                                "data": {
                                    "PosId": 65,
                                    "PosCode": "4 - 41001 - 741",
                                    "PosName": "OC Syd - Skånes universitetssjukhus-Lund - Onkologiska kliniken",
                                    "PosNameWithCode": "OC Syd (4) - Skånes universitetssjukhus-Lund (41001) - Onkologiska kliniken (741)",
                                    "PosId4": 65,
                                    "PosLevel": 3,
                                    "UnitCode": "741",
                                    "TopPosId": 3,
                                    "TopPosCode": "4"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):4 - 41001 - 311,Organisationsenhet(namn och kod):OC Syd - Skånes universitetssjukhus-Lund - Ortopedklinik",
                                "id": 668,
                                "data": {
                                    "PosId": 668,
                                    "PosCode": "4 - 41001 - 311",
                                    "PosName": "OC Syd - Skånes universitetssjukhus-Lund - Ortopedklinik",
                                    "PosNameWithCode": "OC Syd (4) - Skånes universitetssjukhus-Lund (41001) - Ortopedklinik (311)",
                                    "PosId4": 668,
                                    "PosLevel": 3,
                                    "UnitCode": "311",
                                    "TopPosId": 3,
                                    "TopPosCode": "4"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):4 - 41001 - 341,Organisationsenhet(namn och kod):OC Syd - Skånes universitetssjukhus-Lund - Thoraxkliniken",
                                "id": 1548,
                                "data": {
                                    "PosId": 1548,
                                    "PosCode": "4 - 41001 - 341",
                                    "PosName": "OC Syd - Skånes universitetssjukhus-Lund - Thoraxkliniken",
                                    "PosNameWithCode": "OC Syd (4) - Skånes universitetssjukhus-Lund (41001) - Thoraxkliniken (341)",
                                    "PosId4": 1548,
                                    "PosLevel": 3,
                                    "UnitCode": "341",
                                    "TopPosId": 3,
                                    "TopPosCode": "4"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):4 - 41001 - 361,Organisationsenhet(namn och kod):OC Syd - Skånes universitetssjukhus-Lund - Urologiska kliniken",
                                "id": 221,
                                "data": {
                                    "PosId": 221,
                                    "PosCode": "4 - 41001 - 361",
                                    "PosName": "OC Syd - Skånes universitetssjukhus-Lund - Urologiska kliniken",
                                    "PosNameWithCode": "OC Syd (4) - Skånes universitetssjukhus-Lund (41001) - Urologiska kliniken (361)",
                                    "PosId4": 221,
                                    "PosLevel": 3,
                                    "UnitCode": "361",
                                    "TopPosId": 3,
                                    "TopPosCode": "4"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):4 - 41001 - 511,Organisationsenhet(namn och kod):OC Syd - Skånes universitetssjukhus-Lund - Ögonkliniken",
                                "id": 1131,
                                "data": {
                                    "PosId": 1131,
                                    "PosCode": "4 - 41001 - 511",
                                    "PosName": "OC Syd - Skånes universitetssjukhus-Lund - Ögonkliniken",
                                    "PosNameWithCode": "OC Syd (4) - Skånes universitetssjukhus-Lund (41001) - Ögonkliniken (511)",
                                    "PosId4": 1131,
                                    "PosLevel": 3,
                                    "UnitCode": "511",
                                    "TopPosId": 3,
                                    "TopPosCode": "4"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):4 - 41001 - 521,Organisationsenhet(namn och kod):OC Syd - Skånes universitetssjukhus-Lund - ÖNH",
                                "id": 581,
                                "data": {
                                    "PosId": 581,
                                    "PosCode": "4 - 41001 - 521",
                                    "PosName": "OC Syd - Skånes universitetssjukhus-Lund - ÖNH",
                                    "PosNameWithCode": "OC Syd (4) - Skånes universitetssjukhus-Lund (41001) - ÖNH (521)",
                                    "PosId4": 581,
                                    "PosLevel": 3,
                                    "UnitCode": "521",
                                    "TopPosId": 3,
                                    "TopPosCode": "4"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):4 - 30001 - 161,Organisationsenhet(namn och kod):OC Syd - Skånes universitetssjukhus-Malmö - endokrinologisk klinik",
                                "id": 1178,
                                "data": {
                                    "PosId": 1178,
                                    "PosCode": "4 - 30001 - 161",
                                    "PosName": "OC Syd - Skånes universitetssjukhus-Malmö - endokrinologisk klinik",
                                    "PosNameWithCode": "OC Syd (4) - Skånes universitetssjukhus-Malmö (30001) - endokrinologisk klinik (161)",
                                    "PosId4": 1178,
                                    "PosLevel": 3,
                                    "UnitCode": "161",
                                    "TopPosId": 3,
                                    "TopPosCode": "4"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):4 - 30001 - 321,Organisationsenhet(namn och kod):OC Syd - Skånes universitetssjukhus-Malmö - Handkirurgiska klin",
                                "id": 1145,
                                "data": {
                                    "PosId": 1145,
                                    "PosCode": "4 - 30001 - 321",
                                    "PosName": "OC Syd - Skånes universitetssjukhus-Malmö - Handkirurgiska klin",
                                    "PosNameWithCode": "OC Syd (4) - Skånes universitetssjukhus-Malmö (30001) - Handkirurgiska klin (321)",
                                    "PosId4": 1145,
                                    "PosLevel": 3,
                                    "UnitCode": "321",
                                    "TopPosId": 3,
                                    "TopPosCode": "4"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):4 - 30001 - 105,Organisationsenhet(namn och kod):OC Syd - Skånes universitetssjukhus-Malmö - Hematologen",
                                "id": 66,
                                "data": {
                                    "PosId": 66,
                                    "PosCode": "4 - 30001 - 105",
                                    "PosName": "OC Syd - Skånes universitetssjukhus-Malmö - Hematologen",
                                    "PosNameWithCode": "OC Syd (4) - Skånes universitetssjukhus-Malmö (30001) - Hematologen (105)",
                                    "PosId4": 66,
                                    "PosLevel": 3,
                                    "UnitCode": "105",
                                    "TopPosId": 3,
                                    "TopPosCode": "4"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):4 - 30001 - 211,Organisationsenhet(namn och kod):OC Syd - Skånes universitetssjukhus-Malmö - Hudkliniken",
                                "id": 678,
                                "data": {
                                    "PosId": 678,
                                    "PosCode": "4 - 30001 - 211",
                                    "PosName": "OC Syd - Skånes universitetssjukhus-Malmö - Hudkliniken",
                                    "PosNameWithCode": "OC Syd (4) - Skånes universitetssjukhus-Malmö (30001) - Hudkliniken (211)",
                                    "PosId4": 678,
                                    "PosLevel": 3,
                                    "UnitCode": "211",
                                    "TopPosId": 3,
                                    "TopPosCode": "4"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):4 - 30001 - 121,Organisationsenhet(namn och kod):OC Syd - Skånes universitetssjukhus-Malmö - Infektionkliniken",
                                "id": 67,
                                "data": {
                                    "PosId": 67,
                                    "PosCode": "4 - 30001 - 121",
                                    "PosName": "OC Syd - Skånes universitetssjukhus-Malmö - Infektionkliniken",
                                    "PosNameWithCode": "OC Syd (4) - Skånes universitetssjukhus-Malmö (30001) - Infektionkliniken (121)",
                                    "PosId4": 67,
                                    "PosLevel": 3,
                                    "UnitCode": "121",
                                    "TopPosId": 3,
                                    "TopPosCode": "4"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):4 - 30001 - 301,Organisationsenhet(namn och kod):OC Syd - Skånes universitetssjukhus-Malmö - Kirurgiska kliniken",
                                "id": 258,
                                "data": {
                                    "PosId": 258,
                                    "PosCode": "4 - 30001 - 301",
                                    "PosName": "OC Syd - Skånes universitetssjukhus-Malmö - Kirurgiska kliniken",
                                    "PosNameWithCode": "OC Syd (4) - Skånes universitetssjukhus-Malmö (30001) - Kirurgiska kliniken (301)",
                                    "PosId4": 258,
                                    "PosLevel": 3,
                                    "UnitCode": "301",
                                    "TopPosId": 3,
                                    "TopPosCode": "4"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):4 - 30001 - 451,Organisationsenhet(namn och kod):OC Syd - Skånes universitetssjukhus-Malmö - Kvinnoklinik",
                                "id": 730,
                                "data": {
                                    "PosId": 730,
                                    "PosCode": "4 - 30001 - 451",
                                    "PosName": "OC Syd - Skånes universitetssjukhus-Malmö - Kvinnoklinik",
                                    "PosNameWithCode": "OC Syd (4) - Skånes universitetssjukhus-Malmö (30001) - Kvinnoklinik (451)",
                                    "PosId4": 730,
                                    "PosLevel": 3,
                                    "UnitCode": "451",
                                    "TopPosId": 3,
                                    "TopPosCode": "4"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):4 - 30001 - 111,Organisationsenhet(namn och kod):OC Syd - Skånes universitetssjukhus-Malmö - Lung Klin",
                                "id": 687,
                                "data": {
                                    "PosId": 687,
                                    "PosCode": "4 - 30001 - 111",
                                    "PosName": "OC Syd - Skånes universitetssjukhus-Malmö - Lung Klin",
                                    "PosNameWithCode": "OC Syd (4) - Skånes universitetssjukhus-Malmö (30001) - Lung Klin (111)",
                                    "PosId4": 687,
                                    "PosLevel": 3,
                                    "UnitCode": "111",
                                    "TopPosId": 3,
                                    "TopPosCode": "4"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):4 - 30001 - 101,Organisationsenhet(namn och kod):OC Syd - Skånes universitetssjukhus-Malmö - Medicinkliniken",
                                "id": 433,
                                "data": {
                                    "PosId": 433,
                                    "PosCode": "4 - 30001 - 101",
                                    "PosName": "OC Syd - Skånes universitetssjukhus-Malmö - Medicinkliniken",
                                    "PosNameWithCode": "OC Syd (4) - Skånes universitetssjukhus-Malmö (30001) - Medicinkliniken (101)",
                                    "PosId4": 433,
                                    "PosLevel": 3,
                                    "UnitCode": "101",
                                    "TopPosId": 3,
                                    "TopPosCode": "4"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):4 - 30001 - 221,Organisationsenhet(namn och kod):OC Syd - Skånes universitetssjukhus-Malmö - Neurologen",
                                "id": 1279,
                                "data": {
                                    "PosId": 1279,
                                    "PosCode": "4 - 30001 - 221",
                                    "PosName": "OC Syd - Skånes universitetssjukhus-Malmö - Neurologen",
                                    "PosNameWithCode": "OC Syd (4) - Skånes universitetssjukhus-Malmö (30001) - Neurologen (221)",
                                    "PosId4": 1279,
                                    "PosLevel": 3,
                                    "UnitCode": "221",
                                    "TopPosId": 3,
                                    "TopPosCode": "4"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):4 - 30001 - 741,Organisationsenhet(namn och kod):OC Syd - Skånes universitetssjukhus-Malmö - Onkologiska kliniken",
                                "id": 319,
                                "data": {
                                    "PosId": 319,
                                    "PosCode": "4 - 30001 - 741",
                                    "PosName": "OC Syd - Skånes universitetssjukhus-Malmö - Onkologiska kliniken",
                                    "PosNameWithCode": "OC Syd (4) - Skånes universitetssjukhus-Malmö (30001) - Onkologiska kliniken (741)",
                                    "PosId4": 319,
                                    "PosLevel": 3,
                                    "UnitCode": "741",
                                    "TopPosId": 3,
                                    "TopPosCode": "4"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):4 - 30001 - 311,Organisationsenhet(namn och kod):OC Syd - Skånes universitetssjukhus-Malmö - Ortopediska kliniken",
                                "id": 1027,
                                "data": {
                                    "PosId": 1027,
                                    "PosCode": "4 - 30001 - 311",
                                    "PosName": "OC Syd - Skånes universitetssjukhus-Malmö - Ortopediska kliniken",
                                    "PosNameWithCode": "OC Syd (4) - Skånes universitetssjukhus-Malmö (30001) - Ortopediska kliniken (311)",
                                    "PosId4": 1027,
                                    "PosLevel": 3,
                                    "UnitCode": "311",
                                    "TopPosId": 3,
                                    "TopPosCode": "4"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):4 - 30001 - 351,Organisationsenhet(namn och kod):OC Syd - Skånes universitetssjukhus-Malmö - Plastikkirurgi",
                                "id": 723,
                                "data": {
                                    "PosId": 723,
                                    "PosCode": "4 - 30001 - 351",
                                    "PosName": "OC Syd - Skånes universitetssjukhus-Malmö - Plastikkirurgi",
                                    "PosNameWithCode": "OC Syd (4) - Skånes universitetssjukhus-Malmö (30001) - Plastikkirurgi (351)",
                                    "PosId4": 723,
                                    "PosLevel": 3,
                                    "UnitCode": "351",
                                    "TopPosId": 3,
                                    "TopPosCode": "4"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):4 - 30001 - 731,Organisationsenhet(namn och kod):OC Syd - Skånes universitetssjukhus-Malmö - Röntgenavdelningen",
                                "id": 1478,
                                "data": {
                                    "PosId": 1478,
                                    "PosCode": "4 - 30001 - 731",
                                    "PosName": "OC Syd - Skånes universitetssjukhus-Malmö - Röntgenavdelningen",
                                    "PosNameWithCode": "OC Syd (4) - Skånes universitetssjukhus-Malmö (30001) - Röntgenavdelningen (731)",
                                    "PosId4": 1478,
                                    "PosLevel": 3,
                                    "UnitCode": "731",
                                    "TopPosId": 3,
                                    "TopPosCode": "4"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):4 - 30001 - 361,Organisationsenhet(namn och kod):OC Syd - Skånes universitetssjukhus-Malmö - Urologiska kliniken",
                                "id": 222,
                                "data": {
                                    "PosId": 222,
                                    "PosCode": "4 - 30001 - 361",
                                    "PosName": "OC Syd - Skånes universitetssjukhus-Malmö - Urologiska kliniken",
                                    "PosNameWithCode": "OC Syd (4) - Skånes universitetssjukhus-Malmö (30001) - Urologiska kliniken (361)",
                                    "PosId4": 222,
                                    "PosLevel": 3,
                                    "UnitCode": "361",
                                    "TopPosId": 3,
                                    "TopPosCode": "4"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):4 - 419393 - 999,Organisationsenhet(namn och kod):OC Syd - Sophiakliniken - Kliniken saknas",
                                "id": 1466,
                                "data": {
                                    "PosId": 1466,
                                    "PosCode": "4 - 419393 - 999",
                                    "PosName": "OC Syd - Sophiakliniken - Kliniken saknas",
                                    "PosNameWithCode": "OC Syd (4) - Sophiakliniken (419393) - Kliniken saknas (999)",
                                    "PosId4": 1466,
                                    "PosLevel": 3,
                                    "UnitCode": "999",
                                    "TopPosId": 3,
                                    "TopPosCode": "4"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):4 - 41000 - 301,Organisationsenhet(namn och kod):OC Syd - Södra Regionens Läkargrupper - Södra kolorektal grupp",
                                "id": 1176,
                                "data": {
                                    "PosId": 1176,
                                    "PosCode": "4 - 41000 - 301",
                                    "PosName": "OC Syd - Södra Regionens Läkargrupper - Södra kolorektal grupp",
                                    "PosNameWithCode": "OC Syd (4) - Södra Regionens Läkargrupper (41000) - Södra kolorektal grupp (301)",
                                    "PosId4": 1176,
                                    "PosLevel": 3,
                                    "UnitCode": "301",
                                    "TopPosId": 3,
                                    "TopPosCode": "4"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):4 - 41011 - 211,Organisationsenhet(namn och kod):OC Syd - Trelleborgs lasarett - Hudkliniken",
                                "id": 680,
                                "data": {
                                    "PosId": 680,
                                    "PosCode": "4 - 41011 - 211",
                                    "PosName": "OC Syd - Trelleborgs lasarett - Hudkliniken",
                                    "PosNameWithCode": "OC Syd (4) - Trelleborgs lasarett (41011) - Hudkliniken (211)",
                                    "PosId4": 680,
                                    "PosLevel": 3,
                                    "UnitCode": "211",
                                    "TopPosId": 3,
                                    "TopPosCode": "4"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):4 - 41011 - 301,Organisationsenhet(namn och kod):OC Syd - Trelleborgs lasarett - Kirurgiska kliniken",
                                "id": 317,
                                "data": {
                                    "PosId": 317,
                                    "PosCode": "4 - 41011 - 301",
                                    "PosName": "OC Syd - Trelleborgs lasarett - Kirurgiska kliniken",
                                    "PosNameWithCode": "OC Syd (4) - Trelleborgs lasarett (41011) - Kirurgiska kliniken (301)",
                                    "PosId4": 317,
                                    "PosLevel": 3,
                                    "UnitCode": "301",
                                    "TopPosId": 3,
                                    "TopPosCode": "4"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):4 - 41011 - 101,Organisationsenhet(namn och kod):OC Syd - Trelleborgs lasarett - Medicinkliniken",
                                "id": 71,
                                "data": {
                                    "PosId": 71,
                                    "PosCode": "4 - 41011 - 101",
                                    "PosName": "OC Syd - Trelleborgs lasarett - Medicinkliniken",
                                    "PosNameWithCode": "OC Syd (4) - Trelleborgs lasarett (41011) - Medicinkliniken (101)",
                                    "PosId4": 71,
                                    "PosLevel": 3,
                                    "UnitCode": "101",
                                    "TopPosId": 3,
                                    "TopPosCode": "4"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):4 - 41011 - 361,Organisationsenhet(namn och kod):OC Syd - Trelleborgs lasarett - Urologiska kliniken",
                                "id": 793,
                                "data": {
                                    "PosId": 793,
                                    "PosCode": "4 - 41011 - 361",
                                    "PosName": "OC Syd - Trelleborgs lasarett - Urologiska kliniken",
                                    "PosNameWithCode": "OC Syd (4) - Trelleborgs lasarett (41011) - Urologiska kliniken (361)",
                                    "PosId4": 793,
                                    "PosLevel": 3,
                                    "UnitCode": "361",
                                    "TopPosId": 3,
                                    "TopPosCode": "4"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):4 - 41011 - 521,Organisationsenhet(namn och kod):OC Syd - Trelleborgs lasarett - ÖNH",
                                "id": 594,
                                "data": {
                                    "PosId": 594,
                                    "PosCode": "4 - 41011 - 521",
                                    "PosName": "OC Syd - Trelleborgs lasarett - ÖNH",
                                    "PosNameWithCode": "OC Syd (4) - Trelleborgs lasarett (41011) - ÖNH (521)",
                                    "PosId4": 594,
                                    "PosLevel": 3,
                                    "UnitCode": "521",
                                    "TopPosId": 3,
                                    "TopPosCode": "4"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):4 - 40000 - 999,Organisationsenhet(namn och kod):OC Syd - Vårdcentral i Södra regionen - Kliniken saknas",
                                "id": 437,
                                "data": {
                                    "PosId": 437,
                                    "PosCode": "4 - 40000 - 999",
                                    "PosName": "OC Syd - Vårdcentral i Södra regionen - Kliniken saknas",
                                    "PosNameWithCode": "OC Syd (4) - Vårdcentral i Södra regionen (40000) - Kliniken saknas (999)",
                                    "PosId4": 437,
                                    "PosLevel": 3,
                                    "UnitCode": "999",
                                    "TopPosId": 3,
                                    "TopPosCode": "4"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):4 - 41013 - 211,Organisationsenhet(namn och kod):OC Syd - Ystads lasarett - Hudkliniken",
                                "id": 682,
                                "data": {
                                    "PosId": 682,
                                    "PosCode": "4 - 41013 - 211",
                                    "PosName": "OC Syd - Ystads lasarett - Hudkliniken",
                                    "PosNameWithCode": "OC Syd (4) - Ystads lasarett (41013) - Hudkliniken (211)",
                                    "PosId4": 682,
                                    "PosLevel": 3,
                                    "UnitCode": "211",
                                    "TopPosId": 3,
                                    "TopPosCode": "4"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):4 - 41013 - 301,Organisationsenhet(namn och kod):OC Syd - Ystads lasarett - Kirurgiska kliniken",
                                "id": 261,
                                "data": {
                                    "PosId": 261,
                                    "PosCode": "4 - 41013 - 301",
                                    "PosName": "OC Syd - Ystads lasarett - Kirurgiska kliniken",
                                    "PosNameWithCode": "OC Syd (4) - Ystads lasarett (41013) - Kirurgiska kliniken (301)",
                                    "PosId4": 261,
                                    "PosLevel": 3,
                                    "UnitCode": "301",
                                    "TopPosId": 3,
                                    "TopPosCode": "4"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):4 - 41013 - 451,Organisationsenhet(namn och kod):OC Syd - Ystads lasarett - Kvinnoklinik",
                                "id": 1422,
                                "data": {
                                    "PosId": 1422,
                                    "PosCode": "4 - 41013 - 451",
                                    "PosName": "OC Syd - Ystads lasarett - Kvinnoklinik",
                                    "PosNameWithCode": "OC Syd (4) - Ystads lasarett (41013) - Kvinnoklinik (451)",
                                    "PosId4": 1422,
                                    "PosLevel": 3,
                                    "UnitCode": "451",
                                    "TopPosId": 3,
                                    "TopPosCode": "4"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):4 - 41013 - 101,Organisationsenhet(namn och kod):OC Syd - Ystads lasarett - Medicinkliniken",
                                "id": 69,
                                "data": {
                                    "PosId": 69,
                                    "PosCode": "4 - 41013 - 101",
                                    "PosName": "OC Syd - Ystads lasarett - Medicinkliniken",
                                    "PosNameWithCode": "OC Syd (4) - Ystads lasarett (41013) - Medicinkliniken (101)",
                                    "PosId4": 69,
                                    "PosLevel": 3,
                                    "UnitCode": "101",
                                    "TopPosId": 3,
                                    "TopPosCode": "4"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):4 - 41013 - 361,Organisationsenhet(namn och kod):OC Syd - Ystads lasarett - Urologiska kliniken",
                                "id": 795,
                                "data": {
                                    "PosId": 795,
                                    "PosCode": "4 - 41013 - 361",
                                    "PosName": "OC Syd - Ystads lasarett - Urologiska kliniken",
                                    "PosNameWithCode": "OC Syd (4) - Ystads lasarett (41013) - Urologiska kliniken (361)",
                                    "PosId4": 795,
                                    "PosLevel": 3,
                                    "UnitCode": "361",
                                    "TopPosId": 3,
                                    "TopPosCode": "4"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):4 - 41013 - 521,Organisationsenhet(namn och kod):OC Syd - Ystads lasarett - ÖNH",
                                "id": 595,
                                "data": {
                                    "PosId": 595,
                                    "PosCode": "4 - 41013 - 521",
                                    "PosName": "OC Syd - Ystads lasarett - ÖNH",
                                    "PosNameWithCode": "OC Syd (4) - Ystads lasarett (41013) - ÖNH (521)",
                                    "PosId4": 595,
                                    "PosLevel": 3,
                                    "UnitCode": "521",
                                    "TopPosId": 3,
                                    "TopPosCode": "4"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):4 - 28011 - 211,Organisationsenhet(namn och kod):OC Syd - Ängelholms sjukhus - Hudkliniken",
                                "id": 676,
                                "data": {
                                    "PosId": 676,
                                    "PosCode": "4 - 28011 - 211",
                                    "PosName": "OC Syd - Ängelholms sjukhus - Hudkliniken",
                                    "PosNameWithCode": "OC Syd (4) - Ängelholms sjukhus (28011) - Hudkliniken (211)",
                                    "PosId4": 676,
                                    "PosLevel": 3,
                                    "UnitCode": "211",
                                    "TopPosId": 3,
                                    "TopPosCode": "4"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):4 - 28011 - 301,Organisationsenhet(namn och kod):OC Syd - Ängelholms sjukhus - Kirurgiska kliniken",
                                "id": 318,
                                "data": {
                                    "PosId": 318,
                                    "PosCode": "4 - 28011 - 301",
                                    "PosName": "OC Syd - Ängelholms sjukhus - Kirurgiska kliniken",
                                    "PosNameWithCode": "OC Syd (4) - Ängelholms sjukhus (28011) - Kirurgiska kliniken (301)",
                                    "PosId4": 318,
                                    "PosLevel": 3,
                                    "UnitCode": "301",
                                    "TopPosId": 3,
                                    "TopPosCode": "4"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):4 - 28011 - 451,Organisationsenhet(namn och kod):OC Syd - Ängelholms sjukhus - Kvinnoklinik",
                                "id": 729,
                                "data": {
                                    "PosId": 729,
                                    "PosCode": "4 - 28011 - 451",
                                    "PosName": "OC Syd - Ängelholms sjukhus - Kvinnoklinik",
                                    "PosNameWithCode": "OC Syd (4) - Ängelholms sjukhus (28011) - Kvinnoklinik (451)",
                                    "PosId4": 729,
                                    "PosLevel": 3,
                                    "UnitCode": "451",
                                    "TopPosId": 3,
                                    "TopPosCode": "4"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):4 - 28011 - 101,Organisationsenhet(namn och kod):OC Syd - Ängelholms sjukhus - Medicinkliniken",
                                "id": 74,
                                "data": {
                                    "PosId": 74,
                                    "PosCode": "4 - 28011 - 101",
                                    "PosName": "OC Syd - Ängelholms sjukhus - Medicinkliniken",
                                    "PosNameWithCode": "OC Syd (4) - Ängelholms sjukhus (28011) - Medicinkliniken (101)",
                                    "PosId4": 74,
                                    "PosLevel": 3,
                                    "UnitCode": "101",
                                    "TopPosId": 3,
                                    "TopPosCode": "4"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):4 - 28011 - 521,Organisationsenhet(namn och kod):OC Syd - Ängelholms sjukhus - ÖNH",
                                "id": 591,
                                "data": {
                                    "PosId": 591,
                                    "PosCode": "4 - 28011 - 521",
                                    "PosName": "OC Syd - Ängelholms sjukhus - ÖNH",
                                    "PosNameWithCode": "OC Syd (4) - Ängelholms sjukhus (28011) - ÖNH (521)",
                                    "PosId4": 591,
                                    "PosLevel": 3,
                                    "UnitCode": "521",
                                    "TopPosId": 3,
                                    "TopPosCode": "4"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):3 - 22011 - 301,Organisationsenhet(namn och kod):OC Sydost - Eksjö - Kirurgkliniken",
                                "id": 395,
                                "data": {
                                    "PosId": 395,
                                    "PosCode": "3 - 22011 - 301",
                                    "PosName": "OC Sydost - Eksjö - Kirurgkliniken",
                                    "PosNameWithCode": "OC Sydost (3) - Eksjö (22011) - Kirurgkliniken (301)",
                                    "PosId4": 395,
                                    "PosLevel": 3,
                                    "UnitCode": "301",
                                    "TopPosId": 4,
                                    "TopPosCode": "3"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):3 - 22011 - 451,Organisationsenhet(namn och kod):OC Sydost - Eksjö - Kvinnokliniken",
                                "id": 753,
                                "data": {
                                    "PosId": 753,
                                    "PosCode": "3 - 22011 - 451",
                                    "PosName": "OC Sydost - Eksjö - Kvinnokliniken",
                                    "PosNameWithCode": "OC Sydost (3) - Eksjö (22011) - Kvinnokliniken (451)",
                                    "PosId4": 753,
                                    "PosLevel": 3,
                                    "UnitCode": "451",
                                    "TopPosId": 4,
                                    "TopPosCode": "3"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):3 - 22011 - 101,Organisationsenhet(namn och kod):OC Sydost - Eksjö - Medicinkliniken",
                                "id": 396,
                                "data": {
                                    "PosId": 396,
                                    "PosCode": "3 - 22011 - 101",
                                    "PosName": "OC Sydost - Eksjö - Medicinkliniken",
                                    "PosNameWithCode": "OC Sydost (3) - Eksjö (22011) - Medicinkliniken (101)",
                                    "PosId4": 396,
                                    "PosLevel": 3,
                                    "UnitCode": "101",
                                    "TopPosId": 4,
                                    "TopPosCode": "3"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):3 - 22011 - 731,Organisationsenhet(namn och kod):OC Sydost - Eksjö - Röntgen",
                                "id": 1483,
                                "data": {
                                    "PosId": 1483,
                                    "PosCode": "3 - 22011 - 731",
                                    "PosName": "OC Sydost - Eksjö - Röntgen",
                                    "PosNameWithCode": "OC Sydost (3) - Eksjö (22011) - Röntgen (731)",
                                    "PosId4": 1483,
                                    "PosLevel": 3,
                                    "UnitCode": "731",
                                    "TopPosId": 4,
                                    "TopPosCode": "3"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):3 - 22011 - 361,Organisationsenhet(namn och kod):OC Sydost - Eksjö - Urologiska kliniken",
                                "id": 241,
                                "data": {
                                    "PosId": 241,
                                    "PosCode": "3 - 22011 - 361",
                                    "PosName": "OC Sydost - Eksjö - Urologiska kliniken",
                                    "PosNameWithCode": "OC Sydost (3) - Eksjö (22011) - Urologiska kliniken (361)",
                                    "PosId4": 241,
                                    "PosLevel": 3,
                                    "UnitCode": "361",
                                    "TopPosId": 4,
                                    "TopPosCode": "3"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):3 - 39999 - 999,Organisationsenhet(namn och kod):OC Sydost - Enhet utan INCA-rapportör - Klinik",
                                "id": 452,
                                "data": {
                                    "PosId": 452,
                                    "PosCode": "3 - 39999 - 999",
                                    "PosName": "OC Sydost - Enhet utan INCA-rapportör - Klinik",
                                    "PosNameWithCode": "OC Sydost (3) - Enhet utan INCA-rapportör (39999) - Klinik (999)",
                                    "PosId4": 452,
                                    "PosLevel": 3,
                                    "UnitCode": "999",
                                    "TopPosId": 4,
                                    "TopPosCode": "3"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):3 - 21011 - 301,Organisationsenhet(namn och kod):OC Sydost - Finspång - Kirurgkliniken",
                                "id": 408,
                                "data": {
                                    "PosId": 408,
                                    "PosCode": "3 - 21011 - 301",
                                    "PosName": "OC Sydost - Finspång - Kirurgkliniken",
                                    "PosNameWithCode": "OC Sydost (3) - Finspång (21011) - Kirurgkliniken (301)",
                                    "PosId4": 408,
                                    "PosLevel": 3,
                                    "UnitCode": "301",
                                    "TopPosId": 4,
                                    "TopPosCode": "3"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):3 - 21011 - 361,Organisationsenhet(namn och kod):OC Sydost - Finspång - Urologkliniken",
                                "id": 444,
                                "data": {
                                    "PosId": 444,
                                    "PosCode": "3 - 21011 - 361",
                                    "PosName": "OC Sydost - Finspång - Urologkliniken",
                                    "PosNameWithCode": "OC Sydost (3) - Finspång (21011) - Urologkliniken (361)",
                                    "PosId4": 444,
                                    "PosLevel": 3,
                                    "UnitCode": "361",
                                    "TopPosId": 4,
                                    "TopPosCode": "3"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):3 - 22010 - 211,Organisationsenhet(namn och kod):OC Sydost - Jönköping - Hudkliniken",
                                "id": 763,
                                "data": {
                                    "PosId": 763,
                                    "PosCode": "3 - 22010 - 211",
                                    "PosName": "OC Sydost - Jönköping - Hudkliniken",
                                    "PosNameWithCode": "OC Sydost (3) - Jönköping (22010) - Hudkliniken (211)",
                                    "PosId4": 763,
                                    "PosLevel": 3,
                                    "UnitCode": "211",
                                    "TopPosId": 4,
                                    "TopPosCode": "3"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):3 - 22010 - 301,Organisationsenhet(namn och kod):OC Sydost - Jönköping - Kirurgkliniken",
                                "id": 393,
                                "data": {
                                    "PosId": 393,
                                    "PosCode": "3 - 22010 - 301",
                                    "PosName": "OC Sydost - Jönköping - Kirurgkliniken",
                                    "PosNameWithCode": "OC Sydost (3) - Jönköping (22010) - Kirurgkliniken (301)",
                                    "PosId4": 393,
                                    "PosLevel": 3,
                                    "UnitCode": "301",
                                    "TopPosId": 4,
                                    "TopPosCode": "3"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):3 - 22010 - 451,Organisationsenhet(namn och kod):OC Sydost - Jönköping - Kvinnokliniken",
                                "id": 752,
                                "data": {
                                    "PosId": 752,
                                    "PosCode": "3 - 22010 - 451",
                                    "PosName": "OC Sydost - Jönköping - Kvinnokliniken",
                                    "PosNameWithCode": "OC Sydost (3) - Jönköping (22010) - Kvinnokliniken (451)",
                                    "PosId4": 752,
                                    "PosLevel": 3,
                                    "UnitCode": "451",
                                    "TopPosId": 4,
                                    "TopPosCode": "3"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):3 - 22010 - 101,Organisationsenhet(namn och kod):OC Sydost - Jönköping - Medicinkliniken",
                                "id": 394,
                                "data": {
                                    "PosId": 394,
                                    "PosCode": "3 - 22010 - 101",
                                    "PosName": "OC Sydost - Jönköping - Medicinkliniken",
                                    "PosNameWithCode": "OC Sydost (3) - Jönköping (22010) - Medicinkliniken (101)",
                                    "PosId4": 394,
                                    "PosLevel": 3,
                                    "UnitCode": "101",
                                    "TopPosId": 4,
                                    "TopPosCode": "3"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):3 - 22010 - 741,Organisationsenhet(namn och kod):OC Sydost - Jönköping - Onkologen",
                                "id": 392,
                                "data": {
                                    "PosId": 392,
                                    "PosCode": "3 - 22010 - 741",
                                    "PosName": "OC Sydost - Jönköping - Onkologen",
                                    "PosNameWithCode": "OC Sydost (3) - Jönköping (22010) - Onkologen (741)",
                                    "PosId4": 392,
                                    "PosLevel": 3,
                                    "UnitCode": "741",
                                    "TopPosId": 4,
                                    "TopPosCode": "3"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):3 - 22010 - 731,Organisationsenhet(namn och kod):OC Sydost - Jönköping - Röntgen",
                                "id": 1484,
                                "data": {
                                    "PosId": 1484,
                                    "PosCode": "3 - 22010 - 731",
                                    "PosName": "OC Sydost - Jönköping - Röntgen",
                                    "PosNameWithCode": "OC Sydost (3) - Jönköping (22010) - Röntgen (731)",
                                    "PosId4": 1484,
                                    "PosLevel": 3,
                                    "UnitCode": "731",
                                    "TopPosId": 4,
                                    "TopPosCode": "3"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):3 - 22010 - 361,Organisationsenhet(namn och kod):OC Sydost - Jönköping - Urologiska kliniken",
                                "id": 240,
                                "data": {
                                    "PosId": 240,
                                    "PosCode": "3 - 22010 - 361",
                                    "PosName": "OC Sydost - Jönköping - Urologiska kliniken",
                                    "PosNameWithCode": "OC Sydost (3) - Jönköping (22010) - Urologiska kliniken (361)",
                                    "PosId4": 240,
                                    "PosLevel": 3,
                                    "UnitCode": "361",
                                    "TopPosId": 4,
                                    "TopPosCode": "3"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):3 - 22010 - 521,Organisationsenhet(namn och kod):OC Sydost - Jönköping - ÖNH",
                                "id": 569,
                                "data": {
                                    "PosId": 569,
                                    "PosCode": "3 - 22010 - 521",
                                    "PosName": "OC Sydost - Jönköping - ÖNH",
                                    "PosNameWithCode": "OC Sydost (3) - Jönköping (22010) - ÖNH (521)",
                                    "PosId4": 569,
                                    "PosLevel": 3,
                                    "UnitCode": "521",
                                    "TopPosId": 4,
                                    "TopPosCode": "3"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):3 - 25010 - 211,Organisationsenhet(namn och kod):OC Sydost - Kalmar - Hudklinik",
                                "id": 765,
                                "data": {
                                    "PosId": 765,
                                    "PosCode": "3 - 25010 - 211",
                                    "PosName": "OC Sydost - Kalmar - Hudklinik",
                                    "PosNameWithCode": "OC Sydost (3) - Kalmar (25010) - Hudklinik (211)",
                                    "PosId4": 765,
                                    "PosLevel": 3,
                                    "UnitCode": "211",
                                    "TopPosId": 4,
                                    "TopPosCode": "3"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):3 - 25010 - 301,Organisationsenhet(namn och kod):OC Sydost - Kalmar - Kirurgkliniken",
                                "id": 397,
                                "data": {
                                    "PosId": 397,
                                    "PosCode": "3 - 25010 - 301",
                                    "PosName": "OC Sydost - Kalmar - Kirurgkliniken",
                                    "PosNameWithCode": "OC Sydost (3) - Kalmar (25010) - Kirurgkliniken (301)",
                                    "PosId4": 397,
                                    "PosLevel": 3,
                                    "UnitCode": "301",
                                    "TopPosId": 4,
                                    "TopPosCode": "3"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):3 - 25010 - 451,Organisationsenhet(namn och kod):OC Sydost - Kalmar - Kvinnokliniken",
                                "id": 754,
                                "data": {
                                    "PosId": 754,
                                    "PosCode": "3 - 25010 - 451",
                                    "PosName": "OC Sydost - Kalmar - Kvinnokliniken",
                                    "PosNameWithCode": "OC Sydost (3) - Kalmar (25010) - Kvinnokliniken (451)",
                                    "PosId4": 754,
                                    "PosLevel": 3,
                                    "UnitCode": "451",
                                    "TopPosId": 4,
                                    "TopPosCode": "3"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):3 - 25010 - 101,Organisationsenhet(namn och kod):OC Sydost - Kalmar - Medicinkliniken",
                                "id": 398,
                                "data": {
                                    "PosId": 398,
                                    "PosCode": "3 - 25010 - 101",
                                    "PosName": "OC Sydost - Kalmar - Medicinkliniken",
                                    "PosNameWithCode": "OC Sydost (3) - Kalmar (25010) - Medicinkliniken (101)",
                                    "PosId4": 398,
                                    "PosLevel": 3,
                                    "UnitCode": "101",
                                    "TopPosId": 4,
                                    "TopPosCode": "3"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):3 - 25010 - 741,Organisationsenhet(namn och kod):OC Sydost - Kalmar - Onkologen",
                                "id": 1213,
                                "data": {
                                    "PosId": 1213,
                                    "PosCode": "3 - 25010 - 741",
                                    "PosName": "OC Sydost - Kalmar - Onkologen",
                                    "PosNameWithCode": "OC Sydost (3) - Kalmar (25010) - Onkologen (741)",
                                    "PosId4": 1213,
                                    "PosLevel": 3,
                                    "UnitCode": "741",
                                    "TopPosId": 4,
                                    "TopPosCode": "3"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):3 - 25010 - 731,Organisationsenhet(namn och kod):OC Sydost - Kalmar - Radiologiska kliniken",
                                "id": 1487,
                                "data": {
                                    "PosId": 1487,
                                    "PosCode": "3 - 25010 - 731",
                                    "PosName": "OC Sydost - Kalmar - Radiologiska kliniken",
                                    "PosNameWithCode": "OC Sydost (3) - Kalmar (25010) - Radiologiska kliniken (731)",
                                    "PosId4": 1487,
                                    "PosLevel": 3,
                                    "UnitCode": "731",
                                    "TopPosId": 4,
                                    "TopPosCode": "3"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):3 - 25010 - 361,Organisationsenhet(namn och kod):OC Sydost - Kalmar - Urologiska kliniken",
                                "id": 242,
                                "data": {
                                    "PosId": 242,
                                    "PosCode": "3 - 25010 - 361",
                                    "PosName": "OC Sydost - Kalmar - Urologiska kliniken",
                                    "PosNameWithCode": "OC Sydost (3) - Kalmar (25010) - Urologiska kliniken (361)",
                                    "PosId4": 242,
                                    "PosLevel": 3,
                                    "UnitCode": "361",
                                    "TopPosId": 4,
                                    "TopPosCode": "3"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):3 - 25010 - 521,Organisationsenhet(namn och kod):OC Sydost - Kalmar - ÖNH",
                                "id": 570,
                                "data": {
                                    "PosId": 570,
                                    "PosCode": "3 - 25010 - 521",
                                    "PosName": "OC Sydost - Kalmar - ÖNH",
                                    "PosNameWithCode": "OC Sydost (3) - Kalmar (25010) - ÖNH (521)",
                                    "PosId4": 570,
                                    "PosLevel": 3,
                                    "UnitCode": "521",
                                    "TopPosId": 4,
                                    "TopPosCode": "3"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):3 - 21001 - 201,Organisationsenhet(namn och kod):OC Sydost - Linköping US - Barnklinik",
                                "id": 1044,
                                "data": {
                                    "PosId": 1044,
                                    "PosCode": "3 - 21001 - 201",
                                    "PosName": "OC Sydost - Linköping US - Barnklinik",
                                    "PosNameWithCode": "OC Sydost (3) - Linköping US (21001) - Barnklinik (201)",
                                    "PosId4": 1044,
                                    "PosLevel": 3,
                                    "UnitCode": "201",
                                    "TopPosId": 4,
                                    "TopPosCode": "3"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):3 - 21001 - 161,Organisationsenhet(namn och kod):OC Sydost - Linköping US - Endokrinkirurgiska kliniken",
                                "id": 303,
                                "data": {
                                    "PosId": 303,
                                    "PosCode": "3 - 21001 - 161",
                                    "PosName": "OC Sydost - Linköping US - Endokrinkirurgiska kliniken",
                                    "PosNameWithCode": "OC Sydost (3) - Linköping US (21001) - Endokrinkirurgiska kliniken (161)",
                                    "PosId4": 303,
                                    "PosLevel": 3,
                                    "UnitCode": "161",
                                    "TopPosId": 4,
                                    "TopPosCode": "3"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):3 - 21001 - 351,Organisationsenhet(namn och kod):OC Sydost - Linköping US - Hand- och plastikkirurgi",
                                "id": 762,
                                "data": {
                                    "PosId": 762,
                                    "PosCode": "3 - 21001 - 351",
                                    "PosName": "OC Sydost - Linköping US - Hand- och plastikkirurgi",
                                    "PosNameWithCode": "OC Sydost (3) - Linköping US (21001) - Hand- och plastikkirurgi (351)",
                                    "PosId4": 762,
                                    "PosLevel": 3,
                                    "UnitCode": "351",
                                    "TopPosId": 4,
                                    "TopPosCode": "3"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):3 - 21001 - 666,Organisationsenhet(namn och kod):OC Sydost - Linköping US - Hematologiska kliniken",
                                "id": 522,
                                "data": {
                                    "PosId": 522,
                                    "PosCode": "3 - 21001 - 666",
                                    "PosName": "OC Sydost - Linköping US - Hematologiska kliniken",
                                    "PosNameWithCode": "OC Sydost (3) - Linköping US (21001) - Hematologiska kliniken (666)",
                                    "PosId4": 522,
                                    "PosLevel": 3,
                                    "UnitCode": "666",
                                    "TopPosId": 4,
                                    "TopPosCode": "3"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):3 - 21001 - 211,Organisationsenhet(namn och kod):OC Sydost - Linköping US - Hudkliniken",
                                "id": 761,
                                "data": {
                                    "PosId": 761,
                                    "PosCode": "3 - 21001 - 211",
                                    "PosName": "OC Sydost - Linköping US - Hudkliniken",
                                    "PosNameWithCode": "OC Sydost (3) - Linköping US (21001) - Hudkliniken (211)",
                                    "PosId4": 761,
                                    "PosLevel": 3,
                                    "UnitCode": "211",
                                    "TopPosId": 4,
                                    "TopPosCode": "3"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):3 - 21001 - 301,Organisationsenhet(namn och kod):OC Sydost - Linköping US - Kirurgkliniken",
                                "id": 64,
                                "data": {
                                    "PosId": 64,
                                    "PosCode": "3 - 21001 - 301",
                                    "PosName": "OC Sydost - Linköping US - Kirurgkliniken",
                                    "PosNameWithCode": "OC Sydost (3) - Linköping US (21001) - Kirurgkliniken (301)",
                                    "PosId4": 64,
                                    "PosLevel": 3,
                                    "UnitCode": "301",
                                    "TopPosId": 4,
                                    "TopPosCode": "3"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):3 - 21001 - 667,Organisationsenhet(namn och kod):OC Sydost - Linköping US - Kolorektalkirurgi",
                                "id": 391,
                                "data": {
                                    "PosId": 391,
                                    "PosCode": "3 - 21001 - 667",
                                    "PosName": "OC Sydost - Linköping US - Kolorektalkirurgi",
                                    "PosNameWithCode": "OC Sydost (3) - Linköping US (21001) - Kolorektalkirurgi (667)",
                                    "PosId4": 391,
                                    "PosLevel": 3,
                                    "UnitCode": "667",
                                    "TopPosId": 4,
                                    "TopPosCode": "3"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):3 - 21001 - 451,Organisationsenhet(namn och kod):OC Sydost - Linköping US - Kvinnokliniken",
                                "id": 751,
                                "data": {
                                    "PosId": 751,
                                    "PosCode": "3 - 21001 - 451",
                                    "PosName": "OC Sydost - Linköping US - Kvinnokliniken",
                                    "PosNameWithCode": "OC Sydost (3) - Linköping US (21001) - Kvinnokliniken (451)",
                                    "PosId4": 751,
                                    "PosLevel": 3,
                                    "UnitCode": "451",
                                    "TopPosId": 4,
                                    "TopPosCode": "3"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):3 - 21001 - 111,Organisationsenhet(namn och kod):OC Sydost - Linköping US - Lungmedicinska",
                                "id": 622,
                                "data": {
                                    "PosId": 622,
                                    "PosCode": "3 - 21001 - 111",
                                    "PosName": "OC Sydost - Linköping US - Lungmedicinska",
                                    "PosNameWithCode": "OC Sydost (3) - Linköping US (21001) - Lungmedicinska (111)",
                                    "PosId4": 622,
                                    "PosLevel": 3,
                                    "UnitCode": "111",
                                    "TopPosId": 4,
                                    "TopPosCode": "3"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):3 - 21001 - 101,Organisationsenhet(namn och kod):OC Sydost - Linköping US - Medicinkliniken",
                                "id": 390,
                                "data": {
                                    "PosId": 390,
                                    "PosCode": "3 - 21001 - 101",
                                    "PosName": "OC Sydost - Linköping US - Medicinkliniken",
                                    "PosNameWithCode": "OC Sydost (3) - Linköping US (21001) - Medicinkliniken (101)",
                                    "PosId4": 390,
                                    "PosLevel": 3,
                                    "UnitCode": "101",
                                    "TopPosId": 4,
                                    "TopPosCode": "3"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):3 - 21001 - 331,Organisationsenhet(namn och kod):OC Sydost - Linköping US - Neurokirurgiska kliniken",
                                "id": 809,
                                "data": {
                                    "PosId": 809,
                                    "PosCode": "3 - 21001 - 331",
                                    "PosName": "OC Sydost - Linköping US - Neurokirurgiska kliniken",
                                    "PosNameWithCode": "OC Sydost (3) - Linköping US (21001) - Neurokirurgiska kliniken (331)",
                                    "PosId4": 809,
                                    "PosLevel": 3,
                                    "UnitCode": "331",
                                    "TopPosId": 4,
                                    "TopPosCode": "3"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):3 - 21001 - 221,Organisationsenhet(namn och kod):OC Sydost - Linköping US - Neurologen",
                                "id": 1460,
                                "data": {
                                    "PosId": 1460,
                                    "PosCode": "3 - 21001 - 221",
                                    "PosName": "OC Sydost - Linköping US - Neurologen",
                                    "PosNameWithCode": "OC Sydost (3) - Linköping US (21001) - Neurologen (221)",
                                    "PosId4": 1460,
                                    "PosLevel": 3,
                                    "UnitCode": "221",
                                    "TopPosId": 4,
                                    "TopPosCode": "3"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):3 - 21001 - 741,Organisationsenhet(namn och kod):OC Sydost - Linköping US - Onkologen",
                                "id": 389,
                                "data": {
                                    "PosId": 389,
                                    "PosCode": "3 - 21001 - 741",
                                    "PosName": "OC Sydost - Linköping US - Onkologen",
                                    "PosNameWithCode": "OC Sydost (3) - Linköping US (21001) - Onkologen (741)",
                                    "PosId4": 389,
                                    "PosLevel": 3,
                                    "UnitCode": "741",
                                    "TopPosId": 4,
                                    "TopPosCode": "3"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):3 - 21001 - 311,Organisationsenhet(namn och kod):OC Sydost - Linköping US - Ortopedkliniken",
                                "id": 1393,
                                "data": {
                                    "PosId": 1393,
                                    "PosCode": "3 - 21001 - 311",
                                    "PosName": "OC Sydost - Linköping US - Ortopedkliniken",
                                    "PosNameWithCode": "OC Sydost (3) - Linköping US (21001) - Ortopedkliniken (311)",
                                    "PosId4": 1393,
                                    "PosLevel": 3,
                                    "UnitCode": "311",
                                    "TopPosId": 4,
                                    "TopPosCode": "3"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):3 - 21001 - 000,Organisationsenhet(namn och kod):OC Sydost - Linköping US - RegCNS",
                                "id": 1127,
                                "data": {
                                    "PosId": 1127,
                                    "PosCode": "3 - 21001 - 000",
                                    "PosName": "OC Sydost - Linköping US - RegCNS",
                                    "PosNameWithCode": "OC Sydost (3) - Linköping US (21001) - RegCNS (000)",
                                    "PosId4": 1127,
                                    "PosLevel": 3,
                                    "UnitCode": "000",
                                    "TopPosId": 4,
                                    "TopPosCode": "3"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):3 - 21001 - 731,Organisationsenhet(namn och kod):OC Sydost - Linköping US - Röntgenkliniken",
                                "id": 1488,
                                "data": {
                                    "PosId": 1488,
                                    "PosCode": "3 - 21001 - 731",
                                    "PosName": "OC Sydost - Linköping US - Röntgenkliniken",
                                    "PosNameWithCode": "OC Sydost (3) - Linköping US (21001) - Röntgenkliniken (731)",
                                    "PosId4": 1488,
                                    "PosLevel": 3,
                                    "UnitCode": "731",
                                    "TopPosId": 4,
                                    "TopPosCode": "3"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):3 - 21001 - 665,Organisationsenhet(namn och kod):OC Sydost - Linköping US - Sektionen för övre abdominell kirurgi",
                                "id": 302,
                                "data": {
                                    "PosId": 302,
                                    "PosCode": "3 - 21001 - 665",
                                    "PosName": "OC Sydost - Linköping US - Sektionen för övre abdominell kirurgi",
                                    "PosNameWithCode": "OC Sydost (3) - Linköping US (21001) - Sektionen för övre abdominell kirurgi (665)",
                                    "PosId4": 302,
                                    "PosLevel": 3,
                                    "UnitCode": "665",
                                    "TopPosId": 4,
                                    "TopPosCode": "3"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):3 - 21001 - 341,Organisationsenhet(namn och kod):OC Sydost - Linköping US - Thorax-kärlkliniken",
                                "id": 1550,
                                "data": {
                                    "PosId": 1550,
                                    "PosCode": "3 - 21001 - 341",
                                    "PosName": "OC Sydost - Linköping US - Thorax-kärlkliniken",
                                    "PosNameWithCode": "OC Sydost (3) - Linköping US (21001) - Thorax-kärlkliniken (341)",
                                    "PosId4": 1550,
                                    "PosLevel": 3,
                                    "UnitCode": "341",
                                    "TopPosId": 4,
                                    "TopPosCode": "3"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):3 - 21001 - 361,Organisationsenhet(namn och kod):OC Sydost - Linköping US - Urologkliniken",
                                "id": 441,
                                "data": {
                                    "PosId": 441,
                                    "PosCode": "3 - 21001 - 361",
                                    "PosName": "OC Sydost - Linköping US - Urologkliniken",
                                    "PosNameWithCode": "OC Sydost (3) - Linköping US (21001) - Urologkliniken (361)",
                                    "PosId4": 441,
                                    "PosLevel": 3,
                                    "UnitCode": "361",
                                    "TopPosId": 4,
                                    "TopPosCode": "3"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):3 - 21001 - 521,Organisationsenhet(namn och kod):OC Sydost - Linköping US - ÖNH",
                                "id": 568,
                                "data": {
                                    "PosId": 568,
                                    "PosCode": "3 - 21001 - 521",
                                    "PosName": "OC Sydost - Linköping US - ÖNH",
                                    "PosNameWithCode": "OC Sydost (3) - Linköping US (21001) - ÖNH (521)",
                                    "PosId4": 568,
                                    "PosLevel": 3,
                                    "UnitCode": "521",
                                    "TopPosId": 4,
                                    "TopPosCode": "3"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):3 - 211991 - 361,Organisationsenhet(namn och kod):OC Sydost - Läkarhuset, Norrköping - Urologiska kliniken",
                                "id": 249,
                                "data": {
                                    "PosId": 249,
                                    "PosCode": "3 - 211991 - 361",
                                    "PosName": "OC Sydost - Läkarhuset, Norrköping - Urologiska kliniken",
                                    "PosNameWithCode": "OC Sydost (3) - Läkarhuset, Norrköping (211991) - Urologiska kliniken (361)",
                                    "PosId4": 249,
                                    "PosLevel": 3,
                                    "UnitCode": "361",
                                    "TopPosId": 4,
                                    "TopPosCode": "3"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):3 - 211994 - 999,Organisationsenhet(namn och kod):OC Sydost - Medicinskt Centrum - Klinik",
                                "id": 998,
                                "data": {
                                    "PosId": 998,
                                    "PosCode": "3 - 211994 - 999",
                                    "PosName": "OC Sydost - Medicinskt Centrum - Klinik",
                                    "PosNameWithCode": "OC Sydost (3) - Medicinskt Centrum (211994) - Klinik (999)",
                                    "PosId4": 998,
                                    "PosLevel": 3,
                                    "UnitCode": "999",
                                    "TopPosId": 4,
                                    "TopPosCode": "3"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):3 - 21014 - 309,Organisationsenhet(namn och kod):OC Sydost - Motala - Aleris kirurgi",
                                "id": 960,
                                "data": {
                                    "PosId": 960,
                                    "PosCode": "3 - 21014 - 309",
                                    "PosName": "OC Sydost - Motala - Aleris kirurgi",
                                    "PosNameWithCode": "OC Sydost (3) - Motala (21014) - Aleris kirurgi (309)",
                                    "PosId4": 960,
                                    "PosLevel": 3,
                                    "UnitCode": "309",
                                    "TopPosId": 4,
                                    "TopPosCode": "3"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):3 - 21014 - 301,Organisationsenhet(namn och kod):OC Sydost - Motala - Kirurgkliniken",
                                "id": 406,
                                "data": {
                                    "PosId": 406,
                                    "PosCode": "3 - 21014 - 301",
                                    "PosName": "OC Sydost - Motala - Kirurgkliniken",
                                    "PosNameWithCode": "OC Sydost (3) - Motala (21014) - Kirurgkliniken (301)",
                                    "PosId4": 406,
                                    "PosLevel": 3,
                                    "UnitCode": "301",
                                    "TopPosId": 4,
                                    "TopPosCode": "3"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):3 - 21014 - 451,Organisationsenhet(namn och kod):OC Sydost - Motala - Kvinnokliniken",
                                "id": 759,
                                "data": {
                                    "PosId": 759,
                                    "PosCode": "3 - 21014 - 451",
                                    "PosName": "OC Sydost - Motala - Kvinnokliniken",
                                    "PosNameWithCode": "OC Sydost (3) - Motala (21014) - Kvinnokliniken (451)",
                                    "PosId4": 759,
                                    "PosLevel": 3,
                                    "UnitCode": "451",
                                    "TopPosId": 4,
                                    "TopPosCode": "3"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):3 - 21014 - 101,Organisationsenhet(namn och kod):OC Sydost - Motala - Medicinkliniken",
                                "id": 407,
                                "data": {
                                    "PosId": 407,
                                    "PosCode": "3 - 21014 - 101",
                                    "PosName": "OC Sydost - Motala - Medicinkliniken",
                                    "PosNameWithCode": "OC Sydost (3) - Motala (21014) - Medicinkliniken (101)",
                                    "PosId4": 407,
                                    "PosLevel": 3,
                                    "UnitCode": "101",
                                    "TopPosId": 4,
                                    "TopPosCode": "3"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):3 - 21014 - 731,Organisationsenhet(namn och kod):OC Sydost - Motala - Röntgenkliniken",
                                "id": 1490,
                                "data": {
                                    "PosId": 1490,
                                    "PosCode": "3 - 21014 - 731",
                                    "PosName": "OC Sydost - Motala - Röntgenkliniken",
                                    "PosNameWithCode": "OC Sydost (3) - Motala (21014) - Röntgenkliniken (731)",
                                    "PosId4": 1490,
                                    "PosLevel": 3,
                                    "UnitCode": "731",
                                    "TopPosId": 4,
                                    "TopPosCode": "3"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):3 - 21014 - 361,Organisationsenhet(namn och kod):OC Sydost - Motala - Urologkliniken",
                                "id": 443,
                                "data": {
                                    "PosId": 443,
                                    "PosCode": "3 - 21014 - 361",
                                    "PosName": "OC Sydost - Motala - Urologkliniken",
                                    "PosNameWithCode": "OC Sydost (3) - Motala (21014) - Urologkliniken (361)",
                                    "PosId4": 443,
                                    "PosLevel": 3,
                                    "UnitCode": "361",
                                    "TopPosId": 4,
                                    "TopPosCode": "3"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):3 - 21013 - 211,Organisationsenhet(namn och kod):OC Sydost - Norrköping ViN - Hudkliniken",
                                "id": 767,
                                "data": {
                                    "PosId": 767,
                                    "PosCode": "3 - 21013 - 211",
                                    "PosName": "OC Sydost - Norrköping ViN - Hudkliniken",
                                    "PosNameWithCode": "OC Sydost (3) - Norrköping ViN (21013) - Hudkliniken (211)",
                                    "PosId4": 767,
                                    "PosLevel": 3,
                                    "UnitCode": "211",
                                    "TopPosId": 4,
                                    "TopPosCode": "3"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):3 - 21013 - 301,Organisationsenhet(namn och kod):OC Sydost - Norrköping ViN - Kirurgkliniken",
                                "id": 404,
                                "data": {
                                    "PosId": 404,
                                    "PosCode": "3 - 21013 - 301",
                                    "PosName": "OC Sydost - Norrköping ViN - Kirurgkliniken",
                                    "PosNameWithCode": "OC Sydost (3) - Norrköping ViN (21013) - Kirurgkliniken (301)",
                                    "PosId4": 404,
                                    "PosLevel": 3,
                                    "UnitCode": "301",
                                    "TopPosId": 4,
                                    "TopPosCode": "3"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):3 - 21013 - 451,Organisationsenhet(namn och kod):OC Sydost - Norrköping ViN - Kvinnokliniken",
                                "id": 758,
                                "data": {
                                    "PosId": 758,
                                    "PosCode": "3 - 21013 - 451",
                                    "PosName": "OC Sydost - Norrköping ViN - Kvinnokliniken",
                                    "PosNameWithCode": "OC Sydost (3) - Norrköping ViN (21013) - Kvinnokliniken (451)",
                                    "PosId4": 758,
                                    "PosLevel": 3,
                                    "UnitCode": "451",
                                    "TopPosId": 4,
                                    "TopPosCode": "3"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):3 - 21013 - 101,Organisationsenhet(namn och kod):OC Sydost - Norrköping ViN - Medicinkliniken",
                                "id": 405,
                                "data": {
                                    "PosId": 405,
                                    "PosCode": "3 - 21013 - 101",
                                    "PosName": "OC Sydost - Norrköping ViN - Medicinkliniken",
                                    "PosNameWithCode": "OC Sydost (3) - Norrköping ViN (21013) - Medicinkliniken (101)",
                                    "PosId4": 405,
                                    "PosLevel": 3,
                                    "UnitCode": "101",
                                    "TopPosId": 4,
                                    "TopPosCode": "3"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):3 - 21013 - 731,Organisationsenhet(namn och kod):OC Sydost - Norrköping ViN - Röntgenkliniken",
                                "id": 1489,
                                "data": {
                                    "PosId": 1489,
                                    "PosCode": "3 - 21013 - 731",
                                    "PosName": "OC Sydost - Norrköping ViN - Röntgenkliniken",
                                    "PosNameWithCode": "OC Sydost (3) - Norrköping ViN (21013) - Röntgenkliniken (731)",
                                    "PosId4": 1489,
                                    "PosLevel": 3,
                                    "UnitCode": "731",
                                    "TopPosId": 4,
                                    "TopPosCode": "3"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):3 - 21013 - 361,Organisationsenhet(namn och kod):OC Sydost - Norrköping ViN - Urologkliniken",
                                "id": 442,
                                "data": {
                                    "PosId": 442,
                                    "PosCode": "3 - 21013 - 361",
                                    "PosName": "OC Sydost - Norrköping ViN - Urologkliniken",
                                    "PosNameWithCode": "OC Sydost (3) - Norrköping ViN (21013) - Urologkliniken (361)",
                                    "PosId4": 442,
                                    "PosLevel": 3,
                                    "UnitCode": "361",
                                    "TopPosId": 4,
                                    "TopPosCode": "3"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):3 - 21013 - 521,Organisationsenhet(namn och kod):OC Sydost - Norrköping ViN - ÖNH",
                                "id": 572,
                                "data": {
                                    "PosId": 572,
                                    "PosCode": "3 - 21013 - 521",
                                    "PosName": "OC Sydost - Norrköping ViN - ÖNH",
                                    "PosNameWithCode": "OC Sydost (3) - Norrköping ViN (21013) - ÖNH (521)",
                                    "PosId4": 572,
                                    "PosLevel": 3,
                                    "UnitCode": "521",
                                    "TopPosId": 4,
                                    "TopPosCode": "3"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):3 - 22013 - 211,Organisationsenhet(namn och kod):OC Sydost - Nässjö - Hudklinik",
                                "id": 766,
                                "data": {
                                    "PosId": 766,
                                    "PosCode": "3 - 22013 - 211",
                                    "PosName": "OC Sydost - Nässjö - Hudklinik",
                                    "PosNameWithCode": "OC Sydost (3) - Nässjö (22013) - Hudklinik (211)",
                                    "PosId4": 766,
                                    "PosLevel": 3,
                                    "UnitCode": "211",
                                    "TopPosId": 4,
                                    "TopPosCode": "3"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):3 - 22013 - 361,Organisationsenhet(namn och kod):OC Sydost - Nässjö - Urologiska kliniken",
                                "id": 243,
                                "data": {
                                    "PosId": 243,
                                    "PosCode": "3 - 22013 - 361",
                                    "PosName": "OC Sydost - Nässjö - Urologiska kliniken",
                                    "PosNameWithCode": "OC Sydost (3) - Nässjö (22013) - Urologiska kliniken (361)",
                                    "PosId4": 243,
                                    "PosLevel": 3,
                                    "UnitCode": "361",
                                    "TopPosId": 4,
                                    "TopPosCode": "3"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):3 - 25011 - 301,Organisationsenhet(namn och kod):OC Sydost - Oskarshamn - Kirurgkliniken",
                                "id": 399,
                                "data": {
                                    "PosId": 399,
                                    "PosCode": "3 - 25011 - 301",
                                    "PosName": "OC Sydost - Oskarshamn - Kirurgkliniken",
                                    "PosNameWithCode": "OC Sydost (3) - Oskarshamn (25011) - Kirurgkliniken (301)",
                                    "PosId4": 399,
                                    "PosLevel": 3,
                                    "UnitCode": "301",
                                    "TopPosId": 4,
                                    "TopPosCode": "3"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):3 - 25011 - 451,Organisationsenhet(namn och kod):OC Sydost - Oskarshamn - Kvinnokliniken",
                                "id": 755,
                                "data": {
                                    "PosId": 755,
                                    "PosCode": "3 - 25011 - 451",
                                    "PosName": "OC Sydost - Oskarshamn - Kvinnokliniken",
                                    "PosNameWithCode": "OC Sydost (3) - Oskarshamn (25011) - Kvinnokliniken (451)",
                                    "PosId4": 755,
                                    "PosLevel": 3,
                                    "UnitCode": "451",
                                    "TopPosId": 4,
                                    "TopPosCode": "3"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):3 - 25011 - 101,Organisationsenhet(namn och kod):OC Sydost - Oskarshamn - Medicinkliniken",
                                "id": 446,
                                "data": {
                                    "PosId": 446,
                                    "PosCode": "3 - 25011 - 101",
                                    "PosName": "OC Sydost - Oskarshamn - Medicinkliniken",
                                    "PosNameWithCode": "OC Sydost (3) - Oskarshamn (25011) - Medicinkliniken (101)",
                                    "PosId4": 446,
                                    "PosLevel": 3,
                                    "UnitCode": "101",
                                    "TopPosId": 4,
                                    "TopPosCode": "3"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):3 - 25011 - 361,Organisationsenhet(namn och kod):OC Sydost - Oskarshamn - Urologiska kliniken",
                                "id": 244,
                                "data": {
                                    "PosId": 244,
                                    "PosCode": "3 - 25011 - 361",
                                    "PosName": "OC Sydost - Oskarshamn - Urologiska kliniken",
                                    "PosNameWithCode": "OC Sydost (3) - Oskarshamn (25011) - Urologiska kliniken (361)",
                                    "PosId4": 244,
                                    "PosLevel": 3,
                                    "UnitCode": "361",
                                    "TopPosId": 4,
                                    "TopPosCode": "3"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):3 - 241981 - 999,Organisationsenhet(namn och kod):OC Sydost - Oskarshamn, Blå Kustens hälsocentral - Klinik",
                                "id": 1374,
                                "data": {
                                    "PosId": 1374,
                                    "PosCode": "3 - 241981 - 999",
                                    "PosName": "OC Sydost - Oskarshamn, Blå Kustens hälsocentral - Klinik",
                                    "PosNameWithCode": "OC Sydost (3) - Oskarshamn, Blå Kustens hälsocentral (241981) - Klinik (999)",
                                    "PosId4": 1374,
                                    "PosLevel": 3,
                                    "UnitCode": "999",
                                    "TopPosId": 4,
                                    "TopPosCode": "3"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):3 - 30199 - 999,Organisationsenhet(namn och kod):OC Sydost - Privatläkare - Klinik",
                                "id": 450,
                                "data": {
                                    "PosId": 450,
                                    "PosCode": "3 - 30199 - 999",
                                    "PosName": "OC Sydost - Privatläkare - Klinik",
                                    "PosNameWithCode": "OC Sydost (3) - Privatläkare (30199) - Klinik (999)",
                                    "PosId4": 450,
                                    "PosLevel": 3,
                                    "UnitCode": "999",
                                    "TopPosId": 4,
                                    "TopPosCode": "3"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):3 - 221997 - 999,Organisationsenhet(namn och kod):OC Sydost - Prostata Scandinavia AB - Klinik",
                                "id": 14982,
                                "data": {
                                    "PosId": 14982,
                                    "PosCode": "3 - 221997 - 999",
                                    "PosName": "OC Sydost - Prostata Scandinavia AB - Klinik",
                                    "PosNameWithCode": "OC Sydost (3) - Prostata Scandinavia AB (221997) - Klinik (999)",
                                    "PosId4": 14982,
                                    "PosLevel": 3,
                                    "UnitCode": "999",
                                    "TopPosId": 4,
                                    "TopPosCode": "3"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):3 - 211992 - 301,Organisationsenhet(namn och kod):OC Sydost - Sergelkliniken - Kirurgkliniken",
                                "id": 489,
                                "data": {
                                    "PosId": 489,
                                    "PosCode": "3 - 211992 - 301",
                                    "PosName": "OC Sydost - Sergelkliniken - Kirurgkliniken",
                                    "PosNameWithCode": "OC Sydost (3) - Sergelkliniken (211992) - Kirurgkliniken (301)",
                                    "PosId4": 489,
                                    "PosLevel": 3,
                                    "UnitCode": "301",
                                    "TopPosId": 4,
                                    "TopPosCode": "3"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):3 - 241991 - 361,Organisationsenhet(namn och kod):OC Sydost - Specialistläk, kalmar - Urologiska kliniken",
                                "id": 248,
                                "data": {
                                    "PosId": 248,
                                    "PosCode": "3 - 241991 - 361",
                                    "PosName": "OC Sydost - Specialistläk, kalmar - Urologiska kliniken",
                                    "PosNameWithCode": "OC Sydost (3) - Specialistläk, kalmar (241991) - Urologiska kliniken (361)",
                                    "PosId4": 248,
                                    "PosLevel": 3,
                                    "UnitCode": "361",
                                    "TopPosId": 4,
                                    "TopPosCode": "3"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):3 - 241022 - 361,Organisationsenhet(namn och kod):OC Sydost - Vimmerby - Urologiska kliniken",
                                "id": 245,
                                "data": {
                                    "PosId": 245,
                                    "PosCode": "3 - 241022 - 361",
                                    "PosName": "OC Sydost - Vimmerby - Urologiska kliniken",
                                    "PosNameWithCode": "OC Sydost (3) - Vimmerby (241022) - Urologiska kliniken (361)",
                                    "PosId4": 245,
                                    "PosLevel": 3,
                                    "UnitCode": "361",
                                    "TopPosId": 4,
                                    "TopPosCode": "3"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):3 - 30000 - 999,Organisationsenhet(namn och kod):OC Sydost - Vårdcentral - Klinik",
                                "id": 448,
                                "data": {
                                    "PosId": 448,
                                    "PosCode": "3 - 30000 - 999",
                                    "PosName": "OC Sydost - Vårdcentral - Klinik",
                                    "PosNameWithCode": "OC Sydost (3) - Vårdcentral (30000) - Klinik (999)",
                                    "PosId4": 448,
                                    "PosLevel": 3,
                                    "UnitCode": "999",
                                    "TopPosId": 4,
                                    "TopPosCode": "3"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):3 - 22012 - 211,Organisationsenhet(namn och kod):OC Sydost - Värnamo - Hudkliniken",
                                "id": 769,
                                "data": {
                                    "PosId": 769,
                                    "PosCode": "3 - 22012 - 211",
                                    "PosName": "OC Sydost - Värnamo - Hudkliniken",
                                    "PosNameWithCode": "OC Sydost (3) - Värnamo (22012) - Hudkliniken (211)",
                                    "PosId4": 769,
                                    "PosLevel": 3,
                                    "UnitCode": "211",
                                    "TopPosId": 4,
                                    "TopPosCode": "3"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):3 - 22012 - 301,Organisationsenhet(namn och kod):OC Sydost - Värnamo - Kirurgkliniken",
                                "id": 400,
                                "data": {
                                    "PosId": 400,
                                    "PosCode": "3 - 22012 - 301",
                                    "PosName": "OC Sydost - Värnamo - Kirurgkliniken",
                                    "PosNameWithCode": "OC Sydost (3) - Värnamo (22012) - Kirurgkliniken (301)",
                                    "PosId4": 400,
                                    "PosLevel": 3,
                                    "UnitCode": "301",
                                    "TopPosId": 4,
                                    "TopPosCode": "3"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):3 - 22012 - 451,Organisationsenhet(namn och kod):OC Sydost - Värnamo - Kvinnokliniken",
                                "id": 756,
                                "data": {
                                    "PosId": 756,
                                    "PosCode": "3 - 22012 - 451",
                                    "PosName": "OC Sydost - Värnamo - Kvinnokliniken",
                                    "PosNameWithCode": "OC Sydost (3) - Värnamo (22012) - Kvinnokliniken (451)",
                                    "PosId4": 756,
                                    "PosLevel": 3,
                                    "UnitCode": "451",
                                    "TopPosId": 4,
                                    "TopPosCode": "3"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):3 - 22012 - 101,Organisationsenhet(namn och kod):OC Sydost - Värnamo - Medicinkliniken",
                                "id": 401,
                                "data": {
                                    "PosId": 401,
                                    "PosCode": "3 - 22012 - 101",
                                    "PosName": "OC Sydost - Värnamo - Medicinkliniken",
                                    "PosNameWithCode": "OC Sydost (3) - Värnamo (22012) - Medicinkliniken (101)",
                                    "PosId4": 401,
                                    "PosLevel": 3,
                                    "UnitCode": "101",
                                    "TopPosId": 4,
                                    "TopPosCode": "3"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):3 - 22012 - 731,Organisationsenhet(namn och kod):OC Sydost - Värnamo - Röntgen",
                                "id": 1485,
                                "data": {
                                    "PosId": 1485,
                                    "PosCode": "3 - 22012 - 731",
                                    "PosName": "OC Sydost - Värnamo - Röntgen",
                                    "PosNameWithCode": "OC Sydost (3) - Värnamo (22012) - Röntgen (731)",
                                    "PosId4": 1485,
                                    "PosLevel": 3,
                                    "UnitCode": "731",
                                    "TopPosId": 4,
                                    "TopPosCode": "3"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):3 - 22012 - 361,Organisationsenhet(namn och kod):OC Sydost - Värnamo - Urologiska kliniken",
                                "id": 246,
                                "data": {
                                    "PosId": 246,
                                    "PosCode": "3 - 22012 - 361",
                                    "PosName": "OC Sydost - Värnamo - Urologiska kliniken",
                                    "PosNameWithCode": "OC Sydost (3) - Värnamo (22012) - Urologiska kliniken (361)",
                                    "PosId4": 246,
                                    "PosLevel": 3,
                                    "UnitCode": "361",
                                    "TopPosId": 4,
                                    "TopPosCode": "3"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):3 - 24010 - 211,Organisationsenhet(namn och kod):OC Sydost - Västervik - Hudklinik",
                                "id": 770,
                                "data": {
                                    "PosId": 770,
                                    "PosCode": "3 - 24010 - 211",
                                    "PosName": "OC Sydost - Västervik - Hudklinik",
                                    "PosNameWithCode": "OC Sydost (3) - Västervik (24010) - Hudklinik (211)",
                                    "PosId4": 770,
                                    "PosLevel": 3,
                                    "UnitCode": "211",
                                    "TopPosId": 4,
                                    "TopPosCode": "3"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):3 - 24010 - 301,Organisationsenhet(namn och kod):OC Sydost - Västervik - Kirurgkliniken",
                                "id": 402,
                                "data": {
                                    "PosId": 402,
                                    "PosCode": "3 - 24010 - 301",
                                    "PosName": "OC Sydost - Västervik - Kirurgkliniken",
                                    "PosNameWithCode": "OC Sydost (3) - Västervik (24010) - Kirurgkliniken (301)",
                                    "PosId4": 402,
                                    "PosLevel": 3,
                                    "UnitCode": "301",
                                    "TopPosId": 4,
                                    "TopPosCode": "3"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):3 - 24010 - 451,Organisationsenhet(namn och kod):OC Sydost - Västervik - Kvinnokliniken",
                                "id": 757,
                                "data": {
                                    "PosId": 757,
                                    "PosCode": "3 - 24010 - 451",
                                    "PosName": "OC Sydost - Västervik - Kvinnokliniken",
                                    "PosNameWithCode": "OC Sydost (3) - Västervik (24010) - Kvinnokliniken (451)",
                                    "PosId4": 757,
                                    "PosLevel": 3,
                                    "UnitCode": "451",
                                    "TopPosId": 4,
                                    "TopPosCode": "3"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):3 - 24010 - 101,Organisationsenhet(namn och kod):OC Sydost - Västervik - Medicinkliniken",
                                "id": 403,
                                "data": {
                                    "PosId": 403,
                                    "PosCode": "3 - 24010 - 101",
                                    "PosName": "OC Sydost - Västervik - Medicinkliniken",
                                    "PosNameWithCode": "OC Sydost (3) - Västervik (24010) - Medicinkliniken (101)",
                                    "PosId4": 403,
                                    "PosLevel": 3,
                                    "UnitCode": "101",
                                    "TopPosId": 4,
                                    "TopPosCode": "3"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):3 - 24010 - 731,Organisationsenhet(namn och kod):OC Sydost - Västervik - Radiologiska kliniken",
                                "id": 1486,
                                "data": {
                                    "PosId": 1486,
                                    "PosCode": "3 - 24010 - 731",
                                    "PosName": "OC Sydost - Västervik - Radiologiska kliniken",
                                    "PosNameWithCode": "OC Sydost (3) - Västervik (24010) - Radiologiska kliniken (731)",
                                    "PosId4": 1486,
                                    "PosLevel": 3,
                                    "UnitCode": "731",
                                    "TopPosId": 4,
                                    "TopPosCode": "3"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):3 - 24010 - 361,Organisationsenhet(namn och kod):OC Sydost - Västervik - Urologiska kliniken",
                                "id": 247,
                                "data": {
                                    "PosId": 247,
                                    "PosCode": "3 - 24010 - 361",
                                    "PosName": "OC Sydost - Västervik - Urologiska kliniken",
                                    "PosNameWithCode": "OC Sydost (3) - Västervik (24010) - Urologiska kliniken (361)",
                                    "PosId4": 247,
                                    "PosLevel": 3,
                                    "UnitCode": "361",
                                    "TopPosId": 4,
                                    "TopPosCode": "3"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):3 - 24010 - 521,Organisationsenhet(namn och kod):OC Sydost - Västervik - ÖNH",
                                "id": 571,
                                "data": {
                                    "PosId": 571,
                                    "PosCode": "3 - 24010 - 521",
                                    "PosName": "OC Sydost - Västervik - ÖNH",
                                    "PosNameWithCode": "OC Sydost (3) - Västervik (24010) - ÖNH (521)",
                                    "PosId4": 571,
                                    "PosLevel": 3,
                                    "UnitCode": "521",
                                    "TopPosId": 4,
                                    "TopPosCode": "3"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):2 - 54012 - 301,Organisationsenhet(namn och kod):OC Uppsala/Örebro - Arvika sjh - Kir klin",
                                "id": 267,
                                "data": {
                                    "PosId": 267,
                                    "PosCode": "2 - 54012 - 301",
                                    "PosName": "OC Uppsala/Örebro - Arvika sjh - Kir klin",
                                    "PosNameWithCode": "OC Uppsala/Örebro (2) - Arvika sjh (54012) - Kir klin (301)",
                                    "PosId4": 267,
                                    "PosLevel": 3,
                                    "UnitCode": "301",
                                    "TopPosId": 5,
                                    "TopPosCode": "2"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):2 - 54012 - 101,Organisationsenhet(namn och kod):OC Uppsala/Örebro - Arvika sjh - Med klin",
                                "id": 359,
                                "data": {
                                    "PosId": 359,
                                    "PosCode": "2 - 54012 - 101",
                                    "PosName": "OC Uppsala/Örebro - Arvika sjh - Med klin",
                                    "PosNameWithCode": "OC Uppsala/Örebro (2) - Arvika sjh (54012) - Med klin (101)",
                                    "PosId4": 359,
                                    "PosLevel": 3,
                                    "UnitCode": "101",
                                    "TopPosId": 5,
                                    "TopPosCode": "2"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):2 - 54012 - 521,Organisationsenhet(namn och kod):OC Uppsala/Örebro - Arvika sjh - ÖNH",
                                "id": 1055,
                                "data": {
                                    "PosId": 1055,
                                    "PosCode": "2 - 54012 - 521",
                                    "PosName": "OC Uppsala/Örebro - Arvika sjh - ÖNH",
                                    "PosNameWithCode": "OC Uppsala/Örebro (2) - Arvika sjh (54012) - ÖNH (521)",
                                    "PosId4": 1055,
                                    "PosLevel": 3,
                                    "UnitCode": "521",
                                    "TopPosId": 5,
                                    "TopPosCode": "2"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):2 - 57013 - 101,Organisationsenhet(namn och kod):OC Uppsala/Örebro - Avesta las - Med klin",
                                "id": 665,
                                "data": {
                                    "PosId": 665,
                                    "PosCode": "2 - 57013 - 101",
                                    "PosName": "OC Uppsala/Örebro - Avesta las - Med klin",
                                    "PosNameWithCode": "OC Uppsala/Örebro (2) - Avesta las (57013) - Med klin (101)",
                                    "PosId4": 665,
                                    "PosLevel": 3,
                                    "UnitCode": "101",
                                    "TopPosId": 5,
                                    "TopPosCode": "2"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):2 - 61011 - 242,Organisationsenhet(namn och kod):OC Uppsala/Örebro - Bollnäs sjh - Geriatrik",
                                "id": 1276,
                                "data": {
                                    "PosId": 1276,
                                    "PosCode": "2 - 61011 - 242",
                                    "PosName": "OC Uppsala/Örebro - Bollnäs sjh - Geriatrik",
                                    "PosNameWithCode": "OC Uppsala/Örebro (2) - Bollnäs sjh (61011) - Geriatrik (242)",
                                    "PosId4": 1276,
                                    "PosLevel": 3,
                                    "UnitCode": "242",
                                    "TopPosId": 5,
                                    "TopPosCode": "2"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):2 - 61011 - 301,Organisationsenhet(namn och kod):OC Uppsala/Örebro - Bollnäs sjh - Kir klin",
                                "id": 294,
                                "data": {
                                    "PosId": 294,
                                    "PosCode": "2 - 61011 - 301",
                                    "PosName": "OC Uppsala/Örebro - Bollnäs sjh - Kir klin",
                                    "PosNameWithCode": "OC Uppsala/Örebro (2) - Bollnäs sjh (61011) - Kir klin (301)",
                                    "PosId4": 294,
                                    "PosLevel": 3,
                                    "UnitCode": "301",
                                    "TopPosId": 5,
                                    "TopPosCode": "2"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):2 - 61011 - 101,Organisationsenhet(namn och kod):OC Uppsala/Örebro - Bollnäs sjh - Med klin",
                                "id": 273,
                                "data": {
                                    "PosId": 273,
                                    "PosCode": "2 - 61011 - 101",
                                    "PosName": "OC Uppsala/Örebro - Bollnäs sjh - Med klin",
                                    "PosNameWithCode": "OC Uppsala/Örebro (2) - Bollnäs sjh (61011) - Med klin (101)",
                                    "PosId4": 273,
                                    "PosLevel": 3,
                                    "UnitCode": "101",
                                    "TopPosId": 5,
                                    "TopPosCode": "2"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):2 - 61011 - 521,Organisationsenhet(namn och kod):OC Uppsala/Örebro - Bollnäs sjh - ÖNH",
                                "id": 1057,
                                "data": {
                                    "PosId": 1057,
                                    "PosCode": "2 - 61011 - 521",
                                    "PosName": "OC Uppsala/Örebro - Bollnäs sjh - ÖNH",
                                    "PosNameWithCode": "OC Uppsala/Örebro (2) - Bollnäs sjh (61011) - ÖNH (521)",
                                    "PosId4": 1057,
                                    "PosLevel": 3,
                                    "UnitCode": "521",
                                    "TopPosId": 5,
                                    "TopPosCode": "2"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):2 - 5541 - 912,Organisationsenhet(namn och kod):OC Uppsala/Örebro - Capio Läkargruppen i Örebro - Klinik",
                                "id": 1497,
                                "data": {
                                    "PosId": 1497,
                                    "PosCode": "2 - 5541 - 912",
                                    "PosName": "OC Uppsala/Örebro - Capio Läkargruppen i Örebro - Klinik",
                                    "PosNameWithCode": "OC Uppsala/Örebro (2) - Capio Läkargruppen i Örebro (5541) - Klinik (912)",
                                    "PosId4": 1497,
                                    "PosLevel": 3,
                                    "UnitCode": "912",
                                    "TopPosId": 5,
                                    "TopPosCode": "2"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):2 - 12010 - 211,Organisationsenhet(namn och kod):OC Uppsala/Örebro - Enköping las - Hudkliniken",
                                "id": 1029,
                                "data": {
                                    "PosId": 1029,
                                    "PosCode": "2 - 12010 - 211",
                                    "PosName": "OC Uppsala/Örebro - Enköping las - Hudkliniken",
                                    "PosNameWithCode": "OC Uppsala/Örebro (2) - Enköping las (12010) - Hudkliniken (211)",
                                    "PosId4": 1029,
                                    "PosLevel": 3,
                                    "UnitCode": "211",
                                    "TopPosId": 5,
                                    "TopPosCode": "2"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):2 - 12010 - 301,Organisationsenhet(namn och kod):OC Uppsala/Örebro - Enköping las - Kir klin",
                                "id": 281,
                                "data": {
                                    "PosId": 281,
                                    "PosCode": "2 - 12010 - 301",
                                    "PosName": "OC Uppsala/Örebro - Enköping las - Kir klin",
                                    "PosNameWithCode": "OC Uppsala/Örebro (2) - Enköping las (12010) - Kir klin (301)",
                                    "PosId4": 281,
                                    "PosLevel": 3,
                                    "UnitCode": "301",
                                    "TopPosId": 5,
                                    "TopPosCode": "2"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):2 - 12010 - 101,Organisationsenhet(namn och kod):OC Uppsala/Örebro - Enköping las - Med klin",
                                "id": 487,
                                "data": {
                                    "PosId": 487,
                                    "PosCode": "2 - 12010 - 101",
                                    "PosName": "OC Uppsala/Örebro - Enköping las - Med klin",
                                    "PosNameWithCode": "OC Uppsala/Örebro (2) - Enköping las (12010) - Med klin (101)",
                                    "PosId4": 487,
                                    "PosLevel": 3,
                                    "UnitCode": "101",
                                    "TopPosId": 5,
                                    "TopPosCode": "2"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):2 - 13010 - 211,Organisationsenhet(namn och kod):OC Uppsala/Örebro - Eskilstuna M.sjh - Hudklinken",
                                "id": 788,
                                "data": {
                                    "PosId": 788,
                                    "PosCode": "2 - 13010 - 211",
                                    "PosName": "OC Uppsala/Örebro - Eskilstuna M.sjh - Hudklinken",
                                    "PosNameWithCode": "OC Uppsala/Örebro (2) - Eskilstuna M.sjh (13010) - Hudklinken (211)",
                                    "PosId4": 788,
                                    "PosLevel": 3,
                                    "UnitCode": "211",
                                    "TopPosId": 5,
                                    "TopPosCode": "2"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):2 - 13010 - 301,Organisationsenhet(namn och kod):OC Uppsala/Örebro - Eskilstuna M.sjh - Kir klin",
                                "id": 215,
                                "data": {
                                    "PosId": 215,
                                    "PosCode": "2 - 13010 - 301",
                                    "PosName": "OC Uppsala/Örebro - Eskilstuna M.sjh - Kir klin",
                                    "PosNameWithCode": "OC Uppsala/Örebro (2) - Eskilstuna M.sjh (13010) - Kir klin (301)",
                                    "PosId4": 215,
                                    "PosLevel": 3,
                                    "UnitCode": "301",
                                    "TopPosId": 5,
                                    "TopPosCode": "2"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):2 - 13010 - 451,Organisationsenhet(namn och kod):OC Uppsala/Örebro - Eskilstuna M.sjh - Kvinnoklinik",
                                "id": 655,
                                "data": {
                                    "PosId": 655,
                                    "PosCode": "2 - 13010 - 451",
                                    "PosName": "OC Uppsala/Örebro - Eskilstuna M.sjh - Kvinnoklinik",
                                    "PosNameWithCode": "OC Uppsala/Örebro (2) - Eskilstuna M.sjh (13010) - Kvinnoklinik (451)",
                                    "PosId4": 655,
                                    "PosLevel": 3,
                                    "UnitCode": "451",
                                    "TopPosId": 5,
                                    "TopPosCode": "2"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):2 - 13010 - 111,Organisationsenhet(namn och kod):OC Uppsala/Örebro - Eskilstuna M.sjh - Lung klin",
                                "id": 699,
                                "data": {
                                    "PosId": 699,
                                    "PosCode": "2 - 13010 - 111",
                                    "PosName": "OC Uppsala/Örebro - Eskilstuna M.sjh - Lung klin",
                                    "PosNameWithCode": "OC Uppsala/Örebro (2) - Eskilstuna M.sjh (13010) - Lung klin (111)",
                                    "PosId4": 699,
                                    "PosLevel": 3,
                                    "UnitCode": "111",
                                    "TopPosId": 5,
                                    "TopPosCode": "2"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):2 - 13010 - 101,Organisationsenhet(namn och kod):OC Uppsala/Örebro - Eskilstuna M.sjh - Med klin",
                                "id": 368,
                                "data": {
                                    "PosId": 368,
                                    "PosCode": "2 - 13010 - 101",
                                    "PosName": "OC Uppsala/Örebro - Eskilstuna M.sjh - Med klin",
                                    "PosNameWithCode": "OC Uppsala/Örebro (2) - Eskilstuna M.sjh (13010) - Med klin (101)",
                                    "PosId4": 368,
                                    "PosLevel": 3,
                                    "UnitCode": "101",
                                    "TopPosId": 5,
                                    "TopPosCode": "2"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):2 - 13010 - 221,Organisationsenhet(namn och kod):OC Uppsala/Örebro - Eskilstuna M.sjh - Neurologiska kliniken",
                                "id": 1064,
                                "data": {
                                    "PosId": 1064,
                                    "PosCode": "2 - 13010 - 221",
                                    "PosName": "OC Uppsala/Örebro - Eskilstuna M.sjh - Neurologiska kliniken",
                                    "PosNameWithCode": "OC Uppsala/Örebro (2) - Eskilstuna M.sjh (13010) - Neurologiska kliniken (221)",
                                    "PosId4": 1064,
                                    "PosLevel": 3,
                                    "UnitCode": "221",
                                    "TopPosId": 5,
                                    "TopPosCode": "2"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):2 - 13010 - 741,Organisationsenhet(namn och kod):OC Uppsala/Örebro - Eskilstuna M.sjh - Onk klin",
                                "id": 378,
                                "data": {
                                    "PosId": 378,
                                    "PosCode": "2 - 13010 - 741",
                                    "PosName": "OC Uppsala/Örebro - Eskilstuna M.sjh - Onk klin",
                                    "PosNameWithCode": "OC Uppsala/Örebro (2) - Eskilstuna M.sjh (13010) - Onk klin (741)",
                                    "PosId4": 378,
                                    "PosLevel": 3,
                                    "UnitCode": "741",
                                    "TopPosId": 5,
                                    "TopPosCode": "2"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):2 - 13010 - 311,Organisationsenhet(namn och kod):OC Uppsala/Örebro - Eskilstuna M.sjh - Ortopedkliniken",
                                "id": 893,
                                "data": {
                                    "PosId": 893,
                                    "PosCode": "2 - 13010 - 311",
                                    "PosName": "OC Uppsala/Örebro - Eskilstuna M.sjh - Ortopedkliniken",
                                    "PosNameWithCode": "OC Uppsala/Örebro (2) - Eskilstuna M.sjh (13010) - Ortopedkliniken (311)",
                                    "PosId4": 893,
                                    "PosLevel": 3,
                                    "UnitCode": "311",
                                    "TopPosId": 5,
                                    "TopPosCode": "2"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):2 - 13010 - 731,Organisationsenhet(namn och kod):OC Uppsala/Örebro - Eskilstuna M.sjh - Radiologiska kliniken",
                                "id": 1415,
                                "data": {
                                    "PosId": 1415,
                                    "PosCode": "2 - 13010 - 731",
                                    "PosName": "OC Uppsala/Örebro - Eskilstuna M.sjh - Radiologiska kliniken",
                                    "PosNameWithCode": "OC Uppsala/Örebro (2) - Eskilstuna M.sjh (13010) - Radiologiska kliniken (731)",
                                    "PosId4": 1415,
                                    "PosLevel": 3,
                                    "UnitCode": "731",
                                    "TopPosId": 5,
                                    "TopPosCode": "2"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):2 - 13010 - 521,Organisationsenhet(namn och kod):OC Uppsala/Örebro - Eskilstuna M.sjh - ÖNH",
                                "id": 604,
                                "data": {
                                    "PosId": 604,
                                    "PosCode": "2 - 13010 - 521",
                                    "PosName": "OC Uppsala/Örebro - Eskilstuna M.sjh - ÖNH",
                                    "PosNameWithCode": "OC Uppsala/Örebro (2) - Eskilstuna M.sjh (13010) - ÖNH (521)",
                                    "PosId4": 604,
                                    "PosLevel": 3,
                                    "UnitCode": "521",
                                    "TopPosId": 5,
                                    "TopPosCode": "2"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):2 - 56013 - 301,Organisationsenhet(namn och kod):OC Uppsala/Örebro - Fagersta las - Kir klin",
                                "id": 1136,
                                "data": {
                                    "PosId": 1136,
                                    "PosCode": "2 - 56013 - 301",
                                    "PosName": "OC Uppsala/Örebro - Fagersta las - Kir klin",
                                    "PosNameWithCode": "OC Uppsala/Örebro (2) - Fagersta las (56013) - Kir klin (301)",
                                    "PosId4": 1136,
                                    "PosLevel": 3,
                                    "UnitCode": "301",
                                    "TopPosId": 5,
                                    "TopPosCode": "2"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):2 - 56013 - 101,Organisationsenhet(namn och kod):OC Uppsala/Örebro - Fagersta las - Med klin",
                                "id": 360,
                                "data": {
                                    "PosId": 360,
                                    "PosCode": "2 - 56013 - 101",
                                    "PosName": "OC Uppsala/Örebro - Fagersta las - Med klin",
                                    "PosNameWithCode": "OC Uppsala/Örebro (2) - Fagersta las (56013) - Med klin (101)",
                                    "PosId4": 360,
                                    "PosLevel": 3,
                                    "UnitCode": "101",
                                    "TopPosId": 5,
                                    "TopPosCode": "2"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):2 - 56013 - 521,Organisationsenhet(namn och kod):OC Uppsala/Örebro - Fagersta las - ÖNH",
                                "id": 1142,
                                "data": {
                                    "PosId": 1142,
                                    "PosCode": "2 - 56013 - 521",
                                    "PosName": "OC Uppsala/Örebro - Fagersta las - ÖNH",
                                    "PosNameWithCode": "OC Uppsala/Örebro (2) - Fagersta las (56013) - ÖNH (521)",
                                    "PosId4": 1142,
                                    "PosLevel": 3,
                                    "UnitCode": "521",
                                    "TopPosId": 5,
                                    "TopPosCode": "2"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):2 - 57010 - 211,Organisationsenhet(namn och kod):OC Uppsala/Örebro - Falun F.las - Hudkliniken",
                                "id": 870,
                                "data": {
                                    "PosId": 870,
                                    "PosCode": "2 - 57010 - 211",
                                    "PosName": "OC Uppsala/Örebro - Falun F.las - Hudkliniken",
                                    "PosNameWithCode": "OC Uppsala/Örebro (2) - Falun F.las (57010) - Hudkliniken (211)",
                                    "PosId4": 870,
                                    "PosLevel": 3,
                                    "UnitCode": "211",
                                    "TopPosId": 5,
                                    "TopPosCode": "2"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):2 - 57010 - 301,Organisationsenhet(namn och kod):OC Uppsala/Örebro - Falun F.las - Kir klin",
                                "id": 178,
                                "data": {
                                    "PosId": 178,
                                    "PosCode": "2 - 57010 - 301",
                                    "PosName": "OC Uppsala/Örebro - Falun F.las - Kir klin",
                                    "PosNameWithCode": "OC Uppsala/Örebro (2) - Falun F.las (57010) - Kir klin (301)",
                                    "PosId4": 178,
                                    "PosLevel": 3,
                                    "UnitCode": "301",
                                    "TopPosId": 5,
                                    "TopPosCode": "2"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):2 - 57010 - 451,Organisationsenhet(namn och kod):OC Uppsala/Örebro - Falun F.las - Kvinnoklinik",
                                "id": 737,
                                "data": {
                                    "PosId": 737,
                                    "PosCode": "2 - 57010 - 451",
                                    "PosName": "OC Uppsala/Örebro - Falun F.las - Kvinnoklinik",
                                    "PosNameWithCode": "OC Uppsala/Örebro (2) - Falun F.las (57010) - Kvinnoklinik (451)",
                                    "PosId4": 737,
                                    "PosLevel": 3,
                                    "UnitCode": "451",
                                    "TopPosId": 5,
                                    "TopPosCode": "2"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):2 - 57010 - 101,Organisationsenhet(namn och kod):OC Uppsala/Örebro - Falun F.las - Med klin",
                                "id": 369,
                                "data": {
                                    "PosId": 369,
                                    "PosCode": "2 - 57010 - 101",
                                    "PosName": "OC Uppsala/Örebro - Falun F.las - Med klin",
                                    "PosNameWithCode": "OC Uppsala/Örebro (2) - Falun F.las (57010) - Med klin (101)",
                                    "PosId4": 369,
                                    "PosLevel": 3,
                                    "UnitCode": "101",
                                    "TopPosId": 5,
                                    "TopPosCode": "2"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):2 - 57010 - 741,Organisationsenhet(namn och kod):OC Uppsala/Örebro - Falun F.las - Onk klin",
                                "id": 970,
                                "data": {
                                    "PosId": 970,
                                    "PosCode": "2 - 57010 - 741",
                                    "PosName": "OC Uppsala/Örebro - Falun F.las - Onk klin",
                                    "PosNameWithCode": "OC Uppsala/Örebro (2) - Falun F.las (57010) - Onk klin (741)",
                                    "PosId4": 970,
                                    "PosLevel": 3,
                                    "UnitCode": "741",
                                    "TopPosId": 5,
                                    "TopPosCode": "2"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):2 - 57010 - 311,Organisationsenhet(namn och kod):OC Uppsala/Örebro - Falun F.las - Ortoped klin",
                                "id": 1133,
                                "data": {
                                    "PosId": 1133,
                                    "PosCode": "2 - 57010 - 311",
                                    "PosName": "OC Uppsala/Örebro - Falun F.las - Ortoped klin",
                                    "PosNameWithCode": "OC Uppsala/Örebro (2) - Falun F.las (57010) - Ortoped klin (311)",
                                    "PosId4": 1133,
                                    "PosLevel": 3,
                                    "UnitCode": "311",
                                    "TopPosId": 5,
                                    "TopPosCode": "2"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):2 - 57010 - 511,Organisationsenhet(namn och kod):OC Uppsala/Örebro - Falun F.las - Ögon klin",
                                "id": 993,
                                "data": {
                                    "PosId": 993,
                                    "PosCode": "2 - 57010 - 511",
                                    "PosName": "OC Uppsala/Örebro - Falun F.las - Ögon klin",
                                    "PosNameWithCode": "OC Uppsala/Örebro (2) - Falun F.las (57010) - Ögon klin (511)",
                                    "PosId4": 993,
                                    "PosLevel": 3,
                                    "UnitCode": "511",
                                    "TopPosId": 5,
                                    "TopPosCode": "2"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):2 - 57010 - 521,Organisationsenhet(namn och kod):OC Uppsala/Örebro - Falun F.las - ÖNH",
                                "id": 605,
                                "data": {
                                    "PosId": 605,
                                    "PosCode": "2 - 57010 - 521",
                                    "PosName": "OC Uppsala/Örebro - Falun F.las - ÖNH",
                                    "PosNameWithCode": "OC Uppsala/Örebro (2) - Falun F.las (57010) - ÖNH (521)",
                                    "PosId4": 605,
                                    "PosLevel": 3,
                                    "UnitCode": "521",
                                    "TopPosId": 5,
                                    "TopPosCode": "2"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):2 - 61010 - 161,Organisationsenhet(namn och kod):OC Uppsala/Örebro - Gävle sjh - Endokrin",
                                "id": 1192,
                                "data": {
                                    "PosId": 1192,
                                    "PosCode": "2 - 61010 - 161",
                                    "PosName": "OC Uppsala/Örebro - Gävle sjh - Endokrin",
                                    "PosNameWithCode": "OC Uppsala/Örebro (2) - Gävle sjh (61010) - Endokrin (161)",
                                    "PosId4": 1192,
                                    "PosLevel": 3,
                                    "UnitCode": "161",
                                    "TopPosId": 5,
                                    "TopPosCode": "2"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):2 - 61010 - 211,Organisationsenhet(namn och kod):OC Uppsala/Örebro - Gävle sjh - Hudkliniken",
                                "id": 865,
                                "data": {
                                    "PosId": 865,
                                    "PosCode": "2 - 61010 - 211",
                                    "PosName": "OC Uppsala/Örebro - Gävle sjh - Hudkliniken",
                                    "PosNameWithCode": "OC Uppsala/Örebro (2) - Gävle sjh (61010) - Hudkliniken (211)",
                                    "PosId4": 865,
                                    "PosLevel": 3,
                                    "UnitCode": "211",
                                    "TopPosId": 5,
                                    "TopPosCode": "2"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):2 - 61010 - 301,Organisationsenhet(namn och kod):OC Uppsala/Örebro - Gävle sjh - Kir klin",
                                "id": 275,
                                "data": {
                                    "PosId": 275,
                                    "PosCode": "2 - 61010 - 301",
                                    "PosName": "OC Uppsala/Örebro - Gävle sjh - Kir klin",
                                    "PosNameWithCode": "OC Uppsala/Örebro (2) - Gävle sjh (61010) - Kir klin (301)",
                                    "PosId4": 275,
                                    "PosLevel": 3,
                                    "UnitCode": "301",
                                    "TopPosId": 5,
                                    "TopPosCode": "2"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):2 - 61010 - 451,Organisationsenhet(namn och kod):OC Uppsala/Örebro - Gävle sjh - Kvinnoklinik",
                                "id": 696,
                                "data": {
                                    "PosId": 696,
                                    "PosCode": "2 - 61010 - 451",
                                    "PosName": "OC Uppsala/Örebro - Gävle sjh - Kvinnoklinik",
                                    "PosNameWithCode": "OC Uppsala/Örebro (2) - Gävle sjh (61010) - Kvinnoklinik (451)",
                                    "PosId4": 696,
                                    "PosLevel": 3,
                                    "UnitCode": "451",
                                    "TopPosId": 5,
                                    "TopPosCode": "2"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):2 - 61010 - 111,Organisationsenhet(namn och kod):OC Uppsala/Örebro - Gävle sjh - Lung klin",
                                "id": 652,
                                "data": {
                                    "PosId": 652,
                                    "PosCode": "2 - 61010 - 111",
                                    "PosName": "OC Uppsala/Örebro - Gävle sjh - Lung klin",
                                    "PosNameWithCode": "OC Uppsala/Örebro (2) - Gävle sjh (61010) - Lung klin (111)",
                                    "PosId4": 652,
                                    "PosLevel": 3,
                                    "UnitCode": "111",
                                    "TopPosId": 5,
                                    "TopPosCode": "2"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):2 - 61010 - 101,Organisationsenhet(namn och kod):OC Uppsala/Örebro - Gävle sjh - Med klin",
                                "id": 370,
                                "data": {
                                    "PosId": 370,
                                    "PosCode": "2 - 61010 - 101",
                                    "PosName": "OC Uppsala/Örebro - Gävle sjh - Med klin",
                                    "PosNameWithCode": "OC Uppsala/Örebro (2) - Gävle sjh (61010) - Med klin (101)",
                                    "PosId4": 370,
                                    "PosLevel": 3,
                                    "UnitCode": "101",
                                    "TopPosId": 5,
                                    "TopPosCode": "2"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):2 - 61010 - 221,Organisationsenhet(namn och kod):OC Uppsala/Örebro - Gävle sjh - Neurologiska kliniken",
                                "id": 1063,
                                "data": {
                                    "PosId": 1063,
                                    "PosCode": "2 - 61010 - 221",
                                    "PosName": "OC Uppsala/Örebro - Gävle sjh - Neurologiska kliniken",
                                    "PosNameWithCode": "OC Uppsala/Örebro (2) - Gävle sjh (61010) - Neurologiska kliniken (221)",
                                    "PosId4": 1063,
                                    "PosLevel": 3,
                                    "UnitCode": "221",
                                    "TopPosId": 5,
                                    "TopPosCode": "2"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):2 - 61010 - 741,Organisationsenhet(namn och kod):OC Uppsala/Örebro - Gävle sjh - Onk klin",
                                "id": 379,
                                "data": {
                                    "PosId": 379,
                                    "PosCode": "2 - 61010 - 741",
                                    "PosName": "OC Uppsala/Örebro - Gävle sjh - Onk klin",
                                    "PosNameWithCode": "OC Uppsala/Örebro (2) - Gävle sjh (61010) - Onk klin (741)",
                                    "PosId4": 379,
                                    "PosLevel": 3,
                                    "UnitCode": "741",
                                    "TopPosId": 5,
                                    "TopPosCode": "2"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):2 - 61010 - 611,Organisationsenhet(namn och kod):OC Uppsala/Örebro - Gävle sjh - Patologi",
                                "id": 836,
                                "data": {
                                    "PosId": 836,
                                    "PosCode": "2 - 61010 - 611",
                                    "PosName": "OC Uppsala/Örebro - Gävle sjh - Patologi",
                                    "PosNameWithCode": "OC Uppsala/Örebro (2) - Gävle sjh (61010) - Patologi (611)",
                                    "PosId4": 836,
                                    "PosLevel": 3,
                                    "UnitCode": "611",
                                    "TopPosId": 5,
                                    "TopPosCode": "2"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):2 - 61010 - 521,Organisationsenhet(namn och kod):OC Uppsala/Örebro - Gävle sjh - ÖNH",
                                "id": 603,
                                "data": {
                                    "PosId": 603,
                                    "PosCode": "2 - 61010 - 521",
                                    "PosName": "OC Uppsala/Örebro - Gävle sjh - ÖNH",
                                    "PosNameWithCode": "OC Uppsala/Örebro (2) - Gävle sjh (61010) - ÖNH (521)",
                                    "PosId4": 603,
                                    "PosLevel": 3,
                                    "UnitCode": "521",
                                    "TopPosId": 5,
                                    "TopPosCode": "2"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):2 - 61012 - 301,Organisationsenhet(namn och kod):OC Uppsala/Örebro - Hudiksvalls sjh - Kir klin",
                                "id": 212,
                                "data": {
                                    "PosId": 212,
                                    "PosCode": "2 - 61012 - 301",
                                    "PosName": "OC Uppsala/Örebro - Hudiksvalls sjh - Kir klin",
                                    "PosNameWithCode": "OC Uppsala/Örebro (2) - Hudiksvalls sjh (61012) - Kir klin (301)",
                                    "PosId4": 212,
                                    "PosLevel": 3,
                                    "UnitCode": "301",
                                    "TopPosId": 5,
                                    "TopPosCode": "2"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):2 - 61012 - 451,Organisationsenhet(namn och kod):OC Uppsala/Örebro - Hudiksvalls sjh - Kvinnoklinik",
                                "id": 618,
                                "data": {
                                    "PosId": 618,
                                    "PosCode": "2 - 61012 - 451",
                                    "PosName": "OC Uppsala/Örebro - Hudiksvalls sjh - Kvinnoklinik",
                                    "PosNameWithCode": "OC Uppsala/Örebro (2) - Hudiksvalls sjh (61012) - Kvinnoklinik (451)",
                                    "PosId4": 618,
                                    "PosLevel": 3,
                                    "UnitCode": "451",
                                    "TopPosId": 5,
                                    "TopPosCode": "2"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):2 - 61012 - 101,Organisationsenhet(namn och kod):OC Uppsala/Örebro - Hudiksvalls sjh - Med klin",
                                "id": 371,
                                "data": {
                                    "PosId": 371,
                                    "PosCode": "2 - 61012 - 101",
                                    "PosName": "OC Uppsala/Örebro - Hudiksvalls sjh - Med klin",
                                    "PosNameWithCode": "OC Uppsala/Örebro (2) - Hudiksvalls sjh (61012) - Med klin (101)",
                                    "PosId4": 371,
                                    "PosLevel": 3,
                                    "UnitCode": "101",
                                    "TopPosId": 5,
                                    "TopPosCode": "2"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):2 - 61012 - 511,Organisationsenhet(namn och kod):OC Uppsala/Örebro - Hudiksvalls sjh - Ögon klin",
                                "id": 1137,
                                "data": {
                                    "PosId": 1137,
                                    "PosCode": "2 - 61012 - 511",
                                    "PosName": "OC Uppsala/Örebro - Hudiksvalls sjh - Ögon klin",
                                    "PosNameWithCode": "OC Uppsala/Örebro (2) - Hudiksvalls sjh (61012) - Ögon klin (511)",
                                    "PosId4": 1137,
                                    "PosLevel": 3,
                                    "UnitCode": "511",
                                    "TopPosId": 5,
                                    "TopPosCode": "2"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):2 - 61012 - 521,Organisationsenhet(namn och kod):OC Uppsala/Örebro - Hudiksvalls sjh - ÖNH klin",
                                "id": 607,
                                "data": {
                                    "PosId": 607,
                                    "PosCode": "2 - 61012 - 521",
                                    "PosName": "OC Uppsala/Örebro - Hudiksvalls sjh - ÖNH klin",
                                    "PosNameWithCode": "OC Uppsala/Örebro (2) - Hudiksvalls sjh (61012) - ÖNH klin (521)",
                                    "PosId4": 607,
                                    "PosLevel": 3,
                                    "UnitCode": "521",
                                    "TopPosId": 5,
                                    "TopPosCode": "2"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):2 - 55011 - 211,Organisationsenhet(namn och kod):OC Uppsala/Örebro - Karlskoga las - Hudkliniken",
                                "id": 868,
                                "data": {
                                    "PosId": 868,
                                    "PosCode": "2 - 55011 - 211",
                                    "PosName": "OC Uppsala/Örebro - Karlskoga las - Hudkliniken",
                                    "PosNameWithCode": "OC Uppsala/Örebro (2) - Karlskoga las (55011) - Hudkliniken (211)",
                                    "PosId4": 868,
                                    "PosLevel": 3,
                                    "UnitCode": "211",
                                    "TopPosId": 5,
                                    "TopPosCode": "2"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):2 - 55011 - 301,Organisationsenhet(namn och kod):OC Uppsala/Örebro - Karlskoga las - Kir klin",
                                "id": 277,
                                "data": {
                                    "PosId": 277,
                                    "PosCode": "2 - 55011 - 301",
                                    "PosName": "OC Uppsala/Örebro - Karlskoga las - Kir klin",
                                    "PosNameWithCode": "OC Uppsala/Örebro (2) - Karlskoga las (55011) - Kir klin (301)",
                                    "PosId4": 277,
                                    "PosLevel": 3,
                                    "UnitCode": "301",
                                    "TopPosId": 5,
                                    "TopPosCode": "2"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):2 - 55011 - 451,Organisationsenhet(namn och kod):OC Uppsala/Örebro - Karlskoga las - Kvinnoklinik",
                                "id": 749,
                                "data": {
                                    "PosId": 749,
                                    "PosCode": "2 - 55011 - 451",
                                    "PosName": "OC Uppsala/Örebro - Karlskoga las - Kvinnoklinik",
                                    "PosNameWithCode": "OC Uppsala/Örebro (2) - Karlskoga las (55011) - Kvinnoklinik (451)",
                                    "PosId4": 749,
                                    "PosLevel": 3,
                                    "UnitCode": "451",
                                    "TopPosId": 5,
                                    "TopPosCode": "2"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):2 - 55011 - 101,Organisationsenhet(namn och kod):OC Uppsala/Örebro - Karlskoga las - Med klin",
                                "id": 271,
                                "data": {
                                    "PosId": 271,
                                    "PosCode": "2 - 55011 - 101",
                                    "PosName": "OC Uppsala/Örebro - Karlskoga las - Med klin",
                                    "PosNameWithCode": "OC Uppsala/Örebro (2) - Karlskoga las (55011) - Med klin (101)",
                                    "PosId4": 271,
                                    "PosLevel": 3,
                                    "UnitCode": "101",
                                    "TopPosId": 5,
                                    "TopPosCode": "2"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):2 - 55011 - 521,Organisationsenhet(namn och kod):OC Uppsala/Örebro - Karlskoga las - ÖNH",
                                "id": 1232,
                                "data": {
                                    "PosId": 1232,
                                    "PosCode": "2 - 55011 - 521",
                                    "PosName": "OC Uppsala/Örebro - Karlskoga las - ÖNH",
                                    "PosNameWithCode": "OC Uppsala/Örebro (2) - Karlskoga las (55011) - ÖNH (521)",
                                    "PosId4": 1232,
                                    "PosLevel": 3,
                                    "UnitCode": "521",
                                    "TopPosId": 5,
                                    "TopPosCode": "2"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):2 - 54010 - 211,Organisationsenhet(namn och kod):OC Uppsala/Örebro - Karlstad C.sjh - Hudkliniken",
                                "id": 666,
                                "data": {
                                    "PosId": 666,
                                    "PosCode": "2 - 54010 - 211",
                                    "PosName": "OC Uppsala/Örebro - Karlstad C.sjh - Hudkliniken",
                                    "PosNameWithCode": "OC Uppsala/Örebro (2) - Karlstad C.sjh (54010) - Hudkliniken (211)",
                                    "PosId4": 666,
                                    "PosLevel": 3,
                                    "UnitCode": "211",
                                    "TopPosId": 5,
                                    "TopPosCode": "2"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):2 - 54010 - 301,Organisationsenhet(namn och kod):OC Uppsala/Örebro - Karlstad C.sjh - Kir klin",
                                "id": 127,
                                "data": {
                                    "PosId": 127,
                                    "PosCode": "2 - 54010 - 301",
                                    "PosName": "OC Uppsala/Örebro - Karlstad C.sjh - Kir klin",
                                    "PosNameWithCode": "OC Uppsala/Örebro (2) - Karlstad C.sjh (54010) - Kir klin (301)",
                                    "PosId4": 127,
                                    "PosLevel": 3,
                                    "UnitCode": "301",
                                    "TopPosId": 5,
                                    "TopPosCode": "2"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):2 - 54010 - 451,Organisationsenhet(namn och kod):OC Uppsala/Örebro - Karlstad C.sjh - Kvinnoklinik",
                                "id": 656,
                                "data": {
                                    "PosId": 656,
                                    "PosCode": "2 - 54010 - 451",
                                    "PosName": "OC Uppsala/Örebro - Karlstad C.sjh - Kvinnoklinik",
                                    "PosNameWithCode": "OC Uppsala/Örebro (2) - Karlstad C.sjh (54010) - Kvinnoklinik (451)",
                                    "PosId4": 656,
                                    "PosLevel": 3,
                                    "UnitCode": "451",
                                    "TopPosId": 5,
                                    "TopPosCode": "2"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):2 - 54010 - 111,Organisationsenhet(namn och kod):OC Uppsala/Örebro - Karlstad C.sjh - Lung klin",
                                "id": 633,
                                "data": {
                                    "PosId": 633,
                                    "PosCode": "2 - 54010 - 111",
                                    "PosName": "OC Uppsala/Örebro - Karlstad C.sjh - Lung klin",
                                    "PosNameWithCode": "OC Uppsala/Örebro (2) - Karlstad C.sjh (54010) - Lung klin (111)",
                                    "PosId4": 633,
                                    "PosLevel": 3,
                                    "UnitCode": "111",
                                    "TopPosId": 5,
                                    "TopPosCode": "2"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):2 - 54010 - 101,Organisationsenhet(namn och kod):OC Uppsala/Örebro - Karlstad C.sjh - Med klin",
                                "id": 372,
                                "data": {
                                    "PosId": 372,
                                    "PosCode": "2 - 54010 - 101",
                                    "PosName": "OC Uppsala/Örebro - Karlstad C.sjh - Med klin",
                                    "PosNameWithCode": "OC Uppsala/Örebro (2) - Karlstad C.sjh (54010) - Med klin (101)",
                                    "PosId4": 372,
                                    "PosLevel": 3,
                                    "UnitCode": "101",
                                    "TopPosId": 5,
                                    "TopPosCode": "2"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):2 - 54010 - 221,Organisationsenhet(namn och kod):OC Uppsala/Örebro - Karlstad C.sjh - Neurologiska kliniken",
                                "id": 1061,
                                "data": {
                                    "PosId": 1061,
                                    "PosCode": "2 - 54010 - 221",
                                    "PosName": "OC Uppsala/Örebro - Karlstad C.sjh - Neurologiska kliniken",
                                    "PosNameWithCode": "OC Uppsala/Örebro (2) - Karlstad C.sjh (54010) - Neurologiska kliniken (221)",
                                    "PosId4": 1061,
                                    "PosLevel": 3,
                                    "UnitCode": "221",
                                    "TopPosId": 5,
                                    "TopPosCode": "2"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):2 - 54010 - 741,Organisationsenhet(namn och kod):OC Uppsala/Örebro - Karlstad C.sjh - Onk klin",
                                "id": 128,
                                "data": {
                                    "PosId": 128,
                                    "PosCode": "2 - 54010 - 741",
                                    "PosName": "OC Uppsala/Örebro - Karlstad C.sjh - Onk klin",
                                    "PosNameWithCode": "OC Uppsala/Örebro (2) - Karlstad C.sjh (54010) - Onk klin (741)",
                                    "PosId4": 128,
                                    "PosLevel": 3,
                                    "UnitCode": "741",
                                    "TopPosId": 5,
                                    "TopPosCode": "2"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):2 - 54010 - 511,Organisationsenhet(namn och kod):OC Uppsala/Örebro - Karlstad C.sjh - Ögon klin",
                                "id": 1026,
                                "data": {
                                    "PosId": 1026,
                                    "PosCode": "2 - 54010 - 511",
                                    "PosName": "OC Uppsala/Örebro - Karlstad C.sjh - Ögon klin",
                                    "PosNameWithCode": "OC Uppsala/Örebro (2) - Karlstad C.sjh (54010) - Ögon klin (511)",
                                    "PosId4": 1026,
                                    "PosLevel": 3,
                                    "UnitCode": "511",
                                    "TopPosId": 5,
                                    "TopPosCode": "2"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):2 - 54010 - 521,Organisationsenhet(namn och kod):OC Uppsala/Örebro - Karlstad C.sjh - ÖNH",
                                "id": 601,
                                "data": {
                                    "PosId": 601,
                                    "PosCode": "2 - 54010 - 521",
                                    "PosName": "OC Uppsala/Örebro - Karlstad C.sjh - ÖNH",
                                    "PosNameWithCode": "OC Uppsala/Örebro (2) - Karlstad C.sjh (54010) - ÖNH (521)",
                                    "PosId4": 601,
                                    "PosLevel": 3,
                                    "UnitCode": "521",
                                    "TopPosId": 5,
                                    "TopPosCode": "2"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):2 - 13012 - 301,Organisationsenhet(namn och kod):OC Uppsala/Örebro - Katrineholm Kul sjh - Kir klin",
                                "id": 284,
                                "data": {
                                    "PosId": 284,
                                    "PosCode": "2 - 13012 - 301",
                                    "PosName": "OC Uppsala/Örebro - Katrineholm Kul sjh - Kir klin",
                                    "PosNameWithCode": "OC Uppsala/Örebro (2) - Katrineholm Kul sjh (13012) - Kir klin (301)",
                                    "PosId4": 284,
                                    "PosLevel": 3,
                                    "UnitCode": "301",
                                    "TopPosId": 5,
                                    "TopPosCode": "2"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):2 - 13012 - 451,Organisationsenhet(namn och kod):OC Uppsala/Örebro - Katrineholm Kul sjh - Kvinnoklinik",
                                "id": 657,
                                "data": {
                                    "PosId": 657,
                                    "PosCode": "2 - 13012 - 451",
                                    "PosName": "OC Uppsala/Örebro - Katrineholm Kul sjh - Kvinnoklinik",
                                    "PosNameWithCode": "OC Uppsala/Örebro (2) - Katrineholm Kul sjh (13012) - Kvinnoklinik (451)",
                                    "PosId4": 657,
                                    "PosLevel": 3,
                                    "UnitCode": "451",
                                    "TopPosId": 5,
                                    "TopPosCode": "2"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):2 - 13012 - 101,Organisationsenhet(namn och kod):OC Uppsala/Örebro - Katrineholm Kul sjh - Med klin",
                                "id": 373,
                                "data": {
                                    "PosId": 373,
                                    "PosCode": "2 - 13012 - 101",
                                    "PosName": "OC Uppsala/Örebro - Katrineholm Kul sjh - Med klin",
                                    "PosNameWithCode": "OC Uppsala/Örebro (2) - Katrineholm Kul sjh (13012) - Med klin (101)",
                                    "PosId4": 373,
                                    "PosLevel": 3,
                                    "UnitCode": "101",
                                    "TopPosId": 5,
                                    "TopPosCode": "2"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):2 - 13012 - 521,Organisationsenhet(namn och kod):OC Uppsala/Örebro - Katrineholm Kul sjh - ÖNH",
                                "id": 716,
                                "data": {
                                    "PosId": 716,
                                    "PosCode": "2 - 13012 - 521",
                                    "PosName": "OC Uppsala/Örebro - Katrineholm Kul sjh - ÖNH",
                                    "PosNameWithCode": "OC Uppsala/Örebro (2) - Katrineholm Kul sjh (13012) - ÖNH (521)",
                                    "PosId4": 716,
                                    "PosLevel": 3,
                                    "UnitCode": "521",
                                    "TopPosId": 5,
                                    "TopPosCode": "2"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):2 - 54011 - 301,Organisationsenhet(namn och kod):OC Uppsala/Örebro - Kristinehamns sjh - Kir klin",
                                "id": 287,
                                "data": {
                                    "PosId": 287,
                                    "PosCode": "2 - 54011 - 301",
                                    "PosName": "OC Uppsala/Örebro - Kristinehamns sjh - Kir klin",
                                    "PosNameWithCode": "OC Uppsala/Örebro (2) - Kristinehamns sjh (54011) - Kir klin (301)",
                                    "PosId4": 287,
                                    "PosLevel": 3,
                                    "UnitCode": "301",
                                    "TopPosId": 5,
                                    "TopPosCode": "2"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):2 - 54011 - 101,Organisationsenhet(namn och kod):OC Uppsala/Örebro - Kristinehamns sjh - Med klin",
                                "id": 361,
                                "data": {
                                    "PosId": 361,
                                    "PosCode": "2 - 54011 - 101",
                                    "PosName": "OC Uppsala/Örebro - Kristinehamns sjh - Med klin",
                                    "PosNameWithCode": "OC Uppsala/Örebro (2) - Kristinehamns sjh (54011) - Med klin (101)",
                                    "PosId4": 361,
                                    "PosLevel": 3,
                                    "UnitCode": "101",
                                    "TopPosId": 5,
                                    "TopPosCode": "2"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):2 - 13012 - 211,Organisationsenhet(namn och kod):OC Uppsala/Örebro - Kullbergiska sjh - Hud klin",
                                "id": 966,
                                "data": {
                                    "PosId": 966,
                                    "PosCode": "2 - 13012 - 211",
                                    "PosName": "OC Uppsala/Örebro - Kullbergiska sjh - Hud klin",
                                    "PosNameWithCode": "OC Uppsala/Örebro (2) - Kullbergiska sjh (13012) - Hud klin (211)",
                                    "PosId4": 966,
                                    "PosLevel": 3,
                                    "UnitCode": "211",
                                    "TopPosId": 5,
                                    "TopPosCode": "2"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):2 - 13012 - 301,Organisationsenhet(namn och kod):OC Uppsala/Örebro - Kullbergiska sjh - Kir klin",
                                "id": 1022,
                                "data": {
                                    "PosId": 1022,
                                    "PosCode": "2 - 13012 - 301",
                                    "PosName": "OC Uppsala/Örebro - Kullbergiska sjh - Kir klin",
                                    "PosNameWithCode": "OC Uppsala/Örebro (2) - Kullbergiska sjh (13012) - Kir klin (301)",
                                    "PosId4": 1022,
                                    "PosLevel": 3,
                                    "UnitCode": "301",
                                    "TopPosId": 5,
                                    "TopPosCode": "2"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):2 - 13012 - 101,Organisationsenhet(namn och kod):OC Uppsala/Örebro - Kullbergiska sjh - Med klin",
                                "id": 1459,
                                "data": {
                                    "PosId": 1459,
                                    "PosCode": "2 - 13012 - 101",
                                    "PosName": "OC Uppsala/Örebro - Kullbergiska sjh - Med klin",
                                    "PosNameWithCode": "OC Uppsala/Örebro (2) - Kullbergiska sjh (13012) - Med klin (101)",
                                    "PosId4": 1459,
                                    "PosLevel": 3,
                                    "UnitCode": "101",
                                    "TopPosId": 5,
                                    "TopPosCode": "2"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):2 - 56012 - 301,Organisationsenhet(namn och kod):OC Uppsala/Örebro - Köping las - Kir klin",
                                "id": 999,
                                "data": {
                                    "PosId": 999,
                                    "PosCode": "2 - 56012 - 301",
                                    "PosName": "OC Uppsala/Örebro - Köping las - Kir klin",
                                    "PosNameWithCode": "OC Uppsala/Örebro (2) - Köping las (56012) - Kir klin (301)",
                                    "PosId4": 999,
                                    "PosLevel": 3,
                                    "UnitCode": "301",
                                    "TopPosId": 5,
                                    "TopPosCode": "2"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):2 - 56012 - 101,Organisationsenhet(namn och kod):OC Uppsala/Örebro - Köping las - Med klin",
                                "id": 269,
                                "data": {
                                    "PosId": 269,
                                    "PosCode": "2 - 56012 - 101",
                                    "PosName": "OC Uppsala/Örebro - Köping las - Med klin",
                                    "PosNameWithCode": "OC Uppsala/Örebro (2) - Köping las (56012) - Med klin (101)",
                                    "PosId4": 269,
                                    "PosLevel": 3,
                                    "UnitCode": "101",
                                    "TopPosId": 5,
                                    "TopPosCode": "2"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):2 - 55012 - 211,Organisationsenhet(namn och kod):OC Uppsala/Örebro - Lindesbergs las - Hudkliniken",
                                "id": 869,
                                "data": {
                                    "PosId": 869,
                                    "PosCode": "2 - 55012 - 211",
                                    "PosName": "OC Uppsala/Örebro - Lindesbergs las - Hudkliniken",
                                    "PosNameWithCode": "OC Uppsala/Örebro (2) - Lindesbergs las (55012) - Hudkliniken (211)",
                                    "PosId4": 869,
                                    "PosLevel": 3,
                                    "UnitCode": "211",
                                    "TopPosId": 5,
                                    "TopPosCode": "2"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):2 - 55012 - 301,Organisationsenhet(namn och kod):OC Uppsala/Örebro - Lindesbergs las - Kir klin",
                                "id": 279,
                                "data": {
                                    "PosId": 279,
                                    "PosCode": "2 - 55012 - 301",
                                    "PosName": "OC Uppsala/Örebro - Lindesbergs las - Kir klin",
                                    "PosNameWithCode": "OC Uppsala/Örebro (2) - Lindesbergs las (55012) - Kir klin (301)",
                                    "PosId4": 279,
                                    "PosLevel": 3,
                                    "UnitCode": "301",
                                    "TopPosId": 5,
                                    "TopPosCode": "2"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):2 - 55012 - 451,Organisationsenhet(namn och kod):OC Uppsala/Örebro - Lindesbergs las - Kvinnoklinik",
                                "id": 1058,
                                "data": {
                                    "PosId": 1058,
                                    "PosCode": "2 - 55012 - 451",
                                    "PosName": "OC Uppsala/Örebro - Lindesbergs las - Kvinnoklinik",
                                    "PosNameWithCode": "OC Uppsala/Örebro (2) - Lindesbergs las (55012) - Kvinnoklinik (451)",
                                    "PosId4": 1058,
                                    "PosLevel": 3,
                                    "UnitCode": "451",
                                    "TopPosId": 5,
                                    "TopPosCode": "2"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):2 - 55012 - 101,Organisationsenhet(namn och kod):OC Uppsala/Örebro - Lindesbergs las - Med klin",
                                "id": 486,
                                "data": {
                                    "PosId": 486,
                                    "PosCode": "2 - 55012 - 101",
                                    "PosName": "OC Uppsala/Örebro - Lindesbergs las - Med klin",
                                    "PosNameWithCode": "OC Uppsala/Örebro (2) - Lindesbergs las (55012) - Med klin (101)",
                                    "PosId4": 486,
                                    "PosLevel": 3,
                                    "UnitCode": "101",
                                    "TopPosId": 5,
                                    "TopPosCode": "2"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):2 - 55012 - 521,Organisationsenhet(namn och kod):OC Uppsala/Örebro - Lindesbergs las - ÖNH",
                                "id": 580,
                                "data": {
                                    "PosId": 580,
                                    "PosCode": "2 - 55012 - 521",
                                    "PosName": "OC Uppsala/Örebro - Lindesbergs las - ÖNH",
                                    "PosNameWithCode": "OC Uppsala/Örebro (2) - Lindesbergs las (55012) - ÖNH (521)",
                                    "PosId4": 580,
                                    "PosLevel": 3,
                                    "UnitCode": "521",
                                    "TopPosId": 5,
                                    "TopPosCode": "2"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):2 - 61030 - 301,Organisationsenhet(namn och kod):OC Uppsala/Örebro - Ljusdals sjh - Kir klin",
                                "id": 299,
                                "data": {
                                    "PosId": 299,
                                    "PosCode": "2 - 61030 - 301",
                                    "PosName": "OC Uppsala/Örebro - Ljusdals sjh - Kir klin",
                                    "PosNameWithCode": "OC Uppsala/Örebro (2) - Ljusdals sjh (61030) - Kir klin (301)",
                                    "PosId4": 299,
                                    "PosLevel": 3,
                                    "UnitCode": "301",
                                    "TopPosId": 5,
                                    "TopPosCode": "2"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):2 - 57012 - 301,Organisationsenhet(namn och kod):OC Uppsala/Örebro - Ludvika las - Kir klin",
                                "id": 292,
                                "data": {
                                    "PosId": 292,
                                    "PosCode": "2 - 57012 - 301",
                                    "PosName": "OC Uppsala/Örebro - Ludvika las - Kir klin",
                                    "PosNameWithCode": "OC Uppsala/Örebro (2) - Ludvika las (57012) - Kir klin (301)",
                                    "PosId4": 292,
                                    "PosLevel": 3,
                                    "UnitCode": "301",
                                    "TopPosId": 5,
                                    "TopPosCode": "2"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):2 - 57012 - 101,Organisationsenhet(namn och kod):OC Uppsala/Örebro - Ludvika las - Med klin",
                                "id": 362,
                                "data": {
                                    "PosId": 362,
                                    "PosCode": "2 - 57012 - 101",
                                    "PosName": "OC Uppsala/Örebro - Ludvika las - Med klin",
                                    "PosNameWithCode": "OC Uppsala/Örebro (2) - Ludvika las (57012) - Med klin (101)",
                                    "PosId4": 362,
                                    "PosLevel": 3,
                                    "UnitCode": "101",
                                    "TopPosId": 5,
                                    "TopPosCode": "2"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):2 - 57011 - 301,Organisationsenhet(namn och kod):OC Uppsala/Örebro - Mora Las - Kir klin",
                                "id": 220,
                                "data": {
                                    "PosId": 220,
                                    "PosCode": "2 - 57011 - 301",
                                    "PosName": "OC Uppsala/Örebro - Mora Las - Kir klin",
                                    "PosNameWithCode": "OC Uppsala/Örebro (2) - Mora Las (57011) - Kir klin (301)",
                                    "PosId4": 220,
                                    "PosLevel": 3,
                                    "UnitCode": "301",
                                    "TopPosId": 5,
                                    "TopPosCode": "2"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):2 - 57011 - 451,Organisationsenhet(namn och kod):OC Uppsala/Örebro - Mora Las - Kvinnoklinik",
                                "id": 808,
                                "data": {
                                    "PosId": 808,
                                    "PosCode": "2 - 57011 - 451",
                                    "PosName": "OC Uppsala/Örebro - Mora Las - Kvinnoklinik",
                                    "PosNameWithCode": "OC Uppsala/Örebro (2) - Mora Las (57011) - Kvinnoklinik (451)",
                                    "PosId4": 808,
                                    "PosLevel": 3,
                                    "UnitCode": "451",
                                    "TopPosId": 5,
                                    "TopPosCode": "2"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):2 - 57011 - 101,Organisationsenhet(namn och kod):OC Uppsala/Örebro - Mora Las - Med klin",
                                "id": 374,
                                "data": {
                                    "PosId": 374,
                                    "PosCode": "2 - 57011 - 101",
                                    "PosName": "OC Uppsala/Örebro - Mora Las - Med klin",
                                    "PosNameWithCode": "OC Uppsala/Örebro (2) - Mora Las (57011) - Med klin (101)",
                                    "PosId4": 374,
                                    "PosLevel": 3,
                                    "UnitCode": "101",
                                    "TopPosId": 5,
                                    "TopPosCode": "2"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):2 - 57011 - 741,Organisationsenhet(namn och kod):OC Uppsala/Örebro - Mora Las - Onk klin",
                                "id": 971,
                                "data": {
                                    "PosId": 971,
                                    "PosCode": "2 - 57011 - 741",
                                    "PosName": "OC Uppsala/Örebro - Mora Las - Onk klin",
                                    "PosNameWithCode": "OC Uppsala/Örebro (2) - Mora Las (57011) - Onk klin (741)",
                                    "PosId4": 971,
                                    "PosLevel": 3,
                                    "UnitCode": "741",
                                    "TopPosId": 5,
                                    "TopPosCode": "2"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):2 - 57011 - 311,Organisationsenhet(namn och kod):OC Uppsala/Örebro - Mora Las - Ortopedklin",
                                "id": 1020,
                                "data": {
                                    "PosId": 1020,
                                    "PosCode": "2 - 57011 - 311",
                                    "PosName": "OC Uppsala/Örebro - Mora Las - Ortopedklin",
                                    "PosNameWithCode": "OC Uppsala/Örebro (2) - Mora Las (57011) - Ortopedklin (311)",
                                    "PosId4": 1020,
                                    "PosLevel": 3,
                                    "UnitCode": "311",
                                    "TopPosId": 5,
                                    "TopPosCode": "2"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):2 - 57011 - 521,Organisationsenhet(namn och kod):OC Uppsala/Örebro - Mora Las - ÖNH",
                                "id": 1132,
                                "data": {
                                    "PosId": 1132,
                                    "PosCode": "2 - 57011 - 521",
                                    "PosName": "OC Uppsala/Örebro - Mora Las - ÖNH",
                                    "PosNameWithCode": "OC Uppsala/Örebro (2) - Mora Las (57011) - ÖNH (521)",
                                    "PosId4": 1132,
                                    "PosLevel": 3,
                                    "UnitCode": "521",
                                    "TopPosId": 5,
                                    "TopPosCode": "2"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):2 - 13011 - 211,Organisationsenhet(namn och kod):OC Uppsala/Örebro - Nyköping las - Hudkliniken",
                                "id": 1013,
                                "data": {
                                    "PosId": 1013,
                                    "PosCode": "2 - 13011 - 211",
                                    "PosName": "OC Uppsala/Örebro - Nyköping las - Hudkliniken",
                                    "PosNameWithCode": "OC Uppsala/Örebro (2) - Nyköping las (13011) - Hudkliniken (211)",
                                    "PosId4": 1013,
                                    "PosLevel": 3,
                                    "UnitCode": "211",
                                    "TopPosId": 5,
                                    "TopPosCode": "2"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):2 - 13011 - 301,Organisationsenhet(namn och kod):OC Uppsala/Örebro - Nyköping las - Kir klin",
                                "id": 276,
                                "data": {
                                    "PosId": 276,
                                    "PosCode": "2 - 13011 - 301",
                                    "PosName": "OC Uppsala/Örebro - Nyköping las - Kir klin",
                                    "PosNameWithCode": "OC Uppsala/Örebro (2) - Nyköping las (13011) - Kir klin (301)",
                                    "PosId4": 276,
                                    "PosLevel": 3,
                                    "UnitCode": "301",
                                    "TopPosId": 5,
                                    "TopPosCode": "2"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):2 - 13011 - 451,Organisationsenhet(namn och kod):OC Uppsala/Örebro - Nyköping las - Kvinnoklinik",
                                "id": 659,
                                "data": {
                                    "PosId": 659,
                                    "PosCode": "2 - 13011 - 451",
                                    "PosName": "OC Uppsala/Örebro - Nyköping las - Kvinnoklinik",
                                    "PosNameWithCode": "OC Uppsala/Örebro (2) - Nyköping las (13011) - Kvinnoklinik (451)",
                                    "PosId4": 659,
                                    "PosLevel": 3,
                                    "UnitCode": "451",
                                    "TopPosId": 5,
                                    "TopPosCode": "2"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):2 - 13011 - 101,Organisationsenhet(namn och kod):OC Uppsala/Örebro - Nyköping las - Med klin",
                                "id": 63,
                                "data": {
                                    "PosId": 63,
                                    "PosCode": "2 - 13011 - 101",
                                    "PosName": "OC Uppsala/Örebro - Nyköping las - Med klin",
                                    "PosNameWithCode": "OC Uppsala/Örebro (2) - Nyköping las (13011) - Med klin (101)",
                                    "PosId4": 63,
                                    "PosLevel": 3,
                                    "UnitCode": "101",
                                    "TopPosId": 5,
                                    "TopPosCode": "2"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):2 - 13011 - 521,Organisationsenhet(namn och kod):OC Uppsala/Örebro - Nyköping las - ÖNH",
                                "id": 715,
                                "data": {
                                    "PosId": 715,
                                    "PosCode": "2 - 13011 - 521",
                                    "PosName": "OC Uppsala/Örebro - Nyköping las - ÖNH",
                                    "PosNameWithCode": "OC Uppsala/Örebro (2) - Nyköping las (13011) - ÖNH (521)",
                                    "PosId4": 715,
                                    "PosLevel": 3,
                                    "UnitCode": "521",
                                    "TopPosId": 5,
                                    "TopPosCode": "2"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):2 - 99999 - 999,Organisationsenhet(namn och kod):OC Uppsala/Örebro - Okänd sjukhus - Okänd",
                                "id": 1183,
                                "data": {
                                    "PosId": 1183,
                                    "PosCode": "2 - 99999 - 999",
                                    "PosName": "OC Uppsala/Örebro - Okänd sjukhus - Okänd",
                                    "PosNameWithCode": "OC Uppsala/Örebro (2) - Okänd sjukhus (99999) - Okänd (999)",
                                    "PosId4": 1183,
                                    "PosLevel": 3,
                                    "UnitCode": "999",
                                    "TopPosId": 5,
                                    "TopPosCode": "2"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):2 - 22222 - 912,Organisationsenhet(namn och kod):OC Uppsala/Örebro - Privat läkare - Klinik",
                                "id": 880,
                                "data": {
                                    "PosId": 880,
                                    "PosCode": "2 - 22222 - 912",
                                    "PosName": "OC Uppsala/Örebro - Privat läkare - Klinik",
                                    "PosNameWithCode": "OC Uppsala/Örebro (2) - Privat läkare (22222) - Klinik (912)",
                                    "PosId4": 880,
                                    "PosLevel": 3,
                                    "UnitCode": "912",
                                    "TopPosId": 5,
                                    "TopPosCode": "2"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):2 - 56011 - 101,Organisationsenhet(namn och kod):OC Uppsala/Örebro - Sala las - Med klin",
                                "id": 364,
                                "data": {
                                    "PosId": 364,
                                    "PosCode": "2 - 56011 - 101",
                                    "PosName": "OC Uppsala/Örebro - Sala las - Med klin",
                                    "PosNameWithCode": "OC Uppsala/Örebro (2) - Sala las (56011) - Med klin (101)",
                                    "PosId4": 364,
                                    "PosLevel": 3,
                                    "UnitCode": "101",
                                    "TopPosId": 5,
                                    "TopPosCode": "2"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):2 - 12011 - 301,Organisationsenhet(namn och kod):OC Uppsala/Örebro - Samariterhemmet - Kir klin",
                                "id": 513,
                                "data": {
                                    "PosId": 513,
                                    "PosCode": "2 - 12011 - 301",
                                    "PosName": "OC Uppsala/Örebro - Samariterhemmet - Kir klin",
                                    "PosNameWithCode": "OC Uppsala/Örebro (2) - Samariterhemmet (12011) - Kir klin (301)",
                                    "PosId4": 513,
                                    "PosLevel": 3,
                                    "UnitCode": "301",
                                    "TopPosId": 5,
                                    "TopPosCode": "2"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):2 - 54013 - 301,Organisationsenhet(namn och kod):OC Uppsala/Örebro - Säffle sjh - Kir klin",
                                "id": 289,
                                "data": {
                                    "PosId": 289,
                                    "PosCode": "2 - 54013 - 301",
                                    "PosName": "OC Uppsala/Örebro - Säffle sjh - Kir klin",
                                    "PosNameWithCode": "OC Uppsala/Örebro (2) - Säffle sjh (54013) - Kir klin (301)",
                                    "PosId4": 289,
                                    "PosLevel": 3,
                                    "UnitCode": "301",
                                    "TopPosId": 5,
                                    "TopPosCode": "2"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):2 - 54013 - 101,Organisationsenhet(namn och kod):OC Uppsala/Örebro - Säffle sjh - Med klin",
                                "id": 365,
                                "data": {
                                    "PosId": 365,
                                    "PosCode": "2 - 54013 - 101",
                                    "PosName": "OC Uppsala/Örebro - Säffle sjh - Med klin",
                                    "PosNameWithCode": "OC Uppsala/Örebro (2) - Säffle sjh (54013) - Med klin (101)",
                                    "PosId4": 365,
                                    "PosLevel": 3,
                                    "UnitCode": "101",
                                    "TopPosId": 5,
                                    "TopPosCode": "2"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):2 - 54013 - 521,Organisationsenhet(namn och kod):OC Uppsala/Örebro - Säffle sjh - ÖNH",
                                "id": 1134,
                                "data": {
                                    "PosId": 1134,
                                    "PosCode": "2 - 54013 - 521",
                                    "PosName": "OC Uppsala/Örebro - Säffle sjh - ÖNH",
                                    "PosNameWithCode": "OC Uppsala/Örebro (2) - Säffle sjh (54013) - ÖNH (521)",
                                    "PosId4": 1134,
                                    "PosLevel": 3,
                                    "UnitCode": "521",
                                    "TopPosId": 5,
                                    "TopPosCode": "2"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):2 - 61014 - 301,Organisationsenhet(namn och kod):OC Uppsala/Örebro - Söderhamns sjh - Kir klin",
                                "id": 296,
                                "data": {
                                    "PosId": 296,
                                    "PosCode": "2 - 61014 - 301",
                                    "PosName": "OC Uppsala/Örebro - Söderhamns sjh - Kir klin",
                                    "PosNameWithCode": "OC Uppsala/Örebro (2) - Söderhamns sjh (61014) - Kir klin (301)",
                                    "PosId4": 296,
                                    "PosLevel": 3,
                                    "UnitCode": "301",
                                    "TopPosId": 5,
                                    "TopPosCode": "2"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):2 - 54014 - 301,Organisationsenhet(namn och kod):OC Uppsala/Örebro - Torsby sjh - Kir klin",
                                "id": 264,
                                "data": {
                                    "PosId": 264,
                                    "PosCode": "2 - 54014 - 301",
                                    "PosName": "OC Uppsala/Örebro - Torsby sjh - Kir klin",
                                    "PosNameWithCode": "OC Uppsala/Örebro (2) - Torsby sjh (54014) - Kir klin (301)",
                                    "PosId4": 264,
                                    "PosLevel": 3,
                                    "UnitCode": "301",
                                    "TopPosId": 5,
                                    "TopPosCode": "2"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):2 - 54014 - 101,Organisationsenhet(namn och kod):OC Uppsala/Örebro - Torsby sjh - Med klin",
                                "id": 366,
                                "data": {
                                    "PosId": 366,
                                    "PosCode": "2 - 54014 - 101",
                                    "PosName": "OC Uppsala/Örebro - Torsby sjh - Med klin",
                                    "PosNameWithCode": "OC Uppsala/Örebro (2) - Torsby sjh (54014) - Med klin (101)",
                                    "PosId4": 366,
                                    "PosLevel": 3,
                                    "UnitCode": "101",
                                    "TopPosId": 5,
                                    "TopPosCode": "2"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):2 - 12001 - 201,Organisationsenhet(namn och kod):OC Uppsala/Örebro - UAS - Barnmedicin",
                                "id": 1051,
                                "data": {
                                    "PosId": 1051,
                                    "PosCode": "2 - 12001 - 201",
                                    "PosName": "OC Uppsala/Örebro - UAS - Barnmedicin",
                                    "PosNameWithCode": "OC Uppsala/Örebro (2) - UAS (12001) - Barnmedicin (201)",
                                    "PosId4": 1051,
                                    "PosLevel": 3,
                                    "UnitCode": "201",
                                    "TopPosId": 5,
                                    "TopPosCode": "2"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):2 - 12001 - 202,Organisationsenhet(namn och kod):OC Uppsala/Örebro - UAS - Barnonkologi",
                                "id": 16825,
                                "data": {
                                    "PosId": 16825,
                                    "PosCode": "2 - 12001 - 202",
                                    "PosName": "OC Uppsala/Örebro - UAS - Barnonkologi",
                                    "PosNameWithCode": "OC Uppsala/Örebro (2) - UAS (12001) - Barnonkologi (202)",
                                    "PosId4": 16825,
                                    "PosLevel": 3,
                                    "UnitCode": "202",
                                    "TopPosId": 5,
                                    "TopPosCode": "2"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):2 - 12001 - 161,Organisationsenhet(namn och kod):OC Uppsala/Örebro - UAS - Endokrin",
                                "id": 1187,
                                "data": {
                                    "PosId": 1187,
                                    "PosCode": "2 - 12001 - 161",
                                    "PosName": "OC Uppsala/Örebro - UAS - Endokrin",
                                    "PosNameWithCode": "OC Uppsala/Örebro (2) - UAS (12001) - Endokrin (161)",
                                    "PosId4": 1187,
                                    "PosLevel": 3,
                                    "UnitCode": "161",
                                    "TopPosId": 5,
                                    "TopPosCode": "2"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):2 - 12001 - 211,Organisationsenhet(namn och kod):OC Uppsala/Örebro - UAS - Hudkliniken",
                                "id": 867,
                                "data": {
                                    "PosId": 867,
                                    "PosCode": "2 - 12001 - 211",
                                    "PosName": "OC Uppsala/Örebro - UAS - Hudkliniken",
                                    "PosNameWithCode": "OC Uppsala/Örebro (2) - UAS (12001) - Hudkliniken (211)",
                                    "PosId4": 867,
                                    "PosLevel": 3,
                                    "UnitCode": "211",
                                    "TopPosId": 5,
                                    "TopPosCode": "2"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):2 - 12001 - 301,Organisationsenhet(namn och kod):OC Uppsala/Örebro - UAS - Kir klin",
                                "id": 218,
                                "data": {
                                    "PosId": 218,
                                    "PosCode": "2 - 12001 - 301",
                                    "PosName": "OC Uppsala/Örebro - UAS - Kir klin",
                                    "PosNameWithCode": "OC Uppsala/Örebro (2) - UAS (12001) - Kir klin (301)",
                                    "PosId4": 218,
                                    "PosLevel": 3,
                                    "UnitCode": "301",
                                    "TopPosId": 5,
                                    "TopPosCode": "2"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):2 - 12001 - 451,Organisationsenhet(namn och kod):OC Uppsala/Örebro - UAS - Kvinnoklinik",
                                "id": 653,
                                "data": {
                                    "PosId": 653,
                                    "PosCode": "2 - 12001 - 451",
                                    "PosName": "OC Uppsala/Örebro - UAS - Kvinnoklinik",
                                    "PosNameWithCode": "OC Uppsala/Örebro (2) - UAS (12001) - Kvinnoklinik (451)",
                                    "PosId4": 653,
                                    "PosLevel": 3,
                                    "UnitCode": "451",
                                    "TopPosId": 5,
                                    "TopPosCode": "2"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):2 - 12001 - 111,Organisationsenhet(namn och kod):OC Uppsala/Örebro - UAS - Lung klin",
                                "id": 629,
                                "data": {
                                    "PosId": 629,
                                    "PosCode": "2 - 12001 - 111",
                                    "PosName": "OC Uppsala/Örebro - UAS - Lung klin",
                                    "PosNameWithCode": "OC Uppsala/Örebro (2) - UAS (12001) - Lung klin (111)",
                                    "PosId4": 629,
                                    "PosLevel": 3,
                                    "UnitCode": "111",
                                    "TopPosId": 5,
                                    "TopPosCode": "2"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):2 - 12001 - 101,Organisationsenhet(namn och kod):OC Uppsala/Örebro - UAS - Med klin",
                                "id": 367,
                                "data": {
                                    "PosId": 367,
                                    "PosCode": "2 - 12001 - 101",
                                    "PosName": "OC Uppsala/Örebro - UAS - Med klin",
                                    "PosNameWithCode": "OC Uppsala/Örebro (2) - UAS (12001) - Med klin (101)",
                                    "PosId4": 367,
                                    "PosLevel": 3,
                                    "UnitCode": "101",
                                    "TopPosId": 5,
                                    "TopPosCode": "2"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):2 - 12001 - 331,Organisationsenhet(namn och kod):OC Uppsala/Örebro - UAS - Neurokirurgklin",
                                "id": 804,
                                "data": {
                                    "PosId": 804,
                                    "PosCode": "2 - 12001 - 331",
                                    "PosName": "OC Uppsala/Örebro - UAS - Neurokirurgklin",
                                    "PosNameWithCode": "OC Uppsala/Örebro (2) - UAS (12001) - Neurokirurgklin (331)",
                                    "PosId4": 804,
                                    "PosLevel": 3,
                                    "UnitCode": "331",
                                    "TopPosId": 5,
                                    "TopPosCode": "2"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):2 - 12001 - 221,Organisationsenhet(namn och kod):OC Uppsala/Örebro - UAS - Neurologiska kliniken",
                                "id": 1059,
                                "data": {
                                    "PosId": 1059,
                                    "PosCode": "2 - 12001 - 221",
                                    "PosName": "OC Uppsala/Örebro - UAS - Neurologiska kliniken",
                                    "PosNameWithCode": "OC Uppsala/Örebro (2) - UAS (12001) - Neurologiska kliniken (221)",
                                    "PosId4": 1059,
                                    "PosLevel": 3,
                                    "UnitCode": "221",
                                    "TopPosId": 5,
                                    "TopPosCode": "2"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):2 - 12001 - 742,Organisationsenhet(namn och kod):OC Uppsala/Örebro - UAS - Onk Endokrin",
                                "id": 1555,
                                "data": {
                                    "PosId": 1555,
                                    "PosCode": "2 - 12001 - 742",
                                    "PosName": "OC Uppsala/Örebro - UAS - Onk Endokrin",
                                    "PosNameWithCode": "OC Uppsala/Örebro (2) - UAS (12001) - Onk Endokrin (742)",
                                    "PosId4": 1555,
                                    "PosLevel": 3,
                                    "UnitCode": "742",
                                    "TopPosId": 5,
                                    "TopPosCode": "2"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):2 - 12001 - 741,Organisationsenhet(namn och kod):OC Uppsala/Örebro - UAS - Onk klin",
                                "id": 421,
                                "data": {
                                    "PosId": 421,
                                    "PosCode": "2 - 12001 - 741",
                                    "PosName": "OC Uppsala/Örebro - UAS - Onk klin",
                                    "PosNameWithCode": "OC Uppsala/Örebro (2) - UAS (12001) - Onk klin (741)",
                                    "PosId4": 421,
                                    "PosLevel": 3,
                                    "UnitCode": "741",
                                    "TopPosId": 5,
                                    "TopPosCode": "2"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):2 - 12001 - 351,Organisationsenhet(namn och kod):OC Uppsala/Örebro - UAS - Plastikkir. klin",
                                "id": 866,
                                "data": {
                                    "PosId": 866,
                                    "PosCode": "2 - 12001 - 351",
                                    "PosName": "OC Uppsala/Örebro - UAS - Plastikkir. klin",
                                    "PosNameWithCode": "OC Uppsala/Örebro (2) - UAS (12001) - Plastikkir. klin (351)",
                                    "PosId4": 866,
                                    "PosLevel": 3,
                                    "UnitCode": "351",
                                    "TopPosId": 5,
                                    "TopPosCode": "2"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):2 - 12001 - 555,Organisationsenhet(namn och kod):OC Uppsala/Örebro - UAS - Test",
                                "id": 903,
                                "data": {
                                    "PosId": 903,
                                    "PosCode": "2 - 12001 - 555",
                                    "PosName": "OC Uppsala/Örebro - UAS - Test",
                                    "PosNameWithCode": "OC Uppsala/Örebro (2) - UAS (12001) - Test (555)",
                                    "PosId4": 903,
                                    "PosLevel": 3,
                                    "UnitCode": "555",
                                    "TopPosId": 5,
                                    "TopPosCode": "2"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):2 - 12001 - 341,Organisationsenhet(namn och kod):OC Uppsala/Örebro - UAS - Thorax klin",
                                "id": 1049,
                                "data": {
                                    "PosId": 1049,
                                    "PosCode": "2 - 12001 - 341",
                                    "PosName": "OC Uppsala/Örebro - UAS - Thorax klin",
                                    "PosNameWithCode": "OC Uppsala/Örebro (2) - UAS (12001) - Thorax klin (341)",
                                    "PosId4": 1049,
                                    "PosLevel": 3,
                                    "UnitCode": "341",
                                    "TopPosId": 5,
                                    "TopPosCode": "2"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):2 - 12001 - 371,Organisationsenhet(namn och kod):OC Uppsala/Örebro - UAS - Transplantations klin",
                                "id": 1050,
                                "data": {
                                    "PosId": 1050,
                                    "PosCode": "2 - 12001 - 371",
                                    "PosName": "OC Uppsala/Örebro - UAS - Transplantations klin",
                                    "PosNameWithCode": "OC Uppsala/Örebro (2) - UAS (12001) - Transplantations klin (371)",
                                    "PosId4": 1050,
                                    "PosLevel": 3,
                                    "UnitCode": "371",
                                    "TopPosId": 5,
                                    "TopPosCode": "2"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):2 - 12001 - 361,Organisationsenhet(namn och kod):OC Uppsala/Örebro - UAS - Uro klin",
                                "id": 206,
                                "data": {
                                    "PosId": 206,
                                    "PosCode": "2 - 12001 - 361",
                                    "PosName": "OC Uppsala/Örebro - UAS - Uro klin",
                                    "PosNameWithCode": "OC Uppsala/Örebro (2) - UAS (12001) - Uro klin (361)",
                                    "PosId4": 206,
                                    "PosLevel": 3,
                                    "UnitCode": "361",
                                    "TopPosId": 5,
                                    "TopPosCode": "2"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):2 - 12001 - 521,Organisationsenhet(namn och kod):OC Uppsala/Örebro - UAS - ÖNH klin",
                                "id": 550,
                                "data": {
                                    "PosId": 550,
                                    "PosCode": "2 - 12001 - 521",
                                    "PosName": "OC Uppsala/Örebro - UAS - ÖNH klin",
                                    "PosNameWithCode": "OC Uppsala/Örebro (2) - UAS (12001) - ÖNH klin (521)",
                                    "PosId4": 550,
                                    "PosLevel": 3,
                                    "UnitCode": "521",
                                    "TopPosId": 5,
                                    "TopPosCode": "2"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):2 - 1263 - 912,Organisationsenhet(namn och kod):OC Uppsala/Örebro - Uppsala Cancer Clinic - Klinik",
                                "id": 1294,
                                "data": {
                                    "PosId": 1294,
                                    "PosCode": "2 - 1263 - 912",
                                    "PosName": "OC Uppsala/Örebro - Uppsala Cancer Clinic - Klinik",
                                    "PosNameWithCode": "OC Uppsala/Örebro (2) - Uppsala Cancer Clinic (1263) - Klinik (912)",
                                    "PosId4": 1294,
                                    "PosLevel": 3,
                                    "UnitCode": "912",
                                    "TopPosId": 5,
                                    "TopPosCode": "2"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):2 - 11111 - 913,Organisationsenhet(namn och kod):OC Uppsala/Örebro - Vårdcentral - Klinik",
                                "id": 876,
                                "data": {
                                    "PosId": 876,
                                    "PosCode": "2 - 11111 - 913",
                                    "PosName": "OC Uppsala/Örebro - Vårdcentral - Klinik",
                                    "PosNameWithCode": "OC Uppsala/Örebro (2) - Vårdcentral (11111) - Klinik (913)",
                                    "PosId4": 876,
                                    "PosLevel": 3,
                                    "UnitCode": "913",
                                    "TopPosId": 5,
                                    "TopPosCode": "2"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):2 - 56010 - 211,Organisationsenhet(namn och kod):OC Uppsala/Örebro - Västerås C.las - Hudkliniken",
                                "id": 864,
                                "data": {
                                    "PosId": 864,
                                    "PosCode": "2 - 56010 - 211",
                                    "PosName": "OC Uppsala/Örebro - Västerås C.las - Hudkliniken",
                                    "PosNameWithCode": "OC Uppsala/Örebro (2) - Västerås C.las (56010) - Hudkliniken (211)",
                                    "PosId4": 864,
                                    "PosLevel": 3,
                                    "UnitCode": "211",
                                    "TopPosId": 5,
                                    "TopPosCode": "2"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):2 - 56010 - 301,Organisationsenhet(namn och kod):OC Uppsala/Örebro - Västerås C.las - Kir klin",
                                "id": 182,
                                "data": {
                                    "PosId": 182,
                                    "PosCode": "2 - 56010 - 301",
                                    "PosName": "OC Uppsala/Örebro - Västerås C.las - Kir klin",
                                    "PosNameWithCode": "OC Uppsala/Örebro (2) - Västerås C.las (56010) - Kir klin (301)",
                                    "PosId4": 182,
                                    "PosLevel": 3,
                                    "UnitCode": "301",
                                    "TopPosId": 5,
                                    "TopPosCode": "2"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):2 - 56010 - 451,Organisationsenhet(namn och kod):OC Uppsala/Örebro - Västerås C.las - Kvinnoklinik",
                                "id": 658,
                                "data": {
                                    "PosId": 658,
                                    "PosCode": "2 - 56010 - 451",
                                    "PosName": "OC Uppsala/Örebro - Västerås C.las - Kvinnoklinik",
                                    "PosNameWithCode": "OC Uppsala/Örebro (2) - Västerås C.las (56010) - Kvinnoklinik (451)",
                                    "PosId4": 658,
                                    "PosLevel": 3,
                                    "UnitCode": "451",
                                    "TopPosId": 5,
                                    "TopPosCode": "2"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):2 - 56010 - 111,Organisationsenhet(namn och kod):OC Uppsala/Örebro - Västerås C.las - Lung klin",
                                "id": 628,
                                "data": {
                                    "PosId": 628,
                                    "PosCode": "2 - 56010 - 111",
                                    "PosName": "OC Uppsala/Örebro - Västerås C.las - Lung klin",
                                    "PosNameWithCode": "OC Uppsala/Örebro (2) - Västerås C.las (56010) - Lung klin (111)",
                                    "PosId4": 628,
                                    "PosLevel": 3,
                                    "UnitCode": "111",
                                    "TopPosId": 5,
                                    "TopPosCode": "2"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):2 - 56010 - 101,Organisationsenhet(namn och kod):OC Uppsala/Örebro - Västerås C.las - Med klin",
                                "id": 375,
                                "data": {
                                    "PosId": 375,
                                    "PosCode": "2 - 56010 - 101",
                                    "PosName": "OC Uppsala/Örebro - Västerås C.las - Med klin",
                                    "PosNameWithCode": "OC Uppsala/Örebro (2) - Västerås C.las (56010) - Med klin (101)",
                                    "PosId4": 375,
                                    "PosLevel": 3,
                                    "UnitCode": "101",
                                    "TopPosId": 5,
                                    "TopPosCode": "2"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):2 - 56010 - 221,Organisationsenhet(namn och kod):OC Uppsala/Örebro - Västerås C.las - Neurologiska kliniken",
                                "id": 1062,
                                "data": {
                                    "PosId": 1062,
                                    "PosCode": "2 - 56010 - 221",
                                    "PosName": "OC Uppsala/Örebro - Västerås C.las - Neurologiska kliniken",
                                    "PosNameWithCode": "OC Uppsala/Örebro (2) - Västerås C.las (56010) - Neurologiska kliniken (221)",
                                    "PosId4": 1062,
                                    "PosLevel": 3,
                                    "UnitCode": "221",
                                    "TopPosId": 5,
                                    "TopPosCode": "2"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):2 - 56010 - 741,Organisationsenhet(namn och kod):OC Uppsala/Örebro - Västerås C.las - Onk klin",
                                "id": 382,
                                "data": {
                                    "PosId": 382,
                                    "PosCode": "2 - 56010 - 741",
                                    "PosName": "OC Uppsala/Örebro - Västerås C.las - Onk klin",
                                    "PosNameWithCode": "OC Uppsala/Örebro (2) - Västerås C.las (56010) - Onk klin (741)",
                                    "PosId4": 382,
                                    "PosLevel": 3,
                                    "UnitCode": "741",
                                    "TopPosId": 5,
                                    "TopPosCode": "2"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):2 - 56010 - 351,Organisationsenhet(namn och kod):OC Uppsala/Örebro - Västerås C.las - Plastikkir",
                                "id": 894,
                                "data": {
                                    "PosId": 894,
                                    "PosCode": "2 - 56010 - 351",
                                    "PosName": "OC Uppsala/Örebro - Västerås C.las - Plastikkir",
                                    "PosNameWithCode": "OC Uppsala/Örebro (2) - Västerås C.las (56010) - Plastikkir (351)",
                                    "PosId4": 894,
                                    "PosLevel": 3,
                                    "UnitCode": "351",
                                    "TopPosId": 5,
                                    "TopPosCode": "2"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):2 - 56010 - 361,Organisationsenhet(namn och kod):OC Uppsala/Örebro - Västerås C.las - Uro klin",
                                "id": 290,
                                "data": {
                                    "PosId": 290,
                                    "PosCode": "2 - 56010 - 361",
                                    "PosName": "OC Uppsala/Örebro - Västerås C.las - Uro klin",
                                    "PosNameWithCode": "OC Uppsala/Örebro (2) - Västerås C.las (56010) - Uro klin (361)",
                                    "PosId4": 290,
                                    "PosLevel": 3,
                                    "UnitCode": "361",
                                    "TopPosId": 5,
                                    "TopPosCode": "2"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):2 - 56010 - 511,Organisationsenhet(namn och kod):OC Uppsala/Örebro - Västerås C.las - Ögonkliniken",
                                "id": 1000,
                                "data": {
                                    "PosId": 1000,
                                    "PosCode": "2 - 56010 - 511",
                                    "PosName": "OC Uppsala/Örebro - Västerås C.las - Ögonkliniken",
                                    "PosNameWithCode": "OC Uppsala/Örebro (2) - Västerås C.las (56010) - Ögonkliniken (511)",
                                    "PosId4": 1000,
                                    "PosLevel": 3,
                                    "UnitCode": "511",
                                    "TopPosId": 5,
                                    "TopPosCode": "2"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):2 - 56010 - 521,Organisationsenhet(namn och kod):OC Uppsala/Örebro - Västerås C.las - ÖNH",
                                "id": 602,
                                "data": {
                                    "PosId": 602,
                                    "PosCode": "2 - 56010 - 521",
                                    "PosName": "OC Uppsala/Örebro - Västerås C.las - ÖNH",
                                    "PosNameWithCode": "OC Uppsala/Örebro (2) - Västerås C.las (56010) - ÖNH (521)",
                                    "PosId4": 602,
                                    "PosLevel": 3,
                                    "UnitCode": "521",
                                    "TopPosId": 5,
                                    "TopPosCode": "2"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):2 - 55010 - 161,Organisationsenhet(namn och kod):OC Uppsala/Örebro - Örebro U.sjh - Endokrin",
                                "id": 1193,
                                "data": {
                                    "PosId": 1193,
                                    "PosCode": "2 - 55010 - 161",
                                    "PosName": "OC Uppsala/Örebro - Örebro U.sjh - Endokrin",
                                    "PosNameWithCode": "OC Uppsala/Örebro (2) - Örebro U.sjh (55010) - Endokrin (161)",
                                    "PosId4": 1193,
                                    "PosLevel": 3,
                                    "UnitCode": "161",
                                    "TopPosId": 5,
                                    "TopPosCode": "2"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):2 - 55010 - 751,Organisationsenhet(namn och kod):OC Uppsala/Örebro - Örebro U.sjh - Gynonk",
                                "id": 624,
                                "data": {
                                    "PosId": 624,
                                    "PosCode": "2 - 55010 - 751",
                                    "PosName": "OC Uppsala/Örebro - Örebro U.sjh - Gynonk",
                                    "PosNameWithCode": "OC Uppsala/Örebro (2) - Örebro U.sjh (55010) - Gynonk (751)",
                                    "PosId4": 624,
                                    "PosLevel": 3,
                                    "UnitCode": "751",
                                    "TopPosId": 5,
                                    "TopPosCode": "2"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):2 - 55010 - 211,Organisationsenhet(namn och kod):OC Uppsala/Örebro - Örebro U.sjh - Hudkliniken",
                                "id": 574,
                                "data": {
                                    "PosId": 574,
                                    "PosCode": "2 - 55010 - 211",
                                    "PosName": "OC Uppsala/Örebro - Örebro U.sjh - Hudkliniken",
                                    "PosNameWithCode": "OC Uppsala/Örebro (2) - Örebro U.sjh (55010) - Hudkliniken (211)",
                                    "PosId4": 574,
                                    "PosLevel": 3,
                                    "UnitCode": "211",
                                    "TopPosId": 5,
                                    "TopPosCode": "2"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):2 - 55010 - 121,Organisationsenhet(namn och kod):OC Uppsala/Örebro - Örebro U.sjh - Infektionskliniken",
                                "id": 1234,
                                "data": {
                                    "PosId": 1234,
                                    "PosCode": "2 - 55010 - 121",
                                    "PosName": "OC Uppsala/Örebro - Örebro U.sjh - Infektionskliniken",
                                    "PosNameWithCode": "OC Uppsala/Örebro (2) - Örebro U.sjh (55010) - Infektionskliniken (121)",
                                    "PosId4": 1234,
                                    "PosLevel": 3,
                                    "UnitCode": "121",
                                    "TopPosId": 5,
                                    "TopPosCode": "2"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):2 - 55010 - 301,Organisationsenhet(namn och kod):OC Uppsala/Örebro - Örebro U.sjh - Kir klin",
                                "id": 209,
                                "data": {
                                    "PosId": 209,
                                    "PosCode": "2 - 55010 - 301",
                                    "PosName": "OC Uppsala/Örebro - Örebro U.sjh - Kir klin",
                                    "PosNameWithCode": "OC Uppsala/Örebro (2) - Örebro U.sjh (55010) - Kir klin (301)",
                                    "PosId4": 209,
                                    "PosLevel": 3,
                                    "UnitCode": "301",
                                    "TopPosId": 5,
                                    "TopPosCode": "2"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):2 - 55010 - 451,Organisationsenhet(namn och kod):OC Uppsala/Örebro - Örebro U.sjh - Kvinnoklinik",
                                "id": 617,
                                "data": {
                                    "PosId": 617,
                                    "PosCode": "2 - 55010 - 451",
                                    "PosName": "OC Uppsala/Örebro - Örebro U.sjh - Kvinnoklinik",
                                    "PosNameWithCode": "OC Uppsala/Örebro (2) - Örebro U.sjh (55010) - Kvinnoklinik (451)",
                                    "PosId4": 617,
                                    "PosLevel": 3,
                                    "UnitCode": "451",
                                    "TopPosId": 5,
                                    "TopPosCode": "2"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):2 - 55010 - 111,Organisationsenhet(namn och kod):OC Uppsala/Örebro - Örebro U.sjh - Lung klin",
                                "id": 630,
                                "data": {
                                    "PosId": 630,
                                    "PosCode": "2 - 55010 - 111",
                                    "PosName": "OC Uppsala/Örebro - Örebro U.sjh - Lung klin",
                                    "PosNameWithCode": "OC Uppsala/Örebro (2) - Örebro U.sjh (55010) - Lung klin (111)",
                                    "PosId4": 630,
                                    "PosLevel": 3,
                                    "UnitCode": "111",
                                    "TopPosId": 5,
                                    "TopPosCode": "2"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):2 - 55010 - 101,Organisationsenhet(namn och kod):OC Uppsala/Örebro - Örebro U.sjh - Med klin",
                                "id": 376,
                                "data": {
                                    "PosId": 376,
                                    "PosCode": "2 - 55010 - 101",
                                    "PosName": "OC Uppsala/Örebro - Örebro U.sjh - Med klin",
                                    "PosNameWithCode": "OC Uppsala/Örebro (2) - Örebro U.sjh (55010) - Med klin (101)",
                                    "PosId4": 376,
                                    "PosLevel": 3,
                                    "UnitCode": "101",
                                    "TopPosId": 5,
                                    "TopPosCode": "2"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):2 - 55010 - 221,Organisationsenhet(namn och kod):OC Uppsala/Örebro - Örebro U.sjh - Neurologiska kliniken",
                                "id": 1060,
                                "data": {
                                    "PosId": 1060,
                                    "PosCode": "2 - 55010 - 221",
                                    "PosName": "OC Uppsala/Örebro - Örebro U.sjh - Neurologiska kliniken",
                                    "PosNameWithCode": "OC Uppsala/Örebro (2) - Örebro U.sjh (55010) - Neurologiska kliniken (221)",
                                    "PosId4": 1060,
                                    "PosLevel": 3,
                                    "UnitCode": "221",
                                    "TopPosId": 5,
                                    "TopPosCode": "2"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):2 - 55010 - 741,Organisationsenhet(namn och kod):OC Uppsala/Örebro - Örebro U.sjh - Onk klin",
                                "id": 210,
                                "data": {
                                    "PosId": 210,
                                    "PosCode": "2 - 55010 - 741",
                                    "PosName": "OC Uppsala/Örebro - Örebro U.sjh - Onk klin",
                                    "PosNameWithCode": "OC Uppsala/Örebro (2) - Örebro U.sjh (55010) - Onk klin (741)",
                                    "PosId4": 210,
                                    "PosLevel": 3,
                                    "UnitCode": "741",
                                    "TopPosId": 5,
                                    "TopPosCode": "2"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):2 - 55010 - 351,Organisationsenhet(namn och kod):OC Uppsala/Örebro - Örebro U.sjh - Plastikkir. klin",
                                "id": 851,
                                "data": {
                                    "PosId": 851,
                                    "PosCode": "2 - 55010 - 351",
                                    "PosName": "OC Uppsala/Örebro - Örebro U.sjh - Plastikkir. klin",
                                    "PosNameWithCode": "OC Uppsala/Örebro (2) - Örebro U.sjh (55010) - Plastikkir. klin (351)",
                                    "PosId4": 851,
                                    "PosLevel": 3,
                                    "UnitCode": "351",
                                    "TopPosId": 5,
                                    "TopPosCode": "2"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):2 - 55010 - 361,Organisationsenhet(namn och kod):OC Uppsala/Örebro - Örebro U.sjh - Uro klin",
                                "id": 208,
                                "data": {
                                    "PosId": 208,
                                    "PosCode": "2 - 55010 - 361",
                                    "PosName": "OC Uppsala/Örebro - Örebro U.sjh - Uro klin",
                                    "PosNameWithCode": "OC Uppsala/Örebro (2) - Örebro U.sjh (55010) - Uro klin (361)",
                                    "PosId4": 208,
                                    "PosLevel": 3,
                                    "UnitCode": "361",
                                    "TopPosId": 5,
                                    "TopPosCode": "2"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):2 - 55010 - 511,Organisationsenhet(namn och kod):OC Uppsala/Örebro - Örebro U.sjh - Ögon klin",
                                "id": 1286,
                                "data": {
                                    "PosId": 1286,
                                    "PosCode": "2 - 55010 - 511",
                                    "PosName": "OC Uppsala/Örebro - Örebro U.sjh - Ögon klin",
                                    "PosNameWithCode": "OC Uppsala/Örebro (2) - Örebro U.sjh (55010) - Ögon klin (511)",
                                    "PosId4": 1286,
                                    "PosLevel": 3,
                                    "UnitCode": "511",
                                    "TopPosId": 5,
                                    "TopPosCode": "2"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):2 - 55010 - 521,Organisationsenhet(namn och kod):OC Uppsala/Örebro - Örebro U.sjh - ÖNH klin",
                                "id": 551,
                                "data": {
                                    "PosId": 551,
                                    "PosCode": "2 - 55010 - 521",
                                    "PosName": "OC Uppsala/Örebro - Örebro U.sjh - ÖNH klin",
                                    "PosNameWithCode": "OC Uppsala/Örebro (2) - Örebro U.sjh (55010) - ÖNH klin (521)",
                                    "PosId4": 551,
                                    "PosLevel": 3,
                                    "UnitCode": "521",
                                    "TopPosId": 5,
                                    "TopPosCode": "2"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):5 - 510036 - 731,Organisationsenhet(namn och kod):OC Väst - Aleris Röntgen Annedal - Röntgen",
                                "id": 1531,
                                "data": {
                                    "PosId": 1531,
                                    "PosCode": "5 - 510036 - 731",
                                    "PosName": "OC Väst - Aleris Röntgen Annedal - Röntgen",
                                    "PosNameWithCode": "OC Väst (5) - Aleris Röntgen Annedal (510036) - Röntgen (731)",
                                    "PosId4": 1531,
                                    "PosLevel": 3,
                                    "UnitCode": "731",
                                    "TopPosId": 2,
                                    "TopPosCode": "5"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):5 - 510037 - 731,Organisationsenhet(namn och kod):OC Väst - Aleris Röntgen Backa - Röntgen",
                                "id": 1533,
                                "data": {
                                    "PosId": 1533,
                                    "PosCode": "5 - 510037 - 731",
                                    "PosName": "OC Väst - Aleris Röntgen Backa - Röntgen",
                                    "PosNameWithCode": "OC Väst (5) - Aleris Röntgen Backa (510037) - Röntgen (731)",
                                    "PosId4": 1533,
                                    "PosLevel": 3,
                                    "UnitCode": "731",
                                    "TopPosId": 2,
                                    "TopPosCode": "5"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):5 - 510038 - 731,Organisationsenhet(namn och kod):OC Väst - Aleris Röntgen Mölndal - Röntgen",
                                "id": 1541,
                                "data": {
                                    "PosId": 1541,
                                    "PosCode": "5 - 510038 - 731",
                                    "PosName": "OC Väst - Aleris Röntgen Mölndal - Röntgen",
                                    "PosNameWithCode": "OC Väst (5) - Aleris Röntgen Mölndal (510038) - Röntgen (731)",
                                    "PosId4": 1541,
                                    "PosLevel": 3,
                                    "UnitCode": "731",
                                    "TopPosId": 2,
                                    "TopPosCode": "5"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):5 - 500006 - 301,Organisationsenhet(namn och kod):OC Väst - Aleris spec vård Gbg - Kirurgi",
                                "id": 1229,
                                "data": {
                                    "PosId": 1229,
                                    "PosCode": "5 - 500006 - 301",
                                    "PosName": "OC Väst - Aleris spec vård Gbg - Kirurgi",
                                    "PosNameWithCode": "OC Väst (5) - Aleris spec vård Gbg (500006) - Kirurgi (301)",
                                    "PosId4": 1229,
                                    "PosLevel": 3,
                                    "UnitCode": "301",
                                    "TopPosId": 2,
                                    "TopPosCode": "5"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):5 - 52012 - 211,Organisationsenhet(namn och kod):OC Väst - Alingsås - Hudkliniken",
                                "id": 841,
                                "data": {
                                    "PosId": 841,
                                    "PosCode": "5 - 52012 - 211",
                                    "PosName": "OC Väst - Alingsås - Hudkliniken",
                                    "PosNameWithCode": "OC Väst (5) - Alingsås (52012) - Hudkliniken (211)",
                                    "PosId4": 841,
                                    "PosLevel": 3,
                                    "UnitCode": "211",
                                    "TopPosId": 2,
                                    "TopPosCode": "5"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):5 - 52012 - 301,Organisationsenhet(namn och kod):OC Väst - Alingsås - Kirurgen",
                                "id": 161,
                                "data": {
                                    "PosId": 161,
                                    "PosCode": "5 - 52012 - 301",
                                    "PosName": "OC Väst - Alingsås - Kirurgen",
                                    "PosNameWithCode": "OC Väst (5) - Alingsås (52012) - Kirurgen (301)",
                                    "PosId4": 161,
                                    "PosLevel": 3,
                                    "UnitCode": "301",
                                    "TopPosId": 2,
                                    "TopPosCode": "5"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):5 - 52012 - 451,Organisationsenhet(namn och kod):OC Väst - Alingsås - Kvinnoklinik",
                                "id": 638,
                                "data": {
                                    "PosId": 638,
                                    "PosCode": "5 - 52012 - 451",
                                    "PosName": "OC Väst - Alingsås - Kvinnoklinik",
                                    "PosNameWithCode": "OC Väst (5) - Alingsås (52012) - Kvinnoklinik (451)",
                                    "PosId4": 638,
                                    "PosLevel": 3,
                                    "UnitCode": "451",
                                    "TopPosId": 2,
                                    "TopPosCode": "5"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):5 - 52012 - 101,Organisationsenhet(namn och kod):OC Väst - Alingsås - Medicinkliniken",
                                "id": 332,
                                "data": {
                                    "PosId": 332,
                                    "PosCode": "5 - 52012 - 101",
                                    "PosName": "OC Väst - Alingsås - Medicinkliniken",
                                    "PosNameWithCode": "OC Väst (5) - Alingsås (52012) - Medicinkliniken (101)",
                                    "PosId4": 332,
                                    "PosLevel": 3,
                                    "UnitCode": "101",
                                    "TopPosId": 2,
                                    "TopPosCode": "5"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):5 - 52012 - 731,Organisationsenhet(namn och kod):OC Väst - Alingsås - Röntgen",
                                "id": 1520,
                                "data": {
                                    "PosId": 1520,
                                    "PosCode": "5 - 52012 - 731",
                                    "PosName": "OC Väst - Alingsås - Röntgen",
                                    "PosNameWithCode": "OC Väst (5) - Alingsås (52012) - Röntgen (731)",
                                    "PosId4": 1520,
                                    "PosLevel": 3,
                                    "UnitCode": "731",
                                    "TopPosId": 2,
                                    "TopPosCode": "5"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):5 - 52012 - 521,Organisationsenhet(namn och kod):OC Väst - Alingsås - ÖNH",
                                "id": 1028,
                                "data": {
                                    "PosId": 1028,
                                    "PosCode": "5 - 52012 - 521",
                                    "PosName": "OC Väst - Alingsås - ÖNH",
                                    "PosNameWithCode": "OC Väst (5) - Alingsås (52012) - ÖNH (521)",
                                    "PosId4": 1028,
                                    "PosLevel": 3,
                                    "UnitCode": "521",
                                    "TopPosId": 2,
                                    "TopPosCode": "5"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):5 - 510041 - 731,Organisationsenhet(namn och kod):OC Väst - Almedal Röntgen Unilabs - Röntgen",
                                "id": 1543,
                                "data": {
                                    "PosId": 1543,
                                    "PosCode": "5 - 510041 - 731",
                                    "PosName": "OC Väst - Almedal Röntgen Unilabs - Röntgen",
                                    "PosNameWithCode": "OC Väst (5) - Almedal Röntgen Unilabs (510041) - Röntgen (731)",
                                    "PosId4": 1543,
                                    "PosLevel": 3,
                                    "UnitCode": "731",
                                    "TopPosId": 2,
                                    "TopPosCode": "5"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):5 - 500004 - 451,Organisationsenhet(namn och kod):OC Väst - Angereds sjukhus - Kvinnoklinik",
                                "id": 1166,
                                "data": {
                                    "PosId": 1166,
                                    "PosCode": "5 - 500004 - 451",
                                    "PosName": "OC Väst - Angereds sjukhus - Kvinnoklinik",
                                    "PosNameWithCode": "OC Väst (5) - Angereds sjukhus (500004) - Kvinnoklinik (451)",
                                    "PosId4": 1166,
                                    "PosLevel": 3,
                                    "UnitCode": "451",
                                    "TopPosId": 2,
                                    "TopPosCode": "5"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):5 - 500004 - 731,Organisationsenhet(namn och kod):OC Väst - Angereds sjukhus - Röntgen",
                                "id": 1519,
                                "data": {
                                    "PosId": 1519,
                                    "PosCode": "5 - 500004 - 731",
                                    "PosName": "OC Väst - Angereds sjukhus - Röntgen",
                                    "PosNameWithCode": "OC Väst (5) - Angereds sjukhus (500004) - Röntgen (731)",
                                    "PosId4": 1519,
                                    "PosLevel": 3,
                                    "UnitCode": "731",
                                    "TopPosId": 2,
                                    "TopPosCode": "5"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):5 - 500002 - 301,Organisationsenhet(namn och kod):OC Väst - Artclinic - Kirurgi",
                                "id": 1181,
                                "data": {
                                    "PosId": 1181,
                                    "PosCode": "5 - 500002 - 301",
                                    "PosName": "OC Väst - Artclinic - Kirurgi",
                                    "PosNameWithCode": "OC Väst (5) - Artclinic (500002) - Kirurgi (301)",
                                    "PosId4": 1181,
                                    "PosLevel": 3,
                                    "UnitCode": "301",
                                    "TopPosId": 2,
                                    "TopPosCode": "5"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):5 - 500002 - 351,Organisationsenhet(namn och kod):OC Väst - Artclinic - Plastikkirurgi",
                                "id": 504,
                                "data": {
                                    "PosId": 504,
                                    "PosCode": "5 - 500002 - 351",
                                    "PosName": "OC Väst - Artclinic - Plastikkirurgi",
                                    "PosNameWithCode": "OC Väst (5) - Artclinic (500002) - Plastikkirurgi (351)",
                                    "PosId4": 504,
                                    "PosLevel": 3,
                                    "UnitCode": "351",
                                    "TopPosId": 2,
                                    "TopPosCode": "5"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):5 - 52011 - 751,Organisationsenhet(namn och kod):OC Väst - Borås - Gyn onk klinik",
                                "id": 644,
                                "data": {
                                    "PosId": 644,
                                    "PosCode": "5 - 52011 - 751",
                                    "PosName": "OC Väst - Borås - Gyn onk klinik",
                                    "PosNameWithCode": "OC Väst (5) - Borås (52011) - Gyn onk klinik (751)",
                                    "PosId4": 644,
                                    "PosLevel": 3,
                                    "UnitCode": "751",
                                    "TopPosId": 2,
                                    "TopPosCode": "5"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):5 - 52011 - 211,Organisationsenhet(namn och kod):OC Väst - Borås - Hudkliniken",
                                "id": 742,
                                "data": {
                                    "PosId": 742,
                                    "PosCode": "5 - 52011 - 211",
                                    "PosName": "OC Väst - Borås - Hudkliniken",
                                    "PosNameWithCode": "OC Väst (5) - Borås (52011) - Hudkliniken (211)",
                                    "PosId4": 742,
                                    "PosLevel": 3,
                                    "UnitCode": "211",
                                    "TopPosId": 2,
                                    "TopPosCode": "5"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):5 - 52011 - 301,Organisationsenhet(namn och kod):OC Väst - Borås - Kirurgen",
                                "id": 160,
                                "data": {
                                    "PosId": 160,
                                    "PosCode": "5 - 52011 - 301",
                                    "PosName": "OC Väst - Borås - Kirurgen",
                                    "PosNameWithCode": "OC Väst (5) - Borås (52011) - Kirurgen (301)",
                                    "PosId4": 160,
                                    "PosLevel": 3,
                                    "UnitCode": "301",
                                    "TopPosId": 2,
                                    "TopPosCode": "5"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):5 - 52011 - 451,Organisationsenhet(namn och kod):OC Väst - Borås - Kvinnoklinik",
                                "id": 637,
                                "data": {
                                    "PosId": 637,
                                    "PosCode": "5 - 52011 - 451",
                                    "PosName": "OC Väst - Borås - Kvinnoklinik",
                                    "PosNameWithCode": "OC Väst (5) - Borås (52011) - Kvinnoklinik (451)",
                                    "PosId4": 637,
                                    "PosLevel": 3,
                                    "UnitCode": "451",
                                    "TopPosId": 2,
                                    "TopPosCode": "5"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):5 - 52011 - 111,Organisationsenhet(namn och kod):OC Väst - Borås - Lungmedicin",
                                "id": 693,
                                "data": {
                                    "PosId": 693,
                                    "PosCode": "5 - 52011 - 111",
                                    "PosName": "OC Väst - Borås - Lungmedicin",
                                    "PosNameWithCode": "OC Väst (5) - Borås (52011) - Lungmedicin (111)",
                                    "PosId4": 693,
                                    "PosLevel": 3,
                                    "UnitCode": "111",
                                    "TopPosId": 2,
                                    "TopPosCode": "5"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):5 - 52011 - 101,Organisationsenhet(namn och kod):OC Väst - Borås - Medicinkliniken",
                                "id": 330,
                                "data": {
                                    "PosId": 330,
                                    "PosCode": "5 - 52011 - 101",
                                    "PosName": "OC Väst - Borås - Medicinkliniken",
                                    "PosNameWithCode": "OC Väst (5) - Borås (52011) - Medicinkliniken (101)",
                                    "PosId4": 330,
                                    "PosLevel": 3,
                                    "UnitCode": "101",
                                    "TopPosId": 2,
                                    "TopPosCode": "5"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):5 - 52011 - 221,Organisationsenhet(namn och kod):OC Väst - Borås - Neurologiska kliniken",
                                "id": 1167,
                                "data": {
                                    "PosId": 1167,
                                    "PosCode": "5 - 52011 - 221",
                                    "PosName": "OC Väst - Borås - Neurologiska kliniken",
                                    "PosNameWithCode": "OC Väst (5) - Borås (52011) - Neurologiska kliniken (221)",
                                    "PosId4": 1167,
                                    "PosLevel": 3,
                                    "UnitCode": "221",
                                    "TopPosId": 2,
                                    "TopPosCode": "5"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):5 - 52011 - 741,Organisationsenhet(namn och kod):OC Väst - Borås - Onkologiska kliniken",
                                "id": 335,
                                "data": {
                                    "PosId": 335,
                                    "PosCode": "5 - 52011 - 741",
                                    "PosName": "OC Väst - Borås - Onkologiska kliniken",
                                    "PosNameWithCode": "OC Väst (5) - Borås (52011) - Onkologiska kliniken (741)",
                                    "PosId4": 335,
                                    "PosLevel": 3,
                                    "UnitCode": "741",
                                    "TopPosId": 2,
                                    "TopPosCode": "5"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):5 - 52011 - 731,Organisationsenhet(namn och kod):OC Väst - Borås - Röntgen",
                                "id": 1524,
                                "data": {
                                    "PosId": 1524,
                                    "PosCode": "5 - 52011 - 731",
                                    "PosName": "OC Väst - Borås - Röntgen",
                                    "PosNameWithCode": "OC Väst (5) - Borås (52011) - Röntgen (731)",
                                    "PosId4": 1524,
                                    "PosLevel": 3,
                                    "UnitCode": "731",
                                    "TopPosId": 2,
                                    "TopPosCode": "5"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):5 - 52011 - 521,Organisationsenhet(namn och kod):OC Väst - Borås - ÖNH",
                                "id": 334,
                                "data": {
                                    "PosId": 334,
                                    "PosCode": "5 - 52011 - 521",
                                    "PosName": "OC Väst - Borås - ÖNH",
                                    "PosNameWithCode": "OC Väst (5) - Borås (52011) - ÖNH (521)",
                                    "PosId4": 334,
                                    "PosLevel": 3,
                                    "UnitCode": "521",
                                    "TopPosId": 2,
                                    "TopPosCode": "5"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):5 - 500008 - 361,Organisationsenhet(namn och kod):OC Väst - Borås urologcentrum - Urologcentrum",
                                "id": 1402,
                                "data": {
                                    "PosId": 1402,
                                    "PosCode": "5 - 500008 - 361",
                                    "PosName": "OC Väst - Borås urologcentrum - Urologcentrum",
                                    "PosNameWithCode": "OC Väst (5) - Borås urologcentrum (500008) - Urologcentrum (361)",
                                    "PosId4": 1402,
                                    "PosLevel": 3,
                                    "UnitCode": "361",
                                    "TopPosId": 2,
                                    "TopPosCode": "5"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):5 - 50480 - 211,Organisationsenhet(namn och kod):OC Väst - Carlanderska - Hudkliniken",
                                "id": 748,
                                "data": {
                                    "PosId": 748,
                                    "PosCode": "5 - 50480 - 211",
                                    "PosName": "OC Väst - Carlanderska - Hudkliniken",
                                    "PosNameWithCode": "OC Väst (5) - Carlanderska (50480) - Hudkliniken (211)",
                                    "PosId4": 748,
                                    "PosLevel": 3,
                                    "UnitCode": "211",
                                    "TopPosId": 2,
                                    "TopPosCode": "5"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):5 - 50480 - 301,Organisationsenhet(namn och kod):OC Väst - Carlanderska - Kirurgen",
                                "id": 175,
                                "data": {
                                    "PosId": 175,
                                    "PosCode": "5 - 50480 - 301",
                                    "PosName": "OC Väst - Carlanderska - Kirurgen",
                                    "PosNameWithCode": "OC Väst (5) - Carlanderska (50480) - Kirurgen (301)",
                                    "PosId4": 175,
                                    "PosLevel": 3,
                                    "UnitCode": "301",
                                    "TopPosId": 2,
                                    "TopPosCode": "5"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):5 - 50480 - 451,Organisationsenhet(namn och kod):OC Väst - Carlanderska - Kvinnoklinik",
                                "id": 1147,
                                "data": {
                                    "PosId": 1147,
                                    "PosCode": "5 - 50480 - 451",
                                    "PosName": "OC Väst - Carlanderska - Kvinnoklinik",
                                    "PosNameWithCode": "OC Väst (5) - Carlanderska (50480) - Kvinnoklinik (451)",
                                    "PosId4": 1147,
                                    "PosLevel": 3,
                                    "UnitCode": "451",
                                    "TopPosId": 2,
                                    "TopPosCode": "5"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):5 - 50480 - 361,Organisationsenhet(namn och kod):OC Väst - Carlanderska - Urologen",
                                "id": 606,
                                "data": {
                                    "PosId": 606,
                                    "PosCode": "5 - 50480 - 361",
                                    "PosName": "OC Väst - Carlanderska - Urologen",
                                    "PosNameWithCode": "OC Väst (5) - Carlanderska (50480) - Urologen (361)",
                                    "PosId4": 606,
                                    "PosLevel": 3,
                                    "UnitCode": "361",
                                    "TopPosId": 2,
                                    "TopPosCode": "5"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):5 - 50480 - 521,Organisationsenhet(namn och kod):OC Väst - Carlanderska - ÖNH",
                                "id": 1161,
                                "data": {
                                    "PosId": 1161,
                                    "PosCode": "5 - 50480 - 521",
                                    "PosName": "OC Väst - Carlanderska - ÖNH",
                                    "PosNameWithCode": "OC Väst (5) - Carlanderska (50480) - ÖNH (521)",
                                    "PosId4": 1161,
                                    "PosLevel": 3,
                                    "UnitCode": "521",
                                    "TopPosId": 2,
                                    "TopPosCode": "5"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):5 - 500007 - 361,Organisationsenhet(namn och kod):OC Väst - Christer Dahlstrand - Urologmottagningen",
                                "id": 1400,
                                "data": {
                                    "PosId": 1400,
                                    "PosCode": "5 - 500007 - 361",
                                    "PosName": "OC Väst - Christer Dahlstrand - Urologmottagningen",
                                    "PosNameWithCode": "OC Väst (5) - Christer Dahlstrand (500007) - Urologmottagningen (361)",
                                    "PosId4": 1400,
                                    "PosLevel": 3,
                                    "UnitCode": "361",
                                    "TopPosId": 2,
                                    "TopPosCode": "5"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):5 - 52014 - 521,Organisationsenhet(namn och kod):OC Väst - Dalslands sjukhus - ÖNH",
                                "id": 1196,
                                "data": {
                                    "PosId": 1196,
                                    "PosCode": "5 - 52014 - 521",
                                    "PosName": "OC Väst - Dalslands sjukhus - ÖNH",
                                    "PosNameWithCode": "OC Väst (5) - Dalslands sjukhus (52014) - ÖNH (521)",
                                    "PosId4": 1196,
                                    "PosLevel": 3,
                                    "UnitCode": "521",
                                    "TopPosId": 2,
                                    "TopPosCode": "5"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):5 - 59999 - 999,Organisationsenhet(namn och kod):OC Väst - Enhet utan INCA-inrapportör - Okänd klinik",
                                "id": 1188,
                                "data": {
                                    "PosId": 1188,
                                    "PosCode": "5 - 59999 - 999",
                                    "PosName": "OC Väst - Enhet utan INCA-inrapportör - Okänd klinik",
                                    "PosNameWithCode": "OC Väst (5) - Enhet utan INCA-inrapportör (59999) - Okänd klinik (999)",
                                    "PosId4": 1188,
                                    "PosLevel": 3,
                                    "UnitCode": "999",
                                    "TopPosId": 2,
                                    "TopPosCode": "5"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):5 - 53010 - 301,Organisationsenhet(namn och kod):OC Väst - Falköping - Kirurgen",
                                "id": 165,
                                "data": {
                                    "PosId": 165,
                                    "PosCode": "5 - 53010 - 301",
                                    "PosName": "OC Väst - Falköping - Kirurgen",
                                    "PosNameWithCode": "OC Väst (5) - Falköping (53010) - Kirurgen (301)",
                                    "PosId4": 165,
                                    "PosLevel": 3,
                                    "UnitCode": "301",
                                    "TopPosId": 2,
                                    "TopPosCode": "5"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):5 - 53010 - 731,Organisationsenhet(namn och kod):OC Väst - Falköping - Röntgen",
                                "id": 1518,
                                "data": {
                                    "PosId": 1518,
                                    "PosCode": "5 - 53010 - 731",
                                    "PosName": "OC Väst - Falköping - Röntgen",
                                    "PosNameWithCode": "OC Väst (5) - Falköping (53010) - Röntgen (731)",
                                    "PosId4": 1518,
                                    "PosLevel": 3,
                                    "UnitCode": "731",
                                    "TopPosId": 2,
                                    "TopPosCode": "5"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):5 - 510040 - 731,Organisationsenhet(namn och kod):OC Väst - Haga Röntgen Unilabs - Röntgen",
                                "id": 1535,
                                "data": {
                                    "PosId": 1535,
                                    "PosCode": "5 - 510040 - 731",
                                    "PosName": "OC Väst - Haga Röntgen Unilabs - Röntgen",
                                    "PosNameWithCode": "OC Väst (5) - Haga Röntgen Unilabs (510040) - Röntgen (731)",
                                    "PosId4": 1535,
                                    "PosLevel": 3,
                                    "UnitCode": "731",
                                    "TopPosId": 2,
                                    "TopPosCode": "5"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):5 - 500003 - 211,Organisationsenhet(namn och kod):OC Väst - Hagakliniken - Hudkliniken",
                                "id": 969,
                                "data": {
                                    "PosId": 969,
                                    "PosCode": "5 - 500003 - 211",
                                    "PosName": "OC Väst - Hagakliniken - Hudkliniken",
                                    "PosNameWithCode": "OC Väst (5) - Hagakliniken (500003) - Hudkliniken (211)",
                                    "PosId4": 969,
                                    "PosLevel": 3,
                                    "UnitCode": "211",
                                    "TopPosId": 2,
                                    "TopPosCode": "5"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):5 - 510039 - 731,Organisationsenhet(namn och kod):OC Väst - Hisingen Röntgen - Röntgen",
                                "id": 1539,
                                "data": {
                                    "PosId": 1539,
                                    "PosCode": "5 - 510039 - 731",
                                    "PosName": "OC Väst - Hisingen Röntgen - Röntgen",
                                    "PosNameWithCode": "OC Väst (5) - Hisingen Röntgen (510039) - Röntgen (731)",
                                    "PosId4": 1539,
                                    "PosLevel": 3,
                                    "UnitCode": "731",
                                    "TopPosId": 2,
                                    "TopPosCode": "5"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):5 - 511039 - 451,Organisationsenhet(namn och kod):OC Väst - Krokslätts VC - Gyn",
                                "id": 1419,
                                "data": {
                                    "PosId": 1419,
                                    "PosCode": "5 - 511039 - 451",
                                    "PosName": "OC Väst - Krokslätts VC - Gyn",
                                    "PosNameWithCode": "OC Väst (5) - Krokslätts VC (511039) - Gyn (451)",
                                    "PosId4": 1419,
                                    "PosLevel": 3,
                                    "UnitCode": "451",
                                    "TopPosId": 2,
                                    "TopPosCode": "5"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):5 - 421021 - 211,Organisationsenhet(namn och kod):OC Väst - Kungsbacka - Hudkliniken",
                                "id": 845,
                                "data": {
                                    "PosId": 845,
                                    "PosCode": "5 - 421021 - 211",
                                    "PosName": "OC Väst - Kungsbacka - Hudkliniken",
                                    "PosNameWithCode": "OC Väst (5) - Kungsbacka (421021) - Hudkliniken (211)",
                                    "PosId4": 845,
                                    "PosLevel": 3,
                                    "UnitCode": "211",
                                    "TopPosId": 2,
                                    "TopPosCode": "5"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):5 - 421021 - 301,Organisationsenhet(namn och kod):OC Väst - Kungsbacka - Kirurgen",
                                "id": 172,
                                "data": {
                                    "PosId": 172,
                                    "PosCode": "5 - 421021 - 301",
                                    "PosName": "OC Väst - Kungsbacka - Kirurgen",
                                    "PosNameWithCode": "OC Väst (5) - Kungsbacka (421021) - Kirurgen (301)",
                                    "PosId4": 172,
                                    "PosLevel": 3,
                                    "UnitCode": "301",
                                    "TopPosId": 2,
                                    "TopPosCode": "5"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):5 - 421021 - 451,Organisationsenhet(namn och kod):OC Väst - Kungsbacka - Kvinnoklinik",
                                "id": 881,
                                "data": {
                                    "PosId": 881,
                                    "PosCode": "5 - 421021 - 451",
                                    "PosName": "OC Väst - Kungsbacka - Kvinnoklinik",
                                    "PosNameWithCode": "OC Väst (5) - Kungsbacka (421021) - Kvinnoklinik (451)",
                                    "PosId4": 881,
                                    "PosLevel": 3,
                                    "UnitCode": "451",
                                    "TopPosId": 2,
                                    "TopPosCode": "5"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):5 - 421021 - 101,Organisationsenhet(namn och kod):OC Väst - Kungsbacka - Medicinkliniken",
                                "id": 431,
                                "data": {
                                    "PosId": 431,
                                    "PosCode": "5 - 421021 - 101",
                                    "PosName": "OC Väst - Kungsbacka - Medicinkliniken",
                                    "PosNameWithCode": "OC Väst (5) - Kungsbacka (421021) - Medicinkliniken (101)",
                                    "PosId4": 431,
                                    "PosLevel": 3,
                                    "UnitCode": "101",
                                    "TopPosId": 2,
                                    "TopPosCode": "5"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):5 - 421021 - 731,Organisationsenhet(namn och kod):OC Väst - Kungsbacka - Röntgen",
                                "id": 1516,
                                "data": {
                                    "PosId": 1516,
                                    "PosCode": "5 - 421021 - 731",
                                    "PosName": "OC Väst - Kungsbacka - Röntgen",
                                    "PosNameWithCode": "OC Väst (5) - Kungsbacka (421021) - Röntgen (731)",
                                    "PosId4": 1516,
                                    "PosLevel": 3,
                                    "UnitCode": "731",
                                    "TopPosId": 2,
                                    "TopPosCode": "5"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):5 - 421021 - 521,Organisationsenhet(namn och kod):OC Väst - Kungsbacka - ÖNH",
                                "id": 722,
                                "data": {
                                    "PosId": 722,
                                    "PosCode": "5 - 421021 - 521",
                                    "PosName": "OC Väst - Kungsbacka - ÖNH",
                                    "PosNameWithCode": "OC Väst (5) - Kungsbacka (421021) - ÖNH (521)",
                                    "PosId4": 722,
                                    "PosLevel": 3,
                                    "UnitCode": "521",
                                    "TopPosId": 2,
                                    "TopPosCode": "5"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):5 - 51012 - 240,Organisationsenhet(namn och kod):OC Väst - Kungälv - Geriatrik & Rehabklin",
                                "id": 1256,
                                "data": {
                                    "PosId": 1256,
                                    "PosCode": "5 - 51012 - 240",
                                    "PosName": "OC Väst - Kungälv - Geriatrik & Rehabklin",
                                    "PosNameWithCode": "OC Väst (5) - Kungälv (51012) - Geriatrik & Rehabklin (240)",
                                    "PosId4": 1256,
                                    "PosLevel": 3,
                                    "UnitCode": "240",
                                    "TopPosId": 2,
                                    "TopPosCode": "5"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):5 - 51012 - 301,Organisationsenhet(namn och kod):OC Väst - Kungälv - Kirurgen",
                                "id": 162,
                                "data": {
                                    "PosId": 162,
                                    "PosCode": "5 - 51012 - 301",
                                    "PosName": "OC Väst - Kungälv - Kirurgen",
                                    "PosNameWithCode": "OC Väst (5) - Kungälv (51012) - Kirurgen (301)",
                                    "PosId4": 162,
                                    "PosLevel": 3,
                                    "UnitCode": "301",
                                    "TopPosId": 2,
                                    "TopPosCode": "5"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):5 - 51012 - 451,Organisationsenhet(namn och kod):OC Väst - Kungälv - Kvinnoklinik",
                                "id": 639,
                                "data": {
                                    "PosId": 639,
                                    "PosCode": "5 - 51012 - 451",
                                    "PosName": "OC Väst - Kungälv - Kvinnoklinik",
                                    "PosNameWithCode": "OC Väst (5) - Kungälv (51012) - Kvinnoklinik (451)",
                                    "PosId4": 639,
                                    "PosLevel": 3,
                                    "UnitCode": "451",
                                    "TopPosId": 2,
                                    "TopPosCode": "5"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):5 - 51012 - 101,Organisationsenhet(namn och kod):OC Väst - Kungälv - Medicinkliniken",
                                "id": 337,
                                "data": {
                                    "PosId": 337,
                                    "PosCode": "5 - 51012 - 101",
                                    "PosName": "OC Väst - Kungälv - Medicinkliniken",
                                    "PosNameWithCode": "OC Väst (5) - Kungälv (51012) - Medicinkliniken (101)",
                                    "PosId4": 337,
                                    "PosLevel": 3,
                                    "UnitCode": "101",
                                    "TopPosId": 2,
                                    "TopPosCode": "5"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):5 - 51012 - 731,Organisationsenhet(namn och kod):OC Väst - Kungälv - Röntgen",
                                "id": 1523,
                                "data": {
                                    "PosId": 1523,
                                    "PosCode": "5 - 51012 - 731",
                                    "PosName": "OC Väst - Kungälv - Röntgen",
                                    "PosNameWithCode": "OC Väst (5) - Kungälv (51012) - Röntgen (731)",
                                    "PosId4": 1523,
                                    "PosLevel": 3,
                                    "UnitCode": "731",
                                    "TopPosId": 2,
                                    "TopPosCode": "5"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):5 - 53011 - 211,Organisationsenhet(namn och kod):OC Väst - Lidköping - Hudkliniken",
                                "id": 843,
                                "data": {
                                    "PosId": 843,
                                    "PosCode": "5 - 53011 - 211",
                                    "PosName": "OC Väst - Lidköping - Hudkliniken",
                                    "PosNameWithCode": "OC Väst (5) - Lidköping (53011) - Hudkliniken (211)",
                                    "PosId4": 843,
                                    "PosLevel": 3,
                                    "UnitCode": "211",
                                    "TopPosId": 2,
                                    "TopPosCode": "5"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):5 - 53011 - 301,Organisationsenhet(namn och kod):OC Väst - Lidköping - Kirurgen",
                                "id": 169,
                                "data": {
                                    "PosId": 169,
                                    "PosCode": "5 - 53011 - 301",
                                    "PosName": "OC Väst - Lidköping - Kirurgen",
                                    "PosNameWithCode": "OC Väst (5) - Lidköping (53011) - Kirurgen (301)",
                                    "PosId4": 169,
                                    "PosLevel": 3,
                                    "UnitCode": "301",
                                    "TopPosId": 2,
                                    "TopPosCode": "5"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):5 - 53011 - 451,Organisationsenhet(namn och kod):OC Väst - Lidköping - Kvinnoklinik",
                                "id": 646,
                                "data": {
                                    "PosId": 646,
                                    "PosCode": "5 - 53011 - 451",
                                    "PosName": "OC Väst - Lidköping - Kvinnoklinik",
                                    "PosNameWithCode": "OC Väst (5) - Lidköping (53011) - Kvinnoklinik (451)",
                                    "PosId4": 646,
                                    "PosLevel": 3,
                                    "UnitCode": "451",
                                    "TopPosId": 2,
                                    "TopPosCode": "5"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):5 - 53011 - 101,Organisationsenhet(namn och kod):OC Väst - Lidköping - Medicinkliniken",
                                "id": 325,
                                "data": {
                                    "PosId": 325,
                                    "PosCode": "5 - 53011 - 101",
                                    "PosName": "OC Väst - Lidköping - Medicinkliniken",
                                    "PosNameWithCode": "OC Väst (5) - Lidköping (53011) - Medicinkliniken (101)",
                                    "PosId4": 325,
                                    "PosLevel": 3,
                                    "UnitCode": "101",
                                    "TopPosId": 2,
                                    "TopPosCode": "5"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):5 - 50070 - 301,Organisationsenhet(namn och kod):OC Väst - Lundby - Kirurgen",
                                "id": 173,
                                "data": {
                                    "PosId": 173,
                                    "PosCode": "5 - 50070 - 301",
                                    "PosName": "OC Väst - Lundby - Kirurgen",
                                    "PosNameWithCode": "OC Väst (5) - Lundby (50070) - Kirurgen (301)",
                                    "PosId4": 173,
                                    "PosLevel": 3,
                                    "UnitCode": "301",
                                    "TopPosId": 2,
                                    "TopPosCode": "5"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):5 - 50070 - 361,Organisationsenhet(namn och kod):OC Väst - Lundby - Urologen",
                                "id": 429,
                                "data": {
                                    "PosId": 429,
                                    "PosCode": "5 - 50070 - 361",
                                    "PosName": "OC Väst - Lundby - Urologen",
                                    "PosNameWithCode": "OC Väst (5) - Lundby (50070) - Urologen (361)",
                                    "PosId4": 429,
                                    "PosLevel": 3,
                                    "UnitCode": "361",
                                    "TopPosId": 2,
                                    "TopPosCode": "5"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):5 - 50070 - 521,Organisationsenhet(namn och kod):OC Väst - Lundby - ÖNH",
                                "id": 977,
                                "data": {
                                    "PosId": 977,
                                    "PosCode": "5 - 50070 - 521",
                                    "PosName": "OC Väst - Lundby - ÖNH",
                                    "PosNameWithCode": "OC Väst (5) - Lundby (50070) - ÖNH (521)",
                                    "PosId4": 977,
                                    "PosLevel": 3,
                                    "UnitCode": "521",
                                    "TopPosId": 2,
                                    "TopPosCode": "5"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):5 - 51031 - 101,Organisationsenhet(namn och kod):OC Väst - Lysekil - Medicinkliniken",
                                "id": 339,
                                "data": {
                                    "PosId": 339,
                                    "PosCode": "5 - 51031 - 101",
                                    "PosName": "OC Väst - Lysekil - Medicinkliniken",
                                    "PosNameWithCode": "OC Väst (5) - Lysekil (51031) - Medicinkliniken (101)",
                                    "PosId4": 339,
                                    "PosLevel": 3,
                                    "UnitCode": "101",
                                    "TopPosId": 2,
                                    "TopPosCode": "5"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):5 - 500001 - 211,Organisationsenhet(namn och kod):OC Väst - Läkarhuset i Göteborg - Hudkliniken",
                                "id": 847,
                                "data": {
                                    "PosId": 847,
                                    "PosCode": "5 - 500001 - 211",
                                    "PosName": "OC Väst - Läkarhuset i Göteborg - Hudkliniken",
                                    "PosNameWithCode": "OC Väst (5) - Läkarhuset i Göteborg (500001) - Hudkliniken (211)",
                                    "PosId4": 847,
                                    "PosLevel": 3,
                                    "UnitCode": "211",
                                    "TopPosId": 2,
                                    "TopPosCode": "5"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):5 - 500001 - 301,Organisationsenhet(namn och kod):OC Väst - Läkarhuset i Göteborg - Kirurgi",
                                "id": 506,
                                "data": {
                                    "PosId": 506,
                                    "PosCode": "5 - 500001 - 301",
                                    "PosName": "OC Väst - Läkarhuset i Göteborg - Kirurgi",
                                    "PosNameWithCode": "OC Väst (5) - Läkarhuset i Göteborg (500001) - Kirurgi (301)",
                                    "PosId4": 506,
                                    "PosLevel": 3,
                                    "UnitCode": "301",
                                    "TopPosId": 2,
                                    "TopPosCode": "5"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):5 - 53012 - 211,Organisationsenhet(namn och kod):OC Väst - Mariestad - Hudkliniken",
                                "id": 916,
                                "data": {
                                    "PosId": 916,
                                    "PosCode": "5 - 53012 - 211",
                                    "PosName": "OC Väst - Mariestad - Hudkliniken",
                                    "PosNameWithCode": "OC Väst (5) - Mariestad (53012) - Hudkliniken (211)",
                                    "PosId4": 916,
                                    "PosLevel": 3,
                                    "UnitCode": "211",
                                    "TopPosId": 2,
                                    "TopPosCode": "5"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):5 - 53012 - 301,Organisationsenhet(namn och kod):OC Väst - Mariestad - Kirurgen",
                                "id": 170,
                                "data": {
                                    "PosId": 170,
                                    "PosCode": "5 - 53012 - 301",
                                    "PosName": "OC Väst - Mariestad - Kirurgen",
                                    "PosNameWithCode": "OC Väst (5) - Mariestad (53012) - Kirurgen (301)",
                                    "PosId4": 170,
                                    "PosLevel": 3,
                                    "UnitCode": "301",
                                    "TopPosId": 2,
                                    "TopPosCode": "5"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):5 - 53012 - 451,Organisationsenhet(namn och kod):OC Väst - Mariestad - Kvinnoklinik",
                                "id": 1146,
                                "data": {
                                    "PosId": 1146,
                                    "PosCode": "5 - 53012 - 451",
                                    "PosName": "OC Väst - Mariestad - Kvinnoklinik",
                                    "PosNameWithCode": "OC Väst (5) - Mariestad (53012) - Kvinnoklinik (451)",
                                    "PosId4": 1146,
                                    "PosLevel": 3,
                                    "UnitCode": "451",
                                    "TopPosId": 2,
                                    "TopPosCode": "5"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):5 - 52017 - 211,Organisationsenhet(namn och kod):OC Väst - NÄL - Hudkliniken",
                                "id": 842,
                                "data": {
                                    "PosId": 842,
                                    "PosCode": "5 - 52017 - 211",
                                    "PosName": "OC Väst - NÄL - Hudkliniken",
                                    "PosNameWithCode": "OC Väst (5) - NÄL (52017) - Hudkliniken (211)",
                                    "PosId4": 842,
                                    "PosLevel": 3,
                                    "UnitCode": "211",
                                    "TopPosId": 2,
                                    "TopPosCode": "5"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):5 - 52017 - 301,Organisationsenhet(namn och kod):OC Väst - NÄL - Kirurgen",
                                "id": 164,
                                "data": {
                                    "PosId": 164,
                                    "PosCode": "5 - 52017 - 301",
                                    "PosName": "OC Väst - NÄL - Kirurgen",
                                    "PosNameWithCode": "OC Väst (5) - NÄL (52017) - Kirurgen (301)",
                                    "PosId4": 164,
                                    "PosLevel": 3,
                                    "UnitCode": "301",
                                    "TopPosId": 2,
                                    "TopPosCode": "5"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):5 - 52017 - 451,Organisationsenhet(namn och kod):OC Väst - NÄL - Kvinnoklinik",
                                "id": 641,
                                "data": {
                                    "PosId": 641,
                                    "PosCode": "5 - 52017 - 451",
                                    "PosName": "OC Väst - NÄL - Kvinnoklinik",
                                    "PosNameWithCode": "OC Väst (5) - NÄL (52017) - Kvinnoklinik (451)",
                                    "PosId4": 641,
                                    "PosLevel": 3,
                                    "UnitCode": "451",
                                    "TopPosId": 2,
                                    "TopPosCode": "5"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):5 - 52017 - 111,Organisationsenhet(namn och kod):OC Väst - NÄL - Lungmedicin",
                                "id": 690,
                                "data": {
                                    "PosId": 690,
                                    "PosCode": "5 - 52017 - 111",
                                    "PosName": "OC Väst - NÄL - Lungmedicin",
                                    "PosNameWithCode": "OC Väst (5) - NÄL (52017) - Lungmedicin (111)",
                                    "PosId4": 690,
                                    "PosLevel": 3,
                                    "UnitCode": "111",
                                    "TopPosId": 2,
                                    "TopPosCode": "5"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):5 - 52017 - 101,Organisationsenhet(namn och kod):OC Väst - NÄL - Medicinkliniken",
                                "id": 1554,
                                "data": {
                                    "PosId": 1554,
                                    "PosCode": "5 - 52017 - 101",
                                    "PosName": "OC Väst - NÄL - Medicinkliniken",
                                    "PosNameWithCode": "OC Väst (5) - NÄL (52017) - Medicinkliniken (101)",
                                    "PosId4": 1554,
                                    "PosLevel": 3,
                                    "UnitCode": "101",
                                    "TopPosId": 2,
                                    "TopPosCode": "5"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):5 - 52017 - 221,Organisationsenhet(namn och kod):OC Väst - NÄL - Neurologiska kliniken",
                                "id": 1015,
                                "data": {
                                    "PosId": 1015,
                                    "PosCode": "5 - 52017 - 221",
                                    "PosName": "OC Väst - NÄL - Neurologiska kliniken",
                                    "PosNameWithCode": "OC Väst (5) - NÄL (52017) - Neurologiska kliniken (221)",
                                    "PosId4": 1015,
                                    "PosLevel": 3,
                                    "UnitCode": "221",
                                    "TopPosId": 2,
                                    "TopPosCode": "5"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):5 - 52017 - 731,Organisationsenhet(namn och kod):OC Väst - NÄL - Röntgen",
                                "id": 1528,
                                "data": {
                                    "PosId": 1528,
                                    "PosCode": "5 - 52017 - 731",
                                    "PosName": "OC Väst - NÄL - Röntgen",
                                    "PosNameWithCode": "OC Väst (5) - NÄL (52017) - Röntgen (731)",
                                    "PosId4": 1528,
                                    "PosLevel": 3,
                                    "UnitCode": "731",
                                    "TopPosId": 2,
                                    "TopPosCode": "5"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):5 - 52017 - 521,Organisationsenhet(namn och kod):OC Väst - NÄL - ÖNH",
                                "id": 304,
                                "data": {
                                    "PosId": 304,
                                    "PosCode": "5 - 52017 - 521",
                                    "PosName": "OC Väst - NÄL - ÖNH",
                                    "PosNameWithCode": "OC Väst (5) - NÄL (52017) - ÖNH (521)",
                                    "PosId4": 304,
                                    "PosLevel": 3,
                                    "UnitCode": "521",
                                    "TopPosId": 2,
                                    "TopPosCode": "5"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):5 - 511999 - 361,Organisationsenhet(namn och kod):OC Väst - OLSAB klinik 107 Lysekil - Urologi",
                                "id": 1204,
                                "data": {
                                    "PosId": 1204,
                                    "PosCode": "5 - 511999 - 361",
                                    "PosName": "OC Väst - OLSAB klinik 107 Lysekil - Urologi",
                                    "PosNameWithCode": "OC Väst (5) - OLSAB klinik 107 Lysekil (511999) - Urologi (361)",
                                    "PosId4": 1204,
                                    "PosLevel": 3,
                                    "UnitCode": "361",
                                    "TopPosId": 2,
                                    "TopPosCode": "5"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):5 - 52013 - 301,Organisationsenhet(namn och kod):OC Väst - Skene - Kirurgen",
                                "id": 168,
                                "data": {
                                    "PosId": 168,
                                    "PosCode": "5 - 52013 - 301",
                                    "PosName": "OC Väst - Skene - Kirurgen",
                                    "PosNameWithCode": "OC Väst (5) - Skene (52013) - Kirurgen (301)",
                                    "PosId4": 168,
                                    "PosLevel": 3,
                                    "UnitCode": "301",
                                    "TopPosId": 2,
                                    "TopPosCode": "5"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):5 - 52013 - 101,Organisationsenhet(namn och kod):OC Väst - Skene - Medicinkliniken",
                                "id": 333,
                                "data": {
                                    "PosId": 333,
                                    "PosCode": "5 - 52013 - 101",
                                    "PosName": "OC Väst - Skene - Medicinkliniken",
                                    "PosNameWithCode": "OC Väst (5) - Skene (52013) - Medicinkliniken (101)",
                                    "PosId4": 333,
                                    "PosLevel": 3,
                                    "UnitCode": "101",
                                    "TopPosId": 2,
                                    "TopPosCode": "5"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):5 - 52013 - 731,Organisationsenhet(namn och kod):OC Väst - Skene - Röntgen",
                                "id": 1522,
                                "data": {
                                    "PosId": 1522,
                                    "PosCode": "5 - 52013 - 731",
                                    "PosName": "OC Väst - Skene - Röntgen",
                                    "PosNameWithCode": "OC Väst (5) - Skene (52013) - Röntgen (731)",
                                    "PosId4": 1522,
                                    "PosLevel": 3,
                                    "UnitCode": "731",
                                    "TopPosId": 2,
                                    "TopPosCode": "5"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):5 - 53013 - 211,Organisationsenhet(namn och kod):OC Väst - Skövde - Hudkliniken",
                                "id": 432,
                                "data": {
                                    "PosId": 432,
                                    "PosCode": "5 - 53013 - 211",
                                    "PosName": "OC Väst - Skövde - Hudkliniken",
                                    "PosNameWithCode": "OC Väst (5) - Skövde (53013) - Hudkliniken (211)",
                                    "PosId4": 432,
                                    "PosLevel": 3,
                                    "UnitCode": "211",
                                    "TopPosId": 2,
                                    "TopPosCode": "5"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):5 - 53013 - 301,Organisationsenhet(namn och kod):OC Väst - Skövde - Kirurgen",
                                "id": 16,
                                "data": {
                                    "PosId": 16,
                                    "PosCode": "5 - 53013 - 301",
                                    "PosName": "OC Väst - Skövde - Kirurgen",
                                    "PosNameWithCode": "OC Väst (5) - Skövde (53013) - Kirurgen (301)",
                                    "PosId4": 16,
                                    "PosLevel": 3,
                                    "UnitCode": "301",
                                    "TopPosId": 2,
                                    "TopPosCode": "5"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):5 - 53013 - 451,Organisationsenhet(namn och kod):OC Väst - Skövde - Kvinnoklinik",
                                "id": 636,
                                "data": {
                                    "PosId": 636,
                                    "PosCode": "5 - 53013 - 451",
                                    "PosName": "OC Väst - Skövde - Kvinnoklinik",
                                    "PosNameWithCode": "OC Väst (5) - Skövde (53013) - Kvinnoklinik (451)",
                                    "PosId4": 636,
                                    "PosLevel": 3,
                                    "UnitCode": "451",
                                    "TopPosId": 2,
                                    "TopPosCode": "5"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):5 - 53013 - 111,Organisationsenhet(namn och kod):OC Väst - Skövde - Lungmedicin",
                                "id": 691,
                                "data": {
                                    "PosId": 691,
                                    "PosCode": "5 - 53013 - 111",
                                    "PosName": "OC Väst - Skövde - Lungmedicin",
                                    "PosNameWithCode": "OC Väst (5) - Skövde (53013) - Lungmedicin (111)",
                                    "PosId4": 691,
                                    "PosLevel": 3,
                                    "UnitCode": "111",
                                    "TopPosId": 2,
                                    "TopPosCode": "5"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):5 - 53013 - 101,Organisationsenhet(namn och kod):OC Väst - Skövde - Medicinkliniken",
                                "id": 15,
                                "data": {
                                    "PosId": 15,
                                    "PosCode": "5 - 53013 - 101",
                                    "PosName": "OC Väst - Skövde - Medicinkliniken",
                                    "PosNameWithCode": "OC Väst (5) - Skövde (53013) - Medicinkliniken (101)",
                                    "PosId4": 15,
                                    "PosLevel": 3,
                                    "UnitCode": "101",
                                    "TopPosId": 2,
                                    "TopPosCode": "5"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):5 - 53013 - 221,Organisationsenhet(namn och kod):OC Väst - Skövde - Neurologiska kliniken",
                                "id": 1014,
                                "data": {
                                    "PosId": 1014,
                                    "PosCode": "5 - 53013 - 221",
                                    "PosName": "OC Väst - Skövde - Neurologiska kliniken",
                                    "PosNameWithCode": "OC Väst (5) - Skövde (53013) - Neurologiska kliniken (221)",
                                    "PosId4": 1014,
                                    "PosLevel": 3,
                                    "UnitCode": "221",
                                    "TopPosId": 2,
                                    "TopPosCode": "5"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):5 - 53013 - 731,Organisationsenhet(namn och kod):OC Väst - Skövde - Röntgen",
                                "id": 1527,
                                "data": {
                                    "PosId": 1527,
                                    "PosCode": "5 - 53013 - 731",
                                    "PosName": "OC Väst - Skövde - Röntgen",
                                    "PosNameWithCode": "OC Väst (5) - Skövde (53013) - Röntgen (731)",
                                    "PosId4": 1527,
                                    "PosLevel": 3,
                                    "UnitCode": "731",
                                    "TopPosId": 2,
                                    "TopPosCode": "5"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):5 - 53013 - 361,Organisationsenhet(namn och kod):OC Väst - Skövde - Urologen",
                                "id": 377,
                                "data": {
                                    "PosId": 377,
                                    "PosCode": "5 - 53013 - 361",
                                    "PosName": "OC Väst - Skövde - Urologen",
                                    "PosNameWithCode": "OC Väst (5) - Skövde (53013) - Urologen (361)",
                                    "PosId4": 377,
                                    "PosLevel": 3,
                                    "UnitCode": "361",
                                    "TopPosId": 2,
                                    "TopPosCode": "5"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):5 - 53013 - 521,Organisationsenhet(namn och kod):OC Väst - Skövde - ÖNH",
                                "id": 17,
                                "data": {
                                    "PosId": 17,
                                    "PosCode": "5 - 53013 - 521",
                                    "PosName": "OC Väst - Skövde - ÖNH",
                                    "PosNameWithCode": "OC Väst (5) - Skövde (53013) - ÖNH (521)",
                                    "PosId4": 17,
                                    "PosLevel": 3,
                                    "UnitCode": "521",
                                    "TopPosId": 2,
                                    "TopPosCode": "5"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):5 - 421001 - 211,Organisationsenhet(namn och kod):OC Väst - Specialistsjukv i Falkenberg - Hudkliniken",
                                "id": 846,
                                "data": {
                                    "PosId": 846,
                                    "PosCode": "5 - 421001 - 211",
                                    "PosName": "OC Väst - Specialistsjukv i Falkenberg - Hudkliniken",
                                    "PosNameWithCode": "OC Väst (5) - Specialistsjukv i Falkenberg (421001) - Hudkliniken (211)",
                                    "PosId4": 846,
                                    "PosLevel": 3,
                                    "UnitCode": "211",
                                    "TopPosId": 2,
                                    "TopPosCode": "5"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):5 - 421001 - 301,Organisationsenhet(namn och kod):OC Väst - Specialistsjukv i Falkenberg - Kirurgi",
                                "id": 508,
                                "data": {
                                    "PosId": 508,
                                    "PosCode": "5 - 421001 - 301",
                                    "PosName": "OC Väst - Specialistsjukv i Falkenberg - Kirurgi",
                                    "PosNameWithCode": "OC Väst (5) - Specialistsjukv i Falkenberg (421001) - Kirurgi (301)",
                                    "PosId4": 508,
                                    "PosLevel": 3,
                                    "UnitCode": "301",
                                    "TopPosId": 2,
                                    "TopPosCode": "5"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):5 - 51011 - 211,Organisationsenhet(namn och kod):OC Väst - SU/Mölndal - Hudkliniken",
                                "id": 746,
                                "data": {
                                    "PosId": 746,
                                    "PosCode": "5 - 51011 - 211",
                                    "PosName": "OC Väst - SU/Mölndal - Hudkliniken",
                                    "PosNameWithCode": "OC Väst (5) - SU/Mölndal (51011) - Hudkliniken (211)",
                                    "PosId4": 746,
                                    "PosLevel": 3,
                                    "UnitCode": "211",
                                    "TopPosId": 2,
                                    "TopPosCode": "5"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):5 - 51011 - 301,Organisationsenhet(namn och kod):OC Väst - SU/Mölndal - Kirurgen",
                                "id": 166,
                                "data": {
                                    "PosId": 166,
                                    "PosCode": "5 - 51011 - 301",
                                    "PosName": "OC Väst - SU/Mölndal - Kirurgen",
                                    "PosNameWithCode": "OC Väst (5) - SU/Mölndal (51011) - Kirurgen (301)",
                                    "PosId4": 166,
                                    "PosLevel": 3,
                                    "UnitCode": "301",
                                    "TopPosId": 2,
                                    "TopPosCode": "5"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):5 - 51011 - 451,Organisationsenhet(namn och kod):OC Väst - SU/Mölndal - Kvinnoklinik",
                                "id": 642,
                                "data": {
                                    "PosId": 642,
                                    "PosCode": "5 - 51011 - 451",
                                    "PosName": "OC Väst - SU/Mölndal - Kvinnoklinik",
                                    "PosNameWithCode": "OC Väst (5) - SU/Mölndal (51011) - Kvinnoklinik (451)",
                                    "PosId4": 642,
                                    "PosLevel": 3,
                                    "UnitCode": "451",
                                    "TopPosId": 2,
                                    "TopPosCode": "5"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):5 - 51011 - 101,Organisationsenhet(namn och kod):OC Väst - SU/Mölndal - Medicinkliniken",
                                "id": 326,
                                "data": {
                                    "PosId": 326,
                                    "PosCode": "5 - 51011 - 101",
                                    "PosName": "OC Väst - SU/Mölndal - Medicinkliniken",
                                    "PosNameWithCode": "OC Väst (5) - SU/Mölndal (51011) - Medicinkliniken (101)",
                                    "PosId4": 326,
                                    "PosLevel": 3,
                                    "UnitCode": "101",
                                    "TopPosId": 2,
                                    "TopPosCode": "5"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):5 - 51011 - 731,Organisationsenhet(namn och kod):OC Väst - SU/Mölndal - Röntgen",
                                "id": 1526,
                                "data": {
                                    "PosId": 1526,
                                    "PosCode": "5 - 51011 - 731",
                                    "PosName": "OC Väst - SU/Mölndal - Röntgen",
                                    "PosNameWithCode": "OC Väst (5) - SU/Mölndal (51011) - Röntgen (731)",
                                    "PosId4": 1526,
                                    "PosLevel": 3,
                                    "UnitCode": "731",
                                    "TopPosId": 2,
                                    "TopPosCode": "5"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):5 - 50001 - 161,Organisationsenhet(namn och kod):OC Väst - SU/Sahlgrenska - Endokrinologen",
                                "id": 1189,
                                "data": {
                                    "PosId": 1189,
                                    "PosCode": "5 - 50001 - 161",
                                    "PosName": "OC Väst - SU/Sahlgrenska - Endokrinologen",
                                    "PosNameWithCode": "OC Väst (5) - SU/Sahlgrenska (50001) - Endokrinologen (161)",
                                    "PosId4": 1189,
                                    "PosLevel": 3,
                                    "UnitCode": "161",
                                    "TopPosId": 2,
                                    "TopPosCode": "5"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):5 - 50001 - 751,Organisationsenhet(namn och kod):OC Väst - SU/Sahlgrenska - Gyn onk klinik",
                                "id": 634,
                                "data": {
                                    "PosId": 634,
                                    "PosCode": "5 - 50001 - 751",
                                    "PosName": "OC Väst - SU/Sahlgrenska - Gyn onk klinik",
                                    "PosNameWithCode": "OC Väst (5) - SU/Sahlgrenska (50001) - Gyn onk klinik (751)",
                                    "PosId4": 634,
                                    "PosLevel": 3,
                                    "UnitCode": "751",
                                    "TopPosId": 2,
                                    "TopPosCode": "5"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):5 - 50001 - 211,Organisationsenhet(namn och kod):OC Väst - SU/Sahlgrenska - Hudkliniken",
                                "id": 336,
                                "data": {
                                    "PosId": 336,
                                    "PosCode": "5 - 50001 - 211",
                                    "PosName": "OC Väst - SU/Sahlgrenska - Hudkliniken",
                                    "PosNameWithCode": "OC Väst (5) - SU/Sahlgrenska (50001) - Hudkliniken (211)",
                                    "PosId4": 336,
                                    "PosLevel": 3,
                                    "UnitCode": "211",
                                    "TopPosId": 2,
                                    "TopPosCode": "5"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):5 - 50001 - 301,Organisationsenhet(namn och kod):OC Väst - SU/Sahlgrenska - Kirurgen",
                                "id": 18,
                                "data": {
                                    "PosId": 18,
                                    "PosCode": "5 - 50001 - 301",
                                    "PosName": "OC Väst - SU/Sahlgrenska - Kirurgen",
                                    "PosNameWithCode": "OC Väst (5) - SU/Sahlgrenska (50001) - Kirurgen (301)",
                                    "PosId4": 18,
                                    "PosLevel": 3,
                                    "UnitCode": "301",
                                    "TopPosId": 2,
                                    "TopPosCode": "5"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):5 - 50001 - 451,Organisationsenhet(namn och kod):OC Väst - SU/Sahlgrenska - Kvinnoklinik",
                                "id": 643,
                                "data": {
                                    "PosId": 643,
                                    "PosCode": "5 - 50001 - 451",
                                    "PosName": "OC Väst - SU/Sahlgrenska - Kvinnoklinik",
                                    "PosNameWithCode": "OC Väst (5) - SU/Sahlgrenska (50001) - Kvinnoklinik (451)",
                                    "PosId4": 643,
                                    "PosLevel": 3,
                                    "UnitCode": "451",
                                    "TopPosId": 2,
                                    "TopPosCode": "5"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):5 - 50001 - 111,Organisationsenhet(namn och kod):OC Väst - SU/Sahlgrenska - Lungmedicin",
                                "id": 625,
                                "data": {
                                    "PosId": 625,
                                    "PosCode": "5 - 50001 - 111",
                                    "PosName": "OC Väst - SU/Sahlgrenska - Lungmedicin",
                                    "PosNameWithCode": "OC Väst (5) - SU/Sahlgrenska (50001) - Lungmedicin (111)",
                                    "PosId4": 625,
                                    "PosLevel": 3,
                                    "UnitCode": "111",
                                    "TopPosId": 2,
                                    "TopPosCode": "5"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):5 - 50001 - 101,Organisationsenhet(namn och kod):OC Väst - SU/Sahlgrenska - Medicinkliniken",
                                "id": 11,
                                "data": {
                                    "PosId": 11,
                                    "PosCode": "5 - 50001 - 101",
                                    "PosName": "OC Väst - SU/Sahlgrenska - Medicinkliniken",
                                    "PosNameWithCode": "OC Väst (5) - SU/Sahlgrenska (50001) - Medicinkliniken (101)",
                                    "PosId4": 11,
                                    "PosLevel": 3,
                                    "UnitCode": "101",
                                    "TopPosId": 2,
                                    "TopPosCode": "5"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):5 - 50001 - 331,Organisationsenhet(namn och kod):OC Väst - SU/Sahlgrenska - Neurokirurgiska kliniken",
                                "id": 896,
                                "data": {
                                    "PosId": 896,
                                    "PosCode": "5 - 50001 - 331",
                                    "PosName": "OC Väst - SU/Sahlgrenska - Neurokirurgiska kliniken",
                                    "PosNameWithCode": "OC Väst (5) - SU/Sahlgrenska (50001) - Neurokirurgiska kliniken (331)",
                                    "PosId4": 896,
                                    "PosLevel": 3,
                                    "UnitCode": "331",
                                    "TopPosId": 2,
                                    "TopPosCode": "5"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):5 - 50001 - 221,Organisationsenhet(namn och kod):OC Väst - SU/Sahlgrenska - Neurologiska kliniken",
                                "id": 1168,
                                "data": {
                                    "PosId": 1168,
                                    "PosCode": "5 - 50001 - 221",
                                    "PosName": "OC Väst - SU/Sahlgrenska - Neurologiska kliniken",
                                    "PosNameWithCode": "OC Väst (5) - SU/Sahlgrenska (50001) - Neurologiska kliniken (221)",
                                    "PosId4": 1168,
                                    "PosLevel": 3,
                                    "UnitCode": "221",
                                    "TopPosId": 2,
                                    "TopPosCode": "5"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):5 - 50001 - 741,Organisationsenhet(namn och kod):OC Väst - SU/Sahlgrenska - Onkologiska kliniken",
                                "id": 21,
                                "data": {
                                    "PosId": 21,
                                    "PosCode": "5 - 50001 - 741",
                                    "PosName": "OC Väst - SU/Sahlgrenska - Onkologiska kliniken",
                                    "PosNameWithCode": "OC Väst (5) - SU/Sahlgrenska (50001) - Onkologiska kliniken (741)",
                                    "PosId4": 21,
                                    "PosLevel": 3,
                                    "UnitCode": "741",
                                    "TopPosId": 2,
                                    "TopPosCode": "5"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):5 - 50001 - 311,Organisationsenhet(namn och kod):OC Väst - SU/Sahlgrenska - Ortopeden",
                                "id": 1395,
                                "data": {
                                    "PosId": 1395,
                                    "PosCode": "5 - 50001 - 311",
                                    "PosName": "OC Väst - SU/Sahlgrenska - Ortopeden",
                                    "PosNameWithCode": "OC Väst (5) - SU/Sahlgrenska (50001) - Ortopeden (311)",
                                    "PosId4": 1395,
                                    "PosLevel": 3,
                                    "UnitCode": "311",
                                    "TopPosId": 2,
                                    "TopPosCode": "5"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):5 - 50001 - 351,Organisationsenhet(namn och kod):OC Väst - SU/Sahlgrenska - Plastikkirurgen",
                                "id": 840,
                                "data": {
                                    "PosId": 840,
                                    "PosCode": "5 - 50001 - 351",
                                    "PosName": "OC Väst - SU/Sahlgrenska - Plastikkirurgen",
                                    "PosNameWithCode": "OC Väst (5) - SU/Sahlgrenska (50001) - Plastikkirurgen (351)",
                                    "PosId4": 840,
                                    "PosLevel": 3,
                                    "UnitCode": "351",
                                    "TopPosId": 2,
                                    "TopPosCode": "5"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):5 - 50001 - 341,Organisationsenhet(namn och kod):OC Väst - SU/Sahlgrenska - Thorax kirurgen",
                                "id": 1551,
                                "data": {
                                    "PosId": 1551,
                                    "PosCode": "5 - 50001 - 341",
                                    "PosName": "OC Väst - SU/Sahlgrenska - Thorax kirurgen",
                                    "PosNameWithCode": "OC Väst (5) - SU/Sahlgrenska (50001) - Thorax kirurgen (341)",
                                    "PosId4": 1551,
                                    "PosLevel": 3,
                                    "UnitCode": "341",
                                    "TopPosId": 2,
                                    "TopPosCode": "5"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):5 - 50001 - 361,Organisationsenhet(namn och kod):OC Väst - SU/Sahlgrenska - Urologen",
                                "id": 141,
                                "data": {
                                    "PosId": 141,
                                    "PosCode": "5 - 50001 - 361",
                                    "PosName": "OC Väst - SU/Sahlgrenska - Urologen",
                                    "PosNameWithCode": "OC Väst (5) - SU/Sahlgrenska (50001) - Urologen (361)",
                                    "PosId4": 141,
                                    "PosLevel": 3,
                                    "UnitCode": "361",
                                    "TopPosId": 2,
                                    "TopPosCode": "5"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):5 - 50001 - 511,Organisationsenhet(namn och kod):OC Väst - SU/Sahlgrenska - Ögonkliniken",
                                "id": 1141,
                                "data": {
                                    "PosId": 1141,
                                    "PosCode": "5 - 50001 - 511",
                                    "PosName": "OC Väst - SU/Sahlgrenska - Ögonkliniken",
                                    "PosNameWithCode": "OC Väst (5) - SU/Sahlgrenska (50001) - Ögonkliniken (511)",
                                    "PosId4": 1141,
                                    "PosLevel": 3,
                                    "UnitCode": "511",
                                    "TopPosId": 2,
                                    "TopPosCode": "5"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):5 - 50001 - 521,Organisationsenhet(namn och kod):OC Väst - SU/Sahlgrenska - ÖNH",
                                "id": 13,
                                "data": {
                                    "PosId": 13,
                                    "PosCode": "5 - 50001 - 521",
                                    "PosName": "OC Väst - SU/Sahlgrenska - ÖNH",
                                    "PosNameWithCode": "OC Väst (5) - SU/Sahlgrenska (50001) - ÖNH (521)",
                                    "PosId4": 13,
                                    "PosLevel": 3,
                                    "UnitCode": "521",
                                    "TopPosId": 2,
                                    "TopPosCode": "5"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):5 - 50010 - 211,Organisationsenhet(namn och kod):OC Väst - SU/Östra - Hudkliniken",
                                "id": 747,
                                "data": {
                                    "PosId": 747,
                                    "PosCode": "5 - 50010 - 211",
                                    "PosName": "OC Väst - SU/Östra - Hudkliniken",
                                    "PosNameWithCode": "OC Väst (5) - SU/Östra (50010) - Hudkliniken (211)",
                                    "PosId4": 747,
                                    "PosLevel": 3,
                                    "UnitCode": "211",
                                    "TopPosId": 2,
                                    "TopPosCode": "5"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):5 - 50010 - 301,Organisationsenhet(namn och kod):OC Väst - SU/Östra - Kirurgen",
                                "id": 163,
                                "data": {
                                    "PosId": 163,
                                    "PosCode": "5 - 50010 - 301",
                                    "PosName": "OC Väst - SU/Östra - Kirurgen",
                                    "PosNameWithCode": "OC Väst (5) - SU/Östra (50010) - Kirurgen (301)",
                                    "PosId4": 163,
                                    "PosLevel": 3,
                                    "UnitCode": "301",
                                    "TopPosId": 2,
                                    "TopPosCode": "5"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):5 - 50010 - 451,Organisationsenhet(namn och kod):OC Väst - SU/Östra - Kvinnoklinik",
                                "id": 640,
                                "data": {
                                    "PosId": 640,
                                    "PosCode": "5 - 50010 - 451",
                                    "PosName": "OC Väst - SU/Östra - Kvinnoklinik",
                                    "PosNameWithCode": "OC Väst (5) - SU/Östra (50010) - Kvinnoklinik (451)",
                                    "PosId4": 640,
                                    "PosLevel": 3,
                                    "UnitCode": "451",
                                    "TopPosId": 2,
                                    "TopPosCode": "5"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):5 - 50010 - 101,Organisationsenhet(namn och kod):OC Väst - SU/Östra - Medicinkliniken",
                                "id": 329,
                                "data": {
                                    "PosId": 329,
                                    "PosCode": "5 - 50010 - 101",
                                    "PosName": "OC Väst - SU/Östra - Medicinkliniken",
                                    "PosNameWithCode": "OC Väst (5) - SU/Östra (50010) - Medicinkliniken (101)",
                                    "PosId4": 329,
                                    "PosLevel": 3,
                                    "UnitCode": "101",
                                    "TopPosId": 2,
                                    "TopPosCode": "5"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):5 - 50010 - 731,Organisationsenhet(namn och kod):OC Väst - SU/Östra - Röntgen",
                                "id": 1525,
                                "data": {
                                    "PosId": 1525,
                                    "PosCode": "5 - 50010 - 731",
                                    "PosName": "OC Väst - SU/Östra - Röntgen",
                                    "PosNameWithCode": "OC Väst (5) - SU/Östra (50010) - Röntgen (731)",
                                    "PosId4": 1525,
                                    "PosLevel": 3,
                                    "UnitCode": "731",
                                    "TopPosId": 2,
                                    "TopPosCode": "5"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):5 - 51010 - 211,Organisationsenhet(namn och kod):OC Väst - Uddevalla - Hudkliniken",
                                "id": 849,
                                "data": {
                                    "PosId": 849,
                                    "PosCode": "5 - 51010 - 211",
                                    "PosName": "OC Väst - Uddevalla - Hudkliniken",
                                    "PosNameWithCode": "OC Väst (5) - Uddevalla (51010) - Hudkliniken (211)",
                                    "PosId4": 849,
                                    "PosLevel": 3,
                                    "UnitCode": "211",
                                    "TopPosId": 2,
                                    "TopPosCode": "5"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):5 - 51010 - 301,Organisationsenhet(namn och kod):OC Väst - Uddevalla - Kirurgen",
                                "id": 167,
                                "data": {
                                    "PosId": 167,
                                    "PosCode": "5 - 51010 - 301",
                                    "PosName": "OC Väst - Uddevalla - Kirurgen",
                                    "PosNameWithCode": "OC Väst (5) - Uddevalla (51010) - Kirurgen (301)",
                                    "PosId4": 167,
                                    "PosLevel": 3,
                                    "UnitCode": "301",
                                    "TopPosId": 2,
                                    "TopPosCode": "5"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):5 - 51010 - 451,Organisationsenhet(namn och kod):OC Väst - Uddevalla - Kvinnoklinik",
                                "id": 645,
                                "data": {
                                    "PosId": 645,
                                    "PosCode": "5 - 51010 - 451",
                                    "PosName": "OC Väst - Uddevalla - Kvinnoklinik",
                                    "PosNameWithCode": "OC Väst (5) - Uddevalla (51010) - Kvinnoklinik (451)",
                                    "PosId4": 645,
                                    "PosLevel": 3,
                                    "UnitCode": "451",
                                    "TopPosId": 2,
                                    "TopPosCode": "5"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):5 - 51010 - 111,Organisationsenhet(namn och kod):OC Väst - Uddevalla - Lungmedicin",
                                "id": 692,
                                "data": {
                                    "PosId": 692,
                                    "PosCode": "5 - 51010 - 111",
                                    "PosName": "OC Väst - Uddevalla - Lungmedicin",
                                    "PosNameWithCode": "OC Väst (5) - Uddevalla (51010) - Lungmedicin (111)",
                                    "PosId4": 692,
                                    "PosLevel": 3,
                                    "UnitCode": "111",
                                    "TopPosId": 2,
                                    "TopPosCode": "5"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):5 - 51010 - 101,Organisationsenhet(namn och kod):OC Väst - Uddevalla - Medicinkliniken",
                                "id": 324,
                                "data": {
                                    "PosId": 324,
                                    "PosCode": "5 - 51010 - 101",
                                    "PosName": "OC Väst - Uddevalla - Medicinkliniken",
                                    "PosNameWithCode": "OC Väst (5) - Uddevalla (51010) - Medicinkliniken (101)",
                                    "PosId4": 324,
                                    "PosLevel": 3,
                                    "UnitCode": "101",
                                    "TopPosId": 2,
                                    "TopPosCode": "5"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):5 - 51010 - 731,Organisationsenhet(namn och kod):OC Väst - Uddevalla - Röntgen",
                                "id": 1529,
                                "data": {
                                    "PosId": 1529,
                                    "PosCode": "5 - 51010 - 731",
                                    "PosName": "OC Väst - Uddevalla - Röntgen",
                                    "PosNameWithCode": "OC Väst (5) - Uddevalla (51010) - Röntgen (731)",
                                    "PosId4": 1529,
                                    "PosLevel": 3,
                                    "UnitCode": "731",
                                    "TopPosId": 2,
                                    "TopPosCode": "5"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):5 - 511026 - 731,Organisationsenhet(namn och kod):OC Väst - Ulricehamn VC - Röntgen",
                                "id": 1537,
                                "data": {
                                    "PosId": 1537,
                                    "PosCode": "5 - 511026 - 731",
                                    "PosName": "OC Väst - Ulricehamn VC - Röntgen",
                                    "PosNameWithCode": "OC Väst (5) - Ulricehamn VC (511026) - Röntgen (731)",
                                    "PosId4": 1537,
                                    "PosLevel": 3,
                                    "UnitCode": "731",
                                    "TopPosId": 2,
                                    "TopPosCode": "5"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):5 - 500005 - 361,Organisationsenhet(namn och kod):OC Väst - Urologix - Urologen",
                                "id": 1198,
                                "data": {
                                    "PosId": 1198,
                                    "PosCode": "5 - 500005 - 361",
                                    "PosName": "OC Väst - Urologix - Urologen",
                                    "PosNameWithCode": "OC Väst (5) - Urologix (500005) - Urologen (361)",
                                    "PosId4": 1198,
                                    "PosLevel": 3,
                                    "UnitCode": "361",
                                    "TopPosId": 2,
                                    "TopPosCode": "5"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):5 - 50071 - 211,Organisationsenhet(namn och kod):OC Väst - V:a Frölunda - Hudkliniken",
                                "id": 745,
                                "data": {
                                    "PosId": 745,
                                    "PosCode": "5 - 50071 - 211",
                                    "PosName": "OC Väst - V:a Frölunda - Hudkliniken",
                                    "PosNameWithCode": "OC Väst (5) - V:a Frölunda (50071) - Hudkliniken (211)",
                                    "PosId4": 745,
                                    "PosLevel": 3,
                                    "UnitCode": "211",
                                    "TopPosId": 2,
                                    "TopPosCode": "5"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):5 - 50071 - 301,Organisationsenhet(namn och kod):OC Väst - V:a Frölunda - Kirurgen",
                                "id": 174,
                                "data": {
                                    "PosId": 174,
                                    "PosCode": "5 - 50071 - 301",
                                    "PosName": "OC Väst - V:a Frölunda - Kirurgen",
                                    "PosNameWithCode": "OC Väst (5) - V:a Frölunda (50071) - Kirurgen (301)",
                                    "PosId4": 174,
                                    "PosLevel": 3,
                                    "UnitCode": "301",
                                    "TopPosId": 2,
                                    "TopPosCode": "5"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):5 - 50071 - 221,Organisationsenhet(namn och kod):OC Väst - V:a Frölunda - Neurologiska kliniken",
                                "id": 1254,
                                "data": {
                                    "PosId": 1254,
                                    "PosCode": "5 - 50071 - 221",
                                    "PosName": "OC Väst - V:a Frölunda - Neurologiska kliniken",
                                    "PosNameWithCode": "OC Väst (5) - V:a Frölunda (50071) - Neurologiska kliniken (221)",
                                    "PosId4": 1254,
                                    "PosLevel": 3,
                                    "UnitCode": "221",
                                    "TopPosId": 2,
                                    "TopPosCode": "5"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):5 - 50071 - 731,Organisationsenhet(namn och kod):OC Väst - V:a Frölunda - Röntgen",
                                "id": 1521,
                                "data": {
                                    "PosId": 1521,
                                    "PosCode": "5 - 50071 - 731",
                                    "PosName": "OC Väst - V:a Frölunda - Röntgen",
                                    "PosNameWithCode": "OC Väst (5) - V:a Frölunda (50071) - Röntgen (731)",
                                    "PosId4": 1521,
                                    "PosLevel": 3,
                                    "UnitCode": "731",
                                    "TopPosId": 2,
                                    "TopPosCode": "5"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):5 - 50071 - 521,Organisationsenhet(namn och kod):OC Väst - V:a Frölunda - ÖNH",
                                "id": 1215,
                                "data": {
                                    "PosId": 1215,
                                    "PosCode": "5 - 50071 - 521",
                                    "PosName": "OC Väst - V:a Frölunda - ÖNH",
                                    "PosNameWithCode": "OC Väst (5) - V:a Frölunda (50071) - ÖNH (521)",
                                    "PosId4": 1215,
                                    "PosLevel": 3,
                                    "UnitCode": "521",
                                    "TopPosId": 2,
                                    "TopPosCode": "5"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):5 - 42011 - 211,Organisationsenhet(namn och kod):OC Väst - Varberg - Hudklinken",
                                "id": 844,
                                "data": {
                                    "PosId": 844,
                                    "PosCode": "5 - 42011 - 211",
                                    "PosName": "OC Väst - Varberg - Hudklinken",
                                    "PosNameWithCode": "OC Väst (5) - Varberg (42011) - Hudklinken (211)",
                                    "PosId4": 844,
                                    "PosLevel": 3,
                                    "UnitCode": "211",
                                    "TopPosId": 2,
                                    "TopPosCode": "5"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):5 - 42011 - 301,Organisationsenhet(namn och kod):OC Väst - Varberg - Kirurgen",
                                "id": 171,
                                "data": {
                                    "PosId": 171,
                                    "PosCode": "5 - 42011 - 301",
                                    "PosName": "OC Väst - Varberg - Kirurgen",
                                    "PosNameWithCode": "OC Väst (5) - Varberg (42011) - Kirurgen (301)",
                                    "PosId4": 171,
                                    "PosLevel": 3,
                                    "UnitCode": "301",
                                    "TopPosId": 2,
                                    "TopPosCode": "5"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):5 - 42011 - 451,Organisationsenhet(namn och kod):OC Väst - Varberg - Kvinnoklinik",
                                "id": 647,
                                "data": {
                                    "PosId": 647,
                                    "PosCode": "5 - 42011 - 451",
                                    "PosName": "OC Väst - Varberg - Kvinnoklinik",
                                    "PosNameWithCode": "OC Väst (5) - Varberg (42011) - Kvinnoklinik (451)",
                                    "PosId4": 647,
                                    "PosLevel": 3,
                                    "UnitCode": "451",
                                    "TopPosId": 2,
                                    "TopPosCode": "5"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):5 - 42011 - 101,Organisationsenhet(namn och kod):OC Väst - Varberg - Medicinkliniken",
                                "id": 340,
                                "data": {
                                    "PosId": 340,
                                    "PosCode": "5 - 42011 - 101",
                                    "PosName": "OC Väst - Varberg - Medicinkliniken",
                                    "PosNameWithCode": "OC Väst (5) - Varberg (42011) - Medicinkliniken (101)",
                                    "PosId4": 340,
                                    "PosLevel": 3,
                                    "UnitCode": "101",
                                    "TopPosId": 2,
                                    "TopPosCode": "5"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):5 - 42011 - 221,Organisationsenhet(namn och kod):OC Väst - Varberg - Neurologiska kliniken",
                                "id": 1255,
                                "data": {
                                    "PosId": 1255,
                                    "PosCode": "5 - 42011 - 221",
                                    "PosName": "OC Väst - Varberg - Neurologiska kliniken",
                                    "PosNameWithCode": "OC Väst (5) - Varberg (42011) - Neurologiska kliniken (221)",
                                    "PosId4": 1255,
                                    "PosLevel": 3,
                                    "UnitCode": "221",
                                    "TopPosId": 2,
                                    "TopPosCode": "5"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):5 - 42011 - 731,Organisationsenhet(namn och kod):OC Väst - Varberg - Röntgen",
                                "id": 1517,
                                "data": {
                                    "PosId": 1517,
                                    "PosCode": "5 - 42011 - 731",
                                    "PosName": "OC Väst - Varberg - Röntgen",
                                    "PosNameWithCode": "OC Väst (5) - Varberg (42011) - Röntgen (731)",
                                    "PosId4": 1517,
                                    "PosLevel": 3,
                                    "UnitCode": "731",
                                    "TopPosId": 2,
                                    "TopPosCode": "5"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):5 - 42011 - 361,Organisationsenhet(namn och kod):OC Väst - Varberg - Urologen",
                                "id": 1406,
                                "data": {
                                    "PosId": 1406,
                                    "PosCode": "5 - 42011 - 361",
                                    "PosName": "OC Väst - Varberg - Urologen",
                                    "PosNameWithCode": "OC Väst (5) - Varberg (42011) - Urologen (361)",
                                    "PosId4": 1406,
                                    "PosLevel": 3,
                                    "UnitCode": "361",
                                    "TopPosId": 2,
                                    "TopPosCode": "5"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):5 - 42011 - 521,Organisationsenhet(namn och kod):OC Väst - Varberg - ÖNH",
                                "id": 732,
                                "data": {
                                    "PosId": 732,
                                    "PosCode": "5 - 42011 - 521",
                                    "PosName": "OC Väst - Varberg - ÖNH",
                                    "PosNameWithCode": "OC Väst (5) - Varberg (42011) - ÖNH (521)",
                                    "PosId4": 732,
                                    "PosLevel": 3,
                                    "UnitCode": "521",
                                    "TopPosId": 2,
                                    "TopPosCode": "5"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):5 - 500009 - 361,Organisationsenhet(namn och kod):OC Väst - Varberg urologimottagning - Urologi",
                                "id": 1404,
                                "data": {
                                    "PosId": 1404,
                                    "PosCode": "5 - 500009 - 361",
                                    "PosName": "OC Väst - Varberg urologimottagning - Urologi",
                                    "PosNameWithCode": "OC Väst (5) - Varberg urologimottagning (500009) - Urologi (361)",
                                    "PosId4": 1404,
                                    "PosLevel": 3,
                                    "UnitCode": "361",
                                    "TopPosId": 2,
                                    "TopPosCode": "5"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):RCC-Sam - RCC-KI - SCR-99999,Organisationsenhet(namn och kod):RCC Studier - SCREESCO - Administration av SCREESCO",
                                "id": 1464,
                                "data": {
                                    "PosId": 1464,
                                    "PosCode": "RCC-Sam - RCC-KI - SCR-99999",
                                    "PosName": "RCC Studier - SCREESCO - Administration av SCREESCO",
                                    "PosNameWithCode": "RCC Studier (RCC-Sam) - SCREESCO (RCC-KI) - Administration av SCREESCO (SCR-99999)",
                                    "PosId4": 1464,
                                    "PosLevel": 3,
                                    "UnitCode": "SCR-99999",
                                    "TopPosId": 1423,
                                    "TopPosCode": "RCC-Sam"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):RCC-Sam - RCC-KI - SCR-12001,Organisationsenhet(namn och kod):RCC Studier - SCREESCO - Akademiska Sjukhuset",
                                "id": 1427,
                                "data": {
                                    "PosId": 1427,
                                    "PosCode": "RCC-Sam - RCC-KI - SCR-12001",
                                    "PosName": "RCC Studier - SCREESCO - Akademiska Sjukhuset",
                                    "PosNameWithCode": "RCC Studier (RCC-Sam) - SCREESCO (RCC-KI) - Akademiska Sjukhuset (SCR-12001)",
                                    "PosId4": 1427,
                                    "PosLevel": 3,
                                    "UnitCode": "SCR-12001",
                                    "TopPosId": 1423,
                                    "TopPosCode": "RCC-Sam"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):RCC-Sam - RCC-KI - SCR-57013,Organisationsenhet(namn och kod):RCC Studier - SCREESCO - Avesta lasarett",
                                "id": 1428,
                                "data": {
                                    "PosId": 1428,
                                    "PosCode": "RCC-Sam - RCC-KI - SCR-57013",
                                    "PosName": "RCC Studier - SCREESCO - Avesta lasarett",
                                    "PosNameWithCode": "RCC Studier (RCC-Sam) - SCREESCO (RCC-KI) - Avesta lasarett (SCR-57013)",
                                    "PosId4": 1428,
                                    "PosLevel": 3,
                                    "UnitCode": "SCR-57013",
                                    "TopPosId": 1423,
                                    "TopPosCode": "RCC-Sam"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):RCC-Sam - RCC-KI - SCR-27010,Organisationsenhet(namn och kod):RCC Studier - SCREESCO - Blekingesjukhuset",
                                "id": 1429,
                                "data": {
                                    "PosId": 1429,
                                    "PosCode": "RCC-Sam - RCC-KI - SCR-27010",
                                    "PosName": "RCC Studier - SCREESCO - Blekingesjukhuset",
                                    "PosNameWithCode": "RCC Studier (RCC-Sam) - SCREESCO (RCC-KI) - Blekingesjukhuset (SCR-27010)",
                                    "PosId4": 1429,
                                    "PosLevel": 3,
                                    "UnitCode": "SCR-27010",
                                    "TopPosId": 1423,
                                    "TopPosCode": "RCC-Sam"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):RCC-Sam - RCC-KI - SCR-23010,Organisationsenhet(namn och kod):RCC Studier - SCREESCO - Centrallasarettet Växjö",
                                "id": 1430,
                                "data": {
                                    "PosId": 1430,
                                    "PosCode": "RCC-Sam - RCC-KI - SCR-23010",
                                    "PosName": "RCC Studier - SCREESCO - Centrallasarettet Växjö",
                                    "PosNameWithCode": "RCC Studier (RCC-Sam) - SCREESCO (RCC-KI) - Centrallasarettet Växjö (SCR-23010)",
                                    "PosId4": 1430,
                                    "PosLevel": 3,
                                    "UnitCode": "SCR-23010",
                                    "TopPosId": 1423,
                                    "TopPosCode": "RCC-Sam"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):RCC-Sam - RCC-KI - SCR-54010,Organisationsenhet(namn och kod):RCC Studier - SCREESCO - Centralsjukhuset Karlstad",
                                "id": 1431,
                                "data": {
                                    "PosId": 1431,
                                    "PosCode": "RCC-Sam - RCC-KI - SCR-54010",
                                    "PosName": "RCC Studier - SCREESCO - Centralsjukhuset Karlstad",
                                    "PosNameWithCode": "RCC Studier (RCC-Sam) - SCREESCO (RCC-KI) - Centralsjukhuset Karlstad (SCR-54010)",
                                    "PosId4": 1431,
                                    "PosLevel": 3,
                                    "UnitCode": "SCR-54010",
                                    "TopPosId": 1423,
                                    "TopPosCode": "RCC-Sam"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):RCC-Sam - RCC-KI - SCR-28010,Organisationsenhet(namn och kod):RCC Studier - SCREESCO - Centralsjukhuset Kristianstad",
                                "id": 1432,
                                "data": {
                                    "PosId": 1432,
                                    "PosCode": "RCC-Sam - RCC-KI - SCR-28010",
                                    "PosName": "RCC Studier - SCREESCO - Centralsjukhuset Kristianstad",
                                    "PosNameWithCode": "RCC Studier (RCC-Sam) - SCREESCO (RCC-KI) - Centralsjukhuset Kristianstad (SCR-28010)",
                                    "PosId4": 1432,
                                    "PosLevel": 3,
                                    "UnitCode": "SCR-28010",
                                    "TopPosId": 1423,
                                    "TopPosCode": "RCC-Sam"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):RCC-Sam - RCC-KI - SCR-57010,Organisationsenhet(namn och kod):RCC Studier - SCREESCO - Falu lasarett",
                                "id": 1433,
                                "data": {
                                    "PosId": 1433,
                                    "PosCode": "RCC-Sam - RCC-KI - SCR-57010",
                                    "PosName": "RCC Studier - SCREESCO - Falu lasarett",
                                    "PosNameWithCode": "RCC Studier (RCC-Sam) - SCREESCO (RCC-KI) - Falu lasarett (SCR-57010)",
                                    "PosId4": 1433,
                                    "PosLevel": 3,
                                    "UnitCode": "SCR-57010",
                                    "TopPosId": 1423,
                                    "TopPosCode": "RCC-Sam"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):RCC-Sam - RCC-KI - SCR-61010,Organisationsenhet(namn och kod):RCC Studier - SCREESCO - Gävle sjukhus",
                                "id": 1434,
                                "data": {
                                    "PosId": 1434,
                                    "PosCode": "RCC-Sam - RCC-KI - SCR-61010",
                                    "PosName": "RCC Studier - SCREESCO - Gävle sjukhus",
                                    "PosNameWithCode": "RCC Studier (RCC-Sam) - SCREESCO (RCC-KI) - Gävle sjukhus (SCR-61010)",
                                    "PosId4": 1434,
                                    "PosLevel": 3,
                                    "UnitCode": "SCR-61010",
                                    "TopPosId": 1423,
                                    "TopPosCode": "RCC-Sam"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):RCC-Sam - RCC-KI - SCR-42010,Organisationsenhet(namn och kod):RCC Studier - SCREESCO - Hallandssjukhus Halmstad",
                                "id": 1435,
                                "data": {
                                    "PosId": 1435,
                                    "PosCode": "RCC-Sam - RCC-KI - SCR-42010",
                                    "PosName": "RCC Studier - SCREESCO - Hallandssjukhus Halmstad",
                                    "PosNameWithCode": "RCC Studier (RCC-Sam) - SCREESCO (RCC-KI) - Hallandssjukhus Halmstad (SCR-42010)",
                                    "PosId4": 1435,
                                    "PosLevel": 3,
                                    "UnitCode": "SCR-42010",
                                    "TopPosId": 1423,
                                    "TopPosCode": "RCC-Sam"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):RCC-Sam - RCC-KI - SCR-42011,Organisationsenhet(namn och kod):RCC Studier - SCREESCO - Hallandssjukhus Varberg",
                                "id": 1436,
                                "data": {
                                    "PosId": 1436,
                                    "PosCode": "RCC-Sam - RCC-KI - SCR-42011",
                                    "PosName": "RCC Studier - SCREESCO - Hallandssjukhus Varberg",
                                    "PosNameWithCode": "RCC Studier (RCC-Sam) - SCREESCO (RCC-KI) - Hallandssjukhus Varberg (SCR-42011)",
                                    "PosId4": 1436,
                                    "PosLevel": 3,
                                    "UnitCode": "SCR-42011",
                                    "TopPosId": 1423,
                                    "TopPosCode": "RCC-Sam"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):RCC-Sam - RCC-KI - SCR-41012,Organisationsenhet(namn och kod):RCC Studier - SCREESCO - Helsingborgs lasarett",
                                "id": 1437,
                                "data": {
                                    "PosId": 1437,
                                    "PosCode": "RCC-Sam - RCC-KI - SCR-41012",
                                    "PosName": "RCC Studier - SCREESCO - Helsingborgs lasarett",
                                    "PosNameWithCode": "RCC Studier (RCC-Sam) - SCREESCO (RCC-KI) - Helsingborgs lasarett (SCR-41012)",
                                    "PosId4": 1437,
                                    "PosLevel": 3,
                                    "UnitCode": "SCR-41012",
                                    "TopPosId": 1423,
                                    "TopPosCode": "RCC-Sam"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):RCC-Sam - RCC-KI - SCR-61012,Organisationsenhet(namn och kod):RCC Studier - SCREESCO - Hudiksvalls Sjukhus",
                                "id": 1476,
                                "data": {
                                    "PosId": 1476,
                                    "PosCode": "RCC-Sam - RCC-KI - SCR-61012",
                                    "PosName": "RCC Studier - SCREESCO - Hudiksvalls Sjukhus",
                                    "PosNameWithCode": "RCC Studier (RCC-Sam) - SCREESCO (RCC-KI) - Hudiksvalls Sjukhus (SCR-61012)",
                                    "PosId4": 1476,
                                    "PosLevel": 3,
                                    "UnitCode": "SCR-61012",
                                    "TopPosId": 1423,
                                    "TopPosCode": "RCC-Sam"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):RCC-Sam - RCC-KI - SCR-22011,Organisationsenhet(namn och kod):RCC Studier - SCREESCO - Höglandssjukhuset Eksjö",
                                "id": 1438,
                                "data": {
                                    "PosId": 1438,
                                    "PosCode": "RCC-Sam - RCC-KI - SCR-22011",
                                    "PosName": "RCC Studier - SCREESCO - Höglandssjukhuset Eksjö",
                                    "PosNameWithCode": "RCC Studier (RCC-Sam) - SCREESCO (RCC-KI) - Höglandssjukhuset Eksjö (SCR-22011)",
                                    "PosId4": 1438,
                                    "PosLevel": 3,
                                    "UnitCode": "SCR-22011",
                                    "TopPosId": 1423,
                                    "TopPosCode": "RCC-Sam"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):RCC-Sam - RCC-KI - SCR-55012,Organisationsenhet(namn och kod):RCC Studier - SCREESCO - Lindesbergs lasarett",
                                "id": 1439,
                                "data": {
                                    "PosId": 1439,
                                    "PosCode": "RCC-Sam - RCC-KI - SCR-55012",
                                    "PosName": "RCC Studier - SCREESCO - Lindesbergs lasarett",
                                    "PosNameWithCode": "RCC Studier (RCC-Sam) - SCREESCO (RCC-KI) - Lindesbergs lasarett (SCR-55012)",
                                    "PosId4": 1439,
                                    "PosLevel": 3,
                                    "UnitCode": "SCR-55012",
                                    "TopPosId": 1423,
                                    "TopPosCode": "RCC-Sam"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):RCC-Sam - RCC-KI - SCR-64011,Organisationsenhet(namn och kod):RCC Studier - SCREESCO - Lycksele lasarett",
                                "id": 1440,
                                "data": {
                                    "PosId": 1440,
                                    "PosCode": "RCC-Sam - RCC-KI - SCR-64011",
                                    "PosName": "RCC Studier - SCREESCO - Lycksele lasarett",
                                    "PosNameWithCode": "RCC Studier (RCC-Sam) - SCREESCO (RCC-KI) - Lycksele lasarett (SCR-64011)",
                                    "PosId4": 1440,
                                    "PosLevel": 3,
                                    "UnitCode": "SCR-64011",
                                    "TopPosId": 1423,
                                    "TopPosCode": "RCC-Sam"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):RCC-Sam - RCC-KI - SCR-25010,Organisationsenhet(namn och kod):RCC Studier - SCREESCO - Länssjukhuset i Kalmar",
                                "id": 1441,
                                "data": {
                                    "PosId": 1441,
                                    "PosCode": "RCC-Sam - RCC-KI - SCR-25010",
                                    "PosName": "RCC Studier - SCREESCO - Länssjukhuset i Kalmar",
                                    "PosNameWithCode": "RCC Studier (RCC-Sam) - SCREESCO (RCC-KI) - Länssjukhuset i Kalmar (SCR-25010)",
                                    "PosId4": 1441,
                                    "PosLevel": 3,
                                    "UnitCode": "SCR-25010",
                                    "TopPosId": 1423,
                                    "TopPosCode": "RCC-Sam"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):RCC-Sam - RCC-KI - SCR-57011,Organisationsenhet(namn och kod):RCC Studier - SCREESCO - Mora lasarett",
                                "id": 1442,
                                "data": {
                                    "PosId": 1442,
                                    "PosCode": "RCC-Sam - RCC-KI - SCR-57011",
                                    "PosName": "RCC Studier - SCREESCO - Mora lasarett",
                                    "PosNameWithCode": "RCC Studier (RCC-Sam) - SCREESCO (RCC-KI) - Mora lasarett (SCR-57011)",
                                    "PosId4": 1442,
                                    "PosLevel": 3,
                                    "UnitCode": "SCR-57011",
                                    "TopPosId": 1423,
                                    "TopPosCode": "RCC-Sam"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):RCC-Sam - RCC-KI - SCR-13010,Organisationsenhet(namn och kod):RCC Studier - SCREESCO - Mälarsjukhuset Eskilstuna",
                                "id": 1443,
                                "data": {
                                    "PosId": 1443,
                                    "PosCode": "RCC-Sam - RCC-KI - SCR-13010",
                                    "PosName": "RCC Studier - SCREESCO - Mälarsjukhuset Eskilstuna",
                                    "PosNameWithCode": "RCC Studier (RCC-Sam) - SCREESCO (RCC-KI) - Mälarsjukhuset Eskilstuna (SCR-13010)",
                                    "PosId4": 1443,
                                    "PosLevel": 3,
                                    "UnitCode": "SCR-13010",
                                    "TopPosId": 1423,
                                    "TopPosCode": "RCC-Sam"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):RCC-Sam - RCC-KI - SCR-64001,Organisationsenhet(namn och kod):RCC Studier - SCREESCO - Norrlands universitetssjukhus",
                                "id": 1445,
                                "data": {
                                    "PosId": 1445,
                                    "PosCode": "RCC-Sam - RCC-KI - SCR-64001",
                                    "PosName": "RCC Studier - SCREESCO - Norrlands universitetssjukhus",
                                    "PosNameWithCode": "RCC Studier (RCC-Sam) - SCREESCO (RCC-KI) - Norrlands universitetssjukhus (SCR-64001)",
                                    "PosId4": 1445,
                                    "PosLevel": 3,
                                    "UnitCode": "SCR-64001",
                                    "TopPosId": 1423,
                                    "TopPosCode": "RCC-Sam"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):RCC-Sam - RCC-KI - SCR-13011,Organisationsenhet(namn och kod):RCC Studier - SCREESCO - Nyköpings lasarett",
                                "id": 1446,
                                "data": {
                                    "PosId": 1446,
                                    "PosCode": "RCC-Sam - RCC-KI - SCR-13011",
                                    "PosName": "RCC Studier - SCREESCO - Nyköpings lasarett",
                                    "PosNameWithCode": "RCC Studier (RCC-Sam) - SCREESCO (RCC-KI) - Nyköpings lasarett (SCR-13011)",
                                    "PosId4": 1446,
                                    "PosLevel": 3,
                                    "UnitCode": "SCR-13011",
                                    "TopPosId": 1423,
                                    "TopPosCode": "RCC-Sam"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):RCC-Sam - RCC-KI - SCR-51013,Organisationsenhet(namn och kod):RCC Studier - SCREESCO - NÄL Trollhättan",
                                "id": 1444,
                                "data": {
                                    "PosId": 1444,
                                    "PosCode": "RCC-Sam - RCC-KI - SCR-51013",
                                    "PosName": "RCC Studier - SCREESCO - NÄL Trollhättan",
                                    "PosNameWithCode": "RCC Studier (RCC-Sam) - SCREESCO (RCC-KI) - NÄL Trollhättan (SCR-51013)",
                                    "PosId4": 1444,
                                    "PosLevel": 3,
                                    "UnitCode": "SCR-51013",
                                    "TopPosId": 1423,
                                    "TopPosCode": "RCC-Sam"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):RCC-Sam - RCC-KI - SCR-51001,Organisationsenhet(namn och kod):RCC Studier - SCREESCO - Sahlgrenska universitetssjukhuset",
                                "id": 1447,
                                "data": {
                                    "PosId": 1447,
                                    "PosCode": "RCC-Sam - RCC-KI - SCR-51001",
                                    "PosName": "RCC Studier - SCREESCO - Sahlgrenska universitetssjukhuset",
                                    "PosNameWithCode": "RCC Studier (RCC-Sam) - SCREESCO (RCC-KI) - Sahlgrenska universitetssjukhuset (SCR-51001)",
                                    "PosId4": 1447,
                                    "PosLevel": 3,
                                    "UnitCode": "SCR-51001",
                                    "TopPosId": 1423,
                                    "TopPosCode": "RCC-Sam"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):RCC-Sam - RCC-KI - SCR-53014,Organisationsenhet(namn och kod):RCC Studier - SCREESCO - Skaraborgs sjukhus Falköping",
                                "id": 1448,
                                "data": {
                                    "PosId": 1448,
                                    "PosCode": "RCC-Sam - RCC-KI - SCR-53014",
                                    "PosName": "RCC Studier - SCREESCO - Skaraborgs sjukhus Falköping",
                                    "PosNameWithCode": "RCC Studier (RCC-Sam) - SCREESCO (RCC-KI) - Skaraborgs sjukhus Falköping (SCR-53014)",
                                    "PosId4": 1448,
                                    "PosLevel": 3,
                                    "UnitCode": "SCR-53014",
                                    "TopPosId": 1423,
                                    "TopPosCode": "RCC-Sam"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):RCC-Sam - RCC-KI - SCR-64010,Organisationsenhet(namn och kod):RCC Studier - SCREESCO - Skellefteå lasarett",
                                "id": 1449,
                                "data": {
                                    "PosId": 1449,
                                    "PosCode": "RCC-Sam - RCC-KI - SCR-64010",
                                    "PosName": "RCC Studier - SCREESCO - Skellefteå lasarett",
                                    "PosNameWithCode": "RCC Studier (RCC-Sam) - SCREESCO (RCC-KI) - Skellefteå lasarett (SCR-64010)",
                                    "PosId4": 1449,
                                    "PosLevel": 3,
                                    "UnitCode": "SCR-64010",
                                    "TopPosId": 1423,
                                    "TopPosCode": "RCC-Sam"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):RCC-Sam - RCC-KI - SCR-30001,Organisationsenhet(namn och kod):RCC Studier - SCREESCO - Skånes universitetssjukhus Malmö",
                                "id": 1450,
                                "data": {
                                    "PosId": 1450,
                                    "PosCode": "RCC-Sam - RCC-KI - SCR-30001",
                                    "PosName": "RCC Studier - SCREESCO - Skånes universitetssjukhus Malmö",
                                    "PosNameWithCode": "RCC Studier (RCC-Sam) - SCREESCO (RCC-KI) - Skånes universitetssjukhus Malmö (SCR-30001)",
                                    "PosId4": 1450,
                                    "PosLevel": 3,
                                    "UnitCode": "SCR-30001",
                                    "TopPosId": 1423,
                                    "TopPosCode": "RCC-Sam"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):RCC-Sam - RCC-KI - SCR-65016,Organisationsenhet(namn och kod):RCC Studier - SCREESCO - Sunderby sjukhus",
                                "id": 1451,
                                "data": {
                                    "PosId": 1451,
                                    "PosCode": "RCC-Sam - RCC-KI - SCR-65016",
                                    "PosName": "RCC Studier - SCREESCO - Sunderby sjukhus",
                                    "PosNameWithCode": "RCC Studier (RCC-Sam) - SCREESCO (RCC-KI) - Sunderby sjukhus (SCR-65016)",
                                    "PosId4": 1451,
                                    "PosLevel": 3,
                                    "UnitCode": "SCR-65016",
                                    "TopPosId": 1423,
                                    "TopPosCode": "RCC-Sam"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):RCC-Sam - RCC-KI - SCR-51014,Organisationsenhet(namn och kod):RCC Studier - SCREESCO - SÄS Borås",
                                "id": 1452,
                                "data": {
                                    "PosId": 1452,
                                    "PosCode": "RCC-Sam - RCC-KI - SCR-51014",
                                    "PosName": "RCC Studier - SCREESCO - SÄS Borås",
                                    "PosNameWithCode": "RCC Studier (RCC-Sam) - SCREESCO (RCC-KI) - SÄS Borås (SCR-51014)",
                                    "PosId4": 1452,
                                    "PosLevel": 3,
                                    "UnitCode": "SCR-51014",
                                    "TopPosId": 1423,
                                    "TopPosCode": "RCC-Sam"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):RCC-Sam - RCC-KI - SCR-21001,Organisationsenhet(namn och kod):RCC Studier - SCREESCO - Universitetssjukhuset i Linköping",
                                "id": 1453,
                                "data": {
                                    "PosId": 1453,
                                    "PosCode": "RCC-Sam - RCC-KI - SCR-21001",
                                    "PosName": "RCC Studier - SCREESCO - Universitetssjukhuset i Linköping",
                                    "PosNameWithCode": "RCC Studier (RCC-Sam) - SCREESCO (RCC-KI) - Universitetssjukhuset i Linköping (SCR-21001)",
                                    "PosId4": 1453,
                                    "PosLevel": 3,
                                    "UnitCode": "SCR-21001",
                                    "TopPosId": 1423,
                                    "TopPosCode": "RCC-Sam"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):RCC-Sam - RCC-KI - SCR-22012,Organisationsenhet(namn och kod):RCC Studier - SCREESCO - Värnamo sjukhus",
                                "id": 1454,
                                "data": {
                                    "PosId": 1454,
                                    "PosCode": "RCC-Sam - RCC-KI - SCR-22012",
                                    "PosName": "RCC Studier - SCREESCO - Värnamo sjukhus",
                                    "PosNameWithCode": "RCC Studier (RCC-Sam) - SCREESCO (RCC-KI) - Värnamo sjukhus (SCR-22012)",
                                    "PosId4": 1454,
                                    "PosLevel": 3,
                                    "UnitCode": "SCR-22012",
                                    "TopPosId": 1423,
                                    "TopPosCode": "RCC-Sam"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):RCC-Sam - RCC-KI - SCR-24010,Organisationsenhet(namn och kod):RCC Studier - SCREESCO - Västerviks sjukhus",
                                "id": 1455,
                                "data": {
                                    "PosId": 1455,
                                    "PosCode": "RCC-Sam - RCC-KI - SCR-24010",
                                    "PosName": "RCC Studier - SCREESCO - Västerviks sjukhus",
                                    "PosNameWithCode": "RCC Studier (RCC-Sam) - SCREESCO (RCC-KI) - Västerviks sjukhus (SCR-24010)",
                                    "PosId4": 1455,
                                    "PosLevel": 3,
                                    "UnitCode": "SCR-24010",
                                    "TopPosId": 1423,
                                    "TopPosCode": "RCC-Sam"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):RCC-Sam - RCC-KI - SCR-56010,Organisationsenhet(namn och kod):RCC Studier - SCREESCO - Västmanlands sjukhus Västerås",
                                "id": 1456,
                                "data": {
                                    "PosId": 1456,
                                    "PosCode": "RCC-Sam - RCC-KI - SCR-56010",
                                    "PosName": "RCC Studier - SCREESCO - Västmanlands sjukhus Västerås",
                                    "PosNameWithCode": "RCC Studier (RCC-Sam) - SCREESCO (RCC-KI) - Västmanlands sjukhus Västerås (SCR-56010)",
                                    "PosId4": 1456,
                                    "PosLevel": 3,
                                    "UnitCode": "SCR-56010",
                                    "TopPosId": 1423,
                                    "TopPosCode": "RCC-Sam"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):RCC-Sam - RCC-KI - SCR-63010,Organisationsenhet(namn och kod):RCC Studier - SCREESCO - Östersunds sjukhus",
                                "id": 1457,
                                "data": {
                                    "PosId": 1457,
                                    "PosCode": "RCC-Sam - RCC-KI - SCR-63010",
                                    "PosName": "RCC Studier - SCREESCO - Östersunds sjukhus",
                                    "PosNameWithCode": "RCC Studier (RCC-Sam) - SCREESCO (RCC-KI) - Östersunds sjukhus (SCR-63010)",
                                    "PosId4": 1457,
                                    "PosLevel": 3,
                                    "UnitCode": "SCR-63010",
                                    "TopPosId": 1423,
                                    "TopPosCode": "RCC-Sam"
                                }
                            },
                            {
                                "text": "Organisationsenhet(kod):RCC-Sam - RCC-KI - SCR-50010,Organisationsenhet(namn och kod):RCC Studier - SCREESCO - Östra sjukhuset",
                                "id": 1458,
                                "data": {
                                    "PosId": 1458,
                                    "PosCode": "RCC-Sam - RCC-KI - SCR-50010",
                                    "PosName": "RCC Studier - SCREESCO - Östra sjukhuset",
                                    "PosNameWithCode": "RCC Studier (RCC-Sam) - SCREESCO (RCC-KI) - Östra sjukhuset (SCR-50010)",
                                    "PosId4": 1458,
                                    "PosLevel": 3,
                                    "UnitCode": "SCR-50010",
                                    "TopPosId": 1423,
                                    "TopPosCode": "RCC-Sam"
                                }
                            }
                        ]
                    ]
                },
                "getRegisterRecordData": {}
            }
        }
    }
)
