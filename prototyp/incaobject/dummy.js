var formData = {
    "rootTable": "Anmälan",
    "regvars": {
        "a_lakare": {
            "description": "Anmälande läkare",
            "technicalName": "T73477",
            "sortOrder": 0,
            "compareDescription": "Anmälande läkare",
            "isComparable": true,
            "term": {
                "name": "a_lakare",
                "shortName": "a_lakare",
                "description": "Anmälande läkare",
                "dataType": "string"
            }
        },
        "a_kundnöjdhet": {
            "description": "Hur glad var rackaren",
            "technicalName": "T73477",
            "sortOrder": 0,
            "compareDescription": "Hur glad var rackaren",
            "isComparable": true,
            "term": {
                "name": "a_kundnöjdhet",
                "shortName": "a_kundnöjdhet",
                "description": "Hur glad var rackaren",
                "dataType": "string"
            }
        },
        "A_RappDat": {
            "description": "Provtagningsdatum.\nFörsta cytologiska/histologiska provtagning som verifierar cancern.",
            "technicalName": "T73423",
            "sortOrder": 0,
            "compareDescription": "Provtagningsdatum. Cytologisk/Histologisk provtagning som dokumenterar diagnos.",
            "isComparable": true,
            "term": {
                "name": "A_ProvDtm",
                "shortName": "A_provdtm",
                "description": "Provtagningsdatum. Cytologisk/Histologisk provtagning som dokumenterar diagnos.",
                "dataType": "datetime",
                "includeTime": false
            }
        },
        "a_komp": {
            "description": "Komplettering av uppgifter till registret",
            "technicalName": "T73477",
            "sortOrder": 0,
            "compareDescription": "",
            "isComparable": false,
            "term": {
                "name": "A_komp",
                "shortName": "A_komp",
                "description": "Komplettering av uppgifter till registret",
                "dataType": "boolean"
            }
        },
        "a_remiss": {
            "description": "Patienten kommer på remiss",
            "technicalName": "T97772",
            "sortOrder": 0,
            "compareDescription": "Patienten kommer på remiss",
            "isComparable": true,
            "term": {
                "name": "a_remiss",
                "shortName": "a_remiss",
                "description": "Patienten kommer på remiss",
                "dataType": "list",
                "listValues": [{
                    "id": 7481,
                    "text": "Nej",
                    "value": "0",
                    "validFrom": "0001-01-01T00:00:00",
                    "validTo": "9999-12-31T00:00:00"
                }, {
                    "id": 7482,
                    "text": "Ja",
                    "value": "1",
                    "validFrom": "0001-01-01T00:00:00",
                    "validTo": "9999-12-31T00:00:00"
                }, {
                    "id": 7483,
                    "text": "Ej tillämpbart",
                    "value": "2",
                    "validFrom": "0001-01-01T00:00:00",
                    "validTo": "9999-12-31T00:00:00"
                }]
            }
        },
        "a_remdat": {
            "description": "Remissdatum.",
            "technicalName": "T73423",
            "sortOrder": 0,
            "compareDescription": "Datum för utfärdande av remiss",
            "isComparable": true,
            "term": {
                "name": "a_remdat",
                "shortName": "a_remdat",
                "description": "Remissdatum",
                "dataType": "datetime",
                "includeTime": false
            }
        },
        "a_remdatsakn": {
            "description": "Remissdatum saknas",
            "technicalName": "T73477",
            "sortOrder": 0,
            "compareDescription": "Remissdatum saknas",
            "isComparable": true,
            "term": {
                "name": "a_remdatsakn",
                "shortName": "a_remdatsakn",
                "description": "Remissdatum saknas",
                "dataType": "boolean"
            }
        },
        "a_besokdat": {
            "description": "Besöksdatum.",
            "technicalName": "T73423",
            "sortOrder": 0,
            "compareDescription": "Besöksdatum",
            "isComparable": true,
            "term": {
                "name": "a_besokdat",
                "shortName": "a_besokdat",
                "description": "Besöksdatum",
                "dataType": "datetime",
                "includeTime": false
            }
        },
        "a_besokdatsakn": {
            "description": "Besöksdatum saknas",
            "technicalName": "T73477",
            "sortOrder": 0,
            "compareDescription": "",
            "isComparable": true,
            "term": {
                "name": "a_besokdatsakn",
                "shortName": "a_besokdatsakn",
                "description": "Besöksdatum saknas",
                "dataType": "boolean"
            }
        },
        "a_remitteraduppf": {
            "description": "Remitterad till annan klinik för uppföljning",
            "technicalName": "T73477",
            "sortOrder": 0,
            "compareDescription": "Remitterad till annan klinik för uppföljning",
            "isComparable": true,
            "term": {
                "name": "a_remitteraduppf",
                "shortName": "a_remitteraduppf",
                "description": "Remitterad till annan klinik för uppföljning",
                "dataType": "list",
                "listValues": [{
                    "id": 7481,
                    "text": "Nej",
                    "value": "0",
                    "validFrom": "0001-01-01T00:00:00",
                    "validTo": "9999-12-31T00:00:00"
                }, {
                    "id": 7482,
                    "text": "Ja",
                    "value": "1",
                    "validFrom": "0001-01-01T00:00:00",
                    "validTo": "9999-12-31T00:00:00"
                }]
            }
        },
        "a_remitteradenh": {
            "description": "Remitterad till enhet",
            "technicalName": "T73477",
            "sortOrder": 0,
            "compareDescription": "Remitterad till enhet",
            "isComparable": true,
            "term": {
                "name": "a_remitteradenh",
                "shortName": "a_remitteradenh",
                "description": "Remitterad till enhet",
                "dataType": "string"
            }
        },
        "a_remsjukkod": {
            "description": "Remitterad enhet sjukhuskod",
            "technicalName": "T73477",
            "sortOrder": 0,
            "compareDescription": "Remitterad enhet sjukhuskod",
            "isComparable": true,
            "term": {
                "name": "a_remsjukkod",
                "shortName": "a_remsjukkod",
                "description": "Remitterad enhet sjukhuskod",
                "dataType": "string"
            }
        },
        "a_remklinkod": {
            "description": "Remitterad enhet klinikkod",
            "technicalName": "T73477",
            "sortOrder": 0,
            "compareDescription": "Remitterad enhet klinikkod",
            "isComparable": true,
            "term": {
                "name": "a_remklinkod",
                "shortName": "a_remklinkod",
                "description": "Remitterad enhet klinikkod",
                "dataType": "string"
            }
        },
        "a_remEnhSaknas": {
            "description": "Remitterande enhet saknas i listan",
            "technicalName": "T73477",
            "sortOrder": 0,
            "compareDescription": "Remitterande enhet saknas i listan",
            "isComparable": true,
            "term": {
                "name": "a_remEnhSaknas",
                "shortName": "a_remEnhSaknas",
                "description": "Remitterande enhet saknas i listan",
                "dataType": "boolean"
            }
        },
        "a_remEnhFritext": {
            "description": "Remitterad enhet fritext",
            "technicalName": "T73477",
            "sortOrder": 0,
            "compareDescription": "Remitterad enhet fritext",
            "isComparable": true,
            "term": {
                "name": "a_remEnhFritext",
                "shortName": "a_remEnhFritext",
                "description": "Remitterad enhet fritext",
                "dataType": "string"
            }
        },
        "a_mdk": {
            "description": "MDK utförd",
            "technicalName": "T73477",
            "sortOrder": 0,
            "compareDescription": "MDK utförd",
            "isComparable": true,
            "term": {
                "name": "a_mdk",
                "shortName": "a_mdk",
                "description": "MDK utförd",
                "dataType": "list",
                "listValues": [{
                    "id": 7481,
                    "text": "Nej",
                    "value": "0",
                    "validFrom": "0001-01-01T00:00:00",
                    "validTo": "9999-12-31T00:00:00"
                }, {
                    "id": 7482,
                    "text": "Ja",
                    "value": "1",
                    "validFrom": "0001-01-01T00:00:00",
                    "validTo": "9999-12-31T00:00:00"
                }]
            }
        },
        "a_kirurg": {
            "description": "Kirurg",
            "technicalName": "T73477",
            "sortOrder": 0,
            "compareDescription": "",
            "isComparable": false,
            "term": {
                "name": "a_kirurg",
                "shortName": "a_kirurg",
                "description": "Kirurg",
                "dataType": "boolean"
            }
        },
        "a_kontssk": {
            "description": "Kontaktsjuksköterska",
            "technicalName": "T73477",
            "sortOrder": 0,
            "compareDescription": "",
            "isComparable": false,
            "term": {
                "name": "a_kontssk",
                "shortName": "a_kontssk",
                "description": "Kontaktsjuksköterska",
                "dataType": "boolean"
            }
        },
        "a_onkolog": {
            "description": "Onkolog",
            "technicalName": "T73477",
            "sortOrder": 0,
            "compareDescription": "",
            "isComparable": false,
            "term": {
                "name": "a_onkolog",
                "shortName": "a_onkolog",
                "description": "Onkolog",
                "dataType": "boolean"
            }
        },
        "a_medicinare": {
            "description": "Allmänmedicinare",
            "technicalName": "T73477",
            "sortOrder": 0,
            "compareDescription": "",
            "isComparable": false,
            "term": {
                "name": "a_medicinare",
                "shortName": "a_medicinare",
                "description": "Allmänmedicinare",
                "dataType": "boolean"
            }
        },
        "a_radiolog": {
            "description": "Radiolog",
            "technicalName": "T73477",
            "sortOrder": 0,
            "compareDescription": "",
            "isComparable": false,
            "term": {
                "name": "a_radiolog",
                "shortName": "a_radiolog",
                "description": "Radiolog",
                "dataType": "boolean"
            }
        },
        "a_patolog": {
            "description": "Patolog",
            "technicalName": "T73477",
            "sortOrder": 0,
            "compareDescription": "",
            "isComparable": false,
            "term": {
                "name": "a_patolog",
                "shortName": "a_patolog",
                "description": "Patolog",
                "dataType": "boolean"
            }
        }
    },
    "subTables": {}
};

var validering = new RCC.Validation();

vm = new RCC.ViewModel({
    validation: validering,
    incaForm: RCC.Utils.createDummyForm(
        formData
    ),
    incaUser: {
        "id": 6855,
        "firstName": "Utvecklaren",
        "lastName": "Konstruktörsson",
        "username": "oc0utko",
        "email": "demo@mode.se",
        "phoneNumber": null,
        "role": {
            "id": 43,
            "name": "Monitor (Kvalitetsregister)",
            "isReviewer": true
        },
        "position": {
            "id": 36,
            "name": "Medicinkliniken",
            "code": "101",
            "fullNameWithCode": "OC Demo (4) - SU/Sahlgrenska (500010) - Medicinkliniken (101)"
        },
        "region": {
            "id": 7,
            "name": "Region Demo"
        },
        "previousLogin": "2015-02-02T16:17:03.497Z"
    }
});
