/*
 * Generella konstanter
 */
var C_STATUS_MONITOR = 'Monitor';
var C_STATUS_DELSPARAD = 'Delsparad i register*';
var C_STATUS_DELSPARAD_REMITTERAT = 'Remitterat*';

var C_EVENTS_KLAR = '2';
var C_EVENTS_EJKLAR_KVAR_INKORG = '1';
var C_EVENTS_KOMPLETTERA_DELSPARA = '4';
var C_EVENTS_AVBRYT_OCH_RADERA = '6';
var C_EVENTS_SKICKA_TILL_ANNAT_OC = '7';
var C_EVENTS_SKICKA_PA_REMISS = '8';
var C_EVENTS_REMISS_DELSPARA = '23';
var C_EVENTS_AVVAKTA_DELSPARA = '28';
var C_EVENTS_EJ_KLAR_DELSPARA = '29';
var C_EVENTS_KLAR_SAND_OC = '34';
var C_EVENTS_MAKULERA = '46';
var C_EVENTS_SPARA_I_REGISTER = '38'; // Generella Register ärendetyp (istället för klar)
var C_EVENTS_SPARA = '47'; //
var C_EVENTS_SPARA_ANDRINGAR = '49'; // VisaPost ärendetyp
var C_EVENTS_STANG = '50'; // VisaPost ärendetyp. (istället för avbryt och radera)

/*
 * Generella varningsmeddelanden
 */

var C_WARNING_KOPPLA = "KOPPLA ALLTID först INNAN ett pågående ärende görs klart - gäller ÄVEN DELSPARA.\n Ett undantag: FÖRSTA ärendet för en patient om det inte finns någon cancerregisterpost.\n\n Om du har glömt att koppla och vill göra det nu, tryck på avbryt!";
var C_WARNING_AVBRYT = "Bekräfta att du vill AVBRYTA och RADERA inrapporteringen genom att trycka på OK.\n\n Om du ångrar dig och inte vill AVBRYTA och RADERA inrapporteringen, tryck på avbryt!";
var C_WARNING_MAKULERA = "Bekräfta att du vill MAKULERA inrapporteringen genom att trycka på OK.\n\n Om du ångrar dig och inte vill MAKULERA inrapporteringen, tryck på avbryt!";
var C_WARNING_KOMPLETTERING = "Kompletteringskryssrutan är ikryssad, glöm inte att kontrollera att ENBART de termer som du vill spara till registret är ikryssade!";
var C_WARNING_DELSPARA = "Bekräfta att du vill DELSPARA inrapporteringen genom att trycka på OK.\n\n Om du ångrar dig och inte vill DELSPARA inrapporteringen, tryck på avbryt! \n\n\n. KOPPLA ALLTID först INNAN du delspara och delspara BARA när inrapportering är nästan färdig.";

/*
 * Generella datumfunktioner
 */

getDagensDatum = function() {
    var currentTime = new Date();
    var currentYear = currentTime.getFullYear();
    var currentMonth = currentTime.getMonth();
    var currentDay = currentTime.getDate();
    var currentMonthShow = '';
    var currentDayShow = '';
    var strToday = '';

    currentMonthShow = currentMonth + 1;
    if (currentMonthShow < 10) {
        currentMonthShow = '0' + currentMonthShow;
    }

    currentDayShow = currentDay;
    if (currentDayShow < 10) {
        currentDayShow = '0' + currentDayShow;
    }

    strToday = currentYear + '-' + currentMonthShow + '-' + currentDayShow;

    return strToday;

}

function calcTidsskillnadAr(dateDatum1, dateDatum2) {
    var dagar_manad = new Array(31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31);

    var dateDatum2_ar = dateDatum2.getFullYear();
    var dateDatum1_ar = dateDatum1.getFullYear();

    var dateDatum2_manader = dateDatum2.getMonth(); //returnerar månader (0-11)
    var dateDatum1_manader = dateDatum1.getMonth();
    var dateDatum2_dagar = dateDatum2.getDate(); //getDate returnerar dagen I en månad (1-31)
    var dateDatum1_dagar = dateDatum1.getDate();
    var dateDatum2_timmar = dateDatum2.getHours(); //returnerar timmar (0-23)
    var dateDatum1_timmar = dateDatum1.getHours();
    var dateDatum2_minuter = dateDatum2.getMinutes(); //returnerar minuter (0-59)
    var dateDatum1_minuter = dateDatum1.getMinutes();
    var dateDatum2_sekunder = dateDatum2.getSeconds(); //returnerar sekunder (0-59)
    var dateDatum1_sekunder = dateDatum1.getSeconds();

    var dateDatum1_manader_spara = dateDatum1.getMonth();

    if (dateDatum1_sekunder > dateDatum2_sekunder) {
        dateDatum2_sekunder += 60;
        dateDatum2_minuter--;
    }
    if (dateDatum1_minuter > dateDatum2_minuter) {
        dateDatum2_minuter += 60;
        dateDatum2_timmar--;
    }
    if (dateDatum1_timmar > dateDatum2_timmar) {
        dateDatum2_timmar += 24;
        dateDatum2_dagar--;
    }
    if (dateDatum1_dagar > dateDatum2_dagar) {
        dateDatum2_dagar += dagar_manad[dateDatum1_manader_spara];
        dateDatum2_manader--;
    }
    if (dateDatum1_manader > dateDatum2_manader) {
        dateDatum2_manader += 12;
        dateDatum2_ar--;
    }
    if ((dateDatum2_ar - dateDatum1_ar) >= 0) {
        return (dateDatum2_ar - dateDatum1_ar);
    } else {
        if (dateDatum1_sekunder < dateDatum2_sekunder) {
            dateDatum1_sekunder += 60;
            dateDatum1_minuter--;
        }
        if (dateDatum1_minuter < dateDatum2_minuter) {
            dateDatum1_minuter += 60;
            dateDatum1_timmar--;
        }
        if (dateDatum1_timmar < dateDatum2_timmar) {
            dateDatum1_timmar += 24;
            dateDatum1_dagar--;
        }
        if (dateDatum1_dagar < dateDatum2_dagar) {
            dateDatum1_dagar += dagar_manad[dateDatum1_manader_spara];
            dateDatum1_manader--;
        }
        if (dateDatum1_manader < dateDatum2_manader) {
            dateDatum1_manader += 12;
            dateDatum1_ar--;
        }
        return (dateDatum1_ar - dateDatum2_ar);
    }
};

/*
 * Generella beräkningsfunktioner
 */

/**
 * Funktion för att returnera sjukhuskod
 *
 * @method getHospitalCode
 * @param {String} position Positionssträng
 * @return {String} Sjukhuskod
 */
getHospitalCode = function(position) {
    var position = position.toString();
    var hospitalCode;

    var patt = new RegExp(/\)/);

    var find = /\)/g;

    if (patt.test(position) == true) {
        var strInrapp = position.replace(/\)/g, "(");
        strInrapp = strInrapp.split("(");
        hospitalCode = strInrapp[3];
    } else {
        hospitalCode = "";
    }

    return hospitalCode;
};


/**
 * Funktion för att returnera klinikkod
 *
 * @method getClinicCode
 * @param {String} position Positionssträng
 * @return {String} Klinikkod
 */
getClinicCode = function(position) {
    var position = position.toString();;
    var clinicCode;

    var patt = new RegExp(/\)/);

    var find = /\)/g;

    if (patt.test(position) == true) {
        var strInrapp = position.replace(/\)/g, "(");
        strInrapp = strInrapp.split("(");

        var result = position.match(find); //Kontrollerar hur många högerparenteser som finns
        if (result.length == '2') //Om det bara finns 2 parenteser, dvs om inte klinikkod finns
        {
            clinicCode = "";
        } else {
            clinicCode = strInrapp[5];
        }
    } else {
        clinicCode = "";
    }

    return clinicCode;
};


/**
 * Funktion för att returnera patologkod
 *
 * @method getPathologyCode
 * @param {String} position Positionssträng
 * @return {String} Klinikkod
 */
getPathologyCode = function(position) {
    var position = position.toString();;
    var clinicCode;

    var patt = new RegExp(/\)/);

    var find = /\)/g;

    if (patt.test(position) == true) {
        var strInrapp = position.replace(/\)/g, "(");
        strInrapp = strInrapp.split("(");

        var result = position.match(find); //Kontrollerar hur många högerparenteser som finns
        if (result.length == '2') //Om det bara finns 2 parenteser, dvs om inte klinikkod finns
        {
            clinicCode = strInrapp[3];
        } else {
            clinicCode = strInrapp[5];
        }
    } else {
        clinicCode = "";
    }

    return clinicCode;
};



/**
 * Funktion för att beräkna ålder vid diagnos
 *
 * @method calcAgeAtDiagnos
 * @param {Datum} dateOfBirth Födelsedatum
 * @param {Datum} dateOfDiagnos Diagnosdatum
 * @return {Integer} Resulterande ålder
 */
calcAgeAtDiagnos = function(dateOfBirth, dateOfDiagnos) {
    var dateOfBirth = dateOfBirth;
    var dateOfDiagnos = dateOfDiagnos;

    var dateBirth = new Date();

    dateBirth.setFullYear(dateOfBirth.substring(0, 4), dateOfBirth.substring(4, 6), dateOfBirth.substring(6, 8));

    var dateDiagnos = new Date();

    dateDiagnos.setFullYear(dateOfDiagnos.substring(0, 4), dateOfDiagnos.substring(5, 7), dateOfDiagnos.substring(8, 10));

    if (isNaN(calcTidsskillnadAr(dateDiagnos, dateBirth)) == false && (dateDiagnos < dateBirth) == false) {
        return calcTidsskillnadAr(dateDiagnos, dateBirth);
    }
};

/**
 * Funktion för att beräkna summan (addera) av variabler
 *
 * @method getCalcualtedValue
 * @param {Heltal} value1 Heltalvärde
 * @param {Heltal} value1 Heltalvärde
 * @return {Heltal} Resulterande värde
 */
getCalcualtedValue = function(value1, value2) {

    var A = value1;
    var B = value2;
    var calculatedValue;


    calculatedValue = parseInt(A) + parseInt(B);
    return calculatedValue;
};

/**
 * Funktion för returnera sträng med inledande versal
 *
 * @method getStringWithCapital
 * @param {String} str Sträng att manipulera
 * @return {String} Manipulerad sträng
 */
getStringWithCapital = function(str) {
    if (str)
        return str.charAt(0).toUpperCase() + str.slice(1);
    else
        return "";
};

/*
 * Generella testfunktioner
 */

isInteger = function(n) {
    var value = n;

    if (typeof value == 'string') {
        value = parseInt(value);
    }

    return typeof value === 'number' && value % 1 == 0;
};

isFloat = function(n) {
    var value = n;

    if (typeof value == 'string') {
        value = parseFloat(value);
    } else
        return value != "" && !isNaN(value) && Math.round(value) != value;
};

isDecimalString = function(value, decimalPlaces) {
    if (value) {
        var re = new RegExp('^[-+]?(?:\\d|[1-9]\\d*)[,\.]\\d{' + decimalPlaces + '}?$');

        return re.test(value.toString());
    } else
        return false;
};

/*
 * Registervariabel funktioner
 */

/**
 * Funktion för att plocka bort ett span av objekt i en lista
 *
 * @method sliceList
 * @param {Object} obj Listobjekt
 * @param {Object} start Startindex
 * @param {Object} stop Stopindex
 */
sliceInList = function(obj, start, stop) {
    if (obj.rcc.term.dataType == 'list')
        obj.rcc.term.listValues = obj.rcc.term.listValues.slice(start, stop);
};

/**
 * Funktion för att plocka bort ett span av objekt i en lista
 *
 * @method sliceList
 * @param {Object} obj Listobjekt
 * @param {Object} start Startindex
 * @param {Object} count Amount to splice
 */
spliceInList = function(obj, start, count) {
    if (obj.rcc.term.dataType == 'list')
        obj.rcc.term.listValues.splice(start, count);
};

/**
 * Funktion för att plocka bort ett urval av val i en lista
 *
 * @method spliceListItems
 * @param {Object} obj Listobjekt
 * @param {Object} termIDs IDs of terms to slice
 */
spliceListItems = function(obj, termIDs) {
    if (obj.rcc.term.dataType == 'list') {
        for (var i = 0; i < termIDs.length; i++) {
            for (var j = obj.rcc.term.listValues.length - 1; j >= 0; j--) {
                if (obj.rcc.term.listValues[j].id == termIDs[i]) {
                    obj.rcc.term.listValues.splice(j, 1);
                }
            }
        }
    }
};


sliceListItemOnValues = function(obj, values) {
    if (values) {
        for (var i = 0; i < values.length; i++) {
            for (var j = obj.rcc.term.listValues.length - 1; j >= 0; j--) {
                if (obj.rcc.term.listValues[j].value == values[i])
                    obj.rcc.term.listValues.splice(j, 1);
            }
        }
    }
};

/**
 * Funktion för att plocka bort första objektet i en lista
 *
 * @method sliceFirstInList
 * @param {Object} list Listobjekt
 */
sliceFirstInList = function(obj) {
    if (obj.rcc.term.dataType == 'list')
        obj.rcc.term.listValues = obj.rcc.term.listValues.slice(1);
};

/**
 * Funktion för att plocka bort första tomma listvärdet i en lista
 *
 * @method sliceBlankInList
 * @param {Object} list Listobjekt
 */
sliceBlankInList = function(obj) {
    if (obj.rcc.term.dataType == 'list' && obj.rcc.term.listValues[0].value == '')
        obj.rcc.term.listValues = obj.rcc.term.listValues.slice(1);
};

/**
 * Funktion för att plocka bort ett objekt med specifikt value i en lista
 *
 * @method removeValueInList
 * @param {Object} obj Listobjekt
 * @param {Object} value Value
 */
removeValueInList = function(obj, value) {
    if (obj.rcc.term.dataType == 'list') {

        var listItem = ko.utils.arrayFirst(obj.rcc.term.listValues, function(item) {
            return item.value == value;
        });

        if (listItem)
            ko.utils.arrayRemoveItem(obj.rcc.term.listValues, listItem);
    }
};

/**
 * Funktion för search & replace i en lista
 *
 * @method replaceInList
 * @param {Object} obj Listobjekt
 * @param {Object} before Söksträng
 * @param {Object} after Sträng att byta
 */
replaceInList = function(obj, before, after) {
    ko.utils.arrayForEach(obj.rcc.term.listValues, function(item) {
        item.text = item.text.replace(before, after);
    });
};

compareAll = function(vm) {
    for (var obj in vm) {
        if (obj.substring(0, 1) != '$' && vm.hasOwnProperty(obj) && vm[obj].rcc) {
            vm[obj].rcc.showInclude(true);
        }
    }
};

compareNone = function(vm) {
    for (var obj in vm) {
        if (obj.substring(0, 1) != '$' && vm.hasOwnProperty(obj) && vm[obj].rcc)
            vm[obj].rcc.showInclude(false);
    }
};

/**
 * Set all as included in viewmodel
 *
 * @vm viewmodel
 */
includeAll = function(vm) {
    for (var obj in vm) {
        if (obj.substring(0, 1) != '$' && vm.hasOwnProperty(obj) && vm[obj].rcc)
            vm[obj].rcc.include(true);
    }
};

/**
 * Set none as included in viewmodel
 *
 * @vm viewmodel
 */
includeNone = function(vm) {
    for (var obj in vm) {
        if (obj.substring(0, 1) != '$' && vm.hasOwnProperty(obj) && vm[obj].rcc)
            vm[obj].rcc.include(false);
    }
};


/*
 * Valideringsfunktioner
 */

/**
 * Funktion för att tömma en registervariabel
 *
 * @method clear
 * @param {Object} obj Registervariabel
 */
clear = function(obj) {
    if (obj()) {
        // Debug
        //console.log("clear: " + obj.rcc.regvarName + "(" + obj.rcc.term.dataType + ")");
        obj(undefined);
    }
};


/**
 * Funktion för att tömma och deaktivera validering för en registervariabel
 *
 * @method notRequired
 * @param {Object} obj Registervariabel
 */
notRequired = function(obj) {

    if (obj.rcc.validation.required()) {
        // Debug
        //console.log("disable-validation: " + obj.rcc.regvarName + "(" + obj.rcc.term.dataType + ")");
        obj.rcc.validation.required(false);
        obj.rcc.accessed(false);
    }
};

/**
 * Funktion för att tömma och deaktivera validering för en registervariabel
 *
 * @method notRequiredAndClear
 * @param {Object} obj Registervariabel
 */
notRequiredAndClear = function(obj) {
    // Debug
    //console.log("disable-validation-and-clear: " + obj.rcc.regvarName + "(" + obj.rcc.term.dataType + ")");
    notRequired(obj);
    clear(obj);
};


/**
 * Funktion för att aktivera validering för en registervariabel
 *
 * @method required
 * @param {Object} obj Registervariabel
 */
required = function(obj) {

    if (!obj.rcc.validation.required()) {
        // Debug
        //console.log("enable-validation: " + obj.rcc.regvarName + "(" + obj.rcc.term.dataType + ")");
        obj.rcc.validation.required(true);
        obj.rcc.accessed(false);
    }
};


/**
 * Funktion för att aktivera nollställa validering för en registervariabel
 *
 * @method resetValidation
 * @param {Object} obj Registervariabel
 */
resetValidation = function(obj) {
    /*ko.utils.arrayForEach(obj.rcc.validation.errors(), function (error) {
     error.dispose();
     });*/

    ko.utils.arrayForEach(obj.rcc.validation.tests(), function(test) {
        test.dispose();
    });
};

/**
 *
 */
disableAllValidation = function(vm) {
    for (var obj in vm)
        if (obj.substring(0, 1) != '$' && vm.hasOwnProperty(obj) && vm[obj].rcc)
            notRequired(vm[obj]);
};

/**
 *
 */
enableAllValidation = function(vm) {
    for (var obj in vm)
        if (obj.substring(0, 1) != '$' && vm.hasOwnProperty(obj) && vm[obj].rcc)
            required(vm[obj]);
};


/*
 * Dialoger
 */

/**
 * Funktion för att visa "Confirm deletion"-dialog.
 *
 * @method confirmDelete
 * @param {Function} [cbConfirmed] Funktionsanrop vid confirm.
 * @param {Function} [cbCancelled] Funktionsanrop vid cancel.
 */
confirmDelete = function(cbConfirmed, cbCancelled) {
    return function(data, event) {
        var self = this,
            args = arguments;
        var call = function(cb) {
            if (cb) cb.apply(self, args);
        };
        $('#rcc-bekrafta-radera').dialog({
            dialogClass: 'rcc-dialog',
            modal: true,
            position: [200, event.clientY - 20],
            buttons: [{
                    text: 'Avbryt',
                    click: function() {
                        $(this).dialog('close');
                        call(cbCancelled);
                    },
                    priority: 'primary'
                },
                {
                    text: 'Radera',
                    click: function() {
                        $(this).dialog('close');
                        call(cbConfirmed);
                    },
                    priority: 'secondary'
                }
            ]
        });
    };
};


/**
 * Funktion för att generera ett valideringsfelmedelande
 *
 * @method generateErrorMesssage
 * @param {Object} errors Array med samtliga fel
 * @return {String} felmeddelande
 */
generateErrorMesssage = function(errors) {

    // Initiera felmeddelande
    var message = "<p><b>Formuläret innehåller " + errors.length + " fel.</b></p><ul style='margin:0; padding:5px; list-style:none;'>";

    // Skapa array av samtliga nivåtitlar och sortera ut unika
    var viewModelNames = ko.utils.arrayMap(errors, function(item) {
        return item.info.source.viewModelName
    })
    var distinctViewModelNames = ko.utils.arrayGetDistinctValues(viewModelNames);

    // Gå igenom samtliga unika nivåtitlar
    ko.utils.arrayForEach(distinctViewModelNames, function(viewModelName) {
        // Skapa array av fel för aktuell nivåtitel
        var filteredErrorsByViewModelName = ko.utils.arrayFilter(errors, function(error) {
            return (error.info.source.viewModelName === viewModelName);
        });

        // Skapa array av samtliga index för aktuell nivåtitel och sortera ut unika
        var viewModelsIndexes = ko.utils.arrayMap(filteredErrorsByViewModelName, function(item) {
            return item.info.source.viewModelIndex
        })
        var distinctViewModelsIndexes = ko.utils.arrayGetDistinctValues(viewModelsIndexes);

        // Gå igenom samtliga unika index för aktuell nivåtitel
        ko.utils.arrayForEach(distinctViewModelsIndexes, function(viewModelIndex) {
            // Skapa array av fel för aktuellt index för aktuell nivåtitel
            var filteredErrorsByViewModelIndex = ko.utils.arrayFilter(filteredErrorsByViewModelName, function(error) {
                return (error.info.source.viewModelIndex === viewModelIndex);
            });

            // Sortera fel baserat på sortOrder
            filteredErrorsByViewModelIndex.sort(function(a, b) {
                var va = typeof a.info.source.metadata.regvar != 'undefined' ? a.info.source.metadata.regvar.sortOrder : 0;
                var vb = typeof b.info.source.metadata.regvar != 'undefined' ? b.info.source.metadata.regvar.sortOrder : 0;

                return va == vb ? 0 : (va < vb ? -1 : 1);
            });


            var first = true;
            // Gå igenom samtliga fel för aktuellt index för aktuell nivåtitel
            ko.utils.arrayForEach(filteredErrorsByViewModelIndex, function(error) {
                // om första fel skriv ut nivåtitel
                if (first) {
                    // om nivå != root
                    if (viewModelName) {
                        var viewModel = error.info.source.viewModel
                        // hämta egendefinerad nivåtitel
                        var customViewModelName = vm.getCustomViewModelName(viewModel, viewModelName, viewModelIndex);
                        // om egendefinerad nivåtitel != null skriv ut annars använd generell
                        /*if (customViewModelName)
                            message += "<li style='margin-left:10px; padding-top: 10px; padding-bottom: 10px;'><b>" + customViewModelName  + "</b></li>";
                        else
                            message += "<li style='margin-left:10px; padding-top: 10px; padding-bottom: 10px;'><b>" + viewModelName + " " + (viewModelIndex + 1) + "</b></li>";
*/
                    }
                    first = false;
                }

                // skriv ut felmeddelande
                message += "<li style='margin-left:10px;'>" + ((typeof error.info.source.metadata.regvar != 'undefined') ? error.info.source.metadata.regvar.compareDescription : error.info.source.metadata.term.description) +
                    ":</li><li style='margin-left:20px; padding-bottom: 5px;'>" + error.message + "</li>";
            });
        });

    });

    message += "</ul>";
    return message;
};

/**
 * Funktion för att samla formulärspecifika fel
 *
 * @method generateCustomViewModelErrors
 * @param {Object} viewModel referens till nivå
 * @return {Array} Array med formulärspecifika fel
 */
generateCustomViewModelErrors = function(viewModel) {
    // hämta formulärspecifika fel för akutell nivå
    var errorArray = vm.getCustomViewModelErrors(viewModel, null, null);

    // gå igenom undernivåer
    for (var table in viewModel.$$) {
        // gå igenom samtliga index för aktuell undernivå
        for (var i = 0; i < viewModel.$$[table]().length; i++) {
            // hämta formulärspecifika fel för akutellt index och lägg till array med formulärspecifika fel
            errorArray = errorArray.concat(vm.getCustomViewModelErrors(viewModel.$$[table]()[i], table, i));
            generateCustomViewModelErrors(viewModel.$$[table]()[i]);
        }
    }

    return errorArray;
};

/**
 * Skapa ett formulärspecifikt ("custom") felobject
 *
 * @method createCustomViewModelError
 * @param {Object} viewModel referens till nivå
 * @param {String} viewModelName nivåtitel
 * @param {Integer} viewModelIndex nivåindex
 * @param {String} titel feltitel
 * @param {String} message felmeddelande
 * @param {String} sortOrder sorteringsvärde
 * @return {Object} felObject
 */
createCustomViewModelError = function(viewModel, viewModelName, viewModelIndex, title, message, sortOrder) {
    var errorObj = new Object();

    errorObj.info = new Object();
    errorObj.info.source = new Object();
    errorObj.info.source.viewModel = viewModel;
    errorObj.info.source.viewModelName = viewModelName;
    errorObj.info.source.viewModelIndex = viewModelIndex;

    errorObj.info.source.metadata = new Object();
    errorObj.info.source.metadata.regvar = new Object();
    errorObj.info.source.metadata.regvar.compareDescription = title;
    errorObj.info.source.metadata.regvar.sortOrder = sortOrder;

    errorObj.message = message;

    return errorObj;
};



/**
 * Funktion för att beräkna ett decimalvärde baserat på ett procentvärde och ett decimalvärde
 *
 * @method getCalcualtedDecimalValue
 * @param {Integer} value1 Procentvärde
 * @param {Decimal} value2 Decimalvärde
 * @return {String} Resulterande decimalvärde
 */
getCalcualtedDecimalValue = function(value1, value2) {
    var calculatedValue;

    calculatedValue = (value2 / 100) * value1;

    return calculatedValue;
};


/**
 * Funktion för att beräkna ett procentvärde baserat på två decimalvärden
 *
 * @method getCalcualtedPercentValue
 * @param {Decimal} value1 Decimalvärde
 * @param {Decimal} value2 Decimalvärde
 * @return {String} Resulterande procentvärde
 */
getCalcualtedPercentValue = function(value1, value2) {
    var calculatedValue;

    calculatedValue = (value2 / value1) * 100;

    return calculatedValue;
};
