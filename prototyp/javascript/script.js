$(document).ready(function() {

    /**
     *  Initialize viewmodel
     */

    var validering = new RCC.Validation();
    var vm = new RCC.ViewModel({
        validation: validering
    });
    window.vm = vm;

    //********************
    // stäng av runtime validering
    vm.$validation.enabled(false);

    var lastFocused;
    $('#rcc-form').on({
        'focus': function() {
            lastFocused = $(this);
        }
    }, 'input, select');

    $("[data-toggle=popover]").popover();

    //var template = '<div class="popover rcc-errorpopover"><div class="arrow"></div><div class="popover-content"></div></div>';

    //vm.a_kirurg(true);

    $('body').on('click', function(e) {
        $('[data-toggle="popover"]').each(function(index, item, i) {

            var binding = $(item).data().bind;

            if (binding && binding.toLowerCase().indexOf("rcc-helpme:") >= 0) {
                //the 'is' for buttons that trigger popups
                //the 'has' for icons within a button that triggers a popup
                // the last custom addition of select "[for=this.id] > *" is to capture clicks on the label itself (the text) or the child element (the icon)
                if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0 && !$("[helpId=" + this.id + "]").is(e.target) && !$("[helpId=" + this.id + "] > *").is(e.target)) {
                    $(this).popover('hide');
                }
            }
        });
    });

    $('#rcc-form').on('click', 'span span.rcc-help', function(e) {
        e.stopPropagation();
    });

        /*$('#rcc-form').on('click', function(e) {
          if($(e.target) == 'span span.rcc-help')
            e.stopPropagation();
        });*/

    vm.LKF_help = "Variabeln får värde automatiskt men kan ändras.";
    vm.inrapp_help = "Vem den anmälande inrapportören är. Det tillhandahålls av systemet vid uppstart av ärende av inrapportör, monitor måste sätta detta själv.";
    vm.lakare_help = "Vilken läkare är ansvarig för patienten och de uppgifter som rapporteras in.";
    vm.canceranm_help = "Alla uppgifter i underliggande ruta är en spegling av data som har rapporterats in i formuläret och ska tjäna till att underlätta för koppling mot cancerregistret.";
    vm.remitteradenh_help = "I listan finns de kliniker som är inlagda och har inrapportörer i INCA. Den går att söka/filtrera på det du vill hitta genom att du skriver i rutan, till exempel sjukhusnamn. Om enheten skulle saknas i listan var vänlig kontakta support på RCC i din region och meddela detta.";
    vm.diagnosdat_help = "Datum för diagnosbesked och första behandlingsdiskussion.";

    vm.a_omv_kssk_warning = 'Observera att detta är enbart ett giltigt värde för patienter med diagnos från 2015-01-01 till 2015-12-31';
    vm.a_remiss_warning = 'Observera att detta är enbart ett giltigt värde för patienter med diagnos från 2015-01-01 till 2015-12-31';
    vm.a_mdk_prof_warning = 'Alla professioner bör vara närvarande!';

    // variabel för att bocka för/bocka av alla include kryssrutor
    vm.include = ko.observable(true);

    // variabel för att visa/dölja alla gömda delar av formuläret
    vm.forceShow = ko.observable(false);

    vm.externalInca = ko.observable(true);

    vm.validateData = function() {

        if ($('#atgardselect').val() != '-- Välj --') {

            vm.$validation.enabled(true);
            var errors = validering.errors();
            var antalFel = errors.length;

            // hämta formulärspecifika valideringsfel
            if(!vm.a_inr_komplett())
                errors = errors.concat(generateCustomViewModelErrors(vm));

            // fel finns, visa och avbryt åtgärd
            if (antalFel > 0) {
                // Visa alla fel och valideringsfel
                validering.markAllAsAccessed();

                var errorMessage = generateErrorMesssage(errors);

                // start bygget av felmeddelande och lägg till antal fel
                //var sErrorMessage = "<p><b>Formuläret innehåller " + antalFel + " fel.</b></p><ul style='margin:0; padding:5px; list-style:none;'>";

                // lägg till samtliga felande registervariablers beskrivningar och felmeddelanden
                /*ko.utils.arrayForEach(errors, function(error) {
                    sErrorMessage = sErrorMessage + "<li style='margin-left:10px;'>" + vm[error.info.source.name].rcc.term.description +
                        ":</li><li style='margin-left:20px; padding-bottom: 5px;'>" + error.message + "</li>";
                });*/

                //sErrorMessage += "</ul>";

                // visa felmeddelande
                //getValidationDialog("Validering", sErrorMessage);
                vm.showValidationDialog(errorMessage);

                // deaktivera formulärvalidering
                vm.$validation.enabled(false);

                return false;
            }
            // inga fel, skicka
            else {
                vm.actionsToDoBeforePost();
                return true;

            }
        }

    };

    /**
     * Funktion för att definera egna nivåtitlar
     *
     * @method getCustomViewModelName
     * @param {Object} viewModel referens till nivå
     * @param {String} viewModelName nivåtitel
     * @param {Integer} viewModelIndex nivåindex
     * @return {String} nivåtitel
     */
    vm.getCustomViewModelName = function(viewModel, viewModelName, viewModelIndex) {
        var title = null;

        /* EGNA DEFINITIONER (start) */
        /* EGNA DEFINITIONER (slut) */

        return title;
    };

    /**
     * Funktion för att definera formulärspecikfika fel för ett index för aktuell nivå
     *
     * @method getCustomViewModelErrors
     * @param {Object} viewModel referens till nivå
     * @param {String} viewModelName nivåtitel
     * @param {Integer} viewModelIndex nivåindex
     * @return {Array} Array med formulärspecifika fel
     */
    vm.getCustomViewModelErrors = function(viewModel, viewModelName, viewModelIndex) {
        var errorArray = [];

        /* EGNA DEFINITIONER (start) */
        if (vm.externalInca())
            var action = '34';
        else
            var action = inca.errand.action.val();

        // Om nivå = root
        if(!viewModelName){
            // "Kardiovaskulär sjukdom"
            /*if(!vm.A_KardiovaskularSjukdomOK()){
                var title = "Kardiovaskulär sjukdom";
                var message = "Minst ett alternativ måste vara valt";
                var sortOrder = 1000;
                // Skapa och lägg till fel
                errorArray.push(createCustomViewModelError(viewModel, viewModelName, viewModelIndex, title, message, sortOrder));
            }
            // "Cytogenetik vid diagnos"
            if(!vm.A_CytogenetikOK()){
                var title = "Cytogenetik vid diagnos";
                var message = "Minst ett alternativ måste vara valt";
                var sortOrder = 1001;
                // Skapa och lägg till fel
                errorArray.push(createCustomViewModelError(viewModel, viewModelName, viewModelIndex, title, message, sortOrder));
            }
            // "Kromosomförändringar"
            if(!vm.A_KromosomOK()){
                var title = "Kromosomförändringar";
                var message = "Minst ett alternativ måste vara valt";
                var sortOrder = 1002;
                // Skapa och lägg till fel
                errorArray.push(createCustomViewModelError(viewModel, viewModelName, viewModelIndex, title, message, sortOrder));
            }*/

        }
        /* EGNA DEFINITIONER (slut) */

        return errorArray;
    };

    vm.sortUnitList = function() {

        var userPos = vm.$user.position.fullNameWithCode;

        var patt = new RegExp(/\)/);

        var find = /\)/g;

        var first = [];

        if (patt.test(userPos) == true) {
            var strInrapp = userPos.replace(/\)/g, "(");
            strInrapp = strInrapp.split("(");
            userPos = strInrapp[1];
        }

        for (var i = positions.length - 1; i >= 0; i--) {
            var pos = positions[i].data.PosNameWithCode;

            if (patt.test(pos) == true) {
                var strInrapp = pos.replace(/\)/g, "(");
                strInrapp = strInrapp.split("(");
                pos = strInrapp[1];
            }

            if (pos == userPos) {
                // Om regionspositionen är samma som användarens, shift på positions[i] och push(?) på first
                first.unshift(positions.splice(i, 1)[0]);
            }

        }

        // Efter att alla positioner loopats igenom, concat first med positions och få ut en array med "rätt" ordning
        positions = first.concat(positions);

        return positions;
    };

    vm.a_uppf_sjhklk.listValues = ko.observableArray(vm.sortUnitList());

    vm.handleCompareResult = function(data) {
        var shortName;

        for (shortName in data) {
            // Fyll inca.form.data med resultatet av jämförelsen
            //inca.form.data.regvars[shortName].value = data[shortName].value;
            //vm[shortName](data[shortName].value);

            // include motsvarar kryssrutorna för granskande roller
            if (inca.user.role.isReviewer) {
                //inca.form.data.regvars[shortName].include = data[shortName].include;
                vm[shortName].rcc.include(data[shortName].include);
            }
        }
    };

    //inca.off('comparison');
    //inca.on('comparison', vm.handleCompareResult);

    //Slå av/på monitorkryssrutor
    ko.computed(function() {
        if (vm.$user.role.isReviewer) {
            compareAll(vm);
        } else {
            compareNone(vm);
        }
    });

    ko.computed(function() {
        if (vm.$user.role.isReviewer) {
          vm.a_inr_komplett.rcc.showInclude(false);
        }
    });

    vm.include.subscribe(function(newValue) {
        if (newValue == 0) {
            includeNone(vm);
        } else {
            includeAll(vm);
        }
    });

    //notRequired(vm.$vd.vd_anmalan_bc);

    /*ko.computed(function() {

        required(vm.a_remitteradenh);
        required(vm.a_remEnhFritext);

        if (vm.a_komp()) {
            notRequired(vm.A_RappDat);
            notRequired(vm.a_remiss);
            notRequired(vm.a_remdat);
            notRequired(vm.a_besokdat);
            notRequired(vm.a_remitteraduppf);
        } else {
            required(vm.A_RappDat);
            required(vm.a_remiss);
            required(vm.a_remdat);
            required(vm.a_besokdat);
            required(vm.a_remitteraduppf);
        }

    });


    vm.a_besokdat.rcc.validation.add(new RCC.Validation.Tests.NotBeforeDate(vm.a_besokdat, {
        dependables: [vm.a_remdat],
        ignoreMissingValues: true
    }));*/


    vm.getSelectedAndrUppfOrgList = function(id) {
        if (id != null) {
            var item = ko.utils.arrayFirst(vm.a_uppf_sjhklk.listValues() || [], function(item) {
                return item.id == id;
            });

            if (!item)
                return undefined;

            return item;
        } else {
            return undefined;
        }
    };

    vm.a_uppf_sjhklk.subscribe(function(newValue) {
        if (newValue) {
            var inUnitName = vm.getSelectedAndrUppfOrgList(vm.a_uppf_sjhklk()).data['PosNameWithCode'];
            var inSjKod = getHospitalCode(inUnitName);
            var inKlinKod = getClinicCode(inUnitName);

            // sjukhus kod
            vm.a_uppf_sjhkod(inSjKod);
            // klinik kod
            vm.a_uppf_klkkod(inKlinKod);
        } else if (!vm.a_uppf_sjhklk()) {
            clear(vm.a_uppf_sjhkod);
            clear(vm.a_uppf_klkkod);
        }
    });

    /*vm.a_remEnhSaknas.subscribe(function(newValue) {
        if (newValue) {
            clear(vm.a_remitteradenh);
            clear(vm.a_remsjukkod);
            clear(vm.a_remklinkod);
        } else if (!newValue) {
            clear(vm.a_remEnhFritext);
            clear(vm.a_remsjukkod);
            clear(vm.a_remklinkod);
        }
    });*/

    /*ko.components.register('rcc-text-input-editor', {
      viewModel: function(params) {
        this.text = ko.observable(params && params.initialText || '');
      },
      template: 'Message: <input data-bind="value: text" /> '
        + '(length: <span data-bind="text: text().length"></span>)'
  });*/

  vm.showValidationDialog = function(validationErrors) {
    var $validationModal =
      $('<div class="modal fade" id="modalContainer" role="dialog"><div class="modal-dialog"><div class="modal-content" id="modalContent">' +
        '<div class="modal-header" id="modalHeader"><h4 class="modal-title" id="modalTitle">Validering</h4></div>' +
        '<div class="modal-body" id="modalBody">' + validationErrors + '</div><div class="modal-footer" id="modalFooter">' +
        '<button type="button" class="btn btn-default" data-dismiss="modal" data-bind="click: function() {$(\'.rcc-invalid:first\').focus();}">' +
        'Ok</button></div><div></div></div>');

    $validationModal.modal({ backdrop: 'static', keyboard: false });
  };

    ko.applyBindings(vm);

    $("#markera").prop('checked', true);

});
