$(document).ready(function() {
    /**
     *  Initialize viewmodel
     */
    /*var validering = new RCC.Validation();
    var vm = new RCC.ViewModel({
        validation: validering
    });*/
    window.vm = vm;

    //********************
    // stäng av runtime validering
    vm.$validation.enabled(false);

    vm.validated = ko.observable(false);

    var lastFocused;
    $('#rcc-form').on({
        'focus': function() {
            lastFocused = $(this);
        }
    }, 'input, select');

    $("[data-toggle=popover]").popover();

    //var template = '<div class="popover rcc-errorpopover"><div class="arrow"></div><div class="popover-content"></div></div>';

    $('body').on('click', function(e) {
        $('[data-toggle="popover"]').each(function(index, item, i) {

            var binding = $(item).data().bind;

            if (binding && binding.toLowerCase().indexOf("rcc-helpme:") >= 0) {
                //the 'is' for buttons that trigger popups
                //the 'has' for icons within a button that triggers a popup
                // the last custom addition of select "[for=this.id] > *" is to capture clicks on the label itself (the text) or the child element (the icon)
                if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0 && !$("[for=" + this.id + "]").is(e.target) && !$("[for=" + this.id + "] > *").is(e.target)) {
                    $(this).popover('hide');
                }
            }
        });
    });

    $(document).on('click', '#canceranmHelp', function(e) {
        e.stopPropagation();
    });

    vm.LKF_help = "Variabeln får värde automatiskt men kan ändras.";
    vm.anmInrapp = "Vem den anmälande inrapportören är. Det tillhandahålls av systemet vid uppstart av ärende av inrapportör, monitor måste sätta detta själv.";
    vm.anmLakare = "Vilken läkare är ansvarig för patienten och de uppgifter som rapporteras in.";
    vm.canceranm = "Alla uppgifter i underliggande ruta är en spegling av data som har rapporterats in i formuläret och ska tjäna till att underlätta för koppling mot cancerregistret.";
    vm.remitteradenh = "I listan finns de kliniker som är inlagda och har inrapportörer i INCA. Den går att söka/filtrera på det du vill hitta genom att du skriver i rutan, till exempel sjukhusnamn. Om enheten skulle saknas i listan var vänlig kontakta support på RCC i din region och meddela detta.";

    // variabel för att bocka för/bocka av alla include kryssrutor
    vm.include = ko.observable(true);

    // variabel för att visa/dölja alla gömda delar av formuläret
    vm.forceShow = ko.observable(false);

    vm.validateData = function() {

        if ($('#atgardselect').val() != '-- Välj --') {

            vm.$validation.enabled(true);
            var errors = validering.errors();
            var antalFel = errors.length;

            // fel finns, visa och avbryt åtgärd
            if (antalFel > 0) {
                // Visa alla fel och valideringsfel
                validering.markAllAsAccessed();

                // start bygget av felmeddelande och lägg till antal fel
                var sErrorMessage = "<p><b>Formuläret innehåller " + antalFel + " fel.</b></p><ul style='margin:0; padding:5px; list-style:none;'>";

                // lägg till samtliga felande registervariablers beskrivningar och felmeddelanden
                ko.utils.arrayForEach(errors, function(error) {
                    sErrorMessage = sErrorMessage + "<li style='margin-left:10px;'>" + vm[error.info.source.name].rcc.term.description +
                        ":</li><li style='margin-left:20px; padding-bottom: 5px;'>" + error.message + "</li>";
                });

                sErrorMessage += "</ul>";

                // visa felmeddelande
                //getValidationDialog("Validering", sErrorMessage);
                $('#validationErrors').html(sErrorMessage);
                $('#validationModal').modal('show');

                // deaktivera formulärvalidering
                vm.$validation.enabled(false);
                vm.validated(true);
                
                return false;
            }
            // inga fel, skicka
            else {
                vm.actionsToDoBeforePost();
                return true;

            }
        }

    };

    vm.sortUnitList = function() {

        var userPos = vm.$user.position.fullNameWithCode;

        var patt = new RegExp(/\)/);

        var find = /\)/g;

        var first = [];

        if (patt.test(userPos) == true) {
            var strInrapp = userPos.replace(/\)/g, "(");
            strInrapp = strInrapp.split("(");
            userPos = strInrapp[1];
        }

        for (var i = positions.length - 1; i >= 0; i--) {
            var pos = positions[i].data.PosNameWithCode;

            if (patt.test(pos) == true) {
                var strInrapp = pos.replace(/\)/g, "(");
                strInrapp = strInrapp.split("(");
                pos = strInrapp[1];
            }

            if (pos == userPos) {
                // Om regionspositionen är samma som användarens, shift på positions[i] och push(?) på first
                first.unshift(positions.splice(i, 1)[0]);
            }

        }

        // Efter att alla positioner loopats igenom, concat first med positions och få ut en array med "rätt" ordning
        positions = first.concat(positions);

        return positions;
    };

    vm.a_remitteradenh.listValues = ko.observableArray(vm.sortUnitList());

    vm.handleCompareResult = function(data) {
        var shortName;

        for (shortName in data) {
            // Fyll inca.form.data med resultatet av jämförelsen
            //inca.form.data.regvars[shortName].value = data[shortName].value;
            //vm[shortName](data[shortName].value);

            // include motsvarar kryssrutorna för granskande roller
            if (inca.user.role.isReviewer) {
                //inca.form.data.regvars[shortName].include = data[shortName].include;
                vm[shortName].rcc.include(data[shortName].include);
            }
        }
    };

    //inca.off('comparison');
    //inca.on('comparison', vm.handleCompareResult);

    //Slå av/på monitorkryssrutor
    ko.computed(function() {
        if (vm.$user.role.isReviewer) {
            /*if (!vm.a_komp()) {
              compareNone(vm);
            } else {*/
            compareAll(vm);
            //}
        }
    });

    ko.computed(function() {
        vm.a_komp.rcc.showInclude(false);
    });

    vm.include.subscribe(function(newValue) {
        if (newValue == 0) {
            includeNone(vm);
        } else {
            includeAll(vm);
        }
    });

    ko.computed(function() {

        required(vm.a_remitteradenh);
        required(vm.a_remEnhFritext);

        if (vm.a_komp()) {
            notRequired(vm.A_RappDat);
            notRequired(vm.a_remiss);
            notRequired(vm.a_remdat);
            notRequired(vm.a_besokdat);
            notRequired(vm.a_remitteraduppf);
        } else {
            required(vm.A_RappDat);
            required(vm.a_remiss);
            required(vm.a_remdat);
            required(vm.a_besokdat);
            required(vm.a_remitteraduppf);
        }

    });


    vm.a_besokdat.rcc.validation.add(new RCC.Validation.Tests.NotBeforeDate(vm.a_besokdat, {
        dependables: [vm.a_remdat],
        ignoreMissingValues: true
    }));


    vm.getSelectedAndrUppfOrgList = function(id) {
        if (id != null) {
            var item = ko.utils.arrayFirst(vm.a_remitteradenh.listValues() || [], function(item) {
                return item.id == id;
            });

            if (!item)
                return undefined;

            return item;
        } else {
            return undefined;
        }
    };

    vm.a_remitteradenh.subscribe(function(newValue) {
        if (newValue) {
            var inUnitName = vm.getSelectedAndrUppfOrgList(vm.a_remitteradenh()).data['PosNameWithCode'];
            var inSjKod = getHospitalCode(inUnitName);
            var inKlinKod = getClinicCode(inUnitName);

            // sjukhus kod
            vm.a_remsjukkod(inSjKod);
            // klinik kod
            vm.a_remklinkod(inKlinKod);
        } else if (!vm.a_remitteradenh() && !vm.a_remEnhSaknas()) {
            clear(vm.a_remsjukkod);
            clear(vm.a_remklinkod);
        }
    });

    vm.a_remEnhSaknas.subscribe(function(newValue) {
        if (newValue) {
            clear(vm.a_remitteradenh);
            clear(vm.a_remsjukkod);
            clear(vm.a_remklinkod);
        } else if (!newValue) {
            clear(vm.a_remEnhFritext);
            clear(vm.a_remsjukkod);
            clear(vm.a_remklinkod);
        }
    });

    ko.applyBindings(vm);

    $("#markera").prop('checked', true);

});
